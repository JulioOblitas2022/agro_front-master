import React from 'react';
import { Switch, Route, Redirect } from 'react-router-dom';

import { AuthenticationRoutes } from '../modules/authentication/AuthenticationRoutes';

export const AuthRouter = () => {
    return (
        <div className="">
            <Switch>
                <Route exact path="/auth/login" component={ AuthenticationRoutes } />
                <Redirect to="/auth/login" />
            </Switch>
        </div>
    )
}
