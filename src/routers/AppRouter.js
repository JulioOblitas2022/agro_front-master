import React, {useEffect} from 'react';
import {useDispatch, useSelector} from 'react-redux';
import { BrowserRouter as Router, Redirect, Switch } from 'react-router-dom';

import PrincipalLayout from '../framework/layout/PrincipalLayout';
import { checkLoggedIn } from '../services/user.service';
import { AuthRouter } from './AuthRouter';
import { PrivateRoute } from './PrivateRoute';
import { PublicRoute } from './PublicRoute';
import { LoadingPage } from '../modules/authentication/loading/LoadingPage';

export const AppRouter = () => {

    const dispatch = useDispatch();
    const { isLoggedIn, loading } = useSelector( state => state.auth );
    
    useEffect(() => { dispatch(checkLoggedIn()) }, [dispatch]);

    if ( loading ) {
        return (
            <LoadingPage />
        )
    }

    //https://simplelineicons.github.io/
    //https://fontawesome.com/icons

    return (
        <Router>
            <div>
                <Switch>
                    <PublicRoute path="/auth" component={ AuthRouter } isAuthenticated={ isLoggedIn } />
                    <PrivateRoute path="/" component={ PrincipalLayout } isAuthenticated={ isLoggedIn } />
                    <Redirect to="/auth/login" />
                </Switch>
            </div>
        </Router>
    )

}
