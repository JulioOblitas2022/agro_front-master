import { PAGE_MODE, URL_MODE } from '../../constants/constants';
import { UrlUtil } from '../../util/UrlUtil';

export const BASE_PAGE_PROP = {
    PARAM_ID: 'paramId',
    URL_MODE: 'urlMode',
    PAGE_MODE: 'pageMode'
};

export class BasePageApi {
    mapComponentApi = {};
    modeConfig;
    validationConfig;
    properties = {
        paramId: null,
        urlMode: null
    };
    getParentApi = null;//en el caso de ser popup
    name = null;
    config = null;
    url = null;
    constructor() {
        //this.procesarUrl();
    }
    isModal = () => !!this.getParentApi;
    getApi = () => {
        return this;
    }
    setModeConfig(config) {
        this.modeConfig = config;
    }
    setValidationConfig(config) {
        this.validationConfig = config;
    }
    getModeComponentConfig = (name) => {
        if (!this.getModeConfig())
            return null;
        return this.getModeConfig()[name];
    }
    getValidationComponentConfig = (name) => {
        if (!this.getValidationConfig())
            return null;
        return this.getValidationConfig()[name];
    }
    getModeConfig = () => {
        if (!this.modeConfig)
            return null;
        return this.modeConfig[this.getPageMode()];
    }
    getValidationConfig = () => {
        if (!this.validationConfig)
            return null;
        return this.validationConfig[this.getPageMode()] || this.validationConfig;
    }
    /**
     * Metodo para agregar los componentes hijos, componente hijo debe utilizarlo para registrarse y asi pueda ser invocado
     * de la siguiente forma: const form = parent.getComponent('form') 
     * @param {string} name - Es el nombre del componente hijo.
     * @param {object} ref - Es la referencia a los metodos que expone el componente hijo.
     */
    addComponent = (name, ref) => {
        //console.log('registrando componente hijo => ', name);
        this.mapComponentApi[name] = ref;
        return this;
    };
    getComponent = name => {
        let dotPos = name.indexOf('.');
        if (dotPos > -1) {
            let componentName = name.substr(0, dotPos);
            return this.mapComponentApi[componentName]?.getComponent(name);
        }
        return this.mapComponentApi[name];
    }
    getParamId = () => {
        return this.properties[BASE_PAGE_PROP.PARAM_ID];
    }
    getUrlMode = () => {
        return this.properties[BASE_PAGE_PROP.URL_MODE];
    }
    getPageMode = () => {
        return this.properties[BASE_PAGE_PROP.PAGE_MODE];
    }
    getDataPageMode = (data) => {
        return data[this.getPageMode()];
    }



    setParamId(paramId) {
        this.properties[BASE_PAGE_PROP.PARAM_ID] = paramId;
    }
    setUrlMode(urlMode) {
        this.properties[BASE_PAGE_PROP.URL_MODE] = urlMode;
    }
    setPageMode(pageMode) {
        this.properties[BASE_PAGE_PROP.PAGE_MODE] = pageMode;
    }

    getParamIdFromPath(path) {
        return !isNaN(path) ? path : (path.indexOf('id-') === 0 ? path.substring(3) : null);
    }

    procesarUrl() {
        const lastPath = UrlUtil.getLastPath(this.config?.url);
        const previousLast = UrlUtil.getPreviousLastPath(this.config?.url);
        //const previousParamId = !isNaN(previousLast) ?  previousLast : (previousLast.indexOf('id-')==0 ? previousLast.substring(3) : null);

        //identificando el tipo de url de la pagina
        if (lastPath === 'new') {//  New Page  =>  /page/new
            this.setUrlMode(URL_MODE.NEW);
            this.setPageMode(PAGE_MODE.NEW);
        }
        else if (lastPath === 'update') {//  Update Page =>  /page/1/update
            this.setParamId(this.getParamIdFromPath(previousLast));
            this.setUrlMode(URL_MODE.UPDATE);
            this.setPageMode(PAGE_MODE.UPDATE);
        }
        else if (this.getParamIdFromPath(lastPath)) {// Read Page =>  /page/1
            this.setParamId(this.getParamIdFromPath(lastPath));
            this.setUrlMode(URL_MODE.READ);
            this.setPageMode(PAGE_MODE.READ);
        }
        else {
            this.setUrlMode(URL_MODE.OTHER);
            this.setPageMode(PAGE_MODE.OTHER);
        }
    }

    showModal(modalConfig) {
        const getModal = () => this.getComponent('modal');
        getModal().show(modalConfig);
    }
    hideModal() {
        const getModal = () => this.getComponent('modal');
        getModal().hide();
    }
    hideChildModal = () => {
        const getModal = () => this.getComponent('modal');
        getModal().hide();
    }
    hideThisModal = () => {
        if (this.getParentApi)
            this.getParentApi().hideChildModal();
        else
            console.error(`BasePageApi[${this.name}].hideThisModal: Esta página no es un Modal`);
    }
    getEvent = (eventName) => {
        if (eventName && this.config.events)
            return this.config.events[eventName];
        return null;
    }
    /*
    close = () => {//cierra la pantalla en caso de ser modal
        console.log('close Popup');
    }*/
}