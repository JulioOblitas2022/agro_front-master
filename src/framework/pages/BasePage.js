import './BasePage.css';
import React from 'react'
import ModalFw from '../modal/ModalFw';
import { BasePageApi } from './BasePageApi';
import HeaderBarFw from '../header-bar/HeaderBarFw';

export class BasePage extends React.Component {
    api = null;
    name = null;
    /* REACT method => inicio */
    constructor(props) {
        super(props);
        const { name, getParentApi, config } = props;
        this.name = name || 'basePage';

        this.api = new BasePageApi();
        this.api.getParentApi = getParentApi;
        this.api.name = name;
        this.api.config = config;
        //this.api.url = url;
        this.api.procesarUrl();
       // console.log(`BasePage[${this.name}].constructor`);
    }
    getName = () => this.name;
    componentDidMount() {
        //console.log(`${this.constructor.name} componentDidMount`);
        setTimeout(() => {
            //self.initPageBase();
        }, 1000);

    }
    componentWillUnmount() {
        //console.log(`${this.constructor.name} componentWillUnmount`);
    }
    componentDidUpdate() {
        //this.initPageBase();
    }
    render() {
        console.log("Page.render");
        //if (this.createBarHeader) console.error('Metodo [Page.createBarHeader] deprecado, mueva el codigo hacia => [Page.createPageProperties], ejemplo => [PersonaAdminPage.createPagePropertie]');
        const properties = this.createPagePropertiesBase(this.api);
        if (properties.mode)
            this.api.setModeConfig(properties.mode());
        if (properties.validation)
            this.api.setValidationConfig(properties.validation());
        return (
            <>
                <div className="base-page-header">
                    <HeaderBarFw
                        //name={`${this.getName()}.headerBar`}
                        //name = {'headerBar'}
                        config={{
                            title: properties.title,
                            icon: properties.icon || 'icon-user',
                            button: properties.barHeader,
                            //collapsible: config.collapsible
                        }}
                    //getParentApi={pageApi.getApi}
                    //setIsCollapsed = {setIsCollapsed}
                    //isCollapsed = {isCollapsed}
                    />
                </div>
                {this.renderPage(this.api)}
                {this.createModal(this.api, {})}
            </>
        )
    }
    /* REACT method => fin */



    /* customize method => inicio */

    /**
     * Es el primer metodo que se ejecuta luego del constructor del BasePage, este carga el URL_MODE 
     */
    onInitPageBase() {
        //console.log('initMode');

    }
    createPagePropertiesBase() {
        let properties = null;
        if (this.createPageProperties)
            properties = this.createPageProperties(this.api);
        return properties || {};
    }
    /*
    onUrlMode(parent){
        console.log('base.onUrlMode');
    }

    onPageMode(parent){
        console.log('base.onPageMode');
    }

    initPageBase(){
        
        
        if (this.getUrlMode() === URL_MODE.NEW)
            this.onUrlModeNew(this.methodRef);
        else if (this.getUrlMode() === URL_MODE.UPDATE)
            this.onUrlModeUpdate(this.methodRef);
        else if (this.getUrlMode() === URL_MODE.READ)
            this.onUrlModeRead(this.methodRef);
    }
    
    onUrlModeUpdate = (parent) => {}
    onUrlModeNew = (parent) => {}
    onUrlModeRead = (parent) => {}*/


    /* METODOS QUE PUEDEN SER IMPLEMENTADOS POR LOS COMPONENTES QUE HEREDAN DE ESTE => COMPONENTES DEL TIPO PAGE */
    /**
     * Metodo que debe contener la renderizacion del componente hijo que sera llamado por el componente padre.
     * @param {object} parent - Es la referencia a los metodos que expone el componente padre.
     */
    renderPage(pageApi) {
        throw new Error('Debe implementar el metodo: renderPage');
    }
    createModal(pageApi) {
        let config = {};
        return (
            <ModalFw
                //name={`${this.getName()}.modal`}
                name={'modal'}
                getParentApi={pageApi.getApi}
                config={config}
            >
            </ModalFw>
        )
    }


}
