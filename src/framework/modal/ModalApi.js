import { ObjectUtil } from "../../util/ObjectUtil";

export class ModalApi{
    config = null;
    state = null; 
    setState = null;
    //componentModal = null;
    constructor(config, state, setState) {
        this.config = config;
        this.state = state;
        this.setState = setState;
    }
    show(modalConfig){
        this.config = ObjectUtil.merge(this.config, modalConfig);
        this.setState({visible:true});
    }
    hide(){
        this.setState({visible:false});
    }
}