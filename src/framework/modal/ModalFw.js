import Modal from 'antd/lib/modal/Modal';
import React from 'react'
import { ModalApi } from './ModalApi';
import './ModalFw.css';

const ModalFw = ({name, config, getParentApi}) => {
    
    const [state, setState] = React.useState({
        visible: false
    });
    const apiRef = React.useRef(new ModalApi(config, state, setState));
    const showModal = () => {
        setState({visible:true});
    };

    const handleOk = () => {
        setState({visible:false});
    };

    const handleCancel = () => {
        setState({visible:false});
    };

    const getApiRef = () => {
        return apiRef.current;
    }

    const getComponentModal = () => {
        if (!config.component) return null;
        return config.component;
    }
    
    React.useEffect(() => {
        if (getParentApi)
            getParentApi().addComponent(name, apiRef.current);
            console.log(`ModalFw[${name}].Mounting`);
        return () => {
            console.log(`ModalFw[${name}].Unmounting`);
        }
    }, []);
    console.log(`ModalFw[${name}].Render: {visible: ${state.visible}}`);
    if (!state.visible)
        return null
    return (
        <Modal 
            //title = "Basic Modal" 
            closable = {true}
            footer={null}
            //cancelButtonProps = {null}
           // okButtonProps = {{disabled:true}}
            width = {config.width}
            maskClosable = {config.maskClosable}
            visible={state.visible} 
            onOk={handleOk} 
            onCancel={handleCancel}>
            {getComponentModal()}
        </Modal>
    )
}
export default ModalFw;
