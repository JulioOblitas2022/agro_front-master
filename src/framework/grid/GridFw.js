import './GridFw.css';
import React from 'react';
import { Table } from 'antd';
import { createColumn } from './helpers/createColumn';
import { ICON } from '../../constants/iconConstants';
import { GridApi } from './GridApi';
import { EditableCell } from './editable/EditableCell';
import ContainerFw from '../container/ContainerFw';
import { EditableRow } from './editable/EditableRow';

export const COLUMN_DEFAULT = {
    VIEW: { name: 'view', type: 'button', icon: ICON.VIEW, title: 'Visualizar', color: 'black' },
    DELETE: { name: 'delete', type: 'button', icon: ICON.DELETE, title: 'Eliminar', color: 'black', confirm: { title: 'Eliminación', message: '¿Esta seguro que desea eliminar el registro?' } },
    EDIT: { name: 'edit', type: 'button', icon: ICON.EDIT, title: 'Editar', color: 'black' },
    REPORT: { name: 'report', type: 'button', icon: ICON.REPORT , color: 'black'},
    MAIL: { name: 'mail', type: 'button', icon: ICON.MAIL, color: 'black' },
    DROPBOX: { name: 'dropbox', type: 'button', icon: ICON.DROPBOX, color: 'black' },
    OPTIONS: { name: 'unorderedList', type: 'button', icon: ICON.OPTION, color: 'black' },
    PDF: { name: 'pdf', type: 'button', icon: ICON.PDF, color: 'black' },
    CHECK: { name: 'check', type: 'button', icon: ICON.CHECK, color: 'black' }
};

export const SELECTION_TYPE = {
    MULTIPLE: 'checkbox',
    SINGLE: 'radio'
}

const createGrid = (name, config, state, handleTableChange, getApi) => {
    const components = !getApi().isEditable() ? null : {
        body: {
            row: EditableRow,
            cell: EditableCell
        }
    }

    return (
        <Table
            scroll={{ x: 'max-content' }}
            responsive={true}
            size="small"
            className="table-head-agro"
            rowClassName={() => 'grid-fw-row'}
            tableLayout="fixed"
            components={components}
            columns={createColumn(config, getApi)}
            dataSource={state.dataList}
            hasData={getApi().getDataContainer().hasData()}
            loading={state.loading}
            pagination={
                {
                    ...state.pagination,
                    showSizeChanger: true //config.pagination?.showSizeChanger
                }
            }
            onChange={handleTableChange}
            //{config.scroll}//fixed//scroll
            locale={{ emptyText: 'No hay datos' }}
            onRow={(record, rowIndex) => {
                config['api'] = getApi;
                return {
                    record,
                    config,
                    onClick: event => {
                        //log.debug(`GridFw[${name}].rowClick[${record?.key}]`, event);
                    },
                    //onDoubleClick: event => { console.log('onDoubleClick'); }, // double click row
                    //onContextMenu: event => { console.log('onContextMenu'); }, // right button click row
                    //onMouseEnter: event => { console.log('onMouseEnter'); }, // mouse enter row
                    //onMouseLeave: event => { console.log('onMouseLeave'); }, // mouse leave row
                };
            }}
            onHeaderRow={(columns, index) => {
                return {
                    onClick: () => { }, // click header row
                };
            }}

            rowSelection={getApi().createRowSelection()}
        />
    );
}
const GridFw = ({ name, getParentApi, config }) => {
    //log.debug(`GridFw[${name}] => Iniciando`);
    const apiRef = React.useRef(null);
    if (!apiRef.current) {
        apiRef.current = new GridApi(name, null, config, null);
    }
    const [, forceRender] = React.useState(false);
    const getApi = () => apiRef.current;
    const { pagination } = config;
    const [state, setState] = React.useState({
        loading: false,
        pagination: !pagination ? false : {
            current: pagination.current,
            pageSize: pagination.pageSize,
            showSizeChanger: pagination.showSizeChanger
        }
    });
    getApi().state = state;
    getApi().setState = setState;
    getApi().forceRender = forceRender;
    const handleTableChange = (toPage, filter, sorter) => {
        if (pagination) {
            if (pagination.server) {
                getApi().load(filter, sorter, toPage);
            }
            else {
                setState({
                    ...state,
                    pagination: toPage
                });
            }
        }
    };

    React.useEffect(() => {
        //log.debug(`GridFw[${name}] => Mounting`);
        if (config.autoLoad)
            getApi().load();
        if (getParentApi)
            getParentApi().addComponent(name, getApi());
        return () => {
            //log.debug(`GridFw[${name}] => Unmounting`);
        }
    }, []);

    //debugger;
    return (
        <ContainerFw
            childName={name}
            getParentApi={getApi}
            config={config}
        >
            {createGrid(name, config, state, handleTableChange, getApi)}
        </ContainerFw>
    );
};
export default GridFw;