import './editable.css';
import React from 'react';
import log from 'loglevel';
export const EditableCell = ({
  record,
  children,
  config,
  ...restProps
}) => {
  const key = record?.key;
  const name = config?.name;
  const value = name ? record[name] : null;
  //log.debug(`GridFw.EditableCell[${key}] => Iniciando`);
  let cellJsx = children;

  //console.log(restProps);
  let visible = config?.fnVisible ? config.fnVisible(value, record) : true;
  //console.log('pruyeba => ',prueba);

  if (config?.editable && config?.getParentApi && visible) {
    const editing = config.getParentApi().isEditingItem(record);
    if (editing) {
      cellJsx = config.getParentApi().createFieldFormEditable(config, record);
    }
  }
  return (
    <td {...restProps}>
      {cellJsx}
    </td>
  );

};
