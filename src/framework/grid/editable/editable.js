import React from 'react'
import './editable.css';
import { createFormItem } from '../../form/helpers/createFields';
import FormFw from '../../form/FormFw';
import { initializeFormFieldConfig } from '../../form/helpers/initializeFormConfig';

const FormContext = React.createContext(null);
export const EditableRow = ({ index, ...props }) => {
  const formConfig = { forGrid: true, fields: [] };
  const apiRef = React.useRef(null);
  const getApi = () => {
    return {
      addComponent: (name, ref) => {
        apiRef.current = ref;
      }
    }
  }

  return (
    <FormFw
      config={formConfig}
      name={`formGrid_${index}`}
      getParentApi={getApi}
    >
      <FormContext.Provider value={apiRef}>
        <tr {...props} />
      </FormContext.Provider>
    </FormFw>
  );
};

const createCellEditableNoActive = (children, toggleEdit) => {
  return (
    <div
      className="editable-cell-value-wrap"
      onClick={toggleEdit}
    >
      {children}
    </div>
  );
}

export const EditableCell = ({
  record,
  config,
  editable,
  children,
  ...restProps
}) => {
  const [editing, setEditing] = React.useState(false);
  const inputRef = React.useRef(null);
  const formApi = React.useContext(FormContext);

  //console.log('formContext',formApi);

  React.useEffect(() => {
    if (!inputRef) {
      console.error(`GridFw:EditableCell:${config.type}:[${config.name}] =>  inputRef no referenciado`);
      return;
    } else if (editing) {
      inputRef.current.focus();
    }
  }, [editing]);

  const toggleEdit = () => {
    setEditing(!editing);
    formApi.current.setFieldData(config.name, record[config.name]);
  };


  const updateItem = async () => {
    try {
      const values = await formApi.current.validate();
      setEditing(false);
      config.getParentApi().updateItem({ ...record, ...values });
    } catch (errInfo) {
      console.log('Save failed:', errInfo);
    }
  };

  const onKeyDown = (e) => {
    if (e.code === "Tab") {//keyCode: 9
      console.log(e);
    }
  }

  config = {
    ...config,
    compact: true,
    ref: inputRef,
    onPressEnter: updateItem,
    onBlur: updateItem,
    onKeyDown: onKeyDown
  };

  let childNode = children;
  if (editable) {
    childNode = editing ? createFormItem(config) : createCellEditableNoActive(children, toggleEdit);
  }
  return <td {...restProps}>{childNode}</td>;
};

