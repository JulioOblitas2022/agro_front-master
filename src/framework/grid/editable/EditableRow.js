import React from "react";
import log from 'loglevel';

export const EditableRow = (props) => {
    //const [state, setState] = React.useState(false);
    const { config, record } = props;
    const name = config?.name;
    const key = record?.key;
    const getApi = config?.api;
    log.debug(`GridFw.EditableRow[${name}.${key}] => Iniciando`);
    React.useEffect(() => {
        log.debug(`GridFw.EditableRow[${name}.${key}] => Mounting`);
        return () => {
            log.debug(`GridFw.EditableRow[${name}.${key}] => Unmounting`);
        };
    });

    if (key && getApi && getApi().isEditable()) {
        return getApi().createFormEditable(props, key);//setState
    }
    return <tr {...props} />;
};