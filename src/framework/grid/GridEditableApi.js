import log from 'loglevel';
import { ObjectUtil } from '../../util/ObjectUtil';
import { ComponentWithFields } from "../base/ComponentWithFields";
import FormFw from "../form/FormFw";
import { createFormItem } from "../form/helpers/createFields";

export class GridEditableApi extends ComponentWithFields {
    itemEditing = null;

    constructor(name, grid, config, history) {
        super();
        this.name = name;
        this.config = config;
        this.history = history;
    }
    getFormEditable = () => this.getComponent(`${this.name}.FormEditable_${this.itemEditing?.key}`);
    getFormEditableByKey = (key) => this.getComponent(`${this.name}.FormEditable_${key}`);
    //getRowFnForceRender = (key) => this.mapRowFnForceRender[`${this.name}.FormEditable_${this.itemEditing?.key}`];
    createFieldFormEditable = (config, record) => {
        //importante tener el item actual
        const getApi = () => this.getFormEditableByKey(record?.key);
        let fieldConfig = getApi().getFieldConfig(config.name);
        fieldConfig.fnVisible = null;
        fieldConfig.onPressEnter = this.onPressEnterFieldFormEditable;
        fieldConfig.onBlur = this.onBlurFieldFormEditable;
        return createFormItem(fieldConfig);
    }

    createFormEditable = (props, key) => {//setState
        const { record } = props;
        const name = `${this.name}.FormEditable_${key}`;
        let fields = this.getCloneFieldsConfig();

        //quitamos la propiedad visible del form incrustado
        fields = fields.map((field, index) => {
            field.fnVisible = null;
            //permitimos la modificacion de componentes
            if (record && field.fnEditorConfig) {
                const value = field.name ? record[field.name] : null;
                let newConfig = field.fnEditorConfig(value, record);
                if (newConfig) {
                    field = ObjectUtil.merge(field, newConfig);
                }
            }
            return field;
        });



        //this.mapRowFnForceRender[name] = setState;
        const formConfig = {
            forGrid: true,
            fields: fields,
            dataOptions: {
                out: { includeLabel: true }
            }
        };
        //log.debug(`GridFw[${this.name}].createFormEditable[${name}]`);
        //console.log(`GridFw[${this.name}].createFormEditable[${name}]`);
        return (
            <FormFw
                config={formConfig}
                name={name}
                getParentApi={this.getApi}
            >
                <tr {...props} />
            </FormFw>
        );
    }

    isEditingItem = (item) => {
        //console.log(`Comparando: '${item?.key}' y '${this.itemEditing?.key}'`);
        return item?.key === this.itemEditing?.key;
    }
    setEditingItem = (item) => {
        const key = item?.key;
        this.itemEditing = item;
        //console.log(`setEditingItem: ${item?.key} => ${this.itemEditing?.key}`);
        const form = this.getFormEditableByKey(key);
        form.setData(item, true);
        //form.setFieldFocus(fieldConfig.name);
        //es un state que indica que se redibuje el grid
        this.refresh();
    }

    resetItemEditing = () => {
        this.itemEditing = null;
    }

    getItemEditing() {
        return this.itemEditing;
    }
    saveItemEditing = async () => {
        if (this.isEditable() && this.itemEditing) {
            try {
                await this.getFormEditable().validate();
                const newValues = this.getFormEditable().getData();
                const prevValues = this.getItemEditing();
                this.resetItemEditing();
                this.updateItemWithOutRefresh({ ...prevValues, ...newValues });
                return true;
            } catch (errInfo) {
                console.log('Save failed:', errInfo);
            }
        }
        return false;
    }
    onPressEnterFieldFormEditable = async () => {
        //console.log('onPressEnterFieldFormEditable');
        await this.saveItemEditing();
        this.refresh();
    }
    onBlurFieldFormEditable = () => {
        //console.log('onBlurFieldFormEditable');
        //this.saveItemEditing();
    }

}
