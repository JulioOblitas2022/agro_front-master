
export const createColumn = (config, getApi) => {

    return config.fields.map((field, index) => {
        let col = {
            key: field.name,
            dataIndex: field.name,
            colSpan: field.colSpan,
            title: field.label,
            width: field.width || 'auto',
            sorter: field.sorter,
            render: field.render,
            /*
            shouldCellUpdate: (record, prevRecord) => {
                console.log('shouldCellUpdate');
                return 1!=2;
            },*/
            onCell: (record, rowIndex) => ({
                record,
                config: field,
                onClick: async (event) => {
                    //console.log(`cellClick => rowIndex: ${rowIndex}, key: ${record?.key}, name: ${field?.name}`);
                    await getApi().onCellClick(record, field, rowIndex);

                    //if (getApi().isEditable()){
                    //getApi().selectItemEditingEvent(record, field);

                    //}
                },
                //onDoubleClick: event => {console.log('cellEvent');}, // double click row
                //onContextMenu: event => {}, // right button click row
                //onMouseEnter: event => {}, // mouse enter row
                //onMouseLeave: event => {}, // mouse leave row
            })
        };
        /* solo cuando una columna es editable */
        return col;
    });
}





