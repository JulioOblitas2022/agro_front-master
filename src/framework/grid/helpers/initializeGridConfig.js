import moment from 'moment'
import { ObjectUtil } from "../../../util/ObjectUtil";
import ButtonFw from "../../button/ButtonFw";
import { EnvConstants } from "../../../EnvConstants";
import { ProcessUtil } from "../../util/ProcessUtil";
import { StorageCacheUtil } from "../../util/StorageCacheUtil";

const configureButton = (config) => {
    config['width'] = config['width'] || 50;
    const fnRender = (value, row, index) => {
        let buttonConfig = ObjectUtil.merge({}, config);
        const link = config['link'];
        const onClick = config['onClick'];
        buttonConfig['link'] = typeof link === 'function' ? () => link(value, row, index) : (link ? link : null);//envolvemos la funcion para que no pierda la referencia a los valores entregados por la fila del grid
        buttonConfig['onClick'] = onClick && !link ? () => onClick(value, row, index) : null;//envolvemos la funcion para que no pierda la referencia a los valores entregados por la fila del grid
        buttonConfig['subType'] = 'icon';//para que muestre solo un icono
        const pro = config['process'];
        if (pro) {
            const filter = pro.filter;
            buttonConfig.process = {};
            buttonConfig.process.fnOk = pro.fnOk;
            buttonConfig.process.url = EnvConstants.GET_FW_GRID_COLUMN_URL_PROCESS_BASE();
            buttonConfig.process.filter = typeof filter === 'function' ? () => filter(value, row, index) : (filter ? filter : null);//envolvemos la funcion para que no pierda la referencia a los valores entregados por la fila del grid
        }
        return <ButtonFw config={buttonConfig} />
    }

    config['render'] = createColumnBaseRender(config, fnRender);
}


const createSelectColumnRender = (config) => {
    return (text, record, index) => {
        if (config.labelName && record && record[config.labelName]) {
            return {
                children: record[config.labelName],
                props: {},
            };
        }
        else if (text) {
            return {
                children: StorageCacheUtil.getLabel(config.load.queryId, text),
                props: {},
            };
        }
        return null;
    }
}

const valueFormatToMoment = (value, format) => {
    if (!value) return null;//si es nulo
    return moment(value, format);
}

const momentToValueFormat = (date, format) => {
    if (date) {
        return moment(date).format(format);
    }
    return null;
}

const createDateColumnRender = (config) => {
    return (text, record, index) => {
        if (text) {
            let moment = valueFormatToMoment(text, config.inputFormat);
            let result = moment ? momentToValueFormat(moment, config.format) : '';
            return {
                children: result,
                props: {},
            };
        }
        return null;
    }
}

const createDecimalColumnRender = (config) => {
    return (text, record, index) => {
        if (text) {
            return {
                children: parseFloat(text).toFixed(2) || '0.00',
                props: {},
            };
        }
        return null;
    }
}

const createColumnBaseRender = (config, fnRender) => {
    if (config.fnVisible) {
        return (value, row, index) => {
            if (config.fnVisible(value, row, index)) {
                if (!fnRender)
                    return { children: value, props: {} };
                return fnRender(value, row, index);
            }
            return <span></span>;
        };
    }
    return fnRender;
}

export const initializeGridFieldConfig = (config, gridConfig, getParentApi, index = 0) => {
    config['type'] = config['type'] || 'text';
    config['name'] = config['name'] || 'field_' + index;
    //fieldConfig['label'] = fieldConfig['label'] || fieldConfig['name'] || 'field_' + index;
    //fieldConfig['column'] = fieldConfig['column'] || 12;
    if (config['type'] === 'select' && config.load?.queryId) {
        config['render'] = createSelectColumnRender(config);
    }
    else if (config['type'] === 'date') {
        config['render'] = createDateColumnRender(config);
    }
    else if (config['type'] === 'decimal') {
        config['render'] = createDecimalColumnRender(config);
    }
    if (config['type'] === 'button') {
        configureButton(config);
    }
    else if (config.fnVisible) {
        config['render'] = createColumnBaseRender(config, config.fnRender);
    }
    else if (config.fnRender) {
        config['render'] = config.fnRender;
    }

    if (config['editable']) {//si tiene algun field como editable se marca todo el grid como editable
        gridConfig.editable = true;
    }
    /*
    config.shouldCellUpdate = (record, prevRecord) => {
        console.log('shouldCellUpdate',record);
        return false;
    }*/
    config.getParentApi = getParentApi;
}

export const initializeGridConfig = (name, config, getParentApi) => {
    /* si ya esta inicializado */
    if (config.initializedConfig) return config;
    config.name = name;
    let { pagination, fields, showIndex } = config;
    config['load'] = ProcessUtil.configureProcess(config['load'], EnvConstants.GET_FW_GRID_URL_LOAD_BASE());
    config['save'] = ProcessUtil.configureProcess(config['save'], EnvConstants.GET_FW_GRID_URL_SAVE_BASE());

    if (pagination) {
        config.pagination = {
            server: pagination.server,
            current: pagination.current || 1,
            pageSize: pagination.pageSize || 10,
            showSizeChanger: pagination.showSizeChanger || false
        }
    }
    if (showIndex) {
        fields.unshift({ name: 'ROW_ORDER', label: '#', width: 50 });
    }
    fields.forEach((fieldConfig, index) => {
        initializeGridFieldConfig(fieldConfig, config, getParentApi, index);
    });
    config.initializedConfig = true;
    return config;
}