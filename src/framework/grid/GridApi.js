import log from 'loglevel';
import { ObjectUtil } from "../../util/ObjectUtil";
import { ItemsContainer } from '../util/ItemsContainer';
import { ProcessUtil } from "../util/ProcessUtil";
import { GridEditableApi } from "./GridEditableApi";
import { initializeGridConfig } from "./helpers/initializeGridConfig";

export const ROW_STATE = {
    EXISTENT: 1,
    NEW: 2,
    MODIFIED: 3
};
export class GridApi extends GridEditableApi {
    type = 'GridApi';
    history = null;
    grid = null;
    state = null;
    setState = null;
    selectedItem = null;
    params = null;
    forceRender = null;
    dataContainer = null;

    constructor(name, grid, config, history, state, setState) {
        //log.debug(`GridApi[${name}].constructor`);
        super(name, grid, config, history);
        this.dataContainer = new ItemsContainer('ROW_STATE', this.getRowId());
        //this.name = name;
        //this.config = config;
        //this.history = history;
        initializeGridConfig(name, config, () => this);
        //this.state = state;
        //this.setState = setState;
    }

    getDataContainer = () => {
        return this.dataContainer;
    }
    getApi = () => {
        return this;
    }
    getRowId() {
        return this.config.rowId || 'ROW_ID';
    }
    getData() {
        return this.getDataContainer().getData();
    }
    setData(data, page) {
        const serverPagination = this.config?.pagination?.server;
        //creamos el objeto paginacion
        const pagination = !page ? false : {
            current: page.current,
            pageSize: page.pageSize,
            total: page.total
        };
        //agregamos el orden
        //const start =  (serverPagination && pagination) ? ((pagination.current - 1) * pagination.pageSize + 1) : 1 ; revisar bug al borrar elemento de la segunda o mas paginas
        const start = pagination ? ((pagination.current - 1) * pagination.pageSize + 1) : 1;

        this.getDataContainer().setData(data, start);
        this.setState({
            loading: false,
            pagination: pagination,
            dataList: this.getDataContainer().getData()
        });

    }



    reset() {
        this.getDataContainer().reset();
        this.setState(prevState => ({
            loading: false,
            pagination: prevState.pagination
        }));
    }

    load(reqParams, sorter, toPage) {
        const self = this;
        const { pagination, load } = this.config;
        reqParams = reqParams?.params ? reqParams : { params: self.params }
        self.params = reqParams.params;
        //debugger;
        if (!load)
            return;
        if (load.validation && !load.validation())
            return;//si no cumple la validacion no se carga

        //toPage = pagination ? (toPage || this.state.pagination) : null;
        //se coloca la pagina con un valor por defecto para que la busqueda desde el boton se haga desde la primera pagina
        toPage = pagination ? (toPage || { current: 1, pageSize: this.config.pagination.pageSize } || this.state.pagination) : null;
        reqParams = toPage && pagination.server ? ObjectUtil.merge(reqParams, this.pageToReqParams(toPage)) : reqParams;
        reqParams.orderBy = sorter ? this.sorterToOrderBy(sorter) : reqParams.orderBy;

        if (pagination) {
            if (pagination.server) {
                this.loadPaginated(reqParams, toPage);
            }
            else {
                this.loadBase(reqParams, toPage);
            }
        }
        else {
            //console.log('load', reqParams);
            this.loadBase(reqParams);
        }
    }

    loadBase(reqParams, toPage) {
        const self = this;
        let { load, name } = self.config;
        let { fnOk, fnError } = load;

        self.setState({
            loading: true
        });

        load.fnOkBase = resp => {
            self.setData(resp.dataList, toPage);
            if (fnOk)
                fnOk(resp, {});
        }
        load.fnErrorBase = error => {
            self.setData([]);
            //console.error(`Grid : loadBase '${name}' : Load : Error => ${error}`);
            if (fnError)
                fnError(error);
        }
        ProcessUtil.loadBase(load, reqParams);
    }

    loadPaginated(reqParams, toPage) {
        const self = this;
        let { load, name } = self.config;
        let { fnOk, fnError } = load;

        self.setState(prevState => ({
            loading: true,
            pagination: prevState.pagination
        }))

        load.fnOkBase = resp => {
            self.setData(resp.dataList, {
                current: toPage.current,
                pageSize: toPage.pageSize,
                total: resp.filtro.total
            });

            if (fnOk)
                fnOk(resp, {});
        }
        load.fnErrorBase = error => {
            self.setData([], {
                current: 1,
                pageSize: toPage.pageSize,
                total: 0
            });
            //console.error(`Grid : loadPaginated '${name}' : Load : Error => ${error}`);
        }
        ProcessUtil.loadBase(load, reqParams);
    }

    sorterToOrderBy(sorter) {
        if (sorter) {
            if (sorter.length) {
                return sorter
                    .filter(col => col.column)
                    .map(col => `${col.field} ${col.order === 'ascend' ? 'asc' : 'desc'}`).join();
            }
            else if (sorter.column) {
                return `${sorter.field} ${sorter.order === 'ascend' ? 'asc' : 'desc'}`
            }
        }
        return "1";
    }

    pageToReqParams(toPage) {
        return {
            isPaginated: true,
            startRow: (toPage.pageSize * (toPage.current - 1)),
            endRow: (toPage.pageSize * toPage.current)
        };
    }


    refresh = () => {
        //console.log('GridApi.refresh');
        this.setState(prevState => ({
            ...prevState,
            dataList: [...this.getDataContainer().getData()]
        }));
        this.forceRender(Math.random());
    }


    deleteItem(key) {
        let fnBefore = this.config.deleteItem?.fnBefore;
        let fnAfter = this.config.deleteItem?.fnAfter;

        let itemDelete = this.getDataContainer().getItemByKey(key);
        if (fnBefore)
            fnBefore(itemDelete);
        if (itemDelete) {
            this.getDataContainer().deleteItemByKey(key);
            this.refresh();
        }
        else {
            console.log(`intenta borrar fila que no existe => ${key}`);
        }
        if (fnAfter)
            fnAfter(itemDelete);
    }

    addItem = async (row, first = false) => {

        await this.saveItemEditing();

        let fnBefore = this.config.addItem?.fnBefore;
        let fnAfter = this.config.addItem?.fnAfter;
        row = fnBefore ? fnBefore(row) : row;

        if (Array.isArray(row))
            this.addItems(row, first, true);
        else {
            let index = first ? 0 : this.getDataContainer().getSize();
            this.getDataContainer().addItem(row, index);
            this.refresh();
        }
        if (fnAfter)
            fnAfter(row);
    }

    addItems = async (rows, first = false, fromAddItem = false) => {

        if (!fromAddItem)
            await this.saveItemEditing();

        let fnBefore = this.config.addItems?.fnBefore;
        let fnAfter = this.config.addItems?.fnAfter;
        rows = fnBefore ? fnBefore(rows) : rows;

        let index = first ? 0 : this.getDataContainer().getSize();
        this.getDataContainer().addItems(rows, index);
        this.refresh();

        if (fnAfter)
            fnAfter(rows);
    }

    updateItem = (row) => {
        let fnBefore = this.config.updateItem?.fnBefore;
        let fnAfter = this.config.updateItem?.fnAfter;
        row = fnBefore ? fnBefore(row) : row;

        this.getDataContainer().updateItem(row);
        this.refresh();

        if (fnAfter)
            fnAfter(row);
    }


    updateItemWithOutRefresh = (row) => {
        let fnBefore = this.config.updateItem?.fnBefore;
        let fnAfter = this.config.updateItem?.fnAfter;
        row = fnBefore ? fnBefore(row) : row;

        this.getDataContainer().updateItem(row);
        if (fnAfter)
            fnAfter(row);
    }


    getItemsDelete = () => {
        return this.getDataContainer().getItemsDelete();
    }

    getItemsNew = () => {
        return this.getDataContainer().getItemsNew();
    }

    getItemsUpdate = () => {
        return this.getDataContainer().getItemsUpdate();
    }

    createParamsForSave = (reqParams) => {
        const self = this;
        if (!self.config.save)
            return null;
        let { params } = self.config.save;
        if (!this.getDataContainer().hasChange()) {
            return null;
        }

        let itemsUpdate = self.getItemsUpdate();
        let itemsNew = self.getItemsNew();
        let itemsDelete = self.getItemsDelete();

        return {
            ...params,
            ...reqParams?.params,
            itemsUpdate,
            itemsNew,
            itemsDelete
        };

    }
    save = async (reqParams) => {
        const self = this;
        //si es que existem items en edicion
        await self.saveItemEditing();
        let params = self.createParamsForSave(reqParams);
        if (!params) {
            console.log('No existen cambios por guardar');
            return;
        }
        let { url, queryId, fnOk, fnError, postLink, message, errorMessage, fnMessageOk, fnMessageError, fnTransformReq, hideMessage, hideMessageError } = self.config.save;
        const fnOkBase = (resp) => {
            if (fnOk) {
                fnOk(resp, params);
            }
            self.load();
        }

        const fnErrorBase = (error) => {
            if (fnError) {
                fnError(error);
            }
            self.load();
        }
        const postLinkBase = !postLink ? null : (resp) => postLink(resp, params);
        message = message ? message : 'Se actualizo correctamente';
        errorMessage = errorMessage ? errorMessage : 'Existieron problemas al guardar el/los registros';
        let history = this.history;
        const processConfig = {
            url,
            queryId,
            params,
            fnOk: fnOkBase,
            fnError: fnErrorBase,
            message, errorMessage, fnMessageOk, fnMessageError, postLink: postLinkBase, history, fnTransformReq, hideMessage, hideMessageError
        };
        //console.log(processConfig);
        ProcessUtil.process(processConfig);
    }

    isEditable = () => this.config.editable;

    getSelectedItem = () => this.selectedItem;
    createRowSelection = () => {
        const self = this;
        const { selection } = self.config;
        if (!selection)
            return null;
        return {
            type: selection.type,
            /*
            renderCell : (checked, record, index, originNode) =>  {
                console.log('renderCell');
            },*/
            onChange: (selectedRowKeys, selectedRows) => {
                //console.log(`selectedRowKeys: ${selectedRowKeys}`, 'selectedRows: ', selectedRows);
                self.selectedItem = selectedRows;
            }/*,
            getCheckboxProps: (record) => ({
                disabled: record.name === 'Disabled User',
                // Column configuration not to be checked
                name: record.name,
            })*/
        }
    }

    onCellClick = async (record, field, rowIndex) => {
        //console.log(`GridApi.onCellClick => rowIndex: ${rowIndex}, key: ${record?.key}, name: ${field?.name}`);
        const key = record?.key;
        //console.log(`cellClick => rowIndex: ${rowIndex}, key: ${record?.key}, name: ${field?.name}, itemEditing: ${this.getItemEditing()}, getFormEditable: ${this.getFormEditable()}`);
        //console.log('itemEditing', this.getItemEditing());
        //si el grid no es editable salimos de la logica

        if (field.type == 'button' && field?.onClick) {
            return;
        }

        if (field.type !== 'button' && field?.onClick) {
            await field.onClick(record[field.name], record, rowIndex, field.name);
            //return;
        }

        if (!this.isEditable())
            return;
        if (!this.getDataContainer().existItemByKey(key))
            return;
        //debugger;
        if (this.getItemEditing() == null) {
            this.setEditingItem(record);
            //console.log(`cellClick => rowIndex: ${rowIndex}, key: ${record?.key}, name: ${field?.name}, itemEditing: ${this.getItemEditing()}, getFormEditable: ${this.getFormEditable()}`);
        }
        else if (!this.isEditingItem(record)) {
            //si el item seleccionado es distinto al que ya estaba en edicion lo seleccionamos como nuevo item en edicion
            //console.log(`cellClickForEdit => rowIndex: ${rowIndex}, key: ${record?.key}, name: ${field?.name}`);
            await this.saveItemEditing();//optimizar doble refresco
            this.setEditingItem(record);
        }
    }
}

