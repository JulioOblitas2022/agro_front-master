import { UserOutlined } from '@ant-design/icons';
import './UserBarFw.css';
import { Popover } from 'antd';
import Avatar from 'antd/lib/avatar/avatar';
import React from 'react';
import { StorageUtil } from '../../util/StorageUtil';
import { WindowUtil } from '../../util/WindowUtil';
import { UserUtil } from '../../util/UserUtil';
import {  Typography  } from 'antd';

const UserBarFw = () => {
    let config = null;
    //const text=(<div className="user-bar-menu-title">Cuenta</div>);
    //const content = "content"

    

    const createContent = () => {
        const onClick = () => {
            console.log('logout');
            StorageUtil.removeItemObject('AUTH');
            StorageUtil.removeItemObject('AUTH_USER');
            StorageUtil.removeItemObject('AUTH_MENU');
            WindowUtil.reload();
        };
        if (config.fields){
            return config.fields.map((field, index) => (
                <div key={index} className="user-bar-field" onClick={onClick} > <span  className={`user-bar-field-icon ${field.icon}`}></span>  {field.label} </div>
            ));
        }
        return "Nada";
    }

    if (StorageUtil.exist('USER_BAR')){
       config = StorageUtil.getItemObject('USER_BAR');
      

    }
    else
        return null;

    return (
        <div>
            <Popover className="user-bar" placement="bottomRight" title={config.title} content={createContent(config)} trigger="click">
            <Avatar  style={{
                                color: '#ffffff',
                                backgroundColor: '#f56a00',
                            }} >
                                {config.title.charAt(0)}
                    </Avatar>
            </Popover>
            
        </div>
    );
};
export default UserBarFw;