import HeaderBarFw from "../header-bar/HeaderBarFw";

export class HeaderUtil {

    static createHeader = (name, config, getApi, isCollapsed, setIsCollapsed) => (
        <HeaderBarFw
            name={name + '.headerBar'}
            config={{
                title: config.title,
                button: config.barHeader,
                collapsible: config.collapsible
            }}
            getParentApi={getApi}
            setIsCollapsed = {setIsCollapsed}
            isCollapsed = {isCollapsed}
        />
    )

    static hasBarHeader = (config) => config.title || config.barHeader;

    static createConditionalHeader = (name, config, getApi, isCollapsed, setIsCollapsed) => {
        if (HeaderUtil.hasBarHeader(config)) 
            return  HeaderUtil.createHeader(name, config, getApi, isCollapsed, setIsCollapsed);
        return null;
    }
}