import { QueryCache } from '../../modules/shared/constantes/QueryCache';
import { StorageUtil } from '../../util/StorageUtil';
export class StorageCacheUtil {
    static PREFIX_CACHE_ID = "FW_CACHE_";
    static PREFIX_CACHE_QUERY_ID = "FW_CACHE_QUERY_ID_";

    static getDataByQueryId(queryId){
        if (queryId < 0){
            let result = Object.values(QueryCache).filter(item=>item.queryId===queryId);
            result = result?.length > 0 ? result[0] : null;
            return result;
        }
        else{
            
        }
        let id = StorageCacheUtil.PREFIX_CACHE_QUERY_ID + queryId;
        if (StorageUtil.exist(id))
            return StorageUtil.getItemObject(id);
        return null;
    }

    static setDataByQueryId(queryId, data){
        let id = StorageCacheUtil.PREFIX_CACHE_QUERY_ID + queryId;
        StorageUtil.setItemObject(id, data);
    }

    static getItemData(queryId, value){
        const data = StorageCacheUtil.getDataByQueryId(queryId); 
        const result = data ? data.dataList.filter(item => item.value === value) : null;
        return result?.length > 0 ? result[0] : null;
    } 

    static getLabel(queryId, value){
        const item = StorageCacheUtil.getItemData(queryId, value); 
        return item ? item.label : null;
    } 
}
