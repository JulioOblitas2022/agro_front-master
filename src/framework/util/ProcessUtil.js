import { NotificationUtil } from "../../util/NotificationUtil";
import { ObjectUtil } from "../../util/ObjectUtil";
import { RequestUtil } from "../../util/RequestUtil";

export class ProcessUtil {
    static process = (config) => {
        let {url, queryId, params, reqParams, fnMessageOk, hideMessage, fnOk, fnTransform, postLink, history, fnTransformReq } = config;
        reqParams = reqParams || {};
        reqParams.params = ObjectUtil.merge((typeof params === 'function' ? params() : params), reqParams.params);
        reqParams.queryId = queryId;

        reqParams = fnTransformReq ? fnTransformReq(reqParams) : reqParams;
        RequestUtil.postData(url, reqParams)
        .then( resp => {
            if (resp.error){
                ProcessUtil.processError(config, reqParams, resp);
            }
            else{
                resp = fnTransform ? fnTransform(resp) : resp;
                //validamos si es funcion
                let message = ProcessUtil.processMessageOk(config, reqParams, resp); // (typeof message === 'function') ? message(resp, reqParams.params) : (resp.message || message);
                if (fnOk)
                    fnOk(resp, reqParams);
                if (fnMessageOk){
                    fnMessageOk(resp, reqParams.params);
                }
                else if (message && !hideMessage){
                    NotificationUtil.success(message);
                }
                if (postLink){
                    const url = postLink(resp);
                    history.push(url);
                }
            }
        })
        .catch(error=>{
            ProcessUtil.processError(config, reqParams, {error: 'Fallo algo en el servidor, revise la consola de error'});
        });
    }

    static processMessageError = (config, reqParams, resp) => {
        let { messageError } = config;
        return (typeof messageError === 'function') ? messageError(resp, reqParams.params) : (resp.error || messageError);
    }

    static processError = (config, reqParams, resp) => {
        let { fnError, fnMessageError, hideMessageError } = config;
        let messageError = ProcessUtil.processMessageError(config, reqParams, resp);
        if (fnError)/* Siempre que hay error se ejecuta */
            fnError(messageError);

        if (fnMessageError){/* Si existe se reemplaza la notificacion del error en pantalla */
            fnMessageError(resp, reqParams.params);
        }
        else if(messageError && !hideMessageError){/* se muestra el error */
            NotificationUtil.error(messageError);
        }
    }




    static processMessageOk = (config, reqParams, resp) => {
        let {message } = config;
        return (typeof message === 'function') ? message(resp, reqParams.params) : (resp.message || message);
    }

    static configureProcess = (process, urlBase) => {
        if (!process) return null;
        if (process.dataList) return process;/* Si hay data en no se usara el load */
        process.url = process.url || urlBase;
        return process;
    }

    static loadBase = (loadConfig, reqParams) => {   
        let {orderBy, fnOkBase, fnErrorBase} = loadConfig;
        reqParams = reqParams || {};
        reqParams.orderBy = reqParams.orderBy || orderBy;
        ProcessUtil.process({...loadConfig, reqParams, fnOk:fnOkBase, fnError:fnErrorBase});
    }
}