import log from 'loglevel';

export const ROW_STATE = {
    EXISTENT: 1,
    NEW: 2,
    MODIFIED: 3
};

export class ItemsContainer {
    ROW_STATE = 'ROW_STATE';
    ROW_ID = 'ROW_ID';
    ROW_ORDER = 'ROW_ORDER';
    items = null;
    itemsDelete = null;
    keyBase = 0;

    constructor(rowStateField, rowIdField) {
        console.log(`ItemsContainer.constructor`);
        this.ROW_STATE = rowStateField;
        this.ROW_ID = rowIdField;
        this.items = [];
        this.itemsDelete = [];
    }

    generateKey = () => {
        this.keyBase++;
        return this.keyBase;
    }

    reset = () => {
        console.log(`ItemsContainer.reset`);
        this.items = [];
        this.itemsDelete = [];
        this.keyBase = 0;
    }

    hasData = () => {
        return this.items.length > 0;
    }

    getData = () => {
        return this.items;
    }

    setData = (data, startOrder) => {
        startOrder = startOrder || 1;
        console.log(`ItemsContainer.setData => dataSize: ${data?.length || 0}, startOrder: ${startOrder}`);
        this.keyBase = 0;
        this.itemsDelete = [];
        this.items = data.map((row, index) => {
            return this.initItem(row, startOrder + index);
        });
    }

    getSize = () => {
        return this.items.length;
    }

    getItemByKey = (key) => {
        let result = this.items.filter(item => item.key === key);
        return result && result.length > 0 ? result[0] : null;
    }

    existItemByKey = (key) => {
        return this.items.some(item => item.key == key);
    }

    getIndexByKey = (key) => {
        return this.items.findIndex(item => item.key === key);
    }

    deleteItemByKey = (key) => {
        let itemDelete = this.getItemByKey(key);
        console.log(`ItemsContainer.deleteItemByKey => itemKey: ${key}`, itemDelete);
        //si el registro es distinto de nuevo lo agregamos en la lista de elementos por eliminar
        if (itemDelete && (itemDelete[this.ROW_STATE] != ROW_STATE.NEW)) {
            this.itemsDelete.push(itemDelete[this.ROW_ID]);
        }
        if (itemDelete) {
            let index = this.getIndexByKey(key);
            this.items.splice(index, 1); //removemos el elemento sin cambiar el array
            this.updateOrder();
        }

    }

    addItem = (item, index) => {
        console.log(`ItemsContainer.addItem => startIndex: ${index}`, item);
        index = index || 0;
        this.items.splice(index, 0, this.initItem(item));
        this.updateOrder();
    }

    addItems = (items, index) => {
        console.log(`ItemsContainer.addItems => dataSize: ${items.length}, startIndex: ${index}`, items);
        index = index || 0;
        items = items.map((item, index) => {
            return this.initItem(item);
        });
        this.items.splice(index, 0, ...items);
        this.updateOrder();
    }

    updateItem = (item) => {
        console.log(`ItemsContainer.updateItem => itemKey: ${item.key}`, item);
        const index = this.getIndexByKey(item.key);
        if (item[this.ROW_STATE] === ROW_STATE.EXISTENT)//solo se marca si es existente, en otro caso no
            item[this.ROW_STATE] = ROW_STATE.MODIFIED;//momentaneamente marcamos todos
        this.items[index] = item;
        this.updateOrder();
    }

    initItem = (item, order) => {
        return {
            ...item,
            key: this.generateKey(),
            [this.ROW_STATE]: (item[this.ROW_ID] ? ROW_STATE.EXISTENT : ROW_STATE.NEW),
            [this.ROW_ORDER]: order
        };
    }

    updateOrder = () => {
        this.items.forEach((item, index) => {
            item[this.ROW_ORDER] = index + 1;
        });
    };

    getItemsDelete = () => {
        return this.itemsDelete;
    }
    getItemsNew = () => {
        return this.items.filter(item => item[this.ROW_STATE] === ROW_STATE.NEW);
    }
    getItemsUpdate = () => {
        return this.items.filter(item => item[this.ROW_STATE] === ROW_STATE.MODIFIED);
    }
    hasChange = () => {
        if (this.itemsDelete.length > 0)
            return true;
        if (this.items.some(item => item[this.ROW_STATE] === ROW_STATE.MODIFIED))
            return true;
        if (this.items.some(item => item[this.ROW_STATE] === ROW_STATE.NEW))
            return true;
        return false;
    }

}