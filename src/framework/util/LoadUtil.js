import { ObjectUtil } from "../../util/ObjectUtil";
import { RequestUtil } from "../../util/RequestUtil";
import { StorageCacheUtil } from "./StorageCacheUtil";

export class LoadUtil {

    static loadByQueryId = (config) => {  
        const {url, queryId, fnOk, fnError, fnTransform, cache} = config;
        //Soporte cache
        if (cache || queryId < 0){
            let resp = StorageCacheUtil.getDataByQueryId(queryId);
            if (resp){
                resp = fnTransform ? fnTransform(resp) : resp;
                //console.log(resp);
                setTimeout(function(){ 
                    if (fnOk)
                        fnOk(resp);
                 }, 500);
                
            }
        }
        else{
            const reqParams = LoadUtil.getReqParams(config);
            RequestUtil.postData(url, reqParams)
            .then( resp => {
                if (cache) StorageCacheUtil.setDataByQueryId(queryId, resp);
                resp = fnTransform ? fnTransform(resp) : resp;
                if (fnOk)
                    fnOk(resp)
            })
            .catch(error=>{
                if (fnError)
                    fnError(error)
            });
        }
    }
    static load = (api, config, reqParams) => {
        let { load, name } = config;
        let loadConfig = {...load};
        const {fnOk, fnError} = loadConfig;
        loadConfig.reqParams = reqParams;

        api.setLoading(true);
        api.setDataList(null);
        api.render();

        loadConfig.fnOk = resp => {
            api.setDataList(resp.dataList);
            api.setLoading(false);
            api.render();
            if (fnOk)
                fnOk(resp);
        }
        loadConfig.fnError = error => {
            api.setLoading(true);
            api.setDataList(null);
            api.render();
            if (fnError)
                fnError(error);
            console.error(`${name} : LoadUtil.load : Error => ${error}`); 
        }
        LoadUtil.loadByQueryId(loadConfig);
    }















    static loadStateByQueryId = (api, config, nameDesc) => {   
        const {fnOk, fnError} = config;
        LoadUtil.setLoadingState(api, true);
        config.fnOk = resp => {
            LoadUtil.setResponseState(api, resp);
            if (fnOk)
                fnOk(resp);
        }
        config.fnError = error => {
            LoadUtil.setLoadingState(api, false);
            if (fnError)
                fnError(error);
            console.error(`${nameDesc} : loadStateByQueryId : Error => ${error}`); 
        }
        LoadUtil.loadByQueryId(config);
    }
    static loadStandard = (api, config, reqParams) => {
        let { load, name } = config;
        let loadConfig = {...load};
        loadConfig.reqParams = reqParams;
        LoadUtil.loadStateByQueryId(api, loadConfig, `Form : Select[${name}]`);
    }
    static setLoadingState(api, loading){
        api.setState({
            ...api.getState(),
            loading: loading,
            resp: null,
            dataList: [],
            dataObject: null
       });
    }
    static setResponseState(api, resp){
        api.setState({ 
            ...api.getState(),
            loading: false,
            dataObject: resp.dataObject,
            dataList: resp.dataList,
            resp: resp
        });
    }
    static getReqParams(config){
        let {reqParams, queryId, params, orderBy} = config;
        reqParams = reqParams || {};
        reqParams.params = ObjectUtil.merge(reqParams.params, ObjectUtil.getObjFromFuncOrObj(params));
        reqParams.queryId = queryId;
        reqParams.orderBy = orderBy;
        return reqParams;
    }
}