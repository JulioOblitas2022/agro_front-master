export class ModeUtil {
    static includeFieldsByNames = (fields, names) => fields.filter(field => names.includes(field.name));
    static excludeFieldsByNames = (fields, names) => fields.filter(field => !names.includes(field.name));
    static markFieldsReadOnlyByNames = (fields, names) => {
        fields.forEach(field => {
            if (field['readOnly']===undefined)
                field['readOnly'] = names.includes(field.name);
        });
        return fields;
    };
    static markAllFieldsReadOnly = (fields) => {
        fields.forEach(field => {
            if (field['readOnly']===undefined)
                field['readOnly'] = true;
        });
        return fields;
    };
    static notMarkFieldsReadOnlyByNames = (fields, names) => {
        fields.forEach(field => {
            if (field['readOnly']===undefined)
                field['readOnly'] = !names.includes(field.name);
        });
        return fields;
    };
    static markFieldsVisibleByNames = (fields, names) => {
        fields.forEach(field => {
            if (names.includes(field.name))
                field['visible'] = true;
        });
        return fields;
    };
    static markFieldsNotVisibleByNames = (fields, names) => {
        fields.forEach((field, index) => {
            if (names.includes(field.name)){
                field.visible = !field.visible;
            }
        });
        return fields;
    };
}