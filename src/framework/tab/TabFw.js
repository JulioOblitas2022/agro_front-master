import './TabFw.css';
import { Tabs } from 'antd';
import React from 'react';

const TabFw = ({name, getParentApi, config}) => {

    function callback(key) {
      console.log(key);
    }

    return (
        <div
            className="tab-fw"
        >
            <Tabs defaultActiveKey="1" onChange={callback}>
                <Tabs.TabPane tab="Tab 1" key="1">
                Content of Tab Pane 1
                </Tabs.TabPane>
                <Tabs.TabPane tab="Tab 2" key="2">
                Content of Tab Pane 2
                </Tabs.TabPane>
                <Tabs.TabPane tab="Tab 3" key="3">
                Content of Tab Pane 3
                </Tabs.TabPane>
            </Tabs>
        </div>
    );
};
export default TabFw;