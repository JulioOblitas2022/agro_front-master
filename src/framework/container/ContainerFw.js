import './ContainerFw.css';
import React from 'react'
import BarButtonFw from '../bar-button/BarButtonFw';
import { HeaderUtil } from '../util/HeaderUtil';

const createBar = (name, config, getParentApi) => (
    <BarButtonFw  
        name={name + '.bar'}
        getParentApi={getParentApi}
        config={config.bar}
    />
)


const ContainerFw = ({childName, getParentApi, config, children}) => {
    const [isCollapsed, setIsCollapsed] = React.useState(false);
    const hasBar = () => config.bar;
    return (
        <div className={`container-fw ${HeaderUtil.hasBarHeader(config) ? null : 'container-fw-without-header'}`} >
            {HeaderUtil.createConditionalHeader(childName, config, getParentApi, isCollapsed, setIsCollapsed)}
            <div className={`container-fw-body ${isCollapsed ? 'collapsed' : 'expanded'}`}>
                {children}
                {!hasBar() || createBar(childName, config, getParentApi)}
            </div>
        </div>
    )
}
export default ContainerFw;

