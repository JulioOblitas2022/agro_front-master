import { ComponentWithFields } from "./ComponentWithFields";

export class ComponentWithFieldsApi extends ComponentWithFields {
    mapFieldApi = {};
    constructor() {
        super();
    }
    getMapFieldApi = () => this.mapFieldApi;
    getFieldApi = (name) => {
        return this.mapFieldApi[name];
    }
    addFieldApi = (name, fieldApi) => {
        this.mapFieldApi[name] = fieldApi;
    }
    removeFieldApi = (name) => {
        this.mapFieldApi[name] = null;
    }
}

