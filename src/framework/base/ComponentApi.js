export class ComponentApi {
    type = null;
    config = null;
    name = null;
    getParentApi = null;
    mapComponentApi = {};//manejo de componentes hijos
    getConfig = () => this.config;
    /* inicio metodos de componentes */
    addComponent = (name, componentApi) => {
        if (name && componentApi) {
            //console.log(`GridApi[${this.name}].addComponent(${name})`, componentApi);
            this.mapComponentApi[name] = componentApi;
        }
        else {
            //console.error(`${this.type}[${this.name}].addComponent(${name}) => agregando componente nulo`);
        }
    }
    getComponent = (name) => {
        return this.mapComponentApi[name];
    }
    /* fin metodos de componentes */
}