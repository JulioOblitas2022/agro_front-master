import { ComponentApi } from "./ComponentApi";

export class ComponentWithFields extends ComponentApi {
    constructor() {
        super();
    }

    getFieldsConfig = () => {
        if (this.config.hasGroup){
            let result = [];
            this.config.fields.forEach(groupConfig => {
                result.push(...groupConfig.fields);
            });
            return result;
        }
        else
            return this.config.fields;
    }
    getCloneFieldsConfig = () => {
        let array = this.getFieldsConfig();
        return array.map((field, index) => ({...field}));
    }
    getGroupsConfig = () => this.config.hasGroup ? this.config.fields : [];
    getFieldConfig = (name) => this.getFieldsConfig().filter(field => field.name === name)[0];
    getGroupConfig = (name) => this.getGroupsConfig().filter(field => field.name === name)[0];
}

//let fields = this.config.fields.map((field, index) => ({...field}));