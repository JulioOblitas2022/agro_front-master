export const initializeConfig = (config, handleAddField) =>{

    config.fields.forEach((fieldConfig, index) =>{
        if (fieldConfig['visible'] === undefined)
            fieldConfig['visible'] = true;
        fieldConfig['handleAddField'] = handleAddField;
    });
    return config;
}