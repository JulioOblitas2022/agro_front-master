export const initializeBarButtonConfig = (config, handleAddField) =>{

    config.fields.forEach((fieldConfig, index) =>{

        if (fieldConfig['visible'] === undefined)
            fieldConfig['visible'] = true;
        fieldConfig['handleAddField'] = handleAddField;
        fieldConfig['name'] = fieldConfig['name'] || ('buttonFw_' + index);
        fieldConfig['type'] = fieldConfig['type'] || 'default';
    });
    return config;
}