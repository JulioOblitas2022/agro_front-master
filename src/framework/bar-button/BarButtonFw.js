import './BarButtonFw.css';
import React from 'react'
import { Col, Row } from 'antd';
import { ICON } from '../../constants/iconConstants';
import ButtonFw from '../button/ButtonFw';
import { BarButtonApi } from './BarButtonApi';
import { initializeBarButtonConfig } from './helpers/initializeBarButtonConfig';
import { barButtonModeProcess } from './helpers/barButtonModeProcess';

export const BUTTON_DEFAULT = {
    SEARCH  :{name:'search', label: 'Buscar', type: 'primary', icon: ICON.SEARCH}, 
    CLEAR   :{name:'clear', label: 'Limpiar', type: 'danger', icon: ICON.CLEAR}, 
    CLEAR1  :{name:'clear', label: 'Limpiar', type: 'danger', icon: ICON.CLEAR}, 
    SAVE    :{name:'save', label: 'Guardar', type: 'success', icon: ICON.SAVE},
    UPDATE  :{name:'update', label: 'Modificar', type: 'success', icon: ICON.UPDATE},
    RETURN  :{name:'return', label: 'Regresar', type: 'danger', icon: ICON.RETURN},
    NEW     :{name:'new', label: 'Nuevo', type: 'success', icon: ICON.NEW},
    REPORT  :{name:'report', label: 'Report', type: 'danger', icon: ICON.REPORT},
    USER    :{name:'user', label: 'User', type: 'danger', icon: ICON.USER},
    EXECUTE :{name:'execute', label: 'Ejecutar', type: 'danger', icon: ICON.PLAY},
    CANCEL  :{name:'cancel', label: 'Cancelar', type: 'danger', icon: ICON.CANCEL},
    ADD     :{name:'add', label: 'Agregar', type: 'success', icon: ICON.PLUS},
    SETTING :{name:'add', label: 'Configurar Vencimiento', type: 'primary', icon: ICON.SETTING}
};

export const BUTTON_DANGER = {
    USER        :{name:'user', label: 'User', type: 'danger', icon: ICON.USER},
    CLOSE       :{name:'close', type: 'danger', icon: ICON.CLOSE},
    ARROWLEFT   :{name:'arrowLeft', label: 'Cerrar', type: 'danger', icon: ICON.ARROWLEFT},
    SAVE        :{name:'save', label: 'Guardar', type: 'danger', icon: ICON.SAVE},
    RETURN      :{name:'return', label: 'Regresar', type: 'danger', icon: ICON.RETURN},

}

export const BUTTON_PRIMARY = {
    DOWNLOAD  :{name:'download', label: 'Generar Reporte', type: 'primary', icon: ICON.DOWNLOAD},
    USERADD   :{name:'user', label: 'User', type: 'primary', icon: ICON.USERADD},
    SAVE      :{name:'save', label: 'Guardar', type: 'primary', icon: ICON.SAVE},
    EDIT      :{name:'edit', label: 'Editar', type: 'primary', icon: ICON.EDIT},
    SEARCH    :{name:'search', label: 'Buscar', type: 'primary', icon: ICON.SEARCH},
    UPDATE    :{name:'update', label: 'Modificar', type: 'primary', icon: ICON.UPDATE},

}


const BarButtonFw = ({name, getParentApi, handleAddComponent, config}) => {
    const apiRef = React.useRef(new BarButtonApi(null, config, null));
    if (!config || !config.fields){
        console.error(`BarButtonFw [${name}]  => config o fields son nulos`);
    }
    const handleAddField = (name, api) => {
        apiRef.current.addFieldApi(name, api);
    };
    if (handleAddComponent) console.error('Metodo [BarButtonFw.handleAddComponent] deprecado, actualize segun ejemplo => [PersonaDetailPage.getParentApi]');
    initializeBarButtonConfig(config, handleAddField);

    let { mode } = config;
    if (getParentApi  && getParentApi().getModeComponentConfig){
        mode = getParentApi().getModeComponentConfig(name);
    }

    if (mode){
        config.fields = barButtonModeProcess(config.fields, mode);
    }

    const createFields = (config) =>{
        return config.fields.map((fieldConfig, index) => (
                <Col key={index.toString()}>
                    <ButtonFw
                        name={fieldConfig.name}
                        config={fieldConfig}
                    />
                </Col>
            )
        )
    }

    React.useEffect(() => {
        //console.log(`Mounting => BarButtonFw[${name}]`);
        if (getParentApi)
            getParentApi().addComponent(name, apiRef.current);
        return () => {
            //console.log(`Unmounting => BarButtonFw[${name}]`);
        }
    }, []);

    //console.log(`Render => BarButtonFw[${name}]`);
    return (
        <div className="bar-button-fw">
            <Row align="middle" justify="center" gutter={[10, 0]} >
                {createFields(config)}
            </Row>
        </div>
    )
}

export default BarButtonFw;

