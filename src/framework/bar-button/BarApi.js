
export class BarApi {
    history = null;
    bar = null;
    config = null;
    state = null;
    setState = null;
    mapFieldApi = null;
    //fieldsApi = null;
    constructor(bar, config, history, state, setState) {
        this.bar = bar;
        this.config = config;
        this.history = history;
        this.state = state;
        this.setState = setState;
    }
    setMapFieldApi(mapFieldApi){
        this.mapFieldApi = mapFieldApi;
    }
    getFieldApi(name){
        return this.mapFieldApi[name];
    }
    show(names){
        names.forEach((name, index) => {
            this.mapFieldApi[name].show();
        });
    }
    hide(names){
        names.forEach((name, index) => {
            this.mapFieldApi[name].hide();
        });
    }
    getConfig(){
        return this.config;
    }
}

