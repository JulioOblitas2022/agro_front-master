import { ComponentWithFieldsApi } from "../base/ComponentWithFieldsApi";

export class BarButtonApi extends ComponentWithFieldsApi {
    history = null;
    bar = null;
    state = null;
    setState = null;
    constructor(bar, config, history, state, setState) {
        super();
        this.bar = bar;
        this.config = config;
        this.history = history;
        this.state = state;
        this.setState = setState;
    }
    show(names){
        names.forEach((name, index) => {
            this.mapFieldApi[name].show();
        });
    }
    hide(names){
        names.forEach((name, index) => {
            this.mapFieldApi[name].hide();
        });
    }
}

