import './ContainerLayout.css';
import { Content } from 'antd/lib/layout/layout';
import React from 'react';
import { ContainerRoutes } from '../../routers/ContainerRoutes';
import { LoadingPage } from '../loading/LoadingPage';


const ContainerLayout = () => {
    return (
        <Content className="container-layout" >
          <ContainerRoutes/>
          <LoadingPage id="loader"/>
        </Content>
    );
};

export default ContainerLayout;