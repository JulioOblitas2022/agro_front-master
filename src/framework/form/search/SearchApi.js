import { LoadUtil } from "../../util/LoadUtil";

export class SearchApi {
    config = null;
    stateRef = null;
    valueStateRef = null;

    constructor(config, stateRef, valueStateRef) {
        this.config = config;
        this.stateRef = stateRef;
        this.valueStateRef = valueStateRef;
    }
    //metodo para manejo de estados
    setState = (state) => this.stateRef[1](state);
    getState = () => this.stateRef[0];
    setValueState = (state) => this.valueStateRef[1](state);
    getValueState = () => this.valueStateRef[0];

    load(reqParams){   
        LoadUtil.loadStandard(this, this.config, reqParams);
    }
}

