import './SearchFw.css';
import { Col, Row } from 'antd';
import Search from 'antd/lib/input/Search';
import React from 'react'
import { SearchApi } from './SearchApi';

const createResult = (config, state) => {
    if (!config.fnRender)
        return null;
    const fnRender = () => {
        return config.fnRender && state.resp ? config.fnRender(state.resp) : null;
    }
    const result = fnRender();
    if (!result)
        return null;
    return (
            <Col>
                <span className="search-result">{result}</span>
            </Col>
        );
}

const SearchFw = ({value = '', onChange, config}) => {
    const stateRef = React.useState({
        resp: null,
        loading: false
    });
    const apiRef = React.useRef(new SearchApi(config, stateRef));
    const [state, setState]  = stateRef;
    //const [valueState, setValueState]  = valueStateRef;

    const onSearch = (val) => {
        apiRef.current.load({params:{value:val}});
    };

    const handleChange = (value) => {
        if (onChange) {
            onChange(value)
        }
    }

    React.useEffect(() => {
        if (config.getParentApi){
            config.getParentApi().addFieldApi(config.name, apiRef.current);
        } 
        return () => {

        }
    }, []);

    return (
        <Row className="search-row" >
            <Col>
                <Search 
                    value={value}
                    onChange={handleChange}
                    disabled={config.readOnly}
                    onSearch={onSearch} 
                    loading={state.loading}
                />
            </Col>
            {createResult(config, state)}
        </Row>
    )
}
export default SearchFw;

