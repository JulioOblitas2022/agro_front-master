import React from 'react'
import { Col, Radio, Row } from 'antd';
import { RadioApi } from './RadioApi';

/*
const arraySubarray = (array, size) => {
    if (!array) return null;
    let result = [];
    let bloques = Math.floor(array.length/size);
    let residuo = array.length % size;
    for(let i=0; i<bloques; i++){
        result.push(array.slice(i*size, (i+1)*size));
    }
    if (residuo>0)
        result.push(array.slice(bloques*size, bloques*size + residuo));
    return result;
}

const createRow = (config, items) => {
    return items.map((item, index) => {
        return (
            <Col span={8} key={index.toString()}>
                <Radio value={item.value}>{item.label}</Radio>
            </Col>
        );
    });
}

const createOptions = (config, items) => {
    if (!items) return null;
    let result = [];
    let subArrays = arraySubarray(items, 3);
    subArrays.forEach(sub => {
        result.push(
            <Row>
                {createRow(config, sub)}        
            </Row>
        );
    });
    return result;
    
}
*/

const RadioFw = ({value, onChange, config, ...restProps}) => {
    const [,forceRender] = React.useState(true);
    const apiRef = React.useRef(new RadioApi(config));
    //if (!apiRef.current) apiRef.current = new RadioApi(config);//inicializamos
    const getApi = () => apiRef.current;
    //getApi().setInnerValue(value);
    getApi().forceRender = forceRender;
    getApi().onChange = onChange;

    const handleChange = (value, option) => {
        //getApi().setInnerValue(value);
        if (onChange) {
            onChange(value);
        }
    }
    React.useEffect(() => {
        getApi().firstMount();
        //console.log('SelectFw Primer Montado');
        return () => {
            getApi().unMount();
            apiRef.current = null;
            //console.log(`SelectFw[${config.name}].desmontado`);
        }
    }, []);
    return (
        <Radio.Group
            value={value}
            onChange={handleChange}
            options={getApi().getOptions()}
            disabled={config.disabled || false}
        >
            
        </Radio.Group>
    )
}
export default RadioFw;

/*
{createOptions(config, getApi().getOptions())}
            <Radio value="small">Small</Radio>
            <Radio value="default">Default</Radio>
            <Radio value="large">Large</Radio>
*/