import { LoadUtil } from "../../util/LoadUtil";
import { ListFieldApi } from "../field/ListFieldApi";

export class RadioApi extends ListFieldApi {
    forceRender = null;
    constructor(config) {
        super(config);
    }
    firstMount = () => {
        //console.log(`SelectApi[${this.config.name}].firstMount`);
        if (this.getParentApi && this.getParentApi().addFieldApi){
            this.getParentApi().addFieldApi(this.config.name, this);
        }
        if (this.config.load?.dataList){
            this.setDataList([...this.config.load.dataList]);
            this.render();
        }
        else if (this.config.load){
            this.load();
        }
    }
    load = (reqParams) => {  
        LoadUtil.load(this, this.config, reqParams);
    }
    render = () => this.forceRender(prevState => (!prevState));
}