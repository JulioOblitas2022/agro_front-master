import './DecimalFw.css';
import React from 'react';
import { InputNumber } from 'antd';
import { FieldUtil } from '../helpers/FieldUtil';

const DecimalFw = ({value, onChange, config, ...restProps}) => {
    const fnRender = () => {
        let parentData = config.getParentApi ? config.getParentApi().getData() : null;
        return config.fnRender ? config.fnRender(value, parentData) : null;
    }
    let result = fnRender();
    if (result)
        return result;

    const toDecimal = number => parseFloat(number).toFixed(2);
    if(config.readOnly === true){
        return (
            <InputNumber
                ref={FieldUtil.createFieldRef(config)} 
                min={config?.min || 0}
                //defaultValue={0}
                className="disable-fw"
                readOnly={true}
                value={isNaN(toDecimal(value)) ? (isNaN(toDecimal(config.value)) ? 0 : toDecimal(config.value)) : toDecimal(value)}
                onChange={onChange}
                onPressEnter={config.onPressEnter} 
                onBlur={config.onBlur}
                onKeyDown={config.onKeyDown} 
                style={{width:'100%', color: 'black'}}
                precision={2}
                formatter={value => `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, '')}
                parser={value => value.replace(/\$\s?|(,*)/g, '')}
                stringMode
            />
        )
    }
    else{
        return (
            <InputNumber
                ref={FieldUtil.createFieldRef(config)} 
                min={config?.min || 0}
                //defaultValue={0}
                readOnly={false}
                value={isNaN(toDecimal(value)) ? (isNaN(toDecimal(config.value)) ? 0 : toDecimal(config.value)) : toDecimal(value)}
                onChange={onChange}
                onPressEnter={config.onPressEnter} 
                onBlur={config.onBlur}
                onKeyDown={config.onKeyDown} 
                style={{width:'100%', color: 'black'}}
                precision={2}
                formatter={value => `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, '')}
                parser={value => value.replace(/\$\s?|(,*)/g, '')}
                stringMode
            />
        )
    }
}
export default DecimalFw;