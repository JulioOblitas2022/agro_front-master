import React from 'react';
import { Switch } from 'antd';
import { CloseOutlined, CheckOutlined } from '@ant-design/icons';

const SwitchFw = ({value, onChange, config, ...restProps}) => {

    const handleChange = (value) => {
        if (onChange) {
            onChange(value);
        }
    }

    const checked = config.checked || <CheckOutlined />;
    const unChecked = config.checked || <CloseOutlined />;
    return (
        <Switch
            checked={value}
            onChange={handleChange}
            checkedChildren={checked}
            unCheckedChildren={unChecked}
        />
    )
}
export default SwitchFw;