import React from 'react';
import moment from 'moment';
import esES from "antd/es/locale/es_ES";
import locale from 'antd/es/date-picker/locale/es_ES';
import { ConfigProvider, DatePicker, TimePicker } from 'antd';
import { DATE_FORMAT } from '../../../constants/constants';

const valueFormatToMoment = (value, format) => {
    if (!value) return null;//si es nulo
    return moment(value, format);
}

const momentToValueFormat = (date, format) => {
    if (date){
        return moment(date).format(format);
    } 
    return null;
}

const DateFw = ({value = null, onChange, config}) => {
    let val = valueFormatToMoment(value, config.inputFormat);
    let timeFormat = null;

    if (config.format.lastIndexOf(DATE_FORMAT.HHmmss) > -1){
        timeFormat = { format: DATE_FORMAT.HHmmss };
    }
    else if (config.format.lastIndexOf(DATE_FORMAT.HHmm) > -1){
        timeFormat = { format: DATE_FORMAT.HHmm };
    }
    const handleChange = (v) => {
        if (v){
            onChange(momentToValueFormat(v, config.inputFormat));
        }
    }
    
    if (config.format.indexOf('H') === 0){
        return (
            <ConfigProvider locale={esES}>
                <div>
                    <TimePicker 
                        ref={config.ref} 
                        disabled={config.readOnly}
                        format={config.format}
                        /* necesario para interaccion con ant.design => value, change */
                        value={val}
                        onChange={handleChange}
                        onPressEnter={config.onPressEnter} 
                        onBlur={config.onBlur}
                        onKeyDown={config.onKeyDown}
                        placeholder={"Seleccione Hora"} 
                        locale={locale}
                        style={{width:'100%'}}
                    />
                </div>
            </ConfigProvider>
        )
    }
    return (
        <ConfigProvider locale={esES}>
            <div>
            <DatePicker 
                ref={config.ref} 
                disabled={config.readOnly}
                format={config.format}
                showTime={timeFormat}
                /* necesario para interaccion con ant.design => value, change */
                value={val}
                onChange={handleChange}
                onPressEnter={config.onPressEnter} 
                onBlur={config.onBlur}
                onKeyDown={config.onKeyDown} 
                placeholder={"Seleccione Fecha"} 
                locale={locale}
                style={{width:'100%'}}
            />
            </div>
        </ConfigProvider>
    )
}
export default DateFw;