import { Spin } from "antd";
import { ObjectUtil } from "../../util/ObjectUtil";
import { ComponentWithFieldsApi } from "../base/ComponentWithFieldsApi";
import { ProcessUtil } from "../util/ProcessUtil";
import { formModeProcess } from "./helpers/formModeProcess";
import { initializeFormConfig } from "./helpers/initializeFormConfig";
export class FormApi extends ComponentWithFieldsApi {
    type = 'FormApi';
    history = null;
    form = null;

    values = null;
    mapFieldRef = {};

    mapChild = null;//mapa => key: nombre field hijo, value: Array de padres
    mapParent = null;//mapa => key: nombre del field padre, value: Array de field hijos

    FIELD_PREFIX_LABEL = 'FW_LABEL_';

    constructor(name, form, config, history, getParentApi) {
        super();
        //console.log(`FormApi[${name}].constructor`);
        this.name = name;
        this.form = form;
        this.config = config;
        this.history = history;
        this.getParentApi = getParentApi;
        this.processValidation(name, config);
        this.processMode(name, config);
        initializeFormConfig(config, this.getApi);
        this.initializeFieldDependence();
        this.uniqueId = Math.random();
    }
    processMode(name, config) {
        let { mode } = config;
        if (this.getParentApi && this.getParentApi().getModeComponentConfig) {
            mode = this.getParentApi().getModeComponentConfig(name);
        }
        if (mode) {
            config.fields = formModeProcess(config.fields, mode);
        }
    }
    processValidation(name, config) {
        let { validations } = config;
        if (this.getParentApi && this.getParentApi().getValidationComponentConfig) {
            validations = this.getParentApi().getValidationComponentConfig(name);
        }
        if (validations) {
            config.validations = validations;
        }
    }
    /* Permite guardar las relaciones existentes de dependencia entre los fields */
    initializeFieldDependence() {
        let mapChild = {};
        let mapParent = {};
        this.getFieldsConfig().forEach(field => {
            if (field.parents) {
                mapChild[field.name] = field.parents;
                //agregando los padres con hijos a un diccionario
                field.parents.forEach(parent => {
                    mapParent[parent.name] = mapParent[parent.name] || [];
                    mapParent[parent.name].push(field);
                });

            }
        });
        this.mapParent = mapParent;
        this.mapChild = mapChild;
    }

    getData() {
        const out = this.config.dataOptions?.out;
        let data = ObjectUtil.merge(this.values, this.form.getFieldsValue());
        if (out) {
            if (out.includeLabel) {
                Object.keys(this.getMapFieldApi()).forEach(key => {
                    const api = this.getFieldApi(key);
                    if (api && api.getSelectedLabel) {
                        data[this.getFieldConfig(key).labelName] = api.getSelectedLabel();
                    }
                    //data[this.FIELD_PREFIX_LABEL + key] = api.getSelectedLabel();
                });
            }
            if (out.fnTransform)
                data = out.fnTransform(data, this);
        }
        //console.log(`FormApi[${this.name}].getData`,data);
        return data;
    }
    /**
    * Permite setear los datos en el formulario.
    * @param {object} newValues Objeto con los valores que cargaran el formulario
    * @param {boolean} replaceAll Cuando es true reemplaza todos los campos del formulario, cuando es false solo reemplaza los campos existentes en newValues
    */
    setData(newValues, replaceAll, reset) {
        //debugger;
        let oldAllValues = reset ? {} : this.form.getFieldsValue();//copiamos para que no se modifique cuando lo cambiamos
        this.values = newValues;
        if (replaceAll) {
            let newAllValues = ObjectUtil.merge(this.getResetData(), newValues);
            /*
            Object.keys(oldAllValues).forEach(field => {
                if (!newAllValues[field])
                    newAllValues[field] = null;
            });*/
            let changedValues = ObjectUtil.getChangedValues(newAllValues, oldAllValues);
            //console.log(`FormApi[${this.name}].setData`, newAllValues);
            this.form.setFieldsValue(newAllValues);
            this.handleValuesChange(changedValues, newAllValues, false);///se dispara para activar la propagacion de campos dependientes
            //console.log('changed', changedValues);
        }
        else {
            let newAllValues = ObjectUtil.merge({ ...oldAllValues }, newValues);
            let changedValues = ObjectUtil.getChangedValues(newAllValues, oldAllValues);
            //console.log(`FormApi[${this.name}].setData`, newValues);
            this.form.setFieldsValue(newValues);
            this.handleValuesChange(changedValues, newAllValues, false);///se dispara para activar la propagacion de campos dependientes
            //console.log('changed', changedValues);
        }

    }
    setFieldData(name, value) {
        //this.values = values;
        this.form.setFieldsValue({ [name]: value });
    }
    reset() {
        this.form.resetFields();
    }
    load() {
        const self = this;
        let { load, name } = self.config;
        load.fnOkBase = resp => {
            self.setData(resp.dataObject);
            if (load.fnOk)
                load.fnOk(resp, {});
        }
        load.fnErrorBase = error => {
            console.error(`FormApi['${name}'].load => ${error}`);
        }
        ProcessUtil.loadBase(load, {});
    }
    processWithValidationBase(processConfig) {
        this.form.validateFields()
            .then(values => {
                console.log('validation ok=>', values);
                ProcessUtil.process(processConfig);
            })
            .catch(errorInfo => {
                console.error('validation error=>', errorInfo);
            });
    }
    validate(fnOk, fnError) {
        if (!fnOk && !fnError)
            return this.form.validateFields();


        this.form.validateFields()
            .then(values => {
                if (fnOk)
                    fnOk(values);
            })
            .catch(errorInfo => {
                if (fnError)
                    fnError(errorInfo);
            });
    }
    createParamsForSave = (reqParams) => {
        const self = this;
        if (!self.config.save)
            return null;
        let { params } = self.config.save;
        params = ObjectUtil.merge(params, self.getData());
        return {
            ...params,
            ...reqParams?.params
        };
    }
    createParamsForUpdate = (reqParams) => {
        const self = this;
        if (!self.config.update)
            return null;
        let { params } = self.config.update;
        params = ObjectUtil.merge(params, self.getData());
        return {
            ...params,
            ...reqParams?.params
        };
    }
    save(reqParams) {
        const self = this;
        const validations = this.config.validations;
        let params = self.createParamsForSave(reqParams);
        let { url, queryId, fnOk, postLink, message, errorMessage, fnMessageOk, fnMessageError, fnTransformReq, hideMessage, hideMessageError } = this.config.save;
        //const form = this.form;
        //params = ObjectUtil.merge(params,this.getData());
        const fnOkBase = (resp) => {
            if (fnOk)
                fnOk(resp, params);
        }
        const postLinkBase = !postLink ? null : (resp) => postLink(resp, params);
        message = message ? message : 'Se registró correctamente';
        errorMessage = errorMessage ? errorMessage : 'Existieron problemas al guardar el registro';
        let history = this.history;
        const processConfig = { url, queryId, params, fnOk: fnOkBase, message, errorMessage, fnMessageOk, fnMessageError, postLink: postLinkBase, history, fnTransformReq, hideMessage, hideMessageError };
        if (validations)
            this.processWithValidationBase(processConfig);
        else
            ProcessUtil.process(processConfig);
    }
    update(reqParams) {
        const self = this;
        const validations = this.config.validations;
        let params = self.createParamsForSave(reqParams);
        let { url, queryId, fnOk, postLink, message, errorMessage, fnMessageOk, fnMessageError, fnTransformReq, hideMessage, hideMessageError } = this.config.update;
        //const form = this.form;
        //params = ObjectUtil.merge(params, this.getData());
        const fnOkBase = (resp) => {
            if (fnOk)
                fnOk(resp, params);
        }
        const postLinkBase = !postLink ? null : (resp) => postLink(resp, params);
        message = message ? message : 'Se actualizó correctamente';
        errorMessage = errorMessage ? errorMessage : 'Existieron problemas al guardar el registro';
        let history = this.history;
        const processConfig = { url, queryId, params, fnOk: fnOkBase, message, errorMessage, fnMessageOk, fnMessageError, postLink: postLinkBase, history, fnTransformReq, hideMessage, hideMessageError };
        if (validations)
            this.processWithValidationBase(processConfig);
        else
            ProcessUtil.process(processConfig);
    }

    addFieldRef = (name, ref) => {
        if (name && ref) {
            //console.log(`FormApi[${this.name}].addFieldRef(${name})`, ref);
            this.mapFieldRef[name] = ref;
        }
        else {
            console.error(`FormApi[${this.name}].addFieldRef(${name}) => agregando referencia nula`);
        }

    }
    setFieldFocus = (name) => {
        if (this.mapFieldRef[name])
            this.mapFieldRef[name].focus();
    }

    getApi = () => {
        return this;
    }
    setFieldVisible = (name, visible) => {
        const fieldConfig = this.getFieldConfig(name);
        if (fieldConfig) {
            if (fieldConfig && fieldConfig.setVisible)
                fieldConfig.setVisible(visible);
        }
        else {
            const groupConfig = this.getGroupConfig(name);
            if (groupConfig && groupConfig.setVisible)
                groupConfig.setVisible(visible);
        }
    }
    /* captura los cambios de valores del formulario */
    handleValuesChange(changedValues, values, reset = true) {
        //console.log('FormApi.handleValuesChange:', values);
        const self = this;
        let mapParent = self.mapParent;

        self.getGroupsConfig().forEach(groupConfig => {
            const { fnVisible } = groupConfig;
            if (values && fnVisible) {
                const visible = fnVisible ? fnVisible(values[groupConfig.name], values) : true;
                groupConfig.setVisible(visible);
            }
        });

        self.getFieldsConfig().forEach(fieldConfig => {
            const { fnVisible } = fieldConfig;
            if (values && fnVisible) {
                const visible = fnVisible ? fnVisible(values[fieldConfig.name], values) : true;
                fieldConfig.setVisible(visible);
            }
        });
        /*
        Object.keys(values || {}).forEach(name => {
            const { fnVisible } = self.getFieldConfig(name);
            const visible = fnVisible ? fnVisible(value, values) : true;
            self.setFieldVisible(name, visible);
        });*/

        Object.keys(changedValues || {}).forEach(name => {
            const { onChange } = self.getFieldConfig(name) || {};
            if (onChange) {
                //console.log(`FormApi.onChange => ${name}`);
                onChange(values[name], values, this.getApi());
            }
            if (mapParent[name]) {
                self.processChildsForParent(mapParent, values, mapParent[name], name, reset);
            }
        });
    }
    /* Crea el filtro para recargar el hijo en base a los valores de los padres */
    createChildFilter(values, childName) {
        let result = {};
        let allRequired = true;//nos indica que todos los campos requeridos son true
        const parents = this.mapChild[childName] || [];
        parents.forEach(parent => {
            result[parent.paramName] = values[parent.name];
            if (parent.required && !values[parent.name])//si existe un campo requerido que no sea distnto de nulo, no se envia nada
                allRequired = false;
        });
        if (allRequired)
            return result;
        return null;//se envia nulo si no se cumplen todas la validaciones
    }
    getFilterByName(fielName) {
        return this.createChildFilter(this.form.getFieldsValue(), fielName);
    }
    processChildsForParent(mapParent, values, fields, parentName, reset) {
        /* Falta resolver el problema de la doble carga */
        const self = this;
        fields.forEach(field => {
            self.processChild(field, mapParent, values, fields, parentName, reset);
        });
    }
    processChild = (field, mapParent, values, fields, parentName, reset) => {
        const self = this;
        const fieldApi = self.getFieldApi(field.name);
        if (!fieldApi) {
            console.error(`No se encuentra fieldApi de ${field.type} - ${field.name} => Padre :  ${parentName} `);
            return;
        }
        let params = self.createChildFilter(values, field.name);
        if (params) {//si es nulo significa que el combo debe resetearse
            console.log(`Disparando load de ${field.type} - ${field.name} => Padre :  ${parentName} `);
            if (reset) {
                fieldApi.loadWithReset({ params });
            }
            else {
                fieldApi.load({ params });
            }
        }
        else {
            console.log(`Disparando reset de ${field.type} - ${field.name} => Padre :  ${parentName} `);
            if (reset)
                self.getFieldApi(field.name).reset();
            values[field.name] = null;//momentaneamente hasta que se descubra como disparar los cambios
        }
        //llamada recursiva a los hijos del hijo
        if (mapParent[field.name])
            self.processChildsForParent(mapParent, values, mapParent[field.name], field.name, reset);
    }

    getFilterFromData = (name) => this.createChildFilter(this.form.getFieldsValue(), name);
    getConfigData = () => {
        let result = {};
        this.getFieldsConfig().forEach(field => {
            if (field.value)
                result[field.name] = field.value;
        });
        return result;
    }
    getResetData = () => {
        let result = {};
        this.getFieldsConfig().forEach(field => {
            result[field.name] = undefined;
        });
        return result;
    }

    setFieldConfig = (name, newConfig) => {
        debugger;
        let config = this.getFieldConfig(name);
        config.type = newConfig.type;
    }
}

