import React, { useEffect, useState } from 'react';
import { Calendar, Views, momentLocalizer } from 'react-big-calendar';
import 'moment/locale/es';
import moment from 'moment'
import 'react-big-calendar/lib/css/react-big-calendar.css'
import './CalendarFw.css'

const CalendarFw = ({config}) => {

    const localizer = momentLocalizer(moment); 

    const [state, setState] = useState({
    
        events:[
            {
                title: 'Probando',
                start: new Date('2021/08/08 20:30:14'),
                end: new Date('2021/08/08 23:30:14')
            },
            {
                title: 'sss',
                start: new Date('2021/08/08 20:30:14'),
                end: new Date('2021/08/08 23:30:14')
            }
        ]
    });

    //Cambiar el color de fondo del calendar
    // const ColoredDateCellWrapper = ({ children }) =>
    //     React.cloneElement(React.Children.only(children), {
    //     style: {
    //         backgroundColor: 'grey',
    //     },
    // })

    //Permite Insertar data en el calendar
    // const handleSelect = ({ start, end }) => {
    //     const title = window.prompt('New Event name')
    //     if (title)
    //       setState({
    //         events: [
    //           ...state.events,
    //           {
    //             start,
    //             end,
    //             title,
    //           },
    //         ],
    //       })
    //   }
     
      
    
    return (
        <div className="rbc-calendar">
            <Calendar
                selectable
                localizer={localizer}
                events={state.events}
                views={[Views.WEEK,Views.DAY]}
                defaultDate = {new Date()}
                defaultView={Views.WEEK}
                scrollToTime={new Date(2000, 1, 1, 6)}
                onSelectEvent={event => console.log('event-calendar', event)}
                //onSelectSlot={handleSelect}
                messages={{
                    next: ">",
                    previous: "<",
                    today: "Hoy",
                    month: "Mes",
                    week: "Semana",
                    day: "Día"
                }}
                startAccessor="start"
                endAccessor="end"
                // components={{
                //     timeSlotWrapper: ColoredDateCellWrapper,
                // }}
            />
        </div>
        
    )
}
export default CalendarFw;