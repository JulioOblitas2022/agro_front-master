import { FieldApi } from "./FieldApi";


export class ListFieldApi extends FieldApi {
    dataList = null;
    loading = false;
    constructor(config) {
        super(config);
        this.init(config);
    }
    init = (config) => {
        config.propertyLabel = config.propertyLabel || 'label';
        config.propertyValue = config.propertyValue || 'value';
    } 
    isLoading = () => this.loading;
    setLoading = (value) => {this.loading = value;}
    setDataList = (data) => {
        const { aditional } = this.getConfig().load;
        if (data && aditional){
            data.push(...aditional);
        }
        this.dataList = data;
    }
    getDataList = () => this.dataList;
    getOptions = () => {
        const self = this;
        if (!this.getDataList())
            return null;
        const {propertyValue, propertyLabel} = self.config;
        return this.getDataList().map((item, index) => {
            let r = {...item};
            r.value = r[propertyValue] || index;
            r.label = r[propertyLabel] || `No label ${index}`;
            return r;
        });
    }
}