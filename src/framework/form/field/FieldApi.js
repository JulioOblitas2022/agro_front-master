export class FieldApi {
    config = null;
    constructor(config) {
        this.config = config;
    }
    getParentApi = () => this.config.getParentApi();
    unMount = () => {
        if (this.getParentApi && this.getParentApi().addFieldApi){
            this.getParentApi().removeFieldApi(this.config.name);
        } 
    }
    getConfig = () => this.config;
}