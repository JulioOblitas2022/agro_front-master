import './createFields.css';
import { Row, Col} from 'antd';
import FieldFw from '../field/FieldFw';
import GroupFw from '../group/GroupFw';

export const createFormItem = (fieldConfig) => {
    return <FieldFw  config={fieldConfig} />;
    /*
    if (fieldConfig.isInput === false){
        //return  createField(fieldConfig);
        return (<FieldFw  config={fieldConfig} />)
    }
    else {
        console.log('pintando Form.Item');
        return (
            <Form.Item
                name = {fieldConfig.name}
                label = {fieldConfig.compact ? null : fieldConfig.label}
                rules = {fieldConfig.validations}
                style = {fieldConfig.compact ? {margin: 0} : null}
                //hasFeedback={true}
                tooltip={fieldConfig.tooltip}
                //messageVariables={{ label: 'good' }}
                //validateStatus="error"
                //className="compact" 
                //hidden = {true}
            >
                
                <FieldFw  config={fieldConfig}   />
            </Form.Item>
        );
    }*/
}

const createFormItems = (config, getApi) => {
    const childs = config.fields.map((fieldConfig, index) => (<FieldFw key={index.toString()} config={fieldConfig} />));
    return (
        <Row gutter={{ xs: 8, sm: 16, md: 24, lg: 32 }}>
            {childs}
        </Row>
    );
}

const createGroupForm = (config, getApi) => {
    return config.fields.map((groupConfig, index) => (
        <GroupFw 
            key={index.toString()}
            childName = {`group_${index}`}
            getParentApi = {getApi}
            config = {groupConfig}
        >
            {createFormItems(groupConfig)}
        </GroupFw>
    ));
}

export const createFields = (formConfig, getApi) =>{
    if (formConfig.hasGroup)
        return createGroupForm(formConfig, getApi);
    else
        return createFormItems(formConfig, getApi);
}





//ContainerFw = ({childName, getParentApi, config, children}) 

/*

 if (fieldConfig.isInput === false){
            return null;
        }
        else  {
            return (
                <Col span={fieldConfig.column*2} key={index.toString()}>
                    <Form.Item
                        name = {fieldConfig.name}
                        label = {fieldConfig.label}
                        rules = {getValidation(fieldConfig)}
                    >
                        {createField(fieldConfig)}  
                    </Form.Item>
                </Col>
                );
        }
    }

*/