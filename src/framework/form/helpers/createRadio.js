import React from 'react'
import { Radio } from 'antd';

export const createRadio = (config) => {
    return (
        <Radio.Group>
            <Radio value="small">Small</Radio>
            <Radio value="default">Default</Radio>
            <Radio value="large">Large</Radio>
        </Radio.Group>
    )
}
