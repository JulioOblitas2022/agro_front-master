import React from 'react'
import TextArea from 'antd/lib/input/TextArea';

export const createTextarea = (config) => {
    return (
        <TextArea 
            disabled={config.readOnly}
            hidden={config.hidden}
            rows={4} 
            placeholder={config.placeholder}
        />  
    )   
}