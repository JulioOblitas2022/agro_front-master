export class FieldUtil {
    /* Crea la funcion que permite capturar la referencia de los fields */
    static createFieldRef = (config) => {
       return (input) => {
            if (config.getParentApi && input)
                config.getParentApi().addFieldRef(config.name, input);
        }
    }
}