import { DATE_FORMAT, FIELD_TYPE } from "../../../constants/constants";
import { EnvConstants } from "../../../EnvConstants";
import { ProcessUtil } from "../../util/ProcessUtil";


export const initializeFormFieldConfig = (config, formConfig, getParentApi, index = 0) => {
    config.type = config.type || FIELD_TYPE.TEXT;
    config['name'] = config['name'] || `${config.type}_${index}`;
    //config['label'] = config['label'] || config['name'];
    config['column'] = config['column'] || 6;
    config['placeholder'] = config['placeholder'] || '';
    if (config['type']===FIELD_TYPE.SELECT ){
        config['load'] = ProcessUtil.configureProcess(config['load'], EnvConstants.GET_FW_FORM_URL_SELECT_BASE()); 
    } else if (config['type']===FIELD_TYPE.RADIO || config['type']===FIELD_TYPE.CHECKBOX){
        config['load'] = ProcessUtil.configureProcess(config['load'], EnvConstants.GET_FW_FORM_URL_LIST_BASE()); 
    } else if (config['type']==='search'){
        config['load'] = ProcessUtil.configureProcess(config['load'], EnvConstants.GET_FW_FORM_URL_SEARCH_BASE()); 
    } else if (config['type']==='divider'){
        config['column'] = 12;
        config['isInput'] = false; //significa que no es un componente que espere el ingreso de datos
    } else if (config['type']===FIELD_TYPE.DATE){
        config['format'] = config['format'] || DATE_FORMAT.DDMMYYYY;
        config['inputFormat'] = config['inputFormat'] || config['format'];
    } else if (config['type']===FIELD_TYPE.HIDDEN){
        config['column'] = 0;
    } else if (config['type']===FIELD_TYPE.OBJECT){
        config['column'] = 12;
    }
    else if (config['type']===FIELD_TYPE.CALENDAR){
        config['column'] = 12;
    }
    if (getParentApi)
        config['getParentApi'] = getParentApi;
    config.validations = formConfig?.validations ? formConfig.validations[config.name] : null;
    if (formConfig.forGrid){
        config.column = 12;//para que ocupe toda la columna
        config.compact = true;
    }
}
//WITH_ERROR
const initializeGroupConfig = (groupConfig, formConfig, getParentApi, index = 0) => {
    groupConfig.name = groupConfig.name || `${groupConfig.type}_${index}`;
    groupConfig.label = groupConfig.label || groupConfig.name;
    groupConfig.type =  groupConfig.type || FIELD_TYPE.GROUP;
    groupConfig.fields.forEach((fieldConfig, index) =>{
        initializeFormFieldConfig(fieldConfig, formConfig, getParentApi, index);
    });
}
/*
const validateFields = (groupConfig, formConfig, getParentApi) => {
    //if (!groupConfig.fields.length || groupConfig.fields.length==0) 
    //console.error(`El field group[name: ${groupConfig.name}, name: ${groupConfig.label}] no tiene fields`);
    //if (groupConfig.type!=FIELD_TYPE.GROUP) console.error(`El field[${groupConfig.name}] no puede contener otros fields`);
}

const initDefaultType = (config) => {
    config.fields.forEach((fieldConfig, index) =>{
        fieldConfig.name = config.name || `${config.type}_${index}`;
    });
}

const initDefaultName = (config, groupIndex = 0) => {
    config.fields.forEach((fieldConfig, index) =>{
        fieldConfig.name = config.name || `${config.type}_${index}`;
    });
}*/

const validateGroupFields = (groupConfig) => {
    if (groupConfig.type && groupConfig.type !== FIELD_TYPE.GROUP){
        groupConfig.type = FIELD_TYPE.WITH_ERROR;
        console.error(`El field[${groupConfig.name}] no puede contener otros fields`);
        return false;
    }
    if (!groupConfig.fields.length || groupConfig.fields.length === 0){
        groupConfig.type = FIELD_TYPE.WITH_ERROR;
        console.error(`El field group[name: ${groupConfig.name}, name: ${groupConfig.label}] no tiene fields`);
        return false;
    }
    return true;
}

export const initializeFormConfig = (config, getParentApi) =>{

    //const urlBase = process.env.REACT_APP_URL_BASE_BACKEND
    config['load'] = ProcessUtil.configureProcess(config['load'], EnvConstants.GET_FW_FORM_URL_LOAD_BASE()); 
    config['save'] = ProcessUtil.configureProcess(config['save'], EnvConstants.GET_FW_FORM_URL_SAVE_BASE()); 
    config['update'] = ProcessUtil.configureProcess(config['update'], EnvConstants.GET_FW_FORM_URL_UPDATE_BASE()); 
    config.layout = config.layout || "horizontal";
    config.fields.forEach((fieldConfig, index) =>{
        if (fieldConfig.fields){
            if (validateGroupFields(fieldConfig)){
                config.hasGroup = true;
                initializeGroupConfig(fieldConfig, config, getParentApi, index);
            }
        }   
        else
            initializeFormFieldConfig(fieldConfig, config, getParentApi, index);
    });
    return config;
}
