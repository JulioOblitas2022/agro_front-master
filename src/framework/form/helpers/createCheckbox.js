import React from 'react'
import { Checkbox } from 'antd';

export const createCheckbox = (config) => {
    return (
        <Checkbox.Group>
            <Checkbox value="small">Small</Checkbox>
            <Checkbox value="default">Default</Checkbox>
            <Checkbox value="large">Large</Checkbox>
        </Checkbox.Group>
    )
}


