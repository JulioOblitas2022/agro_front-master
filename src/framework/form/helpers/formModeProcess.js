import { ModeUtil } from "../../util/ModeUtil";
import { MODE_FILTER, MODE_SHORTCUT } from "../../../constants/constants";

export const formModeProcess = (fields, mode)=>{
    if (mode){
        const arrayShow = mode[MODE_FILTER.SHOW];
        const arrayHide = mode[MODE_FILTER.HIDE];
        const arrayEdit = mode[MODE_FILTER.EDIT];
        const arrayRead = mode[MODE_FILTER.READ];

        if (arrayShow?.length>0)
            fields = ModeUtil.includeFieldsByNames(fields, arrayShow);
        else if (arrayHide?.length>0)
            fields = ModeUtil.excludeFieldsByNames(fields, arrayHide);

        if (arrayEdit?.length>0)
            fields = ModeUtil.notMarkFieldsReadOnlyByNames(fields, arrayEdit);
        else if (arrayRead?.length>0)
            if (arrayRead.includes(MODE_SHORTCUT.ALL)){
                fields = ModeUtil.markAllFieldsReadOnly(fields, arrayRead);     
            }
                
            else
                fields = ModeUtil.markFieldsReadOnlyByNames(fields, arrayRead); 
        
   
    }
    return fields;
}

