import React from 'react'
import { Input } from 'antd';
import { FieldUtil } from './FieldUtil';

export const createInput = (config) => {
    return (
        <Input 
            ref={FieldUtil.createFieldRef(config)}   
            disabled={config.readOnly}
            hidden={config.hidden}
            placeholder={config.placeholder}
            onPressEnter={config.onPressEnter} 
            onBlur={config.onBlur}
            onKeyDown={config.onKeyDown} 
        />
    )
}

