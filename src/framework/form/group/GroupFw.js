import './GroupFw.css';
import React from 'react'
import HeaderBarFw from '../../header-bar/HeaderBarFw';

const GroupFw = ({formName, getParentApi, config, children}) => {
    const [isCollapsed, setIsCollapsed] = React.useState(false);
    const [visibleState, setVisibleState] = React.useState(true);
    config.setVisible = setVisibleState;
    const hasBarHeader = (config) => config.label || config.barHeader;

    if (!visibleState)
        return null;
    return (
        <div className={`group-fw ${hasBarHeader(config) ? null : 'group-fw-without-header'}`} >
            <HeaderBarFw
                name={formName + '.headerBar'}
                config={{
                    title: config.label,
                    button: config.barHeader,
                    collapsible: config.collapsible
                }}
                getParentApi={getParentApi}
                setIsCollapsed = {setIsCollapsed}
                isCollapsed = {isCollapsed}
            />
            <div className={`group-fw-body ${isCollapsed ? 'collapsed' : 'expanded'}`}>
                {children}
            </div>
        </div>
    )
}
export default GroupFw;

