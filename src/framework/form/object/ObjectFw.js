import React from 'react';
import { FieldUtil } from '../helpers/FieldUtil';

const ObjectFw = ({config, ...restProps}) => {
    
    return (
        <object
        data={config.data} 
        type={config.data_type}
        width="100%" 
        height="650">
        </object>
    )
}
export default ObjectFw;