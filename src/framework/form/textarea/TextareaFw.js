import React from 'react'
import TextArea from 'antd/lib/input/TextArea';

const TextareaFw = ({value, onChange, config, ...restProps}) => {
    return (
        <TextArea 
            value={value}
            onChange={onChange}
            disabled={config.readOnly}
            hidden={config.hidden}
            rows={config.rows} 
            placeholder={config.placeholder}
            style={{width:'100%'}}
        />  
    )
}
export default TextareaFw;