import React from 'react'
import { Input } from 'antd';
import { FieldUtil } from '../helpers/FieldUtil';
import { conformToMask } from 'text-mask-core/dist/textMaskCore'

const InputFw = ({ value, onChange, config }) => {
    const [state, setState] = React.useState({ value: value });
    const handleChange = (event) => {
        const { value } = event.target;
        setState({ value: value });
        //console.log('cambiando : ', value);
        //https://github.com/text-mask/text-mask/tree/master/core
        //const results = conformToMask('5554833902', ['(', /[1-9]/, /\d/, /\d/, ')', ' ', /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/])
        //console.log(results);
        //results.conformedValue // '(555) 483-3902'
        if (onChange) {
            onChange(event);
        }
    }

    if (value != state.value) {
        setState({ value: value });
    }

    //console.log(`render=>InputFw[${config.name}]=> value:${value}, state:${state.value}`);
    return (
        <Input
            //{...this.props}
            value={state.value}
            onChange={handleChange}
            ref={FieldUtil.createFieldRef(config)}
            disabled={config.readOnly}
            hidden={config.hidden}
            placeholder={config.placeholder}
            onPressEnter={config.onPressEnter}
            onBlur={config.onBlur}
            onKeyDown={config.onKeyDown}
            maxLength={config.maxLength}
        />
    )
}
export default InputFw;

