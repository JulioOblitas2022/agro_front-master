import { UploadOutlined } from '@ant-design/icons';
import { Button, Upload } from 'antd';
import React from 'react';
import { DonwloadUtil } from '../../../util/DonwloadUtil';
import { FileUtil } from '../../../util/FileUtil';
import { S3Util } from '../../../util/S3Util';

export const UploadFw = ({value = '', onChange, config}) => {
    const [state, setState] = React.useState({
        url: null
    });
    const [fileListState, setFileListState] = React.useState(null);
    //https://upload-react-component.vercel.app/demo/custom-request

    const handleChange = (e) => {
        console.log(e.fileList);
        let files = e.fileList.map(item => ({
            //...item,
            name: item.uid,//item.name,
            percent: item.percent,
            status: item.status,
            uid: item.uid,
           // url: 'https://zos.alipayobjects.com/rmsportal/jkjgkEfvpUPVyRjUImniVslZfWPnJuuZ.png'
        }))
        console.log('handleChange', files);
        setFileListState(files);
        if (!e.event){
            if(e.file.status === 'done'){           
                if (onChange) {
                    onChange(fileListToValue(e.fileList));
                }
            }
            else if(e.file.status === 'removed'){
                if (onChange) {
                    onChange(fileListToValue(e.fileList));
                }
            }
            else if (e.file.status === 'uploading'){

            }
        }
        
    }

    const fileListToValue = (list) => {
        if (list){
            return list.map(item => item.name);
        }
    }

    const getCantFiles = () => {
        return fileListState ? fileListState.length : 0;
    }
    const getMaxFiles = () => {
        return config.multiple ? config.multiple : 1;
    }

    const handleBeforeUpload = (file, fileList) => {//boolean | Promise<File>
        return new Promise((resolve, reject) => {
            if ((getCantFiles() + (fileList?fileList.length:0)) > getMaxFiles())
                reject('No puede exceder la cantidad de archivos');
            let newName = FileUtil.generateFileName_YYYYMMDD_HHmmss(file.name);
            S3Util.getTemporalPutUrl({
                folder: config.folder,
                fileName: newName,
                fnOk: (resp) => {
                    file.uid = newName;
                    setState({url: resp.dataObject['url']});
                    resolve({
                        status: 'done',
                        uid: newName
                    });
                },
                error: (error) => {
                    console.error(error);
                    reject(error);
                }
            });

        });
    }

    const getUrl = (file) => {
        return new Promise((resolve, reject) => {
            let newName = FileUtil.generateFileName_YYYYMMDD_HHmmss(file.name);
            S3Util.getTemporalPutUrl({
                folder: config.folder,
                fileName: newName,
                fnOk: (resp) => {
                    file.uid = newName;
                    resolve(resp.dataObject['url']);
                },
                error: (error) => {
                    console.error(error);
                    reject(error);
                }
            });
        });
    }

    
    const props = {
        name: config.name,
        multiple: config.multiple,
        method: 'PUT',
        maxCount: 1
    };

    

    const canUpload = (config, fileListState) => {
        let max = config.multiple ? config.multiple : 1;
        if (fileListState){
            if (fileListState.length == max){
                return false;
            }
        }
        return true;
    }

    const handleDownload = async(file) => {
        S3Util.donwload(file.name, config.folder);
    }

    return (
        <Upload 
            {...props}
            action={state.url}
            fileList={fileListState}
            //value={val}
            beforeUpload = {handleBeforeUpload}
            onChange = {handleChange}
            onDownload = {handleDownload}
            showUploadList = {
                {
                    showDownloadIcon: true,
                    showRemoveIcon: true
                }
            }
            progress = {
                {
                    strokeColor: {
                      '0%': '#108ee9',
                      '100%': '#87d068',
                    },
                    strokeWidth: 3,
                    format: percent => `${parseFloat(percent.toFixed(2))}%`,
                }
            }
        >
            <Button disabled={!canUpload(config, fileListState)} icon={<UploadOutlined />}>Upload</Button>
        </Upload>
    )   
}
