import { Button } from 'antd';
import React from 'react';
import { ICON } from '../../../../constants/iconConstants';

export const createButton = ({icon, text}) => {
    return ( <Button icon={icon || ICON.UPLOAD}>{text || 'Click to Upload'}</Button> )   
}