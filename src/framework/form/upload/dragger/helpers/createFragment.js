import React from 'react';
import { ICON } from '../../../../../constants/iconConstants';

export const createFragment = ({icon, text}) => {
    return (
        <>
            <p className="ant-upload-drag-icon">{icon || ICON.DRAGGER}</p>
            <p className="ant-upload-text">Click or drag file to this area to upload</p>
            <p className="ant-upload-hint"> Support for a single or bulk upload. Strictly prohibit from uploading company data or other band files </p>
        </>
    )   
}