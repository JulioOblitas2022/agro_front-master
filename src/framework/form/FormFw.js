import './FormFw.css';
import React from 'react'
import { Col, Form, Row, Spin } from 'antd';
import { createFields } from './helpers/createFields';
import { FormApi } from './FormApi';
import { useHistory } from 'react-router-dom';
import ContainerFw from '../container/ContainerFw';
import { ObjectUtil } from '../../util/ObjectUtil';

const createForm = (config, form, onFinish, onValuesChange, onFieldsChange, getApi) => (
    <Form
        colon={true}
        form={form}
        onFinish={onFinish}
        labelCol={{
            flex: 3
        }}
        wrapperCol={{
            flex: 9
        }}
        layout={config.layout} //horizontal horizontal, vertical, inline.
        //initialValues={initialValue}
        onValuesChange={onValuesChange}
        onFieldsChange={onFieldsChange}
        size={'middle'}
    //fields={fields}
    >
        {createFields(config, getApi)}
    </Form>
)

const createFormHidden = (config, form, onFinish, onValuesChange, onFieldsChange, children) => (
    <Form
        component={false}
        form={form}
        onFinish={onFinish}
        onValuesChange={onValuesChange}
        onFieldsChange={onFieldsChange}
        size={'default'}
    >
        {children}
    </Form>
)

const FormFw = ({ name, getParentApi, config, children }) => {
    const history = useHistory();
    const [form] = Form.useForm();
    const apiRef = React.useRef(new FormApi(name, form, config, history, getParentApi));
    //if (!apiRef.current) apiRef.current = new FormApi(name, form, config, history, getParentApi);//inicializamos
    const getApi = () => apiRef.current;
    const onValuesChange = (changedValues, values) => {
        //console.log('changedValues',changedValues);
        getApi().handleValuesChange(changedValues, values);
    };

    const onFinish = values => { console.log('Received values of form:', values); };
    const onFieldsChange = (changedFields, allFields) => {
        //console.log('form',form.getFieldInstance(0));
    }

    /*
    const onKeyPress = (e) => {
        if (e.charCode === 13) {
            console.log('Enter... (KeyPress, use charCode)');
        }
        if (e.keyCode === 13) {
            console.log('Enter... (KeyDown, use keyCode)');
        }
    }*/

    React.useEffect(() => {
        getApi().setData(ObjectUtil.merge({ ...config.values }, getApi().getConfigData()));
        if (config.autoLoad)
            getApi().load();
        if (getParentApi)
            getParentApi().addComponent(name, getApi());
        //console.log(`FormFw[${name}].Mounting`);
        if (config.onComponentLoad) {
            config.onComponentLoad(getApi(), getParentApi());
        }
        return () => {
            apiRef.current = null;
            //console.log(`FormFw[${name}].Unmounting`);
        }
    }, []);


    if (config.forGrid)//grid invisible
        return createFormHidden(config, form, onFinish, onValuesChange, onFieldsChange, children);
    else if (config.hasGroup)
        return createForm(config, form, onFinish, onValuesChange, onFieldsChange, getApi);
    else
        return (//visible
            <>
                <ContainerFw
                    childName={name}
                    getParentApi={getApi}
                    config={config}
                >
                    {createForm(config, form, onFinish, onValuesChange, onFieldsChange, getApi)}
                </ContainerFw>
            </>

        )
}
export default FormFw;
//<pre>{JSON.stringify(fields, null, 2)}</pre>
