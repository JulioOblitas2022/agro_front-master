import React from 'react'
import { Button, Input } from 'antd';
import { FieldUtil } from '../helpers/FieldUtil';
import { conformToMask } from 'text-mask-core/dist/textMaskCore'
import { SearchOutlined } from '@ant-design/icons';

const ButtonFormFw = ({ value, config }) => {
    const [state, setState] = React.useState({ value: value });



    if (value != state.value) {
        setState({ value: value });
    }

    //console.log(`render=>InputFw[${config.name}]=> value:${value}, state:${state.value}`);
    return (
        <Button type="primary" shape="circle" icon={<SearchOutlined />} onClick={config.onClick} />
    )
}
export default ButtonFormFw;

