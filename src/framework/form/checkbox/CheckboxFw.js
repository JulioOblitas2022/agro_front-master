import React from 'react'
import { Checkbox } from 'antd';
import { CheckboxApi } from './CheckboxApi';

const CheckboxFw = ({value, onChange, config, ...restProps}) => {
    const [,forceRender] = React.useState(true);
    const apiRef = React.useRef(new CheckboxApi(config));
    //if (!apiRef.current) apiRef.current = new CheckboxApi(config);//inicializamos
    const getApi = () => apiRef.current;
    //getApi().setInnerValue(value);
    getApi().forceRender = forceRender;
    getApi().onChange = onChange;

    const handleChange = (value, option) => {
        //getApi().setInnerValue(value);
        if (onChange) {
            onChange(value);
        }
    }
    React.useEffect(() => {
        getApi().firstMount();
        //console.log('SelectFw Primer Montado');
        return () => {
            getApi().unMount();
            apiRef.current = null;
            //console.log(`SelectFw[${config.name}].desmontado`);
        }
    }, []);
    return (
        <Checkbox.Group
            value={value}
            onChange={handleChange}
            options={getApi().getOptions()}
        />
    )
}
export default CheckboxFw;
