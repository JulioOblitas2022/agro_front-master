import { EnvConstants } from "../../../EnvConstants";
import { FileUtil } from "../../../util/FileUtil";
import { NotificationUtil } from "../../../util/NotificationUtil";
import { FieldApi } from "../field/FieldApi";

export class DraggerApi extends FieldApi {
    /*  fileMeta
        lastModified: 1609781010859
        lastModifiedDate: Mon Jan 04 2021 12:23:30 GMT-0500 (hora estándar de Perú)
        __proto__: Object
        name: "03-I35-002335.pdf"
        size: 50987
        type: "application/pdf"
        uid: "file_20210427_221547.pdf"
        webkitRelativePath: ""
    */
    dataList = null;
    fileList = [];
    constructor(config) {
        super(config);
    }
    setDataList = (data) => {
        this.dataList = data;
    }
    getDataList = () => this.dataList;
    initUpload = (fileMeta) => {
        return this.addFile(fileMeta);
    }
    errorUpload = (fileId) => {

    }
    finishUpload = (fileId) => {
        
    }
    addFile = (fileMeta) => {
        let r = this.fileList.filter(item => item.name == fileMeta.name);
        if (r && r.length>0)
            return null;
        let fileId = FileUtil.generateFileName_ramdom_YYYYMMDD_HHmmss(fileMeta.name);
        let temp = {
            fileId,
            name: fileMeta.name,
            size: fileMeta.size,
            type: fileMeta.type,
            uid: fileMeta.fileId
        };
        this.fileList.push(temp);
        //console.log('addFile => ', fileMeta);
        //console.log('addFile', this.fileList);
        return fileId;
    }
    getFileList = () => {
        return this.fileList;
    }
}