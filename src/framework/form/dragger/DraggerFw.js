import { InboxOutlined } from '@ant-design/icons';
import Dragger from 'antd/lib/upload/Dragger';
import axios from 'axios';
import React from 'react';
import { DonwloadUtil } from '../../../util/DonwloadUtil';
import { S3Util } from '../../../util/S3Util';
import { DraggerApi } from './DraggerApi';

export const DraggerFw = ({value = '', onChange, config}) => {
    const apiRef = React.useRef(new DraggerApi(config));
    const getApi = () => apiRef.current;
    const [fileListState, setFileListState] = React.useState(null);
    
  
    const handleChange = (e) => {
        /*
        let files = e.fileList.map(item => ({
            //...item,
            name: item.uid,//
            percent: item.percent,
            status: item.status,
            uid: item.uid,
           // url: 'https://zos.alipayobjects.com/rmsportal/jkjgkEfvpUPVyRjUImniVslZfWPnJuuZ.png'
        }))*/
        
        e.fileList.forEach(item => {
            item.name = item.uid;
        });
        setFileListState(e.fileList);
        if (!e.event){
            if(e.file.status === 'done'){       
                console.log('terminando de subir : ', e.fileList);    
                if (onChange) {
                    onChange(fileListToValue(e.fileList));
                }
            }
            else if(e.file.status === 'removed'){
                if (onChange) {
                    onChange(fileListToValue(e.fileList));
                }
            }
            else if (e.file.status === 'uploading'){

            }
        }
         
    }

    const fileListToValue = (list) => {
        if (list){
            return list.map(item => item.name);
        }
    }

    const getCantFiles = () => {
        return fileListState ? fileListState.length : 0;
    }
    const getMaxFiles = () => {
        return config.multiple ? config.multiple : 1;
    }

    const handleBeforeUpload = (file, fileList) => {//boolean | Promise<File>
        console.log('before');
        if ((getCantFiles() + (fileList?fileList.length:0)) > getMaxFiles())
            return false;
        let fileId = getApi().initUpload(file);
        if (!fileId)
            return false;
                //reject('No puede subir el mismo archivo');
                //reject('No puede exceder la cantidad de archivos');
        return new Promise((resolve, reject) => {
            file.uid = fileId;
            resolve(file);
            /*
            S3Util.getTemporalPutUrl({
                folder: config.folder,
                fileName: fileId,
                fnOk: (resp) => {
                    file.uid = fileId;
                    setState({url: resp.dataObject['url']});
                    resolve(file);
                },
                error: (error) => {
                    console.error(error);
                    reject(error);
                }
            });*/

        });
    }

    
    const props = {
        name: config.name,
        multiple: config.multiple,
        method: 'PUT',
        /*
        onStart(file) {
            console.log('onStart', file, file.name);
        },
        onSuccess(ret) {
            console.log('onSuccess', ret);
        },
        onError(err) {
            console.log('onError', err);
        },*/
        action: (file) => {
            console.log('action', file);
            return new Promise((resolve, reject) => {
                S3Util.getTemporalPutUrl({
                    folder: config.folder,
                    fileName: file.uid,
                    contentType: file.type,
                    fnOk: (resp) => {
                        //file.uid = fileId;
                        //setState({url: resp.dataObject['url']});
                        //console.log('generandoAction', resp.dataObject['url']);
                        resolve(resp.dataObject['url']);
                    },
                    error: (error) => {
                        console.error(error);
                        reject(error);
                    }
                });
            });
            
            /*
            return new Promise(resolve => {
                setTimeout(() => {
                resolve(state.url);
                }, 2000);
            });*/
        }
        
    };

    
    const customRequest = ({
        action,
        data,
        file,
        filename,
        headers,
        onError,
        onProgress,
        onSuccess,
        withCredentials,
      }) => {
        // EXAMPLE: post form-data with 'axios'
        // eslint-disable-next-line no-undef
        /*
        const formData = new FormData();
        if (data) {
          Object.keys(data).forEach(key => {
            formData.append(key, data[key]);
          });
        }
        formData.append(filename, file);*/
        //console.log('headers',headers);
        axios
          .put(action, file, {
            withCredentials,
            headers: { 'Content-Type': file.type },
            onUploadProgress: ({ total, loaded }) => {
              onProgress({ percent: Math.round((loaded / total) * 100) }, file);
            },
          })
          .then(({ data: response }) => {
            onSuccess(response, file);
          })
          .catch(onError);
    
        return {
          abort() {
            console.log('upload progress is aborted.');
          },
        };
      }

    const canUpload = (config, fileListState) => {
        let max = config.multiple ? config.multiple : 1;
        if (fileListState){
            if (fileListState.length === max){
                return false;
            }
        }
        return true;
    }

    const handleDownload = async(file) => {
        S3Util.getTemporalGetUrl({
            folder: config.folder,
            fileName: file.name,
            fnOk: (resp) => {
                console.log(resp);
                if (resp?.dataObject?.url){
                    DonwloadUtil.donwloadGet(resp.dataObject.url, file.name);
                }
            },
            error: (error) => {
                console.error(error);
            }
        });
    }


    React.useEffect(() => {
        if (config.getParentApi){
            config.getParentApi().addFieldApi(config.name, apiRef.current);
        } 
        return () => {

        }
    }, []);

    return (
        <Dragger 
            {...props} 
            //transformFile={null}  
            customRequest={customRequest}
            disabled={!canUpload(config, fileListState)}
            //action={state.url}
            fileList={fileListState}
            //value={val}
            beforeUpload = {handleBeforeUpload}
            onChange = {handleChange}
            onDownload = {handleDownload}
            showUploadList = {
                {
                    showDownloadIcon: true,    
                    showRemoveIcon: true
                }
            }
            progress = {
                {
                    strokeColor: {
                      '0%': '#108ee9',
                      '100%': '#87d068',
                    },
                    strokeWidth: 3,
                    format: percent => `${parseFloat(percent.toFixed(2))}%`,
                }
            }
        >
            <p className="ant-upload-drag-icon">
                <InboxOutlined />
            </p>
            <p className="ant-upload-text">Haga click o arrastre los archivos hacia esta area para subirlos</p>
            <p className="ant-upload-hint">
                
            </p>
        </Dragger>
    )   
}


/*
https://stackoverflow.com/questions/58234437/corrupted-image-on-uploading-image-to-aws-s3-via-signed-url
https://stackoverflow.com/questions/47021594/how-should-customrequest-be-set-in-the-ant-design-upload-component-to-work-with
https://medium.com/@aidan.hallett/securing-aws-s3-uploads-using-presigned-urls-aa821c13ae8d

<input type="file" id="upload"/>

var upload = document.getElementById('upload');
var file = upload.files[0];

//COMMENTED OUT BECAUSE IT WAS CAUSING THE ISSUE
//const formData = new FormData();
//formData.append("file", file);

// Assuming axios

const config = {
    onUploadProgress: function(progressEvent) {
        var percentCompleted = Math.round(
            (progressEvent.loaded * 100) / progressEvent.total
        );
        console.log(percentCompleted);
    },
    header: {
        'Content-Type': file.type
    }
};

axios.put(S3SignedPutURL, file, config)
   .then(async res => {
        callback({res, key})
    })
    .catch(err => {
        console.log(err);
    })

*/