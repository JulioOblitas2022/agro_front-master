import './HiddenFw.css';
import React from 'react';

const HiddenFw = ({value, onChange, config, ...restProps}) => {
    const fnRender = () => {
        let parentData = config.getParentApi ? config.getParentApi().getData() : null;
        return config.fnRender ? config.fnRender(value, parentData) : null;
    }
    let result = fnRender();
    if (result)
        return result;
    return (
        <span className="hidden-fw">{value || config.value}</span>
    )
}
export default HiddenFw;