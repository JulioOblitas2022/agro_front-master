export const initializeSelectConfig = (config) => {
    config.propertyLabel = config.propertyLabel || 'label';
    config.propertyValue = config.propertyValue || 'value';
}