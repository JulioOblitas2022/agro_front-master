import { Select } from 'antd';
const { Option } = Select;

const getLabel = (config, row) => {
    if (row[config.propertyValue])
        return row[config.propertyLabel]
    return 'ERROR => SIN ID UNICO';
}

export const createOptions = (config, data) =>{
    return data.map((row, index) => (
            <Option key={(row[config.propertyValue] || index)}>{getLabel(config, row)}</Option>
        )
    )
}
