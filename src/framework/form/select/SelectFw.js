import React from 'react'
import { Select } from 'antd';
import { SelectApi } from './SelectApi';
import { FieldUtil } from '../helpers/FieldUtil';

const valueProcessToArray = (config, value) => {
    if (config.multiple && config.multipleInString){
        if (value && value != ""){
            //console.log(`valueProcessFromArray ... '${value}'`);
            return value.split(",").map(item => parseInt(item));
        }
        return [];
    }
    return value;
}

const valueProcessFromArray = (config, value) => {
    if (config.multiple && config.multipleInString){
        if (value && value.length){
            //console.log(`valueProcessToArray ... '${value}'`);
            //debugger;
            return value.join();
        }
    }
    return value;
}

const SelectFw = ({value, onChange, config}) => {
    const [,forceRender] = React.useState(true);
    const apiRef = React.useRef(new SelectApi(config));
    //if (!apiRef.current) apiRef.current = new SelectApi(config);//inicializamos
    const getApi = () => apiRef.current;
    value = valueProcessToArray(config, value);
    
    //value = value && config.multipleInString /*&& !value?.length*/ ? value.split(",") : value;
    //console.log(`Transformando string hacia array ${config.multipleInString}`, value);


    getApi().setInnerValue(value);
    getApi().forceRender = forceRender;
    getApi().onChange = onChange;

    const handleChange = (value, option) => {
        getApi().setInnerValue(value);
        if (onChange) {
            //value = value?.length && config.multipleInString ? value.join() : value;
            value = valueProcessFromArray(config, value);
            onChange(value);
        }
    }
    const fnFilter = (input, option) => option[config.propertyLabel].toLowerCase().indexOf(input.toLowerCase()) >= 0;
    const handleSearch = !config.search?.server ? null : val => {
        getApi().load({params:{label:val}});
    }

    React.useEffect(() => {
        getApi().firstMount();
        //console.log('SelectFw Primer Montado');
        return () => {
            getApi().unMount();
            apiRef.current = null;
            //console.log(`SelectFw[${config.name}].desmontado`);
        }
    }, []);

    //console.log("%c" + `SelectFw[${config.name}].Render [value: ${getApi().getValue()}, loading: ${getApi().isLoading()}, items: ${getApi().getDataList()?.length}]`, "color:red");
    //console.log('getApi().getDataList()',getApi().getDataList());


    /*
    const tagRender = (props) => {
        //console.log(props);
        //style={{ marginRight: 3 }}
        const { label, value, closable, onClose } = props;
        return <Tag closable={closable} onClose={onClose} />;
        return (
          <Tag closable={closable} onClose={onClose} >
            {label}
          </Tag>
        );
      }*/

    //console.log('value=============>', value);  
    //console.log(`SelectApi[${config.name}].render`, value);
    return (
        <Select
            ref={FieldUtil.createFieldRef(config)}    
            disabled={config.readOnly}
            allowClear={config.allowClear}
            mode={config.multiple?'multiple':null}
            onBlur={config.onBlur}
            onKeyDown={config.onKeyDown} 
            showSearch={!!config.search}

            //value={getApi().getValue()}
            value={value}
            loading={getApi().isLoading()}
            options={getApi().getOptions()}
            placeholder="Seleccione"
            
            //defaultValue={value}
            //notFoundContent={'Not Founddddd'}
            filterOption={fnFilter}
            onChange={handleChange}
            onSearch={handleSearch}
            maxTagCount={config.maxVisible}
            //tagRender={tagRender}
        />
    )
}
export default SelectFw;

//https://ant.design/components/form/#components-form-demo-customized-form-controls

/* nota */
/* para poder extender los componentes antd es necesario pasar las propiedades => value = '', onChange */
// {createOptions(config, state.dataList)}
