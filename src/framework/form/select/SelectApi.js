import { ObjectUtil } from "../../../util/ObjectUtil";
import { LoadUtil } from "../../util/LoadUtil";
import { ListFieldApi } from "../field/ListFieldApi";
import { initializeSelectConfig } from './helpers/initializeSelectConfig';
export class SelectApi extends ListFieldApi {
    value = null;
    //innerValue = null;
    forceRender = null;
    onChange = null;
    constructor(config) {
        //console.log(`SelectApi[${config.name}].constructor`);
        super(config);
    }
    firstMount = () => {
        //console.log(`SelectApi[${this.config.name}].firstMount`);
        if (this.getParentApi && this.getParentApi().addFieldApi){
            this.getParentApi().addFieldApi(this.config.name, this);
        } 
        if (this.config.load.dataList){
            this.setDataList([...this.config.load.dataList]);
            this.render();
        }
        else if (this.config.load){
            //let params = this.getParentApi().getFilterByName(this.config.name);
            this.load();
            //console.log('Datooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo ', this.getParentApi().getFilterByName(this.config.name));
        }
    }
    
    getSelectedLabel = () => {
        let selected = this.getSelectedItem();
        let result = null;
        if (selected){
            if (Array.isArray(selected)){
                let extra = 0;
                if (this.config.maxVisible){
                    extra = selected.length - this.config.maxVisible;
                }
                selected = extra > 0 ?  selected.slice(0, this.config.maxVisible) : selected;
                result = selected.map(item => item[this.config.propertyLabel]).join(',');
                result = extra > 0 ? `${result}, +${extra}` : result;
            }
            else {
                result = selected[this.config.propertyLabel];
            }
        }
        return result;
    }
    getSelectedItem = () => {
        if (Array.isArray(this.getValue()) && this.getDataList()){
            return this.getDataList().filter(item => this.getValue().includes(item[this.config.propertyValue]));
        }
        else if (this.getValue() && this.getDataList()){
            let r = this.getDataList().filter(item => item[this.config.propertyValue] === this.getValue());
            if (r.length > 0)
                return r[0];
        }
        return null;
    }
    
    getValue = () => this.value; 
    setValue = (value) => { 
        if (!value && this.getDefaultValue())
            value = this.getDefaultValue();
        this.setInnerValue(value);
        this.onChange(value);
    }
    setInnerValue = (value) => { 
        if (!value && this.getDefaultValue())
            value = this.getDefaultValue();
        this.value = value 
    }
    render = () => this.forceRender(prevState => (!prevState));
    
    
    getReqParams=(reqParams, filterParent)=>{
        reqParams = reqParams || {};
        reqParams.params = ObjectUtil.merge(reqParams.params,filterParent);
        return reqParams;
    }
    
    load = (reqParams) => {  
        let filterParent = this.getFilterParent();
        if (filterParent){
            //console.log(`SelectApi[${this.config.name}].load`,reqParams);
            LoadUtil.load(this, this.config, this.getReqParams(reqParams, filterParent));
        }  
    }
    loadWithReset = (reqParams) => { 
        let filterParent = this.getFilterParent();
        if (filterParent){
            this.setValue(null);
            //console.log(`SelectApi[${this.config.name}].loadWithReset`,reqParams);
            LoadUtil.load(this, this.config, this.getReqParams(reqParams, filterParent));
        }
        else{
            this.reset();
        }
    }
    reset = () => {
        //console.log(`SelectApi[${this.config.name}].reset`);
        if (!this.isDefaultValue() && !this.dataList)//si ambos valores son nulos no hay necesidad de renderizar nuevamente
            return;
        this.setDataList(null);
        this.setValue(null);
    }
    hasFilterValid = () => this.getParentApi().getFilterFromData(this.config.name) !=null;
    getFilterParent = () => this.getParentApi().getFilterFromData(this.config.name);
    isReadyForLoad = () => {
        if (this.dataList)  
            return false;
        return true;      
    }
    getDefaultValue = () => this.config.defaultValue;
    isDefaultValue = () => this.getDefaultValue() === this.getValue();
    
}
