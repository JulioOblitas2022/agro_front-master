import React from 'react';
import { Spin } from 'antd';
import './loadingStyle.css';
import { LoadingOutlined } from '@ant-design/icons';
export const LoadingPage = (props) => {

    const antIcon = <LoadingOutlined style={{ fontSize: 60, color: "#0B1849" }} spin/>;
    return (
        <div className="loadingPage" id={props.id} style={{display:"none"}}>
            {/* <img src={LOGO}  className="img-loader"/> */}
            
            <Spin tip="Cargando..." size="large" indicator={antIcon} style={{marginTop:'20%'}} />
        </div>
    )
}
