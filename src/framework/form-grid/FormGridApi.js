
export class FormGridApi {
    name = null;
    config = null;
    history = null;
    mapComponentApi = {};

    constructor(name, config, history) {
        console.log('FormGridApi.constructor');
        this.name = name;
        this.config = config;
        this.history = history;
    }

    getApi = () => {
        return this;
    }
    addComponent = (name, componentApi) => {
        this.mapComponentApi[name] = componentApi;
    }
    getComponent = (name) => {
        return this.mapComponentApi[name];
    }
    getComponent = name => {
        const preName = `${this.name}.`;
        let dotPos = name.indexOf(preName);
        if (dotPos > -1) {//significa que es hijo o subComponent
            dotPos = name.indexOf('.', preName.length);
            let subComponent = name.substr(0, dotPos);
            if (dotPos > -1) {//si es hijo del subComponent
                //console.log('obteniendo sub_subComponent => ', name, subComponent,this.mapComponentApi[subComponent].getComponent(name));
                return this.mapComponentApi[subComponent]?.getComponent(name);
            }
            else {
                //console.log('obteniendo subComponent => ', name);
                return this.mapComponentApi[name];
            }
        }
        return null;
    }
}

