
import { BUTTON_DEFAULT } from '../bar-button/BarButtonFw';
import { COLUMN_DEFAULT } from '../grid/GridFw';

export const configureFormOnlyAdd = (config, formConfig, gridConfig, getForm, getGrid, getBar, getApi, showSave) => {

    const modeNew = () => {
        getForm().reset();
        getBar().show(['add', 'cancel']);
    }

    const fnAddOk = (values) => {
        getGrid().addItem(getForm().getData(), true);
        modeNew();
    }

    const fnAdd = () => {
        getForm().validate(fnAddOk);
    }

    const fnCancel = () => {
        modeNew();
    }

    const fnDelete = (value, row, index) => {
        getGrid().deleteItem(row.key);
        modeNew();
    }

    const fnSave = () => {
        getGrid().save();
    }

    const barButtonconfig = {
        fields: [
            { ...BUTTON_DEFAULT.ADD, onClick: fnAdd },
            { ...BUTTON_DEFAULT.CANCEL, onClick: fnCancel }
        ]
    }

    const gridBarHeaderButtonconfig = {
        fields: [
            { ...BUTTON_DEFAULT.SAVE, type: null, onClick: fnSave }
        ]
    }

    const actions = [
        { ...COLUMN_DEFAULT.DELETE, label: 'Opciones', title: 'Eliminar', color: 'red', onClick: fnDelete, confirm: false }
    ];

    formConfig.dataOptions = formConfig.dataOptions || {};
    formConfig.dataOptions.out = formConfig.dataOptions.out || {};
    formConfig.dataOptions.out.includeLabel = true;

    formConfig.bar = barButtonconfig;
    gridConfig.fields.push(...actions);
    gridConfig.barHeader = showSave ? gridBarHeaderButtonconfig : null;
}

export const configureFormAddEdit = (config, formConfig, gridConfig, getForm, getGrid, getBar, getApi, showSave) => {
    const modeNew = () => {
        getForm().reset();
        getBar().hide(['update']);
        getBar().show(['add', 'cancel']);
    }
    const modeEdit = () => {
        //getForm().reset();
        getBar().hide(['add']);
        getBar().show(['update', 'cancel']);
    }

    const fnAddOk = (values) => {
        //console.log('fnAddOk', values);
        getGrid().addItem(getForm().getData(), true);
        modeNew();
    }

    const fnAdd = () => {
        getForm().validate(fnAddOk);
    }

    const fnUpdateOk = (values) => {
        //console.log('fnUpdateOk', values);
        getGrid().updateItem(getForm().getData());
        modeNew();
    }

    const fnUpdate = () => {
        getForm().validate(fnUpdateOk);
    }

    const fnCancel = () => {
        modeNew();
    }

    const fnEdit = (value, row, index) => {
        modeEdit();
        getForm().setData(row);
    }

    const fnDelete = (value, row, index) => {
        getGrid().deleteItem(row.key);
        modeNew();
    }
    /*
    const fnNew = () => {
        modeNew();
    }*/

    const fnSave = () => {
        getGrid().save();
    }

    const barButtonconfig = {
        fields: [
            { ...BUTTON_DEFAULT.ADD, onClick: fnAdd },
            { ...BUTTON_DEFAULT.UPDATE, onClick: fnUpdate, visible: false },
            { ...BUTTON_DEFAULT.CANCEL, onClick: fnCancel }
        ]
    }

    const gridBarHeaderButtonconfig = {
        fields: [
            { ...BUTTON_DEFAULT.SAVE, type: null, onClick: fnSave }
            //{...BUTTON_DEFAULT.NEW, onClick: fnNew}
        ]
    }

    const actions = [];

    let viewCol = gridConfig.fields.filter((field, index) => field.name === COLUMN_DEFAULT.VIEW.name);
    if (viewCol && viewCol.length > 0) {
        viewCol[0].onClick = fnEdit;
    }
    else {
        actions.push({ ...COLUMN_DEFAULT.VIEW, onClick: fnEdit });
    }

    let editCol = gridConfig.fields.filter((field, index) => field.name === COLUMN_DEFAULT.EDIT.name);
    if (editCol && editCol.length > 0) {
        editCol[0].onClick = fnEdit;
    }
    else {
        actions.push({ ...COLUMN_DEFAULT.EDIT, onClick: fnEdit });
    }

    let deleteCol = gridConfig.fields.filter((field, index) => field.name === COLUMN_DEFAULT.DELETE.name);
    if (deleteCol && deleteCol.length > 0) {
        deleteCol[0].onClick = fnDelete;
    }
    else {
        actions.push({ ...COLUMN_DEFAULT.DELETE, onClick: fnDelete, confirm: false });
    }

    formConfig.dataOptions = formConfig.dataOptions || {};
    formConfig.dataOptions.out = formConfig.dataOptions.out || {};
    formConfig.dataOptions.out.includeLabel = true;
    formConfig.bar = barButtonconfig;
    gridConfig.fields.push(...actions);
    gridConfig.barHeader = showSave ? gridBarHeaderButtonconfig : null;
}
/*
const mixinField = (,) => {
    let field = fields.filter((field, index) => field.name === COLUMN_DEFAULT.VIEW.name);
    if (view && view.length>0){
        view[0].onClick = fnEdit;
    }
    else{
        actions.push({...COLUMN_DEFAULT.VIEW, onClick: fnEdit});
    }
}*/

export const configureFormSearch = (config, formConfig, gridConfig, getForm, getGrid, getBar, getApi) => {
    const barButtonconfig = {
        fields: [
            { ...BUTTON_DEFAULT.SEARCH, onClick: () => getGrid().load({ params: getForm().getData() }) },
            { ...BUTTON_DEFAULT.CLEAR, onClick: () => getForm().reset() }
        ]
    }
    formConfig.bar = barButtonconfig;
}

export const configureFormSearchAdd = (config, formConfig, gridConfig, getForm, getGrid, getBar, getApi) => {

    const fnAdd = () => {
        if (config.afterAddEvent)
            config.afterAddEvent(getGrid().getSelectedItem());
    }
    const barButtonconfig = {
        fields: [
            { ...BUTTON_DEFAULT.SEARCH, onClick: () => getGrid().load({ params: getForm().getData() }) },
            { ...BUTTON_DEFAULT.CLEAR, onClick: () => getForm().reset() }
        ]
    }
    const barGridButtonconfig = {
        fields: [
            { ...BUTTON_DEFAULT.ADD, onClick: fnAdd }
        ]
    }
    formConfig.bar = barButtonconfig;
    gridConfig.bar = barGridButtonconfig;
}
