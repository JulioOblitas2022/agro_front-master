
import './FormGridFw.css';
import React from 'react'
import FormFw from '../form/FormFw';
import GridFw from '../grid/GridFw';
import { FormGridApi } from './FormGridApi';
import HeaderBarFw from '../header-bar/HeaderBarFw';
import { configureFormAddEdit, configureFormOnlyAdd, configureFormSearch, configureFormSearchAdd } from './getFormGridFwMode';

export const FORM_GRID_MODE = {
    FORM_ADD_EDIT_SAVE: 'FORM_ADD_EDIT_SAVE',
    FORM_ADD_SAVE: 'FORM_ADD_SAVE',
    FORM_ADD_EDIT: 'FORM_ADD_EDIT',
    FORM_ADD: 'FORM_ADD',
    FORM_SEARCH: 'FORM_SEARCH',
    FORM_SEARCH_ADD: 'FORM_SEARCH_ADD'
};

const createHeader = (name, config, getParentApi, isCollapsed, setIsCollapsed) => (
    <HeaderBarFw
        name={name + '.headerBar'}
        config={{
            title: config.title,
            button: config.barHeader,
            collapsible: config.collapsible
        }}
        getParentApi={getParentApi}
        setIsCollapsed={setIsCollapsed}
        isCollapsed={isCollapsed}
    />
)


const FormGridFw = ({ name, getParentApi, config }) => {
    const [isCollapsed, setIsCollapsed] = React.useState(false);
    const apiRef = React.useRef(new FormGridApi(name, config, null));
    let { form: formConfig, grid: gridConfig } = config || {};

    const hasBarHeader = () => config.title || config.barHeader;
    const getGrid = () => apiRef.current.getComponent(`${name}.grid`);
    //const getGridBar = () => apiRef.current.getComponent(`${name}.grid.bar`);
    const getForm = () => apiRef.current.getComponent(`${name}.form`);
    const getBar = () => apiRef.current.getComponent(`${name}.form.bar`);
    const getApi = () => apiRef.current;

    if (!config.mode || config.mode === FORM_GRID_MODE.FORM_ADD_EDIT_SAVE)
        configureFormAddEdit(config, formConfig, gridConfig, getForm, getGrid, getBar, getApi, true);
    else if (!config.mode || config.mode === FORM_GRID_MODE.FORM_ADD_SAVE)
        configureFormOnlyAdd(config, formConfig, gridConfig, getForm, getGrid, getBar, getApi, true);
    else if (!config.mode || config.mode === FORM_GRID_MODE.FORM_ADD_EDIT)
        configureFormAddEdit(config, formConfig, gridConfig, getForm, getGrid, getBar, getApi, false);
    else if (!config.mode || config.mode === FORM_GRID_MODE.FORM_ADD)
        configureFormOnlyAdd(config, formConfig, gridConfig, getForm, getGrid, getBar, getApi, false);
    else if (!config.mode || config.mode === FORM_GRID_MODE.FORM_SEARCH)
        configureFormSearch(config, formConfig, gridConfig, getForm, getGrid, getBar, getApi);
    else if (!config.mode || config.mode === FORM_GRID_MODE.FORM_SEARCH_ADD)
        configureFormSearchAdd(config, formConfig, gridConfig, getForm, getGrid, getBar, getApi);

    React.useEffect(() => {
        //console.log(`Mounting => FormGridFw[${name}]`);//Mounting, that is putting inserting elements into the DOM.
        if (getParentApi)
            getParentApi().addComponent(name, apiRef.current);
        return () => {
            //console.log(`Unmounting => FormGridFw[${name}]`);
        }
    }, []);

    //console.log('render');
    return (
        <div className="form-grid-fw">
            {!hasBarHeader() || createHeader(name, config, getApi, isCollapsed, setIsCollapsed)}
            <div className={`form-grid-fw-body ${isCollapsed ? 'collapsed' : 'expanded'}`} >
                <FormFw
                    name={`${name}.form`}
                    getParentApi={getApi}
                    config={formConfig}
                />
                <GridFw
                    name={`${name}.grid`}
                    getParentApi={getApi}
                    config={gridConfig}
                />
            </div>
        </div>
    )
}
export default FormGridFw;