import React from 'react'
import { Popconfirm } from 'antd';

const ConfirmFw = ({config, children}) => {
  const handleOk = () => {
      if (config.onOkClick)
        config.onOkClick();
  };

  const handleCancel = (e) => {
    e.preventDefault();
  };

  return (
    <>
      <Popconfirm
        title={ config.message || "Aviso" }
        //visible={visible}
        onConfirm={handleOk}
        //okButtonProps={{ loading: confirmLoading }}
        onCancel={handleCancel}
      >
          {children}
      </Popconfirm>
    </>
  );
}
export default ConfirmFw;