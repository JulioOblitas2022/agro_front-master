
export class MasterDetailApi {
    config = null;
    history = null;
    component = null;
    mapChild = {};

    constructor(component, config, history) {
        this.component = component;
        this.config = config;
        this.history = history;
    }

    addComponent(name, ref){
        this.mapChild[name] = ref;
    }

    getComponent = name => {
        return this.mapChild[name];
    }

    setMapChild(mapChild){
        this.mapChild = mapChild;
    }
}

