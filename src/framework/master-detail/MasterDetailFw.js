import React from 'react'
import BarButtonFw, { BUTTON_DEFAULT } from '../bar-button/BarButtonFw';
import FormFw from '../form/FormFw';
import GridFw, { COLUMN_DEFAULT } from '../grid/GridFw';
import { MasterDetailApi } from './MasterDetailApi';

const MasterDetailFw = ({name, config}) => {
    const apiRef = React.useRef(null);
    let mapChild = {};
    let {form: formConfig, grid: gridConfig} = config || {};


    const getGrid = () => apiRef.current.getComponent('grid');
    const getForm = () => apiRef.current.getComponent('form');
    const getBar =  () => apiRef.current.getComponent('bar');


    const modeNew = () => {
        getForm().reset();
        getBar().hide(['update']);
        getBar().show(['add', 'cancel']);
    }
    const modeEdit = () => {
        getForm().reset();
        getBar().hide(['add']);
        getBar().show(['update', 'cancel']);
    }


    const fnAdd = () => {
        const fnOk = (values) => {
            getGrid().addItem(getForm().getData());
            modeNew();
        }
        getForm().validate(fnOk);
    }

    const fnUpdate = () => {
        const fnOk = (values) => {
            getGrid().updateItem(getForm().getData());
            modeNew();
        }
        getForm().validate(fnOk);
    }

    const fnCancel = () => {
        modeNew();
    }

    const fnEdit = (value, row, index) => {
        modeEdit();
        getForm().setData(row);
    }

    const fnDelete = (value, row, index) => {
        getGrid().deleteItem(row.key);
        modeNew();
    }

    const fnNew = () => {
        modeNew();
    }

    const barButtonconfig = {
        fields:[
            {...BUTTON_DEFAULT.ADD, onClick: fnAdd },
            {...BUTTON_DEFAULT.UPDATE, onClick: fnUpdate, visible: false },
            {...BUTTON_DEFAULT.CANCEL, onClick: fnCancel }
        ]
    }

    const gridBarHeaderButtonconfig = {
        fields:[
            {...BUTTON_DEFAULT.NEW, onClick: fnNew}
        ]
    }

    const actions = [{...COLUMN_DEFAULT.VIEW, onClick: fnEdit},
        {...COLUMN_DEFAULT.EDIT, onClick: fnEdit },
        {...COLUMN_DEFAULT.DELETE, onClick: fnDelete, confirm: false}
    ];
    gridConfig.fields.push(...actions);

    gridConfig.barHeader = gridBarHeaderButtonconfig;


    const handleAddComponent = (name, ref) => {
        //apiRef.current.addComponent(name, ref);
        mapChild[name] = ref;
        
    }

    React.useEffect(() => {
        const api = new MasterDetailApi(null, config, null);
        console.log('agregando',mapChild);
        api.setMapChild(mapChild);
        console.log(`Mounting => MasterDetailFw[${name}]`);//Mounting, that is putting inserting elements into the DOM.
        apiRef.current = api;
        return () => {
            console.log(`Unmounting => MasterDetailFw[${name}]`);
        }
    }, []); 

    return (
        <>
            <FormFw
                name="form"
                handleAddComponent={handleAddComponent}
                config={formConfig}
            />
            <BarButtonFw  
                name="bar"
                //geParentApi={pageApi.geParentApi}
                handleAddComponent={handleAddComponent}
                config={barButtonconfig}
            />
            <GridFw 
                name="grid"
                handleAddComponent={handleAddComponent}
                config={gridConfig} 
            />
        </>
    )
}
export default MasterDetailFw;