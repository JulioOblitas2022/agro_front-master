
export class ButtonApi {
    config = null;
    state = null; 
    setState = null;
    constructor(config, state, setState) {
        this.config = config;
        this.state = state;
        this.setState = setState;
    }
    show(){
        this.setState({visible:true});
    }
    hide(){
        this.setState({visible:false});
    }
}

