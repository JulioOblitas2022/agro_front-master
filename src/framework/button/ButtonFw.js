import './ButtonFw.css';
import React from 'react'
import { Tooltip } from 'antd';
import {useHistory} from 'react-router-dom';
import ConfirmFw from '../confirm/ConfirmFw';
import { RequestUtil } from '../../util/RequestUtil';
import { ButtonApi } from './ButtonApi';
//import Button from "antd-button-color";
// import 'antd/dist/antd.css'; // or 'antd/dist/antd.less'
// import 'antd-button-color/dist/css/style.css';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';

const createLabel = (config) => {
    return (config['label'] || '');
};

const createButton = (config, fnOnClick, loadingState) => {
    if (config.subType === 'icon'){
        return (
            // <Tooltip placement="left" title={config.title || ''} color={config.color || 'black'}> 
            //     <span className="button-fw-icon" style={{color: config.color, textAlign: 'center'}} onClick={fnOnClick}>{config.icon}</span>
                <IconButton color="primary" aria-label={config.title || ''} size="small" onClick={fnOnClick}>
                    {config.icon}
                </IconButton>
            // </Tooltip>
        );
    }
    else
        return (
        <Button 
            variant="contained" size="small"
            startIcon={config.icon}
            // icon={config.icon} 
            onClick={fnOnClick} 
            //color={config.type}
            disabled={config.readOnly}
            // loading={loadingState}
            //href={config.link}
        >
            {createLabel(config)}
        </Button>
    )
}

const configureProcess = (config) => {
    const process = config['process'];
    config['onClick']  = () => {
        console.log('entrando');
        const params = typeof process.filter === 'function' ?  process.filter() : (process.filter || {});
        RequestUtil.postData(process.url, params)
        .then( result => {
            if (process.fnOk){
                process.fnOk(result, params);
            }
        })
        .catch(error=>{
            
        })

    }
}

const configureLink = (config,history) => {
    const link = config['link'];
    if (typeof link === 'function')
        config['onClick']  = () => history.push(link());
    else if (link){
        config['onClick']  = () => history.push(link);
    }
}

const ButtonFw = ({name, config}) => {
    
    const history = useHistory();
    const { confirm, link, process, visible} = config;
    const [state, setState] = React.useState({visible: (visible === undefined)? true : visible });
    const [loadingState, setLoadingState] = React.useState(false);
    const apiRef = React.useRef(new ButtonApi(config, state, setState));




    /* resolvemos cuando es un button link */
    if (link)
        configureLink(config, history);
    if (process)
        configureProcess(config);


    const onClick = config.onClick;
    const  fnOnClick = !onClick ? null : async() => {
        if (onClick){
            setLoadingState(true);
            await onClick();
            setLoadingState(false);
        }
            
    } 

    let html = null;
    if (confirm){
        const confirmConfig = {
            message: confirm.message,
            onOkClick: fnOnClick
        };
        config['onClick'] = null;
        html = (
            <ConfirmFw 
                config={confirmConfig}
            >
                {createButton(config , null, loadingState)}
            </ConfirmFw>
            );
    }
    else{
        html = createButton(config, fnOnClick, loadingState);
    }

    React.useEffect(() => {
        if (config.handleAddField){
            config.handleAddField(config.name, apiRef.current);
        } 
        return () => {
            //console.log('Combo desmontado');
        }
    }, [])

    if (state.visible)
        return (html);
    else
        return <></>;
    
}

export default ButtonFw;