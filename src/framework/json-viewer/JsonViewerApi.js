export class JsonViewerApi {
    data = null;
    state = null; 
    setState = null;
    getData = () => {
        return this.data || {data:'No hay Data'};
    }
    setData = (data) => {
        this.data = data;
        this.setState(data);
    }
}