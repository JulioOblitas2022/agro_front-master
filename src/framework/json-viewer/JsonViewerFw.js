import './JsonViewerFw.css';
import React from 'react'
import ReactJson from 'react-json-view'
import { JsonViewerApi } from './JsonViewerApi';

const JsonViewerFw = ({name, config, getParentApi}) => {
    const [state, setState] = React.useState();
    const apiRef = React.useRef(new JsonViewerApi());
    const getApi = () => apiRef.current;
    getApi().state = state;
    getApi().setState = setState;
    //const data = config?.data || {data:'No hay Data'};

    React.useEffect(() => {
        if (getParentApi)
            getParentApi().addComponent(name, getApi());
        return () => {
            apiRef.current = null;
            console.log(`JsonViewerFw[${name}].Unmounting`);
        }
    }, []); 
    return (
        <div className="json-viewer-fw" >
            <ReactJson src={getApi().getData()} />
        </div>
    )
}
export default JsonViewerFw;