import './HeaderBarFw.css';
import { Col, Row } from 'antd';
import React from 'react'
import BarButtonFw from '../bar-button/BarButtonFw';
import { DownOutlined, RightOutlined } from '@ant-design/icons';


const createButtonBar = (name, config, getParentApi) => {
    if (config)
        return (
            <Col span={12} className="header-bar-fw-button" >
                <Row justify="end">
                    <Col>
                        <BarButtonFw  
                            name={name}
                            getParentApi={getParentApi}
                            config={config}
                        />
                    </Col>
                </Row>
            </Col> 
        )
    else
        return null;
}

const createCollapsibleIcon = (config, isCollapsed) => {
    let icon = null;
    if (isCollapsed)
        icon = <RightOutlined />
    else
        icon = <DownOutlined />
    return config.collapsible===true ? (<span className="header-bar-fw-collapsible-icon" >{icon}</span>) : null;
}

const createIcon = (config) => {
    return config.icon ? <i className={`header-bar-fw-icon ${config.icon}`}></i> : null;
}

const HeaderBarFw = ({name, getParentApi, config, isCollapsed, setIsCollapsed}) => {
    const handleClick = () => {
        if (config.collapsible===true)
            setIsCollapsed(prev=>!prev);
    }

    if (config.title || config.button)
        return (
            <Row className="header-bar-fw">
                <Col span={12} className="header-bar-fw-title" onClick={handleClick} >
                    {createIcon(config)}
                    {createCollapsibleIcon(config, isCollapsed)}
                    {config.title}
                </Col>
                {isCollapsed || createButtonBar(name, config.button, getParentApi)}
            </Row>
        );
    else
        return null; 
}
export default HeaderBarFw;
