import { message } from 'antd';
import { userConstants } from '../constants/userConstants';
import { EnvConstants } from "../EnvConstants";
// import { NotificationUtil } from "../util/NotificationUtil";
import { RequestUtil } from "../util/RequestUtil";
import { StorageUtil } from '../util/StorageUtil';
import { UserUtil } from '../util/UserUtil';

export const startLogin = (params) => {
    return (dispatch) => {
        //
        dispatch(auth.startLoading());
        //
        const url = EnvConstants.GET_FW_FORM_URL_LOAD_BASE();
        RequestUtil.postData(url, {queryId: 2, params})
        .then(async (r) => {
            if (r.dataObject) {
                const user = r.dataObject;
                UserUtil.createToken(user);
                const ACCESS_TOKEN = StorageUtil.getItem('ACCESS_TOKEN');
                if (ACCESS_TOKEN) {
                    layout.bar(user);
                    await layout.menus();
                    dispatch(auth.login(ACCESS_TOKEN));
                    console.log('[LOGIN]');
                    //
                    dispatch(auth.finishLoading());
                }
			} else {
                dispatch(auth.finishLoading());
                message.error({ content: 'User not found' });
            }
		})
		.catch(error => {
            console.log('ERROR [LOGIN] => Try again', error);
            message.error({ content: 'ERROR [LOGIN] => Try again' });
            // NotificationUtil.error('ERROR => Try again');
            dispatch(auth.finishLoading());
        });

    }
}

const auth  = {
    logout: () => ({ type: userConstants.LOGOUT }),
    login: (ACCESS_TOKEN) => ({ type: userConstants.LOGIN, payload: ACCESS_TOKEN }),
    startLoading: () => ({ type: userConstants.START_LOADING}),
    finishLoading: () => ({ type: userConstants.FINISH_LOADING })
}

export const checkLoggedIn = () => {
    return (dispatch) => {
        const ACCESS_TOKEN = StorageUtil.getItem('ACCESS_TOKEN');
        if (ACCESS_TOKEN) {
            dispatch(auth.login(ACCESS_TOKEN));
        } else {
            dispatch(auth.logout());
        }
    }
}

export const startLogout = () => {
    return (dispatch) => {
        StorageUtil.removeItem('ACCESS_TOKEN');
        StorageUtil.removeItem('USER_BAR');
        StorageUtil.removeItem('USER_MENU');
        dispatch(auth.logout());
    }
}

const layout = {
    bar: (user) => {
        if (StorageUtil.existItem('ACCESS_TOKEN')) {
            StorageUtil.setItemObject('USER_BAR', {
                title: user.name +' '+ user.lastName || 'Account',
                fields: [
                    { label: 'Settings', icon: 'icon-settings' },
                    { label: 'Logout', icon: 'icon-logout', onclick: 'startLogout()' }
                ]
            });
        }
    },
    menus: async() => {
        if (StorageUtil.existItem('ACCESS_TOKEN') && UserUtil.getUserId()) {
            const userId = UserUtil.getUserId();
            await RequestUtil.postData(
                EnvConstants.GET_FW_FORM_URL_LOAD_BASE(),
                {queryId: 10, params: {userId}})
                .then(r => {
                    if (r.dataList && r.dataList.length > 0) {
                        const menus = layout.fnTransform(r.dataList);
                        StorageUtil.setItemObject('USER_MENU', {
                            title: 'Account',
                            fields: menus,
                        });
                    }
                })
                .catch(error => {
                    console.log('ERROR [MENU] => [LOGIN] again', error);
                    // NotificationUtil.error(error);
                });
        }
    },
    fnTransform: (menus) => {
        if (menus.length > 0) {
            const menuParents = menus.filter((menu, index) => menu.type === 1);
            menuParents.forEach(element => {
                let tempChildren = menus.filter((child, idx) => child.parentId === element.routeId);
                // order - children
                tempChildren.sort((a, b) => a.numberOrder - b.numberOrder);
                if (tempChildren.length > 0) {
                    element['fields'] = tempChildren;
                }
            });
            const menuResult = menuParents.filter(e => (!e.parentId || e.parentId === '0') && e.fields?.length > 0);
            // order - parents
            menuResult.sort((a, b) => a.numberOrder - b.numberOrder);
            return menuResult || [];
        }
        return [];
    }
}
