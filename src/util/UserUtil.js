import { StorageUtil } from "./StorageUtil";

export class UserUtil{
    static getUserId(){
        if(StorageUtil.existItem('ACCESS_TOKEN')) {
            const token = StorageUtil.getItem('ACCESS_TOKEN');
            const _array = token.split('[SSF]');
            const idTemp = _array[0].slice(1, -1);
            return idTemp.replace(/TOKEN/g, '-');
        } else {
            return null;
        }
    }
    static createToken(user){
        const token = '[' + user.userId.replace(/-/g, 'TOKEN') + '][SSF][' + user.username.toUpperCase() + ']';
        StorageUtil.setItem('ACCESS_TOKEN', token);
    }
}