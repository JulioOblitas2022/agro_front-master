import moment from 'moment';
import { DATE_FORMAT } from '../constants/constants';

export class DateUtil {
    static stringToMoment = (str, format) => {
        return str ? moment(str, format) : null;
    }
    static stringToDate = (str, format) => {
        let m = DateUtil.stringToMoment(str, format);
        return m ? m.toDate() : null;
    }
    static stringDDMMYYYToDate = (str) => DateUtil.stringToDate(str, DATE_FORMAT.DDMMYYYY);
    static stringYYYYMMDDToDate = (str) => DateUtil.stringToDate(str, DATE_FORMAT.YYYYMMDD);
    static stringDDMMYYY_HHmmToDate = (str) => DateUtil.stringToDate(str, DATE_FORMAT.DDMMYYYY_HHmm);
    static stringYYYYMMDD_HHmmToDate = (str) => DateUtil.stringToDate(str, DATE_FORMAT.YYYYMMDD_HHmm);
    static stringDDMMYYY_HHmmssToDate = (str) => DateUtil.stringToDate(str, DATE_FORMAT.DDMMYYYY_HHmmss);
    static stringYYYYMMDD_HHmmssToDate = (str) => DateUtil.stringToDate(str, DATE_FORMAT.YYYYMMDD_HHmmss);

    static stringAdd = (str, format, years, months, days, hours, minutes, seconds) => {
        let m = DateUtil.stringToMoment(str, format);
        return m ? m.add({ years, months, days, hours, minutes, seconds }).format(format) : null;
    };
    static stringAddSeconds = (str, format, seconds) => {
        return DateUtil.stringAdd(str, format, 0, 0, 0, 0, 0, seconds);
    };
    static stringAddMinutes = (str, format, minutes) => {
        return DateUtil.stringAdd(str, format, 0, 0, 0, 0, minutes);
    };
    static stringAddHours = (str, format, hours) => {
        return DateUtil.stringAdd(str, format, 0, 0, 0, hours);
    };
    static stringAddDays = (str, format, days) => {
        let m = DateUtil.stringToMoment(str, format);
        return m ? m.add('days', days).format(format) : null;
    };
    static stringAddMonths = (str, format, months) => {
        let m = DateUtil.stringToMoment(str, format);
        return m ? m.add('months', months).format(format) : null;
    };
    static stringAddYears = (str, format, years) => {
        let m = DateUtil.stringToMoment(str, format);
        return m ? m.add('years', years).format(format) : null;
    };

    static getDate = moment().format('YYYY/MM/DD');
    static getTime = moment().format("HH:mm:ss");

    static startOfMonth = moment().startOf('month').format('YYYY/MM/DD');
    static endOfMonth = moment().endOf('month').format('YYYY/MM/DD');
}
