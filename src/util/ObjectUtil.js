export class ObjectUtil {
  static deepClone = (obj) => JSON.parse(JSON.stringify(obj));
  static merge = (destination, origin) => Object.assign(destination || {}, origin || {});
  static deepEqual = (object1, object2) => {
      const keys1 = Object.keys(object1);
      const keys2 = Object.keys(object2);
      if (keys1.length !== keys2.length) {
        return false;
      }
      for (const key of keys1) {
        const val1 = object1[key];
        const val2 = object2[key];
        const areObjects = ObjectUtil.isObject(val1) && ObjectUtil.isObject(val2);
        if ((areObjects && !ObjectUtil.deepEqual(val1, val2)) || (!areObjects && val1 !== val2)) {
          return false;
        }
      }
      return true;
  }
  static isObject = (object) => object != null && typeof object === 'object';
  static getObjFromFuncOrObj = (val) => (typeof val === 'function' ? val() : val);
  static getChangedValues =  (newAllValues, oldAllValues) => {
    let changedValues = {};
    Object.keys(oldAllValues).forEach(field => {
        if (oldAllValues[field] !== newAllValues[field])
          changedValues[field] = newAllValues[field];
    });

    Object.keys(newAllValues).forEach(field => {
      if (!oldAllValues[field] && newAllValues[field])
        changedValues[field] = newAllValues[field];
  });
    return changedValues;
  }
}

