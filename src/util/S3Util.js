import { EnvConstants } from "../EnvConstants";
import { RequestUtil } from "./RequestUtil";
import { DonwloadUtil } from "./DonwloadUtil";

export class S3Util {

    static getTemporalPutUrl(config){
        const { folder, fileName, fnOk, fnError} = config;
        RequestUtil.postData(EnvConstants.GET_FW_URL_UPLOAD_PRESIGNED_URL_PUT(), {
            params: {
                folder: folder,
                fileName: fileName
            }
        })
        .then( resp => {
            if (fnOk)
                fnOk(resp);
        })
        .catch(error=>{
            if (fnError)
                fnError(error);
        });
    }

    static getTemporalGetUrl(config){
        const { folder, fileName, fnOk, fnError} = config;
        RequestUtil.postData(EnvConstants.GET_FW_URL_UPLOAD_PRESIGNED_URL_GET(), {
            params: {
                folder: folder,
                fileName: fileName
            }
        })
        .then( resp => {
            if (fnOk)
                fnOk(resp);
        })
        .catch(error=>{
            if (fnError)
                fnError(error);
        });
    }

    static async getTemporalGetUrlAsync(fileName, folder){
        let resp = await RequestUtil.postData(EnvConstants.GET_FW_URL_UPLOAD_PRESIGNED_URL_GET(), {
            params: {
                folder: folder,
                fileName: fileName
            }
        });
        return resp.dataObject.url;
    }


    static async donwload(fileName, folder){
        let url = await S3Util.getTemporalGetUrlAsync(fileName,folder);
        //console.log(fileName, folder, url);
        DonwloadUtil.donwloadGet(url, fileName);
    }

    

    /*
    static downloadS3 = async(fileName, folder) => {
        S3Util.getTemporalGetUrl({
            folder: folder,
            fileName: fileName,
            fnOk: (resp) => {
                console.log(resp);
                if (resp?.dataObject?.url)
                    DonwloadUtil.donwloadGet(resp.dataObject.url, fileName);
            },
            error: (error) => {
                console.error(error);
            }
        });
    }*/

}

