import { EnvConstants } from "../EnvConstants";
import { RequestUtil } from "./RequestUtil";
import { UrlUtil } from "./UrlUtil";
import { WindowUtil } from "./WindowUtil";
import { DonwloadUtil } from "./DonwloadUtil";

export class ReportUtil {
    static openWindowHtml(reporte, filtro) {
        WindowUtil.openWindow(UrlUtil.createUrlWithObject(this.getUrl(reporte,'html'), filtro));
    }
    static donwloadPdf(reporte, filtro) {
        WindowUtil.openWindow(UrlUtil.createUrlWithObject(this.getUrl(reporte,'pdf'), filtro));
    }
    static downloadXls(reporte, filtro) {
        WindowUtil.openWindow(UrlUtil.createUrlWithObject(this.getUrl(reporte,'xls'), filtro));
    }
    static getUrl(reporte,extension){
        return `${EnvConstants.GET_REPORT_FULL_FOLDER()}/${reporte}.${extension}`;
    }
    static login(){
        let auth = EnvConstants.GET_REPORT_AUTH();
        let userPass = null;
        if (auth)
            userPass = atob(EnvConstants.GET_REPORT_AUTH());
        if (userPass){
            let pair = userPass.split(':');
            RequestUtil.getData(EnvConstants.GET_REPORT_URL()+'/login',{j_username: pair[0], j_password: pair[1]})
        }
    }

    static donwloadFile(reporte,extension) {
        //fetch(EnvConstants.GET_REPORT_URL() + '/reports/reports/fw/' + reporte + '.' + extension, {
        fetch('http://35.171.164.24/jasperserver/rest_v2/reports/reports/fw/test1.html?j_username=fw&j_password=12345678', {
            method: 'GET',
            mode: 'cors',
            headers: {
                'Content-Type': 'application/pdf',
                'Authorization': 'Basic Znc6MTIzNDU2Nzg='
            },
        })
        .then((response) => response.blob())
        .then((blob) => {
            // Create blob link to download
            const url = window.URL.createObjectURL(
                new Blob([blob]),
            );
            const link = document.createElement('a');
            link.href = url;
            link.setAttribute(
                'download',
                `FileName.pdf`,
            );
        
            // Append to html link element page
            document.body.appendChild(link);
        
            // Start download
            link.click();
        
            // Clean up and remove the link
            link.parentNode.removeChild(link);
      })
    }


    static async donwloadFromPostExcel(url,reqParams, fileName){
        await DonwloadUtil.donwloadPost(url,reqParams, fileName);
    }

    static async donwloadFromPost(url,reqParams, fileName){
        await DonwloadUtil.donwloadPost(url,reqParams, fileName);
    }
}