// import { notification } from 'antd';
import Swal from 'sweetalert2'

export class NotificationUtil {
    static error = (msg) => {
        // notification['error']({
        //     message: 'Error',
        //     description: msg,
        // });
        Swal.fire(
            {
                title: 'Error',
                text: msg,
                icon: 'error',
                showConfirmButton: false,
                timer: 2000,
                timerProgressBar: true,
                // toast: true
            }
        );
    };

    static success = (msg) => {
        // notification['success']({
        //     message: 'Exito',
        //     description: msg,
        // });
        Swal.fire(
            {
                title: 'Exito',
                text: msg,
                icon: 'success',
                showConfirmButton: false,
                timer: 2000,
                timerProgressBar: true,
                // toast: true
            }
        );
    };
}


