// import { message, notification } from "antd";
import Swal from 'sweetalert2'

export const MessageUtil = (type, title, msg) => {

    // notification[type]({
    //     message: title,
    //     description: msg,
    //     //placement: 'bottomRight' // posisiones
    // });

    /** https://sweetalert2.github.io/#examples
     * 
     */

        Swal.fire(
        {
            title,
            text: msg,
            icon: type,
            showConfirmButton: false,
            timer: 2000,
            timerProgressBar: true,
            // toast: true
        }
    );
}