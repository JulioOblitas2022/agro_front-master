export class LoadingUtil{

    static show(){
        let load = document.querySelector('#loader');
        load.style.display= "block";
    }

    static hide(){
        let load = document.querySelector('#loader');
        load.style.display= "none";
    }
}