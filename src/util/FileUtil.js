import moment from 'moment';
import * as fs from 'file-saver';
import { MIME_TYPE } from '../constants/constants';

export class FileUtil {
    static generateFileName_YYYYMMDD_HHmmss = (originalName) => {
        let ext = FileUtil.getExtension(originalName);
        return 'file_' +  moment(new Date()).format('YYYYMMDD_HHmmss') + (ext ? "." + ext : "");
    }
    static getExtension(fileName){
        let parts = fileName.split('.');
        return parts?.length > 0 ? parts[parts.length-1].toLowerCase() : "";
    }   
    static getMimeTypeFromFileName(fileName){
        let extension = FileUtil.getExtension(fileName);
        console.log(MIME_TYPE);
        let type = MIME_TYPE[extension];
        return type ? type : MIME_TYPE.default;
    }
    static saveBlob(fileName, blob){
        let blobTemp = new Blob([blob],{ type: FileUtil.getMimeTypeFromFileName(fileName)});
        fs.saveAs(blobTemp, fileName);
    }
    static saveUrl(url, fileName){
        fs.saveAs(url, fileName);
    }
}


