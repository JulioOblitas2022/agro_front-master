import { FileUtil } from './FileUtil';
import { NotificationUtil } from "./NotificationUtil";

export class DonwloadUtil {

    static async donwloadPost(url, reqParams, fileName){
        const response = await fetch(url, {
            method: "POST", 
            body: JSON.stringify(reqParams), 
            headers: { 
                "Content-type": "application/json; charset=UTF-8"
            } 
        })
        .catch(err=>{
            console.error(`Error descargando ${fileName} => ${err}`);
        });

        if (response?.ok) {
            const data = await response.blob();
            return FileUtil.saveBlob(fileName, data);
        }
        let err = `Error descargando ${fileName}`;
        try {
            const respError = await response.json();
            err = respError?.message || err;
            console.error(err);
        } catch (error) {
            console.error(response);
        }
        NotificationUtil.error(err);
    }

    static async donwloadGet(url, fileName){
        const response = await fetch(url, {
            method: "GET"
        })
        .catch(err=>{
            console.error(`Error descargando ${fileName} => ${err}`);
        });
        if (response?.ok) {
            const data = await response.blob();
            //console.log('fileName',fileName);
            //console.log(data);
            return FileUtil.saveBlob(fileName, data);
        }
        let err = `Error descargando ${fileName}`;
        NotificationUtil.error(err);
        console.error(response);
    }

    static async donwloadGet2(url, fileName){
        FileUtil.saveUrl(url, fileName);
    }

}
