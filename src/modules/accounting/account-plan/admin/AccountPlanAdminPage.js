import React from 'react'
import BarButtonFw, { BUTTON_DEFAULT } from '../../../../framework/bar-button/BarButtonFw';
import FormFw from '../../../../framework/form/FormFw';
import GridFw from '../../../../framework/grid/GridFw';
import { BasePage } from '../../../../framework/pages/BasePage';
import { COLUMN_DEFAULT } from "../../../../framework/grid/GridFw";
import { getFormDefaultParams } from "../../../shared/util/getFormDefaultParams";
import { getGridServerDefaultParams } from "../../../shared/util/getGridServerDefaultParams";
import { MessageUtil } from '../../../../util/MessageUtil';

export class AccountPlanAdminPage extends BasePage {

    createPagePropertiesBase(pageApi) {
        return {
            title: 'PLAN DE CUENTAS',
            barHeader: {
                fields:[
                    {...BUTTON_DEFAULT.NEW, label:'Nuevo', link: '/accounting/account-plan/account-plan/new'}
                ]
            }
        };
    }

    renderPage(pageApi) {
        const getGrid = () => pageApi.getComponent('grid');
        const getForm = () => pageApi.getComponent('form');

        const form = {
            ...getFormDefaultParams(),
            title: 'Filtros de búsqueda',
            fields:[
                { type: 'select', name: 'companyId', label: 'Empresa', column: 4, load: { queryId: 554 } , search: true},
                {name:'accountNumber', label:'Numero de cuenta', column: 4},
                {name:'description', label:'Descripcion', column: 4},
                // {type: 'date', name:'fromDate', label:'Fecha Desde', column: 2},
                // {type: 'date', name:'untilDate', label:'Fecha Hasta', column: 2},
            ]
        }

        const bar = { fields: [
            {...BUTTON_DEFAULT.SEARCH, onClick: ()=>{
                    if(getForm().getData().companyId === undefined){
                        return MessageUtil('warning', 'Alerta!', 'Debe Seleccionar Empresa!')
                    }
                    getGrid().load({params:getForm().getData()})
                }
            },
            {...BUTTON_DEFAULT.CLEAR, onClick: ()=>getForm().reset()}
        ]}

        const grid = {
            ...getGridServerDefaultParams({queryLoad: 414}),
            title: 'Resultados',
            fields:[
                {name: 'accountNumber', label: 'Nro. Cuenta',},
                {name: 'description', label: 'Descripcion',},
                {name: 'accountDebe', label: 'Dest. Debe',},
                {name: 'accountHaber', label: 'Dest. Haber',},
                // {name: 'saleAccountId', label: 'Divicionaria',},
                {...COLUMN_DEFAULT.VIEW, link: (value, row, index) => `/accounting/account-plan/account-plan/${row.accountPlanId}`, label: 'Opciones',colSpan: 3 },
                {...COLUMN_DEFAULT.EDIT, link: (value, row, index) => `/accounting/account-plan/account-plan/${row.accountPlanId}/update`,colSpan: 0 },
                {...COLUMN_DEFAULT.DELETE,colSpan: 0, process: {fnOk: (result)=>getGrid().load({params:getForm().getData()}),filter: (value, row, index) => ({queryId: 416, params: {accountPlanId: row.accountPlanId}}) }}
            ]
        }
        return (
            <>
                <FormFw
                    name="form"
                    getParentApi={pageApi.getApi}
                    config={form}
                />
                <BarButtonFw
                    name="bar"
                    getParentApi={pageApi.getApi}
                    config={bar}
                />
                <GridFw
                    name="grid"
                    getParentApi={pageApi.getApi}
                    config={grid}
                />
            </>
        )
    }
}