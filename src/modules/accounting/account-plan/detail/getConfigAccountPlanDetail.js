import { PAGE_MODE } from "../../../../constants/constants";
import { BUTTON_DEFAULT } from "../../../../framework/bar-button/BarButtonFw";
import { UserUtil } from "../../../../util/UserUtil";

export const getConfigAccountPlanDetail = (pageApi) => {
    const getForm = () => pageApi.getComponent('form');

    return {
        form: {
            load:{
                queryId: 415,
                params: {accountPlanId: pageApi.getParamId()},
            },
            update:{
                queryId: 413,
                postLink: (resp, values) => '/accounting/account-plan/account-plan-admin',
                params: {modifiedBy: UserUtil.getUserId()}
            },
            save:{
                queryId: 412,
                postLink: (resp, values) => '/accounting/account-plan/account-plan-admin',
                params: {createdBy: UserUtil.getUserId()}
            },
            title: 'DATOS PRINCIPALES',
            fields:[
                { type: 'select', name: 'companyId', label: 'Empresa', column: 4, load: { queryId: 554 } , search: true},
                { name:'accountNumber', label:'Nro.Cuenta', column: 4},
                { name:'description', label:'Descripcion', column: 4},
                { type:'select', name:'accountPlanDebeId', label:'Cta. Destino Debe ', column: 6, load:{queryId:411}, search: true, 
                    onChange: (value, values) => {
                        let accountPlanDebe = getForm().getFieldApi('accountPlanDebeId').getSelectedItem()?.account;
                        getForm().setFieldData('accountPlanDebe', accountPlanDebe);
                    }
                },
                { name:'accountPlanDebe', label:'Cta.', column: 6},
                { type:'select', name:'accountPlanHaberId', label:'Cta. Destino Haber', column: 6, load:{queryId:411}, search: true,
                    onChange: (value, values) => {
                        let accountPlanHaber = getForm().getFieldApi('accountPlanHaberId').getSelectedItem()?.account;
                        getForm().setFieldData('accountPlanHaber', accountPlanHaber);
                    }
                },
                { name:'accountPlanHaber', label:'Cta.', column: 6},
                { type:'select', name:'distributionId', label:'Distribución', column: 6, load:{queryId:410}},
                { type:'select', name:'distributionId_2', label:'2da Distribución', column: 6, load:{queryId:410}},

                { type:'radio', column:6, name:'accountNature', label:'NATURALEZA DE LA CUENTA',load:{dataList:[{value: 'D', label: 'Debe (D)'}, {value: 'H', label: 'Haber (H)'}]}},

                { type:'radio', name:'accountBalance', label:'CUENTA BALANCE DESTINADO A', column:6, load:{dataList:[{value: 0, label: 'Ninguno'},{value: 1, label: 'Activo'},{value: 2, label: 'Pasivo y Patrimonio'}]}},

                { type:'radio', label:'Documentar Cuenta', name: 'accountkeepRecord', load:{dataList:[{value: 1, label: 'Si'},{value:2, label:'No'}]}, column:6},
                { type:'select', name:'moduleId', label:'Módulo', column: 6, load:{queryId:409}},
            ],
            onComponentLoad : (formApi, pageApi) =>{
                if (pageApi.getPageMode() !== PAGE_MODE.NEW)
                    formApi.load();
            }
        },
        bar: {
            fields: [
                {...BUTTON_DEFAULT.SAVE, onClick: () => getForm().save()},
                {...BUTTON_DEFAULT.UPDATE, onClick: () => getForm().update()},
                {...BUTTON_DEFAULT.RETURN, link: '/accounting/account-plan/account-plan-admin'}
            ],
        },
        grid: {}
    }
}