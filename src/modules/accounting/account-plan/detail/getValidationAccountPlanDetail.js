
export const getValidationAccountPlanDetail = () => {
    return {
        form: {
            description: [ {required: true, message: 'Descripcion es requerida' } ],
            abbreviation: [ {required: true, message: 'Abrev. es requerida' } ],
            rate: [ {required: true, message: 'Tasa es requerida' } ],
            purchaseAccountId: [ {required: true, message: 'Cuenta Compra es requerida' } ],
            saleAccountId: [ {required: true, message: 'Cuenta Venta es requerida' } ],
        },
        grid: {

        },
        button: {

        },
        bar: {

        }
    }
}
