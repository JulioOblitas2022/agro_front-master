import React from 'react'
import BarButtonFw from '../../../../framework/bar-button/BarButtonFw';
import FormFw from '../../../../framework/form/FormFw';
import { BasePage } from '../../../../framework/pages/BasePage';
import { getConfigAccountPlanDetail } from './getConfigAccountPlanDetail';
import { getModeAccountPlanDetail } from './getModeAccountPlanDetail';
import { getValidationAccountPlanDetail } from './getValidationAccountPlanDetail';

export class AccountPlanDetailPage extends BasePage {
    
    createPageProperties = (pageApi) => {
        return {
            title: 'REGISTRO DE PLAN DE CUENTAS',
            validation: getValidationAccountPlanDetail,
            mode: getModeAccountPlanDetail
        };
    }

    renderPage(pageApi) {

        const config = getConfigAccountPlanDetail(pageApi);

        return (
            <>
                <FormFw
                    name="form"
                    getParentApi={pageApi.getApi}
                    config={config.form}
                />
                <BarButtonFw
                    name="bar"
                    getParentApi={pageApi.getApi}
                    config={config.bar}
                />
            </>
        )
    }
}