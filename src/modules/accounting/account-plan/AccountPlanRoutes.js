import React from 'react'
import { Route, Switch, useRouteMatch } from 'react-router';
import { AccountPlanAdminPage } from './admin/AccountPlanAdminPage';
import { AccountPlanDetailPage } from './detail/AccountPlanDetailPage';

const AccountPlanRoutes = () => {
    const { path } = useRouteMatch();
    return (
        <Switch>
            <Route exact path={`${path}/account-plan-admin`} component={AccountPlanAdminPage} />
            <Route path={`${path}/account-plan`} component={AccountPlanDetailPage} />
            <Route path={`${path}/account-plan/:id`} component={AccountPlanDetailPage} /> 
        </Switch>
    )
}

export default AccountPlanRoutes;