import React from 'react'
import { Route, Switch, useRouteMatch } from 'react-router';
import { DetractionsAdminPage } from './admin/DetractionsAdminPage';
import { DetractionsDetailPage } from './detail/DetractionsDetailPage';

export const DetractionsRoutes = () => {
    const { path } = useRouteMatch();
    return (
        <Switch>
            <Route exact path={`${path}/detractions-admin`} component={DetractionsAdminPage} />
            <Route path={`${path}/detractions`} component={DetractionsDetailPage} />
            <Route path={`${path}/detractions/:id`} component={DetractionsDetailPage} /> 
        </Switch>
    )
}

export default DetractionsRoutes;
