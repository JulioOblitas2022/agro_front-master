import React from 'react'
import BarButtonFw, { BUTTON_DEFAULT } from '../../../../framework/bar-button/BarButtonFw';
import FormFw from '../../../../framework/form/FormFw';
import GridFw from '../../../../framework/grid/GridFw';
import { BasePage } from '../../../../framework/pages/BasePage';
import { COLUMN_DEFAULT } from '../../../../framework/grid/GridFw';
import { B_SELECT_CONSTANTS } from '../../../shared/constantes/BSelectConstants';
import { getFormDefaultParams } from '../../../shared/util/getFormDefaultParams';
import { getGridServerDefaultParams } from '../../../shared/util/getGridServerDefaultParams';

export class DetractionsAdminPage extends BasePage {

    createPagePropertiesBase(pageApi) {
        return {
            title: 'DETRACCIONES',
            barHeader: {
                fields:[
                    {...BUTTON_DEFAULT.NEW, label:'Nuevo', link: '/accounting/detractions/detractions/new'}
                ]
            }
        };
    }

    renderPage(pageApi) {
        const getGrid = () => pageApi.getComponent('grid');
        const getForm = () => pageApi.getComponent('form');

        const form = {
            ...getFormDefaultParams(),
            title: 'Filtros de búsqueda',
            fields:[
                {name:'description', label:'Descripcion', column: 6},
            ]
        }

        const bar = { fields: [
            {...BUTTON_DEFAULT.SEARCH, onClick: ()=>getGrid().load({params:getForm().getData()})},
            {...BUTTON_DEFAULT.CLEAR, onClick: ()=>getForm().reset()}
        ]}

        const grid = {
            ...getGridServerDefaultParams({queryLoad: 273}),
            title: 'Lista de Detracciones',
            fields:[
                {name: 'description', label: 'Descripcion',},
                { type: 'decimal', name: 'rate', label: 'Tasa',},
                { type: 'decimal', name: 'baseAmount', label: 'Base',},
                {name: 'sunatCode', label: 'Cod. Sunat',},
                {...COLUMN_DEFAULT.VIEW, link: (value, row, index) => `/accounting/detractions/detractions/${row.detractionId}`, label: 'Opciones', colSpan: 3 },
                {...COLUMN_DEFAULT.EDIT, link: (value, row, index) => `/accounting/detractions/detractions/${row.detractionId}/update`, colSpan: 0 },
                {...COLUMN_DEFAULT.DELETE, colSpan: 0, process: {fnOk: (result)=>getGrid().load({params:getForm().getData()}),filter: (value, row, index) => ({queryId: 275, params: {detractionId: row.detractionId}}) }}
            ]
        }
        return (
            <>
                <FormFw
                    name="form"
                    getParentApi={pageApi.getApi}
                    config={form}
                />
                <BarButtonFw
                    name="bar"
                    getParentApi={pageApi.getApi}
                    config={bar}
                />
                <GridFw
                    name="grid"
                    getParentApi={pageApi.getApi}
                    config={grid}
                />
            </>
        )
    }
}