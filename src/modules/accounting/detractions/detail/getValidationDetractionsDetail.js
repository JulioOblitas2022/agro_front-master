
export const getValidationDetractionsDetail = () => {
    return {
        form: {
            description: [ {required: true, message: 'Descripcion es requerida' } ],
            rate: [ {required: true, message: 'Tasa es requerida' } ],
            basicAmount: [ {required: true, message: 'Imp. Base es requerido' } ],
            sunatCode: [ {required: true, message: 'Cod. Sunat es requerido' } ],
        },
        grid: {

        },
        button: {

        },
        bar: {

        }
    }
}
