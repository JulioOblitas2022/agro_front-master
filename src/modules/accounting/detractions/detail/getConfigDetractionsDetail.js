import { DATE_FORMAT, PAGE_MODE } from "../../../../constants/constants";
import { BUTTON_DEFAULT } from "../../../../framework/bar-button/BarButtonFw";
import { UserUtil } from "../../../../util/UserUtil";

export const getConfigDetractionsDetail = (pageApi) => {
    const getForm = () => pageApi.getComponent('form');

    return {
        form: {
            load:{
                queryId: 272,
                params: {detractionId: pageApi.getParamId()},
            },
            update:{
                queryId: 274,
                postLink: (resp, values) => '/accounting/detractions/detractions-admin',
                params: {modifiedBy: UserUtil.getUserId()}
            },
            save:{
                queryId: 271,
                postLink: (resp, values) => '/accounting/detractions/detractions-admin',
                params: {createdBy: UserUtil.getUserId()}
            },
            title: 'DATOS PRINCIPALES',
            fields:[
                {name:'description', label:'Descripcion', column: 3},
                {type: 'number', name:'rate', label:'Tasa', column: 3},
                {type: 'number', name:'baseAmount', label:'Imp. Base', column: 3},
                {name:'sunatCode', label:'Cod. Sunat', column: 3},
            ],
            onComponentLoad : (formApi, pageApi) =>{
                if (pageApi.getPageMode() !== PAGE_MODE.NEW)
                    formApi.load();
            }
        },
        bar: {
            fields: [
                {...BUTTON_DEFAULT.SAVE, onClick: () => getForm().save()},
                {...BUTTON_DEFAULT.UPDATE, onClick: () => getForm().update()},
                {...BUTTON_DEFAULT.RETURN, link: '/accounting/detractions/detractions-admin'}
            ],
        },
        grid: {}
    }
}