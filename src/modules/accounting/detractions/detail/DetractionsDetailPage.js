import React from 'react'
import BarButtonFw from '../../../../framework/bar-button/BarButtonFw';
import FormFw from '../../../../framework/form/FormFw';
import { BasePage } from '../../../../framework/pages/BasePage';
import { getConfigDetractionsDetail } from './getConfigDetractionsDetail';
import { getModeDetractionsDetail } from './getModeDetractionsDetail';
import { getValidationDetractionsDetail } from './getValidationDetractionsDetail';

export class DetractionsDetailPage extends BasePage {
    
    createPageProperties = (pageApi) => {
        return {
            title: 'REGISTRO DE DETRACCIONES',
            validation: getValidationDetractionsDetail,
            mode: getModeDetractionsDetail
        };
    }

    renderPage(pageApi) {

        const config = getConfigDetractionsDetail(pageApi);

        return (
            <>
                <FormFw
                    name="form"
                    getParentApi={pageApi.getApi}
                    config={config.form}
                />
                <BarButtonFw
                    name="bar"
                    getParentApi={pageApi.getApi}
                    config={config.bar}
                />
            </>
        )
    }
}