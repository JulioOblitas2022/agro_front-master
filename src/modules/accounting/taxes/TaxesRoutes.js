import React from 'react'
import { Route, Switch, useRouteMatch } from 'react-router';
import { TaxesAdminPage } from './admin/TaxesAdminPage';
import { TaxesDetailPage } from './detail/TaxesDetailPage';

const TaxesRoutes = () => {
    const { path } = useRouteMatch();
    return (
        <Switch>
            <Route exact path={`${path}/taxes-admin`} component={TaxesAdminPage} />
            <Route path={`${path}/taxes`} component={TaxesDetailPage} />
            <Route path={`${path}/taxes/:id`} component={TaxesDetailPage} /> 
        </Switch>
    )
}

export default TaxesRoutes;