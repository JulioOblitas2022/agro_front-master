import { PAGE_MODE } from "../../../../constants/constants";
import { BUTTON_DEFAULT } from "../../../../framework/bar-button/BarButtonFw";
import { UserUtil } from "../../../../util/UserUtil";
import { AccountContableModal } from "./modals/AccountContableModal";

export const getConfigTaxesDetail = (pageApi) => {
    const getForm = () => pageApi.getComponent('form');

    const accountContablePurchaseConfig = {
        events: {
            afterAddEvent: (data) => {

                getForm().setFieldData('purchaseAccountId', data[0].accountPlanId);
                getForm().setFieldData('accountPlanPurchase', data[0].accountNumber);
                getForm().setFieldData('accountPlanPurchaseDescription', data[0].description);

            }
        }
    }

    const AccountContablePurchaseModalConfig = {
            width: 1000,
            maskClosable: false,
            component: <AccountContableModal name="modalAccount" getParentApi={pageApi.getApi} config={accountContablePurchaseConfig} />
    };

    const accountContableSaleConfig = {
        events: {
            afterAddEvent: (data) => {

                getForm().setFieldData('saleAccountId', data[0].accountPlanId);
                getForm().setFieldData('accountPlanSale', data[0].accountNumber);
                getForm().setFieldData('accountPlanSaleDescription', data[0].description);

            }
        }
    }

    const AccountContableSaleModalConfig = {
            width: 1000,
            maskClosable: false,
            component: <AccountContableModal name="modalAccount" getParentApi={pageApi.getApi} config={accountContableSaleConfig} />
    };

    return {
        form: {
            load:{
                queryId: 297,
                params: {taxId: pageApi.getParamId()},
            },
            update:{
                queryId: 299,
                postLink: (resp, values) => '/accounting/taxes/taxes-admin',
                params: {modifiedBy: UserUtil.getUserId()}
            },
            save:{
                queryId: 296,
                postLink: (resp, values) => '/accounting/taxes/taxes-admin',
                params: {createdBy: UserUtil.getUserId()}
            },
            title: 'DATOS PRINCIPALES',
            fields:[
                { name: 'description', label:'Descripcion', column: 4},
                { name: 'numeral', label:'Numeral.', column: 4},
                { type: 'decimal', name:'rate', label:'Tasa', column: 4},

                { type: 'hidden', name: 'purchaseAccountId' },
                { name: 'accountPlanPurchase', label:'Nro. Cta. Compra', column: 4, readOnly: true, },
                { type: 'button', column: 1, name: 'modal', onClick: (value) => { pageApi.showModal(AccountContablePurchaseModalConfig); }, },
                { name: 'accountPlanPurchaseDescription', label:'Descripción de Cta.', column: 7, readOnly: true},

                { type: 'hidden', name: 'saleAccountId' },
                { name: 'accountPlanSale', label:'Nro. Cta. Venta', column: 4, readOnly: true, },
                { type: 'button', column: 1, name: 'modal', onClick: (value) => { pageApi.showModal(AccountContableSaleModalConfig); }, },
                { name: 'accountPlanSaleDescription', label:'Descripción de Cta.', column: 7, readOnly: true},
            ],
            onComponentLoad : (formApi, pageApi) =>{
                if (pageApi.getPageMode() !== PAGE_MODE.NEW)
                    formApi.load();
            }
        },
        bar: {
            fields: [
                {...BUTTON_DEFAULT.SAVE, onClick: () => getForm().save()},
                {...BUTTON_DEFAULT.UPDATE, onClick: () => getForm().update()},
                {...BUTTON_DEFAULT.RETURN, link: '/accounting/taxes/taxes-admin'}
            ],
        },
        grid: {}
    }
}