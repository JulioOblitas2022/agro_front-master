import React from 'react'
import BarButtonFw from '../../../../framework/bar-button/BarButtonFw';
import FormFw from '../../../../framework/form/FormFw';
import { BasePage } from '../../../../framework/pages/BasePage';
import { getConfigTaxesDetail } from './getConfigTaxesDetail';
import { getModeTaxesDetail } from './getModeTaxesDetail';
import { getValidationTaxesDetail } from './getValidationTaxesDetail';

export class TaxesDetailPage extends BasePage {
    
    createPageProperties = (pageApi) => {
        return {
            title: 'REGISTRO DE IMPUESTOS',
            validation: getValidationTaxesDetail,
            mode: getModeTaxesDetail
        };
    }

    renderPage(pageApi) {

        const config = getConfigTaxesDetail(pageApi);

        return (
            <>
                <FormFw
                    name="form"
                    getParentApi={pageApi.getApi}
                    config={config.form}
                />
                <BarButtonFw
                    name="bar"
                    getParentApi={pageApi.getApi}
                    config={config.bar}
                />
            </>
        )
    }
}