
export const getValidationAssignationRateDocsDetail = () => {
    return {
        form: {
            sunatCode: [{ required: true, message: 'Cod. Sunat es requerido' }],
            description: [{ required: true, message: 'Descripcion es requerida' }],
            // isPrint: [ {required: true, message: 'Se Imprime es requerido' } ],
            docTypeId: [{ required: true, message: 'Tipo Doc. Venta es requerido' }],
            rowsCount: [{ required: true, message: 'Nro. Filas Impresion es requerido' }],
            movementTypeId: [{ required: true, message: 'Tipo de movimiento Venta es requerido' }],
        },
        grid: {

        },
        button: {

        },
        bar: {

        }
    }
}
