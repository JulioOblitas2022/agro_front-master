import React from 'react'
import BarButtonFw from '../../../../framework/bar-button/BarButtonFw';
import FormFw from '../../../../framework/form/FormFw';
import { BasePage } from '../../../../framework/pages/BasePage';
import { getConfigAssignationRateDocsDetail } from './getConfigAssignationRateDocsDetail';
import { getModeAssignationRateDocsDetail } from './getModeAssignationRateDocsDetail';
import { getValidationAssignationRateDocsDetail } from './getValidationAssignationRateDocsDetail';

export class AssignationRateDocsDetailPage extends BasePage {

    createPageProperties = (pageApi) => {
        return {
            title: 'REGISTRO DE ASIGNACION DE IMPUESTOS A DOCUMENTO',
            validation: getValidationAssignationRateDocsDetail,
            mode: getModeAssignationRateDocsDetail
        };
    }

    renderPage(pageApi) {

        const config = getConfigAssignationRateDocsDetail(pageApi);

        return (
            <>
                <FormFw
                    name="form"
                    getParentApi={pageApi.getApi}
                    config={config.form}
                />
                <BarButtonFw
                    name="bar"
                    getParentApi={pageApi.getApi}
                    config={config.bar}
                />
            </>
        )
    }
}