import React from 'react'
import { Route, Switch, useRouteMatch } from 'react-router';
import { AssignationRateDocsAdminPage } from './admin/AssignationRateDocsAdminPage';
import { AssignationRateDocsDetailPage } from './detail/AssignationRateDocsDetailPage';

const AssignationRateDocsRoutes = () => {
    const { path } = useRouteMatch();
    return (
        <Switch>
            <Route exact path={`${path}/assignation-rate-docs-admin`} component={AssignationRateDocsAdminPage} />
            <Route path={`${path}/assignation-rate-docs`} component={AssignationRateDocsDetailPage} />
            <Route path={`${path}/assignation-rate-docs/:id`} component={AssignationRateDocsDetailPage} />
        </Switch>
    )
}

export default AssignationRateDocsRoutes;
