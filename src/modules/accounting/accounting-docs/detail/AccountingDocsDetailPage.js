import React from 'react'
import BarButtonFw from '../../../../framework/bar-button/BarButtonFw';
import FormFw from '../../../../framework/form/FormFw';
import { BasePage } from '../../../../framework/pages/BasePage';
import { getConfigAccountingDocsDetail } from './getConfigAccountingDocsDetail';
import { getModeAccountingDocsDetail } from './getModeAccountingDocsDetail';
import { getValidationAccountingDocsDetail } from './getValidationAccountingDocsDetail';

export class AccountingDocsDetailPage extends BasePage {
    
    createPageProperties = (pageApi) => {
        return {
            title: 'REGISTRO DE DOCUMENTO CONTABLE',
            validation: getValidationAccountingDocsDetail,
            mode: getModeAccountingDocsDetail
        };
    }

    renderPage(pageApi) {

        const config = getConfigAccountingDocsDetail(pageApi);

        return (
            <>
                <FormFw
                    name="form"
                    getParentApi={pageApi.getApi}
                    config={config.form}
                />
                <BarButtonFw
                    name="bar"
                    getParentApi={pageApi.getApi}
                    config={config.bar}
                />
            </>
        )
    }
}