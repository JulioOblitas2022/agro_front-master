
export const getValidationAccountingDocsDetail = () => {
    return {
        form: {
            sunatCode: [ {required: true, message: 'Cod. Sunat es requerido' } ],
            description: [ {required: true, message: 'Descripcion es requerida' } ],
            // isPrint: [ {required: true, message: 'Se Imprime es requerido' } ],
            documentType: [ {required: true, message: 'Tipo Doc. es requerido' } ],
            // active: [ {required: true, message: 'Estado es requerido' } ],
        },
        grid: {

        },
        button: {

        },
        bar: {

        }
    }
}
