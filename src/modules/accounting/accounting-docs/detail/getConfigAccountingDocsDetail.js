import { PAGE_MODE } from '../../../../constants/constants';
import { BUTTON_DEFAULT } from '../../../../framework/bar-button/BarButtonFw';
import { UserUtil } from '../../../../util/UserUtil';

export const getConfigAccountingDocsDetail = (pageApi) => {
    const getForm = () => pageApi.getComponent('form');

    return {
        form: {
            load:{
                queryId: 292,
                params: {documentTypeId: pageApi.getParamId()},
            },
            update:{
                queryId: 294,
                postLink: (resp, values) => '/accounting/accounting-docs/accounting-docs-admin',
                params: {modifiedBy: UserUtil.getUserId()}
            },
            save:{
                queryId: 291,
                postLink: (resp, values) => '/accounting/accounting-docs/accounting-docs-admin',
                params: {createdBy: UserUtil.getUserId()}
            },
            title: 'DATOS PRINCIPALES',
            fields:[
                {name:'sunatCode', label:'Cod. Sunat', column: 4},
                {name:'description', label:'Descripcion', column: 4},
                {name:'abbreviature', label:'Abreviatura', column: 4},
                {type: 'radio', name:'isPrint', label:'Se Imprime', column: 4, load: {dataList: [{value: 1, label: 'Si'}, {value: 2, label: 'No'}]} },
                {type: 'radio', name:'documentType', label:'Tipo', column: 4, load: {dataList: [{value: 2, label: 'Administrativo'}, {value: 1, label: 'Sunat'}]}},
                {type: 'number', name:'rowNumber', label:'Nro. Filas Impresion', column: 4},
                {type: 'radio', name:'isPurchase', label:'Tipo Movimiento', column: 4, load: {dataList: [{value: 1, label: 'Salida'}, {value: 2, label: 'Ingreso'}]}},
                {type: 'radio', name:'active', label:'Estado', column: 4, load: {dataList: [{value: 1, label: 'Activo'}, {value: 2, label: 'Inactivo'}]}},
            ],
            onComponentLoad : (formApi, pageApi) =>{
                if (pageApi.getPageMode() !== PAGE_MODE.NEW)
                    formApi.load();
            }
        },
        bar: {
            fields: [
                {...BUTTON_DEFAULT.SAVE, onClick: () => getForm().save()},
                {...BUTTON_DEFAULT.UPDATE, onClick: () => getForm().update()},
                {...BUTTON_DEFAULT.RETURN, link: '/accounting/accounting-docs/accounting-docs-admin'}
            ],
        },
        grid: {}
    }
}