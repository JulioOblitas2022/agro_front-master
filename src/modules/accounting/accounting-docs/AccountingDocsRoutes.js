import React from 'react'
import { Route, Switch, useRouteMatch } from 'react-router';
import { AccountingDocsAdminPage } from './admin/AccountingDocsAdminPage';
import { AccountingDocsDetailPage } from './detail/AccountingDocsDetailPage';

const AccountingDocsRoutes = () => {
    const { path } = useRouteMatch();
    return (
        <Switch>
            <Route exact path={`${path}/accounting-docs-admin`} component={AccountingDocsAdminPage} />
            <Route path={`${path}/accounting-docs`} component={AccountingDocsDetailPage} />
            <Route path={`${path}/accounting-docs/:id`} component={AccountingDocsDetailPage} /> 
        </Switch>
    )
}

export default AccountingDocsRoutes;