import React from 'react'
import BarButtonFw, { BUTTON_DEFAULT } from '../../../../framework/bar-button/BarButtonFw';
import FormFw from '../../../../framework/form/FormFw';
import GridFw from '../../../../framework/grid/GridFw';
import { BasePage } from '../../../../framework/pages/BasePage';
import { COLUMN_DEFAULT } from "../../../../framework/grid/GridFw";
import { B_SELECT_CONSTANTS } from "../../../shared/constantes/BSelectConstants";
import { getFormDefaultParams } from "../../../shared/util/getFormDefaultParams";
import { getGridServerDefaultParams } from "../../../shared/util/getGridServerDefaultParams";


export class AssignationDocsAdminPage extends BasePage {

    createPagePropertiesBase(pageApi) {
        return {
            title: 'ASIGNACION DE DOCUMENTO',
            barHeader: {
                fields: [
                    { ...BUTTON_DEFAULT.NEW, label: 'Nuevo', link: '/accounting/assignation-docs/assignation-docs/new' }
                ]
            }
        };
    }

    renderPage(pageApi) {
        const getGrid = () => pageApi.getComponent('grid');
        const getForm = () => pageApi.getComponent('form');

        const form = {
            ...getFormDefaultParams(),
            title: 'Filtros de búsqueda',
            fields: [
                { type: 'select', name: 'companyId', label: 'Empresa', column: 4, load: { queryId: 554 } , search: true},
                { type: 'select', name: 'documentTypeId', label: 'Tipo de Documento', column: 4, load: {queryId: 195}, search: true },
            ]
        }

        const bar = {
            fields: [
                { ...BUTTON_DEFAULT.SEARCH, onClick: () => getGrid().load({ params: getForm().getData() }) },
                { ...BUTTON_DEFAULT.CLEAR, onClick: () => getForm().reset() }
            ]
        }

        const grid = {
            ...getGridServerDefaultParams({ queryLoad: 559 }),
            title: 'Resultados',
            fields: [
                { name: 'documentType', label: 'Tipo Doc', },
                { name: 'coin', label: 'Moneda', },
                { name: 'accountNumberPurchase', label: 'Cta. Com. Compra', },
                { name: 'accountPlanPurchase', label: 'Descripcion Cta. Corriente', },
                { name: 'accountNumberSales', label: 'Cta. Com. Venta', },
                { name: 'accountPlanSales', label: 'Descripcion Cta. Venta', },
                { name: 'company', label: 'Empresa', },
                { ...COLUMN_DEFAULT.VIEW, link: (value, row, index) => `/accounting/assignation-docs/assignation-docs/${row.assignationDocId}`, label: 'Opciones', colSpan: 2 },
                { ...COLUMN_DEFAULT.EDIT, link: (value, row, index) => `/accounting/assignation-docs/assignation-docs/${row.assignationDocId}/update`, colSpan: 0 },
                // { ...COLUMN_DEFAULT.DELETE, process: { fnOk: (result) => getGrid().load({ params: getForm().getData() }), filter: (value, row, index) => ({ queryId: 295, params: { assignationDocId: row.assignationDocId } }) } }
            ]
        }

        return (
            <>
                <FormFw
                    name="form"
                    getParentApi={pageApi.getApi}
                    config={form}
                />
                <BarButtonFw
                    name="bar"
                    getParentApi={pageApi.getApi}
                    config={bar}
                />
                <GridFw
                    name="grid"
                    getParentApi={pageApi.getApi}
                    config={grid}
                />
            </>
        )
    }
}