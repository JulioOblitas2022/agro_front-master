import React from 'react'
import { Route, Switch, useRouteMatch } from 'react-router';
import { PerceptionsAdminPage } from './admin/PerceptionsAdminPage';
import { PerceptionsDetailPage } from './detail/PerceptionsDetailPage';

const PerceptionsRoutes = () => {
    const { path } = useRouteMatch();
    return (
        <Switch>
            <Route exact path={`${path}/perceptions-admin`} component={PerceptionsAdminPage} />
            <Route path={`${path}/perceptions`} component={PerceptionsDetailPage} />
            <Route path={`${path}/perceptions/:id`} component={PerceptionsDetailPage} /> 
        </Switch>
    )
}

export default PerceptionsRoutes;