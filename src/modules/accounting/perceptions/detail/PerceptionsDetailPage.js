import React from 'react'
import BarButtonFw from '../../../../framework/bar-button/BarButtonFw';
import FormFw from '../../../../framework/form/FormFw';
import { BasePage } from '../../../../framework/pages/BasePage';
import { getConfigPerceptionsDetail } from './getConfigPerceptionsDetail';
import { getModePerceptionsDetail } from './getModePerceptionsDetail';
import { getValidationPerceptionsDetail } from './getValidationPerceptionsDetail';

export class PerceptionsDetailPage extends BasePage {
    
    createPageProperties = (pageApi) => {
        return {
            title: 'REGISTRO DE PERCEPCIONES',
            validation: getValidationPerceptionsDetail,
            mode: getModePerceptionsDetail
        };
    }

    renderPage(pageApi) {

        const config = getConfigPerceptionsDetail(pageApi);

        return (
            <>
                <FormFw
                    name="form"
                    getParentApi={pageApi.getApi}
                    config={config.form}
                />
                <BarButtonFw
                    name="bar"
                    getParentApi={pageApi.getApi}
                    config={config.bar}
                />
            </>
        )
    }
}