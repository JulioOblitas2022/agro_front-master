import { PAGE_MODE } from '../../../../constants/constants';
import { BUTTON_DEFAULT } from '../../../../framework/bar-button/BarButtonFw';
import { UserUtil } from '../../../../util/UserUtil';
import { AccountContableModal } from './modals/AccountContableModal';

export const getConfigAssignationDocsDetail = (pageApi) => {
    const getForm = () => pageApi.getComponent('form');

    const accountContablePurchaseConfig = {
        events: {
            afterAddEvent: (data) => {

                getForm().setFieldData('purchaseAccountId', data[0].accountPlanId);
                getForm().setFieldData('accountPlanPurchase', data[0].accountNumber);
                getForm().setFieldData('accountPlanPurchaseDescription', data[0].description);

            }
        }
    }

    const AccountContablePurchaseModalConfig = {
            width: 1000,
            maskClosable: false,
            component: <AccountContableModal name="modalAccount" getParentApi={pageApi.getApi} config={accountContablePurchaseConfig} />
    };

    const accountContableSaleConfig = {
        events: {
            afterAddEvent: (data) => {

                getForm().setFieldData('salesAccountId', data[0].accountPlanId);
                getForm().setFieldData('accountPlanSale', data[0].accountNumber);
                getForm().setFieldData('accountPlanSaleDescription', data[0].description);

            }
        }
    }

    const AccountContableSaleModalConfig = {
            width: 1000,
            maskClosable: false,
            component: <AccountContableModal name="modalAccount" getParentApi={pageApi.getApi} config={accountContableSaleConfig} />
    };

    return {
        form: {
            load: {
                queryId: 580,
                params: { assignationDocId: pageApi.getParamId() },
            },
            update: {
                queryId: 581,
                postLink: (resp, values) => '/accounting/assignation-docs/assignation-docs-admin',
                params: { modifiedBy: UserUtil.getUserId() }
            },
            save: {
                queryId: 291,
                postLink: (resp, values) => '/accounting/assignation-docs/assignation-docs-admin',
                params: { createdBy: UserUtil.getUserId() }
            },
            title: 'DATOS PRINCIPALES',
            fields: [
                { type: 'select', name: 'companyId', label: 'Empresa', column: 4, load: { queryId: 554 } , search: true},
                { type: 'select', name: 'documentTypeId', label: 'Tipo Doc.', column: 4, load: { queryId: 195 }, search: true },
                { type: 'select', name: 'coinId', label: 'Moneda', column: 4, load: { queryId: 187 }, search: true },
                { type: 'hidden', name: 'purchaseAccountId' },
                { name:'accountPlanPurchase', label:'Nro. Cta. Compra', column: 4, readOnly: true, },
                { type: 'button', column: 1, name: 'modal', onClick: (value) => { pageApi.showModal(AccountContablePurchaseModalConfig); }, },
                { name:'accountPlanPurchaseDescription', label:'Descripción de Cta.', column: 7, readOnly: true},

                { type: 'hidden', name: 'salesAccountId' },
                { name:'accountPlanSale', label:'Nro. Cta. Venta', column: 4, readOnly: true, },
                { type: 'button', column: 1, name: 'modal', onClick: (value) => { pageApi.showModal(AccountContableSaleModalConfig); }, },
                { name:'accountPlanSaleDescription', label:'Descripción de Cta.', column: 7, readOnly: true},

            ],
            onComponentLoad: (formApi, pageApi) => {
                if (pageApi.getPageMode() !== PAGE_MODE.NEW)
                    formApi.load();
            }
        },
        bar: {
            fields: [
                { ...BUTTON_DEFAULT.SAVE, onClick: () => getForm().save() },
                { ...BUTTON_DEFAULT.UPDATE, onClick: () => getForm().update() },
                { ...BUTTON_DEFAULT.RETURN, link: '/accounting/assignation-docs/assignation-docs-admin' }
            ],
        },
        grid: {}
    }
}