import React from 'react'
import BarButtonFw from '../../../../framework/bar-button/BarButtonFw';
import FormFw from '../../../../framework/form/FormFw';
import { BasePage } from '../../../../framework/pages/BasePage';
import { getConfigAssignationDocsDetail } from './getConfigAssignationDocsDetail';
import { getModeAssignationDocsDetail } from './getModeAssignationDocsDetail';
import { getValidationAssignationDocsDetail } from './getValidationAssignationDocsDetail';

export class AssignationDocsDetailPage extends BasePage {

    createPageProperties = (pageApi) => {
        return {
            title: 'REGISTRO DE ASIGNACION DE DOCUMENTO',
            validation: getValidationAssignationDocsDetail,
            mode: getModeAssignationDocsDetail
        };
    }

    renderPage(pageApi) {

        const config = getConfigAssignationDocsDetail(pageApi);

        return (
            <>
                <FormFw
                    name="form"
                    getParentApi={pageApi.getApi}
                    config={config.form}
                />
                <BarButtonFw
                    name="bar"
                    getParentApi={pageApi.getApi}
                    config={config.bar}
                />
            </>
        )
    }
}