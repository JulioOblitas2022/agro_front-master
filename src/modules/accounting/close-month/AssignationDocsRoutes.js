import React from 'react'
import { Route, Switch, useRouteMatch } from 'react-router';
import { AssignationDocsAdminPage } from './admin/AssignationDocsAdminPage';
import { AssignationDocsDetailPage } from './detail/AssignationDocsDetailPage';

const AssignationDocsRoutes = () => {
    const { path } = useRouteMatch();
    return (
        <Switch>
            <Route exact path={`${path}/assignation-docs-admin`} component={AssignationDocsAdminPage} />
            <Route path={`${path}/assignation-docs`} component={AssignationDocsDetailPage} />
            <Route path={`${path}/assignation-docs/:id`} component={AssignationDocsDetailPage} />
        </Switch>
    )
}

export default AssignationDocsRoutes;
