import React from 'react'
import BarButtonFw from '../../../../framework/bar-button/BarButtonFw';
import FormFw from '../../../../framework/form/FormFw';
import { BasePage } from '../../../../framework/pages/BasePage';
import { getConfigVariousSeatsDetail } from './getConfigVariousSeatsDetail';
import { getModeVariousSeatsDetail } from './getModeVariousSeatsDetail';
import { getValidationVariousSeatsDetail } from './getValidationVariousSeatsDetail';

export class VariousSeatsDetailPage extends BasePage {
    
    createPageProperties = (pageApi) => {
        return {
            title: 'Asientos Diversos',
            validation: getValidationVariousSeatsDetail,
            mode: getModeVariousSeatsDetail
        };
    }

    renderPage(pageApi) {

        const config = getConfigVariousSeatsDetail(pageApi);

        return (
            <>
                <FormFw
                    name="form"
                    getParentApi={pageApi.getApi}
                    config={config.form}
                />
                <BarButtonFw
                    name="bar"
                    getParentApi={pageApi.getApi}
                    config={config.bar}
                />
            </>
        )
    }
}