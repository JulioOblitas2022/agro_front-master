import { DATE_FORMAT, PAGE_MODE } from "../../../../constants/constants";
import { BUTTON_DEFAULT } from "../../../../framework/bar-button/BarButtonFw";
import { LoadingUtil } from "../../../../util/LoadingUtil";
import { UserUtil } from "../../../../util/UserUtil";

export const getConfigVariousSeatsDetail = (pageApi) => {
    const getForm = () => pageApi.getComponent('form');

    return {
        form: {
            load:{
                queryId: 272,
                params: {detractionId: pageApi.getParamId()},
            },
            update:{
                queryId: 274,
                postLink: (resp, values) => '/accounting/detractions/detractions-admin',
                params: {modifiedBy: UserUtil.getUserId()}
            },
            save:{
                queryId: 271,
                postLink: (resp, values) => '/accounting/detractions/detractions-admin',
                params: {createdBy: UserUtil.getUserId()}
            },
            title: 'DATOS PRINCIPALES',
            fields:[
                { type: 'select', name:'accountingBookId', label:'Libro', column: 4, load: {queryId: 573}},
                { type: 'select', name:'accountingBookSubId', label:'Sub Libro', column: 4, load: {queryId: 574},
                    parents: [{name: 'accountingBookId', paramName: 'accountingBookId', required: true}]
                },
                {type: 'decimal', name:'exchangeRate', label:'Tipo de Cambio', column: 4, readOnly: true},
                { type: 'select', name:'coinId', label:'Moneda', column: 4, load: {queryId: 187}},
                { type: 'date', name:'documentDate', label: 'Fecha Documento', column: 4, format: DATE_FORMAT.DDMMYYYY, inputFormat: DATE_FORMAT.YYYYMMDD},
                { type: 'select', name:'documentTypeId', label:'Tipo de Documento', column: 4, load: {queryId: 195}},
            ],
            onComponentLoad : (formApi, pageApi) =>{
                if (pageApi.getPageMode() !== PAGE_MODE.NEW)
                    formApi.load();
            }
        },
        bar: {
            fields: [
                {...BUTTON_DEFAULT.SAVE, onClick: () => getForm().save()},
                {...BUTTON_DEFAULT.UPDATE, onClick: () => getForm().update()},
                {...BUTTON_DEFAULT.RETURN, link: '/accounting/detractions/detractions-admin'}
            ],
        },
        grid: {}
    }
}