import React from 'react'
import { Route, Switch, useRouteMatch } from 'react-router';
import { VariousSeatsAdminPage } from './admin/VariousSeatsAdminPage';
import { VariousSeatsDetailPage } from './detail/VariousSeatsDetailPage';

export const VariousSeatsRoutes = () => {
    const { path } = useRouteMatch();
    return (
        <Switch>
            <Route exact path={`${path}/various-seats-admin`} component={VariousSeatsAdminPage} />
            <Route path={`${path}/various-seats`} component={VariousSeatsDetailPage} />
            <Route path={`${path}/various-seats/:id`} component={VariousSeatsDetailPage} /> 
        </Switch>
    )
}

export default VariousSeatsRoutes;
