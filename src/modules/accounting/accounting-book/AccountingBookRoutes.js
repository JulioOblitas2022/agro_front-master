import React from 'react'
import { Route, Switch, useRouteMatch } from 'react-router';
import { AccountingBookAdminPage } from './admin/AccountingBookAdminPage';
import { AccountingBookDetailPage } from './detail/AccountingBookDetailPage';

const AccountingBookRoutes = () => {
    const { path } = useRouteMatch();
    return (
        <Switch>
            <Route exact path={`${path}/accounting-book-admin`} component={AccountingBookAdminPage} />
            <Route path={`${path}/accounting-book`} component={AccountingBookDetailPage} />
            <Route path={`${path}/accounting-book/:id`} component={AccountingBookDetailPage} /> 
        </Switch>
    )
}

export default AccountingBookRoutes;