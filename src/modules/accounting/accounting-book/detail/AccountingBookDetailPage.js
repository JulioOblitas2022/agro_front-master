import React from 'react'
import BarButtonFw from '../../../../framework/bar-button/BarButtonFw';
import FormFw from '../../../../framework/form/FormFw';
import { BasePage } from '../../../../framework/pages/BasePage';
import { getConfigAccountingBookDetail } from './getConfigAccountingBookDetail';
import { getModeAccountingBookDetail } from './getModeAccountingBookDetail';
import { getValidationAccountingBookDetail } from './getValidationAccountingBookDetail';

export class AccountingBookDetailPage extends BasePage {
    
    createPageProperties = (pageApi) => {
        return {
            title: 'REGISTRO DE LIBRO CONTABLE',
            validation: getValidationAccountingBookDetail,
            mode: getModeAccountingBookDetail
        };
    }

    renderPage(pageApi) {

        const config = getConfigAccountingBookDetail(pageApi);

        return (
            <>
                <FormFw
                    name="form"
                    getParentApi={pageApi.getApi}
                    config={config.form}
                />
                <BarButtonFw
                    name="bar"
                    getParentApi={pageApi.getApi}
                    config={config.bar}
                />
            </>
        )
    }
}