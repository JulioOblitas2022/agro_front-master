import { PAGE_MODE } from '../../../../constants/constants';
import { BUTTON_DEFAULT } from '../../../../framework/bar-button/BarButtonFw';
import { UserUtil } from '../../../../util/UserUtil';

export const getConfigAccountingBookDetail = (pageApi) => {
    const getForm = () => pageApi.getComponent('form');

    return {
        form: {
            load:{
                queryId: 287,
                params: {accountingBookId: pageApi.getParamId()},
            },
            update:{
                queryId: 289,
                postLink: (resp, values) => '/accounting/accounting-book/accounting-book-admin',
                params: {modifiedBy: UserUtil.getUserId()}
            },
            save:{
                queryId: 286,
                postLink: (resp, values) => '/accounting/accounting-book/accounting-book-admin',
                params: {createdBy: UserUtil.getUserId()}
            },
            title: 'DATOS PRINCIPALES',
            fields:[
                {name:'sunatCode', label:'Cod. Sunat', column: 4},
                {name:'description', label:'Descripcion', column: 4},
                {type: 'switch', name:'active', label:'Estado', column: 4, value: true},
            ],
            onComponentLoad : (formApi, pageApi) =>{
                if (pageApi.getPageMode() !== PAGE_MODE.NEW)
                    formApi.load();
            }
        },
        bar: {
            fields: [
                {...BUTTON_DEFAULT.SAVE, onClick: () => getForm().save()},
                {...BUTTON_DEFAULT.UPDATE, onClick: () => getForm().update()},
                {...BUTTON_DEFAULT.RETURN, link: '/accounting/accounting-book/accounting-book-admin'}
            ],
        },
        grid: {}
    }
}