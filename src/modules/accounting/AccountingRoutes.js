import React, { lazy } from 'react'
import { Route, Switch, useRouteMatch } from 'react-router-dom'

const AccountPlanRoutes = lazy(() => import('./account-plan/AccountPlanRoutes'));
const ExchangeRateRoutes = lazy(() => import('./exchange-rate/ExchangeRateRoutes'));
const DetractionsRoutes = lazy(() => import('./detractions/DetractionsRoutes'));
const WithholdingsRoutes = lazy(() => import('./withholdings/WithholdingsRoutes'));
const PerceptionsRoutes = lazy(() => import('./perceptions/PerceptionsRoutes'));
const AccountingBookRoutes = lazy(() => import('./accounting-book/AccountingBookRoutes'));
const AccountingDocsRoutes = lazy(() => import('./accounting-docs/AccountingDocsRoutes'));
const TaxesRoutes = lazy(() => import('./taxes/TaxesRoutes'));
const AssignationDocsRoutes = lazy(() => import('./assignation-docs/AssignationDocsRoutes'));
const VariousSeatsRoutes = lazy(() => import('./various-seats/VariousSeatsRoutes'));
const AssignationRateDocsRoutes = lazy(() => import('./assignation-rate-docs/AssignationRateDocsRoutes'));
const AssignationItemsRoutes = lazy(() => import('./assignation-items/AssignationItemsRoutes'));

const AccountingRoutes = () => {
    const { path } = useRouteMatch();
    return (
        <Switch>
            <Route path={`${path}/account-plan`} component={AccountPlanRoutes} />
            <Route path={`${path}/exchange-rate`} component={ExchangeRateRoutes} />
            <Route path={`${path}/detractions`} component={DetractionsRoutes} />
            <Route path={`${path}/withholdings`} component={WithholdingsRoutes} />
            <Route path={`${path}/perceptions`} component={PerceptionsRoutes} />
            <Route path={`${path}/accounting-book`} component={AccountingBookRoutes} />
            <Route path={`${path}/accounting-docs`} component={AccountingDocsRoutes} />
            <Route path={`${path}/taxes`} component={TaxesRoutes} />
            <Route path={`${path}/assignation-docs`} component={AssignationDocsRoutes} />
            <Route path={`${path}/various-seats`} component={VariousSeatsRoutes} />
            <Route path={`${path}/assignation-rate-docs`} component={AssignationRateDocsRoutes} />
            <Route path={`${path}/assignation-items`} component={AssignationItemsRoutes} />
        </Switch>
    )
}

export default AccountingRoutes;