import React from 'react'
import { Route, Switch, useRouteMatch } from 'react-router';
import { ExchangeRateAdminPage } from './admin/ExchangeRateAdminPage';
import { ExchangeRateDetailPage } from './detail/ExchangeRateDetailPage';

export const ExchangeRateRoutes = () => {
    const { path } = useRouteMatch();
    return (
        <Switch>
            <Route exact path={`${path}/exchange-rate-admin`} component={ExchangeRateAdminPage} />
            <Route path={`${path}/exchange-rate`} component={ExchangeRateDetailPage} />
            <Route path={`${path}/exchange-rate/:id`} component={ExchangeRateDetailPage} /> 
        </Switch>
    )
}

export default ExchangeRateRoutes;