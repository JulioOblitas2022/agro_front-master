import React from 'react'
import BarButtonFw, { BUTTON_DEFAULT } from '../../../../framework/bar-button/BarButtonFw';
import FormFw from '../../../../framework/form/FormFw';
import GridFw from '../../../../framework/grid/GridFw';
import { BasePage } from '../../../../framework/pages/BasePage';
import { COLUMN_DEFAULT } from '../../../../framework/grid/GridFw';
import { B_SELECT_CONSTANTS } from '../../../shared/constantes/BSelectConstants';
import { getFormDefaultParams } from '../../../shared/util/getFormDefaultParams';
import { getGridServerDefaultParams } from '../../../shared/util/getGridServerDefaultParams';
import { DATE_FORMAT } from '../../../../constants/constants';

export class ExchangeRateAdminPage extends BasePage {

    createPagePropertiesBase(pageApi) {
        return {
            title: 'TIPO DE CAMBIO',
            barHeader: {
                fields:[
                    {...BUTTON_DEFAULT.NEW, label:'Nuevo', link: '/accounting/exchange-rate/exchange-rate/new'}
                ]
            }
        };
    }

    renderPage(pageApi) {
        const getGrid = () => pageApi.getComponent('grid');
        const getForm = () => pageApi.getComponent('form');

        const form = {
            ...getFormDefaultParams(),
            title: 'Filtros de búsqueda',
            fields:[
                {type: 'date', name:'fromDate', label:'Fecha Desde', column: 4},
                {type: 'date', name:'untilDate', label:'Fecha Hasta', column: 4},
            ]
        }

        const bar = { 
            fields: [
                {...BUTTON_DEFAULT.SEARCH, onClick: ()=>getGrid().load({params:getForm().getData()})},
                {...BUTTON_DEFAULT.CLEAR, onClick: ()=>getForm().reset()}
            ]
        }

        const grid = {
            ...getGridServerDefaultParams({queryLoad: 268}),
            title: 'Lista de Tipo de Cambio',
            fields:[
                { type: 'date', name: 'dateExchange', label: 'Fecha', format: DATE_FORMAT.DDMMYYYY },
                { name: 'coin', label: 'Moneda',},
                { type: 'decimal', name: 'purchasePrice', label: 'Precio Compra',},
                { type: 'decimal', name: 'salePrice', label: 'Precio Venta',},
                {...COLUMN_DEFAULT.VIEW, link: (value, row, index) => `/accounting/exchange-rate/exchange-rate/${row.exchangeRateId}`, label: 'Opciones', colSpan: 3 },
                {...COLUMN_DEFAULT.EDIT, link: (value, row, index) => `/accounting/exchange-rate/exchange-rate/${row.exchangeRateId}/update`, colSpan: 0 },
                {...COLUMN_DEFAULT.DELETE, process: {fnOk: (result)=>getGrid().load({params:getForm().getData()}),filter: (value, row, index) => ({queryId: 270, params: {exchangeRateId: row.exchangeRateId}}) }, colSpan: 0}
            ]
        }

        return (
            <>
                <FormFw
                    name="form"
                    getParentApi={pageApi.getApi}
                    config={form}
                />
                <BarButtonFw
                    name="bar"
                    getParentApi={pageApi.getApi}
                    config={bar}
                />
                <GridFw
                    name="grid"
                    getParentApi={pageApi.getApi}
                    config={grid}
                />
            </>
        )
    }
}