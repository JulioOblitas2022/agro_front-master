import React from 'react'
import BarButtonFw from '../../../../framework/bar-button/BarButtonFw';
import FormFw from '../../../../framework/form/FormFw';
import { BasePage } from '../../../../framework/pages/BasePage';
import { getConfigExchangeRateDetail } from './getConfigExchangeRateDetail';
import { getModeExchangeRateDetail } from './getModeExchangeRateDetail';
import { getValidationExchangeRateDetail } from './getValidationExchangeRateDetail';

export class ExchangeRateDetailPage extends BasePage {
    
    createPageProperties = (pageApi) => {
        return {
            title: 'REGISTRO DE TIPO DE CAMBIO',
            validation: getValidationExchangeRateDetail,
            mode: getModeExchangeRateDetail
        };
    }

    renderPage(pageApi) {

        const config = getConfigExchangeRateDetail(pageApi);

        return (
            <>
                <FormFw
                    name="form"
                    getParentApi={pageApi.getApi}
                    config={config.form}
                />
                <BarButtonFw
                    name="bar"
                    getParentApi={pageApi.getApi}
                    config={config.bar}
                />
            </>
        )
    }
}