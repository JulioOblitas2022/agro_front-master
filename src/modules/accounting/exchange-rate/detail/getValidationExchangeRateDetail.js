
export const getValidationExchangeRateDetail = () => {
    return {
        form: {
            dateExchange: [ {required: true, message: 'Fecha es requerida' } ],
            coinId: [ {required: true, message: 'Moneda es requerida' } ],
            purchasePrice: [ {required: true, message: 'Precio Compra es requerido' } ],
            salePrice: [ {required: true, message: 'Precio Venta es requerido' } ],
        },
        grid: {

        },
        button: {

        },
        bar: {

        }
    }
}
