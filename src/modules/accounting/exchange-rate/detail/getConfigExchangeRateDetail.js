import { DATE_FORMAT, PAGE_MODE } from "../../../../constants/constants";
import { BUTTON_DEFAULT } from "../../../../framework/bar-button/BarButtonFw";
import { UserUtil } from "../../../../util/UserUtil";

export const getConfigExchangeRateDetail = (pageApi) => {
    const getForm = () => pageApi.getComponent('form');

    return {
        form: {
            load:{
                queryId: 267,
                params: {exchangeRateId: pageApi.getParamId()},
            },
            update:{
                queryId: 269,
                postLink: (resp, values) => '/accounting/exchange-rate/exchange-rate-admin',
                params: {modifiedBy: UserUtil.getUserId()}
            },
            save:{
                queryId: 266,
                postLink: (resp, values) => '/accounting/exchange-rate/exchange-rate-admin',
                params: {createdBy: UserUtil.getUserId()}
            },
            title: 'DATOS PRINCIPALES',
            fields:[
                {type: 'date', name:'dateExchange', label:'Fecha', column: 3, format: DATE_FORMAT.DDMMYYYY, inputFormat: DATE_FORMAT.YYYYMMDD},
                { type: 'select', name:'coinId', label:'Moneda', column: 3, load: {queryId: 187}, search: true},
                {type: 'number', name:'purchasePrice', label:'Precio Compra', column: 3},
                {type: 'number', name:'salePrice', label:'Precio Venta', column: 3},
            ],
            onComponentLoad : (formApi, pageApi) =>{
                if (pageApi.getPageMode() !== PAGE_MODE.NEW)
                    formApi.load();
            }
        },
        bar: {
            fields: [
                {...BUTTON_DEFAULT.SAVE, onClick: () => getForm().save()},
                {...BUTTON_DEFAULT.UPDATE, onClick: () => getForm().update()},
                {...BUTTON_DEFAULT.RETURN, link: '/accounting/exchange-rate/exchange-rate-admin'}
            ],
        },
        grid: {}
    }
}