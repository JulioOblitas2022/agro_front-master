import React from 'react'
import { Route, Switch, useRouteMatch } from 'react-router';
import { WithholdingsAdminPage } from './admin/WithholdingsAdminPage';
import { WithholdingsDetailPage } from './detail/WithholdingsDetailPage';

const WithholdingsRoutes = () => {
    const { path } = useRouteMatch();
    return (
        <Switch>
            <Route exact path={`${path}/withholdings-admin`} component={WithholdingsAdminPage} />
            <Route path={`${path}/withholdings`} component={WithholdingsDetailPage} />
            <Route path={`${path}/withholdings/:id`} component={WithholdingsDetailPage} /> 
        </Switch>
    )
}

export default WithholdingsRoutes;