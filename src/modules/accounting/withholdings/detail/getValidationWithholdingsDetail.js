
export const getValidationWithholdingsDetail = () => {
    return {
        form: {
            description: [ {required: true, message: 'Descripcion es requerida' } ],
            rate: [ {required: true, message: 'Tasa es requerida' } ],
            purchaseAccountId: [ {required: true, message: 'Cuenta Compra es requerida' } ],
            saleAccountId: [ {required: true, message: 'Cuenta Venta es requerida' } ],
        },
        grid: {

        },
        button: {

        },
        bar: {

        }
    }
}
