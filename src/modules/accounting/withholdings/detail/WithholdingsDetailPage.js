import React from 'react'
import BarButtonFw from '../../../../framework/bar-button/BarButtonFw';
import FormFw from '../../../../framework/form/FormFw';
import { BasePage } from '../../../../framework/pages/BasePage';
import { getConfigWithholdingsDetail } from './getConfigWithholdingsDetail';
import { getModeWithholdingsDetail } from './getModeWithholdingsDetail';
import { getValidationWithholdingsDetail } from './getValidationWithholdingsDetail';

export class WithholdingsDetailPage extends BasePage {
    
    createPageProperties = (pageApi) => {
        return {
            title: 'REGISTRO DE RETENCIONES',
            validation: getValidationWithholdingsDetail,
            mode: getModeWithholdingsDetail
        };
    }

    renderPage(pageApi) {

        const config = getConfigWithholdingsDetail(pageApi);

        return (
            <>
                <FormFw
                    name="form"
                    getParentApi={pageApi.getApi}
                    config={config.form}
                />
                <BarButtonFw
                    name="bar"
                    getParentApi={pageApi.getApi}
                    config={config.bar}
                />
            </>
        )
    }
}