import React from 'react'
import { Route, Switch, useRouteMatch } from 'react-router';
import { AssignationItemsAdminPage } from './admin/AssignationItemsAdminPage';
import { AssignationItemsDetailPage } from './detail/AssignationItemsDetailPage';

const AssignationItemsRoutes = () => {
    const { path } = useRouteMatch();
    return (
        <Switch>
            <Route exact path={`${path}/assignation-items-admin`} component={AssignationItemsAdminPage} />
            <Route path={`${path}/assignation-items`} component={AssignationItemsDetailPage} />
            <Route path={`${path}/assignation-items/:id`} component={AssignationItemsDetailPage} />
        </Switch>
    )
}

export default AssignationItemsRoutes;
