import { PAGE_MODE } from '../../../../constants/constants';
import { BUTTON_DEFAULT } from '../../../../framework/bar-button/BarButtonFw';
import { UserUtil } from '../../../../util/UserUtil';
import { AccountContableModal } from './modals/AccountContableModal';
import { ProductModal } from './modals/ProductModal';

export const getConfigAssignationItemsDetail = (pageApi) => {
    const getForm = () => pageApi.getComponent('form');

    const accountContablePurchaseConfig = {
        events: {
            afterAddEvent: (data) => {

                getForm().setFieldData('purchaseAccountId', data[0].accountPlanId);
                getForm().setFieldData('accountPlanPurchase', data[0].accountNumber);
                getForm().setFieldData('accountPlanPurchaseDescription', data[0].description);

            }
        }
    }

    const AccountContablePurchaseModalConfig = {
            width: 1000,
            maskClosable: false,
            component: <AccountContableModal name="modalAccount" getParentApi={pageApi.getApi} config={accountContablePurchaseConfig} />
    };

    const accountContableSaleConfig = {
        events: {
            afterAddEvent: (data) => {

                getForm().setFieldData('salesAccountId', data[0].accountPlanId);
                getForm().setFieldData('accountPlanSale', data[0].accountNumber);
                getForm().setFieldData('accountPlanSaleDescription', data[0].description);

            }
        }
    }

    const AccountContableSaleModalConfig = {
            width: 1000,
            maskClosable: false,
            component: <AccountContableModal name="modalAccount" getParentApi={pageApi.getApi} config={accountContableSaleConfig} />
    };

    const itemConfig = {
        events: {
            afterAddEvent : (data) => {
                getForm().setFieldData('masterItemId', data[0].masterItemId);
                getForm().setFieldData('description', data[0].description);
            }
        }
    }

    const ItemModalConfig = (data) =>  {
        return {
            width: 1600,
            maskClosable: false,
            component: <ProductModal name={data}  getParentApi={pageApi.getApi} config={itemConfig}  />
        }
    };

    return {
        form: {
            load: {
                queryId: 593,
                params: { assignationItemId: pageApi.getParamId() },
            },
            update: {
                queryId: 594,
                postLink: (resp, values) => '/accounting/assignation-items/assignation-items-admin',
                params: { modifiedBy: UserUtil.getUserId() }
            },
            save: {
                queryId: 595,
                postLink: (resp, values) => '/accounting/assignation-items/assignation-items-admin',
                params: { createdBy: UserUtil.getUserId() }
            },
            title: 'DATOS PRINCIPALES',
            fields: [
                { type: 'select', name: 'companyId', label: 'Empresa', column: 4, load: { queryId: 554 } , search: true},
                { type: 'hidden', name: 'masterItemId' },
                { name:'description', label:'Item', column: 4, readOnly: true, },
                { type: 'button', column: 1, name: 'modal', onClick: (value) => { pageApi.showModal(ItemModalConfig('')); }, },


                { type: 'hidden', name: 'purchaseAccountId' },
                { name:'accountPlanPurchase', label:'Nro. Cta. Compra', column: 4, readOnly: true, },
                { type: 'button', column: 1, name: 'modal', onClick: (value) => { pageApi.showModal(AccountContablePurchaseModalConfig); }, },
                { name:'accountPlanPurchaseDescription', label:'Descripción de Cta.', column: 7, readOnly: true},

                { type: 'hidden', name: 'salesAccountId' },
                { name:'accountPlanSale', label:'Nro. Cta. Venta', column: 4, readOnly: true, },
                { type: 'button', column: 1, name: 'modal', onClick: (value) => { pageApi.showModal(AccountContableSaleModalConfig); }, },
                { name:'accountPlanSaleDescription', label:'Descripción de Cta.', column: 7, readOnly: true},

                { type: 'select', name: 'detractionId', label: 'Detracción', column: 4, load: { queryId: 589 } , search: true},
                { type: 'select', name: 'perceptionId', label: 'Percepción', column: 4, load: { queryId: 590 } , search: true},
                { type: 'select', name: 'withholdingId', label: 'Retención', column: 4, load: { queryId: 591 } , search: true},
                { type: 'select', name: 'igvTypePurchase', label: 'IGV Compra', column: 4, load: { queryId: 592 } , search: true},
                { type: 'select', name: 'igvTypeSale', label: 'IGV Venta', column: 4, load: { queryId: 592 } , search: true},
                { type: 'select', name: 'iscId', label: 'ISC', column: 4, load: { queryId: 592 } , search: true},

            ],
            onComponentLoad: (formApi, pageApi) => {
                if (pageApi.getPageMode() !== PAGE_MODE.NEW)
                    formApi.load();
            }
        },
        bar: {
            fields: [
                { ...BUTTON_DEFAULT.SAVE, onClick: () => getForm().save() },
                { ...BUTTON_DEFAULT.UPDATE, onClick: () => getForm().update() },
                { ...BUTTON_DEFAULT.RETURN, link: '/accounting/assignation-items/assignation-items-admin' }
            ],
        },
        grid: {}
    }
}