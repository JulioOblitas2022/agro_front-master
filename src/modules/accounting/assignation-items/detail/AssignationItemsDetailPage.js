import React from 'react'
import BarButtonFw from '../../../../framework/bar-button/BarButtonFw';
import FormFw from '../../../../framework/form/FormFw';
import { BasePage } from '../../../../framework/pages/BasePage';
import { getConfigAssignationItemsDetail } from './getConfigAssignationItemsDetail';
import { getModeAssignationItemsDetail } from './getModeAssignationItemsDetail';
import { getValidationAssignationItemsDetail } from './getValidationAssignationItemsDetail';

export class AssignationItemsDetailPage extends BasePage {

    createPageProperties = (pageApi) => {
        return {
            title: 'REGISTRO DE ASIGNACION DE CTA. CONTABLE A ITEMS',
            validation: getValidationAssignationItemsDetail,
            mode: getModeAssignationItemsDetail
        };
    }

    renderPage(pageApi) {

        const config = getConfigAssignationItemsDetail(pageApi);

        return (
            <>
                <FormFw
                    name="form"
                    getParentApi={pageApi.getApi}
                    config={config.form}
                />
                <BarButtonFw
                    name="bar"
                    getParentApi={pageApi.getApi}
                    config={config.bar}
                />
            </>
        )
    }
}