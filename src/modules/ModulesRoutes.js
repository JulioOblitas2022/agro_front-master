import React, { lazy, Suspense } from 'react'
import { Switch, Route, useRouteMatch } from "react-router-dom";

const DevelopmentRoutes = lazy(() => import('./development/DevelopmentRoutes'));
const SecurityRoutes = lazy(() => import('./security/SecurityRoutes'));
const MaintainersRoutes = lazy(() => import('./maintainers/MaintainersRoutes'));
const WarehouseRoutes = lazy(() => import('./warehouse/WarehouseRoutes'));
const LogisticsRoutes = lazy(() => import('./logistics/LogisticsRoutes'));
const SalesRoutes = lazy(() => import('./sales/SalesRoutes'));
const TreasuryRoutes = lazy(() => import('./treasury/TreasuryRoutes'));
const AccountingRoutes = lazy(() => import('./accounting/AccountingRoutes'));
const ProductionRoutes = lazy(() => import('./production/ProductionRoutes'));
const PaysheetRoutes = lazy(() => import('./paysheet/PaysheetRoutes'));
const ManagementRoutes = lazy(() => import('./management/ManagementRoutes'));

export const ModulesRoutes = () => {
    const { path } = useRouteMatch();
    return (
        <Suspense fallback={<div>Loading...</div>}>
            <Switch>
                <Route path={`${path}development`} component={DevelopmentRoutes}/>
                <Route path={`${path}security`} component={SecurityRoutes} />
                <Route path={`${path}maintainers`} component={MaintainersRoutes}/>
                <Route path={`${path}warehouse`} component={WarehouseRoutes}/>
                <Route path={`${path}logistics`} component={LogisticsRoutes}/>
                <Route path={`${path}sales`} component={SalesRoutes}/>
                <Route path={`${path}treasury`} component={TreasuryRoutes}/>
                <Route path={`${path}accounting`} component={AccountingRoutes}/>
                <Route path={`${path}production`} component={ProductionRoutes}/>
                <Route path={`${path}paysheet`} component={PaysheetRoutes}/>
                <Route path={`${path}management`} component={ManagementRoutes}/>
            </Switch>
        </Suspense>
    )
}
//https://learnwithparam.com/blog/how-to-handle-query-params-in-react-router/