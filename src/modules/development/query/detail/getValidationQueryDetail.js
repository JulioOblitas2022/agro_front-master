
export const getValidationQueryDetail = () => {
    return {
        form: {
            queryId: [ {required: true, message: 'queryId is required' } ],
            queryType: [ {required: true, message: 'queryType is required' } ],
            description: [ {required: true, message: 'description is required' } ],
            query: [ {required: true, message: 'query is required'} ]
        },
        grid: {

        },
        button: {

        },
        bar: {

        }
    }
}
