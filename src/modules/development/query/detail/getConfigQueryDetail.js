import sqlFormatter from 'sql-formatter';
import { PAGE_MODE } from '../../../../constants/constants';
import { BUTTON_DEFAULT } from '../../../../framework/bar-button/BarButtonFw';

export const getConfigQueryDetail = (parent) => {
    const getForm = () => parent.getComponent('form');

    const formatSql = (sql) => {
        const sqlConfig = { language: 'sql', indent: '\t' }
        return sqlFormatter.format(sql, sqlConfig)
    }

    return {
        form: {
            load:{
                queryId: 64,
                params: {queryId: parent.getParamId()},
                fnTransform: (resp) => {
                    resp.dataObject.query = formatSql(resp.dataObject.query);
                    return resp;
                },
            },
            update:{
                queryId: 63,
                postLink: (resp, values) => '/development/query/query-admin'
            },
            save:{
                queryId: 62,
                postLink: (resp, values) => '/development/query/query-admin'
            },
            fields:[
                { name:'description', label:'Description', column: 6},
                { type: 'select', name:'queryType', label:'QueryType', column: 6, load: { queryId: 241 } },
                { type:'textarea', name:'query', label:'Query', column: 12, rows: 20},
                
            ],
            onComponentLoad : (formApi, pageApi) =>{
                if (pageApi.getPageMode() !== PAGE_MODE.NEW)    
                    formApi.load();
            }
        },
        bar: {
            fields: [
                {...BUTTON_DEFAULT.SAVE, onClick: () => getForm().save()},
                {...BUTTON_DEFAULT.UPDATE, onClick: () => getForm().update()},
                {...BUTTON_DEFAULT.RETURN, link: '/development/query/query-admin'}
            ],
        },
        grid: {}
    };
}