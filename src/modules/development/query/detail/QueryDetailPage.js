import BarButtonFw from "../../../../framework/bar-button/BarButtonFw";
import FormFw from "../../../../framework/form/FormFw";
import { BasePage } from "../../../../framework/pages/BasePage";
import { getConfigQueryDetail } from "./getConfigQueryDetail";
import { getModeQueryDetail } from "./getModeQueryDetail";
import { getValidationQueryDetail } from "./getValidationQueryDetail";

export class QueryDetailPage extends BasePage {
    
    createPageProperties = (pageApi) => {
        return {
            title: 'Query Detail'
        };
    }

    createPageProperties = (pageApi) => {
        return {
            title: 'Query Detail',
            validation: getValidationQueryDetail,
            mode: getModeQueryDetail
        };
    }

    renderPage(pageApi) {
        const config = getConfigQueryDetail(pageApi);
        
        return (
            <>
                <FormFw
                    name="form"
                    getParentApi={pageApi.getApi}
                    config={config.form}
                />
                <BarButtonFw
                    name="bar"
                    getParentApi={pageApi.getApi}
                    config={config.bar}
                />
            </>
        )
    }
}


