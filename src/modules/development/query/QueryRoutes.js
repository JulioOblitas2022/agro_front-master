import React from 'react'
import { Switch, Route, useRouteMatch } from "react-router-dom"
import { QueryAdminPage } from './admin/QueryAdminPage'
import { QueryDetailPage } from './detail/QueryDetailPage'

const QueryRoutes = () => {
    const { path } = useRouteMatch();
    return (
        <Switch>
            <Route exact path={`${path}/query-admin`} component={QueryAdminPage} />
            <Route path={`${path}/query`} component={QueryDetailPage} />
            <Route path={`${path}/query/:id`} component={QueryDetailPage} />
        </Switch>
    )
}
export default QueryRoutes;