import { BUTTON_DEFAULT } from "../../../../framework/bar-button/BarButtonFw";
import { COLUMN_DEFAULT } from "../../../../framework/grid/GridFw";
import { getFormDefaultParams } from "../../../shared/util/getFormDefaultParams";
import { getGridServerDefaultParams } from "../../../shared/util/getGridServerDefaultParams";


export const getConfigQueryAdmin = (pageApi) => {
    const getGrid = () => pageApi.getComponent('grid');
    const getForm = () => pageApi.getComponent('form');
    return {
        form: {
            ...getFormDefaultParams(),
            fields:[
                {name:'queryId', label:'QueryId', column: 4},
                {name:'description', label:'Description', column: 8},
            ]
        },
        bar: { fields: [
            {...BUTTON_DEFAULT.SEARCH, onClick: ()=>getGrid().load({params:getForm().getData()})},
            {...BUTTON_DEFAULT.CLEAR, onClick: ()=>getForm().reset()}
        ]},
        grid: {
            ...getGridServerDefaultParams({queryLoad: 1}),
            fields:[
                {name:'queryId', label:'QueryId', sorter: {multiple: 1}},
                {name:'description', label:'Description', sorter: {multiple: 2}},
                {name:'queryType', label:'QueryType'},
                {...COLUMN_DEFAULT.VIEW, link: (value, row, index) => `/development/query/query/${row.queryId}` },
                {...COLUMN_DEFAULT.EDIT, link: (value, row, index) => `/development/query/query/${row.queryId}/update` },
            ]
        }
    }
}