import React from 'react';
import {Switch, Route, useRouteMatch } from "react-router-dom";
import { RouteAdminPage } from './admin/RouteAdminPage';
import { RouteDetailPage } from './detail/RouteDetailPage';

const RouteRoutes = () => {
    const { path } = useRouteMatch();
    return (
        <Switch>
            <Route exact path={`${path}/route-admin`} component={RouteAdminPage} />
            <Route path={`${path}/route`} component={RouteDetailPage} />
            <Route path={`${path}/route/:id`} component={RouteDetailPage} />
        </Switch>
    )
}
export default RouteRoutes;
