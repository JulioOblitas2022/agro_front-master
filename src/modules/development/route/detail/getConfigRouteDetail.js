import { PAGE_MODE } from '../../../../constants/constants';
import { BUTTON_DEFAULT } from '../../../../framework/bar-button/BarButtonFw';

export const getConfigRouteDetail = (pageApi) => {
    const getForm = () => pageApi.getComponent('form');
    //const getDataPageMode = () => pageApi.getDataPageMode(getModeRouteDetail());

    return {
        form: {
            load:{
                queryId: 66,
                params: {routeId: pageApi.getParamId()},
            },
            update:{
                queryId: 68,
                postLink: (resp, values) => '/development/route/route-admin'
            },
            save:{
                queryId: 67,
                postLink: (resp, values) => '/development/route/route-admin'
            },
            fields:[
                { name:'label', label:'Label', column: 6},
                { name:'url', label:'Url', column: 6},
                { name:'icon', label:'Icon', column: 3},
                { type: 'select', name:'type', label:'Type', column: 3, 
                    load: {
                            dataList: [
                            {label: 'Parent', value: '1' },
                            {label: 'Child', value: '2'},
                        ]
                    }
                },
                // {
                //     type:'radio', name:'type', label:'Type', load: {
                //         dataList: [
                //             {label: 'Parent', value: '1'},
                //             {label: 'Child', value: '2'},
                //         ]
                //     },
                //     column: 4
                // },
                { type: 'select', name:'parentId', label:'ParentId', column: 6, search: true, load: {queryId: 91}},
                { type: 'number', name:'numberOrder', label:'NumberOrder', column: 3, pattern: /^\d+(\.\d{1,2})?$/ },
                { type: 'switch', name:'active', label:'Active', column: 3, value: true},
                
            ],
            //validations: getValidationRouteDetail()['form'],
            //mode : getDataPageMode(),
            onComponentLoad : (formApi, pageApi) =>{
                if (pageApi.getPageMode() !== PAGE_MODE.NEW)    
                    formApi.load();
            }
        },
        bar: {
            fields: [
                {...BUTTON_DEFAULT.SAVE, onClick: () => getForm().save()},
                {...BUTTON_DEFAULT.UPDATE, onClick: () => getForm().update()},
                {...BUTTON_DEFAULT.RETURN, link: '/development/route/route-admin'}
            ],
            //mode : getDataPageMode()
        },
        grid: {}
    }
}
