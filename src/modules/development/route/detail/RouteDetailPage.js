import React from 'react'
import BarButtonFw from '../../../../framework/bar-button/BarButtonFw';
import FormFw from '../../../../framework/form/FormFw';
import { BasePage } from '../../../../framework/pages/BasePage';
import { getConfigRouteDetail } from './getConfigRouteDetail';
import { getModeRouteDetail } from './getModeRouteDetail';
import { getValidationRouteDetail } from './getValidationRouteDetail';


export class RouteDetailPage extends BasePage {

    createPageProperties = (pageApi) => {
        return {
            title: 'Route Detail',
            validation: getValidationRouteDetail,
            mode: getModeRouteDetail
        };
    }

    renderPage(pageApi) {
        const config = getConfigRouteDetail(pageApi);
          
        return (
            <>
                <FormFw
                    name="form"
                    getParentApi={pageApi.getApi}
                    config={config.form}
                />
                <BarButtonFw
                    name="bar"
                    getParentApi={pageApi.getApi}
                    config={config.bar}
                />
            </>
        )
    }
}
