export const getValidationRouteDetail = () => {
    return {
        form: {
            label: [ {required: true, message: 'label is required' } ],
            url: [ {required: true, message: 'url is required' } ],
            icon: [ {required: true, message: 'icon is required' } ],
            type: [ {required: true, message: 'type is required'} ],
            active: [ {required: true, message: 'active is required'} ],
            parentId: [ {required: true, message: 'parentId is required'} ],
            numberOrder: [ {required: true, message: 'numberOrder is required'} ],
        },
        grid: {

        },
        button: {

        },
        bar: {

        }
    }
}
