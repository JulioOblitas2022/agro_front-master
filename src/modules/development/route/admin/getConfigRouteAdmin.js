import { BUTTON_DEFAULT } from "../../../../framework/bar-button/BarButtonFw";
import { COLUMN_DEFAULT } from "../../../../framework/grid/GridFw";
import { getFormDefaultParams } from "../../../shared/util/getFormDefaultParams";
import { getGridServerDefaultParams } from "../../../shared/util/getGridServerDefaultParams";

export const getConfigRouteAdmin = (pageApi) => {
    const getGrid = () => pageApi.getComponent('grid');
    const getForm = () => pageApi.getComponent('form');
    return {
        form: {
            ...getFormDefaultParams(),
            fields:[
                {name:'label', label:'Label', column: 8},
            ]
        },
        bar: { fields: [
            {...BUTTON_DEFAULT.SEARCH, onClick: ()=>getGrid().load({params:getForm().getData()})},
            {...BUTTON_DEFAULT.CLEAR, onClick: ()=>getForm().reset()}
        ]},
        grid: {
            ...getGridServerDefaultParams({queryLoad: 65}),
            showIndex: true,
            fields:[
                {name:'label', label:'LABEL'},
                {name:'url', label:'URL'},
                {name:'icon', label:'ICON'},
                {name:'numberOrder', label:'ORDER', sorter: {multiple: 1}},
                {...COLUMN_DEFAULT.VIEW, link: (value, row, index) => `/development/route/route/id-${row.routeId}` },
                {...COLUMN_DEFAULT.EDIT, link: (value, row, index) => `/development/route/route/id-${row.routeId}/update` },
            ]
        }
    }
}
