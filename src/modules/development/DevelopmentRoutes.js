import React, { lazy } from 'react';
import { Switch, Route, useRouteMatch } from 'react-router-dom';

const RouteRoutes = lazy(() => import('./route/RouteRoutes'));
const QueryRoutes = lazy(() => import('./query/QueryRoutes'));
const ProfileRouteRoutes = lazy(() => import('./profile-route/ProfileRouteRoutes'));

export const DevelopmentRoutes = () => {
    const { path } = useRouteMatch();
    return (
        <Switch>
             <Route path={`${path}/route`} component={RouteRoutes} />
             <Route path={`${path}/query`} component={QueryRoutes} />
             <Route path={`${path}/profile-route`} component={ProfileRouteRoutes} />
        </Switch>
    )
}


export default DevelopmentRoutes;