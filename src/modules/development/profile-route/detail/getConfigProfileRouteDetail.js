import { PAGE_MODE } from '../../../../constants/constants';
import { BUTTON_DEFAULT } from '../../../../framework/bar-button/BarButtonFw';

export const getConfigProfileRouteDetail = (pageApi) => {
    const getForm = () => pageApi.getComponent('form');

    return {
        form: {
            load:{
                queryId: 72,
                params: {profileRouteId: pageApi.getParamId()},
            },
            update:{
                queryId: 74,
                postLink: (resp, values) => '/development/profile-route/profile-route-admin'
            },
            save:{
                queryId: 73,
                postLink: (resp, values) => '/development/profile-route/profile-route-admin'
            },
            fields:[
                {type: 'select', name:'routeId', label:'ROUTE', column: 6, load: {queryId: 70}},
                {type: 'select', name:'profileId', label:'PROFILE', column: 6, load: {queryId: 71}},
            ],
            onComponentLoad : (formApi, pageApi) =>{
                if (pageApi.getPageMode() !== PAGE_MODE.NEW)
                    formApi.load();
            }
        },
        bar: {
            fields: [
                {...BUTTON_DEFAULT.SAVE, onClick: () => getForm().save()},
                {...BUTTON_DEFAULT.UPDATE, onClick: () => getForm().update()},
                {...BUTTON_DEFAULT.RETURN, link: '/development/profile-route/profile-route-admin'}
            ],
        },
        grid: {}
    }
}
