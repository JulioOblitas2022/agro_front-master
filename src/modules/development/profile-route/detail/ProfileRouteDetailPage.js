import React from 'react'
import BarButtonFw from '../../../../framework/bar-button/BarButtonFw';
import FormFw from '../../../../framework/form/FormFw';
import { BasePage } from '../../../../framework/pages/BasePage';
import { getConfigProfileRouteDetail } from './getConfigProfileRouteDetail';
import { getModeProfileRouteDetail } from './getModeProfileRouteDetail';
import { getValidationProfileRouteDetail } from './getValidationProfileRouteDetail';

export class ProfileRouteDetailPage extends BasePage {

    createPageProperties = (pageApi) => {
        return {
            title: 'Profile - Route Detail',
            validation: getValidationProfileRouteDetail,
            mode: getModeProfileRouteDetail
        };
    }

    renderPage(pageApi) {
        const config = getConfigProfileRouteDetail(pageApi);
          
          
        return (
            <>
                <FormFw
                    name="form"
                    getParentApi={pageApi.getApi}
                    config={config.form}
                />
                <BarButtonFw
                    name="bar"
                    getParentApi={pageApi.getApi}
                    config={config.bar}
                />
            </>
        )
    }
}
