export const getValidationProfileRouteDetail = () => {
    return {
        form: {
            routeId: [ {required: true, message: 'routeId is required' } ],
            profileId: [ {required: true, message: 'profileId is required' } ],
        },
        grid: {

        },
        button: {

        },
        bar: {

        }
    }
}
