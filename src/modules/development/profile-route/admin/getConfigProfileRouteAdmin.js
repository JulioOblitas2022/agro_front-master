import { BUTTON_DEFAULT } from '../../../../framework/bar-button/BarButtonFw';
import { COLUMN_DEFAULT } from '../../../../framework/grid/GridFw';
import { getFormDefaultParams } from '../../../shared/util/getFormDefaultParams';
import { getGridServerDefaultParams } from '../../../shared/util/getGridServerDefaultParams';

export const getConfigProfileRouteAdmin = (pageApi) => {
    const getGrid = () => pageApi.getComponent('grid');
    const getForm = () => pageApi.getComponent('form');
    return {
        form: {
            ...getFormDefaultParams(),
            fields:[
                // {type: 'select', name:'routeId', label:'ROUTE', column: 6, load: {queryId: 70}},
                {type: 'select', name:'profileId', label:'PROFILE', column: 6, load: {queryId: 71}},
            ]
        },
        bar: { fields: [
            {...BUTTON_DEFAULT.SEARCH, onClick: ()=>getGrid().load({params:getForm().getData()})},
            {...BUTTON_DEFAULT.CLEAR, onClick: ()=>getForm().reset()}
        ]},
        grid: {
            ...getGridServerDefaultParams({queryLoad: 69}),
            fields:[
                {name:'route', label:'ROUTE'},
                {name:'profile', label:'PROFILE'},
                {...COLUMN_DEFAULT.VIEW, link: (value, row, index) => `/development/profile-route/profile-route/id-${row.profileRouteId}` },
                {...COLUMN_DEFAULT.EDIT, link: (value, row, index) => `/development/profile-route/profile-route/id-${row.profileRouteId}/update` },
            ]
        }
    }
}
