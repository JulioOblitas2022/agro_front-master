import React from 'react'
import BarButtonFw, { BUTTON_DEFAULT } from '../../../../framework/bar-button/BarButtonFw';
import FormFw from '../../../../framework/form/FormFw';
import GridFw from '../../../../framework/grid/GridFw';
import { BasePage } from '../../../../framework/pages/BasePage';
import { getConfigProfileRouteAdmin } from './getConfigProfileRouteAdmin';

export class ProfileRouteAdminPage extends BasePage {
    
    createPageProperties = (pageApi) => {
        return {
            title: 'Profile - Routes',
            barHeader: {
                fields:[
                    {...BUTTON_DEFAULT.NEW, label:'Nuevo', link: '/development/profile-route/profile-route/new'}
                ]
            }
        };
    }
    renderPage(pageApi) {
        const config = getConfigProfileRouteAdmin(pageApi);
        
        return (
            <>
                <FormFw
                    name="form"
                    getParentApi={pageApi.getApi}
                    config={config.form}
                />
                <BarButtonFw
                    name="bar"
                    getParentApi={pageApi.getApi}
                    config={config.bar}
                />
                <GridFw
                    name="grid"
                    getParentApi={pageApi.getApi}
                    config={config.grid}
                />
            </>
        )
    }
}
