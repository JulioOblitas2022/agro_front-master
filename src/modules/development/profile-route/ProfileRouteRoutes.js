import React from 'react';
import { Route, Switch, useRouteMatch } from 'react-router-dom';
import { ProfileRouteAdminPage } from './admin/ProfileRouteAdminPage';
import { ProfileRouteDetailPage } from './detail/ProfileRouteDetailPage';

const ProfileRouteRoutes = () => {
    const { path } = useRouteMatch();
    return (
        <Switch>
            <Route exact path={`${path}/profile-route-admin`} component={ProfileRouteAdminPage} />
            <Route path={`${path}/profile-route`} component={ProfileRouteDetailPage} />
            <Route path={`${path}/profile-route/:id`} component={ProfileRouteDetailPage} />
        </Switch>
    )
}

export default ProfileRouteRoutes;
