import React from 'react';
import BarButtonFw from '../../../../framework/bar-button/BarButtonFw';
import FormFw from '../../../../framework/form/FormFw';
import { BasePage } from '../../../../framework/pages/BasePage';
import { getConfigReferralGuideDetail } from './getConfigReferralGuideDetail';
import { getModeReferralGuideDetail } from './getModeReferralGuideDetail';
import { getValidationReferralGuideDetail } from './getValidationReferralGuideDetail';
import { createItemsSection } from './sections/ItemsSection';

export class ReferralGuideDetailPage extends BasePage {
    
    createPageProperties = (pageApi) => {
        return {
            title: 'REGISTRO DE GUIAS',
            validation: getValidationReferralGuideDetail,
            mode: getModeReferralGuideDetail
        };
    }

    renderPage(pageApi) {

        const config = getConfigReferralGuideDetail(pageApi);

        return (
            <>
                <FormFw name="form" getParentApi={pageApi.getApi} config={config.form} />
                {createItemsSection(pageApi)}
                <BarButtonFw name="bar" getParentApi={pageApi.getApi} config={config.bar} />
            </>
        )
    }
}