import { MODE_SHORTCUT } from "../../../../constants/constants"

export const getModeReferralGuideDetail = () => {
    return {
        new: {
            form: {
                hide: ['guideNumber'],
                read: ['broadcastDate', 'deliverDate', 'licenseNumber', 'plateNumber', 'address']
            },
            bar: {
                show: ['save', 'return']
            }
        },
        update:{
            form: {
                // hide: ['docTypeId']
                read: ['broadcastDate', 'deliverDate', 'licenseNumber', 'plateNumber', 'address', 'guideNumber']
            },
            bar: {
                show: ['update', 'return']
            }
        },
        read:{
            form: {
                read: [MODE_SHORTCUT.ALL]
            },
            bar: {
                show: ['return']
            }
        }
    }
}
