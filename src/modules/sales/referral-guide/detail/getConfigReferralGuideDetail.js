import { DATE_FORMAT, PAGE_MODE } from "../../../../constants/constants";
import { BUTTON_DEFAULT } from "../../../../framework/bar-button/BarButtonFw";
import { UserUtil } from "../../../../util/UserUtil";
import { DocReferencesModal } from "./modals/DocReferencesModal";

export const getConfigReferralGuideDetail = (pageApi) => {
    
    const getForm = () => pageApi.getComponent('form');
    const getItemsProformasGrid = () => pageApi.getComponent('itemsProformasGrid');

    const docRefConfig = {
        events: {
            afterAddEvent: (data) => {
                let { items, customerOrderId, docNumber, orderDate} = data[0];
                let itemData = JSON.parse(items);
                if (itemData !== null) {
                    //getProductGrid().reset();
                    getItemsProformasGrid().setData(itemData);
                }
                else {
                    getItemsProformasGrid().reset();
                }
                getForm().setFieldData('documentRefId', customerOrderId);
                getForm().setFieldData('documentRefNumber', docNumber);
                getForm().setFieldData('broadcastDate', orderDate);
                getForm().setFieldData('deliverDate', orderDate);

            }
        }
    }

    const DocRefModalConfig = (data) => {
        return {
            width: 1000,
            maskClosable: false,
            component: <DocReferencesModal name={data} getParentApi={pageApi.getApi} config={docRefConfig} />
        }
    };

    return {
        form: {
            autoLoad: true,
            load:{
                queryId: 350,
                params: {referralGuideId: pageApi.getParamId() || 0},
                fnOk: (resp) => {
                    getItemsProformasGrid().load({params: pageApi.getParamId()});
                }
            },
            update:{
                queryId: 349,
                postLink: (resp, values) => '/sales/referral-guide/referral-guide-admin',
                params: {modifiedBy: UserUtil.getUserId(), referralGuideId: pageApi.getParamId()},
                fnOk: (resp) => {
                    getItemsProformasGrid().save();
                }
            },
            save:{
                queryId: 348,
                postLink: (resp, values) => '/sales/referral-guide/referral-guide-admin',
                params: {createdBy: UserUtil.getUserId()},
                fnOk: (resp) => {
                    const referralGuideId = resp.dataObject?.referralGuideId;
                    if (referralGuideId){
                        getItemsProformasGrid().save({params:{referralGuideId}});
                    }
                }
            },
            fields:[
                { type: 'divider', label: 'Datos Principales'},
                { type: 'select', name: 'companyId', label: 'Empresa', column: 4, load: { queryId: 554, }, search: true  },
                { name: 'documentNumber', label: 'Nro. Documento', column: 4, readOnly: true  },
                { type: 'date', name:'entryDate', label:'Fecha Emision', format: 'DD/MM/YYYY', inputFormat: DATE_FORMAT.YYYYMMDD, column: 4},
                { type: 'select', name:'transferReasonId', label:'Motivo Traslado', column: 4, load: { queryId: 345 }, search: true},
                { type: 'select', name:'customerId', label:'Cliente', column: 4, load: { queryId: 361 }, search: true},
                { type: 'select', name: 'localId', label: 'Punto Partida', load: {queryId: 571}, column: 4, search: true,
                    parents: [
                        {name:'companyId', paramName: 'companyId', required: true}                    
                    ],
                    onChange: (value, values) => {
                        const address = getForm().getFieldApi('localId')?.getSelectedItem()?.address;
                        getForm().setFieldData('address', address);
                    } 
                },
                { name: 'address', label: 'Direccion Partida', column: 6},
                { type: 'radio', name: 'typeOrder', label: 'Seleccione', load: {dataList: [{value: 1, label: 'Sin Orden de Compra'},{value: 2, label: 'Con Orden de Compra'}]}, column: 6},
                { type: 'divider', label: 'Datos de Pedido del Cliente'},
                { type: 'hidden', name: 'documentRefId'},
                { name: 'documentRefNumber', label:'Pedido', column: 3},
                { type: 'button', column: 1, name: 'modal',
                    onClick: (value) => {
                        pageApi.showModal(DocRefModalConfig(getForm().mapFieldApi.customerId.value));
                    },
                },
                { type: 'date', name: 'broadcastDate',label:'Fecha Emisión', format: 'DD/MM/YYYY', inputFormat: DATE_FORMAT.YYYYMMDD, column: 4, readOnly: true},
                { type: 'date', name: 'deliverDate',label:'Fecha Entrega', format: 'DD/MM/YYYY', inputFormat: DATE_FORMAT.YYYYMMDD, column: 4, readOnly: true},
                { type: 'select', name: 'pointSaleId', label: 'Punto Venta', column: 4, search: true, load: { queryId: 229 },
                    parents: [
                        { name: 'customerId', paramName: 'customerId', required: true }
                    ],
                    onChange: (value, values) => {
                        const label = getForm().getFieldApi('pointSaleId')?.getSelectedItem()?.label;
                        getForm().setFieldData('startingPoint', label);
                    }
                },
                { name:'startingPoint', label:'Lugar de Entrega', column: 4},
                { type: 'divider', label: 'Datos de Transporte'},
                { type: 'select', name:'transportCompanyId', label:'Transportista', column: 4, load: { queryId: 344 }, search: true },
                { type: 'select', name:'driverId', label:'Chofer', column: 4, load: { queryId: 343 }, search: true,
                    onChange: (value, values) => {
                        const licenseNumber = getForm().getFieldApi('driverId').getSelectedItem()?.licenseNumber;
                        getForm().setFieldData('licenseNumber', licenseNumber);
                    }
                },
                { name: 'licenseNumber',label:'N° Brevete', column: 4},
                { type: 'select', name:'vehicleId', label:'Vehiculo', column: 4, load: { queryId: 113 }, search: true,
                    onChange: (value, values) => {
                        const plateNumber = getForm().getFieldApi('vehicleId').getSelectedItem()?.plateNumber;
                        getForm().setFieldData('plateNumber', plateNumber);
                    }
                },
                { name: 'plateNumber',label:'Placa', column: 4},
            ],
            onComponentLoad : (formApi, pageApi) =>{
                if (pageApi.getPageMode() !== PAGE_MODE.NEW)
                    formApi.load();
            }
        },
        bar: {
            fields: [
                {...BUTTON_DEFAULT.SAVE, onClick: () => getForm().save()},
                {...BUTTON_DEFAULT.UPDATE, onClick: () => getForm().update()},
                {...BUTTON_DEFAULT.RETURN, link: '/sales/referral-guide/referral-guide-admin'}
            ],
        },
        grid: {}
    }
}