import React from 'react';
import { BUTTON_DEFAULT } from '../../../../../framework/bar-button/BarButtonFw';
import GridFw, { COLUMN_DEFAULT } from '../../../../../framework/grid/GridFw';
import { ProductModal } from '../modals/ProductModal';

export const createItemsSection = (pageApi) => {

    const getGrid = () => pageApi.getComponent('itemsProformasGrid');

    const fnDelete = (value, row, index) => {
        getGrid().deleteItem(row.key);      
    }

    const itemConfig = {
        events: {
            afterAddEvent : (data) => {
                getGrid().addItem(data);
            }
        }
    }

    const ItemModalConfig = (data) =>  {
        return {
            width: 1600,
            maskClosable: false,
            component: <ProductModal name={data}  getParentApi={pageApi.getApi} config={itemConfig}  />
        }
    };


    const barHeaderConfig = {
        fields:[
            {...BUTTON_DEFAULT.ADD, label:'Agregar Item', onClick: ()=>{  pageApi.showModal(ItemModalConfig('')); }}
        ]
    }

    const gridConfig = {
        title: 'Items',
        rowId: 'itemReferralGuideId',
        showIndex: true,
        pagination: true,
        autoLoad: true,
        load: {
            validation: () => pageApi.getParamId()!=null, //solo se dispara si el valor es distinto de nulo
            queryId: 352,
            orderBy: 'itemReferralGuideId desc',
            params: {referralGuideId: pageApi.getParamId()},
            hideMessage: true, 
            hideMessageError: true
        },
        save: {
            queryId: 351,
            params: {referralGuideId: pageApi.getParamId()},
            hideMessage: true, 
            hideMessageError: true
        },
        fields: [
            {name:'itemTypeText', label:'Tipo Item'},
            {name:'description', label:'Item'},
            //{ type: 'select', name: 'unitMeasureId', label: 'Uni. Med.', labelName: 'unitMeasureIdDes', load: { queryId: 568, params: () => {return {masterItemId: getGrid()?.itemEditing?.masterItemId}}}, search: true, editable: true },
            {type: 'number', name:'shippingQuantity', label:'Cantidad Enviado', editable: true},
            {type: 'number', name:'unitPrice', label:'Precio Unitario', editable: true},
            {type: 'number', name:'amount', label:'Importe', editable: true},
            {type: 'number', name:'receivedAmount', label:'Cantidad Recibida', editable: true},
            {name:'lot', label:'N° Lote', editable: true},
            {...COLUMN_DEFAULT.DELETE, onClick: fnDelete}
        ],
        barHeader: barHeaderConfig
    };
    

    return (
        <GridFw
            name="itemsProformasGrid"
            getParentApi={pageApi.getApi}
            config={gridConfig} 
        />
    )
}