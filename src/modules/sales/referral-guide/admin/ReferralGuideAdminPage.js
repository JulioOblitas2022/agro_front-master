import React from 'react'
import BarButtonFw, { BUTTON_DEFAULT } from '../../../../framework/bar-button/BarButtonFw';
import FormFw from '../../../../framework/form/FormFw';
import GridFw from '../../../../framework/grid/GridFw';
import { BasePage } from '../../../../framework/pages/BasePage';
import { DATE_FORMAT } from "../../../../constants/constants";
import { COLUMN_DEFAULT } from "../../../../framework/grid/GridFw";
import { getFormDefaultParams } from "../../../shared/util/getFormDefaultParams";
import { getGridServerDefaultParams } from "../../../shared/util/getGridServerDefaultParams";

export class ReferralGuideAdminPage extends BasePage {

    createPagePropertiesBase(pageApi) {
        return { 
            title: 'BANDEJA DE GUIAS',
            barHeader:  {
                fields:[
                    {...BUTTON_DEFAULT.NEW, label:'Nuevo', link: '/sales/referral-guide/referral-guide/new'}
                ]
            }
        };
    }

    renderPage(pageApi) {

        const getGrid = () => pageApi.getComponent('grid');
        const getForm = () => pageApi.getComponent('form');
        
        const form = {
            ...getFormDefaultParams(),
            title: 'FILTROS DE BUSQUEDA',
            fields:[
                { name: 'documentNumber', label:'N° Guia', column: 4},
                { type: 'select', name:'customerId', label:'Cliente', column: 4, load: { queryId: 361 }, search: true},
            ]
        }

        const bar = { fields: [
            {...BUTTON_DEFAULT.SEARCH, onClick: ()=>getGrid().load({params:getForm().getData()})},
            {...BUTTON_DEFAULT.CLEAR, onClick: ()=>getForm().reset()}
        ]}

        const grid = {
            ...getGridServerDefaultParams({queryLoad: 347}),
            title: 'RESULTADOS',
            fields:[
                {name: 'documentNumber', label: 'N° Guia',},
                {name: 'customer', label: 'Cliente',},
                {type: 'date', name: 'entryDate', label: 'Fch. Emision', format: DATE_FORMAT.DDMMYYYY},
                {name: 'documentRefNumber', label: 'N° Pedido',},
                {...COLUMN_DEFAULT.VIEW, link: (value, row, index) => `/sales/referral-guide/referral-guide/${row.referralGuideId}`, label: 'Opciones', colSpan: 2 },
                {...COLUMN_DEFAULT.EDIT, link: (value, row, index) => `/sales/referral-guide/referral-guide/${row.referralGuideId}/update`, colSpan: 0 },
            ]
        }
        return (
            <>
                <FormFw
                    name="form"
                    getParentApi={pageApi.getApi}
                    config={form}
                />
                <BarButtonFw
                    name="bar"
                    getParentApi={pageApi.getApi}
                    config={bar}
                />
                <GridFw
                    name="grid"
                    getParentApi={pageApi.getApi}
                    config={grid}
                />
            </>
        )
    }
}