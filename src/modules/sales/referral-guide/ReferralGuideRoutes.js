import React from 'react'
import { Route, Switch, useRouteMatch } from 'react-router';
import { ReferralGuideAdminPage } from './admin/ReferralGuideAdminPage';
import { ReferralGuideDetailPage } from './detail/ReferralGuideDetailPage';

const ReferralGuideRoutes = () => {
    const { path } = useRouteMatch();
    return (
        <Switch>
            <Route exact path={`${path}/referral-guide-admin`} component={ReferralGuideAdminPage} />
            <Route path={`${path}/referral-guide`} component={ReferralGuideDetailPage} />
            <Route path={`${path}/referral-guide/:id`} component={ReferralGuideDetailPage} />
        </Switch>
    )
}

export default ReferralGuideRoutes;