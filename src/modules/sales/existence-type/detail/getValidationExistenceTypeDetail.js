
export const getValidationExistenceTypeDetail = () => {
    return {
        form: {
            description: [{ required: true, message: 'Descripcion es requerido' }],
            sunatCode: [{ required: true, message: 'Codigo Sunat es requerido' }],
            companyId: [{ required: true, message: 'Empresa es requerido' }],
        },
        grid: {

        },
        button: {

        },
        bar: {

        }
    }
}
