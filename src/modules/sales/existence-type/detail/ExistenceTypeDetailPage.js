import React from 'react'
import BarButtonFw from '../../../../framework/bar-button/BarButtonFw';
import FormFw from '../../../../framework/form/FormFw';
import { BasePage } from '../../../../framework/pages/BasePage';
import { getConfigExistenceTypeDetail } from './getConfigExistenceTypeDetail';
import { getModeExistenceTypeDetail } from './getModeExistenceTypeDetail';
import { getValidationExistenceTypeDetail } from './getValidationExistenceTypeDetail';

export class ExistenceTypeDetailPage extends BasePage {
    
    createPageProperties = (pageApi) => {
        return {
            title: 'REGISTRAR T.EXISTENCIA',//'REGISTRO DE MOTIVO NC/ND',
            validation: getValidationExistenceTypeDetail,
            mode: getModeExistenceTypeDetail
        };
    }

    renderPage(pageApi) {

        const config = getConfigExistenceTypeDetail(pageApi);

        return (
            <>
                <FormFw
                    name="form"
                    getParentApi={pageApi.getApi}
                    config={config.form}
                />
                <BarButtonFw
                    name="bar"
                    getParentApi={pageApi.getApi}
                    config={config.bar}
                />
            </>
        )
    }
}