import { PAGE_MODE } from '../../../../constants/constants';
import { BUTTON_DEFAULT } from '../../../../framework/bar-button/BarButtonFw';

export const getConfigExistenceTypeDetail = (pageApi) => {

    const getForm = () => pageApi.getComponent('form');

    return {
        form: {
            load: {
                queryId: 216,
                params: { existencesTypeId: pageApi.getParamId() },
            },
            update: {
                queryId: 218,
                postLink: (resp, values) => '/sales/existence-type/existence-type-admin'
            },
            save: {
                queryId: 217,
                postLink: (resp, values) => '/sales/existence-type/existence-type-admin'
            },
            fields: [
                { name: 'description', label: 'Descripcion', column: 6 },
                { name: 'sunatCode', label: 'Codigo Sunat', column: 6 },
                { type: 'select', name: 'companyId', label: 'Empresa', column: 6, load: { queryId: 554, } },
            ],
            onComponentLoad: (formApi, pageApi) => {
                if (pageApi.getPageMode() !== PAGE_MODE.NEW)
                    formApi.load();
            }
        },
        bar: {
            fields: [
                { ...BUTTON_DEFAULT.SAVE, onClick: () => getForm().save() },
                { ...BUTTON_DEFAULT.UPDATE, onClick: () => getForm().update() },
                { ...BUTTON_DEFAULT.RETURN, link: '/sales/existence-type/existence-type-admin' }
            ],
        },
        grid: {}
    }
}