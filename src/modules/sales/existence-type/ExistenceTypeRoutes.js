import React from 'react'
import { Route, Switch, useRouteMatch } from 'react-router';
import { ExistenceTypeAdminPage } from './admin/ExistenceTypeAdminPage';
import { ExistenceTypeDetailPage } from './detail/ExistenceTypeDetailPage';

const ExistenceTypeRoutes = () => {
    const { path } = useRouteMatch();
    return (
        <Switch>
            <Route exact path={`${path}/existence-type-admin`} component={ExistenceTypeAdminPage} />
            <Route path={`${path}/existence-type`} component={ExistenceTypeDetailPage} />
            <Route path={`${path}/existence-type/:id`} component={ExistenceTypeDetailPage} />
        </Switch>
    )
}

export default ExistenceTypeRoutes;