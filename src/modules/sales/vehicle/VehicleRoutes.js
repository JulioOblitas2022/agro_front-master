import React from 'react';
import { Route, Switch, useRouteMatch } from 'react-router-dom';

import { VehicleAdminPage } from './admin/VehicleAdminPage';
import { VehicleDetailPage } from './detail/VehicleDetailPage';

const VehicleRoutes = () => {
    const { path } = useRouteMatch();
    return (
        <Switch>
            <Route exact path={`${path}/vehicle-admin`} component={VehicleAdminPage} />
            <Route path={`${path}/vehicle`} component={VehicleDetailPage} />
            <Route path={`${path}/vehicle/:id`} component={VehicleDetailPage} />
        </Switch>
    )
}

export default VehicleRoutes;
