
export const getValidationVehicleDetail = () => {
    return {
        form: {
            mark: [{ required: true, message: 'Marca es requerida' }],
            plateNumber: [{ required: true, message: 'Placa Sunat es requerida' }],
            companyId: [{ required: true, message: 'Empresa es requerido' }],
        },
        grid: {

        },
        button: {

        },
        bar: {

        }
    }
}
