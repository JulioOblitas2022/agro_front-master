import React from 'react'
import BarButtonFw from '../../../../framework/bar-button/BarButtonFw';
import FormFw from '../../../../framework/form/FormFw';
import { BasePage } from '../../../../framework/pages/BasePage';
import { getConfigVehicleDetail } from './getConfigVehicleDetail';
import { getModeVehicleDetail } from './getModeVehicleDetail';
import { getValidationVehicleDetail } from './getValidationVehicleDetail';

export class VehicleDetailPage extends BasePage {
    
    createPageProperties = (pageApi) => {
        return {
            title: 'REGISTRO DE VEHICULOS',
            validation: getValidationVehicleDetail,
            mode: getModeVehicleDetail
        };
    }

    renderPage(pageApi) {

        const config = getConfigVehicleDetail(pageApi);

        return (
            <>
                <FormFw
                    name="form"
                    getParentApi={pageApi.getApi}
                    config={config.form}
                />
                <BarButtonFw
                    name="bar"
                    getParentApi={pageApi.getApi}
                    config={config.bar}
                />
            </>
        )
    }
}