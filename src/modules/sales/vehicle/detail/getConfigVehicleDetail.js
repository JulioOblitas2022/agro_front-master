import { PAGE_MODE } from "../../../../constants/constants";
import { BUTTON_DEFAULT } from "../../../../framework/bar-button/BarButtonFw";

export const getConfigVehicleDetail = (pageApi) => {

    const getForm = () => pageApi.getComponent('form');

    return {
        form: {
            load: {
                queryId: 107,
                params: { vehicleId: pageApi.getParamId() },
            },
            update: {
                queryId: 109,
                postLink: (resp, values) => '/sales/vehicle/vehicle-admin'
            },
            save: {
                queryId: 108,
                postLink: (resp, values) => '/sales/vehicle/vehicle-admin'
            },
            fields: [
                { name: 'mark', label: 'Marca', column: 6 },
                { name: 'plateNumber', label: 'Placa', column: 6 },
                { type: 'select', name: 'companyId', label: 'Empresa', column: 6, load: { queryId: 554, } },
            ],
            onComponentLoad: (formApi, pageApi) => {
                if (pageApi.getPageMode() !== PAGE_MODE.NEW)
                    formApi.load();
            }
        },
        bar: {
            fields: [
                { ...BUTTON_DEFAULT.SAVE, onClick: () => getForm().save() },
                { ...BUTTON_DEFAULT.UPDATE, onClick: () => getForm().update() },
                { ...BUTTON_DEFAULT.RETURN, link: '/sales/vehicle/vehicle-admin' }
            ],
        },
        grid: {}
    }
}
