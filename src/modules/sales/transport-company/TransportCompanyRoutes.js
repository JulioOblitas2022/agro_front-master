import React from 'react';
import { Route, Switch, useRouteMatch } from 'react-router-dom';

import { TransportCompanyAdminPage } from './admin/TransportCompanyAdminPage';
import { TransportCompanyDetailPage } from './detail/TransportCompanyDetailPage';

const TransportCompanyRoutes = () => {
    const { path } = useRouteMatch();
    return (
        <Switch>
            <Route exact path={`${path}/transport-company-admin`} component={TransportCompanyAdminPage} />
            <Route path={`${path}/transport-company`} component={TransportCompanyDetailPage} />
            <Route path={`${path}/transport-company/:id`} component={TransportCompanyDetailPage} />
        </Switch>
    )
}

export default TransportCompanyRoutes;
