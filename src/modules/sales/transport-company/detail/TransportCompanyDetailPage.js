import React from 'react'
import BarButtonFw from '../../../../framework/bar-button/BarButtonFw';
import FormFw from '../../../../framework/form/FormFw';
import { BasePage } from '../../../../framework/pages/BasePage';
import { getConfigTransportCompanyDetail } from './getConfigTransportCompanyDetail';
import { getModeTransportCompanyDetail } from './getModeTransportCompanyDetail';
import { getValidationTransportCompanyDetail } from './getValidationTransportCompanyDetail';

export class TransportCompanyDetailPage extends BasePage {
    
    createPageProperties = (pageApi) => {
        return {
            title: 'REGISTRO DE EMPRESA TRANSPORTE',
            validation: getValidationTransportCompanyDetail,
            mode: getModeTransportCompanyDetail
        };
    }

    renderPage(pageApi) {

        const config = getConfigTransportCompanyDetail(pageApi);

        return (
            <>
                <FormFw
                    name="form"
                    getParentApi={pageApi.getApi}
                    config={config.form}
                />
                <BarButtonFw
                    name="bar"
                    getParentApi={pageApi.getApi}
                    config={config.bar}
                />
            </>
        )
    }
}