import { PAGE_MODE } from "../../../../constants/constants";
import { BUTTON_DEFAULT } from "../../../../framework/bar-button/BarButtonFw";
import { UserUtil } from "../../../../util/UserUtil";
import { B_SELECT_CONSTANTS } from "../../../shared/constantes/BSelectConstants";

export const getConfigTransportCompanyDetail = (pageApi) => {

    const getForm = () => pageApi.getComponent('form');

    return {
        form: {
            load: {
                queryId: 132,
                params: { transportCompanyId: pageApi.getParamId() },
            },
            update: {
                queryId: 134,
                params: {modifiedBy: UserUtil.getUserId(), transportCompanyId: pageApi.getParamId()},
                postLink: (resp, values) => '/sales/transport-company/transport-company-admin'
            },
            save: {
                queryId: 133,
                params: {createdBy: UserUtil.getUserId()},
                postLink: (resp, values) => '/sales/transport-company/transport-company-admin'
            },
            title: 'DATOS PRINCIPALES',
            fields: [
                { type: 'select', name: 'providerId', label: 'Proveedor', column: 4, load: { queryId: 159 }, search: true },
                { type: 'select', name: 'companyId', label: 'Empresa', column: 4, load: { queryId: 554, } },

            ],
            onComponentLoad: (formApi, pageApi) => {
                if (pageApi.getPageMode() !== PAGE_MODE.NEW)
                    formApi.load();
            }
        },
        bar: {
            fields: [
                { ...BUTTON_DEFAULT.SAVE, onClick: () => getForm().save() },
                { ...BUTTON_DEFAULT.UPDATE, onClick: () => getForm().update() },
                { ...BUTTON_DEFAULT.RETURN, link: '/sales/transport-company/transport-company-admin' }
            ],
        },
        grid: {}
    }
}
