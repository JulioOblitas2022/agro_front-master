
export const getValidationTransportCompanyDetail = () => {
    return {
        form: {
            providerId: [{ required: true, message: 'Proveedor es requerido' }],
            rucNumber: [{ required: true, message: 'Nro. RUC es requerido' }],
            address: [{ required: true, message: 'Direccion es requerida' }],
            departamentoId: [{ required: true, message: 'Departamento es requerido' }],
            provinciaId: [{ required: true, message: 'Provincia es requerida' }],
            distritoId: [{ required: true, message: 'Distrito es requerido' }],
            companyId: [{ required: true, message: 'Empresa es requerido' }],
        },
        grid: {

        },
        button: {

        },
        bar: {

        }
    }
}
