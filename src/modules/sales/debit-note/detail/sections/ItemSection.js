import React from 'react';
import FormGridFw, { FORM_GRID_MODE } from '../../../../../framework/form-grid/FormGridFw';

export const createItemSection = (pageApi) => {

    const gridConfig = {
        title: 'Items',
        mode: FORM_GRID_MODE.FORM_ADD,
        collapsible: true,
        form: {
            dataOptions: {
                out: {
                    fnTransform: (values, api) => {
                        let result = [];
                        if (values['masterItemId'].length > 0){
                            let items = api.getFieldApi('masterItemId').getSelectedItem();
                            result = items.map(item => ({masterItemId: item.value, item: item.label, unitMeasuse: '', itemTypeText: item.itemTypeText}));
                        }
                        return result;
                    }
                }
            },
            fields:[
                {type:'select', name:'familyId', label:'Seleccionar Familia', column: 6, load: {queryId: 50}},
                {type:'select', name:'masterItemId', label: 'Seleccione Items',multiple:true, column: 6, load: {queryId: 191},
                    parents: [
                        {name:'familyId', paramName: 'familyId', required: true}
                    ]
                }
            ],
        },
        grid: {
            title: ' ',
            rowId: 'noteDetailId',
            showIndex: true,
            pagination: true,
            autoLoad: true,
            collapsible: true,
            load: {
                validation: () => pageApi.getParamId()!=null, //solo se dispara si el valor es distinto de nulo
                queryId: 373,
                orderBy: 'noteDetailId desc',
                params: {noteId: pageApi.getParamId()}
            },
            save: {
                queryId: 372,
                params: {noteId: pageApi.getParamId()}
            },
            fields:[
                {name:'item', label:'Producto', width: 100, editable: false},
                {name:'unitMeasure', label:'Uni. Med.', width: 100, editable: false},
                {type: 'number', name:'quantity', label:'Cantidad', width: 100, editable: true},
                {type: 'number', name:'unitPrice', label:'Precio Unitario', width: 100, editable: true},
                {type: 'number', name:'total', label:'Importe Total', width: 100, editable: false},
                {type: 'select', name:'saleTypeId', label:'Tipo Venta', width: 100, editable: true,
                    load: {
                        dataList: [
                            {value: '1', label: 'Retiro'},
                            {value: '2', label: 'Grabado'},
                        ]
                    } 
                },
            ],
        }
    };
    return (
        <>
            <FormGridFw
                name="itemGrid"
                getParentApi={pageApi.getApi}
                config={gridConfig} 
            />
        </>
    )
}