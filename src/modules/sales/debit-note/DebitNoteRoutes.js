import React from 'react';
import { Route, Switch, useRouteMatch } from 'react-router-dom';

import { DebitNoteAdminPage } from './admin/DebitNoteAdminPage';
import { DebitNoteDetailPage } from './detail/DebitNoteDetailPage';

const DebitNoteRoutes = () => {
    const { path } = useRouteMatch();
    return (
        <Switch>
            <Route exact path={`${path}/debit-note-admin`} component={DebitNoteAdminPage} />
            <Route path={`${path}/debit-note`} component={DebitNoteDetailPage} />
            <Route path={`${path}/debit-note/:id`} component={DebitNoteDetailPage} /> 
        </Switch>
    )
}

export default DebitNoteRoutes;