import React from 'react'
import BarButtonFw from '../../../../framework/bar-button/BarButtonFw';
import FormFw from '../../../../framework/form/FormFw';
import { BasePage } from '../../../../framework/pages/BasePage';
import { getConfigProformasDetail } from './getConfigProformasDetail';
import { getModeProformasDetail } from './getModeProformasDetail';
import { getValidationProformasDetail } from './getValidationProformasDetail';
import { createItemsProformasSection } from './sections/ItemsProformasSection';

export class ProformasDetailPage extends BasePage {
    
    createPageProperties = (pageApi) => {
        return {
            title: 'REGISTRO DE VENTAS NO EMITIDAS',
            validation: getValidationProformasDetail,
            mode: getModeProformasDetail
        };
    }

    renderPage(pageApi) {

        const config = getConfigProformasDetail(pageApi);

        return (
            <>
                <FormFw
                    name="form"
                    getParentApi={pageApi.getApi}
                    config={config.form}
                />
                {createItemsProformasSection(pageApi)}
                <BarButtonFw
                    name="bar"
                    getParentApi={pageApi.getApi}
                    config={config.bar}
                />
            </>
        )
    }
}