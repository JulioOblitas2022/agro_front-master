import { MODE_SHORTCUT } from '../../../../constants/constants'

export const getModeProformasDetail = () => {
    return {
        new: {
            form: {
                // hide: ['docTypeId']
                read: ['docTypeId']
            },
            bar: {
                show: ['save', 'return']
            }
        },
        update:{
            form: {
                read: ['docTypeId']
            },
            bar: {
                show: ['update', 'return']
            }
        },
        read:{
            form: {
                read: [MODE_SHORTCUT.ALL]
            },
            bar: {
                show: ['return']
            }
        }
    }
}
