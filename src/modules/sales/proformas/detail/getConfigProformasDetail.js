import { DATE_FORMAT, PAGE_MODE } from "../../../../constants/constants";
import { BUTTON_DEFAULT } from "../../../../framework/bar-button/BarButtonFw";
import { UserUtil } from "../../../../util/UserUtil";
import { B_SELECT_CONSTANTS } from "../../../shared/constantes/BSelectConstants";

export const getConfigProformasDetail = (pageApi) => {
    
    const getForm = () => pageApi.getComponent('form');
    const getItemsProformasGrid = () => pageApi.getComponent('itemsProformasGrid.grid');

    return {
        form: {
            load:{
                queryId: 340,
                params: {proformaId: pageApi.getParamId()},
                fnOk: (resp) => {
                    getItemsProformasGrid().load({params: pageApi.getParamId()});
                }
            },
            update:{
                queryId: 339,
                postLink: (resp, values) => '/sales/proformas/proformas-admin',
                params: {modifiedBy: UserUtil.getUserId()},
                fnOk: (resp) => {
                    getItemsProformasGrid().save();
                }
            },
            save:{
                queryId: 338,
                postLink: (resp, values) => '/sales/proformas/proformas-admin',
                params: {createdBy: UserUtil.getUserId()},
                fnOk: (resp) => {
                    const proformaId = resp.dataObject?.proformaId;
                    if (proformaId){
                        getItemsProformasGrid().save({params:{proformaId}});
                    }
                        
                }
            },
            fields:[
                {type: 'date', name:'entryDate', label:'Fecha Emision', format: 'DD/MM/YYYY', inputFormat: DATE_FORMAT.YYYYMMDD, column: 4},
                {type:'select', name:'coinId', label:'Moneda', column: 4, search: true, load: {queryId: 187}},
                {type:'select', name:'operationTypeId', label:'Tipo Operacion', column: 4, search: true, load: {queryId: 77}},
                {type: 'select', name:'customerId', label:'Cliente', column: 4, load: { queryId: 159 }, search: true},
                {type:'select', name:'docTypeId', label:'Tipo Doc', column: 4, search: true, load: {queryId: 195}, search: true, value: 68},
                {name: 'docNumber', label: 'N° Doc', column: 4},
                {name: 'seatNumber', label: 'N° Asiento', column: 4},
                {type: 'number', name: 'exchangeRate', label: 'Tipo Cambio', column: 4},
                {type: 'select', name:'reasonId', label:'Motivo', column: 4, load: { queryId: 189 }, search: true},
                {...B_SELECT_CONSTANTS.DOC_REF, label: 'Tipo Doc. Ref', search: true},
                {type: 'select', name:'purchaseOrderId', label:'Orden Compra', column: 4, load: { queryId: 326 }, search: true },
                {type: 'select', name:'paymentConditionId', label:'Cond. Pago', column: 4, load: { queryId: 188 }, search: true },
                {type: 'date', name:'dueDate', label:'Fecha Vencimiento', format: 'DD/MM/YYYY', inputFormat: DATE_FORMAT.YYYYMMDD},
                {type: 'select', name:'sellerId', label:'Vendedor', column: 4, load: { queryId: 335 }, search: true},
                {type: 'textarea', name: 'glossary', label:'Glosa', column: 8, rows: 5},
                {type: 'select', name:'billingTypeId', label:'Tipo Fac.', column: 4, load: { queryId: 336 }},
            ],
            onComponentLoad : (formApi, pageApi) =>{
                if (pageApi.getPageMode() !== PAGE_MODE.NEW)
                    formApi.load();
            }
        },
        bar: {
            fields: [
                {...BUTTON_DEFAULT.SAVE, onClick: () => getForm().save()},
                {...BUTTON_DEFAULT.UPDATE, onClick: () => getForm().update()},
                {...BUTTON_DEFAULT.RETURN, link: '/sales/proformas/proformas-admin'}
            ],
        },
        grid: {}
    }
}
