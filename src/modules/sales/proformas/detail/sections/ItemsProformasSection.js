import React from 'react'
import { DATE_FORMAT } from '../../../../../constants/constants';
import FormGridFw, { FORM_GRID_MODE } from '../../../../../framework/form-grid/FormGridFw';

export const createItemsProformasSection = (pageApi) => {

    const gridConfig = {
        title: 'ITEMS',
        mode: FORM_GRID_MODE.FORM_ADD,
        collapsible: true,
        form: {
            dataOptions: {
                out: {
                    fnTransform: (values, api) => {
                        let result = [];
                        if (values['masterItemId'].length > 0){
                            let items = api.getFieldApi('masterItemId').getSelectedItem();
                            result = items.map(item => ({masterItemId: item.value, description: item.label, unitMeasuse: '', itemTypeText: item.itemTypeText}));
                        }
                        return result;
                    }
                }
            },
            fields:[
                {type:'select', name:'familyId', label:'Seleccionar Productos', column: 6, load: {queryId: 50}},
                {type:'select', name:'masterItemId', label: 'Items', multiple:true, column: 6, load: {queryId: 191},
                    parents: [
                        {name:'familyId', paramName: 'familyId', required: true}
                    ]
                }
            ],
        },
        grid: {
            title: '',
            rowId: 'itemProformaId',
            showIndex: true,
            pagination: true,
            autoLoad: true,
            collapsible: true,
            load: {
                validation: () => pageApi.getParamId()!=null, //solo se dispara si el valor es distinto de nulo
                queryId: 342,
                orderBy: 'itemProformaId desc',
                params: {proformaId: pageApi.getParamId()}
            },
            save: {
                queryId: 341,
                params: {proformaId: pageApi.getParamId()}
            },
            fields:[
                {name:'itemTypeText', label:'Tipo Item', width: 200},
                {name:'description', label:'Item', width: 200},
                {name:'unitMeasureId', label:'Uni. Med.', width: 200},
                {type: 'number', name:'quantity', label:'Cantidad', editable: true},
                {type: 'number', name:'unitPrice', label:'Precio Unitario', editable: true},
                {type: 'number', name:'grossPrice', label:'Precio Bruto', editable: true},
                {type: 'date', name:'deliveryDate', label:'Fecha Entrega', format: 'DD/MM/YYYY', inputFormat: DATE_FORMAT.YYYYMMDD, editable: true},
            ],
        }
    };

    return (
        <FormGridFw
            name="itemsProformasGrid"
            getParentApi={pageApi.getApi}
            config={gridConfig} 
        />
    )
}