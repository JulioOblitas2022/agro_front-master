import React from 'react'
import { Route, Switch, useRouteMatch } from 'react-router';
import { ProformasAdminPage } from './admin/ProformasAdminPage';
import { ProformasDetailPage } from './detail/ProformasDetailPage';

const ProformasRoutes = () => {
    const { path } = useRouteMatch();
    return (
        <Switch>
            <Route exact path={`${path}/proformas-admin`} component={ProformasAdminPage} />
            <Route path={`${path}/proformas`} component={ProformasDetailPage} />
            <Route path={`${path}/proformas/:id`} component={ProformasDetailPage} />
        </Switch>
    )
}

export default ProformasRoutes;