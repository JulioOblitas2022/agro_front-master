import { BUTTON_DEFAULT } from "../../../../framework/bar-button/BarButtonFw";
import { COLUMN_DEFAULT } from "../../../../framework/grid/GridFw";
import { B_SELECT_CONSTANTS } from "../../../shared/constantes/BSelectConstants";
import { getFormDefaultParams } from "../../../shared/util/getFormDefaultParams";
import { getGridServerDefaultParams } from "../../../shared/util/getGridServerDefaultParams";

export const getConfigProformasAdmin = (pageApi) => {
    const getGrid = () => pageApi.getComponent('grid');
    const getForm = () => pageApi.getComponent('form');
    
    return {
        form: {
            ...getFormDefaultParams(),
            title: 'FILTROS DE BUSQUEDA',
            fields:[
                {name:'docNumber', label:'Nro Doc.', column: 3},
                {type: 'select', name:'customerId', label:'Cliente', column: 3, load: { queryId: 159 }, search: true},
                {type: 'date', name:'startDate', label:'Desde', column: 2},
                {type: 'date', name:'endDate', label:'Hasta', column: 2},
                {...B_SELECT_CONSTANTS.ACTIVE, column: 2}
            ]
        },
        bar: { fields: [
            {...BUTTON_DEFAULT.SEARCH, onClick: ()=>getGrid().load({params:getForm().getData()})},
            {...BUTTON_DEFAULT.CLEAR, onClick: ()=>getForm().reset()}
        ]},
        grid: {
            ...getGridServerDefaultParams({queryLoad: 337}),
            title: 'RESULTADOS',
            fields:[
                {name: 'proformaId', label: 'Nro. Registro',},
                {name: 'docTypeId', label: 'T.D',},
                {name: 'docNumber', label: 'Nro. Documento',},
                {name: 'entryDate', label: 'Fch. Doc',},
                {name: 'customerId', label: 'Cliente',},
                {name: 'mon', label: 'Mon',},
                {name: 'operationTypeId', label: 'T.C',},
                {name: 'grossAmount', label: 'Imp. Bruto',},
                {name: 'unaffectedAmount', label: 'Imp. Inafecto',},
                {name: 'igvTotal', label: 'IGV. Imp. Total',},
                {name: 'balance', label: 'Saldo',},
                {...COLUMN_DEFAULT.VIEW, link: (value, row, index) => `/sales/proformas/proformas/${row.proformaId}` },
                {...COLUMN_DEFAULT.EDIT, link: (value, row, index) => `/sales/proformas/proformas/${row.proformaId}/update` },
            ]
        }
    }
}
