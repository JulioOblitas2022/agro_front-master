import { BUTTON_DEFAULT } from '../../../../framework/bar-button/BarButtonFw';
import { B_SELECT_CONSTANTS } from '../../../shared/constantes/BSelectConstants';
import { getFormDefaultParams } from '../../../shared/util/getFormDefaultParams';
import { getGridServerDefaultParams } from '../../../shared/util/getGridServerDefaultParams';

export const getConfigOrderAdmin = (pageApi) => {
    const getGrid = () => pageApi.getComponent('grid');
    const getForm = () => pageApi.getComponent('form');
    
    return {
        form: {
            ...getFormDefaultParams(),
            title: 'FILTROS DE BUSQUEDA',
            fields:[
                {name:'code', label:'Codigo', column: 3},
                {name:'description', label:'Descripcion', column: 3},
                {type: 'date', name:'startDate', label:'Desde', column: 2},
                {type: 'date', name:'endDate', label:'Hasta', column: 2},
                {...B_SELECT_CONSTANTS.ACTIVE, column: 2}
            ]
        },
        bar: { fields: [
            {...BUTTON_DEFAULT.SEARCH, onClick: ()=>getGrid().load({params:getForm().getData()})},
            {...BUTTON_DEFAULT.CLEAR, onClick: ()=>getForm().reset()}
        ]},
        grid: {
            ...getGridServerDefaultParams({queryLoad: 0}),
            title: 'RESULTADOS',
            fields:[
                {name: 'customer', label: 'Cliente',},
                {name: 'pointSale', label: 'Punto Venta',},
                {name: 'orderNumber', label: 'Nro. Orden',},
                {name: 'deliveryDate', label: 'Fecha Entrega',},
                {name: 'guideNumber', label: 'Numero Guia',},
                {name: 'dispatched', label: 'Despachado',},
                // {...COLUMN_DEFAULT.VIEW, link: (value, row, index) => `/sales/order/order/${row.orderId}` },
                // {...COLUMN_DEFAULT.EDIT, link: (value, row, index) => `/sales/order/order/${row.orderId}/update` },
                // {...COLUMN_DEFAULT.DELETE, process: {fnOk: (result)=>getGrid().load({params:getForm().getData()}),filter: (value, row, index) => ({queryId: 0, params: {orderId: row.orderId}}) }}
            ]
        }
    }
}
