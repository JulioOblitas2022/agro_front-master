import React from 'react'
import { Route, Switch, useRouteMatch } from 'react-router';
import { OrderAdminPage } from './admin/OrderAdminPage';
import { OrderDetailPage } from './detail/OrderDetailPage';

const OrderRoutes = () => {
    const { path } = useRouteMatch();
    return (
        <Switch>
            <Route exact path={`${path}/order-admin`} component={OrderAdminPage} />
            <Route path={`${path}/order`} component={OrderDetailPage} />
            <Route path={`${path}/order/:id`} component={OrderDetailPage} />
        </Switch>
    )
}

export default OrderRoutes;