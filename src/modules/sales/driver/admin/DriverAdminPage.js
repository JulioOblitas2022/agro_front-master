import React from 'react'
import BarButtonFw, { BUTTON_DEFAULT } from '../../../../framework/bar-button/BarButtonFw';
import FormFw from '../../../../framework/form/FormFw';
import GridFw from '../../../../framework/grid/GridFw';
import { BasePage } from '../../../../framework/pages/BasePage';
import { DATE_FORMAT } from '../../../../constants/constants';
import { COLUMN_DEFAULT } from '../../../../framework/grid/GridFw';
import { getFormDefaultParams } from '../../../shared/util/getFormDefaultParams';
import { getGridServerDefaultParams } from '../../../shared/util/getGridServerDefaultParams';

export class DriverAdminPage extends BasePage {
    createPagePropertiesBase(pageApi) {
        return { 
            title: 'BANDEJA DE CHOFERES',
            barHeader: {
                fields:[
                    {...BUTTON_DEFAULT.NEW, label:'Nuevo', link: '/sales/driver/driver/new'}
                ]
            }
        };
    }

    renderPage(pageApi) {
        const getGrid = () => pageApi.getComponent('grid');
        const getForm = () => pageApi.getComponent('form');

        const form = {
            ...getFormDefaultParams(),
            title: 'FILTROS DE BUSQUEDA',
            fields: [
                { type: 'date', name: 'dateFrom', label: 'Fecha Reg. Desde', column: 4, format: 'DD/MM/YYYY', inputFormat: DATE_FORMAT.YYYYMMDD, },
                { type: 'date', name: 'dateUntil', label: 'Fecha Reg. Hasta', column: 4, format: 'DD/MM/YYYY', inputFormat: DATE_FORMAT.YYYYMMDD, },
                { type: 'select', name: 'companyId', label: 'Empresa', column: 4, load: { queryId: 554, } },
            ]
        }

        const bar = {
            fields: [
                { ...BUTTON_DEFAULT.SEARCH, onClick: () => getGrid().load({ params: getForm().getData() }) },
                { ...BUTTON_DEFAULT.CLEAR, onClick: () => getForm().reset() }
            ]
        }

        const grid = {
            ...getGridServerDefaultParams({ queryLoad: 117 }),
            title: 'LISTA DE CHOFERES',
            fields: [
                { name: 'employee', label: 'Chofer', },
                { name: 'category', label: 'Categoria', },
                { name: 'licenseNumber', label: 'Num. Brevete', },
                { name: 'mark', label: 'Vehículo', },
                { name: 'plateNumber', label: 'Placa', },
                { name: 'companyName', label: 'EMPRESA', },
                { ...COLUMN_DEFAULT.VIEW, link: (value, row, index) => `/sales/driver/driver/${row.driverId}`, label: 'Opciones', colSpan: 3 },
                { ...COLUMN_DEFAULT.EDIT, link: (value, row, index) => `/sales/driver/driver/${row.driverId}/update`, colSpan: 0 },
                { ...COLUMN_DEFAULT.DELETE, process: { fnOk: (result) => getGrid().load({ params: getForm().getData() }), filter: (value, row, index) => ({ queryId: 122, params: { driverId: row.driverId } }) }, colSpan: 0 }
            ]
        }
        return (
            <>
                <FormFw
                    name="form"
                    getParentApi={pageApi.getApi}
                    config={form}
                />
                <BarButtonFw
                    name="bar"
                    getParentApi={pageApi.getApi}
                    config={bar}
                />
                <GridFw
                    name="grid"
                    getParentApi={pageApi.getApi}
                    config={grid}
                />
            </>
        )
    }
}