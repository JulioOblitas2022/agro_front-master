
export const getValidationDriverDetail = () => {
    return {
        form: {
            employeeId: [{ required: true, message: 'Chofer es requerido' }],
            category: [{ required: true, message: 'Categoria - Brevete Sunat es requerida' }],
            licenseNumber: [{ required: true, message: 'Nro. Brevete es requerido' }],
            vehicleId: [{ required: true, message: 'Vehiculo Sunat es requerido' }],
            companyId: [{ required: true, message: 'Empresa es requerido' }],
        },
        grid: {

        },
        button: {

        },
        bar: {

        }
    }
}
