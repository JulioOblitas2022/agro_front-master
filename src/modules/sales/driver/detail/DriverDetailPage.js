import React from 'react'
import BarButtonFw from '../../../../framework/bar-button/BarButtonFw';
import FormFw from '../../../../framework/form/FormFw';
import { BasePage } from '../../../../framework/pages/BasePage';
import { getConfigDriverDetail } from './getConfigDriverDetail';
import { getModeDriverDetail } from './getModeDriverDetail';
import { getValidationDriverDetail } from './getValidationDriverDetail';

export class DriverDetailPage extends BasePage {
    
    createPageProperties = (pageApi) => {
        return {
            title: 'REGISTRO DE CHOFERES',
            validation: getValidationDriverDetail,
            mode: getModeDriverDetail
        };
    }

    renderPage(pageApi) {

        const config = getConfigDriverDetail(pageApi);

        return (
            <>
                <FormFw
                    name="form"
                    getParentApi={pageApi.getApi}
                    config={config.form}
                />
                <BarButtonFw
                    name="bar"
                    getParentApi={pageApi.getApi}
                    config={config.bar}
                />
            </>
        )
    }
}