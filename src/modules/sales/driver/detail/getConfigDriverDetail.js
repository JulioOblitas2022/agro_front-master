import { PAGE_MODE } from "../../../../constants/constants";
import { BUTTON_DEFAULT } from "../../../../framework/bar-button/BarButtonFw";
import { UserUtil } from "../../../../util/UserUtil";

export const getConfigDriverDetail = (pageApi) => {

    const getForm = () => pageApi.getComponent('form');

    return {
        form: {
            load: {
                queryId: 118,
                params: { driverId: pageApi.getParamId() },
            },
            update: {
                queryId: 121,
                postLink: (resp, values) => '/sales/driver/driver-admin',
                params: { modifiedBy: UserUtil.getUserId(), driverId: pageApi.getParamId() },
            },
            save: {
                queryId: 120,
                postLink: (resp, values) => '/sales/driver/driver-admin',
                params: { createdBy: UserUtil.getUserId() },
            },
            fields: [
                { type: 'select', name: 'employeeId', label: 'Chofer', column: 4, load: {queryId: 309}, search: true },
                { name: 'category', label: 'Categoria', column: 4},
                { name: 'licenseNumber', label: 'Num. Brevete', column: 4 },
                { type: 'select', name: 'vehicleId', label: 'Vehiculo', column: 4, load: { queryId: 113 } },
                { type: 'select', name: 'companyId', label: 'Empresa', column: 4, load: { queryId: 554, } },
            ],
            onComponentLoad: (formApi, pageApi) => {
                if (pageApi.getPageMode() !== PAGE_MODE.NEW)
                    formApi.load();
            }
        },
        bar: {
            fields: [
                { ...BUTTON_DEFAULT.SAVE, onClick: () => getForm().save() },
                { ...BUTTON_DEFAULT.UPDATE, onClick: () => getForm().update() },
                { ...BUTTON_DEFAULT.RETURN, link: '/sales/driver/driver-admin' }
            ],
        },
        grid: {}
    }
}