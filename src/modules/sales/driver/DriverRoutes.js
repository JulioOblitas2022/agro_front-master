import React from 'react';
import { Route, Switch, useRouteMatch } from 'react-router-dom';

import { DriverAdminPage } from './admin/DriverAdminPage';
import { DriverDetailPage } from './detail/DriverDetailPage';

const DriverRoutes = () => {
    const { path } = useRouteMatch();
    return (
        <Switch>
            <Route exact path={`${path}/driver-admin`} component={DriverAdminPage} />
            <Route path={`${path}/driver`} component={DriverDetailPage} />
            <Route path={`${path}/driver/:id`} component={DriverDetailPage} />
        </Switch>
    )
}

export default DriverRoutes;