import { BUTTON_DEFAULT } from '../../../../framework/bar-button/BarButtonFw';
import { COLUMN_DEFAULT } from '../../../../framework/grid/GridFw';
import { getFormDefaultParams } from '../../../shared/util/getFormDefaultParams';
import { getGridServerDefaultParams } from '../../../shared/util/getGridServerDefaultParams';

export const getConfigCreditNoteAdmin = (pageApi) => {
    const getGrid = () => pageApi.getComponent('grid');
    const getForm = () => pageApi.getComponent('form');
    
    return {
        form: {
            ...getFormDefaultParams(),
            // title: 'Filtros de busqueda',
            fields:[
                {type: 'divider', label: 'Filtros de busqueda'},
                {type: 'select', name:'customerId', label:'Cliente', column: 4, load: { queryId: 361 }, search: true },
                {type: 'date', name: 'dateFrom',label:'Fecha Desde', format: 'DD/MM/YYYY', column: 4},
                {type: 'date', name: 'dateUntil',label:'Fecha Hasta', format: 'DD/MM/YYYY', column: 4},
                {type: 'switch', name:'active', label:'Estado', column: 6, value: true},
            ]
        },
        bar: { fields: [
            {...BUTTON_DEFAULT.SEARCH, onClick: ()=>getGrid().load({params:getForm().getData()})},
            {...BUTTON_DEFAULT.CLEAR, onClick: ()=>getForm().reset()}
        ]},
        grid: {
            ...getGridServerDefaultParams({queryLoad: 371, params: {docTypeId: 8}}),
            title: 'RESULTADOS',
            fields:[
                {name: 'noteNumber', label: 'N° Reg.',},
                {name: 'docType', label: 'T.D',},
                {name: 'docTypeNumber', label: 'N° Doc.',},
                {name: 'docDate', label: 'Fec. Doc',},
                {name: 'customer', label: 'Cliente',},
                {name: 'coin', label: 'Moneda',},
                {name: 'exchangeRate', label: 'T.C',},
                {name: 'grossAmount', label: 'Importe Bruto',},
                {name: 'unaffectedAmount', label: 'Imp. Inafecto',},
                {name: 'igv', label: 'IGV',},
                {name: 'total', label: 'Total',},
                {name: 'balance', label: 'Saldo',},
                {...COLUMN_DEFAULT.VIEW, link: (value, row, index) => `/sales/credit-note/credit-note/id-${row.noteId}` },
                {...COLUMN_DEFAULT.EDIT, link: (value, row, index) => `/sales/credit-note/credit-note/id-${row.noteId}/update` },
            ]
        }
    }
}
