import { MODE_SHORTCUT } from '../../../../constants/constants'

export const getModeCreditNoteDetail = () => {
    return {
        new: {
            form: {
                read: ['docTypeId']
            },
            bar: {
                show: ['save', 'return']
            }
        },
        update:{
            form: {
                read: ['docTypeId']
            },
            bar: {
                show: ['update', 'return']
            }
        },
        read:{
            form: {
                read: [MODE_SHORTCUT.ALL]
            },
            bar: {
                show: ['return']
            }
        }
    }
}
