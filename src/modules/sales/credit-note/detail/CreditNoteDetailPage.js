import React from 'react'
import BarButtonFw from '../../../../framework/bar-button/BarButtonFw';
import FormFw from '../../../../framework/form/FormFw';
import { BasePage } from '../../../../framework/pages/BasePage';
import { getConfigCreditNoteDetail } from './getConfigCreditNoteDetail';
import { getModeCreditNoteDetail } from './getModeCreditNoteDetail';
import { getValidationCreditNoteDetail } from './getValidationCreditNoteDetail';
import { createItemSection } from './sections/ItemSection';

export class CreditNoteDetailPage extends BasePage {
    
    createPageProperties = (pageApi) => {
        return {
            title: 'REGISTRO DE NOTA DE CRÉDITO',
            validation: getValidationCreditNoteDetail,
            mode: getModeCreditNoteDetail
        };
    }

    renderPage(pageApi) {

        const config = getConfigCreditNoteDetail(pageApi);

        return (
            <>
                <FormFw
                    name="form"
                    getParentApi={pageApi.getApi}
                    config={config.form}
                />
                
                {createItemSection(pageApi)}

                <BarButtonFw
                    name="bar"
                    getParentApi={pageApi.getApi}
                    config={config.bar}
                />
            </>
        )
    }
}