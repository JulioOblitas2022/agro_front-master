import { DATE_FORMAT, PAGE_MODE } from '../../../../constants/constants';
import { BUTTON_DEFAULT } from '../../../../framework/bar-button/BarButtonFw';
import { UserUtil } from '../../../../util/UserUtil';

export const getConfigCreditNoteDetail = (pageApi) => {
    const getForm = () => pageApi.getComponent('form');
    const getGridItemDetailGrid = () => pageApi.getComponent('itemGrid.grid');
    return {
        form: {
            load:{
                queryId: 370,
                params: {noteId: pageApi.getParamId()},

            },
            update:{
                queryId: 369,
                postLink: (resp, values) => '/sales/credit-note/credit-note-admin',
                params: {modifiedBy: UserUtil.getUserId()},
                fnOk: (resp) => {
                    getGridItemDetailGrid().save();
                }
            },
            save:{
                queryId: 368,
                postLink: (resp, values) => '/sales/credit-note/credit-note-admin',
                params: {createdBy: UserUtil.getUserId()},
                fnOk: (resp) => {
                    const noteId = resp.dataObjec?.noteId;
                    if (noteId){
                        getGridItemDetailGrid().save({params:{noteId}});
                    }
                }
            },
            // title: 'REGISTRO DE NOTA DE CRÉDITO',
            fields:[
                {type: 'divider', label: 'DATOS PRINCIPALES'},
                {type: 'date', name: 'entryDate',label:'Fecha Emisión', format: 'DD/MM/YYYY', inputFormat: DATE_FORMAT.YYYYMMDD, column: 4},
                {type: 'select', name:'coinId', label:'Moneda', column: 4, load: { queryId: 187 }, search: true },
                {type: 'select', name:'operationTypeId', label:'Tipo Operacion', column: 4, load: { queryId: 77 }, search: true },
                {type: 'select', name:'customerId', label:'Cliente', column: 4, load: { queryId: 361 }, search: true },
                {name:'exchangeRate', label:'Tipo de Cambio', column: 4},
                {name:'seatNumber', label:'Tipo de Cambio', column: 4},
                {type: 'select', name:'docTypeId', label:'Tipo Doc.', column: 4, load: { queryId: 195 }, search: true, value: 8 },
                {name:'docTypeNumber', label:'N° Doc.', column: 4},
                {type: 'select', name:'reasonId', label:'Motivo', column: 4, load: { queryId: 189 }, search: true },
                {type: 'select', name:'docTypeChangeId', label:'Tipo Doc. Modificar', column: 4, load: { queryId: 195 }, search: true },
                {name:'docTypeChangeNumber', label:'N° Doc. Modificar', column: 4},
                {type: 'textarea', name:'reason', label:'Motivo / Sustento', column: 6},
                {type: 'textarea', name:'glossary', label:'Glosa', column: 6},
                
            ],
            onComponentLoad : (formApi, pageApi) =>{
                if (pageApi.getPageMode() !== PAGE_MODE.NEW)
                    formApi.load();
            }
        },
        bar: {
            fields: [
                {...BUTTON_DEFAULT.SAVE, onClick: () => getForm().save()},
                {...BUTTON_DEFAULT.UPDATE, onClick: () => getForm().update()},
                {...BUTTON_DEFAULT.RETURN, link: '/sales/credit-note/credit-note-admin'}
            ],
        },
        grid: {}
    }
}
