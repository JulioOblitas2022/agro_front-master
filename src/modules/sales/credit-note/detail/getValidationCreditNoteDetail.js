export const getValidationCreditNoteDetail = () => {
    return {
        form: {
            broadcastDate: [ {required: true, message: 'Fecha Emisión es requerida' } ],
            coinId: [ {required: true, message: 'Moneda es requerido' } ],
            operationTypeId: [ {required: true, message: 'Tipo Operacion es requerida' } ],
            providerId: [ {required: true, message: 'Cliente es requerido' } ],
            changeType: [ {required: true, message: 'Tipo Cambio es requerido' } ],
            documentTypeId: [ {required: true, message: 'Tipo Doc. es requerido' } ],
            documentTypeReferencesId: [ {required: true, message: 'Tipo Doc. Ref es requerida' } ],
            paymentConditionId: [ {required: true, message: 'Cond. Pago es requerida' } ],
            expirationDate: [ {required: true, message: 'Fecha Venc. es requerida' } ],
        },
        grid: {

        },
        button: {

        },
        bar: {

        }
    }
}
