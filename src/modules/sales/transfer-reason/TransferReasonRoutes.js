import React from 'react';
import { Route, Switch, useRouteMatch } from 'react-router-dom';

import { TransferReasonAdminPage } from './admin/TransferReasonAdminPage';
import { TransferReasonDetailPage } from './detail/TransferReasonDetailPage';

const TransferReasonRoutes = () => {
    const { path } = useRouteMatch();
    return (
        <Switch>
            <Route exact path={`${path}/transfer-reason-admin`} component={TransferReasonAdminPage} />
            <Route path={`${path}/transfer-reason`} component={TransferReasonDetailPage} />
            <Route path={`${path}/transfer-reason/:id`} component={TransferReasonDetailPage} /> 
        </Switch>
    )
}

export default TransferReasonRoutes;
