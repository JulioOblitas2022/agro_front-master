
export const getValidationTransferReasonDetail = () => {
    return {
        form: {
            description: [{ required: true, message: 'Descipcion es requerida' }],
            sunatCode: [{ required: true, message: 'Codigo Sunat es requerido' }],
            companyId: [{ required: true, message: 'Empresa es requerido' }],
        },
        grid: {

        },
        button: {

        },
        bar: {

        }
    }
}
