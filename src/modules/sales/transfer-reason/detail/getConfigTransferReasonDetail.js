import { PAGE_MODE } from "../../../../constants/constants";
import { BUTTON_DEFAULT } from "../../../../framework/bar-button/BarButtonFw";
import { UserUtil } from "../../../../util/UserUtil";

export const getConfigTransferReasonDetail = (pageApi) => {

    const getForm = () => pageApi.getComponent('form');

    return {
        form: {
            load: {
                queryId: 103,
                params: { transferReasonId: pageApi.getParamId() },
            },
            update: {
                queryId: 104,
                params: { transferReasonId: pageApi.getParamId(), modifiedBy: UserUtil.getUserId()},
                postLink: (resp, values) => '/sales/transfer-reason/transfer-reason-admin'
            },
            save: {
                queryId: 102,
                params: {createdBy: UserUtil.getUserId()},
                postLink: (resp, values) => '/sales/transfer-reason/transfer-reason-admin'
            },
            fields: [
                { name: 'description', label: 'Descripcion', column: 6 },
                { name: 'sunatCode', label: 'Cod. Sunat', column: 6 },
                { type: 'select', name: 'companyId', label: 'Empresa', column: 6, load: { queryId: 554, } },

            ],
            onComponentLoad: (formApi, pageApi) => {
                if (pageApi.getPageMode() !== PAGE_MODE.NEW)
                    formApi.load();
            }
        },
        bar: {
            fields: [
                { ...BUTTON_DEFAULT.SAVE, onClick: () => getForm().save() },
                { ...BUTTON_DEFAULT.UPDATE, onClick: () => getForm().update() },
                { ...BUTTON_DEFAULT.RETURN, link: '/sales/transfer-reason/transfer-reason-admin' }
            ]
        },
        grid: {}
    }
}
