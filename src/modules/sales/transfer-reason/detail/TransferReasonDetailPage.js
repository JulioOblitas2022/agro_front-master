import React from 'react'
import BarButtonFw from '../../../../framework/bar-button/BarButtonFw';
import FormFw from '../../../../framework/form/FormFw';
import { BasePage } from '../../../../framework/pages/BasePage';
import { getConfigTransferReasonDetail } from './getConfigTransferReasonDetail';
import { getModeTransferReasonDetail } from './getModeTransferReasonDetail';
import { getValidationTransferReasonDetail } from './getValidationTransferReasonDetail';

export class TransferReasonDetailPage extends BasePage {
    
    createPageProperties = (pageApi) => {
        return {
            title: 'REGISTRO DE MOTIVO DE TRASLADO',
            validation: getValidationTransferReasonDetail,
            mode: getModeTransferReasonDetail
        };
    }

    renderPage(pageApi) {

        const config = getConfigTransferReasonDetail(pageApi);

        return (
            <>
                <FormFw
                    name="form"
                    getParentApi={pageApi.getApi}
                    config={config.form}
                />
                <BarButtonFw
                    name="bar"
                    getParentApi={pageApi.getApi}
                    config={config.bar}
                />
            </>
        )
    }
}