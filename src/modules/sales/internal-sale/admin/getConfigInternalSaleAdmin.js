import { DATE_FORMAT } from "../../../../constants/constants";
import { BUTTON_DEFAULT } from "../../../../framework/bar-button/BarButtonFw";
import { COLUMN_DEFAULT } from "../../../../framework/grid/GridFw";
import { getFormDefaultParams } from "../../../shared/util/getFormDefaultParams";
import { getGridServerDefaultParams } from "../../../shared/util/getGridServerDefaultParams";

export const getConfigInternalSaleAdmin = (pageApi) => {
    const getGrid = () => pageApi.getComponent('grid');
    const getForm = () => pageApi.getComponent('form');
    
    return {
        form: {
            ...getFormDefaultParams(),
            title: 'Filtros de busqueda',
            fields:[
                {name: 'ruc', label:'N° de registro', column: 4},
                {type: 'select', name:'customerId', label:'Cliente', column: 4, load: { queryId: 159 }, search: true },
                {type: 'date', name: 'dateFrom',label:'Fecha Desde', format: DATE_FORMAT.DDMMYYYY, column: 4},
                {type: 'date', name: 'dateUntil',label:'Fecha Hasta', format: DATE_FORMAT.DDMMYYYY, column: 4},
                {type: 'switch', name:'active', label:'Estado', column: 4, value: true},
            ]
        },
        bar: { fields: [
            {...BUTTON_DEFAULT.SEARCH, onClick: ()=>getGrid().load({params:getForm().getData()})},
            {...BUTTON_DEFAULT.CLEAR, onClick: ()=>getForm().reset()}
        ]},
        grid: {
            ...getGridServerDefaultParams({queryLoad: 362}),
            title: 'RESULTADOS',
            fields:[
                {name: 'internalSaleNumber', label: 'N° Reg', sorter: {multiple: 1}},
                {name: 'ruc', label: 'Ruc',},
                {name: 'provider', label: 'Cliente',},
                {name: 'perception', label: 'T.D',},
                {name: 'broadcastDate_2', label: 'Fec. Emisión',},
                {name: 'documentNumber', label: 'Nro. Documento',},
                {name: 'coin', label: 'M.',},
                {name: 'perceptionTax', label: 'Imp. Percepción',},
                {name: 'balance', label: 'Saldo',},
                {...COLUMN_DEFAULT.VIEW, link: (value, row, index) => `/sales/internal-sale/internal-sale/${row.internalSaleId}` },
                {...COLUMN_DEFAULT.EDIT, link: (value, row, index) => `/sales/internal-sale/internal-sale/${row.internalSaleId}/update` },
            ]
        }
    }
}
