import React from 'react'
import { Route, Switch, useRouteMatch } from 'react-router-dom';
import { InternalSaleAdminPage } from './admin/InternalSaleAdminPage';
import { InternalSaleDetailPage } from './detail/InternalSaleDetailPage';

const InternalSaleRoutes = () => {
    const { path } = useRouteMatch();
    return (
        <Switch>
            <Route exact path={`${path}/internal-sale-admin`} component={InternalSaleAdminPage} />
            <Route path={`${path}/internal-sale`} component={InternalSaleDetailPage} />
            <Route path={`${path}/internal-sale/:id`} component={InternalSaleDetailPage} /> 
        </Switch>
    )
}

export default InternalSaleRoutes;