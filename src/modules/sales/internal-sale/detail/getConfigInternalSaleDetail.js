import { DATE_FORMAT, PAGE_MODE } from "../../../../constants/constants";
import { BUTTON_DEFAULT } from "../../../../framework/bar-button/BarButtonFw";
import { UserUtil } from "../../../../util/UserUtil";

export const getConfigInternalSaleDetail = (pageApi) => {
    const getForm = () => pageApi.getComponent('form');
    const getGridItemDetailGrid = () => pageApi.getComponent('itemGrid.grid');
    return {
        form: {
            load:{
                queryId: 365,
                params: {internalSaleId: pageApi.getParamId()},
            },
            update:{
                queryId: 364,
                postLink: (resp, values) => '/sales/internal-sale/internal-sale-admin',
                params: {modifiedBy: UserUtil.getUserId()},
                fnOk: (resp) => {
                    getGridItemDetailGrid().save();
                }
            },
            save:{
                queryId: 363,
                postLink: (resp, values) => '/sales/internal-sale/internal-sale-admin',
                params: {createdBy: UserUtil.getUserId()},
                fnOk: (resp) => {
                    const internalSaleId = resp.dataObject?.internalSaleId;
                    if (internalSaleId){
                        getGridItemDetailGrid().save({params:{internalSaleId}});
                    }
                }
            },
            title: 'REGITRO DE VENTAS',
            fields:[
                {type: 'divider', label: 'DATOS PRINCIPALES'},
                {type: 'select', name:'customerId', label:'Cliente', column: 4, load: { queryId: 361 }, search: true },
                {type: 'date', name: 'entryDate',label:'Fecha Emisión', format: 'DD/MM/YYYY', inputFormat: DATE_FORMAT.YYYYMMDD, column: 4},
                {type: 'select', name:'coinId', label:'Moneda', column: 4, load: { queryId: 187 }, search: true },
                {type: 'number', name: 'exchangeRate', label: 'Tipo de Cambio', column: 4},
                {type: 'select', name:'perceptionId', label:'Percepción', column: 4, load: { queryId: 196 }, search: true },
                {type: 'number', name: 'rate', label: '% Tasa', column: 4},
                {type: 'number', name: 'seatNumber', label: 'N° Asiento', column: 4},
                {type: 'textarea', name:'glossary', label:'Glosa', column: 12, rows: 5},
                
            ],
            onComponentLoad : (formApi, pageApi) =>{
                if (pageApi.getPageMode() !== PAGE_MODE.NEW)
                    formApi.load();
            }
        },
        bar: {
            fields: [
                {...BUTTON_DEFAULT.SAVE, onClick: () => getForm().save()},
                {...BUTTON_DEFAULT.UPDATE, onClick: () => getForm().update()},
                {...BUTTON_DEFAULT.RETURN, link: '/sales/internal-sale/internal-sale-admin'}
            ],
        },
        grid: {}
    }
}