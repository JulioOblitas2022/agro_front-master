import React from 'react';
import BarButtonFw from '../../../../framework/bar-button/BarButtonFw';
import FormFw from '../../../../framework/form/FormFw';
import { BasePage } from '../../../../framework/pages/BasePage';
import { getConfigInternalSaleDetail } from './getConfigInternalSaleDetail';
import { getModeInternalSaleDetail } from './getModeInternalSaleDetail';
import { getValidationInternalSaleDetail } from './getValidationInternalSaleDetail';
import { createItemSection } from './sections/ItemSection';

export class InternalSaleDetailPage extends BasePage {
    
    createPageProperties = (pageApi) => {
        return {
            title: 'VENTAS',
            validation: getValidationInternalSaleDetail,
            mode: getModeInternalSaleDetail
        };
    }

    renderPage(pageApi) {

        const config = getConfigInternalSaleDetail(pageApi);

        return (
            <>
                <FormFw
                    name="form"
                    getParentApi={pageApi.getApi}
                    config={config.form}
                />
                
                {createItemSection(pageApi)}

                <BarButtonFw
                    name="bar"
                    getParentApi={pageApi.getApi}
                    config={config.bar}
                />
            </>
        )
    }
}