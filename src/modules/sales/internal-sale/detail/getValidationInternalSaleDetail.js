export const getValidationInternalSaleDetail = () => {
    return {
        form: {
            providerId: [ {required: true, message: 'Cliente es requerido' } ],
            broadcastDate: [ {required: true, message: 'Fecha Emisión es requerida' } ],
            coinId: [ {required: true, message: 'Moneda es requerida' } ],
            changeType: [ {required: true, message: 'Tipo de Cambio es requerido' } ],
            perceptionId: [ {required: true, message: 'Percepción es requerido' } ],
            rate: [ {required: true, message: '% Tasa es requerido' } ],
            seatNumber: [ {required: true, message: 'N° Asiento es requerido' } ],
        },
        grid: {

        },
        button: {

        },
        bar: {

        }
    }
}
