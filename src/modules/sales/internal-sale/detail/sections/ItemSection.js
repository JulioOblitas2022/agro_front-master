import React from 'react'
import FormGridFw, { FORM_GRID_MODE } from '../../../../../framework/form-grid/FormGridFw';

export const createItemSection = (pageApi) => {

    const gridConfig = {
        title: 'ITEMS',
        mode: FORM_GRID_MODE.FORM_ADD,
        collapsible: true,
        form: {
            dataOptions: {
                out: {
                    fnTransform: (values, api) => {
                        let result = [];
                        if (values['puchaseOrderId'].length > 0){
                            let items = api.getFieldApi('puchaseOrderId').getSelectedItem();
                            result = items.map(item => ({
                                purchaseOrderId: item.value, 
                                documentNumber: item.label, 
                                amountSoles: item.soles,
                                amountDollar: item.dolares,
                                coin: item.coin,
                                broadcastDate: item.broadcastDate
                            }));
                        }
                        return result;
                    }
                }
            },
            fields:[
                {type:'select', name:'puchaseOrderId', label:'Seleccionar Documento', column: 6, load: {queryId: 326}, multiple: true, search: true},
            ],
        },
        grid: {
            // title: '',
            rowId: 'internalSaleDetailId',
            showIndex: true,
            pagination: true,
            autoLoad: true,
            collapsible: true,
            load: {
                validation: () => pageApi.getParamId()!=null, //solo se dispara si el valor es distinto de nulo
                queryId: 367,
                orderBy: 'internalSaleDetailId desc',
                params: {internalSaleId: pageApi.getParamId()}
            },
            save: {
                queryId: 366,
                params: {internalSaleId: pageApi.getParamId()}
            },
            fields:[
                {name:'documentNumber', label:'Nro. Documento', width: 100},
                {name:'coin', label:'M', width: 100, editable: false},
                {name:'broadcastDate', label:'Fec. Emisión', width: 100},
                {type: 'number', name:'amountDollar', label:'Importe US$'},
                {type: 'number', name:'amountSoles', label:'Importe S/.'},
                {type: 'number', name:'perceptionRate', label:'% Tasa Percepcion', editable: true},
                {type: 'number', name:'perceptionAmount', label:'Importe Percepción', editable: true},
            ],
        }
    };

    return (
        <>
            <FormGridFw
                name="itemGrid"
                getParentApi={pageApi.getApi}
                config={gridConfig} 
            />
        </>
    )
}