import { DATE_FORMAT } from "../../../../constants/constants";
import { BUTTON_DEFAULT } from "../../../../framework/bar-button/BarButtonFw";
import { getFormDefaultParams } from "../../../shared/util/getFormDefaultParams";
import { getGridServerDefaultParams } from "../../../shared/util/getGridServerDefaultParams";

export const getConfigCurrentAccount = (pageApi) => {
    const getGrid = () => pageApi.getComponent('grid');
    const getForm = () => pageApi.getComponent('form');
    
    return {
        form: {
            ...getFormDefaultParams(),
            fields:[
                {type:'date', name:'dateFrom', label:'Fecha Desde', column: 4, format: DATE_FORMAT.DDMMYYYY, inputFormat: DATE_FORMAT.YYYYMMDD},
                {type:'date', name:'dateUntil', label:'Fecha Hasta', column: 4, format: DATE_FORMAT.DDMMYYYY, inputFormat: DATE_FORMAT.YYYYMMDD},
                {type: 'switch', name:'active', label:'Estado', column: 4, value: false},
            ]
        },
        bar: { fields: [
            {...BUTTON_DEFAULT.SEARCH, onClick: ()=>getGrid().load({params:getForm().getData()})},
            {...BUTTON_DEFAULT.CLEAR, onClick: ()=>getForm().reset()}
        ]},
        grid: {
            ...getGridServerDefaultParams({queryLoad: 230}),
            fields:[
                {name: 'docNumber', label: 'Tipo Doc'},
                {name: 'purchaseNumber', label: 'Nro Pedido'},
                {name: 'paymentConditionId', label: 'Condicion'},
                {name: 'docNumber', label: 'Producto'},
                {name: 'docNumber', label: 'Uni. Med'},
                {name: 'deliveryDate', label: 'Fch Entrega'},
                {name: 'customerId', label: 'Cantidad'},
                {name: 'paymentConditionId', label: 'Cantidad Entregada'},
                {name: 'docRefNumber', label: 'TD'},
                {name: 'customerId', label: 'Nro. Fac'},
                {name: 'customerId', label: 'Fecha Doc'},
                {name: 'customerId', label: 'Cliente'},
            ]
        }
    }
}
