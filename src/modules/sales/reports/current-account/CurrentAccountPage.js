import React from 'react'
import BarButtonFw, { BUTTON_DEFAULT } from '../../../../framework/bar-button/BarButtonFw';
import FormFw from '../../../../framework/form/FormFw';
import GridFw from '../../../../framework/grid/GridFw';
import { BasePage } from '../../../../framework/pages/BasePage';
import { getConfigCurrentAccount } from './getConfigCurrentAccount';

export class CurrentAccountPage extends BasePage {

    createPageProperties = (pageApi) => {
        return {
            title: 'BANDEJA DE CUENTA CORRIENTE PEDIDOS',
            barHeader: {
                fields:[
                    {...BUTTON_DEFAULT.REPORT, label:'Export', onClick: () => {}}
                ]
            }
        };
    }

    renderPage(pageApi) {

        const config = getConfigCurrentAccount(pageApi);

        return (
            <>
                <FormFw
                    name="form"
                    getParentApi={pageApi.getApi}
                    config={config.form}
                />
                <BarButtonFw
                    name="bar"
                    getParentApi={pageApi.getApi}
                    config={config.bar}
                />
                <GridFw
                    name="grid"
                    getParentApi={pageApi.getApi}
                    config={config.grid}
                />
            </>
        )
    }
}