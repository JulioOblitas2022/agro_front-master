import React from 'react';
import { Route, Switch, useRouteMatch } from 'react-router';

import { AnnualGuidesPage } from './annual-guides/AnnualGuidesPage';
import { AnnualSalesPage } from './annual-sales/AnnualSalesPage';
import { CurrentAccountPage } from './current-account/CurrentAccountPage';
import { DispatchGuidesPage } from './dispatch-guides/DispatchGuidesPage';
import { SalesPage } from './sales/SalesPage';

const ReportsRoutes = () => {
    const { path } = useRouteMatch();
    return (
        <Switch>
            <Route exact path={`${path}/sales`} component={SalesPage} />
            <Route exact path={`${path}/dispatch-guides`} component={DispatchGuidesPage} />
            <Route exact path={`${path}/annual-sales`} component={AnnualSalesPage} />
            <Route exact path={`${path}/annual-guides`} component={AnnualGuidesPage} />
            <Route exact path={`${path}/current-account-orders`} component={CurrentAccountPage} />
        </Switch>
    )
}

export default ReportsRoutes;