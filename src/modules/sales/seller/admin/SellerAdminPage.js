import React from 'react'
import BarButtonFw, { BUTTON_DEFAULT } from '../../../../framework/bar-button/BarButtonFw';
import FormFw from '../../../../framework/form/FormFw';
import GridFw from '../../../../framework/grid/GridFw';
import { BasePage } from '../../../../framework/pages/BasePage';
import { DATE_FORMAT } from "../../../../constants/constants";
import { COLUMN_DEFAULT } from "../../../../framework/grid/GridFw";
import { getFormDefaultParams } from "../../../shared/util/getFormDefaultParams";
import { getGridServerDefaultParams } from "../../../shared/util/getGridServerDefaultParams";


export class SellerAdminPage extends BasePage {

    createPagePropertiesBase(pageApi) {
        return { 
            title: 'VENDEDORES',
            barHeader:  {
                fields:[
                    {...BUTTON_DEFAULT.NEW, label:'Nuevo', link: '/sales/seller/seller/new'}
                ]
            }
        };
    }

    renderPage(pageApi) {
        const getGrid = () => pageApi.getComponent('grid');
        const getForm = () => pageApi.getComponent('form');

        const form = {
            ...getFormDefaultParams(),
            title: 'Filtros de búsqueda',
            fields: [
                { name: 'name', label: 'Nombres', column: 4, },
                { name: 'lastName', label: 'Apellidos', column: 4, },
                { type: 'date', name: 'dateFrom', label: 'Desde', column: 4, format: DATE_FORMAT.DDMMYYYY, inputFormat: DATE_FORMAT.YYYYMMDD },
                { type: 'date', name: 'dateUntil', label: 'Hasta', column: 4, format: DATE_FORMAT.DDMMYYYY, inputFormat: DATE_FORMAT.YYYYMMDD },
                { type: 'select', name: 'companyId', label: 'Empresa', column: 4, load: { queryId: 554, } },
            ]
        }

        const bar = {
            fields: [
                { ...BUTTON_DEFAULT.SEARCH, onClick: () => getGrid().load({ params: getForm().getData() }) },
                { ...BUTTON_DEFAULT.CLEAR, onClick: () => getForm().reset() }
            ]
        }

        const grid = {
            ...getGridServerDefaultParams({ queryLoad: 236 }),
            title: 'LISTA DE VENDEDORES',
            fields: [
                { name: 'employee', label: 'Vendedor', },
                { name: 'basicAmount', label: 'Sueldo Básico', },
                { name: 'percentage', label: '% Comisión', },
                { name: 'fec_reg', label: 'Fecha Registro', },
                { name: 'companyName', label: 'EMPRESA', },
                { ...COLUMN_DEFAULT.VIEW, link: (value, row, index) => `/sales/seller/seller/${row.sellerId}`, label: 'Opciones', colSpan: 3 },
                { ...COLUMN_DEFAULT.EDIT, link: (value, row, index) => `/sales/seller/seller/${row.sellerId}/update`, colSpan: 0 },
                { ...COLUMN_DEFAULT.DELETE, process: { fnOk: (result) => getGrid().load({ params: getForm().getData() }), filter: (value, row, index) => ({ queryId: 240, params: { sellerId: row.sellerId } }) }, colSpan: 0 }
            ]
        }
        return (
            <>
                <FormFw
                    name="form"
                    getParentApi={pageApi.getApi}
                    config={form}
                />
                <BarButtonFw
                    name="bar"
                    getParentApi={pageApi.getApi}
                    config={bar}
                />
                <GridFw
                    name="grid"
                    getParentApi={pageApi.getApi}
                    config={grid}
                />
            </>
        )
    }
}
