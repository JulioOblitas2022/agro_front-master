
export const getValidationSellerDetail = () => {
    return {
        form: {
            employeeId: [{ required: true, message: 'Empleado es requerido' }],
            basicAmount: [{ required: true, message: 'Imp. Basico es requerido' }],
            percentage: [{ required: true, message: 'Porcentaje es requerido' }],
            companyId: [{ required: true, message: 'Empresa es requerido' }],
        },
        grid: {

        },
        button: {

        },
        bar: {

        }
    }
}
