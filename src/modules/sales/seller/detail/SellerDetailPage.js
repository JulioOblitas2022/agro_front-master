import React from 'react'
import BarButtonFw from '../../../../framework/bar-button/BarButtonFw';
import FormFw from '../../../../framework/form/FormFw';
import { BasePage } from '../../../../framework/pages/BasePage';
import { getConfigSellerDetail } from './getConfigSellerDetail';
import { getModeSellerDetail } from './getModeSellerDetail';
import { getValidationSellerDetail } from './getValidationSellerDetail';

export class SellerDetailPage extends BasePage {
    
    createPageProperties = (pageApi) => {
        return {
            title: 'REGISTRO DE VENDEDOR',
            validation: getValidationSellerDetail,
            mode: getModeSellerDetail
        };
    }

    renderPage(pageApi) {

        const config = getConfigSellerDetail(pageApi);

        return (
            <>
                <FormFw
                    name="form"
                    getParentApi={pageApi.getApi}
                    config={config.form}
                />
                <BarButtonFw
                    name="bar"
                    getParentApi={pageApi.getApi}
                    config={config.bar}
                />
            </>
        )
    }
}