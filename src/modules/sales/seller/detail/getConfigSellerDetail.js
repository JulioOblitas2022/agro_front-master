import { PAGE_MODE } from "../../../../constants/constants";
import { BUTTON_DEFAULT } from "../../../../framework/bar-button/BarButtonFw";
import { UserUtil } from "../../../../util/UserUtil";

export const getConfigSellerDetail = (pageApi) => {

    const getForm = () => pageApi.getComponent('form');

    return {
        form: {
            load: {
                queryId: 238,
                params: { sellerId: pageApi.getParamId() },
            },
            update: {
                queryId: 239,
                postLink: (resp, values) => '/sales/seller/seller-admin',
                params: { modifiedBy: UserUtil.getUserId() },
            },
            save: {
                queryId: 237,
                postLink: (resp, values) => '/sales/seller/seller-admin',
                params: { createdBy: UserUtil.getUserId() },
            },
            fields: [
                { type: 'select', name: 'employeeId', label: 'Empleado', column: 4, load: { queryId: 309 }, search: true },
                { type: 'number', name: 'basicAmount', label: 'Imp. Basico', column: 4 },
                { type: 'number', name: 'percentage', label: 'Porcentaje', column: 4 },
                { type: 'select', name: 'companyId', label: 'Empresa', column: 4, load: { queryId: 554, } },
            ],
            onComponentLoad: (formApi, pageApi) => {
                if (pageApi.getPageMode() !== PAGE_MODE.NEW)
                    formApi.load();
            }
        },
        bar: {
            fields: [
                { ...BUTTON_DEFAULT.SAVE, onClick: () => getForm().save() },
                { ...BUTTON_DEFAULT.UPDATE, onClick: () => getForm().update() },
                { ...BUTTON_DEFAULT.RETURN, link: '/sales/seller/seller-admin' }
            ],
        },
        grid: {}
    }
}
