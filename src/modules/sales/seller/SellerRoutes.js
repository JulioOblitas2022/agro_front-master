import React from 'react'
import { Route, Switch, useRouteMatch } from 'react-router';

import { SellerAdminPage } from './admin/SellerAdminPage';
import { SellerDetailPage } from './detail/SellerDetailPage';

const SellerRoutes = () => {
    const { path } = useRouteMatch();
    return (
        <Switch>
            <Route exact path={`${path}/seller-admin`} component={SellerAdminPage} />
            <Route path={`${path}/seller`} component={SellerDetailPage} />
            <Route path={`${path}/seller/:id`} component={SellerDetailPage} />
        </Switch>
    )
}

export default SellerRoutes;