import React from 'react'
import BarButtonFw, { BUTTON_DEFAULT } from '../../../../framework/bar-button/BarButtonFw';
import FormFw from '../../../../framework/form/FormFw';
import GridFw from '../../../../framework/grid/GridFw';
import { BasePage } from '../../../../framework/pages/BasePage';
import { DATE_FORMAT } from '../../../../constants/constants';
import { COLUMN_DEFAULT } from '../../../../framework/grid/GridFw';
import { B_SELECT_CONSTANTS } from '../../../shared/constantes/BSelectConstants';
import { getFormDefaultParams } from '../../../shared/util/getFormDefaultParams';
import { getGridServerDefaultParams } from '../../../shared/util/getGridServerDefaultParams';



export class ProductAdminPage extends BasePage {

    createPagePropertiesBase(pageApi) {
        return { 
            title: 'BANDEJA DE ITEMS',
            barHeader:  {
                fields:[
                    {...BUTTON_DEFAULT.NEW, label:'Nuevo', link: '/sales/product/product/new'}
                ]
            }
        };
    }

    renderPage(pageApi) {
        const getGrid = () => pageApi.getComponent('grid');
        const getForm = () => pageApi.getComponent('form');

        const form = {
            ...getFormDefaultParams(),
            title: 'FILTROS DE BUSQUEDA',
            fields:[
                {name:'cenCode', label:'Codigo', column: 3},
                {name:'description', label:'Descripcion', column: 3},
                {type: 'date', name:'dateFrom', label:'Desde', column: 2, format: DATE_FORMAT.DDMMYYYY},
                {type: 'date', name:'dateUntil', label:'Hasta', column: 2,format: DATE_FORMAT.DDMMYYYY},
            ]
        };

        const bar = { fields: [
            {...BUTTON_DEFAULT.SEARCH, onClick: ()=>getGrid().load({params:getForm().getData()})},
            {...BUTTON_DEFAULT.CLEAR, onClick: ()=>getForm().reset()}
        ]};

        const grid = {
            ...getGridServerDefaultParams({queryLoad: 210}),
            title: 'RESULTADOS',
            fields:[
                {name: 'masterItem', label: 'Desc. Producto',},
                {name: 'cenCode', label: 'Cod. CEN',},
                {name: 'description', label: 'Desc. CEN',},
                {type: 'date', name: 'fec_reg', label: 'Fecha Registro', format: DATE_FORMAT.DDMMYYYY},
                {...COLUMN_DEFAULT.VIEW, link: (value, row, index) => `/sales/product/product/${row.productId}`, label: 'Opciones', colSpan: 3 },
                {...COLUMN_DEFAULT.EDIT, link: (value, row, index) => `/sales/product/product/${row.productId}/update`, colSpan: 0 },
                {...COLUMN_DEFAULT.DELETE, process: {fnOk: (result)=>getGrid().load({params:getForm().getData()}),filter: (value, row, index) => ({queryId: 214, params: {productId: row.productId}}) }, colSpan: 0 }
            ]
        };

        return (
            <>
                <FormFw
                    name="form"
                    getParentApi={pageApi.getApi}
                    config={form}
                />
                <BarButtonFw
                    name="bar"
                    getParentApi={pageApi.getApi}
                    config={bar}
                />
                <GridFw
                    name="grid"
                    getParentApi={pageApi.getApi}
                    config={grid}
                />
            </>
        )
    }
}