import React from 'react'
import BarButtonFw from '../../../../framework/bar-button/BarButtonFw';
import FormFw from '../../../../framework/form/FormFw';
import { BasePage } from '../../../../framework/pages/BasePage';
import { getConfigProductDetail } from './getConfigProductDetail';
import { getModeProductDetail } from './getModeProductDetail';
import { getValidationProductDetail } from './getValidationProductDetail';

export class ProductDetailPage extends BasePage {
    
    createPageProperties = (pageApi) => {
        return {
            title: 'REGISTRO DE ITEM CEN',
            validation: getValidationProductDetail,
            mode: getModeProductDetail
        };
    }

    renderPage(pageApi) {

        const config = getConfigProductDetail(pageApi);

        return (
            <>
                <FormFw
                    name="form"
                    getParentApi={pageApi.getApi}
                    config={config.form}
                />
                <BarButtonFw
                    name="bar"
                    getParentApi={pageApi.getApi}
                    config={config.bar}
                />
            </>
        )
    }
}