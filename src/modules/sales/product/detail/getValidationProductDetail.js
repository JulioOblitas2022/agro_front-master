
export const getValidationProductDetail = () => {
    return {
        form: {
            customerId: [ {required: true, message: 'Cliente es requerido' } ],
            masterItemId: [ {required: true, message: 'Item es requerido' } ],
            cenCode: [ {required: true, message: 'Codigo CEN es requerido' } ],
            description: [ {required: true, message: 'Descripcion CEN es requerido' } ],
        },
        grid: {

        },
        button: {

        },
        bar: {

        }
    }
}
