import { PAGE_MODE } from "../../../../constants/constants";
import { BUTTON_DEFAULT } from "../../../../framework/bar-button/BarButtonFw";

export const getConfigProductDetail = (pageApi) => {
    
    const getForm = () => pageApi.getComponent('form');

    return {
        form: {
            load:{
                queryId: 211,
                params: {productId: pageApi.getParamId()},
            },
            update:{
                queryId: 213,
                postLink: (resp, values) => '/sales/product/product-admin'
            },
            save:{
                queryId: 212,
                postLink: (resp, values) => '/sales/product/product-admin'
            },
            fields:[
                {type:'select', name:'customerId', label:'Cliente', column: 6, search: true, load: {queryId: 361}},
                {type:'select', name:'masterItemId', label:'Item', column: 6, search: true, load: {queryId: 158}},
                {name:'cenCode', label:'Código Producto', column: 6},
                {name:'description', label:'Descripcion CEN', column: 6},
            ],
            onComponentLoad : (formApi, pageApi) =>{
                if (pageApi.getPageMode() !== PAGE_MODE.NEW)
                    formApi.load();
            }
        },
        bar: {
            fields: [
                {...BUTTON_DEFAULT.SAVE, onClick: () => getForm().save()},
                {...BUTTON_DEFAULT.UPDATE, onClick: () => getForm().update()},
                {...BUTTON_DEFAULT.RETURN, link: '/sales/product/product-admin'}
            ],
        },
        grid: {}
    }
}
