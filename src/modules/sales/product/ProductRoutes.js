import React from 'react'
import { Route, Switch, useRouteMatch } from 'react-router';
import { ProductAdminPage } from './admin/ProductAdminPage';
import { ProductDetailPage } from './detail/ProductDetailPage';

const ProductRoutes = () => {
    const { path } = useRouteMatch();
    return (
        <Switch>
            <Route exact path={`${path}/product-admin`} component={ProductAdminPage} />
            <Route path={`${path}/product`} component={ProductDetailPage} />
            <Route path={`${path}/product/:id`} component={ProductDetailPage} />
        </Switch>
    )
}

export default ProductRoutes;
