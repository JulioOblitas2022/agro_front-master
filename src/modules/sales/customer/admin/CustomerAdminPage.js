import React from 'react'
import BarButtonFw, { BUTTON_DEFAULT } from '../../../../framework/bar-button/BarButtonFw';
import FormFw from '../../../../framework/form/FormFw';
import GridFw from '../../../../framework/grid/GridFw';
import { BasePage } from '../../../../framework/pages/BasePage';
import { DATE_FORMAT } from "../../../../constants/constants";
import { COLUMN_DEFAULT } from "../../../../framework/grid/GridFw";
import { getFormDefaultParams } from "../../../shared/util/getFormDefaultParams";
import { getGridServerDefaultParams } from "../../../shared/util/getGridServerDefaultParams";

export class CustomerAdminPage extends BasePage {
    createPagePropertiesBase(pageApi) {
        return {
            title: 'BANDEJA DE CLIENTES',
            barHeader: {
                fields:[
                    {...BUTTON_DEFAULT.NEW, label:'Nuevo', link: '/sales/customer/customer/new'}
                ]
            }
        };
    }

    renderPage(pageApi) {

        const getGrid = () => pageApi.getComponent('grid');
        const getForm = () => pageApi.getComponent('form');

        const form = {
            ...getFormDefaultParams(),
            title: 'Filtros de busqueda',
            fields: [
                //{type: 'divider', label: 'Filtros de busqueda'},
                { name: 'docNumber', label: 'N° Documento', column: 4 },
                { type: 'select', name: 'customerId', label: 'Cliente', column: 4, load: { queryId: 361 }, search: true },
                { type: 'date', name: 'dateFrom', label: 'Fecha Desde', format: DATE_FORMAT.DDMMYYYY, column: 4 },
                { type: 'date', name: 'dateUntil', label: 'Fecha Hasta', format: DATE_FORMAT.DDMMYYYY, column: 4 },
                { type: 'select', name: 'companyId', label: 'Empresa', column: 4, load: { queryId: 554, } },
                // {type: 'switch', name:'active', label:'Estado', column: 4, value: true},
            ]
        };

        const bar = {
            fields: [
                { ...BUTTON_DEFAULT.SEARCH, onClick: () => getGrid().load({ params: getForm().getData() }) },
                { ...BUTTON_DEFAULT.CLEAR, onClick: () => getForm().reset() }
            ]
        };

        const grid = {
            ...getGridServerDefaultParams({ queryLoad: 353 }),
            title: 'LISTA DE CLIENTES',
            fields: [
                { name: 'docType', label: 'Tipo Documento', sorter: { multiple: 1 } },
                { name: 'documentNumber', label: 'N° Documento', },
                { name: 'fullName', label: 'Nombres', },
                { name: 'address', label: 'Dirección', },
                { name: 'phone', label: 'Teléfono', },
                { type: 'date', name: 'registerDate', label: 'Fec. Registro', format: DATE_FORMAT.DDMMYYYY },
                { name: 'companyName', label: 'EMPRESA', },
                { ...COLUMN_DEFAULT.VIEW, link: (value, row, index) => `/sales/customer/customer/${row.id}`, label: 'Opciones', colSpan: 2 },
                { ...COLUMN_DEFAULT.EDIT, link: (value, row, index) => `/sales/customer/customer/${row.id}/update`, colSpan: 0 },
            ]
        }

        return (
            <>
                <FormFw
                    name="form"
                    getParentApi={pageApi.getApi}
                    config={form}
                />
                <BarButtonFw
                    name="bar"
                    getParentApi={pageApi.getApi}
                    config={bar}
                />
                <GridFw
                    name="grid"
                    getParentApi={pageApi.getApi}
                    config={grid}
                />
            </>
        )
    }
}