import { MODE_SHORTCUT } from '../../../../constants/constants'

export const getModeCustomerDetail = () => {
    return {
        new: {
            form: {
                read: ['registerTypeId']
            },
            bar: {
                show: ['save', 'return']
            }
        },
        update:{
            form: {
                read: ['registerTypeId']
            },
            bar: {
                show: ['update', 'return']
            }
        },
        read:{
            form: {
                read: [MODE_SHORTCUT.ALL]
            },
            bar: {
                show: ['return']
            }
        }
    }
}
