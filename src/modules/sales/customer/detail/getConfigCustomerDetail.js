import { PAGE_MODE } from "../../../../constants/constants";
import { BUTTON_DEFAULT } from "../../../../framework/bar-button/BarButtonFw";
import { UserUtil } from "../../../../util/UserUtil";
import { B_SELECT_CONSTANTS } from "../../../shared/constantes/BSelectConstants";

export const getConfigCustomerDetail = (pageApi) => {
    const getForm = () => pageApi.getComponent('form');
    return {
        form: {
            load: {
                queryId: 356,
                params: { id: pageApi.getParamId() },
            },
            update: {
                queryId: 355,
                postLink: (resp, values) => '/sales/customer/customer-admin',
                params: { modifiedBy: UserUtil.getUserId(), id: pageApi.getParamId()  },
            },
            save: {
                queryId: 354,
                postLink: (resp, values) => '/sales/customer/customer-admin',
                params: { createdBy: UserUtil.getUserId() },
            },
            title: 'REGISTRO DE CLIENTES',
            fields: [
                { type: 'divider', label: 'DATOS PRINCIPALES' },
                { type: 'select', name: 'categoryId', label: 'Categoría', column: 4, load: { queryId: 197 }, search: true },
                { type: 'select', name: 'typeTaxpayerId', label: 'Tipo Contribuyente', column: 4, load: { queryId: 183 }, search: true },
                { type: 'select', name: 'documentTypePersonId', label: 'Doc. Identidad', column: 4, load: { queryId: 220 } },
                { name: 'documentNumber', label: 'N° Doc.', column: 4 },
                { name: 'name', label: 'Nombres', column: 4 },
                { name: 'lastName', label: 'Apellidos', column: 4 },
                { name: 'tradeName', label: 'Nombre Comercial', column: 4 },
                { type: 'select', name: 'registerTypeId', label: 'Tipo Registro', column: 4, load: { queryId: 251 }, value: 1 },
                { type: 'select', name: 'conditionPaymentId', label: 'Cond. Pago', column: 4, load: { queryId: 188 }, search: true },
                { type: 'radio', name: 'retentionAgentId', label: 'Agente Retención', column: 4, load: {dataList: [{value: 1, label: 'Si'},{value: 2, label:'No'}]} },
                { type: 'select', name: 'companyId', label: 'Empresa', column: 4, load: { queryId: 554, } },

                { type: 'divider', label: 'DIRECCIÓN DE RESIDENCIA' },
                { name: 'address', label: 'Dirección', column: 8 },
                { ...B_SELECT_CONSTANTS.DEPARTAMENTO, column: 4, search: true },
                { ...B_SELECT_CONSTANTS.PROVINCIA, column: 4, search: true },
                { ...B_SELECT_CONSTANTS.DISTRITO, column: 4, search: true },
                { name: 'phone', label: 'Teléfono', column: 4 },
                { name: 'fax', label: 'Fax', column: 4 },
                { name: 'webPage', label: 'Página Web', column: 4 },
                { name: 'mail', label: 'Email', column: 4 },

                { type: 'divider', label: 'DATOS DEL GIRADOR DE LETRAS' },
                { name: 'spinnerName', label: 'Girador', column: 4 },
                { name: 'spinnerAddress', label: 'Dirección', column: 4 },
                { name: 'spinnerDocumentNumber', label: 'N° Doc. Girador', column: 4 },
                { name: 'spinnerPhone', label: 'Teléfono', column: 4 },
            ],
            onComponentLoad: (formApi, pageApi) => {
                if (pageApi.getPageMode() !== PAGE_MODE.NEW)
                    formApi.load();
            }
        },
        bar: {
            fields: [
                { ...BUTTON_DEFAULT.SAVE, onClick: () => getForm().save() },
                { ...BUTTON_DEFAULT.UPDATE, onClick: () => getForm().update() },
                { ...BUTTON_DEFAULT.RETURN, link: '/sales/customer/customer-admin' }
            ],
        },
        grid: {}
    }
}