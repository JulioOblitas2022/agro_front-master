import React from 'react'
import BarButtonFw from '../../../../framework/bar-button/BarButtonFw';
import FormFw from '../../../../framework/form/FormFw';
import { BasePage } from '../../../../framework/pages/BasePage';
import { getConfigCustomerDetail } from './getConfigCustomerDetail';
import { getModeCustomerDetail } from './getModeCustomerDetail';
import { getValidationCustomerDetail } from './getValidationCustomerDetail';

export class CustomerDetailPage extends BasePage {
    
    createPageProperties = (pageApi) => {
        return {
            title: 'CLIENTES',
            validation: getValidationCustomerDetail,
            mode: getModeCustomerDetail
        };
    }

    renderPage(pageApi) {

        const config = getConfigCustomerDetail(pageApi);

        return (
            <>
                <FormFw
                    name="form"
                    getParentApi={pageApi.getApi}
                    config={config.form}
                />
                
                <BarButtonFw
                    name="bar"
                    getParentApi={pageApi.getApi}
                    config={config.bar}
                />
            </>
        )
    }
}