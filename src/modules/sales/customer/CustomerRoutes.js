import React from 'react';
import { Route, Switch, useRouteMatch } from 'react-router-dom';

import { CustomerAdminPage } from './admin/CustomerAdminPage';
import { CustomerDetailPage } from './detail/CustomerDetailPage';

const CustomerRoutes = () => {
    const { path } = useRouteMatch();
    return (
        <Switch>
            <Route exact path={`${path}/customer-admin`} component={CustomerAdminPage} />
            <Route path={`${path}/customer`} component={CustomerDetailPage} />
            <Route path={`${path}/customer/:id`} component={CustomerDetailPage} />
        </Switch>
    )
}

export default CustomerRoutes;