import { DATE_FORMAT, PAGE_MODE } from "../../../../constants/constants";
import { BUTTON_DEFAULT } from "../../../../framework/bar-button/BarButtonFw";

export const getConfigItemPriceDetail = (pageApi) => {
    const getForm = () => pageApi.getComponent('form');
    const getGridItemDetailGrid = () => pageApi.getComponent('itemGrid.grid');
    return {
        form: {
            load:{
                queryId: 0,
                params: {itemPriceId: pageApi.getParamId()},
            },
            update:{
                queryId: 0,
                postLink: (resp, values) => '/sales/item-price/item-price-admin',
                fnOk: (resp) => {
                    getGridItemDetailGrid().save();
                }
            },
            save:{
                queryId: 0,
                postLink: (resp, values) => '/sales/item-price/item-price-admin',
                fnOk: (resp) => {
                    const itemPriceId = resp?.dataObject;
                    if (itemPriceId){
                        getGridItemDetailGrid().save({params:{itemPriceId}});
                    }
                }
            },
            title: 'REGITRO DE ITEMS Y PRECIO',
            fields:[
                {type: 'divider', label: 'DATOS PRINCIPALES'},
                {type: 'select', name:'documentoIdentityId', label:'Doc. Identidad', column: 4, load: { queryId: 159 }, search: true },
                {name: 'documentNumber',label:'Nro. Documento', column: 4},
                {name:'name', label:'Nombre', column: 4,},
            ],
            onComponentLoad : (formApi, pageApi) =>{
                if (pageApi.getPageMode() !== PAGE_MODE.NEW)
                    formApi.load();
            }
        },
        bar: {
            fields: [
                {...BUTTON_DEFAULT.SAVE, onClick: () => getForm().save()},
                {...BUTTON_DEFAULT.UPDATE, onClick: () => getForm().update()},
                {...BUTTON_DEFAULT.RETURN, link: '/sales/item-price/item-price-admin'}
            ],
        },
        grid: {}
    }
}