import React from 'react'
import BarButtonFw from '../../../../framework/bar-button/BarButtonFw';
import FormFw from '../../../../framework/form/FormFw';
import { BasePage } from '../../../../framework/pages/BasePage';
import { getConfigItemPriceDetail } from './getConfigItemPriceDetail';
import { getModeItemPriceDetail } from './getModeItemPriceDetail';
import { getValidationItemPriceDetail } from './getValidationItemPriceDetail';
import { createItemSection } from './sections/ItemSection';

export class ItemPriceDetailPage extends BasePage {
    
    createPageProperties = (pageApi) => {
        return {
            title: 'ITEMS PRECIO',
            validation: getValidationItemPriceDetail,
            mode: getModeItemPriceDetail
        };
    }

    renderPage(pageApi) {

        const config = getConfigItemPriceDetail(pageApi);

        return (
            <>
                <FormFw
                    name="form"
                    getParentApi={pageApi.getApi}
                    config={config.form}
                />
                
                {createItemSection(pageApi)}

                <BarButtonFw
                    name="bar"
                    getParentApi={pageApi.getApi}
                    config={config.bar}
                />
            </>
        )
    }
}