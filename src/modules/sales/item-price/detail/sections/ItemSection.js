import React from 'react'
import FormGridFw, { FORM_GRID_MODE } from '../../../../../framework/form-grid/FormGridFw';
import FormFw from '../../../../../framework/form/FormFw';

export const createItemSection = (pageApi) => {

    const gridConfig = {
        title: 'PRODUCTOS A VENDER',
        mode: FORM_GRID_MODE.FORM_ADD,
        collapsible: true,
        form: {
            dataOptions: {
                out: {
                    fnTransform: (values, api) => {
                        let result = [];
                        if (values['masterItemId'].length > 0){
                            let items = api.getFieldApi('masterItemId').getSelectedItem();
                            result = items.map(item => ({masterItemId: item.value, description: item.label, unitMeasuse: '', itemTypeText: item.itemTypeText}));
                        }
                        return result;
                    }
                }
            },
            fields:[
                {type:'select', name:'familyId', label:'Seleccionar Producto', column: 6, load: {queryId: 50}},
                {type:'select', name:'masterItemId', multiple:true, column: 6, load: {queryId: 191},
                    parents: [
                        {name:'familyId', paramName: 'familyId', required: true}
                    ]
                }
            ],
        },
        grid: {
            // title: '',
            rowId: 'documentId',
            showIndex: true,
            pagination: true,
            autoLoad: true,
            collapsible: true,
            load: {
                validation: () => pageApi.getParamId()!=null, //solo se dispara si el valor es distinto de nulo
                queryId: 0,
                orderBy: 'code desc',
                params: {purchaseOrderId: pageApi.getParamId()}
            },
            save: {
                queryId: 0,
                params: {purchaseOrderId: pageApi.getParamId()}
            },
            fields:[
                {name:'code', label:'Codigo', width: 100},
                {name:'commodity', label:'Mercadería', width: 100, editable: false},
                {name:'unitMeasure', label:'Uni. Med. ', width: 100, editable: false},
                {name:'igvRate', label:'Tasa IGV', width: 100},
                {type: 'number', name:'netWeight', label:'Peso Neto', editable: true},
               
            ],
        }
    };
    return (
        <>
            <FormGridFw
                name="itemGrid"
                getParentApi={pageApi.getApi}
                config={gridConfig} 
            />
        </>
    )
}