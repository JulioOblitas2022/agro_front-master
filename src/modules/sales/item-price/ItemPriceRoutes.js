import React from 'react'
import { Route, Switch } from 'react-router-dom';
import { ItemPriceAdminPage } from './admin/ItemPriceAdminPage';
import { ItemPriceDetailPage } from './detail/ItemPriceDetailPage';

const ItemPriceRoutes = () => {
    return (
        <Switch>
            <Route exact path="/sales/item-price/item-price-admin" component={ItemPriceAdminPage} />
            <Route path="/sales/item-price/item-price" component={ItemPriceDetailPage} />
            <Route path="/sales/item-price/item-price/:id" component={ItemPriceDetailPage} /> 
        </Switch>
    )
}

export default ItemPriceRoutes;