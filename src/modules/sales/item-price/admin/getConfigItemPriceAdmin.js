import { DATE_FORMAT } from "../../../../constants/constants";
import { BUTTON_DEFAULT } from "../../../../framework/bar-button/BarButtonFw";
import { COLUMN_DEFAULT } from "../../../../framework/grid/GridFw";
import { getFormDefaultParams } from "../../../shared/util/getFormDefaultParams";
import { getGridServerDefaultParams } from "../../../shared/util/getGridServerDefaultParams";

export const getConfigInternalSaleAdmin = (pageApi) => {
    const getGrid = () => pageApi.getComponent('grid');
    const getForm = () => pageApi.getComponent('form');
    
    return {
        form: {
            ...getFormDefaultParams(),
            title: 'Filtros de busqueda',
            fields:[
                //{type: 'divider', label: 'Filtros de busqueda'},
                {name: 'ruc', label:'Ruc', column: 4},
                {type: 'select', name:'customerId', label:'Cliente', column: 4, load: { queryId: 159 }, search: true },
                {type: 'date', name: 'inputDate',label:'Fecha Desde', format: DATE_FORMAT.DDMMYYYY, column: 4},
                {type: 'date', name: 'outputDate',label:'Fecha Hasta', format: DATE_FORMAT.DDMMYYYY, column: 4},
                {type: 'select', name:'state', label:'Estado', column: 4,
                    load: {
                        dataList: [
                            {label: 'Activo', value: '1'},
                            {label: 'Inactivo', value: '2'},
                        ]
                    }
                },
            ]
        },
        bar: { fields: [
            {...BUTTON_DEFAULT.SEARCH, onClick: ()=>getGrid().load({params:getForm().getData()})},
            {...BUTTON_DEFAULT.CLEAR, onClick: ()=>getForm().reset()}
        ]},
        grid: {
            ...getGridServerDefaultParams({queryLoad: 94}),
            title: 'RESULTADOS',
            fields:[
                {name: 'internalSaleId', label: 'N° Reg', sorter: {multiple: 1}},
                {name: 'ruc', label: 'Ruc',},
                {name: 'customer', label: 'Cliente',},
                {name: 'td', label: 'T.D',},
                {name: 'issueDate', label: 'Fec. Emisión',},
                {name: 'documentNumber', label: 'Nro. Documento',},
                {name: 'm', label: 'M.',},
                {name: 'perceptionTax', label: 'Imp. Percepción',},
                {name: 'balance', label: 'Saldo',},
                {...COLUMN_DEFAULT.VIEW, link: (value, row, index) => `/logistics/internal-sale/internal-sale/${row.internalSaleId}` },
                {...COLUMN_DEFAULT.EDIT, link: (value, row, index) => `/logistics/internal-sale/internal-sale/${row.internalSaleId}/update` },
            ]
        }
    }
}
