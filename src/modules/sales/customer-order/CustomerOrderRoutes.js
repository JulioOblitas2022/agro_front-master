import React from 'react'
import { Route, Switch, useRouteMatch } from 'react-router';
import { CustomerOrderAdminPage } from './admin/CustomerOrderAdminPage';
import { CustomerOrderDetailPage } from './detail/CustomerOrderDetailPage';

const CustomerOrderRoutes = () => {
    const { path } = useRouteMatch();
    return (
        <Switch>
            <Route exact path={`${path}/customer-order-admin`} component={CustomerOrderAdminPage} />
            <Route path={`${path}/customer-order`} component={CustomerOrderDetailPage} />
            <Route path={`${path}/customer-order/:id`} component={CustomerOrderDetailPage} />
        </Switch>
    )
}

export default CustomerOrderRoutes;