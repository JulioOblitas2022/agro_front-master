import BarButtonFw from "../../../../framework/bar-button/BarButtonFw";
import FormFw from "../../../../framework/form/FormFw";
import { BasePage } from "../../../../framework/pages/BasePage";
import { getConfigCustomerOrderDetail } from "./getConfigCustomerOrderDetail";
import { getModeCustomerOrderDetail } from "./getModeCustomerOrderDetail";
import { getValidationCustomerOrderDetail } from "./getValidationCustomerOrderDetail";
import { createCustomerOrderSection } from "./sections/CustomerOrderSection";

export class CustomerOrderDetailPage extends BasePage {

    createPageProperties = (pageApi) => {
        return {
            title: 'REGISTRO DE PEDIDO CLIENTE',
            validation: getValidationCustomerOrderDetail,
            mode: getModeCustomerOrderDetail
        };
    }

    renderPage(pageApi) {

        const config = getConfigCustomerOrderDetail(pageApi);

        return (
            <>
                <FormFw
                    name="form"
                    getParentApi={pageApi.getApi}
                    config={config.form}
                />
                {createCustomerOrderSection(pageApi)}
                <BarButtonFw
                    name="bar"
                    getParentApi={pageApi.getApi}
                    config={config.bar}
                />
            </>
        )
    }
}
