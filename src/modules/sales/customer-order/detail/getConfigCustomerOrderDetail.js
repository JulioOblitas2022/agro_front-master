import { DATE_FORMAT, PAGE_MODE } from "../../../../constants/constants";
import { BUTTON_DEFAULT } from "../../../../framework/bar-button/BarButtonFw";
import { DateUtil } from "../../../../util/DateUtil";
import { UserUtil } from "../../../../util/UserUtil";
import { B_SELECT_CONSTANTS } from "../../../shared/constantes/BSelectConstants";

export const getConfigCustomerOrderDetail = (pageApi) => {
    const getForm = () => pageApi.getComponent('form');
    const getCustomerOrderGrid = () => pageApi.getComponent('customerOrderGrid');
    const getFormSumas = () => pageApi.getComponent('formSumas');

    return {
        form: {
            autoLoad: true,
            load: {
                queryId: 231,
                params: { customerOrderId: pageApi.getParamId() || 0 },
                fnOk: (resp) => {
                    getCustomerOrderGrid().load({ params: pageApi.getParamId() });
                }
            },
            update: {
                queryId: 233,
                postLink: (resp, values) => '/sales/customer-order/customer-order-admin',
                params: { modifiedBy: UserUtil.getUserId()},
                fnOk: (resp) => {
                    getCustomerOrderGrid().save();
                }
            },
            save: {
                queryId: 232,
                postLink: (resp, values) => '/sales/customer-order/customer-order-admin',
                params: { createdBy: UserUtil.getUserId()},
                fnOk: (resp) => {
                    const customerOrderId = resp.dataObject?.customerOrderId;
                    if (customerOrderId) {
                        getCustomerOrderGrid().save({ params: { customerOrderId } });
                    }

                }
            },
            title: 'Datos Principales',
            fields: [
                { name: 'documentNumber', label: 'Nro. Documento', column: 4, readOnly: true },
                { type: 'select', name: 'companyId', label: 'Empresa', column: 4, load: { queryId: 554, }, search: true },
                { type: 'date', name: 'registerDate', label: 'Fecha Registro', format: 'DD/MM/YYYY', inputFormat: DATE_FORMAT.YYYYMMDD, column: 4 },
                { type: 'select', name: 'customerId', label: 'Cliente', column: 4, load: { queryId: 361 }, search: true },
                { type: 'select', name: 'pointSaleId', label: 'Punto Venta', column: 4, search: true, load: { queryId: 229 },
                    parents: [
                        { name: 'customerId', paramName: 'customerId', required: true }
                    ]
                },
                { type: 'date', name: 'orderDate', label: 'Fecha Pedido', format: 'DD/MM/YYYY', inputFormat: DATE_FORMAT.YYYYMMDD, column: 4 }, // inputFormat: DATE_FORMAT.YYYYMMDD
                { type: 'select', label: 'Tipo Doc. Ref', name: 'docRefId', load: { queryId: 195 }, search: true, column: 4 },
                { name: 'docRefNumber', label: 'Nro. Doc. Ref', column: 4 },
                { type: 'select', name: 'coinId', label: 'Moneda', column: 4, load: { queryId: 187 }, search: true },
                { type: 'select', name: 'paymentConditionId', label: 'Cond. Pago', column: 4, load: { queryId: 188 }, search: true },
                { type: 'select', name: 'sellerId', label: 'Vendedor', column: 4, load: { queryId: 335 }, search: true },
                { type: 'textarea', name: 'observations', label: 'Observaciones', column: 8, rows: 5 },
            ],
            onComponentLoad: (formApi, pageApi) => {
                if (pageApi.getPageMode() !== PAGE_MODE.NEW)
                    formApi.load();
            }
        },
        bar: {
            fields: [
                { ...BUTTON_DEFAULT.SAVE, onClick: () => getForm().save() },
                { ...BUTTON_DEFAULT.UPDATE, onClick: () => getForm().update() },
                { ...BUTTON_DEFAULT.RETURN, link: '/sales/customer-order/customer-order-admin' }
            ],
        },
        grid: {}
    }
}
