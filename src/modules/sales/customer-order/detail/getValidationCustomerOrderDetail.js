
export const getValidationCustomerOrderDetail = () => {
    return {
        form: {
            docNumber: [ {required: true, message: 'Nro Documento es requerido' } ],
            registerDate: [ {required: true, message: 'Fecha Registro es requerido' } ],
            customerId: [ {required: true, message: 'Cliente es requerido' } ],
            pointSaleId: [ {required: true, message: 'Punto Venta es requerido' } ],
            docRefId: [ {required: true, message: 'Tipo Doc. Ref es requerido' } ],
            docRefNumber: [ {required: true, message: 'Nro. Doc. Ref es requerido' } ],
            orderDate: [ {required: true, message: 'Fecha Pedido es requerido' } ],
            coinId: [ {required: true, message: 'Moneda es requerida' } ],
            paymentConditionId: [ {required: true, message: 'Cond. Pago es requerido' } ],
            sellerId: [ {required: true, message: 'Vendedor es requerido' } ],
            observations: [ {required: true, message: 'Observaciones son requeridas' } ],
            companyId: [ {required: true, message: 'Empresa es requerido' } ],
        },
        grid: {
            deliveryDate: [ {required: true, message: 'Ingresar Fecha de Entrega' } ],
        },
        button: {

        },
        bar: {

        }
    }
}
