import React from 'react'
import { DATE_FORMAT } from '../../../../../constants/constants';
import { BUTTON_DEFAULT } from '../../../../../framework/bar-button/BarButtonFw';
import FormGridFw, { FORM_GRID_MODE } from '../../../../../framework/form-grid/FormGridFw';
import FormFw from '../../../../../framework/form/FormFw';
import GridFw, { COLUMN_DEFAULT } from '../../../../../framework/grid/GridFw';
import { ProductModal } from '../modals/ProductModal';

export const createCustomerOrderSection = (pageApi) => {

    const getFormRate = () => pageApi.getComponent('formSumas');
    const getGrid = () => pageApi.getComponent('customerOrderGrid');

    const fnDelete = (value, row, index) => {
        getGrid().deleteItem(row.key);      
    }

    const itemConfig = {
        events: {
            afterAddEvent : (data) => {
                getGrid().addItem(data);
            }
        }
    }

    const ItemModalConfig = (data) =>  {
        return {
            width: 1600,
            maskClosable: false,
            component: <ProductModal name={data}  getParentApi={pageApi.getApi} config={itemConfig}  />
        }
    };


    const barHeaderConfig = {
        fields:[
            {...BUTTON_DEFAULT.ADD, label:'Agregar Item', onClick: ()=>{  pageApi.showModal(ItemModalConfig('')); }}
        ]
    }


    const calculateTotal = () => {
        let items = getGrid().getData();
        if (items) {
            let grossPrice = 0;
            items.forEach(item => {
                grossPrice = grossPrice + parseFloat(item.grossPrice);
            });
            let total = parseFloat(grossPrice).toFixed(2);

                getFormRate().setFieldData('grossTax', total);
                getFormRate().setFieldData('igv', parseFloat(total * 0.18).toFixed(2));
                getFormRate().setFieldData('total', parseFloat(total * 1.18).toFixed(2));
        }
    }
    

    const gridConfig = {
        title: 'Items',
        rowId: 'customerOrderDetailId',
        showIndex: true,
        pagination: true,
        autoLoad: true,
        load: {
            validation: () => pageApi.getParamId() != null, //solo se dispara si el valor es distinto de nulo
            queryId: 234,
            orderBy: 'customerOrderDetailId desc',
            params: { customerOrderId: pageApi.getParamId() },
            fnOk: () => {
                calculateTotal();
            },
            hideMessage: true, 
            hideMessageError: true
        },
        save: {
            queryId: 235,
            params: { customerOrderId: pageApi.getParamId() },
            hideMessage: true, 
            hideMessageError: true
        },
        addItem: {
            fnAfter: (row) => {
                calculateTotal();
            }
        },
        updateItem: {
            fnAfter: (row) => {
                calculateTotal();
            }
        },
        fields: [
            { name: 'itemTypeText', label: 'Tipo Item' },
            { name: 'description', label: 'Item' },
            { type: 'select', name: 'unitMeasureId', label: 'Uni. Med.', labelName: 'unitMeasureDes', load: {
                    queryId: 568,
                    params: () => {
                        return { masterItemId: getGrid()?.itemEditing?.masterItemId }
                    }
                }, search: true, editable: true,
            },
            { type: 'number', name: 'quantity', label: 'Cantidad', editable: true,
                onChange: (value, values, formApi) => {
                    let quantity = parseFloat(values['quantity']);
                    let unitPrice = parseFloat(values['unitPrice']);
                    let grossPrice = parseFloat(quantity * unitPrice).toFixed(2);

                    formApi?.setFieldData('grossPrice', grossPrice);
                }
            },
            { type: 'decimal', name: 'unitPrice', label: 'Precio Unitario', editable: true,
                onChange: (value, values, formApi) => {
                    let quantity = parseFloat(values['quantity']);
                    let unitPrice = parseFloat(values['unitPrice']);
                    let grossPrice = parseFloat(quantity * unitPrice).toFixed(2);

                    formApi?.setFieldData('grossPrice', grossPrice);
                }
            },
            { type: 'decimal', name: 'grossPrice', label: 'Precio Bruto', editable: true, readOnly: true },
            { type: 'date', name: 'deliveryDate', label: 'Fecha Entrega', format: 'DD/MM/YYYY', inputFormat: DATE_FORMAT.YYYYMMDD, editable: true },
            {...COLUMN_DEFAULT.DELETE, onClick: fnDelete}
        ],
        barHeader: barHeaderConfig
    };

    const form_sumas = {
        // load: {
        //     validation: () => pageApi.getParamId() != null, //solo se dispara si el valor es distinto de nulo
        //     queryId: 203,
        //     params: { purchaseOrderId: pageApi.getParamId() }
        // },
        // save: {
        //     queryId: 204,
        //     params: { purchaseOrderId: pageApi.getParamId() }
        // },
        fields: [
            { type: 'decimal', name: 'grossTax', label: 'Imp. Bruto', column: 4, readOnly: true },
            { type: 'decimal', name: 'igv', label: 'I.G.V ( 18% )', column: 4, readOnly: true },
            { type: 'decimal', name: 'total', label: 'Total', column: 4, readOnly: true }
        ],
    }

    return (
        <>
            <GridFw
                name="customerOrderGrid"
                getParentApi={pageApi.getApi}
                config={gridConfig}
            />

            <FormFw
                name="formSumas"
                getParentApi={pageApi.getApi}
                config={form_sumas}
            />
        </>
    )
}