import React from 'react'
import BarButtonFw, { BUTTON_DEFAULT } from '../../../../framework/bar-button/BarButtonFw';
import FormFw from '../../../../framework/form/FormFw';
import GridFw from '../../../../framework/grid/GridFw';
import { BasePage } from '../../../../framework/pages/BasePage';
import { DATE_FORMAT } from "../../../../constants/constants";
import { COLUMN_DEFAULT } from "../../../../framework/grid/GridFw";
import { B_SELECT_CONSTANTS } from "../../../shared/constantes/BSelectConstants";
import { getFormDefaultParams } from "../../../shared/util/getFormDefaultParams";
import { getGridServerDefaultParams } from "../../../shared/util/getGridServerDefaultParams";
import confirm from 'antd/lib/modal/confirm';
import { ExclamationCircleOutlined } from '@ant-design/icons';
import { LoadUtil } from '../../../../framework/util/LoadUtil';
import { EnvConstants } from '../../../../EnvConstants';

export class CustomerOrderAdminPage extends BasePage {

    createPageProperties = (pageApi) => {
        return {
            title: 'BANDEJA DE PEDIDOS DEL CLIENTE',
            barHeader: {
                fields:[
                    {...BUTTON_DEFAULT.NEW, label:'Nuevo', link: '/sales/customer-order/customer-order/new'}
                ]
            }
        };
    }

    renderPage(pageApi) {

        const getGrid = () => pageApi.getComponent('grid');
        const getForm = () => pageApi.getComponent('form');

        const form = {
            ...getFormDefaultParams(),
            fields:[
                {name:'docNumber', label:'Nro. Documento', column: 4},
                {type:'select', name:'customer', label:'Cliente', column: 4, load:{queryId: 361}},
                {type:'date', name:'dateFrom', label:'Fecha Desde', column: 4, format: DATE_FORMAT.DDMMYYYY, inputFormat: DATE_FORMAT.YYYYMMDD},
                {type:'date', name:'dateUntil', label:'Fecha Hasta', column: 4, format: DATE_FORMAT.DDMMYYYY, inputFormat: DATE_FORMAT.YYYYMMDD},
            ]
        };

        const bar = { 
            fields: [
                {...BUTTON_DEFAULT.SEARCH, onClick: ()=>getGrid().load({params:getForm().getData()})},
                {...BUTTON_DEFAULT.CLEAR, onClick: ()=>getForm().reset()}
            ]
        };

        const grid = {
            ...getGridServerDefaultParams({queryLoad: 230}),
            fields:[
                {name: 'documentNumber', label: 'Nro Documento', sorter: {multiple: 1}},
                {type: 'date', name: 'orderDate', label: 'Fecha Pedido', format: DATE_FORMAT.DDMMYYYY},
                {name: 'customer', label: 'Cliente'},
                // {name: 'purchaseNumber', label: 'Nro Compra'},
                {type: 'date', name: 'registerDate', label: 'Fecha Entrega', format: DATE_FORMAT.DDMMYYYY},
                {name: 'paymentCondition', label: 'Condicion Venta'},
                {name: 'docRef', label: 'Tipo Doc.'},
                {name: 'docRefNumber', label: 'Nro. Doc. Entrega'},
                {...COLUMN_DEFAULT.CHECK, label:'Opciones', title:'Enviar a Prod.', colSpan: 3,
                    fnVisible: (value, row, index) => {
                        return row?.active == 1;
                    },
                    onClick:(value, row) => {
                        confirm({
                            title: 'Desea Enviar a Producción ?',
                            icon: <ExclamationCircleOutlined />,
                            content: '' ,
                            onOk() {
                              LoadUtil.loadByQueryId({
                                  url: EnvConstants.GET_FW_FORM_URL_UPDATE_BASE(),
                                  queryId: 569,
                                  params: {customerOrderId: row.customerOrderId},
                                  fnOk: () => {
                                    getGrid().load();
                                  }
                              })
                            },
                            onCancel() {
                            },
                          });
                    } 
                },
                {...COLUMN_DEFAULT.VIEW, colSpan: 0, link: (value, row, index) => `/sales/customer-order/customer-order/${row.customerOrderId}` },
                {...COLUMN_DEFAULT.EDIT, colSpan: 0, link: (value, row, index) => `/sales/customer-order/customer-order/${row.customerOrderId}/update` },
            ]
        }

        return (
            <>
                <FormFw
                    name="form"
                    getParentApi={pageApi.getApi}
                    config={form}
                />
                <BarButtonFw
                    name="bar"
                    getParentApi={pageApi.getApi}
                    config={bar}
                />
                <GridFw
                    name="grid"
                    getParentApi={pageApi.getApi}
                    config={grid}
                />
            </>
        )
    }
}