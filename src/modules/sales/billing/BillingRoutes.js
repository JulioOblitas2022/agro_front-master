import React from 'react'
import { Route, Switch, useRouteMatch } from 'react-router';
import { BillingAdminPage } from './admin/BillingAdminPage';
import { BillingDetailPage } from './detail/BillingDetailPage';

const BillingRoutes = () => {
    const { path } = useRouteMatch();
    return (
        <Switch>
            <Route exact path={`${path}/billing-admin`} component={BillingAdminPage} />
            <Route path={`${path}/billing`} component={BillingDetailPage} />
            <Route path={`${path}/billing/:id`} component={BillingDetailPage} />
        </Switch>
    )
}

export default BillingRoutes;