import React from 'react'
import BarButtonFw, { BUTTON_DEFAULT } from '../../../../framework/bar-button/BarButtonFw';
import FormFw from '../../../../framework/form/FormFw';
import GridFw from '../../../../framework/grid/GridFw';
import { BasePage } from '../../../../framework/pages/BasePage';
import { DATE_FORMAT } from '../../../../constants/constants';
import { COLUMN_DEFAULT } from '../../../../framework/grid/GridFw';
import { getFormDefaultParams } from '../../../shared/util/getFormDefaultParams';
import { getGridServerDefaultParams } from '../../../shared/util/getGridServerDefaultParams';

export class BillingAdminPage extends BasePage {
    createPagePropertiesBase(pageApi) {
        return {
            title: 'FACTURACIÓN',
            barHeader: {
                fields:[
                    {...BUTTON_DEFAULT.NEW, label:'Nuevo', link: '/sales/billing/billing/new'}
                ]
            }
        };
    }

    renderPage(pageApi) {

        const getGrid = () => pageApi.getComponent('grid');
        const getForm = () => pageApi.getComponent('form');

        const form = {
            ...getFormDefaultParams(),
            title: 'FACTURACIÓN',
            fields:[
                {name:'documentNumber', label:'N° Documento', column: 4},
                { type: 'select', name:'customerId', label:'Cliente', column: 4, load: { queryId: 361 }, search: true },
                { type: 'date', name: 'broadcastDate',label:'Fecha Desde', format: DATE_FORMAT.DDMMYYYY, column: 4},
                { type: 'date', name: 'deliverDate',label:'Fecha Hasta', format: DATE_FORMAT.DDMMYYYY, column: 4},
            ]
        }

        const bar = { fields: [
            {...BUTTON_DEFAULT.SEARCH, onClick: ()=>getGrid().load({params:getForm().getData()})},
            {...BUTTON_DEFAULT.CLEAR, onClick: ()=>getForm().reset()}
        ]}

        const grid = {
            ...getGridServerDefaultParams({queryLoad: 402}),
            title: 'RESULTADOS',
            fields:[
                { name: 'documentType', label: 'Tipo de Doc.',},
                { name: 'documentNumber', label: 'Nro. Doc.',},
                { type: 'date', name: 'registerDate', label: 'Fec. Doc', format: DATE_FORMAT.DDMMYYYY},
                { name: 'customer', label: 'Cliente',},
                { type: 'deciaml', name: 'changeType', label: 'Tipo Cambio',},
                { type: 'deciaml', name: 'subTotal', label: 'Importe Bruto',},
                { type: 'deciaml', name: 'igvAmount', label: 'I.G.V',},
                { type: 'deciaml', name: 'totalAmount', label: 'Importe Total',},
                { type: 'deciaml', name: 'balance', label: 'Saldo',},
                {name: 'sent', label: 'Enviado',},
                { name: 'aceepted', label: 'Aceptado',},
                { name: 'canceled', label: 'Anulado',},
                {...COLUMN_DEFAULT.VIEW, link: (value, row, index) => `/sales/billing/billing/${row.billingId}`, label: 'Opciones', colSpan: 2 },
                {...COLUMN_DEFAULT.EDIT, link: (value, row, index) => `/sales/billing/billing/${row.billingId}/update`, colSpan: 0 }
            ]
        }

        return (
            <>
                <FormFw
                    name="form"
                    getParentApi={pageApi.getApi}
                    config={form}
                />
                <BarButtonFw
                    name="bar"
                    getParentApi={pageApi.getApi}
                    config={bar}
                />
                <GridFw
                    name="grid"
                    getParentApi={pageApi.getApi}
                    config={grid}
                />
            </>
        )
    }
}
