
import { DATE_FORMAT } from '../../../../../constants/constants';
import { BUTTON_DEFAULT } from '../../../../../framework/bar-button/BarButtonFw';
import GridFw, { COLUMN_DEFAULT } from '../../../../../framework/grid/GridFw';
import { DocumentModal } from '../modals/DocumentModal';
export const createDocumentSection = (pageApi) => {
    
    const fnDelete = (value, row, index) => {
        getGrid().deleteItem(row.key);      
    }
    
    const getGrid = () => pageApi.getComponent('DocumentSection');
    const getGridItems = () => pageApi.getComponent('ItemsSection');

    const documentConfig = {
        events: {
            afterAddEvent : (data) => {
                console.log(data);
                getGrid().addItem(data);
                let items = JSON.parse(data[0].items);
                getGridItems().setData(items)

            }
        }
    }

    const DocumentModalConfig = {
        width: 1600,
        maskClosable: false,
        component: <DocumentModal name="documentModal"  getParentApi={pageApi.getApi} config={documentConfig} />
    };


    const barHeaderConfig = {
        fields:[
            {...BUTTON_DEFAULT.SEARCH, label:'Buscar Documentos', onClick: ()=>{  pageApi.showModal(DocumentModalConfig); }}
        ]
    }
    const gridConfig = {
        title: 'Documentos',
        rowId: 'billingDocumentDetail',
        showIndex: true,
        pagination: true,
        autoLoad: false,
        collapsible: true,
        load: {
            validation: () => pageApi.getParamId()!=null, //solo se dispara si el valor es distinto de nulo
            queryId: 405, 
            orderBy: 'billingDocumentDetail desc',
            params: {billingId: pageApi.getParamId()},
            hideMessage: true,
            hideMessageError: true
        },
        save: {
            queryId: 404,
            params: {billingId: pageApi.getParamId()},
            hideMessage: true,
            hideMessageError: true
        },
        fields:[
            { name: 'documentType', label: 'Tipo Documento',},
            { name: 'docRefNumber', label: 'Nro. Documento',},
            { type: 'date', name: 'orderDate', label:'Fecha', format: DATE_FORMAT.DDMMYYYY },
            {...COLUMN_DEFAULT.DELETE, onClick: fnDelete}
        ],
        barHeader: barHeaderConfig
    };

    return (
        
        <GridFw
            name="DocumentSection"
            getParentApi={pageApi.getApi}
            config={gridConfig} 
        />
    )
}
