
import { BUTTON_DEFAULT } from '../../../../../framework/bar-button/BarButtonFw';
import FormFw from '../../../../../framework/form/FormFw';
import GridFw, { COLUMN_DEFAULT } from '../../../../../framework/grid/GridFw';
import { ItemModal } from '../modals/ItemModal';
export const createItemSection = (pageApi) => {
    
    const getGrid = () => pageApi.getComponent('ItemsSection');
    const getForm = () => pageApi.getComponent('FormSumasSection');
    

    const fnDelete = (value, row, index) => {
        getGrid().deleteItem(row.key);      
    }
    
    const calculateTotal = () => {
        let items = getGrid().getData();
        if (items){
            let subTotal = 0;
            items.forEach(item => {
                subTotal = subTotal + parseFloat(item.total);
                
            });
            getForm().setFieldData('subTotal',parseFloat(subTotal).toFixed(2));
            getForm().setFieldData('igvAmount',parseFloat(subTotal * 0.18).toFixed(2));
            getForm().setFieldData('totalAmount',parseFloat(subTotal * 1.18).toFixed(2));
        }
    }


    const itemConfig = {
        events: {
            afterAddEvent : (data) => {
                getGrid().addItem(data);
            }
        }
    }

    const ItemModalConfig = {
        width: 1600,
        maskClosable: false,
        component: <ItemModal name="documentModal"  getParentApi={pageApi.getApi} config={itemConfig} />
    };


    const barHeaderConfig = {
        fields:[
            {...BUTTON_DEFAULT.SEARCH, label:'Buscar Items', onClick: ()=>{  pageApi.showModal(ItemModalConfig); }}
        ]
    }
    const gridConfig = {
        title: 'Items',
        rowId: 'billingItemsDetail',
        showIndex: true,
        pagination: true,
        autoLoad: false,
        collapsible: true,
        load: {
            validation: () => pageApi.getParamId()!=null, //solo se dispara si el valor es distinto de nulo
            queryId: 407,
            orderBy: 'billingItemsDetail desc',
            params: {billingId: pageApi.getParamId()},
            fnOk: () => {
                calculateTotal();
            },
            hideMessage: true,
            hideMessageError: true
        },
        save: {
            queryId: 406,
            params: {billingId: pageApi.getParamId()},
            hideMessage: true,
            hideMessageError: true
        },
        addItem: {
            fnAfter: (row) => {
                calculateTotal();
            }
        },
        updateItem: {
            fnAfter: (row) => {
                calculateTotal();
            }
        },
        fields:[
            { name:'itemTypeText', label:'Tipo Item', editable: false},
            { name:'description', label:'Item', editable: false},
            { type: 'select', name: 'unitMeasureId', label: 'Uni. Med.', labelName: 'unitMeasureDes', load: { queryId: 568, params: () => {return {masterItemId: getGrid()?.itemEditing?.masterItemId}}}, search: true, editable: true },
            { name: 'quantity', label:'Cantidad', editable:true,
                onChange: (value, values, formApi) => {
                    let quantity = parseFloat(values['quantity']);
                    let netPrice = parseFloat(values['netPrice']);
                    let discount = parseFloat(values['discount']);

                    if(discount > 0){
                        let total = parseFloat(quantity * netPrice).toFixed(2);

                        formApi?.setFieldData('total', parseFloat(total - (total * (discount /100))).toFixed(2));
                    }
                    else{
                        let total = parseFloat(quantity * netPrice).toFixed(2);

                        formApi?.setFieldData('total', total);
                    }
                    
                }
            },
            { type: 'decimal', name: 'unitPrice', label: 'Precio Unitario', editable:false},
            { type: 'decimal', name: 'discount', label: 'Descuento %', editable:true,
                onChange: (value, values, formApi) => {
                    let quantity = parseFloat(values['quantity']);
                    let netPrice = parseFloat(values['netPrice']);
                    let discount = parseFloat(value);
                    if(discount > 0){
                        let total = parseFloat(quantity * netPrice).toFixed(2);

                        formApi?.setFieldData('total', parseFloat(total - (total * (discount /100))).toFixed(2));
                    }
                    else{
                        let total = parseFloat(quantity * netPrice).toFixed(2);

                        formApi?.setFieldData('total', total);
                    }
                    
                }
            },
            { type: 'decimal', name: 'netPrice', label:'Precio Neto', editable:true,
                onChange: (value, values, formApi) => {
                    let quantity = parseFloat(values['quantity']);
                    let netPrice = parseFloat(values['netPrice']);
                    let discount = parseFloat(values['discount']);

                    if(discount > 0){
                        let total = parseFloat(quantity * netPrice).toFixed(2);

                        formApi?.setFieldData('total', parseFloat(total - (total * (discount /100))).toFixed(2));
                    }
                    else{
                        let total = parseFloat(quantity * netPrice).toFixed(2);

                        formApi?.setFieldData('total', total);
                    }
                }
            },
            { type: 'decimal', name: 'total', label: 'Imp. Total', editable: true, readOnly: true},
            { name: 'typeSale', label:'Tipo Venta', editable:true,},
            {...COLUMN_DEFAULT.DELETE, onClick: fnDelete}
        ],
        barHeader: barHeaderConfig
    };

    const form_sumas = {
        load: {
            //validation: () => pageApi.getParamId()!=null, //solo se dispara si el valor es distinto de nulo
            queryId: 577,
            params: {billingId: pageApi.getParamId() || 0},
            hideMessage: true,
            hideMessageError: true
        },
        save: {
            queryId: 576,
            params: {billingId: pageApi.getParamId()},
            hideMessage: true,
            hideMessageError: true
        },
        fields:[
            {type: 'decimal', name:'subTotal', label: 'Sub Total',column: 4, readOnly: true},
            {type: 'decimal', name:'percentage', label:'Descuento %', column: 4},
            {type: 'decimal', name:'grossAmount1', label: 'Importe Bruto',column: 4, readOnly: true},
            {type: 'decimal', name:'unaffectedAmount', label: 'Importe Inafecto',column: 4, readOnly: true},
            {type: 'decimal', name:'igvAmount', label:'I.G.V ', column: 4, readOnly: true},
            {type: 'decimal', name:'iscAmount', label:'I.S.C', column: 4, readOnly: true},
            {type: 'decimal', name:'totalAmount', label: 'Total',column: 4, readOnly: true}
        ],
    }

    return (
        <>
            <GridFw
                name="ItemsSection"
                getParentApi={pageApi.getApi}
                config={gridConfig} 
            />

            <FormFw
                name="FormSumasSection"
                getParentApi={pageApi.getApi}
                config={form_sumas} 
            />
        </>
    )
}

