export const getValidationBillingDetail = () => {
    return {
        form: {
            companyId: [ {required: true, message: 'Empresa es requerido' } ],
            customerId: [ {required: true, message: 'Cliente es requerido' } ],
            docNumber: [ {required: true, message: 'N° Documento es requerido' } ],
            applicantId: [ {required: true, message: 'Solicitante es requerido' } ],
            sellerId: [ {required: true, message: 'Vendedor es requerido' } ],
            coinId: [ {required: true, message: 'Moneda es requerido' } ],
            paymentConditionId: [ {required: true, message: 'Condición de Pago es requerido' } ],
            reasonId: [ {required: true, message: 'Motivo es requerido' } ],
        },
        grid: {

        },
        button: {

        },
        bar: {

        }
    }
}
