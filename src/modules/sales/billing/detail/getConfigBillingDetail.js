import { DATE_FORMAT, PAGE_MODE } from '../../../../constants/constants';
import { BUTTON_DEFAULT } from '../../../../framework/bar-button/BarButtonFw';
import { DateUtil } from '../../../../util/DateUtil';
import { MessageUtil } from '../../../../util/MessageUtil';
import { UserUtil } from '../../../../util/UserUtil';
import { DocReferencesModal } from './modals/DocReferencesModal';

export const getConfigBillingDetail = (pageApi) => {
    const getForm = () => pageApi.getComponent('form');
    const getGridItemDetailSection = () => pageApi.getComponent('ItemsSection');
    const getGridDocumentDetailSection = () => pageApi.getComponent('DocumentSection');
    const getFormSumas = () => pageApi.getComponent('FormSumasSection');

    const docRefConfig = {
        events: {
            afterAddEvent: (data) => {

                // let itemData = JSON.parse(data[0].items);
                // //console.log(itemData);
                // if (itemData !== null) {
                //     getGridItemDetailSection().setData(itemData);
                // }
                // else {
                //     getGridItemDetailSection().reset();
                // }
                getForm().setFieldData('documentRefId', data[0].customerOrderId);
                getForm().setFieldData('documentNumberReferences', data[0].documentNumber);

            }
        }
    }

    const DocReferencesConfig = (id) => {
        return {
            width: 1000,
            maskClosable: false,
            component: <DocReferencesModal name={id} getParentApi={pageApi.getApi} config={docRefConfig} />
        }
    };

    return {
        form: {
            autoLoad: true,
            load:{
                queryId: 403,
                params: {billingId: pageApi.getParamId() || 0},
                fnOk: (resp) => {
                    getGridDocumentDetailSection().load({params:{billingId: pageApi.getParamId()}});
                    getGridItemDetailSection().load({params:{billingId: pageApi.getParamId()}})
                    getFormSumas().load({params:{billingId: pageApi.getParamId() || 0}});
                }
            },
            update:{
                queryId: 401,
                postLink: (resp, values) => '/sales/billing/billing-admin',
                params: {billingId: pageApi.getParamId(), modifiedBy: UserUtil.getUserId() },
                fnOk: (resp) => {
                    getGridDocumentDetailSection().save({params:{billingId: pageApi.getParamId()}});
                    getGridItemDetailSection().save({params:{billingId: pageApi.getParamId()}});
                    getFormSumas().save({params:{billingId: pageApi.getParamId()}});
                }
            },
            save:{
                queryId: 400,
                postLink: (resp, values) => '/sales/billing/billing-admin',
                params: { cratedBy: UserUtil.getUserId()},
                fnOk: (resp) => {
                    const billingId = resp.dataObject?.billingId;
                    if (billingId){
                        getGridDocumentDetailSection().save({params:{billingId}});
                        getGridItemDetailSection().save({params:{billingId}});
                        getFormSumas().save({params:{billingId}});
                    }
                }
            },
            title: 'REGISTRO DE VENTA',
            fields:[
                // {type: 'divider', label: 'DATOS PRINCIPALES'},
                { type: 'select', name: 'companyId', label: 'Empresa', column: 4, load: { queryId: 554, }, search: true },
                { type: 'date', name: 'registerDate',label:'Fecha Emisión', format: DATE_FORMAT.DDMMYYYY, inputFormat: DATE_FORMAT.YYYYMMDD, column: 4, value: DateUtil.getDate},
                { type: 'select', name:'coinId', label:'Moneda', column: 4, load: { queryId: 187 }, search: true },
                { type: 'select', name:'operationTypeId', label:'Tipo Operacion', column: 4, load: { queryId: 77 }, search: true, value: 1},
                { type: 'select', name:'customerId', label:'Cliente', column: 4, load: { queryId: 361 }, search: true },
                { name:'seatNumber', label:'N° Asiento', column: 4, },
                { type: 'select', name:'documentTypeId', label:'Tipo Documento', column: 4, load: { queryId: 575 }, search: true,
                    onChange: (value, values, formApi) => {
                        let correlative = getForm()?.getFieldApi('documentTypeId')?.getSelectedItem()?.correlative;
                        getForm()?.setFieldData('documentNumber', correlative);
                    }
                },
                { name:'documentNumber', label:'Nro Documento', column: 4},
                { type: 'number', name:'changeType', label:'Tipo de Cambio', column: 4},
                { type: 'select', name:'documentTypeReferencesId', label:'Tipo Doc. Ref', column: 4, load: { queryId: 195 }, search: true, value: 77 },
                { type: 'hidden', name:'documentRefId'},
                { name:'documentNumberReferences', label:'Nro Documento', column: 4},
                { type: 'button', column: 4, name: 'modal',
                    onClick: (value) => {
                        let customerId = getForm()?.getData()?.customerId;
                        if (customerId !== undefined) {
                            pageApi.showModal(DocReferencesConfig(getForm()?.getData()?.customerId));
                        } 
                        else {
                            return MessageUtil('warning', 'Alerta!', 'Debe Seleccionar el Cliente');
                        }

                    },
                },
                { type: 'select', name:'existencesTypeId', label:'Tipo Existencia', column: 4, load: { queryId: 185 }, search: true, value: 3 },
                { type: 'select', name: 'paymentConditionId', label: 'Cond. Pago', column: 4, load: { queryId: 188 }, search: true, value: 1,
                    onChange: (value, values) => {
                        let fecha_emision = getForm()?.getData()?.registerDate;
                        if(value == 1){
                            getForm()?.setFieldData('expirationDate', fecha_emision);
                        }
                        if(value == 2){
                            getForm()?.setFieldData('expirationDate', DateUtil.stringAddDays(fecha_emision, DATE_FORMAT.YYYYMMDD, 7) );
                        }
                        if(value == 3){
                            getForm()?.setFieldData('expirationDate', DateUtil.stringAddDays(fecha_emision, DATE_FORMAT.YYYYMMDD, 15) );
                        }
                        if(value == 4){
                            getForm()?.setFieldData('expirationDate', DateUtil.stringAddDays(fecha_emision, DATE_FORMAT.YYYYMMDD, 30) );
                        }
                        if(value == 5){
                            getForm()?.setFieldData('expirationDate', DateUtil.stringAddDays(fecha_emision, DATE_FORMAT.YYYYMMDD, 45) );
                        }
                        if(value == 6){
                            getForm()?.setFieldData('expirationDate', DateUtil.stringAddDays(fecha_emision, DATE_FORMAT.YYYYMMDD, 60) );
                        }
                        if(value == 7){
                            getForm()?.setFieldData('expirationDate', DateUtil.stringAddDays(fecha_emision, DATE_FORMAT.YYYYMMDD, 90) );
                        }
                    } 
                },
                { type: 'date', name: 'expirationDate',label:'Fecha Vencimiento', format: DATE_FORMAT.DDMMYYYY, inputFormat: DATE_FORMAT.YYYYMMDD, column: 4},
                { type: 'select', name:'sellerId', label:'Vendedor', column: 4, load: { queryId: 335 } },
                { type: 'select', name:'billingTypeId', label:'Tipo Facturación', column: 4, load: { queryId: 336 } },
                { type: 'textarea', name:'gloss', label:'Glosa', column: 12},
                
                
            ],
            onComponentLoad : (formApi, pageApi) =>{
                if (pageApi.getPageMode() !== PAGE_MODE.NEW)
                    formApi.load();
            }
        },
        bar: {
            fields: [
                {...BUTTON_DEFAULT.SAVE, onClick: () => getForm().save()},
                {...BUTTON_DEFAULT.UPDATE, onClick: () => getForm().update()},
                {...BUTTON_DEFAULT.RETURN, link: '/sales/billing/billing-admin'}
            ],
        },
        grid: {}
    }
}
