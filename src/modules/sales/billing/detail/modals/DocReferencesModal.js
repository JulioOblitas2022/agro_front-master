import React from 'react';
import { DATE_FORMAT } from '../../../../../constants/constants';
import FormGridFw, { FORM_GRID_MODE } from '../../../../../framework/form-grid/FormGridFw';
import { SELECTION_TYPE } from '../../../../../framework/grid/GridFw';
import { BasePage } from '../../../../../framework/pages/BasePage';

export class DocReferencesModal extends BasePage {


    createPageProperties = (pageApi) => {
        return {
            title: 'Documentos de Referencia',
        };
    }

    renderPage(pageApi) {
        const formGridConfig = {
            mode: FORM_GRID_MODE.FORM_SEARCH_ADD,
            afterAddEvent: (data) => {
                pageApi.getEvent('afterAddEvent')(data);
                pageApi.hideThisModal();
            },
            form: {
                fields: [
                    { name: 'numDoc', label: 'N° Documento', column: 6, maxLength: 10 },
                    //{type: 'select', name:'customerId', label:'Cliente', column: 6, load: { queryId: 361 }, search: true},
                ]
            },
            grid: {
                title: 'Documentos seleccionados',
                rowId: 'codigo',
                showIndex: false,
                autoLoad: true,
                pagination: false,
                selection: { type: SELECTION_TYPE.SINGLE },
                load: {
                    queryId: 572,
                    params: () => {
                        return {customerId: pageApi?.name}
                    },
                    hideMessage: true,
                    hideMessageError: true
                },
                fields: [
                    { name: 'documentNumber', label: 'N° Documento' },
                    { name: 'customer', label: 'Proveedor' },
                    { type: 'date', name: 'orderDate', label: 'Fecha', format: DATE_FORMAT.DDMMYYYY },
                    { name: 'paymentCondition', label: 'Cond. Pago' },
                ]
            }
        };

        return (
            <>
                <FormGridFw
                    name="docRefGrid"
                    getParentApi={pageApi.getApi}
                    config={formGridConfig}
                />
            </>
        )

    }

}