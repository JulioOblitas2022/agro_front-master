import React from 'react';
import { DATE_FORMAT } from '../../../../../constants/constants';
import BarButtonFw, { BUTTON_DEFAULT } from '../../../../../framework/bar-button/BarButtonFw';
import FormGridFw, { FORM_GRID_MODE } from '../../../../../framework/form-grid/FormGridFw';
import FormFw from '../../../../../framework/form/FormFw';
import { SELECTION_TYPE } from '../../../../../framework/grid/GridFw';
import { BasePage } from '../../../../../framework/pages/BasePage';

export class DocumentModal extends BasePage {
    
    
    createPageProperties = (pageApi) => {
        return {
            title: 'Busqueda de Documentos',
        };
    }
    
    renderPage(pageApi) {
        const formGridConfig = {
            mode: FORM_GRID_MODE.FORM_SEARCH_ADD,
            afterAddEvent: (data) => {
                pageApi.getEvent('afterAddEvent')(data);
                pageApi.hideThisModal();
            },
            form: {
                fields:[
                    {type: 'select',name:'documentTypeId', label:'Tipo Documento', column: 6, load: {queryId: 195}},
                ]
            },
            grid: {
                title: 'Documentos seleccionados',
                rowId: 'codigo',
                showIndex: false,
                autoLoad: true,
                pagination: false,
                selection: {type: SELECTION_TYPE.MULTIPLE},
                load: {
                    queryId: 392,
                    orderBy: "1",
                    hideMessage: true,
                    hideMessageError: true
                },
                fields:[
                    { name:'documentType', label:'Tipo'},
                    { name:'docRefNumber', label:'Nro. Documento'},
                    { type: 'date', name: 'orderDate', label:'Fecha', format: DATE_FORMAT.DDMMYYYY },
                ]
            }
        };  
    
        return (
            <>
                <FormGridFw 
                    name="documentosGrid" 
                    getParentApi={pageApi.getApi}
                    config={formGridConfig}
                />
            </>
        )

    }
    
}