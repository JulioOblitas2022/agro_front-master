import React from 'react'
import BarButtonFw from '../../../../framework/bar-button/BarButtonFw';
import FormFw from '../../../../framework/form/FormFw';
import { BasePage } from '../../../../framework/pages/BasePage';
import { getConfigBillingDetail } from './getConfigBillingDetail';
import { getModeBillingDetail } from './getModeBillingDetail';
import { getValidationBillingDetail } from './getValidationBillingDetail';
import { createDocumentSection } from './sections/DocumentSection';
import { createItemSection } from './sections/ItemSection';

export class BillingDetailPage extends BasePage {
    
    createPageProperties = (pageApi) => {
        return {
            title: 'DETALLE DE FACTURACIÓN',
            validation: getValidationBillingDetail,
            mode: getModeBillingDetail
        };
    }

    renderPage(pageApi) {

        const config = getConfigBillingDetail(pageApi);

        return (
            <>
                <FormFw
                    name="form"
                    getParentApi={pageApi.getApi}
                    config={config.form}
                />
                
                {createDocumentSection(pageApi)}

                {createItemSection(pageApi)}

                <BarButtonFw
                    name="bar"
                    getParentApi={pageApi.getApi}
                    config={config.bar}
                />
            </>
        )
    }
}