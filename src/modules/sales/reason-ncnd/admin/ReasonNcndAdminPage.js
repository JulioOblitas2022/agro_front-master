import React from 'react'
import BarButtonFw, { BUTTON_DEFAULT } from '../../../../framework/bar-button/BarButtonFw';
import FormFw from '../../../../framework/form/FormFw';
import GridFw from '../../../../framework/grid/GridFw';
import { BasePage } from '../../../../framework/pages/BasePage';
import { DATE_FORMAT } from "../../../../constants/constants";
import { COLUMN_DEFAULT } from "../../../../framework/grid/GridFw";
import { getFormDefaultParams } from "../../../shared/util/getFormDefaultParams";
import { getGridServerDefaultParams } from "../../../shared/util/getGridServerDefaultParams";

export class ReasonNcndAdminPage extends BasePage {

    createPagePropertiesBase(pageApi) {
        return { 
            title: 'BANDEJA DE MOTIVOS NC/ND',
            barHeader:  {
                fields:[
                    {...BUTTON_DEFAULT.NEW, label:'Nuevo', link: '/sales/reason-ncnd/reason-ncnd/new'}
                ]
            }
        };
    }

    renderPage(pageApi) {
        const getGrid = () => pageApi.getComponent('grid');
        const getForm = () => pageApi.getComponent('form');

        const form = {
            ...getFormDefaultParams(),
            title: 'FILTROS DE BUSQUEDA',
            fields: [
                { type: 'date', name: 'startDate', label: 'Desde', column: 4, format: DATE_FORMAT.DDMMYYYY, inputFormat: DATE_FORMAT.YYYYMMDD },
                { type: 'date', name: 'endDate', label: 'Hasta', column: 4, format: DATE_FORMAT.DDMMYYYY, inputFormat: DATE_FORMAT.YYYYMMDD },
                { type: 'select', name: 'companyId', label: 'Empresa', column: 4, load: { queryId: 554, } },
            ]
        }

        const bar = {
            fields: [
                { ...BUTTON_DEFAULT.SEARCH, onClick: () => getGrid().load({ params: getForm().getData() }) },
                { ...BUTTON_DEFAULT.CLEAR, onClick: () => getForm().reset() }
            ]
        }

        const grid = {
            ...getGridServerDefaultParams({ queryLoad: 205 }),
            title: 'LISTA DE MOTIVOS',
            fields: [
                { name: 'description', label: 'Descripción', },
                { name: 'sunatCode', label: 'Cod. Sunat', },
                { type: 'date', name: 'createdAt', label: 'Fecha Registro', format: DATE_FORMAT.DDMMYYYY },
                { name: 'active', label: 'Estado', },
                { name: 'companyName', label: 'EMPRESA', },
                { ...COLUMN_DEFAULT.VIEW, link: (value, row, index) => `/sales/reason-ncnd/reason-ncnd/${row.reasonNcndId}`, label: 'Opciones', colSpan: 3 },
                { ...COLUMN_DEFAULT.EDIT, link: (value, row, index) => `/sales/reason-ncnd/reason-ncnd/${row.reasonNcndId}/update`, colSpan: 0 },
                { ...COLUMN_DEFAULT.DELETE, process: { fnOk: (result) => getGrid().load({ params: getForm().getData() }), filter: (value, row, index) => ({ queryId: 209, params: { reasonNcndId: row.reasonNcndId } }) }, colSpan: 0 }
            ]
        }

        return (
            <>
                <FormFw
                    name="form"
                    getParentApi={pageApi.getApi}
                    config={form}
                />
                <BarButtonFw
                    name="bar"
                    getParentApi={pageApi.getApi}
                    config={bar}
                />
                <GridFw
                    name="grid"
                    getParentApi={pageApi.getApi}
                    config={grid}
                />
            </>
        )
    }
}