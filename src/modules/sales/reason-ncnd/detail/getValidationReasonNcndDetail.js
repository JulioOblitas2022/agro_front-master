
export const getValidationReasonNcndDetail = () => {
    return {
        form: {
            description: [{ required: true, message: 'Descripcion es requerido' }],
            sunatCode: [{ required: true, message: 'Codigo Sunat es requerido' }],
            movementTypeId: [{ required: true, message: 'Tipo Movimiento es requerido' }],
            makesKardex: [{ required: true, message: 'Hace Kardex es requerido' }],
            companyId: [{ required: true, message: 'Empresa es requerido' }],
        },
        grid: {

        },
        button: {

        },
        bar: {

        }
    }
}
