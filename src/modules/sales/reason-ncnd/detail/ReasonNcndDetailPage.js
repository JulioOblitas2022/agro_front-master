import React from 'react'
import BarButtonFw from '../../../../framework/bar-button/BarButtonFw';
import FormFw from '../../../../framework/form/FormFw';
import { BasePage } from '../../../../framework/pages/BasePage';
import { getConfigReasonNcndDetail } from './getConfigReasonNcndDetail';
import { getModeReasonNcndDetail } from './getModeReasonNcndDetail';
import { getValidationReasonNcndDetail } from './getValidationReasonNcndDetail';

export class ReasonNcndDetailPage extends BasePage {
    
    createPageProperties = (pageApi) => {
        return {
            title: 'REGISTRO DE MOTIVO NC/ND',
            validation: getValidationReasonNcndDetail,
            mode: getModeReasonNcndDetail
        };
    }

    renderPage(pageApi) {

        const config = getConfigReasonNcndDetail(pageApi);

        return (
            <>
                <FormFw
                    name="form"
                    getParentApi={pageApi.getApi}
                    config={config.form}
                />
                <BarButtonFw
                    name="bar"
                    getParentApi={pageApi.getApi}
                    config={config.bar}
                />
            </>
        )
    }
}