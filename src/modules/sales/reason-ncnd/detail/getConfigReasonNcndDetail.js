import { PAGE_MODE } from "../../../../constants/constants";
import { BUTTON_DEFAULT } from "../../../../framework/bar-button/BarButtonFw";
import { UserUtil } from "../../../../util/UserUtil";

export const getConfigReasonNcndDetail = (pageApi) => {

    const getForm = () => pageApi.getComponent('form');

    return {
        form: {
            load: {
                queryId: 206,
                params: { reasonNcndId: pageApi.getParamId() },
                fnTransform: (resp) => {
                    const makesKardex = resp.dataObject.makesKardex;
                    resp.dataObject.makesKardex = makesKardex == 1 ? true : false;
                    return resp;
                },
            },
            update: {
                queryId: 208,
                postLink: (resp, values) => '/sales/reason-ncnd/reason-ncnd-admin',
                params: { modifiedBy: UserUtil.getUserId() }
            },
            save: {
                queryId: 207,
                postLink: (resp, values) => '/sales/reason-ncnd/reason-ncnd-admin',
                params: { createdBy: UserUtil.getUserId() }
            },
            fields: [
                { name: 'description', label: 'Descripcion', column: 4 },
                { name: 'sunatCode', label: 'Codigo Sunat', column: 4 },
                {
                    type: 'select', name: 'movementTypeId', label: 'Tipo Movimiento', column: 4,
                    load: {
                        dataList: [
                            { value: '1', label: 'CREDITO' },
                            { value: '2', label: 'DEBITO' },
                        ]
                    }
                },
                { type: 'select', name: 'companyId', label: 'Empresa', column: 4, load: { queryId: 554, } },
                { type: 'switch', name: 'makesKardex', label: 'Hace Kardex', column: 3, value: true },
                // {type: 'switch', name:'sino', label: '¿Acepta?', checked:"Si", unChecked:"No"}
            ],
            onComponentLoad: (formApi, pageApi) => {
                if (pageApi.getPageMode() !== PAGE_MODE.NEW)
                    formApi.load();
            }
        },
        bar: {
            fields: [
                { ...BUTTON_DEFAULT.SAVE, onClick: () => getForm().save() },
                { ...BUTTON_DEFAULT.UPDATE, onClick: () => getForm().update() },
                { ...BUTTON_DEFAULT.RETURN, link: '/sales/reason-ncnd/reason-ncnd-admin' }
            ],
        },
        grid: {}
    }
}