import React from 'react'
import { Route, Switch, useRouteMatch } from 'react-router';
import { ReasonNcndAdminPage } from './admin/ReasonNcndAdminPage';
import { ReasonNcndDetailPage } from './detail/ReasonNcndDetailPage';

const ReasonNcndRoutes = () => {
    const { path } = useRouteMatch();
    return (
        <Switch>
            <Route exact path={`${path}/reason-ncnd-admin`} component={ReasonNcndAdminPage} />
            <Route path={`${path}/reason-ncnd`} component={ReasonNcndDetailPage} />
            <Route path={`${path}/reason-ncnd/:id`} component={ReasonNcndDetailPage} />
        </Switch>
    )
}

export default ReasonNcndRoutes;