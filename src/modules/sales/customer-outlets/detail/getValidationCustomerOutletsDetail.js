
export const getValidationCustomerOutletsDetail = () => {
    return {
        form: {
            cenCode: [{ required: true, message: 'Codigo CEN es requerido' }],
            description: [{ required: true, message: 'Descripcion es requerido' }],
            address: [{ required: true, message: 'Direccion es requerido' }],
            departamentoId: [{ required: true, message: 'Departamento es requerido' }],
            provinciaId: [{ required: true, message: 'Provincia es requerido' }],
            distritoId: [{ required: true, message: 'Distrito es requerido' }],
            companyId: [{ required: true, message: 'Empresa es requerido' }],
        },
        grid: {

        },
        button: {

        },
        bar: {

        }
    }
}
