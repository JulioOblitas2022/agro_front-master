import { PAGE_MODE } from "../../../../constants/constants";
import { BUTTON_DEFAULT } from "../../../../framework/bar-button/BarButtonFw";
import { UserUtil } from "../../../../util/UserUtil";
import { B_SELECT_CONSTANTS } from "../../../shared/constantes/BSelectConstants";

export const getConfigCustomerOutletsDetail = (pageApi) => {

    const getForm = () => pageApi.getComponent('form');

    return {
        form: {
            load: {
                queryId: 359,
                params: { pointSaleId: pageApi.getParamId() },
            },
            update: {
                queryId: 360,
                postLink: (resp, values) => '/sales/customer-outlets/customer-outlets-admin',
                params: { modifiedBy: UserUtil.getUserId() , pointSaleId: pageApi.getParamId() },
            },
            save: {
                queryId: 357,
                postLink: (resp, values) => '/sales/customer-outlets/customer-outlets-admin',
                params: { createdBy: UserUtil.getUserId() },
            },
            fields: [
                { type: 'select', name: 'customerId', label: 'Cliente', column: 3, load: { queryId: 361 }, search: true },
                { name: 'cenCode', label: 'Codigo CEN', column: 3 },
                { name: 'description', label: 'Descripción', column: 3 },
                { name: 'address', label: 'Dirección', column: 3 },
                { ...B_SELECT_CONSTANTS.DEPARTAMENTO },
                { ...B_SELECT_CONSTANTS.PROVINCIA },
                { ...B_SELECT_CONSTANTS.DISTRITO },
                { type: 'select', name: 'companyId', label: 'Empresa', column: 4, load: { queryId: 554, } },
            ],
            onComponentLoad: (formApi, pageApi) => {
                if (pageApi.getPageMode() !== PAGE_MODE.NEW)
                    formApi.load();
            }
        },
        bar: {
            fields: [
                { ...BUTTON_DEFAULT.SAVE, onClick: () => getForm().save() },
                { ...BUTTON_DEFAULT.UPDATE, onClick: () => getForm().update() },
                { ...BUTTON_DEFAULT.RETURN, link: '/sales/customer-outlets/customer-outlets-admin' }
            ],
        },
        grid: {}
    }
}