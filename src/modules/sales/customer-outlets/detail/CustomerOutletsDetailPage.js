import React from 'react'
import BarButtonFw from '../../../../framework/bar-button/BarButtonFw';
import FormFw from '../../../../framework/form/FormFw';
import { BasePage } from '../../../../framework/pages/BasePage';
import { getConfigCustomerOutletsDetail } from './getConfigCustomerOutletsDetail';
import { getModeCustomerOutletsDetail } from './getModeCustomerOutletsDetail';
import { getValidationCustomerOutletsDetail } from './getValidationCustomerOutletsDetail';

export class CustomerOutletsDetailPage extends BasePage {
    
    createPageProperties = (pageApi) => {
        return {
            title: 'REGISTRO DE PUNTOS DE VENTA',
            validation: getValidationCustomerOutletsDetail,
            mode: getModeCustomerOutletsDetail
        };
    }

    renderPage(pageApi) {

        const config = getConfigCustomerOutletsDetail(pageApi);

        return (
            <>
                <FormFw
                    name="form"
                    getParentApi={pageApi.getApi}
                    config={config.form}
                />
                <BarButtonFw
                    name="bar"
                    getParentApi={pageApi.getApi}
                    config={config.bar}
                />
            </>
        )
    }
}
