import React from 'react'
import BarButtonFw, { BUTTON_DEFAULT } from '../../../../framework/bar-button/BarButtonFw';
import FormFw from '../../../../framework/form/FormFw';
import GridFw from '../../../../framework/grid/GridFw';
import { BasePage } from '../../../../framework/pages/BasePage';
import { DATE_FORMAT } from "../../../../constants/constants";
import { COLUMN_DEFAULT } from "../../../../framework/grid/GridFw";
import { getFormDefaultParams } from "../../../shared/util/getFormDefaultParams";
import { getGridServerDefaultParams } from "../../../shared/util/getGridServerDefaultParams";


export class CustomerOutletsAdminPage extends BasePage {

    createPagePropertiesBase(pageApi) {
        return { 
            title: 'BANDEJA DE PUNTOS DE VENTA',
            barHeader:  {
                fields:[
                    {...BUTTON_DEFAULT.NEW, label:'Nuevo', link: '/sales/customer-outlets/customer-outlets/new'}
                ]
            }
        };
    }

    renderPage(pageApi) {


        const getGrid = () => pageApi.getComponent('grid');
        const getForm = () => pageApi.getComponent('form');

        const form = {
            ...getFormDefaultParams(),
            title: 'FILTROS DE BUSQUEDA',
            fields: [
                { type: 'date', name: 'dateFrom', label: 'Desde', column: 4, format: DATE_FORMAT.DDMMYYYY, inputFormat: DATE_FORMAT.YYYYMMDD },
                { type: 'date', name: 'dateUntil', label: 'Hasta', column: 4, format: DATE_FORMAT.DDMMYYYY, inputFormat: DATE_FORMAT.YYYYMMDD },
                { type: 'select', name: 'companyId', label: 'Empresa', column: 4, load: { queryId: 554, } },
            ]
        };

        const bar = {
            fields: [
                { ...BUTTON_DEFAULT.SEARCH, onClick: () => getGrid().load({ params: getForm().getData() }) },
                { ...BUTTON_DEFAULT.CLEAR, onClick: () => getForm().reset() }
            ]
        };

        const grid = {
            ...getGridServerDefaultParams({ queryLoad: 358 }),
            title: 'RESULTADOS',
            fields: [
                { name: 'customer', label: 'Cliente', },
                { name: 'description', label: 'Punto Venta', },
                { name: 'address', label: 'Direccion', },
                { name: 'distrito', label: 'Distrito', },
                { type: 'date', name: 'fec_reg', label: 'Fecha Registro', format: DATE_FORMAT.DDMMYYYY },
                { name: 'companyName', label: 'EMPRESA', },
                { ...COLUMN_DEFAULT.VIEW, link: (value, row, index) => `/sales/customer-outlets/customer-outlets/${row.pointSaleId}`, label: 'Opciones', colSpan: 2 },
                { ...COLUMN_DEFAULT.EDIT, link: (value, row, index) => `/sales/customer-outlets/customer-outlets/${row.pointSaleId}/update`, colSpan: 0 },
            ]
        };

        return (
            <>
                <FormFw
                    name="form"
                    getParentApi={pageApi.getApi}
                    config={form}
                />
                <BarButtonFw
                    name="bar"
                    getParentApi={pageApi.getApi}
                    config={bar}
                />
                <GridFw
                    name="grid"
                    getParentApi={pageApi.getApi}
                    config={grid}
                />
            </>
        )
    }
}
