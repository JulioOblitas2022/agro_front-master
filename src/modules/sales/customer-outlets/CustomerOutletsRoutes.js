import React from 'react'
import { Route, Switch, useRouteMatch } from 'react-router';

import { CustomerOutletsAdminPage } from './admin/CustomerOutletsAdminPage';
import { CustomerOutletsDetailPage } from './detail/CustomerOutletsDetailPage';

const CustomerOutletsRoutes = () => {
    const { path } = useRouteMatch();
    return (
        <Switch>
            <Route exact path={`${path}/customer-outlets-admin`} component={CustomerOutletsAdminPage} />
            <Route path={`${path}/customer-outlets`} component={CustomerOutletsDetailPage} />
            <Route path={`${path}/customer-outlets/:id`} component={CustomerOutletsDetailPage} />
        </Switch>
    )
}

export default CustomerOutletsRoutes;