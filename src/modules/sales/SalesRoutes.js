import React, { lazy } from 'react'
import { Route, Switch, useRouteMatch } from 'react-router-dom';

const CustomerRoutes = lazy(() => import('./customer/CustomerRoutes'));
const TransferReasonRoutes = lazy(() => import('./transfer-reason/TransferReasonRoutes'));
const VehicleRoutes = lazy(() => import('./vehicle/VehicleRoutes'));
const DriverRoutes = lazy(() => import('./driver/DriverRoutes'));
const TransportCompanyRoutes = lazy(() => import('./transport-company/TransportCompanyRoutes'));
const SellerRoutes = lazy(() => import('./seller/SellerRoutes'));
const CustomerOutletsRoutes = lazy(() => import('./customer-outlets/CustomerOutletsRoutes'));
const BillingRoutes = lazy(() => import('./billing/BillingRoutes'));
const InternalSaleRoutes = lazy(() => import('./internal-sale/InternalSaleRoutes'));
const CreditNoteRoutes = lazy(() => import('./credit-note/CreditNoteRoutes'));
const DebitNoteRoutes = lazy(() => import('./debit-note/DebitNoteRoutes'));
const ProductRoutes = lazy(() => import('./product/ProductRoutes'));
const OrderRoutes = lazy(() => import('./order/OrderRoutes'));
const ProformasRoutes = lazy(() => import('./proformas/ProformasRoutes'));
const ReasonNcndRoutes = lazy(() => import('./reason-ncnd/ReasonNcndRoutes'));
const ExistenceTypeRoutes = lazy(() => import('./existence-type/ExistenceTypeRoutes'));
const CustomerOrderRoutes = lazy(() => import('./customer-order/CustomerOrderRoutes'));
const ReferralGuideRoutes = lazy(() => import('./referral-guide/ReferralGuideRoutes'));
const ReportsRoutes = lazy(() => import('./reports/ReportsRoutes'));

const SalesRoutes = () => {
    const { path } = useRouteMatch();
    return (
        <Switch>
            <Route path={`${path}/customer`} component={CustomerRoutes} />
            <Route path={`${path}/transfer-reason`} component={TransferReasonRoutes} />
            <Route path={`${path}/vehicle`} component={VehicleRoutes} />
            <Route path={`${path}/driver`} component={DriverRoutes} />
            <Route path={`${path}/transport-company`} component={TransportCompanyRoutes} />
            <Route path={`${path}/seller`} component={SellerRoutes} />
            <Route path={`${path}/customer-outlets`} component={CustomerOutletsRoutes} />
            <Route path={`${path}/billing`} component={BillingRoutes} />
            <Route path={`${path}/internal-sale`} component={InternalSaleRoutes} />
            <Route path={`${path}/credit-note`} component={CreditNoteRoutes} />
            <Route path={`${path}/debit-note`} component={DebitNoteRoutes} />
            <Route path={`${path}/product`} component={ProductRoutes} />
            <Route path={`${path}/order`} component={OrderRoutes} />
            <Route path={`${path}/proformas`} component={ProformasRoutes} />
            <Route path={`${path}/reason-ncnd`} component={ReasonNcndRoutes} />
            <Route path={`${path}/existence-type`} component={ExistenceTypeRoutes} />
            <Route path={`${path}/customer-order`} component={CustomerOrderRoutes} />
            <Route path={`${path}/referral-guide`} component={ReferralGuideRoutes} />
            <Route path={`${path}/reports`} component={ReportsRoutes} />
        </Switch>
    )
}

export default SalesRoutes;
