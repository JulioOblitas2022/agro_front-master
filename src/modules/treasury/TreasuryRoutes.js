import React, { lazy } from 'react';
import { Route, Switch, useRouteMatch } from 'react-router-dom';

const BankRoutes = lazy(() => import('./bank/BankRoutes'));
const BankAccountRoutes = lazy(() => import('./bank-account/BankAccountRoutes'));
const BankDocumentRoutes = lazy(() => import('./bank-document/BankDocumentRoutes'));
const PaymentMethodRoutes = lazy(() => import('./payment-method/PaymentMethodRoutes'));
const SourceInputsRoutes = lazy(() => import('./source-inputs/SourceInputsRoutes'));
const SourceOutputsRoutes = lazy(() => import('./source-outputs/SourceOutputsRoutes'));
const InputsRoutes = lazy(() => import('./inputs/InputsRoutes'));
const OutputsRoutes = lazy(() => import('./outputs/OutputsRoutes'));
const ExchangDocsRoutes = lazy(() => import('./exchange-docs/ExchangDocsRoutes'));
const DestinationInputsRoutes = lazy(() => import('./destination-inputs/DestinationInputsRoutes'));
const DestinationOutputsRoutes = lazy(() => import('./destination-outputs/DestinationOutputsRoutes'));
const LetterCustomerRoutes = lazy(() => import('./letters-customer/LetterCustomerRoutes'));


const TreasuryRoutes = () => {
    const { path } = useRouteMatch();
    return (
        <Switch>
            <Route path={`${path}/bank`} component={BankRoutes} />
            <Route path={`${path}/bank-account`} component={BankAccountRoutes} />
            <Route path={`${path}/bank-document`} component={BankDocumentRoutes} />
            <Route path={`${path}/payment-method`} component={PaymentMethodRoutes} />
            <Route path={`${path}/source-inputs`} component={SourceInputsRoutes} />
            <Route path={`${path}/source-outputs`} component={SourceOutputsRoutes} />
            <Route path={`${path}/destination-inputs`} component={DestinationInputsRoutes} />
            <Route path={`${path}/destination-outputs`} component={DestinationOutputsRoutes} />
            <Route path={`${path}/inputs`} component={InputsRoutes} />
            <Route path={`${path}/outputs`} component={OutputsRoutes} />
            <Route path={`${path}/exchange-docs`} component={ExchangDocsRoutes} />
            <Route path={`${path}/letters-customer`} component={LetterCustomerRoutes} />
        </Switch>
    )
}

export default TreasuryRoutes;
