
export const getValidationSourceOutputsDetail = () => {
    return {
        form: {
            accountNumber: [ {required: true, message: 'Cta. Contable es requerida' } ],
            moduleId: [ {required: true, message: 'Modulo es requerido' } ],
            coinId: [ {required: true, message: 'Moneda es requerida' } ],
            bankId: [ {required: true, message: 'Banco es requerido' } ],
            bankAccountId: [ {required: true, message: 'Cta. Banco es requerido' } ],
        },
        grid: {

        },
        button: {

        },
        bar: {

        }
    }
}
