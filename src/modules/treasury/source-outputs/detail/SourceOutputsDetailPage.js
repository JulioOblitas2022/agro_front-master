import React from 'react'
import BarButtonFw from '../../../../framework/bar-button/BarButtonFw';
import FormFw from '../../../../framework/form/FormFw';
import { BasePage } from '../../../../framework/pages/BasePage';
import { getConfigSourceOutputsDetail } from './getConfigSourceOutputsDetail';
import { getModeSourceOutputsDetail } from './getModeSourceOutputsDetail';
import { getValidationSourceOutputsDetail } from './getValidationSourceOutputsDetail';

export class SourceOutputsDetailPage extends BasePage {
    
    createPageProperties = (pageApi) => {
        return {
            title: 'REGISTRO DE CONCEPTOS ORIGEN - EGRESOS',
            validation: getValidationSourceOutputsDetail,
            mode: getModeSourceOutputsDetail
        };
    }
    
    renderPage(pageApi) {

        const config = getConfigSourceOutputsDetail(pageApi);

        return (
            <>
                <FormFw
                    name="form"
                    getParentApi={pageApi.getApi}
                    config={config.form}
                />
                <BarButtonFw
                    name="bar"
                    getParentApi={pageApi.getApi}
                    config={config.bar}
                />
            </>
        )
    }
}