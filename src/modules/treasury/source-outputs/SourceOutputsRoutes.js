import React from 'react'
import { Route, Switch, useRouteMatch } from 'react-router';
import { SourceOutputsAdminPage } from './admin/SourceOutputsAdminPage';
import { SourceOutputsDetailPage } from './detail/SourceOutputsDetailPage';

const SourceOutputsRoutes = () => {
    const { path } = useRouteMatch();
    return (
        <Switch>
            <Route exact path={`${path}/source-outputs-admin`} component={SourceOutputsAdminPage} />
            <Route path={`${path}/source-outputs`} component={SourceOutputsDetailPage} />
            <Route path={`${path}/source-outputs/:id`} component={SourceOutputsDetailPage} /> 
        </Switch>
    )
}

export default SourceOutputsRoutes;
