import React from 'react'
import BarButtonFw from '../../../../framework/bar-button/BarButtonFw';
import FormFw from '../../../../framework/form/FormFw';
import { BasePage } from '../../../../framework/pages/BasePage';
import { getConfigDestinationInputsDetail } from './getConfigDestinationInputsDetail';
import { getModeDestinationInputsDetail } from './getModeDestinationInputsDetail';
import { getValidationDestinationInputsDetail } from './getValidationDestinationInputsDetail';

export class DestinationInputsDetailPage extends BasePage {
    
    createPageProperties = (pageApi) => {
        return {
            title: 'REGISTRO DE CONCEPTOS DESTINO - INGRESOS',
            validation: getValidationDestinationInputsDetail,
            mode: getModeDestinationInputsDetail
        };
    }
    
    renderPage(pageApi) {

        const config = getConfigDestinationInputsDetail(pageApi);

        return (
            <>
                <FormFw
                    name="form"
                    getParentApi={pageApi.getApi}
                    config={config.form}
                />
                <BarButtonFw
                    name="bar"
                    getParentApi={pageApi.getApi}
                    config={config.bar}
                />
            </>
        )
    }
}