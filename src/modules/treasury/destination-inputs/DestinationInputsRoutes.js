import React from 'react'
import { Route, Switch, useRouteMatch } from 'react-router';
import { DestinationInputsAdminPage } from './admin/DestinationInputsAdminPage';
import { DestinationInputsDetailPage } from './detail/DestinationInputsDetailPage';

const DestinationInputsRoutes = () => {
    const { path } = useRouteMatch();
    return (
        <Switch>
            <Route exact path={`${path}/destination-inputs-admin`} component={DestinationInputsAdminPage} />
            <Route path={`${path}/destination-inputs`} component={DestinationInputsDetailPage} />
            <Route path={`${path}/destination-inputs/:id`} component={DestinationInputsDetailPage} /> 
        </Switch>
    )
}

export default DestinationInputsRoutes;
