import React from 'react'
import { Route, Switch, useRouteMatch } from 'react-router';
import { SourceInputsAdminPage } from './admin/SourceInputsAdminPage';
import { SourceInputsDetailPage } from './detail/SourceInputsDetailPage';

const SourceInputsRoutes = () => {
    const { path } = useRouteMatch();
    return (
        <Switch>
            <Route exact path={`${path}/source-inputs-admin`} component={SourceInputsAdminPage} />
            <Route path={`${path}/source-inputs`} component={SourceInputsDetailPage} />
            <Route path={`${path}/source-inputs/:id`} component={SourceInputsDetailPage} /> 
        </Switch>
    )
}

export default SourceInputsRoutes;
