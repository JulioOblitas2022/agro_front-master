import React from 'react'
import BarButtonFw from '../../../../framework/bar-button/BarButtonFw';
import FormFw from '../../../../framework/form/FormFw';
import { BasePage } from '../../../../framework/pages/BasePage';
import { getConfigSourceInputsDetail } from './getConfigSourceInputsDetail';
import { getModeSourceInputsDetail } from './getModeSourceInputsDetail';
import { getValidationSourceInputsDetail } from './getValidationSourceInputsDetail';

export class SourceInputsDetailPage extends BasePage {
    
    createPageProperties = (pageApi) => {
        return {
            title: 'REGISTRO DE CONCEPTOS ORIGEN - INGRESOS',
            validation: getValidationSourceInputsDetail,
            mode: getModeSourceInputsDetail
        };
    }
    
    renderPage(pageApi) {

        const config = getConfigSourceInputsDetail(pageApi);

        return (
            <>
                <FormFw
                    name="form"
                    getParentApi={pageApi.getApi}
                    config={config.form}
                />
                <BarButtonFw
                    name="bar"
                    getParentApi={pageApi.getApi}
                    config={config.bar}
                />
            </>
        )
    }
}