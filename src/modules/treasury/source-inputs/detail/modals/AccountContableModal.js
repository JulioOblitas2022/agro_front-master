import React from 'react';
import FormGridFw, { FORM_GRID_MODE } from '../../../../../framework/form-grid/FormGridFw';
import { SELECTION_TYPE } from '../../../../../framework/grid/GridFw';
import { BasePage } from '../../../../../framework/pages/BasePage';

export class AccountContableModal extends BasePage {

    createPageProperties = (pageApi) => {
        return {
            title: 'Cuentas Contables',
        };
    }

    renderPage(pageApi) {
        const formGridConfig = {
            mode: FORM_GRID_MODE.FORM_SEARCH_ADD,
            afterAddEvent: (data) => {
                pageApi.getEvent('afterAddEvent')(data);
                pageApi.hideThisModal();
            },
            form: {
                fields: [
                    { name: 'description', label: 'Decripción', column: 6 },
                ]
            },
            grid: {
                title: 'Documentos seleccionados',
                rowId: 'codigo',
                showIndex: false,
                autoLoad: true,
                pagination: false,
                selection: { type: SELECTION_TYPE.SINGLE },
                load: {
                    queryId: 578,
                    params: {companyId: parseInt(pageApi?.name?.companyId)}
                },
                fields: [
                    { name: 'accountNumber', label: 'N° Cuenta' },
                    { name: 'description', label: 'Descripción' },
                ]
            }
        };

        return (
            <>
                <FormGridFw
                    name="accountContableGrid"
                    getParentApi={pageApi.getApi}
                    config={formGridConfig}
                />
            </>
        )

    }

}