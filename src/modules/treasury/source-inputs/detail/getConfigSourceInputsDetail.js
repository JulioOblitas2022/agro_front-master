import { PAGE_MODE } from "../../../../constants/constants";
import { BUTTON_DEFAULT } from "../../../../framework/bar-button/BarButtonFw";
import { MessageUtil } from "../../../../util/MessageUtil";
import { UserUtil } from "../../../../util/UserUtil";
import { AccountContableModal } from "./modals/AccountContableModal";

export const getConfigSourceInputsDetail = (pageApi) => {
    
    const getForm = () => pageApi.getComponent('form');

    const accountContableConfig = {
        events: {
            afterAddEvent: (data) => {

                getForm().setFieldData('accountPlanId', data[0].accountPlanId);
                getForm().setFieldData('accountNumber', data[0].accountNumber);
                getForm().setFieldData('accountPlan', data[0].description);

            }
        }
    }

    const AccountContableModalConfig = (id) => {
        return {
            width: 1000,
            maskClosable: false,
            component: <AccountContableModal name={{companyId: id}} getParentApi={pageApi.getApi} config={accountContableConfig} />
        }
    };

    return {
        form: {
            load:{
                queryId: 249,
                params: {originId: pageApi.getParamId()},
            },
            update:{
                queryId: 248,
                postLink: (resp, values) => '/treasury/source-inputs/source-inputs-admin',
                params: {modifiedBy: UserUtil.getUserId()}
            },
            save:{
                queryId: 247,
                postLink: (resp, values) => '/treasury/source-inputs/source-inputs-admin',
                params: {createdBy: UserUtil.getUserId(), type: 1}
            },
            fields:[
                { type: 'select', name: 'companyId', label: 'Empresa', column: 4, load: { queryId: 554, }, search: true },
                { type: 'hidden', name: 'accountPlanId' },
                { name:'accountNumber', label:'Nro. Cta. Contable', column: 4, readOnly: true, },
                { type: 'button', column: 4, name: 'modal',
                    onClick: (value) => {
                        let companyId = getForm()?.getFieldApi('companyId')?.getSelectedItem()?.value;
                        if (companyId !== undefined) {
                            pageApi.showModal(AccountContableModalConfig(companyId));
                        } else {
                            return MessageUtil('warning', 'Alerta!', 'Debe Seleccionar Empresa');
                        }
                    },
                },
                { name:'accountPlan', label:'Descripción de Cta.', column: 4, readOnly: true},
                { type: 'select', name:'moduleId', label:'Modulo', column: 4, load:{ queryId: 409 }, search: true},
                { type: 'select', name:'coinId', label:'Moneda', column: 4, load:{ queryId: 187 }, search: true},
                { type: 'radio', name:'detail', label:'Detallar', column: 4, value: 1, load:{ dataList:[{value:1, label:'Detallar'},{value:0,label:'No Detallar'}] }},
                { type: 'radio', name:'originCoin', label:'Origen Dinero', column: 4, value: 1, load:{ dataList:[{value:1, label:'Efectivo'},{value:2,label:'Cuenta Corriente'}] }},
                { type: 'select', name:'bankId', label:'Banco', column: 4, load:{ queryId: 119 }, search: true},
                { type: 'select', name:'bankAccountId', label:'Cta. Banco', column: 4, load:{ queryId: 246 }, search: true},
            ],
            onComponentLoad : (formApi, pageApi) =>{
                if (pageApi.getPageMode() !== PAGE_MODE.NEW)
                    formApi.load();
            }
        },
        bar: {
            fields: [
                {...BUTTON_DEFAULT.SAVE, onClick: () => getForm().save()},
                {...BUTTON_DEFAULT.UPDATE, onClick: () => getForm().update()},
                {...BUTTON_DEFAULT.RETURN, link: '/treasury/source-inputs/source-inputs-admin'}
            ],
        },
        grid: {}
    }
}