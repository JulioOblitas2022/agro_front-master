import React from 'react';
import { Route, Switch, useRouteMatch } from 'react-router-dom';

import { PaymentMethodAdminPage } from './admin/PaymentMethodAdminPage';
import { PaymentMethodDetailPage } from './detail/PaymentMethodDetailPage';

const PaymentMethodRoutes = () => {
    const { path } = useRouteMatch();
    return (
        <Switch>
            <Route exact path={`${path}/payment-method-admin`} component={PaymentMethodAdminPage} />
            <Route path={`${path}/payment-method`} component={PaymentMethodDetailPage} />
            <Route path={`${path}/payment-method/:id`} component={PaymentMethodDetailPage} /> 
        </Switch>
    )
}

export default PaymentMethodRoutes;