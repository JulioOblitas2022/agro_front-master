import { PAGE_MODE } from '../../../../constants/constants';
import { BUTTON_DEFAULT } from '../../../../framework/bar-button/BarButtonFw';

export const getConfigPaymentMethodDetail = (pageApi) => {
    const getForm = () => pageApi.getComponent('form');
    return {
        form: {
            load:{
                queryId: 142,
                params: {paymentMethodId: pageApi.getParamId()},
            },
            update:{
                queryId: 141,
                postLink: (resp, values) => '/treasury/payment-method/payment-method-admin'
            },
            save:{
                queryId: 139,
                postLink: (resp, values) => '/treasury/payment-method/payment-method-admin'
            },
            title: 'REGITRO DE MÉTODOS DE PAGO',
            fields:[
                {type: 'divider', label: 'DATOS PRINCIPALES'},
                {name:'description', label:'Descripción', column: 4},
                {name:'code', label:'Código', column: 4},
            ],
            onComponentLoad : (formApi, pageApi) =>{
                if (pageApi.getPageMode() !== PAGE_MODE.NEW)
                    formApi.load();
            }
        },
        bar: {
            fields: [
                {...BUTTON_DEFAULT.SAVE, onClick: () => getForm().save()},
                {...BUTTON_DEFAULT.UPDATE, onClick: () => getForm().update()},
                {...BUTTON_DEFAULT.RETURN, link: '/treasury/payment-method/payment-method-admin'}
            ],
        },
        grid: {}
    }
}
