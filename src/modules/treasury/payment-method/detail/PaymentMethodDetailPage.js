import React from 'react'
import BarButtonFw from '../../../../framework/bar-button/BarButtonFw';
import FormFw from '../../../../framework/form/FormFw';
import { BasePage } from '../../../../framework/pages/BasePage';
import { getConfigPaymentMethodDetail } from './getConfigPaymentMethodDetail';
import { getModePaymentMethodDetail } from './getModePaymentMethodDetail';
import { getValidationPaymentMethodDetail } from './getValidationPaymentMethodDetail';

export class PaymentMethodDetailPage extends BasePage {
    
    createPageProperties = (pageApi) => {
        return {
            title: 'DETALLE DE MÉTODOS DE PAGO',
            validation: getValidationPaymentMethodDetail,
            mode: getModePaymentMethodDetail
        };
    }

    renderPage(pageApi) {

        const config = getConfigPaymentMethodDetail(pageApi);

        return (
            <>
                <FormFw
                    name="form"
                    getParentApi={pageApi.getApi}
                    config={config.form}
                />
                <BarButtonFw
                    name="bar"
                    getParentApi={pageApi.getApi}
                    config={config.bar}
                />
            </>
        )
    }
}