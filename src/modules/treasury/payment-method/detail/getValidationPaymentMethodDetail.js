export const getValidationPaymentMethodDetail = () => {
    return {
        form: {
            description: [ {required: true, message: 'Descripción es requerido' } ],
            code: [ {required: true, message: 'Código es requerido' } ],
        },
        grid: {

        },
        button: {

        },
        bar: {

        }
    }
}
