import React from 'react'
import BarButtonFw, { BUTTON_DEFAULT } from '../../../../framework/bar-button/BarButtonFw';
import FormFw from '../../../../framework/form/FormFw';
import GridFw from '../../../../framework/grid/GridFw';
import { BasePage } from '../../../../framework/pages/BasePage';
import { COLUMN_DEFAULT } from "../../../../framework/grid/GridFw";
import { getFormDefaultParams } from "../../../shared/util/getFormDefaultParams";
import { getGridServerDefaultParams } from "../../../shared/util/getGridServerDefaultParams";
import { DATE_FORMAT } from "../../../../constants/constants";

export class InputsAdminPage extends BasePage {
    createPagePropertiesBase(pageApi) {
        return {
            title: 'BANDEJA DE INGRESOS',
            barHeader: {
                fields:[
                    {...BUTTON_DEFAULT.NEW, label:'Nuevo', link: '/treasury/inputs/inputs/new'}
                ]
            }
        };
    }

    renderPage(pageApi) {
        const getGrid = () => pageApi.getComponent('grid');
        const getForm = () => pageApi.getComponent('form');

        const form = {
            ...getFormDefaultParams(),
            title: 'FILTROS DE BUSQUEDA',
            fields:[
                { type: 'select', name: 'companyId', label: 'Empresa', column: 4, load: { queryId: 554, }, search: true },
                { type: 'select', name:'coinId', label:'Moneda', column: 4, load:{queryId:187}},
            ]
        }

        const bar = { fields: [
            {...BUTTON_DEFAULT.SEARCH, onClick: ()=>getGrid().load({params:getForm().getData()})},
            {...BUTTON_DEFAULT.CLEAR, onClick: ()=>getForm().reset()}
        ]}

        const grid = {
            ...getGridServerDefaultParams({queryLoad: 432, type: 1}),
            title: 'RESULTADOS',
            fields:[
                { type: 'date', name: 'operationDate', label: 'Fec. Emisión', inputFormat: DATE_FORMAT.DDMMYYYY},
                { name: 'coin', label: 'Moneda',},
                { name: 'seatNumber', label: 'Asiento',},
                {...COLUMN_DEFAULT.VIEW, link: (value, row, index) => `/treasury/inputs/inputs/${row.treasuryId}`, label: 'Opciones', colSpan: 2 },
                {...COLUMN_DEFAULT.EDIT, link: (value, row, index) => `/treasury/inputs/inputs/${row.treasuryId}/update`, colSpan: 0 },
            ]
        }
        return (
            <>
                <FormFw
                    name="form"
                    getParentApi={pageApi.getApi}
                    config={form}
                />
                <BarButtonFw
                    name="bar"
                    getParentApi={pageApi.getApi}
                    config={bar}
                />
                <GridFw
                    name="grid"
                    getParentApi={pageApi.getApi}
                    config={grid}
                />
            </>
        )
    }
}