import React from 'react'
import { Route, Switch, useRouteMatch } from 'react-router';
import { InputsAdminPage } from './admin/InputsAdminPage';
import { InputsDetailPage } from './detail/InputsDetailPage';

export const InputsRoutes = () => {
    const { path } = useRouteMatch();
    return (
        <Switch>
            <Route exact path={`${path}/inputs-admin`} component={InputsAdminPage} />
            <Route path={`${path}/inputs`} component={InputsDetailPage} />
            <Route path={`${path}/inputs/:id`} component={InputsDetailPage} /> 
        </Switch>
    )
}

export default InputsRoutes;
