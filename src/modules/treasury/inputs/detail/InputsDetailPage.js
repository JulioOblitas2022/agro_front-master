import React from 'react'
import BarButtonFw from '../../../../framework/bar-button/BarButtonFw';
import FormFw from '../../../../framework/form/FormFw';
import { BasePage } from '../../../../framework/pages/BasePage';
import { getConfigInputsDetail } from './getConfigInputsDetail';
import { getModeInputsDetail } from './getModeInputsDetail';
import { getValidationInputsDetail } from './getValidationInputsDetail';
import { createDestinationInputSection } from './sections/destinationInputSection';
import { createSourceInputSection } from './sections/sourceInputSection';

export class InputsDetailPage extends BasePage {
    
    createPageProperties = (pageApi) => {
        return {
            title: 'REGISTRO DE INGRESOS',
            validation: getValidationInputsDetail,
            mode: getModeInputsDetail
        };
    }
    
    renderPage(pageApi) {

        const config = getConfigInputsDetail(pageApi);

        return (
            <>
                <FormFw
                    name="form"
                    getParentApi={pageApi.getApi}
                    config={config.form}
                />

                {createSourceInputSection(pageApi)}
                
                {createDestinationInputSection(pageApi)}

                <BarButtonFw
                    name="bar"
                    getParentApi={pageApi.getApi}
                    config={config.bar}
                />
            </>
        )
    }
}