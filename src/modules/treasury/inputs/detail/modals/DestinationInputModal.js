import React from 'react';
import { SELECTION_TYPE } from '../../../../../framework/grid/GridFw';
import FormGridFw, { FORM_GRID_MODE } from '../../../../../framework/form-grid/FormGridFw';
import { BasePage } from "../../../../../framework/pages/BasePage";

export class DestinationInputModal extends BasePage {

    createPageProperties = (pageApi) => {
        return { title: 'DESTINO - INGRESOS' };
    }
    renderPage(pageApi) {
        const formGridConfig = {
            mode: FORM_GRID_MODE.FORM_SEARCH_ADD,
            afterAddEvent: (data) => {
                pageApi.getEvent('afterAddEvent')(data);
                pageApi.hideThisModal();
            },
            form: {
                fields:[
                    {name:'accountNumber', label:'Nro. Cuenta', column: 4},
                ]
            },
            grid: {
                title: 'Lista de Ingresos',
                rowId: 'codigo',
                showIndex: true,
                autoLoad: true,
                pagination: {server: true},
                selection: {type: SELECTION_TYPE.MULTIPLE},
                load: {
                    queryId: 434,
                    orderBy: "1"
                },
                fields:[
                    {name:'accountNumber', label:'Nro. Cuenta'},
                    {name:'accountPlan', label:'Descripción'},
                ]
            }
        };  
    
        return (
            <FormGridFw 
                name="destinationInputGrid" 
                getParentApi={pageApi.getApi}
                config={formGridConfig}
            />
        )

    }
    
}
