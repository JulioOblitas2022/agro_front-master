import React from 'react'
import BarButtonFw, { BUTTON_DEFAULT } from '../../../../framework/bar-button/BarButtonFw';
import FormFw from '../../../../framework/form/FormFw';
import GridFw from '../../../../framework/grid/GridFw';
import { BasePage } from '../../../../framework/pages/BasePage';
import { COLUMN_DEFAULT } from '../../../../framework/grid/GridFw';
import { getFormDefaultParams } from '../../../shared/util/getFormDefaultParams';
import { getGridServerDefaultParams } from '../../../shared/util/getGridServerDefaultParams';

export class BankAccountAdminPage extends BasePage {
    createPagePropertiesBase(pageApi) {
        return { 
            title: 'CUENTAS DE BANCO',
            barHeader:{
                fields:[
                    {...BUTTON_DEFAULT.NEW, label:'Nuevo', link: '/treasury/bank-account/bank-account/new'}
                ] 
            } 
        };
    }

    renderPage(pageApi) {
        const getGrid = () => pageApi.getComponent('grid');
        const getForm = () => pageApi.getComponent('form');
        
        const form = {
            ...getFormDefaultParams(),
            title: 'BANDEJA DE CUENTA DE BANCOS',
            fields:[
                {type: 'divider', label: 'Filtros de busqueda'},
                { type: 'select', name: 'companyId', label: 'Empresa', column: 4, load: { queryId: 554 }, search: true },
                {name:'accountNumber', label:'N° de cuenta', column: 4},
                {type: 'select', name:'bankId', label:'Banco', column: 4, load: { queryId: 119 } },
                {type: 'select', name:'coin', label:'Moneda', column: 4, load: { queryId: 187 } },
            ]
        }

        const bar = { fields: [
            {...BUTTON_DEFAULT.SEARCH, onClick: ()=>getGrid().load({params:getForm().getData()})},
            {...BUTTON_DEFAULT.CLEAR, onClick: ()=>getForm().reset()}
        ]}

        const grid = {
            ...getGridServerDefaultParams({queryLoad: 123}),
            title: 'Lista - Cuentas de Banco',
            fields:[
                {name: 'company', label: 'Empresa',},
                {name: 'bank', label: 'Banco',},
                {name: 'accountNumber', label: 'N° de cuenta',},
                {name: 'coin', label: 'Tipo de Moneda',},
                {...COLUMN_DEFAULT.VIEW, link: (value, row, index) => `/treasury/bank-account/bank-account/id-${row.bankAccountId}`, label: 'Opciones', colSpan: 3 },
                {...COLUMN_DEFAULT.EDIT, link: (value, row, index) => `/treasury/bank-account/bank-account/id-${row.bankAccountId}/update`, colSpan: 0 },
                {...COLUMN_DEFAULT.DELETE, colSpan: 0, process: {fnOk: (result)=>getGrid().load({params:getForm().getData()}),filter: (value, row, index) => ({queryId: 125, params: {bankAccountId: row.bankAccountId}}) }}
            ]
        }
        return (
            <>
                <FormFw
                    name="form"
                    getParentApi={pageApi.getApi}
                    config={form}
                />
                <BarButtonFw
                    name="bar"
                    getParentApi={pageApi.getApi}
                    config={bar}
                />
                <GridFw
                    name="grid"
                    getParentApi={pageApi.getApi}
                    config={grid}
                />
            </>
        )
    }
}
