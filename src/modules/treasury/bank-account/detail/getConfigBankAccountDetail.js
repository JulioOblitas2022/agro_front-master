import { PAGE_MODE } from '../../../../constants/constants';
import { BUTTON_DEFAULT } from '../../../../framework/bar-button/BarButtonFw';
import { UserUtil } from '../../../../util/UserUtil';

export const getConfigBankAccountDetail = (pageApi) => {
    const getForm = () => pageApi.getComponent('form');
    return {
        form: {
            load:{
                queryId: 126,
                params: {bankAccountId: pageApi.getParamId()},
            },
            update:{
                queryId: 127,
                postLink: (resp, values) => '/treasury/bank-account/bank-account-admin',
                params: {modifiedBy: UserUtil.getUserId()}
            },
            save:{
                queryId: 124,
                postLink: (resp, values) => '/treasury/bank-account/bank-account-admin',
                params: {createdBy: UserUtil.getUserId()}
            },
            title: 'REGITRO DE CUENTAS DE BANCO',
            fields:[
                {type: 'divider', label: 'DATOS PRINCIPALES'},
                { type: 'select', name: 'companyId', label: 'Empresa', column: 4, load: { queryId: 554 }, search: true },
                { name:'accountNumber', label:'N° de cuenta', column: 4},
                { type: 'select', name:'bankId', label:'Banco', column: 4, load: { queryId: 119 } },
                { type: 'select', name:'coinId', label:'Moneda', column: 4, load: { queryId: 187 } },
            ],
            onComponentLoad : (formApi, pageApi) =>{
                if (pageApi.getPageMode() !== PAGE_MODE.NEW)
                    formApi.load();
            }
        },
        bar: {
            fields: [
                {...BUTTON_DEFAULT.SAVE, onClick: () => getForm().save()},
                {...BUTTON_DEFAULT.UPDATE, onClick: () => getForm().update()},
                {...BUTTON_DEFAULT.RETURN, link: '/treasury/bank-account/bank-account-admin'}
            ],
        },
        grid: {}
    }
}
