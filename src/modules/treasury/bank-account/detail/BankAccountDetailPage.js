import React from 'react'
import BarButtonFw from '../../../../framework/bar-button/BarButtonFw';
import FormFw from '../../../../framework/form/FormFw';
import { BasePage } from '../../../../framework/pages/BasePage';
import { getConfigBankAccountDetail } from './getConfigBankAccountDetail';
import { getModeBankAccountDetail } from './getModeBankAccountDetail';
import { getValidationBankAccountDetail } from './getValidationBankAccountDetail';

export class BankAccountDetailPage extends BasePage {
    
    createPageProperties = (pageApi) => {
        return {
            title: 'DETALLE DE CUENTAS DE BANCO',
            validation: getValidationBankAccountDetail,
            mode: getModeBankAccountDetail
        };
    }

    renderPage(pageApi) {

        const config = getConfigBankAccountDetail(pageApi);

        return (
            <>
                <FormFw
                    name="form"
                    getParentApi={pageApi.getApi}
                    config={config.form}
                />
                <BarButtonFw
                    name="bar"
                    getParentApi={pageApi.getApi}
                    config={config.bar}
                />
            </>
        )
    }
}