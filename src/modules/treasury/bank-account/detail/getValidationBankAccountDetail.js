export const getValidationBankAccountDetail = () => {
    return {
        form: {
            bankId: [ {required: true, message: 'Banco es requerido' } ],
            accountNumber: [ {required: true, message: 'N° de cuenta es requerido' } ],
            coinId: [ {required: true, message: 'El Tipo de moneda es requerido' } ],
        },
        grid: {

        },
        button: {

        },
        bar: {

        }
    }
}
