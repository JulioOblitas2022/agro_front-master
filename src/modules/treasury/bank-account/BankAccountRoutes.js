import React from 'react';
import { Route, Switch, useRouteMatch } from 'react-router-dom';

import { BankAccountAdminPage } from './admin/BankAccountAdminPage';
import { BankAccountDetailPage } from './detail/BankAccountDetailPage';

const BankAccountRoutes = () => {
    const { path } = useRouteMatch();
    return (
        <Switch>
            <Route exact path={`${path}/bank-account-admin`} component={BankAccountAdminPage} />
            <Route path={`${path}/bank-account`} component={BankAccountDetailPage} />
            <Route path={`${path}/bank-account/:id`} component={BankAccountDetailPage} /> 
        </Switch>
    )
}

export default BankAccountRoutes;