
export const getValidationDestinationOutputsDetail = () => {
    return {
        form: {
            accountingAccount: [ {required: true, message: 'Cta. Contable es requerida' } ],
            moduleId: [ {required: true, message: 'Modulo es requerido' } ],
            coinId: [ {required: true, message: 'Moneda es requerida' } ],
            operationTypeId: [ {required: true, message: 'Tipo Operacion es requerido' } ],
            movementTypeId: [ {required: true, message: 'Tipo Movimiento es requerido' } ],
            bankId: [ {required: true, message: 'banco es requerido' } ],
            bankAccountId: [ {required: true, message: 'Cta. Banco es requerido' } ],
        },
        grid: {

        },
        button: {

        },
        bar: {

        }
    }
}
