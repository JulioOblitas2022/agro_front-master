import React from 'react'
import BarButtonFw from '../../../../framework/bar-button/BarButtonFw';
import FormFw from '../../../../framework/form/FormFw';
import { BasePage } from '../../../../framework/pages/BasePage';
import { getConfigDestinationOutputsDetail } from './getConfigDestinationOutputsDetail';
import { getModeDestinationOutputsDetail } from './getModeDestinationOutputsDetail';
import { getValidationDestinationOutputsDetail } from './getValidationDestinationOutputsDetail';

export class DestinationOutputsDetailPage extends BasePage {
    
    createPageProperties = (pageApi) => {
        return {
            title: 'REGISTRO DE CONCEPTOS DESTINO - EGRESOS',
            validation: getValidationDestinationOutputsDetail,
            mode: getModeDestinationOutputsDetail
        };
    }
    
    renderPage(pageApi) {

        const config = getConfigDestinationOutputsDetail(pageApi);

        return (
            <>
                <FormFw
                    name="form"
                    getParentApi={pageApi.getApi}
                    config={config.form}
                />
                <BarButtonFw
                    name="bar"
                    getParentApi={pageApi.getApi}
                    config={config.bar}
                />
            </>
        )
    }
}