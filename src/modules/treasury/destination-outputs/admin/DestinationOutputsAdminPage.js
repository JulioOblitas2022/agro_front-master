import React from 'react'
import BarButtonFw, { BUTTON_DEFAULT } from '../../../../framework/bar-button/BarButtonFw';
import FormFw from '../../../../framework/form/FormFw';
import GridFw from '../../../../framework/grid/GridFw';
import { BasePage } from '../../../../framework/pages/BasePage';
import { COLUMN_DEFAULT } from "../../../../framework/grid/GridFw";
import { getFormDefaultParams } from "../../../shared/util/getFormDefaultParams";
import { getGridServerDefaultParams } from "../../../shared/util/getGridServerDefaultParams";

export class DestinationOutputsAdminPage extends BasePage {
    createPagePropertiesBase(pageApi) {
        return {
            title: 'BANDEJA DE CONCEPTOS DESTINO - EGRESOS',
            barHeader: {
                fields:[
                    {...BUTTON_DEFAULT.NEW, label:'Nuevo', link: '/treasury/destination-outputs/destination-outputs/new'}
                ]
            }
        };
    }

    renderPage(pageApi) {
        const getGrid = () => pageApi.getComponent('grid');
        const getForm = () => pageApi.getComponent('form');

        const form = {
            ...getFormDefaultParams(),
            title: 'FILTROS DE BUSQUEDA',
            fields:[
                { type: 'select', name: 'companyId', label: 'Empresa', column: 4, load: { queryId: 554, }, search: true },
                { name:'account', label:'Cuenta', column: 4},
                { type: 'select', name:'moduleId', label:'Modulo', column: 4, load:{ queryId: 409 }, search: true},
            ]
        }

        const bar = { fields: [
            {...BUTTON_DEFAULT.SEARCH, onClick: ()=>getGrid().load({params:getForm().getData()})},
            {...BUTTON_DEFAULT.CLEAR, onClick: ()=>getForm().reset()}
        ]}

        const grid = {
            ...getGridServerDefaultParams({queryLoad: 421, params: {type: 2}}),
            title: 'RESULTADOS',
            fields:[
                { name: 'accountNumber', label: 'Nro. Cuenta',},
                { name: 'account', label: 'Cuenta',},
                { name: 'module', label: 'Módulo',},
                { name: 'coin', label: 'Moneda',},
                { name: 'detailText', label: 'Detalla',},
                { name: 'company', label: 'Empresa',},
                {...COLUMN_DEFAULT.VIEW, link: (value, row, index) => `/treasury/destination-inputs/destination-inputs/${row.destinyId}`, label: 'Opciones', colSpan: 2 },
                {...COLUMN_DEFAULT.EDIT, link: (value, row, index) => `/treasury/destination-inputs/destination-inputs/${row.destinyId}/update`, colSpan: 0 },
                // {...COLUMN_DEFAULT.DELETE, process: {fnOk: (result)=>getGrid().load({params:getForm().getData()}),filter: (value, row, index) => ({queryId: 419, params: {destinationId: row.destinationId}}) }}
            ]
        }

        return (
            <>
                <FormFw
                    name="form"
                    getParentApi={pageApi.getApi}
                    config={form}
                />
                <BarButtonFw
                    name="bar"
                    getParentApi={pageApi.getApi}
                    config={bar}
                />
                <GridFw
                    name="grid"
                    getParentApi={pageApi.getApi}
                    config={grid}
                />
            </>
        )
    }
}