import React from 'react'
import { Route, Switch, useRouteMatch } from 'react-router';
import { DestinationOutputsAdminPage } from './admin/DestinationOutputsAdminPage';
import { DestinationOutputsDetailPage } from './detail/DestinationOutputsDetailPage';

const DestinationOutputsRoutes = () => {
    const { path } = useRouteMatch();
    return (
        <Switch>
            <Route exact path={`${path}/destination-outputs-admin`} component={DestinationOutputsAdminPage} />
            <Route path={`${path}/destination-outputs`} component={DestinationOutputsDetailPage} />
            <Route path={`${path}/destination-outputs/:id`} component={DestinationOutputsDetailPage} /> 
        </Switch>
    )
}

export default DestinationOutputsRoutes;
