import React from 'react'
import { Route, Switch, useRouteMatch } from 'react-router';
import { ExchangDocsAdminPage } from './admin/ExchangDocsAdminPage';
import { ExchangDocsDetailPage } from './detail/ExchangDocsDetailPage';

const ExchangDocsRoutes = () => {
    const { path } = useRouteMatch();
    return (
        <Switch>
            <Route exact path={`${path}/exchange-docs-admin`} component={ExchangDocsAdminPage} />
            <Route path={`${path}/exchange-docs`} component={ExchangDocsDetailPage} />
            <Route path={`${path}/exchange-docs/:id`} component={ExchangDocsDetailPage} /> 
        </Switch>
    )
}

export default ExchangDocsRoutes;
