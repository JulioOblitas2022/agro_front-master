import React from 'react';
import { DATE_FORMAT } from '../../../../../constants/constants';
import FormGridFw, { FORM_GRID_MODE } from '../../../../../framework/form-grid/FormGridFw';
import { SELECTION_TYPE } from '../../../../../framework/grid/GridFw';
import { BasePage } from '../../../../../framework/pages/BasePage';

export class DocumentProviderModal extends BasePage {
    
    
    createPageProperties = (pageApi) => {
        return {
            title: 'Busqueda de Documentos',
        };
    }
    
    renderPage(pageApi) {
        const formGridConfig = {
            mode: FORM_GRID_MODE.FORM_SEARCH_ADD,
            afterAddEvent: (data) => {
                pageApi.getEvent('afterAddEvent')(data);
                pageApi.hideThisModal();
            },
            form: {
                fields:[
                    {name:'documentNumber', label:'Documento', column: 6},
                ]
            },
            grid: {
                rowId: 'purchaseOrderId',
                showIndex: false,
                autoLoad: true,
                pagination: false,
                selection: {type: SELECTION_TYPE.SINGLE},
                load: {
                    queryId: 599,
                    params: { providerId: pageApi.name}
                },
                fields:[
                    {name: 'provider', label: 'Proveedor'},
                    {name: 'documentType', label:'Tipo Documento'},
                    {name: 'documentTypeNumber', label:'Nro. Documento'},
                    {name: 'coin', label: 'Moneda'},
                    {type: 'date', name:'broadcastDate', label:'Fecha Emisión', format: DATE_FORMAT.DDMMYYYY},
                    {type: 'date', name:'expirationDate', label:'Fecha Vencimiento', format: DATE_FORMAT.DDMMYYYY},
                    {type: 'decimal', name:'totalAmount', label:'Importe'},
                    {type: 'decimal', name:'balance', label:'Saldo'}
                ]
            }
        };  
    
        return (
            <>
                <FormGridFw 
                    name="documentProviderGrid" 
                    getParentApi={pageApi.getApi}
                    config={formGridConfig}
                />
            </>
        )

    }
    
}