import React from 'react'
import BarButtonFw from '../../../../framework/bar-button/BarButtonFw';
import FormFw from '../../../../framework/form/FormFw';
import { BasePage } from '../../../../framework/pages/BasePage';
import { getConfigExchangDocsDetail } from './getConfigExchangDocsDetail';
import { getModeExchangDocsDetail } from './getModeExchangDocsDetail';
import { getValidationExchangDocsDetail } from './getValidationExchangDocsDetail';
import { DocsCustomer } from './sections/DocsCustomer';
import { DocsProvider } from './sections/DocsProvider';

export class ExchangDocsDetailPage extends BasePage {
    createPageProperties = (pageApi) => {
        return {
            title: 'REGISTRO DE CANJE DE DOCUMENTOS',
            validation: getValidationExchangDocsDetail,
            mode: getModeExchangDocsDetail
        };
    }
    
    renderPage(pageApi) {

        const config = getConfigExchangDocsDetail(pageApi);

        return (
            <>
                <FormFw
                    name="form"
                    getParentApi={pageApi.getApi}
                    config={config.form}
                />
                
                {DocsProvider(pageApi)}

                {DocsCustomer(pageApi)}
                
                <BarButtonFw
                    name="bar"
                    getParentApi={pageApi.getApi}
                    config={config.bar}
                />
            </>
        )
    }
}
