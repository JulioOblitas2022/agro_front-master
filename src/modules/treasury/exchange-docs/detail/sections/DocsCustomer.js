import React from 'react'
import { DATE_FORMAT } from '../../../../../constants/constants';
import { BUTTON_DEFAULT } from '../../../../../framework/bar-button/BarButtonFw';
import FormFw from '../../../../../framework/form/FormFw';
import GridFw, { COLUMN_DEFAULT } from '../../../../../framework/grid/GridFw';
import { DocumentCustomerModal } from '../modals/DocumentCustomerModal';
export const DocsCustomer = (pageApi) => {

    const fnDelete = (value, row, index) => {
        getGrid().deleteItem(row.key);      
    }
    
    const getFormPrincipal = () => pageApi.getComponent('form');
    const getGrid = () => pageApi.getComponent('customerGridSection');
    const getForm = () => pageApi.getComponent('customerFormSection');

    const customerConfig = {
        events: {
            afterAddEvent : (data) => {
                getGrid().addItem(data);
            }
        }
    }

    const DocumentCustomerModalConfig = (customerId) => {
        return {
            width: 1600,
            maskClosable: false,
            component: <DocumentCustomerModal name={customerId} getParentApi={pageApi.getApi} config={customerConfig} />
        }
    };

    const barHeaderConfig = {
        fields:[
            {...BUTTON_DEFAULT.ADD, label:'Agregar Doc. Cliente', onClick: ()=>{  pageApi.showModal(DocumentCustomerModalConfig(getFormPrincipal().getData().customerId)); }}
        ]
    }

    const calculateTotal = () => {
        let items = getGrid().getData();
        if (items){
            let total_s = 0;
            let total_d = 0;
            items.forEach(item => {
                total_s = total_s + parseFloat(item.amount);
                total_d = total_d + parseFloat(item.balance);
                
            });

            getForm()?.setFieldData('totalAmount',  isNaN((parseFloat(total_s)).toFixed(2),0));
            getForm()?.setFieldData('totalBalance',isNaN((parseFloat(total_d)).toFixed(2),0));
        }
    }
    const gridConfig = {
        title: 'Documentos del Cliente',
        rowId: 'exchangeDetailId',
        showIndex: true,
        pagination: true,
        autoLoad: true,
        collapsible: true,
        load: {
            validation: () => pageApi.getParamId()!=null, //solo se dispara si el valor es distinto de nulo
            queryId: 604,
            orderBy: 'exchangeDetailId desc',
            params: {exchangeId: pageApi.getParamId()},
            fnOk: (resp) => {
                calculateTotal();
            },
            hideMessage: true,
            hideMessageError: true,
        },
        save: {
            queryId: 602,
            params: {exchangeId: pageApi.getParamId()},
            fnOk: (resp) => {
                calculateTotal();
            },
            hideMessage: true,
            hideMessageError: true,
        },
        addItem: {
            fnAfter: (row) => {
                calculateTotal();
            }
        },
        updateItem: {
            fnAfter: (row) => {
                calculateTotal();
            }
        },
        fields:[
            { name:'documentType', label:'Tip. Documento' },
            { name:'documentNumber', label:'N° Documento' },
            { type: 'date', name:'registerDate', label:'Fecha Emisión', format: DATE_FORMAT.DDMMYYYY},
            { type: 'date', name:'expirationDate', label:'Fecha Vencimiento', format: DATE_FORMAT.DDMMYYYY},
            { name:'coin', label:'Moneda' },
            { name:'amount', label:'Importe' },
            { name:'balance', label:'Saldo' },
            {...COLUMN_DEFAULT.DELETE, onClick: fnDelete}
        ],
        barHeader: barHeaderConfig
    };

    const form_sumas = {
        fields:[
            { type: 'decimal', name:'totalAmount', label: 'Total Importe', column: 4, readOnly: true},
            { type: 'decimal', name:'totalBalance', label: 'Total Saldo', column: 4, readOnly: true}
        ],
    }

    return (
        <>
            <GridFw
                name="customerGridSection"
                getParentApi={pageApi.getApi}
                config={gridConfig} 
            />
            <FormFw
                name="customerFormSection"
                getParentApi={pageApi.getApi}
                config={form_sumas}
            />
        </>
    )
}