
export const getValidationOutputsDetail = () => {
    return {
        form: {
            registerDate: [ {required: true, message: 'Fecha es requerido' } ],
            coinId: [ {required: true, message: 'Moneda es requerida' } ],
        },
        grid: {

        },
        button: {

        },
        bar: {

        }
    }
}
