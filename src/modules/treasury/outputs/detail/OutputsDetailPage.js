import React from 'react'
import BarButtonFw from '../../../../framework/bar-button/BarButtonFw';
import FormFw from '../../../../framework/form/FormFw';
import { BasePage } from '../../../../framework/pages/BasePage';
import { getConfigOutputsDetail } from './getConfigOutputsDetail';
import { getModeOutputsDetail } from './getModeOutputsDetail';
import { getValidationOutputsDetail } from './getValidationOutputsDetail';
import { createDestinationOutputSection } from './sections/destinationOutputSection';
import { createSourceOutputSection } from './sections/sourceOutputSection';

export class OutputsDetailPage extends BasePage {
    
    createPageProperties = (pageApi) => {
        return {
            title: 'REGISTRO DE EGRESOS',
            validation: getValidationOutputsDetail,
            mode: getModeOutputsDetail
        };
    }
    
    renderPage(pageApi) {

        const config = getConfigOutputsDetail(pageApi);

        return (
            <>
                <FormFw
                    name="form"
                    getParentApi={pageApi.getApi}
                    config={config.form}
                />

                {createSourceOutputSection(pageApi)}
                
                {createDestinationOutputSection(pageApi)}

                <BarButtonFw
                    name="bar"
                    getParentApi={pageApi.getApi}
                    config={config.bar}
                />
            </>
        )
    }
}