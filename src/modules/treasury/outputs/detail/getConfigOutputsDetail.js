import { DATE_FORMAT, PAGE_MODE } from "../../../../constants/constants";
import { BUTTON_DEFAULT } from "../../../../framework/bar-button/BarButtonFw";
import { UserUtil } from "../../../../util/UserUtil";

export const getConfigOutputsDetail = (pageApi) => {
    
    const getForm = () => pageApi.getComponent('form');
    const getGridDestinationOutput = () => pageApi.getComponent('DestinationOutputSection');
    const getGridSourceOutput = () => pageApi.getComponent('SourceOutputSection');

    return {
        form: {
            autoLoad: true,
            load:{
                queryId: 431,
                params: {treasuryId: pageApi.getParamId() || 0, type: 2},
                fnOk: (resp) => {
                    getGridDestinationOutput().load({params:{treasuryId: pageApi.getParamId()}});
                    getGridSourceOutput().load({params:{treasuryId: pageApi.getParamId()}});
                }
            },
            update:{
                queryId: 430,
                postLink: (resp, values) => '/treasury/outputs/outputs-admin',
                params: {modifiedBy: UserUtil.getUserId()},
                fnOk: (resp) => {
                    getGridDestinationOutput().save({params:{treasuryId: pageApi.getParamId()}});
                    getGridSourceOutput().save({params:{treasuryId: pageApi.getParamId()}});
                }
            },
            save:{
                queryId: 429,
                postLink: (resp, values) => '/treasury/outputs/outputs-admin',
                params: {createdBy: UserUtil.getUserId(), type: 2},
                fnOk: (resp) => {
                    let treasuryId = resp.dataObject.treasuryId;
                    if(treasuryId){
                        getGridDestinationOutput().save({params:{treasuryId}});
                        getGridSourceOutput().save({params:{treasuryId}});
                    }
                    
                }
            },
            fields:[
                { type: 'select', name: 'companyId', label: 'Empresa', column: 4, load: { queryId: 554, }, search: true },
                { type: 'date', name: 'registerDate', label: 'Fec. Emisión', inputFormat: DATE_FORMAT.YYYYMMDD, format: DATE_FORMAT.DDMMYYYY, column: 4},
                { type: 'select', name:'coinId', label:'Moneda', column: 4, load:{ queryId: 187 }, search: true},
                { type: 'decimal', name:'exchangeType', label:'Tipo de Cambio', column: 4, readOnly: true},
                { name: 'seatNumber', label:'Asiento', column: 4, readOnly: true},
                { type: 'textarea', name:'glosa', label:'Observaciones', column: 12},
            ],
            onComponentLoad : (formApi, pageApi) =>{
                if (pageApi.getPageMode() !== PAGE_MODE.NEW)
                    formApi.load();
            }
        },
        bar: {
            fields: [
                {...BUTTON_DEFAULT.SAVE, onClick: () => getForm().save()},
                {...BUTTON_DEFAULT.UPDATE, onClick: () => getForm().update()},
                {...BUTTON_DEFAULT.RETURN, link: '/treasury/outputs/outputs-admin'}
            ],
        },
        grid: {}
    }
}