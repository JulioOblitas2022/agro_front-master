import React from 'react'
import { BUTTON_DEFAULT } from '../../../../../framework/bar-button/BarButtonFw';
import FormFw from '../../../../../framework/form/FormFw';
import GridFw, { COLUMN_DEFAULT } from '../../../../../framework/grid/GridFw';
import { SourceOutputModal } from '../modals/SourceOutputModal';

export const createSourceOutputSection = (pageApi) => {

    const fnDelete = (value, row, index) => {
        getGrid().deleteItem(row.key);      
    }
    
    const getGrid = () => pageApi.getComponent('SourceOutputSection');
    const getForm = () => pageApi.getComponent('formSumas');

    const sourceOutputConfig = {
        events: {
            afterAddEvent : (data) => {
                getGrid().addItem(data);
            }
        }
    }

    const SourceOutputModalConfig = {
        width: 1600,
        maskClosable: false,
        component: <SourceOutputModal name="sourceOutputModal" getParentApi={pageApi.getApi} config={sourceOutputConfig} />
    };

    const barHeaderConfig = {
        fields:[
            {...BUTTON_DEFAULT.ADD, label:'Agregar Origen - Egreso', onClick: ()=>{  pageApi.showModal(SourceOutputModalConfig); }}
        ]
    }

    const calculateTotal = () => {
        let items = getGrid().getData();
        if (items){
            let total_s = 0;
            let total_d = 0;
            items.forEach(item => {
                total_s = total_s + parseFloat(item.ammountSoles);
                total_d = total_d + parseFloat(item.ammountDolares);
                
            });

            getForm()?.setFieldData('total_soles',  isNaN((parseFloat(total_s)).toFixed(2),0));
            getForm()?.setFieldData('total_dolares',isNaN((parseFloat(total_d)).toFixed(2),0));
        }
    }
    const gridConfig = {
        title: 'Origen - Egresos',
        rowId: 'sourceOutputDetailId',
        showIndex: true,
        pagination: true,
        autoLoad: true,
        collapsible: true,
        load: {
            validation: () => pageApi.getParamId()!=null, //solo se dispara si el valor es distinto de nulo
            queryId: 544,
            orderBy: 'sourceOutputDetailId desc',
            params: {treasuryId: pageApi.getParamId()},
            fnOk: (resp) => {
                calculateTotal();
            }
        },
        save: {
            queryId: 543,
            params: {treasuryId: pageApi.getParamId()},
            fnOk: (resp) => {
                calculateTotal();
            }
        },
        addItem: {
            fnAfter: (row) => {
                calculateTotal();
            }
        },
        updateItem: {
            fnAfter: (row) => {
                calculateTotal();
            }
        },
        scroll: {
            x: 1300 
        },
        
        fields:[
            { name:'account', label:'Concepto', column: 4,  },
            { type: 'decimal', name:'changeType', label:'Tipo de Cambio', column: 4,  editable: true, 
                onChange: (value, values) => {
                    let changeType = parseFloat(values['changeType']);
                    //let ammountSoles =  parseFloat(values['ammountSoles']);

                    getGrid()?.getFormEditable()?.setFieldData('ammountDolares', parseFloat(changeType).toFixed(2));
                }
            },
            { type: 'decimal', type: 'number', name: 'ammountSoles', label: 'Importe Soles', editable: true, value: 1, 
                onChange: (value, values) => {
                    let changeType = parseFloat(values['changeType']);
                    let ammountSoles =  parseFloat(values['ammountSoles']);

                    getGrid()?.getFormEditable()?.setFieldData('ammountDolares', parseFloat(ammountSoles / changeType).toFixed(2));
                }
            },
            { type: 'decimal', name: 'ammountDolares', label:'Importe Dólares',  editable: true, 
                onChange: (value, values) => {
                    let changeType = parseFloat(values['changeType']);
                    let ammountDolares =  parseFloat(values['ammountDolares']);

                    getGrid()?.getFormEditable()?.setFieldData('ammountSoles', parseFloat(ammountDolares * changeType).toFixed(2));
                }
            },
            {...COLUMN_DEFAULT.DELETE, onClick: fnDelete}
        ],
        barHeader: barHeaderConfig
    };

    const form_sumas = {
        fields:[
            { type: 'decimal', name:'total_soles', label: 'Total Soles',column: 4, readOnly: true},
            { type: 'decimal', name:'total_dolares', label: 'Total Dólares',column: 4, readOnly: true}
        ],
    }

    return (
        <>
            <GridFw
                name="SourceOutputSection"
                getParentApi={pageApi.getApi}
                config={gridConfig} 
            />
            <FormFw
                name="formSumas"
                getParentApi={pageApi.getApi}
                config={form_sumas}
            />
        </>
    )
}