import React from 'react'
import { BUTTON_DEFAULT } from '../../../../../framework/bar-button/BarButtonFw';
import FormFw from '../../../../../framework/form/FormFw';
import GridFw, { COLUMN_DEFAULT } from '../../../../../framework/grid/GridFw';
import { DestinationOutputModal } from '../modals/DestinationOutputModal';

export const createDestinationOutputSection = (pageApi) => {

    const fnDelete = (value, row, index) => {
        getGrid().deleteItem(row.key);      
    }
    
    const getGrid = () => pageApi.getComponent('DestinationOutputSection');
    const getForm = () => pageApi.getComponent('formSumasDestination');

    const destinationOutputConfig = {
        events: {
            afterAddEvent : (data) => {
                getGrid().addItem(data);
            }
        }
    }

    const DestinationOutputModalConfig = {
        width: 1600,
        maskClosable: false,
        component: <DestinationOutputModal name="destinationOutputModal" getParentApi={pageApi.getApi} config={destinationOutputConfig} />
    };

    const barHeaderConfig = {
        fields:[
            {...BUTTON_DEFAULT.ADD, label:'Agregar Destino - Egresos', onClick: ()=>{  pageApi.showModal(DestinationOutputModalConfig); }}
        ]
    }

    const calculateTotal = () => {
        let items = getGrid().getData();
        if (items){
            let total_s = 0;
            let total_d = 0;
            items.forEach(item => {
                total_s = total_s + parseFloat(item.ammountSoles);
                total_d = total_d + parseFloat(item.ammountDolares);
                
            });

            getForm()?.setFieldData('total_soles',  isNaN((parseFloat(total_s)).toFixed(2),0));
            getForm()?.setFieldData('total_dolares',isNaN((parseFloat(total_d)).toFixed(2),0));
        }
    }
    const gridConfig = {
        title: 'Destino - Egresos',
        rowId: 'destinationOutputDetailId',
        showIndex: true,
        pagination: true,
        autoLoad: true,
        collapsible: true,
        load: {
            validation: () => pageApi.getParamId()!=null, //solo se dispara si el valor es distinto de nulo
            queryId: 542,
            orderBy: 'destinationOutputDetailId desc',
            params: {treasuryId: pageApi.getParamId()},
            fnOk: (resp) => {
                calculateTotal();
            }
        },
        save: {
            queryId: 541,
            params: {treasuryId: pageApi.getParamId()},
            fnOk: (resp) => {
                calculateTotal();
            }
        },
        addItem: {
            fnAfter: (row) => {
                calculateTotal();
            }
        },
        updateItem: {
            fnAfter: (row) => {
                calculateTotal();
            }
        },
        scroll: {
            x: 1300 
        },
        
        fields:[
            { name:'account', label:'Concepto', column: 4, width: 100, column: 12, },
            { type: 'decimal', name:'changeType', label:'Tipo de Cambio', column: 4,  width: 100, column: 12, 
                onChange: (value, values) => {
                    let changeType = parseFloat(values['changeType']);
                    //let ammountSoles =  parseFloat(values['ammountSoles']) = '' ? 1 : parseFloat(values['ammountSoles']);

                    getGrid()?.getFormEditable()?.setFieldData('ammountDolares', parseFloat(changeType).toFixed(2));
                },  editable: true
            },
            { type: 'decimal', type: 'number', name: 'ammountSoles', label: 'Importe Soles',  width: 100, column: 12, editable: true, value: 1, 
                onChange: (value, values) => {
                    let changeType = parseFloat(values['changeType']);
                    let ammountSoles =  parseFloat(values['ammountSoles']);

                    getGrid()?.getFormEditable()?.setFieldData('ammountDolares', parseFloat(ammountSoles / changeType).toFixed(2));
                }
            },
            { type: 'decimal', name: 'ammountDolares', label:'Importe Dólares',  width: 100, column: 12, 
                onChange: (value, values) => {
                    let changeType = parseFloat(values['changeType']);
                    let ammountDolares =  parseFloat(values['ammountDolares']);

                    getGrid()?.getFormEditable()?.setFieldData('ammountSoles', parseFloat(ammountDolares * changeType).toFixed(2));
                },  editable: true
            }, 
            {...COLUMN_DEFAULT.DELETE, onClick: fnDelete}
        ],
        barHeader: barHeaderConfig
    };

    const form_sumas = {
        fields:[
            {type: 'decimal', name:'total_soles', label: 'Total Soles',column: 4, readOnly: true},
            {type: 'decimal', name:'total_dolares', label: 'Total Dólares',column: 4, readOnly: true}
        ],
    }

    return (
        <>
            <GridFw
                name="DestinationOutputSection"
                getParentApi={pageApi.getApi}
                config={gridConfig} 
            />
            <FormFw
                name="formSumasDestination"
                getParentApi={pageApi.getApi}
                config={form_sumas}
            />
        </>
    )
}