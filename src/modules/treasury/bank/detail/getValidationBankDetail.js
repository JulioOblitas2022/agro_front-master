export const getValidationBankDetail = () => {
    return {
        form: {
            description: [ {required: true, message: 'Descripción es requerido' } ],
            sunatCode: [ {required: true, message: 'Código Sunat es requerido' } ],
            abbreviature: [ {required: true, message: 'Abreviatura es requerido' } ],
        },
        grid: {

        },
        button: {

        },
        bar: {

        }
    }
}
