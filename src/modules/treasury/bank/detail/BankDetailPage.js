import React from 'react'
import BarButtonFw from '../../../../framework/bar-button/BarButtonFw';
import FormFw from '../../../../framework/form/FormFw';
import { BasePage } from '../../../../framework/pages/BasePage';
import { getConfigBankDetail } from './getConfigBankDetail';
import { getModeBankDetail } from './getModeBankDetail';
import { getValidationBankDetail } from './getValidationBankDetail';

export class BankDetailPage extends BasePage {
    
    createPageProperties = (pageApi) => {
        return {
            title: 'DETALLE DE BANCO',
            validation: getValidationBankDetail,
            mode: getModeBankDetail
        };
    }

    renderPage(pageApi) {

        const config = getConfigBankDetail(pageApi);

        return (
            <>
                <FormFw
                    name="form"
                    getParentApi={pageApi.getApi}
                    config={config.form}
                />
                <BarButtonFw
                    name="bar"
                    getParentApi={pageApi.getApi}
                    config={config.bar}
                />
            </>
        )
    }
}