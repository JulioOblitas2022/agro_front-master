import { PAGE_MODE } from '../../../../constants/constants';
import { BUTTON_DEFAULT } from '../../../../framework/bar-button/BarButtonFw';

export const getConfigBankDetail = (pageApi) => {
    const getForm = () => pageApi.getComponent('form');

    return {
        form: {
            load:{
                queryId: 116,
                params: {bankId: pageApi.getParamId()},
            },
            update:{
                queryId: 114,
                postLink: (resp, values) => '/treasury/bank/bank-admin'
            },
            save:{
                queryId: 112,
                postLink: (resp, values) => '/treasury/bank/bank-admin'
            },
            title: 'REGITRO DE APROBADORES',
            fields:[
                {type: 'divider', label: 'DATOS PRINCIPALES'},
                {name:'description', label:'Descripción', column: 4},
                {name:'sunatCode', label:'Código Sunat', column: 4},
                {name:'ruc', label:'RUC', column: 4},
                {name:'abbreviature', label:'Abreviatura', column: 4},
            ],
            onComponentLoad : (formApi, pageApi) =>{
                if (pageApi.getPageMode() !== PAGE_MODE.NEW)
                    formApi.load();
            }
        },
        bar: {
            fields: [
                {...BUTTON_DEFAULT.SAVE, onClick: () => getForm().save()},
                {...BUTTON_DEFAULT.UPDATE, onClick: () => getForm().update()},
                {...BUTTON_DEFAULT.RETURN, link: '/treasury/bank/bank-admin'}
            ],
        },
        grid: {}
    }
}
