import React from 'react';
import { Route, Switch, useRouteMatch } from 'react-router-dom';

import { BankAdminPage } from './admin/BankAdminPage';
import { BankDetailPage } from './detail/BankDetailPage';

const BankRoutes = () => {
    const { path } = useRouteMatch();
    return (
        <Switch>
            <Route exact path={`${path}/bank-admin`} component={BankAdminPage} />
            <Route path={`${path}/bank`} component={BankDetailPage} />
            <Route path={`${path}/bank/:id`} component={BankDetailPage} /> 
        </Switch>
    )
}

export default BankRoutes;