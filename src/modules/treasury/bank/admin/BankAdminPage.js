import React from 'react'
import BarButtonFw, { BUTTON_DEFAULT } from '../../../../framework/bar-button/BarButtonFw';
import FormFw from '../../../../framework/form/FormFw';
import GridFw from '../../../../framework/grid/GridFw';
import { BasePage } from '../../../../framework/pages/BasePage';
import { COLUMN_DEFAULT } from '../../../../framework/grid/GridFw';
import { getFormDefaultParams } from '../../../shared/util/getFormDefaultParams';
import { getGridServerDefaultParams } from '../../../shared/util/getGridServerDefaultParams';

export class BankAdminPage extends BasePage {

    createPagePropertiesBase(pageApi) {
        return { 
            title: 'BANCOS',
            barHeader:{
                fields:[
                    {...BUTTON_DEFAULT.NEW, label:'Nuevo', link: '/treasury/bank/bank/new'}
                ]
            }
        };
    }

    renderPage(pageApi) {
        const getGrid = () => pageApi.getComponent('grid');
        const getForm = () => pageApi.getComponent('form');
        
        const form = {
            ...getFormDefaultParams(),
            title: 'BANDEJA DE BANCOS',
            fields:[
                {type: 'divider', label: 'Filtros de busqueda'},
                {name:'description', label:'Descripción', column: 4},
                {name:'sunatCode', label:'Código Sunat', column: 4},
                {name:'ruc', label:'RUC.', column: 4, maxLength: 11},
            ]
        }

        const bar = { fields: [
            {...BUTTON_DEFAULT.SEARCH, onClick: ()=>getGrid().load({params:getForm().getData()})},
            {...BUTTON_DEFAULT.CLEAR, onClick: ()=>getForm().reset()}
        ]}

        const grid = {
            ...getGridServerDefaultParams({queryLoad: 111}),
            title: 'LISTA DE BANCOS',
            fields:[
                {name: 'sunatCode', label: 'Código Sunat',},
                {name: 'description', label: 'Descripción',},
                {name: 'ruc', label: 'RUC',},
                {...COLUMN_DEFAULT.VIEW, link: (value, row, index) => `/treasury/bank/bank/id-${row.bankId}`, label: 'Opciones', colSpan: 3 },
                {...COLUMN_DEFAULT.EDIT, link: (value, row, index) => `/treasury/bank/bank/id-${row.bankId}/update`, colSpan: 0 },
                {...COLUMN_DEFAULT.DELETE, colSpan: 0, process: {fnOk: (result)=>getGrid().load({params:getForm().getData()}),filter: (value, row, index) => ({queryId: 115, params: {bankId: row.bankId}}) }}
            ]
        }

        return (
            <>
                <FormFw
                    name="form"
                    getParentApi={pageApi.getApi}
                    config={form}
                />
                <BarButtonFw
                    name="bar"
                    getParentApi={pageApi.getApi}
                    config={bar}
                />
                <GridFw
                    name="grid"
                    getParentApi={pageApi.getApi}
                    config={grid}
                />
            </>
        )
    }
}
