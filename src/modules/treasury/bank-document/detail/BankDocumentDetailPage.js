import React from 'react'
import BarButtonFw from '../../../../framework/bar-button/BarButtonFw';
import FormFw from '../../../../framework/form/FormFw';
import { BasePage } from '../../../../framework/pages/BasePage';
import { getConfigBankDocumentDetail } from './getConfigBankDocumentDetail';
import { getModeBankDocumentDetail } from './getModeBankDocumentDetail';
import { getValidationBankDocumentDetail } from './getValidationBankDocumentDetail';

export class BankDocumentDetailPage extends BasePage {
    
    createPageProperties = (pageApi) => {
        return {
            title: 'DETALLE DE DOCUMENTOS DE BANCO',
            validation: getValidationBankDocumentDetail,
            mode: getModeBankDocumentDetail
        };
    }

    renderPage(pageApi) {

        const config = getConfigBankDocumentDetail(pageApi);

        return (
            <>
                <FormFw
                    name="form"
                    getParentApi={pageApi.getApi}
                    config={config.form}
                />
                <BarButtonFw
                    name="bar"
                    getParentApi={pageApi.getApi}
                    config={config.bar}
                />
            </>
        )
    }
}