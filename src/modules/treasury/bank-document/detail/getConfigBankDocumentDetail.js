import { PAGE_MODE } from '../../../../constants/constants';
import { BUTTON_DEFAULT } from '../../../../framework/bar-button/BarButtonFw';

export const getConfigBankDocumentDetail = (pageApi) => {
    const getForm = () => pageApi.getComponent('form');
    return {
        form: {
            load:{
                queryId: 136,
                params: {bankDocumentId: pageApi.getParamId()},
            },
            update:{
                queryId: 131,
                postLink: (resp, values) => '/treasury/bank-document/bank-document-admin'
            },
            save:{
                queryId: 129,
                postLink: (resp, values) => '/treasury/bank-document/bank-document-admin'
            },
            title: 'REGITRO DE DOCUMENTOS DE BANCO',
            fields:[
                {type: 'divider', label: 'DATOS PRINCIPALES'},
                {name:'description', label:'Descripción', column: 4},
                {name:'abbreviature', label:'Abreviatura', column: 4},
                {type: 'select', name:'documentTypeId', label:'Tipo Documento', column: 4, load:{ queryId: 195 } }  
            ],
            onComponentLoad : (formApi, pageApi) =>{
                if (pageApi.getPageMode() !== PAGE_MODE.NEW)
                    formApi.load();
            }
        },
        bar: {
            fields: [
                {...BUTTON_DEFAULT.SAVE, onClick: () => getForm().save()},
                {...BUTTON_DEFAULT.UPDATE, onClick: () => getForm().update()},
                {...BUTTON_DEFAULT.RETURN, link: '/treasury/bank-document/bank-document-admin'}
            ],
        },
        grid: {}
    }
}
