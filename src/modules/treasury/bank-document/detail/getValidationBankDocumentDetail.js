export const getValidationBankDocumentDetail = () => {
    return {
        form: {
            description: [ {required: true, message: 'Descripción es requerido' } ],
            abbreviature: [ {required: true, message: 'Abreviatura es requerido' } ],
            documentTypeId: [ {required: true, message: 'Tipo Documento' } ]
        },
        grid: {

        },
        button: {

        },
        bar: {

        }
    }
}
