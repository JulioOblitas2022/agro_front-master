import React from 'react';
import { Route, Switch, useRouteMatch } from 'react-router-dom';

import { BankDocumentAdminPage } from './admin/BankDocumentAdminPage';
import { BankDocumentDetailPage } from './detail/BankDocumentDetailPage';

const BankDocumentRoutes = () => {
    const { path } = useRouteMatch();
    return (
        <Switch>
            <Route exact path={`${path}/bank-document-admin`} component={BankDocumentAdminPage} />
            <Route path={`${path}/bank-document`} component={BankDocumentDetailPage} />
            <Route path={`${path}/bank-document/:id`} component={BankDocumentDetailPage} /> 
        </Switch>
    )
}

export default BankDocumentRoutes;