import React from 'react'
import BarButtonFw, { BUTTON_DEFAULT } from '../../../../framework/bar-button/BarButtonFw';
import FormFw from '../../../../framework/form/FormFw';
import GridFw from '../../../../framework/grid/GridFw';
import { BasePage } from '../../../../framework/pages/BasePage';
import { COLUMN_DEFAULT } from '../../../../framework/grid/GridFw';
import { getFormDefaultParams } from '../../../shared/util/getFormDefaultParams';
import { getGridServerDefaultParams } from '../../../shared/util/getGridServerDefaultParams';

export class BankDocumentAdminPage extends BasePage {
    createPagePropertiesBase(pageApi) {
        return { 
            title: 'DOCUMENTOS DE BANCO',
            barHeader:{
                fields:[
                    {...BUTTON_DEFAULT.NEW, label:'Nuevo', link: '/treasury/bank-document/bank-document/new'}
                ]
            }
         };
    }

    renderPage(pageApi) {
        const getGrid = () => pageApi.getComponent('grid');
        const getForm = () => pageApi.getComponent('form');
        
        const form = {
            ...getFormDefaultParams(),
            title: 'BANDEJA DE DOCUMENTOS DE BANCOS',
            fields:[
                {type: 'divider', label: 'Filtros de busqueda'},
                {name:'description', label:'Descripción', column: 4},
                {name:'abbreviature', label:'Abreviatura', column: 4},
                {type: 'select', name:'documentTypeId', label:'Tipo Documento', column: 4, load:{ queryId: 195 } }
            ]
        }

        const bar = { 
            fields: [
                {...BUTTON_DEFAULT.SEARCH, onClick: ()=>getGrid().load({params:getForm().getData()})},
                {...BUTTON_DEFAULT.CLEAR, onClick: ()=>getForm().reset()}
            ]
        }

        const grid = {
            ...getGridServerDefaultParams({queryLoad: 128}),
            title: 'Lista de Documentos de Caja y Bancos',
            fields:[
                {name: 'description', label: 'Descripción',},
                {name: 'abbreviature', label: 'Abreviatura',},
                {name: 'documentType', label: 'Tipo Documento',},
                {...COLUMN_DEFAULT.VIEW, link: (value, row, index) => `/treasury/bank-document/bank-document/id-${row.bankDocumentId}`, label: 'Opciones', colSpan: 3 },
                {...COLUMN_DEFAULT.EDIT, link: (value, row, index) => `/treasury/bank-document/bank-document/id-${row.bankDocumentId}/update`, colSpan: 0 },
                {...COLUMN_DEFAULT.DELETE, colSpan: 0, process: {fnOk: (result)=>getGrid().load({params:getForm().getData()}),filter: (value, row, index) => ({queryId: 137, params: {bankDocumentId: row.bankDocumentId}}) }}
            ]
        }
        return (
            <>
                <FormFw
                    name="form"
                    getParentApi={pageApi.getApi}
                    config={form}
                />
                <BarButtonFw
                    name="bar"
                    getParentApi={pageApi.getApi}
                    config={bar}
                />
                <GridFw
                    name="grid"
                    getParentApi={pageApi.getApi}
                    config={grid}
                />
            </>
        )
    }
}
