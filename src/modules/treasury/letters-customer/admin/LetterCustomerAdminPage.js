import React from 'react'
import BarButtonFw, { BUTTON_DEFAULT } from '../../../../framework/bar-button/BarButtonFw';
import FormFw from '../../../../framework/form/FormFw';
import GridFw from '../../../../framework/grid/GridFw';
import { BasePage } from '../../../../framework/pages/BasePage';
import { COLUMN_DEFAULT } from "../../../../framework/grid/GridFw";
import { getFormDefaultParams } from "../../../shared/util/getFormDefaultParams";
import { getGridServerDefaultParams } from "../../../shared/util/getGridServerDefaultParams";
import { DATE_FORMAT } from '../../../../constants/constants';

export class LetterCustomerAdminPage extends BasePage {
    createPagePropertiesBase(pageApi) {
        return {
            title: 'BANDEJA DE LETRAS A CLIENTE',
            barHeader: {
                fields:[
                    {...BUTTON_DEFAULT.NEW, label:'Nuevo', link: '/treasury/letters-customer/letters-customer/new'}
                ]
            }
        };
    }

    renderPage(pageApi) {
        const getGrid = () => pageApi.getComponent('grid');
        const getForm = () => pageApi.getComponent('form');
        
        const form = {
            ...getFormDefaultParams(),
            title: 'FILTROS DE BUSQUEDA',
            fields:[
                {name:'documentNumber', label:'Nro Documento', column: 4},
                {name:'registerNumber', label:'Nro Registro', column: 4},
            ]
        }

        const bar = { fields: [
            {...BUTTON_DEFAULT.SEARCH, onClick: ()=>getGrid().load({params:getForm().getData()})},
            {...BUTTON_DEFAULT.CLEAR, onClick: ()=>getForm().reset()}
        ]}

        const grid = {
            ...getGridServerDefaultParams({queryLoad: 579}),
            title: 'RESULTADOS',
            fields:[
                { name: 'registerNumber', label: 'Nro Registro',},
                { name: 'documentNumber', label: 'Nro. Documento',},
                { type: 'date', name: 'issueDate', label: 'Fec. Emisión', format: DATE_FORMAT.DDMMYYYY},
                { name: 'customer', label: 'Cliente',},
                { name: 'provider', label: 'Proveedor',},
                { name: 'coin', label: 'Moneda',},
                { type: 'decimal', name: 'ammountExchange', label: 'Imp. Canjeado',},
                {...COLUMN_DEFAULT.VIEW, link: (value, row, index) => `/treasury/letters-customer/letters-customer/${row.letterCustomerId}`, label: 'Opciones', colSpan: 2 },
                {...COLUMN_DEFAULT.EDIT, link: (value, row, index) => `/treasury/letters-customer/letters-customer/${row.letterCustomerId}/update`, colSpan: 0 },
            ]
        }
        return (
            <>
                <FormFw
                    name="form"
                    getParentApi={pageApi.getApi}
                    config={form}
                />
                <BarButtonFw
                    name="bar"
                    getParentApi={pageApi.getApi}
                    config={bar}
                />
                <GridFw
                    name="grid"
                    getParentApi={pageApi.getApi}
                    config={grid}
                />
            </>
        )
    }
}