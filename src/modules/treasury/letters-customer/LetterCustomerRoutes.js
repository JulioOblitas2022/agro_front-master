import React from 'react'
import { Route, Switch, useRouteMatch } from 'react-router';
import { LetterCustomerAdminPage } from './admin/LetterCustomerAdminPage';
import { LetterCustomerDetailPage } from './detail/LetterCustomerDetailPage';

const LetterCustomerRoutes = () => {
    const { path } = useRouteMatch();
    return (
        <Switch>
            <Route exact path={`${path}/letters-customer-admin`} component={LetterCustomerAdminPage} />
            <Route path={`${path}/letters-customer`} component={LetterCustomerDetailPage} />
            <Route path={`${path}/letters-customer/:id`} component={LetterCustomerDetailPage} /> 
        </Switch>
    )
}

export default LetterCustomerRoutes;
