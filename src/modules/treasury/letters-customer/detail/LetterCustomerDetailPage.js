import React from 'react'
import BarButtonFw from '../../../../framework/bar-button/BarButtonFw';
import FormFw from '../../../../framework/form/FormFw';
import { BasePage } from '../../../../framework/pages/BasePage';
import { getConfigLetterCustomerDetail } from './getConfigLetterCustomerDetail';
import { getModeLetterCustomerDetail } from './getModeLetterCustomerDetail';
import { getValidationLetterCustomerDetail } from './getValidationLetterCustomerDetail';

export class LetterCustomerDetailPage extends BasePage {
    createPageProperties = (pageApi) => {
        return {
            title: 'REGISTRO DE LETRAS A CLIENTE',
            validation: getValidationLetterCustomerDetail,
            mode: getModeLetterCustomerDetail
        };
    }
    
    renderPage(pageApi) {

        const config = getConfigLetterCustomerDetail(pageApi);

        return (
            <>
                <FormFw
                    name="form"
                    getParentApi={pageApi.getApi}
                    config={config.form}
                />
                <BarButtonFw
                    name="bar"
                    getParentApi={pageApi.getApi}
                    config={config.bar}
                />
            </>
        )
    }
}
