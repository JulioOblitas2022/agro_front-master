import { DATE_FORMAT, PAGE_MODE } from "../../../../constants/constants";
import { BUTTON_DEFAULT } from "../../../../framework/bar-button/BarButtonFw";
import { UserUtil } from "../../../../util/UserUtil";

export const getConfigLetterCustomerDetail = (pageApi) => {
    const getForm = () => pageApi.getComponent('form');

    return {
        form: {
            autoLoad: true,
            load:{
                queryId: 249,
                params: {exchangeId: pageApi.getParamId() || 0},
            },
            update:{
                queryId: 248,
                postLink: (resp, values) => '/treasury/letters-customer/letters-customer-admin',
                params: {modifiedBy: UserUtil.getUserId()}
            },
            save:{
                queryId: 247,
                postLink: (resp, values) => '/treasury/letters-customer/letters-customer-admin',
                params: {createdBy: UserUtil.getUserId()}
            },
            fields:[
                { type: 'select', name: 'companyId', label: 'Empresa', column: 4, load: { queryId: 554, }, search: true },
                { type: 'date', name:'issueDate', label:'Fec. Emisión', column: 4, format: DATE_FORMAT.DDMMYYYY, inputFormat: DATE_FORMAT.YYYYMMDD },
                { type: 'select', name:'coinId', label:'Moneda', column: 4, load:{ queryId: 187 }, search: true},
                { name:'documentNumber', label:'Nro Documento', column: 4, maxLength: 10},
                { type: 'decimal', name:'exchangeType', label:'Tipo de Cambio', column: 4, readOnly: true},
                { type: 'select', name:'customerId', label:'Cliente', column: 4, load:{ queryId: 361 }, search: true},
                { type: 'select', name:'providerId', label:'Proveedor', column: 4, load:{ queryId: 159 }, search: true},
                { type: 'textarea', name:'observations', label:'Observaciones', column: 4, row: 4},
            ],
            onComponentLoad : (formApi, pageApi) =>{
                if (pageApi.getPageMode() !== PAGE_MODE.NEW)
                    formApi.load();
            }
        },
        bar: {
            fields: [
                {...BUTTON_DEFAULT.SAVE, onClick: () => getForm().save()},
                {...BUTTON_DEFAULT.UPDATE, onClick: () => getForm().update()},
                {...BUTTON_DEFAULT.RETURN, link: '/treasury/letters-customer/letters-customer-admin'}
            ],
        },
        grid: {}
    }
}
