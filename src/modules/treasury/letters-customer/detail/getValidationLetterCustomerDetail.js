export const getValidationLetterCustomerDetail = () => {
    return {
        form: {
            customerId: [ {required: true, message: 'Cliente es requerida' } ],
            providerId: [ {required: true, message: 'Proveedor es requerido' } ],
            coinId: [ {required: true, message: 'Moneda es requerida' } ],
            issueDate: [ {required: true, message: 'Fecha de Emisión es requerido' } ],
        },
        grid: {

        },
        button: {

        },
        bar: {

        }
    }
}
