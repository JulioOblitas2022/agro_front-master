import React from 'react';
import { Route, Switch, useRouteMatch } from 'react-router-dom';

import { ApproverAdminPage } from './admin/ApproverAdminPage';
import { ApproverDetailPage } from './detail/ApproverDetailPage';

const ApproverRoutes = () => {
    const { path } = useRouteMatch();
    return (
        <Switch>
            <Route exact path={`${path}/approver-admin`} component={ApproverAdminPage} />
            <Route path={`${path}/approver`} component={ApproverDetailPage} />
            <Route path={`${path}/approver/:id`} component={ApproverDetailPage} /> 
        </Switch>
    )
}

export default ApproverRoutes;