import React from 'react'
import { BUTTON_DEFAULT } from '../../../../framework/bar-button/BarButtonFw';
import { COLUMN_DEFAULT } from '../../../../framework/grid/GridFw';
import { getFormDefaultParams } from '../../../shared/util/getFormDefaultParams';
import { getGridServerDefaultParams } from '../../../shared/util/getGridServerDefaultParams';

export const getConfigApproverAdmin = (pageApi) => {
    const getGrid = () => pageApi.getComponent('grid');
    const getForm = () => pageApi.getComponent('form');

    return {
        form: {
            ...getFormDefaultParams(),
            title: 'BANDEJA DE APROBADORES',
            fields: [
                { type: 'divider', label: 'Filtros de busqueda' },
                { type: 'select', name: 'userId', label: 'Usuario', column: 3, load: { queryId: 100 } },
                { type: 'select', name: 'areaId', label: 'Area', column: 3, load: { queryId: 186 }, search: true },
                { type: 'select', name: 'companyId', label: 'Empresa', column: 4, load: { queryId: 554, } },
            ]
        },
        bar: {
            fields: [
                { ...BUTTON_DEFAULT.SEARCH, onClick: () => getGrid().load({ params: getForm().getData() }) },
                { ...BUTTON_DEFAULT.CLEAR, onClick: () => getForm().reset() }
            ]
        },
        grid: {
            ...getGridServerDefaultParams({ queryLoad: 95 }),
            title: 'RESULTADOS',
            fields: [
                { name: 'user', label: 'Usuario', },
                { name: 'area', label: 'Área', },
                { name: 'companyName', label: 'EMPRESA', },
                { ...COLUMN_DEFAULT.VIEW, link: (value, row, index) => `/logistics/approver/approver/id-${row.approverId}` },
                { ...COLUMN_DEFAULT.EDIT, link: (value, row, index) => `/logistics/approver/approver/id-${row.approverId}/update` },
                { ...COLUMN_DEFAULT.DELETE, process: { fnOk: (result) => getGrid().load({ params: getForm().getData() }), filter: (value, row, index) => ({ queryId: 223, params: { approverId: row.approverId } }) } }
            ]
        }
    }
}
