import React from 'react'
import BarButtonFw, { BUTTON_DEFAULT } from '../../../../framework/bar-button/BarButtonFw';
import FormFw from '../../../../framework/form/FormFw';
import GridFw from '../../../../framework/grid/GridFw';
import { BasePage } from '../../../../framework/pages/BasePage';
import { COLUMN_DEFAULT } from '../../../../framework/grid/GridFw';
import { getFormDefaultParams } from '../../../shared/util/getFormDefaultParams';
import { getGridServerDefaultParams } from '../../../shared/util/getGridServerDefaultParams';

export class ApproverAdminPage extends BasePage {

    createPagePropertiesBase(pageApi) {
        return {
            title: 'APROBADORES',
            barHeader: {
                fields:[
                    {...BUTTON_DEFAULT.NEW, label:'Nuevo', link: '/logistics/approver/approver/new'}
                ]
            }
        };
    }

    renderPage(pageApi) {
        const getGrid = () => pageApi.getComponent('grid');
        const getForm = () => pageApi.getComponent('form');

        const form = {
            ...getFormDefaultParams(),
            title: 'BANDEJA DE APROBADORES',
            fields: [
                { type: 'divider', label: 'Filtros de busqueda' },
                { type: 'select', name: 'userId', label: 'Usuario', column: 3, load: { queryId: 100 } },
                { type: 'select', name: 'areaId', label: 'Area', column: 3, load: { queryId: 186 }, search: true },
                { type: 'select', name: 'companyId', label: 'Empresa', column: 4, load: { queryId: 554, } },
            ]
        }

        const bar = {
            fields: [
                { ...BUTTON_DEFAULT.SEARCH, onClick: () => getGrid().load({ params: getForm().getData() }) },
                { ...BUTTON_DEFAULT.CLEAR, onClick: () => getForm().reset() }
            ]
        }

        const grid = {
            ...getGridServerDefaultParams({ queryLoad: 95 }),
            title: 'RESULTADOS',
            fields: [
                { name: 'user', label: 'Usuario', },
                { name: 'area', label: 'Área', },
                { name: 'companyName', label: 'EMPRESA', },
                { ...COLUMN_DEFAULT.VIEW, link: (value, row, index) => `/logistics/approver/approver/id-${row.approverId}`, label: 'Opciones', colSpan: 3 },
                { ...COLUMN_DEFAULT.EDIT, link: (value, row, index) => `/logistics/approver/approver/id-${row.approverId}/update`, colSpan: 0 },
                { ...COLUMN_DEFAULT.DELETE, process: { fnOk: (result) => getGrid().load({ params: getForm().getData() }), filter: (value, row, index) => ({ queryId: 223, params: { approverId: row.approverId } }) }, colSpan: 0 }
            ]
        }
        return (
            <>
                <FormFw
                    name="form"
                    getParentApi={pageApi.getApi}
                    config={form}
                />
                <BarButtonFw
                    name="bar"
                    getParentApi={pageApi.getApi}
                    config={bar}
                />
                <GridFw
                    name="grid"
                    getParentApi={pageApi.getApi}
                    config={grid}
                />
            </>
        )
    }
}
