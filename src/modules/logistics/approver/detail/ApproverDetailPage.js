import React from 'react'
import BarButtonFw from '../../../../framework/bar-button/BarButtonFw';
import FormFw from '../../../../framework/form/FormFw';
import { BasePage } from '../../../../framework/pages/BasePage';
import { getConfigApproverDetail } from './getConfigApproverDetail';
import { getModeApproverDetail } from './getModeApproverDetail';
import { getValidationApproverDetail } from './getValidationApproverDetail';

export class ApproverDetailPage extends BasePage {
    
    createPageProperties = (pageApi) => {
        return {
            title: 'DETALLE DE APROBADORES',
            validation: getValidationApproverDetail,
            mode: getModeApproverDetail
        };
    }

    renderPage(pageApi) {

        const config = getConfigApproverDetail(pageApi);

        return (
            <>
                <FormFw
                    name="form"
                    getParentApi={pageApi.getApi}
                    config={config.form}
                />
                <BarButtonFw
                    name="bar"
                    getParentApi={pageApi.getApi}
                    config={config.bar}
                />
            </>
        )
    }
}