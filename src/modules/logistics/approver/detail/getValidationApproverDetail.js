export const getValidationApproverDetail = () => {
    return {
        form: {
            userId: [{ required: true, message: 'Usuario es requerido' }],
            areaId: [{ required: true, message: 'Area es requerido' }],
            companyId: [{ required: true, message: 'Empresa es requerido' }],
        },
        grid: {

        },
        button: {

        },
        bar: {

        }
    }
}
