import React from 'react'
import { PAGE_MODE } from '../../../../constants/constants';
import { BUTTON_DEFAULT } from '../../../../framework/bar-button/BarButtonFw';

export const getConfigApproverDetail = (pageApi) => {
    const getForm = () => pageApi.getComponent('form');

    return {
        form: {
            load: {
                queryId: 99,
                params: { approverId: pageApi.getParamId() },
            },
            update: {
                queryId: 97,
                postLink: (resp, values) => '/logistics/approver/approver-admin'
            },
            save: {
                queryId: 96,
                postLink: (resp, values) => '/logistics/approver/approver-admin',
                message: (resp, values) => {
                    return `${resp?.message}`;
                },
                error: (resp, values) => {
                    return `${resp?.error}`;
                }

            },
            title: 'REGITRO DE APROBADORES',
            fields: [
                { type: 'divider', label: 'DATOS PRINCIPALES' },
                { type: 'select', name: 'userId', label: 'Usuario', column: 3, load: { queryId: 100 } },
                { type: 'select', name: 'areaId', label: 'Area', column: 3, load: { queryId: 186 }, search: true },
                { type: 'select', name: 'companyId', label: 'Empresa', column: 4, load: { queryId: 554, } },

            ],
            onComponentLoad: (formApi, pageApi) => {
                if (pageApi.getPageMode() !== PAGE_MODE.NEW)
                    formApi.load();
            }
        },
        bar: {
            fields: [
                { ...BUTTON_DEFAULT.SAVE, onClick: () => getForm().save() },
                { ...BUTTON_DEFAULT.UPDATE, onClick: () => getForm().update() },
                { ...BUTTON_DEFAULT.RETURN, link: '/logistics/approver/approver-admin' }
            ],
        },
        grid: {}
    }
}
