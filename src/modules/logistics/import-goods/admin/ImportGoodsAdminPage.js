import React from 'react'
import BarButtonFw, { BUTTON_DEFAULT } from '../../../../framework/bar-button/BarButtonFw';
import FormFw from '../../../../framework/form/FormFw';
import GridFw from '../../../../framework/grid/GridFw';
import { BasePage } from '../../../../framework/pages/BasePage';
import { DATE_FORMAT } from '../../../../constants/constants';
import { COLUMN_DEFAULT } from '../../../../framework/grid/GridFw';
import { getFormDefaultParams } from '../../../shared/util/getFormDefaultParams';
import { getGridServerDefaultParams } from '../../../shared/util/getGridServerDefaultParams';


export class ImportGoodsAdminPage extends BasePage {
    createPagePropertiesBase(pageApi) {
        return {
            title: 'BANDEJA DE COMPRAS - IMPORTACIÓN DE BIENES',
            barHeader: {
                fields:[
                    {...BUTTON_DEFAULT.NEW, label:'Nuevo', link: '/logistics/import-goods/import-goods/new'}
                ]
            }
        };
    }

    renderPage(pageApi) {

        const getGrid = () => pageApi.getComponent('grid');
        const getForm = () => pageApi.getComponent('form');

        const form = {
            ...getFormDefaultParams(),
            title: 'Filtros de busqueda',
            fields: [
                { name: 'numberPurchaseRecord', label: 'N° Doc', column: 4 },
                { type: 'select', name: 'providerId', label: 'Proveedor', column: 4, load: { queryId: 159 }, search: true },
                { type: 'date', name: 'broadcastDate', label: 'Fecha Emisión', format: DATE_FORMAT.DDMMYYYY, inputFormat: DATE_FORMAT.YYYYMMDD, column: 4 },
                { type: 'date', name: 'expirationDate', label: 'Fecha Vencimiento', format: DATE_FORMAT.DDMMYYYY, inputFormat: DATE_FORMAT.YYYYMMDD, column: 4 },
            ]
        }

        const bar = {
            fields: [
                { ...BUTTON_DEFAULT.SEARCH, onClick: () => getGrid().load({ params: getForm().getData() }) },
                { ...BUTTON_DEFAULT.CLEAR, onClick: () => getForm().reset() }
            ]
        }

        const grid = {
            ...getGridServerDefaultParams({
                queryLoad: 311,
                params: { type: 2 }
            }),
            title: 'Listado de Registros de Compras',
            fields: [
                { name: 'documentType', label: 'T.Documento', },
                { name: 'documentTypeNumber', label: 'N° Doc', },
                { type: 'date', name: 'broadcastDate', label: 'Fec. Emi', format: DATE_FORMAT.DDMMYYYY },
                { type: 'date', name: 'expirationDate', label: 'Fec. ven', format: DATE_FORMAT.DDMMYYYY },
                { name: 'provider', label: 'Proveedor', },
                { name: 'coin', label: 'Moneda', },
                { type: 'decimal', name: 'exchangeType', label: 'Tipo de Cambio', },
                { type: 'decimal', name: 'totalAmount', label: 'Importe', },
                { ...COLUMN_DEFAULT.VIEW, label: 'Opciones', colSpan: 2, link: (value, row, index) => `/logistics/import-goods/import-goods/${row.purchaseId}` },
                { ...COLUMN_DEFAULT.EDIT,colSpan: 0, link: (value, row, index) => `/logistics/import-goods/import-goods/${row.purchaseId}/update` },
            ]
        }
        return (
            <>
                <FormFw
                    name="form"
                    getParentApi={pageApi.getApi}
                    config={form}
                />
                <BarButtonFw
                    name="bar"
                    getParentApi={pageApi.getApi}
                    config={bar}
                />
                <GridFw
                    name="grid"
                    getParentApi={pageApi.getApi}
                    config={grid}
                />
            </>
        )
    }
}