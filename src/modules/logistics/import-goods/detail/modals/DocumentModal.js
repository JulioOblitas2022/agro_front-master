import React from 'react';
import { DATE_FORMAT } from '../../../../../constants/constants';
import FormGridFw, { FORM_GRID_MODE } from '../../../../../framework/form-grid/FormGridFw';
import { SELECTION_TYPE } from '../../../../../framework/grid/GridFw';
import { BasePage } from '../../../../../framework/pages/BasePage';

export class DocumentModal extends BasePage {
    
    
    createPageProperties = (pageApi) => {
        return {
            title: 'Busqueda de Documentos',
        };
    }
    
    renderPage(pageApi) {
        const formGridConfig = {
            mode: FORM_GRID_MODE.FORM_SEARCH_ADD,
            afterAddEvent: (data) => {
                pageApi.getEvent('afterAddEvent')(data);
                pageApi.hideThisModal();
            },
            form: {
                fields:[
                    {name:'documentNumber', label:'Documento', column: 6},
                ]
            },
            grid: {
                rowId: 'purchaseOrderId',
                showIndex: false,
                autoLoad: true,
                pagination: false,
                selection: {type: SELECTION_TYPE.SINGLE},
                load: {
                    queryId: 551,
                    params: { providerId: pageApi.name}
                },
                fields:[
                    {name:'documentType', label:'Tipo Documento'},
                    {type: 'date', name:'broadcastDate', label:'Fecha', format: DATE_FORMAT.DDMMYYYY},
                    {name:'documentNumber', label:'Nro. Documento'},
                    {name:'provider', label: 'Proveedor'}
                ]
            }
        };  
    
        return (
            <>
                <FormGridFw 
                    name="documentFormGrid" 
                    getParentApi={pageApi.getApi}
                    config={formGridConfig}
                />
            </>
        )

    }
    
}