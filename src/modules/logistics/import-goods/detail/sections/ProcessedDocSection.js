import React from 'react'
import { BUTTON_DEFAULT } from '../../../../../framework/bar-button/BarButtonFw';
import GridFw from '../../../../../framework/grid/GridFw';
import { MessageUtil } from '../../../../../util/MessageUtil';
import { DocumentModal } from '../modals/DocumentModal';

export const createProcessedDocSection = (pageApi) => {

    const getForm = () => pageApi.getComponent('form');
    const getGridDocument = () => pageApi.getComponent('documentGrid');
    const getGridItem = () => pageApi.getComponent('itemGrid');

    const barHeaderConfig = {
        fields: [
            {...BUTTON_DEFAULT.ADD, onClick: () => {
                let providerId = getForm().getData().providerId;
                if(providerId){
                    pageApi.showModal(DocumentModalConfig(providerId));
                }
                else{
                    return MessageUtil('warning','Alerta!','Debe Seleccionar Proveedor');
                }
            }},
        ],
    };

    const documentConfig = {
        events: {
            afterAddEvent : (data) => {
                let items = JSON.parse(data[0].items);
                
                getGridDocument().addItem(data);

                getGridItem().addItem(items);
            }
        }
    }


    const DocumentModalConfig = (data) => {
        return {
            width: 950,
            maskClosable: false,
            component: <DocumentModal name={data} getParentApi={pageApi.getApi} config={documentConfig}/>
        }
    };

    const gridConfig = {
        title: 'Documentos',
        rowId: 'purchaseRecordDetailDocumentId',
        showIndex: true,
        pagination: true,
        autoLoad: true,
        collapsible: true,
        load: {
            validation: () => pageApi.getParamId()!=null, //solo se dispara si el valor es distinto de nulo
            queryId: 553,
            params: {purchaseId: pageApi.getParamId()},
            hideMessage: true,
            hideMessageError: true
        },
        save: {
            queryId: 552,
            params: {purchaseId: pageApi.getParamId()},
            hideMessage: true,
            hideMessageError: true
        },
        fields:[
            {name:'documentType', label:'Tipo Doc.'},
            {name:'documentNumber', label:'N° Doc.n'},
        ],
        barHeader: barHeaderConfig
    };
    return (
        <>
            <GridFw
                name="documentGrid"
                getParentApi={pageApi.getApi}
                config={gridConfig} 
            />
        </>
    )
}