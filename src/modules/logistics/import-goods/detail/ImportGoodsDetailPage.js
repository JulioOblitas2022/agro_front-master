import React from 'react'
import BarButtonFw from '../../../../framework/bar-button/BarButtonFw';
import FormFw from '../../../../framework/form/FormFw';
import { BasePage } from '../../../../framework/pages/BasePage';
import { getConfigImportGoodsDetail } from './getConfigImportGoodsDetail';
import { getModeImportGoodsDetail } from './getModeImportGoodsDetail';
import { getValidationImportGoodsDetail } from './getValidationImportGoodsDetail';
import { createItemSection } from './sections/ItemSection';
import { createProcessedDocSection } from './sections/ProcessedDocSection';

export class ImportGoodsDetailPage extends BasePage {
    
    createPageProperties = (pageApi) => {
        return {
            title: 'REGISTRO DE COMPRA',
            validation: getValidationImportGoodsDetail,
            mode: getModeImportGoodsDetail
        };
    }

    renderPage(pageApi) {

        const config = getConfigImportGoodsDetail(pageApi);

        return (
            <>
                <FormFw
                    name="form"
                    getParentApi={pageApi.getApi}
                    config={config.form}
                />
                
                {createProcessedDocSection(pageApi)}
                
                {createItemSection(pageApi)}

                <BarButtonFw
                    name="bar"
                    getParentApi={pageApi.getApi}
                    config={config.bar}
                />
            </>
        )
    }
}