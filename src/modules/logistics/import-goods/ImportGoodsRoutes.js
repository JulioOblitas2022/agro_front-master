import React from 'react';
import { Route, Switch, useRouteMatch } from 'react-router';

import { ImportGoodsAdminPage } from './admin/ImportGoodsAdminPage';
import { ImportGoodsDetailPage } from './detail/ImportGoodsDetailPage';

const ImportGoodsRoutes = () => {
    const { path } = useRouteMatch();
    return (
        <Switch>
            <Route exact path={`${path}/import-goods-admin`} component={ImportGoodsAdminPage} />
            <Route path={`${path}/import-goods`} component={ImportGoodsDetailPage} />
            <Route path={`${path}/import-goods/:id`} component={ImportGoodsDetailPage} /> 
        </Switch>
    )
}

export default ImportGoodsRoutes;