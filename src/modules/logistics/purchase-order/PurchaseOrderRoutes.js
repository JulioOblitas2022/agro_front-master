import React from 'react';
import { Route, Switch, useRouteMatch } from 'react-router-dom';

import { PurchaseOrderAdminPage } from './admin/PurchaseOrderAdminPage';
import { PurchaseOrderDetailPage } from './detail/PurchaseOrderDetailPage';

const PurchaseOrderRoutes = () => {
    const { path } = useRouteMatch();
    return (
        <Switch>
            <Route exact path={`${path}/purchase-order-admin`} component={PurchaseOrderAdminPage} />
            <Route path={`${path}/purchase-order`} component={PurchaseOrderDetailPage} />
            <Route path={`${path}/purchase-order/:id`} component={PurchaseOrderDetailPage} /> 
        </Switch>
    )
}

export default PurchaseOrderRoutes;