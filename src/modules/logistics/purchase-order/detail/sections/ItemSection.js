import React from 'react'
import { DATE_FORMAT } from '../../../../../constants/constants';
import { BUTTON_DEFAULT } from '../../../../../framework/bar-button/BarButtonFw';
import FormGridFw, { FORM_GRID_MODE } from '../../../../../framework/form-grid/FormGridFw';
import FormFw from '../../../../../framework/form/FormFw';
import GridFw, { COLUMN_DEFAULT } from '../../../../../framework/grid/GridFw';
import { ProductModal } from '../../../../sales/referral-guide/detail/modals/ProductModal';

export const createItemSection = (pageApi) => {

    const getGrid = () => pageApi.getComponent('itemGrid');
    const getForm = () => pageApi.getComponent('formSumas');

    const fnDelete = (value, row, index) => {
        getGrid().deleteItem(row.key);      
    }

    const itemConfig = {
        events: {
            afterAddEvent : (data) => {
                getGrid().addItem(data);
            }
        }
    }

    const ItemModalConfig = (data) =>  {
        return {
            width: 1600,
            maskClosable: false,
            component: <ProductModal name={data}  getParentApi={pageApi.getApi} config={itemConfig}  />
        }
    };


    const barHeaderConfig = {
        fields:[
            {...BUTTON_DEFAULT.ADD, label:'Agregar Item', onClick: ()=>{  pageApi.showModal(ItemModalConfig('')); }}
        ]
    }

    const calculateTotal = () => {
        let items = getGrid().getData();
        if (items) {
            let total = 0;
            items.forEach(item => {
                total = total + parseFloat(item.subTotal);
            });
            let total_analisis = parseFloat(total).toFixed(2);

            getForm().setFieldData('grossTax', total_analisis);
            getForm().setFieldData('igv', parseFloat(total_analisis * 0.18).toFixed(2));
            getForm().setFieldData('total', parseFloat(total_analisis * 1.18).toFixed(2));
        }
    }


    const gridConfig = {
        title: 'Otros items registrados',
        rowId: 'purchaseOrderDetailId',
        showIndex: true,
        pagination: true,
        autoLoad: true,
        addItem: {
            fnAfter: (row) => {
                calculateTotal();
            }
        },
        updateItem: {
            fnAfter: (row) => {
                calculateTotal();
            }
        },
        load: {
            validation: () => pageApi.getParamId() != null, //solo se dispara si el valor es distinto de nulo
            queryId: 201,
            orderBy: 'purchaseOrderDetailId desc',
            params: { purchaseOrderId: pageApi.getParamId() },
            fnOk: () => {
                calculateTotal();
            },
            hideMessage: true, 
            hideMessageError: true
        },
        save: {
            queryId: 194,
            params: { purchaseOrderId: pageApi.getParamId() },
            hideMessage: true, 
            hideMessageError: true
        },
        fields: [
            { name: 'itemTypeText', label: 'Tipo Item', editable: false },
            { name: 'description', label: 'Item', editable: false },
            { type: 'select', name: 'unitMeasureId', label: 'Uni. Med.', labelName: 'unitMeasureIdDes', load: { queryId: 568, params: () => {return {masterItemId: getGrid()?.itemEditing?.masterItemId}}}, search: true, editable: true },
            { type: 'number', name: 'quantity', label: 'Cantidad', editable: true, },
            {
                type: 'number', name: 'receivedAmount', label: 'Cantidad Recibida', editable: true,
                onChange: (value, values) => {
                    let quantity = parseInt(values['receivedAmount']);
                    let unitPriceAffected = 0;

                    if (parseInt(values['unitPriceAffected']) === 0) {
                        values['unitPriceUnaffected'] = 0;
                        unitPriceAffected = parseFloat(values['unitPriceUnaffected'])
                    }
                    else {
                        unitPriceAffected = parseFloat(values['unitPriceAffected']);
                        values['unitPriceUnaffected'] = 0;
                    }

                    let sub_total = parseFloat(quantity * unitPriceAffected).toFixed(2);

                    getGrid()?.getFormEditable().setFieldData('subTotal', sub_total);
                }
            },
            {
                type: 'decimal', name: 'unitPriceAffected', label: 'P.U Afecto', editable: true,
                onChange: (value, values, formApi) => {
                    let quantity = parseInt(values['quantity']);
                    let unitPriceAffected = 0;

                    if (parseFloat(values['unitPriceAffected']) > 0) {
                        values['unitPriceUnaffected'] = '0.00';

                        let dataGrid = getGrid().getData();
                        dataGrid.map(x => {
                            x.unitPriceUnaffected = '0.00';
                            if (x.unitPriceUnaffected === '0.00') {
                                x.subTotal = '0.00';
                            }
                            x.subTotal = parseFloat(x.quantity * x.unitPriceAffected).toFixed(2);

                        })
                        getGrid().setData(dataGrid);
                        getGrid()?.getFormEditable().setFieldData('unitPriceUnaffected', '0.00');
                        unitPriceAffected = parseFloat(values['unitPriceAffected']).toFixed(2);

                        let sub_total = parseFloat(quantity * unitPriceAffected).toFixed(2);

                        formApi?.setFieldData('subTotal', sub_total);
                    }
                    calculateTotal();
                }
            },
            {
                type: 'decimal', name: 'unitPriceUnaffected', label: 'P.U Inafecto', editable: true,
                onChange: (value, values, formApi) => { //el formApi trae la fila

                    let quantity = parseInt(values['quantity']);
                    let unitPriceUnaffected = 0;

                    if (parseFloat(values['unitPriceUnaffected']) > 0) {
                        values['unitPriceAffected'] = 0;

                        let dataGrid = getGrid().getData();
                        dataGrid.map(x => {
                            x.unitPriceAffected = '0.00';
                            if (x.unitPriceAffected === '0.00') {
                                x.subTotal = '0.00';
                            }
                            x.subTotal = parseFloat(x.quantity * x.unitPriceUnaffected).toFixed(2);
                        })
                        getGrid().setData(dataGrid);
                        unitPriceUnaffected = parseFloat(values['unitPriceUnaffected']).toFixed(2);
                        getGrid()?.getFormEditable().setFieldData('unitPriceAffected', '0.00');

                        let sub_total = parseFloat(quantity * unitPriceUnaffected).toFixed(2);

                        formApi?.setFieldData('subTotal', sub_total);

                    }
                    calculateTotal();
                }
            },
            { type: 'decimal', name: 'subTotal', label: 'Sub Total', editable: true, readOnly: true },
            {...COLUMN_DEFAULT.DELETE, onClick: fnDelete}
        ],
        barHeader: barHeaderConfig
    };

    const form_sumas = {
        fields: [
            { type: 'decimal', name: 'grossTax', label: 'Imp. Bruto', column: 3, readOnly: true },
            //{ type: 'decimal', name: 'unaffectedTax', label: 'Imp. Inafecto', column: 3, readOnly: true },
            { type: 'decimal', name: 'igv', label: 'I.G.V', column: 3, readOnly: true },
            { type: 'decimal', name: 'total', label: 'Total', column: 3, readOnly: true }
        ],
    }
    return (
        <>
            <GridFw
                name="itemGrid"
                getParentApi={pageApi.getApi}
                config={gridConfig}
            />

            <FormFw
                name="formSumas"
                getParentApi={pageApi.getApi}
                config={form_sumas}
            />
        </>
    )
}