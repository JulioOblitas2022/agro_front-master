import { DATE_FORMAT, PAGE_MODE } from '../../../../constants/constants';
import { BUTTON_DEFAULT } from '../../../../framework/bar-button/BarButtonFw';
import { DateUtil } from '../../../../util/DateUtil';
import { MessageUtil } from '../../../../util/MessageUtil';
import { UserUtil } from '../../../../util/UserUtil';
import { DocReferencesRequerimentModal } from './modals/DocReferencesRequerimentModal';

export const getConfigPurchaseOrderDetail = (pageApi) => {
    const getForm = () => pageApi.getComponent('form');
    const getGridItemDetailGrid = () => pageApi.getComponent('itemGrid');

    const docRefConfig = {
        events: {
            afterAddEvent: (data) => {

                let itemData = JSON.parse(data[0].items);
                //console.log(itemData);
                if (itemData !== null) {
                    getGridItemDetailGrid().setData(itemData);
                }
                else {
                    getGridItemDetailGrid().reset();
                }
                getForm().setFieldData('documentRefId', data[0].documentRefId);
                getForm().setFieldData('documentRefNumber', data[0].documentRefNumber);

                const motiveId = data[0].reasonId;
                if (motiveId !== null) {
                    getForm().setFieldData('reasonId', data[0].reasonId);
                } else {
                    getForm().setFieldData('reasonId', ''); //ARREGLAR Y CAMBIAR POR RESET 
                }

            }
        }
    }

    const DocRefModalOPuuchaseConfig = (id) => {
        return {
            width: 1000,
            maskClosable: false,
            component: <DocReferencesRequerimentModal name={id} getParentApi={pageApi.getApi} config={docRefConfig} />
        }
    };

    return {
        form: {
            autoLoad: true,
            load: {
                queryId: 200,
                params: { purchaseOrderId: pageApi.getParamId() || 0 }
            },
            save: {
                queryId: 193,
                postLink: (resp, values) => '/logistics/purchase-order/purchase-order-admin',
                params: { createdBy: UserUtil.getUserId() },
                fnOk: (resp) => {
                    const purchaseOrderId = resp.dataObject?.purchaseOrderId;
                    if (purchaseOrderId) {
                        getGridItemDetailGrid().save({ params: { purchaseOrderId } });
                    }
                }
            },
            update: {
                queryId: 202,
                postLink: (resp, values) => '/logistics/purchase-order/purchase-order-admin',
                params: { purchaseOrderId: pageApi.getParamId(), modifiedBy: UserUtil.getUserId() },
                fnOk: (resp) => {
                    getGridItemDetailGrid().save();
                }
            },
            title: 'REGISTRO DE ORDEN DE COMPRA',
            fields: [
                //{ type: 'divider', label: 'DATOS PRINCIPALES' },
                { name:'numberPurchaseOrder', label: 'Nro. Documento', column: 4, readOnly: true},
                { type: 'select', name: 'companyId', label: 'Empresa', column: 4, load: { queryId: 554, }, search: true },
                { type: 'select', name: 'localId', label: 'Local', column: 4, load: { queryId: 198 },
                    parents: [{paramName: 'companyId', name: 'companyId', required: true}]
                },
                { type: 'select', name: 'applicantId', label: 'Solicitante', column: 4, load: { queryId: 309 } },
                { type: 'select', name: 'areaId', label: 'Area', column: 4, load: { queryId: 186 }, search: true },
                { type: 'select', name: 'providerId', label: 'Proveedor', column: 4, load: { queryId: 159 }, search: true },
                { type: 'select', name: 'documentTypeRefId', label: 'Tipo Doc. Ref.', column: 4, load: { queryId: 549 }, value: 75,
                    onChange: (value) => {
                        if (value == 75) {
                            getForm()?.getConfig()?.fields[8].setVisible(true);
                        } 
                        else if (value != 75) {
                            getForm()?.getConfig()?.fields[8].setVisible(false);
                            getForm()?.setFieldData('documentRefId', '');
                            getForm()?.setFieldData('documentRefNumber', '');
                        }
                    },
                },
                { type: 'hidden', name: 'documentRefId' },
                { name: 'documentRefNumber', label: 'Nro. Documento', column: 4, readOnly: true },
                { type: 'button', column: 1, name: 'modal',
                    onClick: (value) => {
                        let idLocal = getForm()?.getFieldApi('localId')?.getSelectedItem()?.value;
                        if (idLocal !== undefined) {
                            pageApi.showModal(DocRefModalOPuuchaseConfig(getForm()?.getData()?.localId));
                        } else {
                            return MessageUtil('warning', 'Alerta!', 'Debe Seleccionar el Local');
                        }

                    },
                },
                { type: 'select', name: 'coinId', label: 'Moneda', column: 3, load: { queryId: 187 }, search: true },
                { type: 'date', name: 'broadcastDate', label: 'Fecha Emisión', format: DATE_FORMAT.DDMMYYYY, inputFormat: DATE_FORMAT.YYYYMMDD, column: 4, value: DateUtil.getDate },
                { type: 'date', name: 'deliverDate', label: 'Fecha Entrega', format: DATE_FORMAT.DDMMYYYY, inputFormat: DATE_FORMAT.YYYYMMDD, column: 4, value: DateUtil.getDate },
                { type: 'select', name: 'paymentConditionId', label: 'Cond. Pago', column: 4, load: { queryId: 188 }, search: true },
                { type: 'select', name: 'reasonId', label: 'Motivo', column: 4, load: { queryId: 189 }, search: true },
                { type: 'radio', name: 'active', label: 'Estado', column: 4, value: 1, disabled: true,
                    load: {
                        dataList: [
                            { label: 'PENDIENTE', value: 1 },
                            { label: 'TERMINADO', value: 2 },
                        ]
                    }
                },
                { type: 'textarea', name: 'observation', label: 'Observaciones', column: 12 },
            ],
            onComponentLoad: (formApi, pageApi) => {
                if (pageApi.getPageMode() !== PAGE_MODE.NEW)
                    formApi.load();
            }
        },
        bar: {
            fields: [
                { ...BUTTON_DEFAULT.SAVE, onClick: () => getForm().save() },
                { ...BUTTON_DEFAULT.UPDATE, onClick: () => getForm().update() },
                { ...BUTTON_DEFAULT.RETURN, link: '/logistics/purchase-order/purchase-order-admin' }
            ],
        },
        grid: {}
    }
}
