import React from 'react'
import BarButtonFw from '../../../../framework/bar-button/BarButtonFw';
import FormFw from '../../../../framework/form/FormFw';
import { BasePage } from '../../../../framework/pages/BasePage';
import { getConfigPurchaseOrderDetail } from './getConfigPurchaseOrderDetail';
import { getModePurchaseOrderDetail } from './getModePurchaseOrderDetail';
import { getValidationPurchaseOrderDetail } from './getValidationPurchaseOrderDetail';
import { createItemSection } from './sections/ItemSection';

export class PurchaseOrderDetailPage extends BasePage {
    
    createPageProperties = (pageApi) => {
        return {
            title: 'DETALLE DE ORDEN DE COMPRA',
            validation: getValidationPurchaseOrderDetail,
            mode: getModePurchaseOrderDetail
        };
    }

    renderPage(pageApi) {

        const config = getConfigPurchaseOrderDetail(pageApi);

        return (
            <>
                <FormFw
                    name="form"
                    getParentApi={pageApi.getApi}
                    config={config.form}
                />
                
                {createItemSection(pageApi)}

                <BarButtonFw
                    name="bar"
                    getParentApi={pageApi.getApi}
                    config={config.bar}
                />
            </>
        )
    }
}