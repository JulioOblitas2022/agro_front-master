import React from 'react'
import BarButtonFw, { BUTTON_DEFAULT } from '../../../../framework/bar-button/BarButtonFw';
import FormFw from '../../../../framework/form/FormFw';
import GridFw from '../../../../framework/grid/GridFw';
import { BasePage } from '../../../../framework/pages/BasePage';
import { DATE_FORMAT } from '../../../../constants/constants';
import { COLUMN_DEFAULT } from '../../../../framework/grid/GridFw';
import { getFormDefaultParams } from '../../../shared/util/getFormDefaultParams';
import { getGridServerDefaultParams } from '../../../shared/util/getGridServerDefaultParams';
import PurchaseOrder from './pdf/PurchaseOrder';


export class PurchaseOrderAdminPage extends BasePage {
    createPagePropertiesBase(pageApi) {
        return {
            title: 'Orden de Compra',
            barHeader: {
                fields:[
                    {...BUTTON_DEFAULT.NEW, label:'Nuevo', link: '/logistics/purchase-order/purchase-order/new'}
                ]
            }
        };
    }

    renderPage(pageApi) {

        const getGrid = () => pageApi.getComponent('grid');
        const getForm = () => pageApi.getComponent('form');

        const form = {
            ...getFormDefaultParams(),
            title: 'Orden de Compra',
            fields: [
                //{type: 'divider', label: 'Filtros de busqueda'},
                { name: 'numberPurchaseOrder', label: 'N° Orden Compra', column: 4 },
                { type: 'select', name: 'providerId', label: 'Proveedor', column: 4, load: { queryId: 159 }, search: true },
                { type: 'select', name: 'localId', label: 'Local', column: 4, load: { queryId: 198 } },
                { type: 'date', name: 'broadcastDate', label: 'Fecha Emisión', format: DATE_FORMAT.DDMMYYYY, inputFormat: DATE_FORMAT.YYYYMMDD, column: 4 },
                { type: 'date', name: 'deliverDate', label: 'Fecha Entrega', format: DATE_FORMAT.DDMMYYYY, inputFormat: DATE_FORMAT.YYYYMMDD, column: 4 },
                { type: 'select', name: 'companyId', label: 'Empresa', column: 4, load: { queryId: 554, }, search: true },
            ]
        }

        const bar = {
            fields: [
                { ...BUTTON_DEFAULT.SEARCH, onClick: () => getGrid().load({ params: getForm().getData() }) },
                { ...BUTTON_DEFAULT.CLEAR, onClick: () => getForm().reset() }
            ]
        }

        const grid = {
            ...getGridServerDefaultParams({ queryLoad: 199 }),
            title: 'LISTADO DE ORDEN DE COMPRA',
            fields: [
                { name: 'numberPurchaseOrder', label: 'N° Orden de Compra' },
                { name: 'provider', label: 'Proveedor', },
                { type: 'date', name: 'broadcastDate', label: 'Fec. Emisión', format: DATE_FORMAT.DDMMYYYY },
                { type: 'date', name: 'deliverDate', label: 'Fec. Entrega', format: DATE_FORMAT.DDMMYYYY },
                { name: 'local', label: 'Local', },
                { name: 'coin', label: 'Moneda', },
                { type: 'decimal', name: 'grossTax', label: 'Importe Bruto', },
                { type: 'decimal', name: 'igv', label: 'I.G.V', },
                { type: 'decimal', name: 'total', label: 'Importe Total', },
                { name: 'companyName', label: 'Empresa', },
                { name: 'active', label: 'Estado', },
                { ...COLUMN_DEFAULT.VIEW, label: 'Opciones', colSpan: 3, link: (value, row, index) => `/logistics/purchase-order/purchase-order/${row.purchaseOrderId}` },
                { ...COLUMN_DEFAULT.EDIT, colSpan: 0, link: (value, row, index) => `/logistics/purchase-order/purchase-order/${row.purchaseOrderId}/update` },
                { ...COLUMN_DEFAULT.PDF, title: 'Descargar Documento', colSpan: 0, onClick: (value, row, index) => PurchaseOrder(row) }
            ]
        }
        return (
            <>
                <FormFw
                    name="form"
                    getParentApi={pageApi.getApi}
                    config={form}
                />
                <BarButtonFw
                    name="bar"
                    getParentApi={pageApi.getApi}
                    config={bar}
                />
                <GridFw
                    name="grid"
                    getParentApi={pageApi.getApi}
                    config={grid}
                />
            </>
        )
    }
}
