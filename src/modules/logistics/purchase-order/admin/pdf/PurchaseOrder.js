import { jsPDF } from 'jspdf'
import autoTable from 'jspdf-autotable'
import { DATE_FORMAT } from '../../../../../constants/constants';
import { DateUtil } from '../../../../../util/DateUtil';
//import logo from '../../../../assets/logo_bv.jpeg';


const PurchaseOrder = (props) => {

    let data = JSON.parse(props.items);

    const doc = new jsPDF();

    let rows = [
        ["Fch. Emision: " + props.fecha_emision, "Fch. Entrega: "+ props.fecha_entrega ],
        ["Nro. RUC: " + props.RUC],
        ["Proveedor: " + props.provider],
        ["Dirección: " + props.address],
        ["Cond. Pago: " + props.paymentCondition, "Moneda: "+props.coin],
    ];

    
    doc.autoTable({
        body: rows, 
        startY: 20, 
        styles: {
            cellPadding: 1,
            fontSize: 7,
            lineColor: 255,
            lineWidth: 0,
            fillColor: 255,
            textColor: 20,
            halign: 'center', // left, center, right
            valign: 'middle', // top, middle, bottom
            fillStyle: 'DF', // 'S', 'F' or 'DF' (stroke, fill or fill then stroke)
            rowHeight: 5,
            columnWidth: 'auto', // 'auto', 'wrap' or a number
            overflow: 'linebreak'
        }
    });

    const headRows = () => {
        return [
          { code: 'Código', item: 'Descripcion', unitMeasure: 'Uni. Med.', unitPriceAffected: 'Imp. Afecto', unitPriceUnaffected: 'Imp. Inafecto', total: 'Sub Total'},
        ]
    }
      
    let suma_afecto = 0;
    let suma_inafecto = 0;
    let suma_sub_total = 0;  
    const bodyRows = (data) => {
        let body = [];
        data.forEach( x => {
            suma_afecto += x.unitPriceAffected;
            suma_inafecto += x.unitPriceUnaffected;
            suma_sub_total += (x.receivedAmount * (x.unitPriceAffected == 0 ? x.unitPriceUnaffected : x.unitPriceAffected));
            let obj = {
                code: x.code,
                item: x.item,
                unitMeasure: x.unitMeasure,
                unitPriceAffected: parseFloat(x.unitPriceAffected).toFixed(2),
                unitPriceUnaffected: parseFloat(x.unitPriceUnaffected).toFixed(2),
                total: parseFloat((x.receivedAmount * (x.unitPriceAffected == 0 ? x.unitPriceUnaffected : x.unitPriceAffected))).toFixed(2)
            }
            body = [...body, obj];
        });
        return body
      }

    let foot = [
        ["","","Imp. Afecto","Imp. Inafecto","IGV","Total"],
        ["","",parseFloat(suma_afecto).toFixed(2),parseFloat(suma_inafecto).toFixed(2),parseFloat((suma_afecto == 0 ? suma_inafecto *0.18: suma_afecto *0.18)).toFixed(2),parseFloat(suma_sub_total).toFixed(2)],
    ];

    var totalPagesExp = doc.internal.getNumberOfPages()
    // doc.setFontSize(18)
    // doc.text('With content', 14, 22)
    // doc.setFontSize(11)
    // doc.setTextColor(100)

    // jsPDF 1.4+ uses getWidth, <1.4 uses .width
    var pageSize = doc.internal.pageSize
    var pageWidth = pageSize.width ? pageSize.width : pageSize.getWidth()
    var text = doc.splitTextToSize('', pageWidth - 35, {})
    doc.text(text, 14, 30)

    doc.autoTable({
        theme: 'striped',
        styles: {
                cellPadding: 1,
                fontSize: 7,
                lineColor: 200,
                lineWidth: 0.1,
                fillColor: 255,
                textColor: 20,
                halign: 'center', // left, center, right
                valign: 'middle', // top, middle, bottom
                fillStyle: 'F', // 'S', 'F' or 'DF' (stroke, fill or fill then stroke)
                rowHeight: 5,
                columnWidth: 'auto', // 'auto', 'wrap' or a number
                overflow: 'linebreak'
        },
        head: headRows(),
        body: bodyRows(data),
        foot: foot,
        startY: doc.lastAutoTable.finalY + 10,
        showHead: 'firstPage',
        pageBreak: 'auto', 
        tableWidth: 'auto',
        didDrawPage: function (data) {
            // Header
            doc.setFontSize(20)
            doc.setTextColor(40)
            // if (base64Img) {
            //   doc.addImage(base64Img, 'JPEG', data.settings.margin.left, 15, 10, 10)
            // }
            // doc.text('Report', data.settings.margin.left + 15, 22)
      
            // Footer
            var str = 'Página ' + doc.internal.getNumberOfPages()
            // Total page number plugin only available in jspdf v1.0+
            if (typeof doc.putTotalPages === 'function') {
              str = str + ' de ' + totalPagesExp
            }
            doc.setFontSize(10)
      
            // jsPDF 1.4+ uses getWidth, <1.4 uses .width
            var pageSize = doc.internal.pageSize
            var pageHeight = pageSize.height ? pageSize.height : pageSize.getHeight()
            doc.text(str, data.settings.margin.left, pageHeight - 10)
          },
    })

    //doc.text('text', 14, doc.lastAutoTable.finalY + 10)
    doc.save('Orden de Compra '+props.numberPurchaseOrder+'.pdf');
};

export default PurchaseOrder;

//doc.lastAutoTable.finalY
