import React from 'react'
import BarButtonFw, { BUTTON_DEFAULT } from '../../../../framework/bar-button/BarButtonFw';
import FormFw from '../../../../framework/form/FormFw';
import GridFw from '../../../../framework/grid/GridFw';
import { BasePage } from '../../../../framework/pages/BasePage';
import { DATE_FORMAT } from "../../../../constants/constants";
import { COLUMN_DEFAULT } from "../../../../framework/grid/GridFw";
import { getFormDefaultParams } from "../../../shared/util/getFormDefaultParams";
import { getGridServerDefaultParams } from "../../../shared/util/getGridServerDefaultParams";
export class RequestOrderAdminPage extends BasePage {

    createPageProperties = (pageApi) => {
        return {
            title: 'BANDEJA DE ORDEN DE REQUERIMIENTO',
            barHeader: {
                fields:[
                    {...BUTTON_DEFAULT.NEW, label:'Nuevo', link: '/logistics/request-order/request-order/new'}
                ]
            }
        };
    }

    renderPage(pageApi) {

        const getGrid = () => pageApi.getComponent('grid');
        const getForm = () => pageApi.getComponent('form');

        const form = {
            ...getFormDefaultParams(),
            fields: [
                { name: 'requestOrderNumber', label: 'Nro Orden Req.', column: 4 },
                { type: 'select', name: 'areaId', label: 'Area', column: 4, load: { queryId: 186 }, search: true },
                { type: 'select', name: 'localId', label: 'Local', column: 4, load: { queryId: 198 } },
                { type: 'select', name: 'companyId', label: 'Empresa', column: 4, load: { queryId: 554, } },
            ]
        }

        const bar = {
            fields: [
                { ...BUTTON_DEFAULT.SEARCH, onClick: () => getGrid().load({ params: getForm().getData() }) },
                { ...BUTTON_DEFAULT.CLEAR, onClick: () => getForm().reset() }
            ]
        }

        const grid = {
            ...getGridServerDefaultParams({ queryLoad: 228 }),
            title: 'LISTADO DE ÓRDENES DE REQUERIMIENTO',
            fields: [
                { name: 'requestOrderNumber', label: 'Nro Orden Req.' },
                { type: 'date', name: 'broadcastDate', label: 'Fecha Emision', format: DATE_FORMAT.DDMMYYYY },
                { type: 'date', name: 'deliverDate', label: 'Fecha Entrega', format: DATE_FORMAT.DDMMYYYY },
                { name: 'local', label: 'Local' },
                { name: 'area', label: 'Área' },
                { name: 'applicant', label: 'Solicitante' },
                { name: 'priority', label: 'Prioridad' },
                { name: 'active', label: 'Estado' },
                { name: 'companyName', label: 'EMPRESA', },
                { ...COLUMN_DEFAULT.VIEW, label: 'Opciones', colSpan: 2, link: (value, row, index) => `/logistics/request-order/request-order/${row.requestOrderId}` },
                { ...COLUMN_DEFAULT.EDIT, colSpan: 0, link: (value, row, index) => `/logistics/request-order/request-order/${row.requestOrderId}/update` },
            ]
        }

        return (
            <>
                <FormFw
                    name="form"
                    getParentApi={pageApi.getApi}
                    config={form}
                />
                <BarButtonFw
                    name="bar"
                    getParentApi={pageApi.getApi}
                    config={bar}
                />
                <GridFw
                    name="grid"
                    getParentApi={pageApi.getApi}
                    config={grid}
                />
            </>
        )
    }
}