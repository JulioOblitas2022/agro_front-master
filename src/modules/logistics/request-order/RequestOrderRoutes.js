import React from 'react';
import { Route, Switch, useRouteMatch } from 'react-router-dom';

import { RequestOrderAdminPage } from './admin/RequestOrderAdminPage';
import { RequestOrderDetailPage } from './detail/RequestOrderDetailPage';

const RequestOrderRoutes = () => {
    const { path } = useRouteMatch();
    return (
        <Switch>
            <Route exact path={`${path}/request-order-admin`} component={RequestOrderAdminPage} />
            <Route path={`${path}/request-order`} component={RequestOrderDetailPage} />
            <Route path={`${path}/request-order/:id`} component={RequestOrderDetailPage} /> 
        </Switch>
    )
}

export default RequestOrderRoutes;
