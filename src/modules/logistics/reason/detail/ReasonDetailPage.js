import React from 'react'
import BarButtonFw from '../../../../framework/bar-button/BarButtonFw';
import FormFw from '../../../../framework/form/FormFw';
import { BasePage } from '../../../../framework/pages/BasePage';
import { getConfigReasonDetail } from './getConfigReasonDetail';
import { getModeReasonDetail } from './getModeReasonDetail';
import { getValidationReasonDetail } from './getValidationReasonDetail';

export class ReasonDetailPage extends BasePage {
    
    createPageProperties = (pageApi) => {
        return {
            title: 'DETALLE DE MOTIVO',
            validation: getValidationReasonDetail,
            mode: getModeReasonDetail
        };
    }

    renderPage(pageApi) {

        const config = getConfigReasonDetail(pageApi);

        return (
            <>
                <FormFw
                    name="form"
                    getParentApi={pageApi.getApi}
                    config={config.form}
                />
                <BarButtonFw
                    name="bar"
                    getParentApi={pageApi.getApi}
                    config={config.bar}
                />
            </>
        )
    }
}