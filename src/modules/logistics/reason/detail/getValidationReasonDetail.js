export const getValidationReasonDetail = () => {
    return {
        form: {
            description: [{ required: true, message: 'Descripción es requerido' }],
            companyId: [{ required: true, message: 'Empresa es requerido' }],
        },
        grid: {

        },
        button: {

        },
        bar: {

        }
    }
}
