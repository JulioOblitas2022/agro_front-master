import { PAGE_MODE } from '../../../../constants/constants';
import { BUTTON_DEFAULT } from '../../../../framework/bar-button/BarButtonFw';

export const getConfigReasonDetail = (pageApi) => {
    const getForm = () => pageApi.getComponent('form');

    return {
        form: {
            load: {
                queryId: 86,
                params: { reasonId: pageApi.getParamId() },
            },
            update: {
                queryId: 88,
                postLink: (resp, values) => '/logistics/reason/reason-admin'
            },
            save: {
                queryId: 89,
                postLink: (resp, values) => '/logistics/reason/reason-admin'
            },
            title: 'REGITRO DE MOTIVOS',
            fields: [
                { name: 'description', label: 'Descripcion', column: 4 },
                { type: 'select', name: 'companyId', label: 'Empresa', column: 4, load: { queryId: 554, } },

            ],
            onComponentLoad: (formApi, pageApi) => {
                if (pageApi.getPageMode() !== PAGE_MODE.NEW)
                    formApi.load();
            }
        },
        bar: {
            fields: [
                { ...BUTTON_DEFAULT.SAVE, onClick: () => getForm().save() },
                { ...BUTTON_DEFAULT.UPDATE, onClick: () => getForm().update() },
                { ...BUTTON_DEFAULT.RETURN, link: '/logistics/reason/reason-admin' }
            ],
        },
        grid: {}
    }
}
