import React from 'react';
import { Route, Switch, useRouteMatch } from 'react-router-dom';

import { ReasonAdminPage } from './admin/ReasonAdminPage';
import { ReasonDetailPage } from './detail/ReasonDetailPage';

const ReasonRoutes = () => {
    const { path } = useRouteMatch();
    return (
        <Switch>
            <Route exact path={`${path}/reason-admin`} component={ReasonAdminPage} />
            <Route path={`${path}/reason`} component={ReasonDetailPage} />
            <Route path={`${path}/reason/:id`} component={ReasonDetailPage} /> 
        </Switch>
    )
}

export default ReasonRoutes;