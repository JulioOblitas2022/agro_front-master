import React from 'react'
import { BUTTON_DEFAULT } from '../../../../framework/bar-button/BarButtonFw';
import { COLUMN_DEFAULT } from '../../../../framework/grid/GridFw';
import { getFormDefaultParams } from '../../../shared/util/getFormDefaultParams';
import { getGridServerDefaultParams } from '../../../shared/util/getGridServerDefaultParams';

export const getConfigReasonAdmin = (pageApi) => {
    const getGrid = () => pageApi.getComponent('grid');
    const getForm = () => pageApi.getComponent('form');

    return {
        form: {
            ...getFormDefaultParams(),
            title: 'BANDEJA DE MOTIVOS',
            fields: [
                { name: 'description', label: 'Descripción', column: 4 },
                { type: 'select', name: 'companyId', label: 'Empresa', column: 4, load: { queryId: 554, } },
            ]
        },
        bar: {
            fields: [
                { ...BUTTON_DEFAULT.SEARCH, onClick: () => getGrid().load({ params: getForm().getData() }) },
                { ...BUTTON_DEFAULT.CLEAR, onClick: () => getForm().reset() }
            ]
        },
        grid: {
            ...getGridServerDefaultParams({ queryLoad: 90 }),
            title: 'LISTA DE MOTIVOS',
            fields: [
                { name: 'description', label: 'Descripción' },
                { name: 'companyName', label: 'EMPRESA', },
                { ...COLUMN_DEFAULT.VIEW, label: 'Opciones', title: 'Detalle', color: 'blue', colSpan: 3, link: (value, row, index) => `/logistics/reason/reason/id-${row.reasonId}` },
                { ...COLUMN_DEFAULT.EDIT, title: 'Editar', color: 'green', colSpan: 0, link: (value, row, index) => `/logistics/reason/reason/id-${row.reasonId}/update` },
                { ...COLUMN_DEFAULT.DELETE, title: 'Eliminar', color: 'red', colSpan: 0, process: { fnOk: (result) => getGrid().load({ params: getForm().getData() }), filter: (value, row, index) => ({ queryId: 87, params: { reasonId: row.reasonId } }) } }
            ]
        }
    }
}
