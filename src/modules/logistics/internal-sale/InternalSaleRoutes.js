import React from 'react'
import { Route, Switch } from 'react-router-dom';
import { InternalSaleAdminPage } from './admin/InternalSaleAdminPage';
import { InternalSaleDetailPage } from './detail/InternalSaleDetailPage';

const InternalSaleRoutes = () => {
    return (
        <Switch>
            <Route exact path="/logistics/internal-sale/internal-sale-admin" component={InternalSaleAdminPage} />
            <Route path="/logistics/internal-sale/internal-sale" component={InternalSaleDetailPage} />
            <Route path="/logistics/internal-sale/internal-sale/:id" component={InternalSaleDetailPage} /> 
        </Switch>
    )
}

export default InternalSaleRoutes;