import { DATE_FORMAT, PAGE_MODE } from "../../../../constants/constants";
import { BUTTON_DEFAULT } from "../../../../framework/bar-button/BarButtonFw";

export const getConfigInternalSaleDetail = (pageApi) => {
    const getForm = () => pageApi.getComponent('form');
    const getGridItemDetailGrid = () => pageApi.getComponent('itemGrid.grid');
    return {
        form: {
            load:{
                queryId: 330,
                params: {internalSaleId: pageApi.getParamId()},
            },
            update:{
                queryId: 328,
                postLink: (resp, values) => '/logistics/internal-sale/internal-sale-admin',
                fnOk: (resp) => {
                    getGridItemDetailGrid().save();
                }
            },
            save:{
                queryId: 327,
                postLink: (resp, values) => '/logistics/internal-sale/internal-sale-admin',
                fnOk: (resp) => {
                    const internalSaleId = resp.dataObject?.internalSaleId;
                    if (internalSaleId){
                        getGridItemDetailGrid().save({params:{internalSaleId}});
                    }
                }
            },
            title: 'REGITRO DE PERCEPCIÓN VENTA INTERNA',
            fields:[
                {type: 'divider', label: 'DATOS PRINCIPALES'},
                {type: 'select', name:'providerId', label:'Proveedor', column: 4, load: { queryId: 159 }, search: true },
                {type: 'date', name: 'broadcastDate',label:'Fecha Emisión', format: DATE_FORMAT.DDMMYYY, inputFormat: DATE_FORMAT.YYYYMMDD, column: 4},
                {type: 'select', name:'coinId', label:'Moneda', column: 4, load: { queryId: 187 }, search: true },
                {name: 'documentNumber', label: 'Nro. Documento', column: 4},
                {type: 'number', name: 'changeType', label: 'Tipo de Cambio', column: 4},
                {type: 'select', name:'perceptionId', label:'Percepción', column: 4, load: { queryId: 196 }, search: true },
                {type: 'number', name: 'rate', label: '% Tasa', column: 4},
                {type: 'number', name: 'seatNumber', label: 'Nro. Asiento', column: 4},
                {type: 'textarea', name:'glosa', label:'Glosa', column: 12},
                
            ],
            onComponentLoad : (formApi, pageApi) =>{
                if (pageApi.getPageMode() !== PAGE_MODE.NEW)
                    formApi.load();
            }
        },
        bar: {
            fields: [
                {...BUTTON_DEFAULT.SAVE, onClick: () => getForm().save()},
                {...BUTTON_DEFAULT.UPDATE, onClick: () => getForm().update()},
                {...BUTTON_DEFAULT.RETURN, link: '/logistics/internal-sale/internal-sale-admin'}
            ],
        },
        grid: {}
    }
}