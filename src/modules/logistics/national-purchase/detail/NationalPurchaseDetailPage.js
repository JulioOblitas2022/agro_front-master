import React from 'react'
import BarButtonFw from '../../../../framework/bar-button/BarButtonFw';
import FormFw from '../../../../framework/form/FormFw';
import { BasePage } from '../../../../framework/pages/BasePage';
import { getConfigNationalPurchaseDetail } from './getConfigNationalPurchaseDetail';
import { getModeNationalPurchaseDetail } from './getModeNationalPurchaseDetail';
import { getValidationNationalPurchaseDetail } from './getValidationNationalPurchaseDetail';
import { createItemSection } from './sections/ItemSection';
import { createProcessedDocSection } from './sections/ProcessedDocSection';

export class NationalPurchaseDetailPage extends BasePage {
    
    createPageProperties = (pageApi) => {
        return {
            title: 'REGISTRO DE COMPRA',
            validation: getValidationNationalPurchaseDetail,
            mode: getModeNationalPurchaseDetail
        };
    }

    renderPage(pageApi) {

        const config = getConfigNationalPurchaseDetail(pageApi);

        return (
            <>
                <FormFw
                    name="form"
                    getParentApi={pageApi.getApi}
                    config={config.form}
                />
                
                {createProcessedDocSection(pageApi)}
                
                {createItemSection(pageApi)}

                <BarButtonFw
                    name="bar"
                    getParentApi={pageApi.getApi}
                    config={config.bar}
                />
            </>
        )
    }
}