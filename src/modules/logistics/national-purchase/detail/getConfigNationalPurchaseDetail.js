import { DATE_FORMAT, PAGE_MODE } from "../../../../constants/constants";
import { BUTTON_DEFAULT } from "../../../../framework/bar-button/BarButtonFw";
import { DateUtil } from "../../../../util/DateUtil";
import { UserUtil } from "../../../../util/UserUtil";

export const getConfigNationalPurchaseDetail = (pageApi) => {
    const getForm = () => pageApi.getComponent('form');
    const getGridItemDetailGrid = () => pageApi.getComponent('itemGrid');
    const getFormSumas = () => pageApi.getComponent('form_2');
    const getGridDocument = () => pageApi.getComponent('documentGrid');
    return {
        form: {
            autoLoad: true,
            load:{
                // validation: () => pageApi.getParamId()!=null,
                queryId: 312,
                params: {purchaseId: pageApi.getParamId() || 0},
                fnOk: (resp) => {
                    getFormSumas().load({params:{purchaseId: pageApi.getParamId()}});
                    getGridItemDetailGrid().load({params:{purchaseId: pageApi.getParamId()}});
                    getGridDocument().load({params:{purchaseId: pageApi.getParamId()}});
                }
            },
            update:{
                queryId: 313,
                postLink: (resp, values) => '/logistics/national-purchase/national-purchase-admin',
                params: {modifiedBy: UserUtil.getUserId()},
                fnOk: (resp) => {
                   getGridDocument().save();
                   getGridItemDetailGrid().save();
                   getFormSumas().save();
                }
            },
            save:{
                queryId: 310,
                params: {type: 1, createdBy: UserUtil.getUserId()}, //1  registro de compra - 2 Importacion de bienes
                postLink: (resp, values) => '/logistics/national-purchase/national-purchase-admin',
                fnOk: (resp) => {
                    const purchaseId = resp.dataObject?.purchaseId;
                    if (purchaseId){
                        getGridDocument().save({params:{purchaseId}});
                        getGridItemDetailGrid().save({params:{purchaseId}});
                        getFormSumas().save({params:{purchaseId}});
                    }
                }
            },
            title: 'REGISTRO DE COMPRA - COMPRA NACIONAL',
            fields:[
                // {type: 'divider', label: 'DATOS PRINCIPALES'},
                { type: 'select', name: 'companyId', label: 'Empresa', column: 4, load: { queryId: 554, }, search: true },
                { type: 'date', name: 'broadcastDate',label:'Fecha Emisión', format: DATE_FORMAT.DDMMYYYY, inputFormat: DATE_FORMAT.YYYYMMDD, column: 4, value: DateUtil.getDate},
                { type: 'select', name:'coinId', label:'Moneda', column: 4, load: { queryId: 187 }, search: true },  
                { type: 'select', name:'operationTypeId', label:'Tipo Operación', column: 4, load: { queryId: 77 }, value:2, search: true },
                { type: 'select', name:'providerId', label:'Proveedor', column: 4, load: { queryId: 159 }, search: true },
                { type: 'decimal', name:'exchangeType', label:'Tipo de Cambio', column: 4},
                { name:'seatNumber', label:'N° Asiento', column: 4, readOnly: true},
                { type: 'select', name:'documentTypeId', label:'Tipo Documento', column: 4, load: { queryId: 575 }, search: true,
                    onChange: (value, values, formApi) => {
                        let correlative = getForm().getFieldApi('documentTypeId').getSelectedItem()?.correlative;
                        getForm().setFieldData('documentTypeNumber', correlative);
                    }
                },
                { name:'documentTypeNumber', label:'Nro Documento', column: 4},
                { type: 'select', name:'existencesTypeId', label:'Tipo Existencia', column: 4, load: { queryId: 185 }, search: true, value: 3 },
                { type: 'select', name:'paymentConditionId', label:'Cond. Pago', column: 4, load: { queryId: 188 }, search: true, value: 1,
                    onChange: (value, values) => {
                        let fecha_emision = getForm()?.getData()?.broadcastDate;
                        if(value == 1){
                            getForm()?.setFieldData('expirationDate', fecha_emision);
                        }
                        if(value == 2){
                            getForm()?.setFieldData('expirationDate', DateUtil.stringAddDays(fecha_emision, DATE_FORMAT.YYYYMMDD, 7) );
                        }
                        if(value == 3){
                            getForm()?.setFieldData('expirationDate', DateUtil.stringAddDays(fecha_emision, DATE_FORMAT.YYYYMMDD, 15) );
                        }
                        if(value == 4){
                            getForm()?.setFieldData('expirationDate', DateUtil.stringAddDays(fecha_emision, DATE_FORMAT.YYYYMMDD, 30) );
                        }
                        if(value == 5){
                            getForm()?.setFieldData('expirationDate', DateUtil.stringAddDays(fecha_emision, DATE_FORMAT.YYYYMMDD, 45) );
                        }
                        if(value == 6){
                            getForm()?.setFieldData('expirationDate', DateUtil.stringAddDays(fecha_emision, DATE_FORMAT.YYYYMMDD, 60) );
                        }
                        if(value == 7){
                            getForm()?.setFieldData('expirationDate', DateUtil.stringAddDays(fecha_emision, DATE_FORMAT.YYYYMMDD, 90) );
                        }
                    } 
                },
                { type: 'date', name: 'expirationDate',label:'Fecha Venc.', format: DATE_FORMAT.DDMMYYYY, inputFormat: DATE_FORMAT.YYYYMMDD, column: 4},
                { type: 'select', name:'orderTypeId', label:'Opc. Compra', column: 4, load: { queryId: 265 }, search: true, value: 2 },
                { type: 'radio', name:'active', label:'Estado', column: 4, value: 1, disabled: true,
                    load: {
                        dataList: [
                            {label: 'PENDIENTE', value: 1},
                            {label: 'TERMINADO', value: 2},
                        ]
                    }
                },
                { type: 'textarea', name:'glosa', label:'Glosa', column: 12},
                
                
            ],
            onComponentLoad : (formApi, pageApi) =>{
                if (pageApi.getPageMode() !== PAGE_MODE.NEW)
                    formApi.load();
            }
        },
        bar: {
            fields: [
                {...BUTTON_DEFAULT.SAVE, onClick: () => getForm().save()},
                {...BUTTON_DEFAULT.UPDATE, onClick: () => getForm().update()},
                {...BUTTON_DEFAULT.RETURN, link: '/logistics/national-purchase/national-purchase-admin'}
            ],
        },
        grid: {}
    }
}