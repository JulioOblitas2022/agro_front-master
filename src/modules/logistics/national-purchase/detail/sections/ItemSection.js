import React from 'react'
import { BUTTON_DEFAULT } from '../../../../../framework/bar-button/BarButtonFw';
import FormFw from '../../../../../framework/form/FormFw';
import GridFw, { COLUMN_DEFAULT } from '../../../../../framework/grid/GridFw';
import { ProductModal } from '../modals/ProductModal';

export const createItemSection = (pageApi) => {

    const getGrid = () => pageApi.getComponent('itemGrid');
    const getForm = () => pageApi.getComponent('form_2');

    const calculateTotal = () => {
        let items = getGrid().getData();
        if (items){
            let total = 0;
            items.forEach(item => {
                total = total + parseFloat(item.subTotal);
                
            });
            getForm().setFieldData('grossAmount1',parseFloat(total).toFixed(2));
            getForm().setFieldData('igv1',parseFloat(total * 0.18).toFixed(2));
            getForm().setFieldData('totalAmount',parseFloat(total * 1.18).toFixed(2));
        }
    }

    const fnDelete = (value, row, index) => {
        getGrid().deleteItem(row.key);      
    }

    const itemConfig = {
        events: {
            afterAddEvent : (data) => {
                getGrid().addItem(data);
            }
        }
    }

    const ItemModalConfig = (data) =>  {
        return {
            width: 1600,
            maskClosable: false,
            component: <ProductModal name={data}  getParentApi={pageApi.getApi} config={itemConfig}  />
        }
    };


    const barHeaderConfig = {
        fields:[
            {...BUTTON_DEFAULT.ADD, label:'Agregar Item', onClick: ()=>{  pageApi.showModal(ItemModalConfig('')); }}
        ]
    }

    const gridConfig = {
            title: 'Items',
            rowId: 'purchaseRecordDetailId',
            showIndex: true,
            pagination: true,
            autoLoad: true,
            collapsible: true,
            load: {
                validation: () => pageApi.getParamId()!=null, //solo se dispara si el valor es distinto de nulo
                queryId: 315,
                params: {purchaseId: pageApi.getParamId()},
                fnOk: () => {
                    calculateTotal();
                },
                hideMessage: true,
                hideMessageError: true
            },
            save: {
                queryId: 314,
                params: {purchaseId: pageApi.getParamId()},
                hideMessage: true,
                hideMessageError: true
            },
            addItem: {
                fnAfter: (row) => {
                    calculateTotal();
                }
            },
            updateItem: {
                fnAfter: (row) => {
                    calculateTotal();
                }
            },
            fields:[
                { name:'itemTypeText', label:'Tipo Item', editable: false},
                { name:'description', label:'Item', editable: false},
                { type: 'select', name: 'unitMeasureId', label: 'Uni. Med.', labelName: 'unitMeasureDes', load: { queryId: 568, params: () => {return {masterItemId: getGrid()?.itemEditing?.masterItemId}}}, search: true, editable: true },
                { type: 'number', name:'quantity', label:'Cantidad', editable: true,
                    onChange: (value, values) => {
                        let quantity = parseInt(values['receivedAmount']);
                        let unitPriceAffected =  0;

                        if(parseInt(values['unitPriceAffected']) === 0){
                            values['unitPriceUnaffected'] = 0;
                            unitPriceAffected = parseFloat(values['unitPriceUnaffected']) 
                        } 
                        else {
                            unitPriceAffected = parseFloat(values['unitPriceAffected']);
                            values['unitPriceUnaffected'] = 0;
                        }

                        let sub_total = parseFloat(quantity * unitPriceAffected).toFixed(2);

                        getGrid()?.getFormEditable().setFieldData('subTotal', sub_total);
                    }
                },
                { type: 'decimal', name:'unitPriceAffected', label:'P.U Afecto', editable: true, 
                    onChange: (value, values) => {
                        let quantity = parseInt(values['quantity']);
                        let unitPriceAffected =  0;

                        if(parseInt(values['unitPriceAffected']) > 0){
                            values['unitPriceUnaffected'] = 0;
                            unitPriceAffected = parseFloat(values['unitPriceAffected']) 
                        } 
                        if(parseInt(values['unitPriceAffected']) == 0){
                            values['unitPriceUnaffected'] = 0;
                            unitPriceAffected = parseFloat(values['unitPriceUnaffected']) 
                        } 
                        let sub_total = parseFloat(quantity * unitPriceAffected).toFixed(2);

                        getGrid()?.getFormEditable().setFieldData('unitPriceNew', parseFloat(unitPriceAffected));
                        getGrid()?.getFormEditable().setFieldData('subTotal', sub_total);
                    }
                },
                { type: 'decimal', name:'unitPriceUnaffected', label:'P.U Inafecto', editable: true, 
                    onChange: (value, values) => {
                        let quantity = parseInt(values['quantity']);
                        let unitPriceAffected =  0;

                        if(parseInt(values['unitPriceUnaffected']) > 0){
                            values['unitPriceUnaffected'] = 0;
                            unitPriceAffected = parseFloat(values['unitPriceUnaffected']) 
                        } 
                        if(parseInt(values['unitPriceUnaffected']) == 0){
                            values['unitPriceUnaffected'] = 0;
                            unitPriceAffected = parseFloat(values['unitPriceAffected']) 
                        } 

                        let sub_total = parseFloat(quantity * unitPriceAffected).toFixed(2);
                        getGrid()?.getFormEditable().setFieldData('unitPriceNew', parseFloat(unitPriceAffected));
                        getGrid()?.getFormEditable().setFieldData('subTotal', sub_total);
                        
                    }
                },
                { type: 'decimal', name:'discount', label:'Descuento', editable: true, readOnly: true},
                { type: 'decimal', name:'unitPriceNew', label:'Nuevo P.U', editable: true, readOnly: true},
                { type: 'decimal', name:'subTotal', label:'Sub Total', editable: true, readOnly: true},
                {...COLUMN_DEFAULT.DELETE, onClick: fnDelete}
            ],
            barHeader: barHeaderConfig
        };
    
    const form_sumas = {
        layout: 'vertical',
        load: {
            //validation: () => pageApi.getParamId()!=null, //solo se dispara si el valor es distinto de nulo
            queryId: 316,
            params: {purchaseId: pageApi.getParamId() || 0},
            hideMessage: true,
            hideMessageError: true
        },
        save: {
            queryId: 317,
            params: {purchaseId: pageApi.getParamId()},
            hideMessage: true,
            hideMessageError: true
        },
        fields:[
            {type: 'decimal', name:'grossAmount1', label: 'Base Imp. Ope. Grav. y Exp.',column: 2, readOnly: true},
            {type: 'decimal', name:'grossAmount2', label:'Base Imp. Ope. Grav y Exp o no', column: 2, readOnly: true},
            {type: 'decimal', name:'grossAmount3', label: 'A.G sin Derecho a crédito fiscal',column: 2, readOnly: true},
            {type: 'decimal', name:'unaffectedAmount', label: 'Adquisiciones No grabadas',column: 2, readOnly: true},
            {type: 'decimal', name:'igv1', label:'I.G.V 1', column: 2, readOnly: true},
            {type: 'decimal', name:'igv2', label:'I.G.V 2', column: 2, readOnly: true},
            {type: 'decimal', name:'igv3', label:'I.G.V 3', column: 2, readOnly: true},
            {type: 'decimal', name:'othersAmount', label:'Otros', column: 2, readOnly: true},
            {type: 'decimal', name:'iscAmount', label:'I.S.C', column: 2, readOnly: true},
            {type: 'decimal', name:'totalAmount', label: 'Total',column: 2, readOnly: true}
        ],
    }
    return (
        <>
            <GridFw
                name="itemGrid"
                getParentApi={pageApi.getApi}
                config={gridConfig}
            />
            
            <FormFw
                name="form_2"
                getParentApi={pageApi.getApi}
                config={form_sumas}
            />
        </>
    )
}