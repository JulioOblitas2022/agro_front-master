
export const getValidationNationalPurchaseDetail = () => {
    return {
        form: {
            companyId: [{ required: true, message: 'Empresa es requerido' }],
            coinId: [{ required: true, message: 'Moneda es requerido' }],
            providerId: [{ required: true, message: 'Proveedor es requerido' }],
            documentTypeId: [{ required: true, message: 'Tipo de Documento es requerido' }],
            areaId: [{ required: true, message: 'Área es requerido' }],
            coinId: [{ required: true, message: 'Moneda es requerido' }],
            paymentConditionId: [{ required: true, message: 'Condición de Pago es requerido' }],
            reasonId: [{ required: true, message: 'Motivo es requerido' }],
            companyId: [{ required: true, message: 'Empresa es requerido' }],
            providerId: [{ required: true, message: 'Proveedor es requerido' }],
        },
        grid: {

        },
        button: {

        },
        bar: {

        }
    }
}
