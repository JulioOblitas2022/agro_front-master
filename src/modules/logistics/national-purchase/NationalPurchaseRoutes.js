import React from 'react';
import { Route, Switch, useRouteMatch } from 'react-router';

import { NationalPurchaseAdminPage } from './admin/NationalPurchaseAdminPage';
import { NationalPurchaseDetailPage } from './detail/NationalPurchaseDetailPage';

const NationalPurchaseRoutes = () => {
    const { path } = useRouteMatch();
    return (
        <Switch>
            <Route exact path={`${path}/national-purchase-admin`} component={NationalPurchaseAdminPage} />
            <Route path={`${path}/national-purchase`} component={NationalPurchaseDetailPage} />
            <Route path={`${path}/national-purchase/:id`} component={NationalPurchaseDetailPage} /> 
        </Switch>
    )
}

export default NationalPurchaseRoutes;
