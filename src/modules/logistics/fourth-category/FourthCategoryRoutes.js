import React from 'react';
import { Route, Switch, useRouteMatch } from 'react-router-dom';

import { FourthCategoryAdminPage } from './admin/FourthCategoryAdminPage';
import { FourthCategoryDetailPage } from './detail/FourthCategoryDetailPage';

const FourthCategoryRoutes = () => {
    const { path } = useRouteMatch();
    return (
        <Switch>
            <Route exact path={`${path}/fourth-category-admin`} component={FourthCategoryAdminPage} />
            <Route path={`${path}/fourth-category`} component={FourthCategoryDetailPage} />
            <Route path={`${path}/fourth-category/:id`} component={FourthCategoryDetailPage} /> 
        </Switch>
    )
}

export default FourthCategoryRoutes;