import React from 'react'
import { DATE_FORMAT } from '../../../../../constants/constants';
import FormGridFw, { FORM_GRID_MODE } from '../../../../../framework/form-grid/FormGridFw';
import FormFw from '../../../../../framework/form/FormFw';

export const createItemSection = (pageApi) => {

    const gridConfig = {
        title: 'Items',
        mode: FORM_GRID_MODE.FORM_ADD,
        collapsible: true,
        form: {
            dataOptions: {
                out: {
                    fnTransform: (values, api) => {
                        let result = [];
                        if (values['masterItemId'].length > 0){
                            let items = api.getFieldApi('masterItemId').getSelectedItem();
                            result = items.map(item => ({masterItemId: item.value, description: item.label, unitMeasuse: '', itemTypeText: item.itemTypeText}));
                        }
                        return result;
                    }
                }
            },
            fields:[
                {type:'select', name:'familyId', label:'Seleccionar Familia', column: 6, load: {queryId: 50}},
                {type:'select', name:'masterItemId', label: 'Seleccione Items',multiple:true, column: 6, load: {queryId: 191},
                    parents: [
                        {name:'familyId', paramName: 'familyId', required: true}
                    ]
                }
            ],
        },
        grid: {
            title: ' ',
            rowId: 'fourthCategoryDetailId',
            showIndex: true,
            pagination: true,
            autoLoad: true,
            collapsible: true,
            load: {
                validation: () => pageApi.getParamId()!=null, //solo se dispara si el valor es distinto de nulo
                queryId: 257,
                orderBy: 'fourthCategoryDetailId desc',
                params: {fourthCategoryId: pageApi.getParamId()}
            },
            save: {
                queryId: 258,
                params: {fourthCategoryId: pageApi.getParamId()}
            },
            fields:[
                {name:'itemTypeText', label:'Descripcion Servicio', width: 100, editable: false},
                {type: 'number', name:'afectImport', label:'Importe Afecto', width: 100, editable: true},
                {type: 'number', name:'totalImport', label:'Importe Total', width: 100, editable: true},
            ],
        }
    };
    const form_sumas = {
        load: {
            validation: () => pageApi.getParamId()!=null, //solo se dispara si el valor es distinto de nulo
            queryId: 263,
            params: {fourthCategoryId: pageApi.getParamId()}
        },
        save: {
            queryId: 264,
            params: {fourthCategoryId: pageApi.getParamId()}
        },
        fields:[
            {type: 'number', name:'seatNumber', label:'N° Asiento', column: 3},
            {type: 'number', name:'taxableTax', label: 'Imp. Imponible',column: 3},
            {type: 'number', name:'taxe', label:'Impuesto', column: 3},
            {type: 'number', name:'totalImport', label: 'Importe Total',column: 3}
        ],
    }
    return (
        <>
            <FormGridFw
                name="itemGrid"
                getParentApi={pageApi.getApi}
                config={gridConfig} 
            />
            
            <FormFw
                name="form_2"
                getParentApi={pageApi.getApi}
                config={form_sumas}
            />
        </>
    )
}