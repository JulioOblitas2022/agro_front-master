import React from 'react'
import BarButtonFw from '../../../../framework/bar-button/BarButtonFw';
import FormFw from '../../../../framework/form/FormFw';
import { BasePage } from '../../../../framework/pages/BasePage';
import { getConfigFourthCategoryDetail } from './getConfigFourthCategoryDetail';
import { getModeFourthCategoryDetail } from './getModeFourthCategoryDetail';
import { getValidationFourthCategoryDetail } from './getValidationFourthCategoryDetail';
import { createItemSection } from './sections/ItemSection';

export class FourthCategoryDetailPage extends BasePage {
    
    createPageProperties = (pageApi) => {
        return {
            title: 'DETALLE DE CUARTA CATEGORÍA',
            validation: getValidationFourthCategoryDetail,
            mode: getModeFourthCategoryDetail
        };
    }

    renderPage(pageApi) {

        const config = getConfigFourthCategoryDetail(pageApi);

        return (
            <>
                <FormFw
                    name="form"
                    getParentApi={pageApi.getApi}
                    config={config.form}
                />
                
                {createItemSection(pageApi)}

                <BarButtonFw
                    name="bar"
                    getParentApi={pageApi.getApi}
                    config={config.bar}
                />
            </>
        )
    }
}