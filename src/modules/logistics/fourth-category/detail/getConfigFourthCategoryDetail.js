import { DATE_FORMAT, PAGE_MODE } from '../../../../constants/constants';
import { BUTTON_DEFAULT } from '../../../../framework/bar-button/BarButtonFw';
import { B_SELECT_CONSTANTS } from '../../../shared/constantes/BSelectConstants';

export const getConfigFourthCategoryDetail = (pageApi) => {
    const getForm = () => pageApi.getComponent('form');
    const getGridItemDetailGrid = () => pageApi.getComponent('itemGrid.grid');
    const getFormSumas = () => pageApi.getComponent('form_2');
    return {
        form: {
            load:{
                queryId: 254,
                params: {fourthCategoryId: pageApi.getParamId()},
                fnOk: (resp) => {
                    getFormSumas().load();
                }
            },
            update:{
                queryId: 256,
                postLink: (resp, values) => '/logistics/fourth-category/fourth-category-admin',
                fnOk: (resp) => {
                    getGridItemDetailGrid().save();
                    getFormSumas().save();
                }
            },
            save:{
                queryId: 255,
                postLink: (resp, values) => '/logistics/fourth-category/fourth-category-admin',
                fnOk: (resp) => {
                    const fourthCategoryId = resp.dataObject?.fourthCategoryId;
                    if (fourthCategoryId){
                        getGridItemDetailGrid().save({params:{fourthCategoryId}});
                        getFormSumas().save({params:{fourthCategoryId}});
                    }
                }
            },
            title: 'REGITRO DE CUARTA CATEGORIA',
            fields:[
                {type: 'divider', label: 'DATOS PRINCIPALES'},
                {type: 'date', name: 'broadcastDate',label:'Fecha Emisión', format: DATE_FORMAT.DDMMYYYY, inputFormat: DATE_FORMAT.YYYYMMDD, column: 4},
                {type: 'select', name:'coinId', label:'Moneda', column: 4, load: { queryId: 187 }, search: true },
                {type: 'select', name:'operationTypeId', label:'Tipo Operacion', column: 3, load: { queryId: 77 }, search: true},
                {type: 'select', name:'providerId', label:'Proveedor', column: 4, load: { queryId: 159 }, search: true },
                {type: 'number', name:'changeType', label:'Tipo de Cambio', column: 4},
                {type: 'select', name:'documentTypeId', label:'Tipo Doc.', column: 4, load: { queryId: 195 }, search: true },
                {name:'documentNumber', label:'Nro Documento', column: 4},
                {type: 'select', name:'paymentConditionId', label:'Cond. Pago', column: 4, load: { queryId: 188 }, search: true },
                {type: 'date', name: 'expirationDate',label:'Fecha Venc.', format: DATE_FORMAT.DDMMYYYY, inputFormat: DATE_FORMAT.YYYYMMDD, column: 4},
                {type: 'select', name:'taxeFourthCategory', label:'Imp. Cuarta Cat.', column: 4, load: { queryId: 250 } },
                {type: 'textarea', name:'gloss', label:'Glosa', column: 12},
                
            ],
            onComponentLoad : (formApi, pageApi) =>{
                if (pageApi.getPageMode() !== PAGE_MODE.NEW)
                    formApi.load();
            }
        },
        bar: {
            fields: [
                {...BUTTON_DEFAULT.SAVE, onClick: () => getForm().save()},
                {...BUTTON_DEFAULT.UPDATE, onClick: () => getForm().update()},
                {...BUTTON_DEFAULT.RETURN, link: '/logistics/fourth-category/fourth-category-admin'}
            ],
        },
        grid: {}
    }
}
