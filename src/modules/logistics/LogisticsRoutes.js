import React, { lazy } from 'react'
import { Route, Switch, useRouteMatch } from 'react-router-dom'

const ProviderRoutes = lazy(() => import('./provider/ProviderRoutes'));
const PointSaleRoutes = lazy(() => import('./point-sale/PointSaleRoutes'));
const ReasonRoutes = lazy(() => import('./reason/ReasonRoutes'));
const ApproverRoutes = lazy(() => import('./approver/ApproverRoutes'));
const RequestOrderRoutes = lazy(() => import('./request-order/RequestOrderRoutes'));
const PurchaseOrderRoutes = lazy(() => import('./purchase-order/PurchaseOrderRoutes'));
const NationalPurchaseRoutes = lazy(() => import('./national-purchase/NationalPurchaseRoutes'));
const ImportGoodsRoutes = lazy(() => import('./import-goods/ImportGoodsRoutes'));
const FourthCategoryRoutes = lazy(() => import('./fourth-category/FourthCategoryRoutes'));
const CreditNoteRoutes = lazy(() => import('./credit-note/CreditNoteRoutes'));
const DebitNoteRoutes = lazy(() => import('./debit-note/DebitNoteRoutes'));
const InternalSaleRoutes = lazy(() => import('./internal-sale/InternalSaleRoutes'));
const ReportsRoutes = lazy(() => import('./reports/ReportsRoutes'));

const LogisticsRoutes = () => {
    const { path } = useRouteMatch();
    return (
        <Switch>
            <Route path={`${path}/provider`} component={ProviderRoutes} />
            <Route path={`${path}/point-sale`} component={PointSaleRoutes} />
            <Route path={`${path}/reason`} component={ReasonRoutes} />
            <Route path={`${path}/approver`} component={ApproverRoutes} />
            <Route path={`${path}/request-order`} component={RequestOrderRoutes} />
            <Route path={`${path}/purchase-order`} component={PurchaseOrderRoutes} />
            <Route path={`${path}/national-purchase`} component={NationalPurchaseRoutes} />
            <Route path={`${path}/import-goods`} component={ImportGoodsRoutes} />
            <Route path={`${path}/fourth-category`} component={FourthCategoryRoutes} />
            <Route path={`${path}/credit-note`} component={CreditNoteRoutes} />
            <Route path={`${path}/debit-note`} component={DebitNoteRoutes} />
            <Route path={`${path}/internal-sale`} component={InternalSaleRoutes} />
            <Route path={`${path}/reports`} component={ReportsRoutes} />
        </Switch>
    )
}

export default LogisticsRoutes;