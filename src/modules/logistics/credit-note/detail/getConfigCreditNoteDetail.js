import { DATE_FORMAT, PAGE_MODE } from '../../../../constants/constants';
import { BUTTON_DEFAULT } from '../../../../framework/bar-button/BarButtonFw';
import { B_SELECT_CONSTANTS } from '../../../shared/constantes/BSelectConstants';

export const getConfigCreditNoteDetail = (pageApi) => {
    const getForm = () => pageApi.getComponent('form');
    const getGridItemDetailGrid = () => pageApi.getComponent('itemGrid.grid');
    const getFormSumas = () => pageApi.getComponent('form_2');
    return {
        form: {
            load:{
                queryId: 321,
                params: {noteId: pageApi.getParamId()},
                fnOk: (resp) => {
                    getFormSumas().load();
                }

            },
            update:{
                queryId: 319,
                postLink: (resp, values) => '/logistics/credit-note/credit-note-admin',
                fnOk: (resp) => {
                    getGridItemDetailGrid().save();
                    getFormSumas().save();
                }
            },
            save:{
                queryId: 318,
                postLink: (resp, values) => '/logistics/credit-note/credit-note-admin',
                params: { type: 1 },
                fnOk: (resp) => {
                    const noteId = resp.dataObject?.noteId;
                    if (noteId){
                        getGridItemDetailGrid().save({params:{noteId}});
                        getFormSumas().save({params:{noteId}});
                    }
                }
            },
            title: 'REGITRO DE NOTA DE CRÉDITO',
            fields:[
                {type: 'divider', label: 'DATOS PRINCIPALES'},
                {type: 'date', name: 'broadcastDate',label:'Fecha Emisión', format: DATE_FORMAT.DDMMYYYY, inputFormat: DATE_FORMAT.YYYYMMDD, column: 4},
                {type: 'select', name:'coinId', label:'Moneda', column: 4, load: { queryId: 187 }, search: true },
                {type: 'select', name:'operationTypeId', label:'Tipo Operacion', column: 4, load: { queryId: 77 }, search: true, value: 1},
                {type: 'select', name:'providerId', label:'Proveedor', column: 4, load: { queryId: 159 }, search: true },
                {name:'changeType', label:'Tipo de Cambio', column: 4},
                {type: 'select', name:'documentTypeId', label:'Tipo Doc.', column: 4, load: { queryId: 195 }, search: true, value: 8 },
                {name:'documentNumber', label:'Nro Documento', column: 4},
                {type: 'select', name:'documentTypeReferencesId', label:'Tipo Doc. Ref', column: 4, load: { queryId: 195 }, search: true },
                {name:'documentNumberReferences', label:'Nro Documento', column: 4},
                {type: 'select', name:'paymentConditionId', label:'Cond. Pago', column: 4, load: { queryId: 188 }, search: true, value: 1 },
                {type: 'date', name: 'expirationDate',label:'Fecha Venc.', format: DATE_FORMAT.DDMMYYYY, inputFormat: DATE_FORMAT.YYYYMMDD, column: 4},
                {type: 'textarea', name:'gloss', label:'Glosa', column: 12},
                
            ],
            onComponentLoad : (formApi, pageApi) =>{
                if (pageApi.getPageMode() !== PAGE_MODE.NEW)
                    formApi.load();
            }
        },
        bar: {
            fields: [
                {...BUTTON_DEFAULT.SAVE, onClick: () => getForm().save()},
                {...BUTTON_DEFAULT.UPDATE, onClick: () => getForm().update()},
                {...BUTTON_DEFAULT.RETURN, link: '/logistics/credit-note/credit-note-admin'}
            ],
        },
        grid: {}
    }
}
