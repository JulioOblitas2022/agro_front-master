import React from 'react';
import { Route, Switch, useRouteMatch } from 'react-router-dom';

import { CreditNoteAdminPage } from './admin/CreditNoteAdminPage';
import { CreditNoteDetailPage } from './detail/CreditNoteDetailPage';

const CreditNoteRoutes = () => {
    const { path } = useRouteMatch();
    return (
        <Switch>
            <Route exact path={`${path}/credit-note-admin`} component={CreditNoteAdminPage} />
            <Route path={`${path}/credit-note`} component={CreditNoteDetailPage} />
            <Route path={`${path}/credit-note/:id`} component={CreditNoteDetailPage} /> 
        </Switch>
    )
}

export default CreditNoteRoutes;