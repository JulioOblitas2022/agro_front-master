import React from 'react'
import BarButtonFw, { BUTTON_DEFAULT } from '../../../../framework/bar-button/BarButtonFw';
import FormFw from '../../../../framework/form/FormFw';
import GridFw from '../../../../framework/grid/GridFw';
import { BasePage } from '../../../../framework/pages/BasePage';
import { getConfigFourthCategory } from './getConfigFourthCategory';

export class FourthCategoryPage extends BasePage {
    createPageProperties = (pageApi) => {
        return {
            title: 'CONSULTA DE RENTA DE 4TA CATEGORIA',
            barHeader: {
                fields:[
                    //{...BUTTON_DEFAULT.REPORT, label:'Export', onClick: ()=>{console.log('Export')}}
                ]
            }
        };
    }

    renderPage(pageApi) {

        const config = getConfigFourthCategory(pageApi);

        return (
            <>
                <FormFw
                    name="form"
                    getParentApi={pageApi.getApi}
                    config={config.form}
                />
                <BarButtonFw
                    name="bar"
                    getParentApi={pageApi.getApi}
                    config={config.bar}
                />
                <GridFw
                    name="grid"
                    getParentApi={pageApi.getApi}
                    config={config.grid}
                />
            </>
        )
    }
}