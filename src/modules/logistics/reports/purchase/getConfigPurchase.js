import { BUTTON_DEFAULT } from "../../../../framework/bar-button/BarButtonFw";
import { B_SELECT_CONSTANTS } from "../../../shared/constantes/BSelectConstants";
import { getFormDefaultParams } from "../../../shared/util/getFormDefaultParams";
import { getGridServerDefaultParams } from "../../../shared/util/getGridServerDefaultParams";

export const getConfigPurchase = (pageApi) => {
    const getGrid = () => pageApi.getComponent('grid');
    const getForm = () => pageApi.getComponent('form');
    
    return {
        form: {
            ...getFormDefaultParams(),
            fields:[
                {type: 'date', label:'Fecha Desde', column: 4},
                {type: 'date', label:'Fecha Hasta', column: 4},
                {type: 'select', name:'dateType', label:'Tipo fecha', column: 4, load: {queryId: 306}},
                {type: 'select', name:'coinId', label:'Moneda', column: 4, load: {queryId: 187}},
                {type: 'select', name:'state', label:'Estado', column: 4, load: {queryId: 308}},
                {type: 'select', name:'productType', label:'Tipo Producto', column: 4, load: {queryId: 185}},
                {type: 'select', name:'productType', label:'Tipo Consulta', column: 4, load: {queryId: 307}},
            ]
        },
        bar: { fields: [
            {...BUTTON_DEFAULT.SEARCH, onClick: ()=>getGrid().load({params:getForm().getData()})},
            {...BUTTON_DEFAULT.CLEAR, onClick: ()=>getForm().reset()}
        ]},
        grid: {
            ...getGridServerDefaultParams({queryLoad: 182}),
            fields:[
                {name: 'ruc', label: 'N° Ruc'},
                {name: 'provider', label: 'Proveedor', width: 200},
                {name: 'documentNumber', label: 'N° Docts'},
                {name: 'unitMeasuse', label: 'M. Nacional Imp. soles'},
                {name: 'unitMeasuse', label: 'M. Nacional Saldo Soles'},
                {name: 'unitMeasuse', label: 'M. Extranjera Imp'},
                {name: 'unitMeasuse', label: 'M. Extranjera Saldo'},
                {name: 'unitMeasuse', label: 'Expresado en MN Imp.Exp.'},
                {name: 'unitMeasuse', label: 'Expresado en MN Abo.Exp.'},
                {name: 'unitMeasuse', label: 'Expresado en MN Sal.Exp.'},
                {name: 'unitMeasuse', label: 'Expresado en ME Imp.Exp.'},
                {name: 'unitMeasuse', label: 'Expresado en ME Abo.Exp.'},
                {name: 'unitMeasuse', label: 'Expresado en ME Sal.Exp.'},
            ]
        }
    }
}
