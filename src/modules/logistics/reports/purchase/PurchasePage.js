import React from 'react'
import BarButtonFw, { BUTTON_DEFAULT } from '../../../../framework/bar-button/BarButtonFw';
import FormFw from '../../../../framework/form/FormFw';
import GridFw from '../../../../framework/grid/GridFw';
import { BasePage } from '../../../../framework/pages/BasePage';
import { getConfigPurchase } from './getConfigPurchase';

export class PurchasePage extends BasePage {
    createPageProperties = (pageApi) => {
        return {
            title: 'CONSULTA DE COMPRAS',
            barHeader: {
                fields:[
                    //{...BUTTON_DEFAULT.REPORT, label:'Export', onClick: ()=>{console.log('Export')}}
                ]
            }
        };
    }

    renderPage(pageApi) {

        const config = getConfigPurchase(pageApi);

        return (
            <>
                <FormFw
                    name="form"
                    getParentApi={pageApi.getApi}
                    config={config.form}
                />
                <BarButtonFw
                    name="bar"
                    getParentApi={pageApi.getApi}
                    config={config.bar}
                />
                <GridFw
                    name="grid"
                    getParentApi={pageApi.getApi}
                    config={config.grid}
                />
            </>
        )
    }
}