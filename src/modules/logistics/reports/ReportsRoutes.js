import React from 'react';
import { Route, Switch, useRouteMatch } from 'react-router';

import { FourthCategoryPage } from './fourth-category/FourthCategoryPage';
import { PurchasePage } from './purchase/PurchasePage';

const ReportsRoutes = () => {
    const { path } = useRouteMatch();
    return (
        <Switch>
            <Route exact path={`${path}/purchase`} component={PurchasePage} />
            <Route exact path={`${path}/fourth-category`} component={FourthCategoryPage} />
        </Switch>
    )
}

export default ReportsRoutes;
