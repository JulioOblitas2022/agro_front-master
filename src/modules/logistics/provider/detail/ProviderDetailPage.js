import React from 'react'
import BarButtonFw from '../../../../framework/bar-button/BarButtonFw';
import FormFw from '../../../../framework/form/FormFw';
import { BasePage } from '../../../../framework/pages/BasePage';
import { getConfigProviderDetail } from './getConfigProviderDetail';
import { getModeProviderDetail } from './getModeProviderDetail';
import { getValidationProviderDetail } from './getValidationProviderDetail';

export class ProviderDetailPage extends BasePage {
    
    createPageProperties = (pageApi) => {
        return {
            title: 'DETALLE DE PROVEEDORES',
            validation: getValidationProviderDetail,
            mode: getModeProviderDetail
        };
    }

    renderPage(pageApi) {

        const config = getConfigProviderDetail(pageApi);

        return (
            <>
                <FormFw
                    name="form"
                    getParentApi={pageApi.getApi}
                    config={config.form}
                />
                <BarButtonFw
                    name="bar"
                    getParentApi={pageApi.getApi}
                    config={config.bar}
                />
            </>
        )
    }
}