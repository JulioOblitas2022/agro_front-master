import { PAGE_MODE } from '../../../../constants/constants';
import { BUTTON_DEFAULT } from '../../../../framework/bar-button/BarButtonFw';
import { UserUtil } from '../../../../util/UserUtil';
import { B_SELECT_CONSTANTS } from '../../../shared/constantes/BSelectConstants';

export const getConfigProviderDetail = (pageApi) => {
    const getForm = () => pageApi.getComponent('form');

    return {
        form: {
            load: {
                queryId: 219,
                params: { providerId: pageApi.getParamId() },
            },
            update: {
                queryId: 221,
                postLink: (resp, values) => '/logistics/provider/provider-admin',
                params: { providerId: pageApi.getParamId(), modifiedBy: UserUtil.getUserId() }
            },
            save: {
                queryId: 93,
                params: { createdBy: UserUtil.getUserId()},
                postLink: (resp, values) => '/logistics/provider/provider-admin'
            },
            title: 'REGITRO DE PROVEEDORES',
            fields: [
                { type: 'divider', label: 'DATOS PRINCIPALES DEL PROVEEDOR' },
                { type: 'select', name: 'categoryId', label: 'Categoria', column: 4, load: { queryId: 197 } },
                { type: 'select', name: 'typeTaxpayerId', label: 'Tipo de Contribuyente', column: 8, load: { queryId: 183 }, search: true },
                { type: 'select', name: 'documentTypePersonId', label: 'Tipo Documento', column: 4, load: { queryId: 220 } },
                { name: 'documentNumber', label: 'Nro Documento', column: 4 },
                { name: 'name', label: 'Nombres', column: 4 },
                { name: 'lastName', label: 'Apellidos', column: 4 },
                { name: 'tradeName', label: 'Nombre Comercial', column: 4 },
                { type: 'select', name: 'type', label: 'Tipo de Registro', column: 4, load: { queryId: 251 }, value: 1 },
                { type: 'select', name: 'conditionPaymentId', label: 'Condición de Pago', column: 4, load: { queryId: 188 } },
                { type: 'select', name: 'retentionAgentId', label: 'Agente de Retencion', column: 4, load: { queryId: 250 } },
                { type: 'select', name: 'companyId', label: 'Empresa', column: 4, load: { queryId: 554, } },

                { type: 'divider', label: 'DIRECCION DE RESIDENCIA' },
                { name: 'address', label: 'Direccion', column: 4 },
                { ...B_SELECT_CONSTANTS.DEPARTAMENTO },
                { ...B_SELECT_CONSTANTS.PROVINCIA },
                { ...B_SELECT_CONSTANTS.DISTRITO },
                { name: 'phone', label: 'Teléfono', column: 4 },
                { name: 'fax', label: 'Fax', column: 4 },
                { name: 'webPage', label: 'Pagina Web', column: 4 },
                { name: 'mail', label: 'Email', column: 4 },

                { type: 'divider', label: 'DATOS DEL GIRADOR DE LETRAS' },
                { name: 'spinnerName', label: 'Girador', column: 4 },
                { name: 'spinnerAddress', label: 'Direccion Girador', column: 4 },
                { name: 'spinnerDocumentNumber', label: 'N Doc. Girador', column: 4 },
                { name: 'spinnerPhone', label: 'Teléfono Girador', column: 4 },

            ],
            onComponentLoad: (formApi, pageApi) => {
                if (pageApi.getPageMode() !== PAGE_MODE.NEW)
                    formApi.load();
            }
        },
        bar: {
            fields: [
                { ...BUTTON_DEFAULT.SAVE, onClick: () => getForm().save() },
                { ...BUTTON_DEFAULT.UPDATE, onClick: () => getForm().update() },
                { ...BUTTON_DEFAULT.RETURN, link: '/logistics/provider/provider-admin' }
            ],
        },
        grid: {}
    }
}
