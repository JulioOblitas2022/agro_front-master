import React from 'react'
import BarButtonFw, { BUTTON_DEFAULT } from '../../../../framework/bar-button/BarButtonFw';
import FormFw from '../../../../framework/form/FormFw';
import GridFw from '../../../../framework/grid/GridFw';
import { BasePage } from '../../../../framework/pages/BasePage';
import { COLUMN_DEFAULT } from '../../../../framework/grid/GridFw';
import { getFormDefaultParams } from '../../../shared/util/getFormDefaultParams';
import { getGridServerDefaultParams } from '../../../shared/util/getGridServerDefaultParams';


export class ProviderAdminPage extends BasePage {
    createPagePropertiesBase(pageApi) {
        return {
            title: 'PROVEEDORES',
            barHeader: {
                fields:[
                    {...BUTTON_DEFAULT.NEW, label:'Nuevo', link: '/logistics/provider/provider/new'}
                ]
            }
        };
    }

    renderPage(pageApi) {

        const getGrid = () => pageApi.getComponent('grid');
        const getForm = () => pageApi.getComponent('form');
    

        const form = {
            ...getFormDefaultParams(),
            title: 'BANDEJA DE PROVEEDORES',
            fields:[
                //{type: 'divider', label: 'Filtros de busqueda'},
                {name:'name', label:'Proveedor', column: 4 },
                {name:'documentNumber', label:'Nro Documento', column: 4 },
                { type: 'select', name: 'companyId', label: 'Empresa', column: 4, load: { queryId: 554, } },
            ]
        }
        
        const bar = { fields: [
            {...BUTTON_DEFAULT.SEARCH, onClick: ()=>getGrid().load({params:getForm().getData()})},
            {...BUTTON_DEFAULT.CLEAR, onClick: ()=>getForm().reset()}
        ]}

        const grid = {
            ...getGridServerDefaultParams({queryLoad: 94}),
            title: 'RELACIÓN DE PROVEEDORES',
            fields:[
                {name: 'documentText', label: 'Tipo Doc. Identidad'},
                {name: 'documentNumber', label: 'Nro Documento',},
                {name: 'name', label: 'Nombre Proveedor'},
                {name: 'tradename', label: 'NC. Proveedor',},
                {name: 'address', label: 'Direccion',},
                {name: 'phone', label: 'Telefono',},
                { name: 'companyName', label: 'Empresa', },
                {...COLUMN_DEFAULT.VIEW, link: (value, row, index) => `/logistics/provider/provider/${row.id}`, label: 'Opciones', colSpan: 3 },
                {...COLUMN_DEFAULT.EDIT, link: (value, row, index) => `/logistics/provider/provider/${row.id}/update`, colSpan: 0 },
                {...COLUMN_DEFAULT.DELETE, colSpan: 0, process: {fnOk: (result)=>getGrid().load({params:getForm().getData()}),filter: (value, row, index) => ({queryId: 222, params: {providerId: row.id}}) }}
            ]
        }

        return (
            <>
                <FormFw
                    name="form"
                    getParentApi={pageApi.getApi}
                    config={form}
                />
                <BarButtonFw
                    name="bar"
                    getParentApi={pageApi.getApi}
                    config={bar}
                />
                <GridFw
                    name="grid"
                    getParentApi={pageApi.getApi}
                    config={grid}
                />
            </>
        )
    }
}
