import React from 'react';
import { Route, Switch, useRouteMatch } from 'react-router-dom';

import { ProviderAdminPage } from './admin/ProviderAdminPage';
import { ProviderDetailPage } from './detail/ProviderDetailPage';

const ProviderRoutes = () => {
    const { path } = useRouteMatch();
    return (
        <Switch>
            <Route exact path={`${path}/provider-admin`} component={ProviderAdminPage} />
            <Route path={`${path}/provider`} component={ProviderDetailPage} />
            <Route path={`${path}/provider/:id`} component={ProviderDetailPage} /> 
        </Switch>
    )
}

export default ProviderRoutes;