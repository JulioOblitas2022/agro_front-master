export const getValidationDebitNoteDetail = () => {
    return {
        form: {
            providerId: [ {required: true, message: 'Proveedor es requerido' } ],
            coinId: [ {required: true, message: 'Moneda es requerido' } ],
            paymentConditionId: [ {required: true, message: 'Condición de Pago es requerido' } ],
            documentTypeId: [ {required: true, message: 'Tipo Documento es requerido' } ],
            operationTypeId: [ {required: true, message: 'Tipo Operacion es requerido' } ],
        },
        grid: {

        },
        button: {

        },
        bar: {

        }
    }
}
