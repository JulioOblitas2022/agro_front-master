import React from 'react'
import BarButtonFw from '../../../../framework/bar-button/BarButtonFw';
import FormFw from '../../../../framework/form/FormFw';
import { BasePage } from '../../../../framework/pages/BasePage';
import { getConfigDebitNoteDetail } from './getConfigDebitNoteDetail';
import { getModeDebitNoteDetail } from './getModeDebitNoteDetail';
import { getValidationDebitNoteDetail } from './getValidationDebitNoteDetail';
import { createItemSection } from './sections/ItemSection';

export class DebitNoteDetailPage extends BasePage {
    
    createPageProperties = (pageApi) => {
        return {
            title: 'DETALLE NOTA DE DÉBITO',
            validation: getValidationDebitNoteDetail,
            mode: getModeDebitNoteDetail
        };
    }

    renderPage(pageApi) {

        const config = getConfigDebitNoteDetail(pageApi);

        return (
            <>
                <FormFw
                    name="form"
                    getParentApi={pageApi.getApi}
                    config={config.form}
                />
                
                {createItemSection(pageApi)}

                <BarButtonFw
                    name="bar"
                    getParentApi={pageApi.getApi}
                    config={config.bar}
                />
            </>
        )
    }
}