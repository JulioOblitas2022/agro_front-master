import React from 'react'
import BarButtonFw, { BUTTON_DEFAULT } from '../../../../framework/bar-button/BarButtonFw';
import FormFw from '../../../../framework/form/FormFw';
import GridFw from '../../../../framework/grid/GridFw';
import { BasePage } from '../../../../framework/pages/BasePage';
import { DATE_FORMAT } from '../../../../constants/constants';
import { COLUMN_DEFAULT } from '../../../../framework/grid/GridFw';
import { getFormDefaultParams } from '../../../shared/util/getFormDefaultParams';
import { getGridServerDefaultParams } from '../../../shared/util/getGridServerDefaultParams';
export class DebitNoteAdminPage extends BasePage {

    createPagePropertiesBase(pageApi) {
        return {
            title: 'Nota de Débito',
            barHeader: {
                fields:[
                    {...BUTTON_DEFAULT.NEW, label:'Nuevo', link: '/logistics/debit-note/debit-note/new'}
                ]
            }
        };
    }

    renderPage(pageApi) {
        const getGrid = () => pageApi.getComponent('grid');
        const getForm = () => pageApi.getComponent('form');

        const form = {
            ...getFormDefaultParams(),
            title: 'BANDEJA NOTA DE DÉBITO',
            fields:[
                {type: 'divider', label: 'Filtros de busqueda'},
                {type: 'select', name:'providerId', label:'Proveedor', column: 4, load: { queryId: 159 }, search: true },
                {type: 'date', name: 'inputDate',label:'Fecha Desde', format: DATE_FORMAT.DDMMYYYY, column: 4},
                {type: 'date', name: 'outputDate',label:'Fecha Hasta', format: DATE_FORMAT.DDMMYYYY, column: 4},
            ]
        }

        const bar = { fields: [
            {...BUTTON_DEFAULT.SEARCH, onClick: ()=>getGrid().load({params:getForm().getData()})},
            {...BUTTON_DEFAULT.CLEAR, onClick: ()=>getForm().reset()}
        ]}

        const grid = {
            ...getGridServerDefaultParams({
                queryLoad: 320,
                params: {type: 2}
            }),
            title: 'RESULTADOS',
            fields:[
                {name: 'numberNote', label: 'N° Reg.',},
                {name: 'documentType', label: 'T.D',},
                {name: 'documentNumber', label: 'N° Doc.',},
                {type: 'date', name: 'broadcastDate', label: 'Fec. Emisión', format: DATE_FORMAT.DDMMYYYY},
                {type: 'date', name: 'expirationDate', label: 'Fec. Entrega', format: DATE_FORMAT.DDMMYYYY},
                {name: 'provider', label: 'Proveedor',},
                {name: 'coin', label: 'M.',},
                {name: 'changeType', label: 'TC.',},
                {name: 'taxableTax', label: 'Importe',},
                {name: 'taxe', label: 'Retención',},
                {name: 'totalImport', label: 'Total',},
                {...COLUMN_DEFAULT.VIEW, link: (value, row, index) => `/logistics/debit-note/debit-note/id-${row.noteId}` },
                {...COLUMN_DEFAULT.EDIT, link: (value, row, index) => `/logistics/debit-note/debit-note/id-${row.noteId}/update` },
            ]
        }
        return (
            <>
                <FormFw
                    name="form"
                    getParentApi={pageApi.getApi}
                    config={form}
                />
                <BarButtonFw
                    name="bar"
                    getParentApi={pageApi.getApi}
                    config={bar}
                />
                <GridFw
                    name="grid"
                    getParentApi={pageApi.getApi}
                    config={grid}
                />
            </>
        )
    }
}
