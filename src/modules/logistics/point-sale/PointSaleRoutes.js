import React from 'react';
import { Route, Switch, useRouteMatch } from 'react-router-dom';

import { PointSaleAdminPage } from './admin/PointSaleAdminPage';
import { PointSaleDetailPage } from './detail/PointSaleDetailPage';

const PointSaleRoutes = () => {
    const { path } = useRouteMatch();
    return (
        <Switch>
            <Route exact path={`${path}/point-sale-admin`} component={PointSaleAdminPage} />
            <Route path={`${path}/point-sale`} component={PointSaleDetailPage} />
            <Route path={`${path}/point-sale/:id`} component={PointSaleDetailPage} /> 
        </Switch>
    )
}

export default PointSaleRoutes;