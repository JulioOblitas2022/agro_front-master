import { BUTTON_DEFAULT } from "../../../../framework/bar-button/BarButtonFw";
import { COLUMN_DEFAULT } from "../../../../framework/grid/GridFw";
import { getFormDefaultParams } from "../../../shared/util/getFormDefaultParams";
import { getGridServerDefaultParams } from "../../../shared/util/getGridServerDefaultParams";

export const getConfigPointSaleAdmin = (pageApi) => {
    const getGrid = () => pageApi.getComponent('grid');
    const getForm = () => pageApi.getComponent('form');
    
    return {
        form: {
            ...getFormDefaultParams(),
            fields:[
                {name:'description', label:'Punto de venta', column: 4},
                {type:'select',name:'customerId', label:'Cliente', column: 6, load:{queryId:361}, search:true },
                { type: 'select', name: 'companyId', label: 'Empresa', column: 4, load: { queryId: 554, } },
            ]
        },
        bar: { fields: [
            {...BUTTON_DEFAULT.SEARCH, onClick: ()=>getGrid().load({params:getForm().getData()})},
            {...BUTTON_DEFAULT.CLEAR, onClick: ()=>getForm().reset()}
        ]},
        grid: {
            ...getGridServerDefaultParams({queryLoad: 81}),
            fields:[
                {name: 'customer', label: 'Cliente'},
                {name: 'description', label: 'Punto de Venta'},
                {name: 'address', label: 'Direccion'},
                {name: 'distrito', label: 'Distrito'},
                { name: 'companyName', label: 'EMPRESA', },
                {...COLUMN_DEFAULT.VIEW, link: (value, row, index) => `/logistics/point-sale/point-sale/id-${row.pointSaleId}` },
                {...COLUMN_DEFAULT.EDIT, link: (value, row, index) => `/logistics/point-sale/point-sale/id-${row.pointSaleId}/update` },
                {...COLUMN_DEFAULT.DELETE, process: {fnOk: (result)=>getGrid().load({params:getForm().getData()}),filter: (value, row, index) => ({queryId: 84, params: {pointSaleId: row.pointSaleId}}) }}
            ]
        }
    }
}
