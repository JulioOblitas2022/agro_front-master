import React from 'react'
import BarButtonFw, { BUTTON_DEFAULT } from '../../../../framework/bar-button/BarButtonFw';
import FormFw from '../../../../framework/form/FormFw';
import GridFw from '../../../../framework/grid/GridFw';
import { BasePage } from '../../../../framework/pages/BasePage';
import { getConfigPointSaleAdmin } from './getConfigPointSaleAdmin';

export class PointSaleAdminPage extends BasePage {
    createPagePropertiesBase(pageApi) {
        return {
            title: 'Puntos de Venta',
            barHeader: {
                fields:[
                    {...BUTTON_DEFAULT.NEW, label:'Nuevo', link: '/logistics/point-sale/point-sale/new'}
                ]
            }
        };
    }

    renderPage(pageApi) {

        const config = getConfigPointSaleAdmin(pageApi);

        return (
            <>
                <FormFw
                    name="form"
                    getParentApi={pageApi.getApi}
                    config={config.form}
                />
                <BarButtonFw
                    name="bar"
                    getParentApi={pageApi.getApi}
                    config={config.bar}
                />
                <GridFw
                    name="grid"
                    getParentApi={pageApi.getApi}
                    config={config.grid}
                />
            </>
        )
    }
}
