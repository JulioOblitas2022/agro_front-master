import React from 'react'
import BarButtonFw from '../../../../framework/bar-button/BarButtonFw';
import FormFw from '../../../../framework/form/FormFw';
import { BasePage } from '../../../../framework/pages/BasePage';
import { getConfigPointSaleDetail } from './getConfigPointSaleDetail';
import { getModePointSaleDetail } from './getModePointSaleDetail';
import { getValidationPointSaleDetail } from './getValidationPointSaleDetail';

export class PointSaleDetailPage extends BasePage {
    
    createPageProperties = (pageApi) => {
        return {
            title: 'Detalle Puntos de Venta',
            validation: getValidationPointSaleDetail,
            mode: getModePointSaleDetail
        };
    }

    renderPage(pageApi) {

        const config = getConfigPointSaleDetail(pageApi);

        return (
            <>
                <FormFw
                    name="form"
                    getParentApi={pageApi.getApi}
                    config={config.form}
                />
                <BarButtonFw
                    name="bar"
                    getParentApi={pageApi.getApi}
                    config={config.bar}
                />
            </>
        )
    }
}