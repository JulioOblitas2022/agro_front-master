export const getValidationPointSaleDetail = () => {
    return {
        form: {
            customerId: [ {required: true, message: 'Cliente es requerido' } ],
            cenCode: [ {required: true, message: 'Codigo CEN es requerido' } ],
            description: [ {required: true, message: 'Descripcion es requerida' } ],
            address: [ {required: true, message: 'Direccion es requerida' } ],
            departamentoId: [ {required: true, message: 'Departamento es requerido' } ],
            provinciaId: [ {required: true, message: 'Provincia es requerida' } ],
            distritoId: [ {required: true, message: 'Distrito es requerido' } ],
            companyId: [ {required: true, message: 'Empresa es requerido' } ],
        },
        grid: {

        },
        button: {

        },
        bar: {

        }
    }
}
