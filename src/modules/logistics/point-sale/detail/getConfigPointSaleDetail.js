import { PAGE_MODE } from "../../../../constants/constants";
import { BUTTON_DEFAULT } from "../../../../framework/bar-button/BarButtonFw";
import { B_SELECT_CONSTANTS } from "../../../shared/constantes/BSelectConstants";

export const getConfigPointSaleDetail = (pageApi) => {
    
    const getForm = () => pageApi.getComponent('form');
    return {
        form: {
            load:{
                queryId: 85,
                params: {pointSaleId: pageApi.getParamId()},
            },
            update:{
                queryId: 83,
                postLink: (resp, values) => '/logistics/point-sale/point-sale-admin'
            },
            save:{
                queryId: 82,
                postLink: (resp, values) => '/logistics/point-sale/point-sale-admin'
            },
            fields:[
                {type:'select',name:'customerId', label:'Cliente', column: 6, load:{queryId:361}, search:true},
                {name:'cenCode', label:'Codigo CEN', column: 6},
                {name:'description', label:'Descripcion', column: 6},
                {name:'address', label:'Direccion', column: 6},
                {...B_SELECT_CONSTANTS.DEPARTAMENTO},
                {...B_SELECT_CONSTANTS.PROVINCIA},
                {...B_SELECT_CONSTANTS.DISTRITO},
                { type: 'select', name: 'companyId', label: 'Empresa', column: 4, load: { queryId: 554, } },

                
            ],
            onComponentLoad : (formApi, pageApi) =>{
                if (pageApi.getPageMode() !== PAGE_MODE.NEW)
                    formApi.load();
            }
        },
        bar: {
            fields: [
                {...BUTTON_DEFAULT.SAVE, onClick: () => getForm().save()},
                {...BUTTON_DEFAULT.UPDATE, onClick: () => getForm().update()},
                {...BUTTON_DEFAULT.RETURN, link: '/logistics/point-sale/point-sale-admin'}
            ],
        },
        grid: {}
    }
}
