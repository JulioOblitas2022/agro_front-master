import React from 'react';
import { BasePage } from '../../../../framework/pages/BasePage';
import { TreePremissions } from './TreePremissions';
import BarButtonFw, { BUTTON_DEFAULT } from '../../../../framework/bar-button/BarButtonFw';
import { EnvConstants } from '../../../../EnvConstants';
import { LoadUtil } from '../../../../framework/util/LoadUtil';
import { getModePermissionsDetail } from './getModePermissionsDetail';

export class PermissionsDetailPage extends BasePage {

    createPageProperties = (pageApi) => {
        return {
            title: 'Asignar Permisos',
            mode: getModePermissionsDetail
        };
    }

    renderPage(pageApi) {

        const barConfig = {
            fields: [
                // {...BUTTON_DEFAULT.SAVE, onClick: () => {}},
                {...BUTTON_DEFAULT.UPDATE,
                    onClick: () => {
                        LoadUtil.loadByQueryId({
                            url: EnvConstants.GET_FW_FORM_URL_LOAD_BASE(),
                            queryId: 92,
                            params: {profileId: pageApi.getParamId(), routes: []},
                            fnOk: (resp)=>{
                                console.log(resp?.dataList);
                                // history.push('/security/permissions/permissions-admin')
                            },
                        })
                    },
                },
                {...BUTTON_DEFAULT.RETURN, link: '/security/permissions/permissions-admin'}
            ],
        }

        return (
            <>
			    <TreePremissions config={pageApi}/>
                <BarButtonFw
                    name="bar"
                    getParentApi={pageApi.getApi}
                    config={barConfig}
                />
            </>
        )
    }
}