import { Tree } from 'antd';
import React from 'react'
import { EnvConstants } from '../../../../EnvConstants';
import { LoadUtil } from '../../../../framework/util/LoadUtil';
import { PermissionUtil } from '../util/PermissionUtil';

export const TreePremissions = ({config}) => {
    const [fields, setFields] = React.useState([]);
	const [expandedKeys, setExpandedKeys] = React.useState([]);
	const [checkedKeys, setCheckedKeys] = React.useState([]);
	const [selectedKeys, setSelectedKeys] = React.useState([]);
	const [autoExpandParent, setAutoExpandParent] = React.useState(true);

	React.useEffect(() => {
		LoadUtil.loadByQueryId({
			url: EnvConstants.GET_FW_FORM_URL_LOAD_BASE(),
			queryId: 92,
			params: {profileId: config.getParamId()},
			fnOk: (resp)=>{
				if (resp.dataList && resp.dataList.length > 0) {
					let checkedKeys = [];
					let expandedKeys = [];
					resp.dataList.forEach(x => {
						if (x.checked == 1) {
							checkedKeys = [...checkedKeys, x.key]
						}
						if (x.parentId == 0) {
							expandedKeys = [...expandedKeys, x.key]
						}
					});
					setCheckedKeys(checkedKeys);
					// setExpandedKeys(expandedKeys);
					const fields = PermissionUtil.fnTransform(resp.dataList);
					setFields(fields)
				}
			}
		})
	}, []);

	const onExpand = (expandedKeys) => {
		console.log('onExpand', expandedKeys);

		setExpandedKeys(expandedKeys);
		setAutoExpandParent(false);
	};

	const onCheck = (checkedKeys) => {
		console.log('onCheck', checkedKeys);
		setCheckedKeys(checkedKeys);
	};

	const onSelect = (selectedKeys, info) => {
		console.log('onSelect', info);
		setSelectedKeys(selectedKeys);
	};

	return (
		<Tree
			checkable
			onExpand={onExpand}
			expandedKeys={expandedKeys}
			autoExpandParent={autoExpandParent}
			onCheck={onCheck}
			checkedKeys={checkedKeys}
			onSelect={onSelect}
			selectedKeys={selectedKeys}
			treeData={fields}
		/>
	)
}
