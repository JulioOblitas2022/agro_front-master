import { EnvConstants } from '../../../../EnvConstants';
import { RequestUtil } from '../../../../util/RequestUtil';
import { PermissionUtil } from '../util/PermissionUtil';

export const getConfigPermissionsDetail = (pageApi) => {

    let state = {};

    const load = async() => {
        let r = await RequestUtil.postData( EnvConstants.GET_FW_FORM_URL_LOAD_BASE(), {queryId: 92, params: {profileId: pageApi.getParamId()}})
        if (r.dataList && r.dataList.length > 0) {
            const fields = PermissionUtil.fnTransform(r.dataList);
            state['fields'] = fields;
        }
	}

    // await load();

    return state;
}
