
export class PermissionUtil {
    static fnTransform(fields){
        if (fields.length > 0) {
			const menuParents = fields.filter((menu, index) => menu.type === 1);
			menuParents.forEach(element => {
				let tempChildren = fields.filter((child, idx) => child.parentId === element.key);
				//
				tempChildren.sort((a, b) => a.numberOrder - b.numberOrder);
				if (tempChildren.length > 0) {
					element['children'] = tempChildren;
				}
			});
			const menuResult = menuParents.filter(e => (!e.parentId || e.parentId === '0') && e.children?.length > 0);
			//
			menuResult.sort((a, b) => a.numberOrder - b.numberOrder);
			return menuResult || [];
		}
		return [];
    }
}
