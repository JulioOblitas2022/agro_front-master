import React from 'react'
import { Switch, Route, useRouteMatch } from "react-router-dom";

import { PermissionsAdminPage } from './admin/PermissionsAdminPage';
import { PermissionsDetailPage } from './detail/PermissionsDetailPage';

const PermissionsRoutes = () => {
    const { path } = useRouteMatch();
    return (
        <Switch>
            <Route exact path={`${path}/permissions-admin`} component={PermissionsAdminPage} />
            <Route path={`${path}/permissions`} component={PermissionsDetailPage} />
            <Route path={`${path}/permissions/:id`} component={PermissionsDetailPage} />
        </Switch>
    )
}
export default PermissionsRoutes;