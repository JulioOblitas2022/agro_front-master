import React, { useState } from 'react';
import { BasePage } from '../../../../framework/pages/BasePage';
import GridFw from '../../../../framework/grid/GridFw';
import { getConfigPermissionsAdmin } from './getConfigPermissionsAdmin';
import { BUTTON_DEFAULT } from '../../../../framework/bar-button/BarButtonFw';
import { ReportUtil } from '../../../../util/ReportUtil';

export class PermissionsAdminPage extends BasePage {
    
    createPageProperties = (parent) => {
        return {
            title: 'Permisos',
            barHeader: {
                fields: [
                    {...BUTTON_DEFAULT.REPORT, label:'pdf', onClick: ()=> {  
                        ReportUtil.donwloadPdf('persona',{
                            P_JSON: '{"StartRow": 0, "EndRow": 100}'
                        }); 
                    }},
                    {...BUTTON_DEFAULT.REPORT, label:'xls', onClick: ()=> {  ReportUtil.downloadXls('test1'); }},
                    {...BUTTON_DEFAULT.REPORT, label:'html', onClick: ()=> {  ReportUtil.openWindowHtml('test1'); }}
                ]
            }
        };
    }

    renderPage(parent) {
        const config = getConfigPermissionsAdmin(parent);
        
        return (
            <>
                <GridFw
                    name="grid"
                    handleAddComponent={parent.handleAddComponent}
                    config={config.grid}
                />
            </>
        )
    }
}
