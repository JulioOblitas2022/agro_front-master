import { COLUMN_DEFAULT } from "../../../../framework/grid/GridFw"
import { getGridServerDefaultParams } from "../../../shared/util/getGridServerDefaultParams"

export const getConfigPermissionsAdmin = (parent) => {
    return {
        form: {},
        bar: {},
        grid: {
            ...getGridServerDefaultParams({queryLoad: 4}),
            fields:[
                {name: 'description', label: 'Descripcion', sorter: {multiple: 1}},
                {name: 'shortDesc', label: 'Desc. Corta'},
                {...COLUMN_DEFAULT.VIEW, link: (value, row, index) => `/security/permissions/permissions/id-${row.profileId}` },
                {...COLUMN_DEFAULT.EDIT, link: (value, row, index) => `/security/permissions/permissions/id-${row.profileId}/update` },
            ]
        }
    }
}