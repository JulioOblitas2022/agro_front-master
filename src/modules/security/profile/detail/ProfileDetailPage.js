import React from 'react';
import BarButtonFw from '../../../../framework/bar-button/BarButtonFw';
import FormFw from '../../../../framework/form/FormFw';
import { BasePage } from '../../../../framework/pages/BasePage';
import { getConfigProfileDetail } from './getConfigProfileDetail';
import { getModeProfileDetail } from './getModeProfileDetail';
import { getValidationProfileDetail } from './getValidationProfileDetail';


export class ProfileDetailPage extends BasePage {
    
    createPageProperties = (pageApi) => {
        return {
            title: 'Detalle Perfiles',
            validation: getValidationProfileDetail,
            mode: getModeProfileDetail
        };
    }

    renderPage(pageApi) {
        const config = getConfigProfileDetail(pageApi);
        
        return (
            <>
                <FormFw
                    name="form"
                    getParentApi={pageApi.getApi}
                    config={config.form}
                />
                <BarButtonFw
                    name="bar"
                    getParentApi={pageApi.getApi}
                    config={config.bar}
                />
            </>
        )
    }
}


