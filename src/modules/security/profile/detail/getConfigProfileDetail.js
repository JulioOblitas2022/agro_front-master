import { PAGE_MODE } from "../../../../constants/constants";
import { BUTTON_DEFAULT } from "../../../../framework/bar-button/BarButtonFw";


export const getConfigProfileDetail = (parent) => {
    const getForm = () => parent.getComponent('form');

    return {
        form: {
            load:{
                queryId: 17,
                params: {profileId: parent.getParamId()},
            },
            update:{
                queryId: 16,
                postLink: (resp, values) => '/security/profile/profile-admin'
            },
            save:{
                queryId: 7,
                postLink: (resp, values) => '/security/profile/profile-admin'
            },
            fields:[
                {name:'description', label:'Descripcion', column: 6},
                {name:'shortDesc', label:'Desc. Corta', column: 6},
                
            ],
            onComponentLoad : (formApi, pageApi) =>{
                if (pageApi.getPageMode() !== PAGE_MODE.NEW)
                    formApi.load();
            }
        },
        bar: {
            fields: [
                {...BUTTON_DEFAULT.SAVE, onClick: () => getForm().save()},
                {...BUTTON_DEFAULT.UPDATE, onClick: () => getForm().update()},
                {...BUTTON_DEFAULT.RETURN, link: '/security/profile/profile-admin'}
            ],
        },
        grid: {}
    };
}