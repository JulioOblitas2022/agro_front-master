
export const getValidationProfileDetail = () => {
    return {
        form: {
            description: [ {required: true, message: 'Descripcion es requerida' } ],
            shortDesc: [ {required: true, message: 'Desc. Corta es requerida' } ],
        },
        grid: {

        },
        button: {

        },
        bar: {

        }
    }
}
