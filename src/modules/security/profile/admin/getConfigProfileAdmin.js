import { BUTTON_DEFAULT } from "../../../../framework/bar-button/BarButtonFw";
import { COLUMN_DEFAULT } from "../../../../framework/grid/GridFw";
import { getFormDefaultParams } from "../../../shared/util/getFormDefaultParams";
import { getGridServerDefaultParams } from "../../../shared/util/getGridServerDefaultParams";


export const getConfigProfileAdmin = (parent) => {
    const getGrid = () => parent.getComponent('grid');
    const getForm = () => parent.getComponent('form');
    return {
        form: {
            ...getFormDefaultParams(),
            fields:[
                {name:'description', label:'Descripcion', column: 6},
                {name:'shortDesc', label:'Desc. Corta', column: 6},
            ]
        },
        bar: { fields: [
            {...BUTTON_DEFAULT.SEARCH, onClick: ()=>getGrid().load({params:getForm().getData()})},
            {...BUTTON_DEFAULT.CLEAR, onClick: ()=>getForm().reset()}
        ]},
        grid: {
            ...getGridServerDefaultParams({queryLoad: 4}),
            fields:[
                {name: 'description', label: 'Descripcion', sorter: {multiple: 1}},
                {name: 'shortDesc', label: 'Desc. Corta', sorter: {multiple: 2}},
                {...COLUMN_DEFAULT.VIEW, link: (value, row, index) => `/security/profile/profile/id-${row.profileId}` },
                {...COLUMN_DEFAULT.EDIT, link: (value, row, index) => `/security/profile/profile/id-${row.profileId}/update` },
                {...COLUMN_DEFAULT.DELETE, process: {fnOk: (result)=>getGrid().load({params:getForm().getData()}),filter: (value, row, index) => ({queryId: 8, params: {profileId: row.profileId}}) }}
            ]
        }
    }
}