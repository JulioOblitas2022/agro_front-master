import React from 'react'
import { Switch, Route, useRouteMatch } from "react-router-dom";

import { ProfileAdminPage } from './admin/ProfileAdminPage';
import { ProfileDetailPage } from './detail/ProfileDetailPage';

const ProfileRoutes = () => {
    const { path } = useRouteMatch();
    return (
        <Switch>
            <Route exact path={`${path}/profile-admin`} component={ProfileAdminPage} />
            <Route path={`${path}/profile`} component={ProfileDetailPage} />
            <Route path={`${path}/profile/:id`} component={ProfileDetailPage} />
        </Switch>
    )
}
export default ProfileRoutes;