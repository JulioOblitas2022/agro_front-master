import React, { lazy } from 'react';
import { Switch, Route, useRouteMatch } from "react-router-dom";

const UserRoutes = lazy(() => import('./user/UserRoutes'));
const ProfileRoutes = lazy(() => import('./profile/ProfileRoutes'));
const PermissionsRoutes = lazy(() => import('./permissions/PermissionsRoutes'));

const SecurityRoutes = () => {
    const { path } = useRouteMatch();
    return (
        <Switch>
            <Route path={`${path}/user`} component={UserRoutes} />
            <Route path={`${path}/profile`} component={ProfileRoutes} />
            <Route path={`${path}/permissions`} component={PermissionsRoutes} />
        </Switch>
    )
}
export default SecurityRoutes;