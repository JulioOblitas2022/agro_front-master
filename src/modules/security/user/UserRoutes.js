import React from 'react'
import { Switch, Route, useRouteMatch } from "react-router-dom"

import { UserAdminPage } from './admin/UserAdminPage'
import { UserDetailPage } from './detail/UserDetailPage'

const UserRoutes = () => {
    const { path } = useRouteMatch();
    return (
        <Switch>
            <Route exact path={`${path}/user-admin`} component={UserAdminPage} />
            <Route path={`${path}/user`} component={UserDetailPage} />
            <Route path={`${path}/user/:id`} component={UserDetailPage} />
        </Switch>
    )
}
export default UserRoutes;