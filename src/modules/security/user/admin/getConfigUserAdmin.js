import { BUTTON_DEFAULT } from "../../../../framework/bar-button/BarButtonFw";
import { COLUMN_DEFAULT } from "../../../../framework/grid/GridFw";
import { getFormDefaultParams } from "../../../shared/util/getFormDefaultParams";


export const getConfigUserAdmin = (parent) => {
    const getGrid = () => parent.getComponent('grid');
    const getForm = () => parent.getComponent('form');
    return {
        form: {
            ...getFormDefaultParams(),
            fields:[
                {name:'name', label:'Nombre', column: 3},
                {name:'lastName', label:'Apellido', column: 3},
                {name:'username', label:'Usuario', column: 3},
                {type: 'select', name:'profileId', label:'PROFILE', column: 3, load: {queryId: 71}},
            ]
        },
        bar: { fields: [
            {...BUTTON_DEFAULT.SEARCH, onClick: ()=>getGrid().load({params:getForm().getData()})},
            {...BUTTON_DEFAULT.CLEAR, onClick: ()=>getForm().reset()}
        ]},
        grid: {
            showIndex: true,
            autoLoad: true,
            pagination: {server: true},
            load: {
                queryId: 3,
                orderBy: "1"
            },
            // ...getGridServerDefaultParams({queryLoad: 3}),
            fields:[
                {name: 'name', label: 'Nombres', sorter: {multiple: 1}},
                {name: 'lastName', label: 'Apellidos', sorter: {multiple: 2}},
                {name: 'email', label: 'Correo',},
                {name: 'username', label: 'Usuario',},
                {...COLUMN_DEFAULT.VIEW, link: (value, row, index) => `/security/user/user/id-${row.userId}` },
                {...COLUMN_DEFAULT.EDIT, link: (value, row, index) => `/security/user/user/id-${row.userId}/update` },
                // {...COLUMN_DEFAULT.DELETE, process: {fnOk: (result)=>getGrid().load({params:getForm().getData()}),filter: (value, row, index) => ({queryId: 9, params: {userId: row.userId}}) }}
            ]
        }
    }
}