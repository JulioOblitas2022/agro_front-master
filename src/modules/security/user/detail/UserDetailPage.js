import React from 'react';
import BarButtonFw from '../../../../framework/bar-button/BarButtonFw';
import FormFw from '../../../../framework/form/FormFw';
import { BasePage } from '../../../../framework/pages/BasePage';
import { getConfigUserDetail } from './getConfigUserDetail';
import { getModeUserDetail } from './getModeUserDetail';
import { getValidationUserDetail } from './getValidationUserDetail';


export class UserDetailPage extends BasePage {
    
    createPageProperties = (pageApi) => {
        return {
            title: 'Detalle Usuario',
            validation: getValidationUserDetail,
            mode: getModeUserDetail
        };
    }

    renderPage(pageApi) {
        const config = getConfigUserDetail(pageApi);
          
        return (
            <>
                <FormFw
                    name="form"
                    getParentApi={pageApi.getApi}
                    config={config.form}
                />
                <BarButtonFw
                    name="bar"
                    getParentApi={pageApi.getApi}
                    config={config.bar}
                />
            </>
        )
    }
}


