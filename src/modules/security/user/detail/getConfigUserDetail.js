import { PAGE_MODE } from '../../../../constants/constants';
import { BUTTON_DEFAULT } from '../../../../framework/bar-button/BarButtonFw';
import { UserUtil } from '../../../../util/UserUtil';

export const getConfigUserDetail = (parent) => {
    const getForm = () => parent.getComponent('form');
    // const getDataPageMode = () => parent.getDataPageMode(getModeUserDetail());

    return {
        form: {
            load:{
                queryId: 6,
                params: {userId: parent.getParamId()},
            },
            update:{
                queryId: 15,
                postLink: (resp, values) => '/security/user/user-admin',
                params: {modifiedBy: UserUtil.getUserId()}
            },
            save:{
                queryId: 5,
                postLink: (resp, values) => '/security/user/user-admin',
                params: {createdBy: UserUtil.getUserId()}
            },
            fields:[
                {name:'name', label:'Nombre', column: 4},
                {name:'lastName', label:'Apellido', column: 4},
                {name:'username', label:'Usuario', column: 4},
                {name:'email', label:'Correo', column: 4},
                {name:'cardId', label:'Id Card', column: 4},
                {name:'password', label:'Clave', column: 4},
                {type: 'select', name:'profileId', label:'PROFILE', column: 6, load: {queryId: 71}},
                
            ],
            // validations: getValidationUserDetail()['form'],
            // mode : getDataPageMode(),
            onComponentLoad : (formApi, pageApi) =>{
                if (pageApi.getPageMode() !== PAGE_MODE.NEW)
                    formApi.load();
            }
        },
        bar: {
            fields: [
                {...BUTTON_DEFAULT.SAVE, onClick: () => getForm().save()},
                {...BUTTON_DEFAULT.UPDATE, onClick: () => getForm().update()},
                {...BUTTON_DEFAULT.RETURN, link: '/security/user/user-admin'}
            ],
            // mode : getDataPageMode()
        },
        grid: {}
    };
}