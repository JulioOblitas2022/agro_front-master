
export const getValidationUserDetail = () => {
    return {
        form: {
            name: [ {required: true, message: 'Nombre es requerido' } ],
            lastName: [ {required: true, message: 'Apellido es requerido' } ],
            username: [ {required: true, message: 'Usuario es requerido' } ],
            email: [ {required: true, message: 'Correo es requerido'} ],
            cardId: [ {required: true, message: 'Identificacion es requerida'} ],
            password: [ {required: true, message: 'Clave es requerida'} ],
            profileId: [ {required: true, message: 'Perfil es requerido'} ],
        },
        grid: {

        },
        button: {

        },
        bar: {

        }
    }
}
