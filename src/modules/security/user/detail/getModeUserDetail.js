import { MODE_SHORTCUT } from "../../../../constants/constants"

export const getModeUserDetail = () => {
    return {
        new: {
            bar: {
                show: ['save', 'return']
            }
        },
        update:{
            form: {
                read: ['username'],
                hide: ['password'],
            },
            bar: {
                show: ['update', 'return']
            }
        },
        read:{
            form: {
                read: [MODE_SHORTCUT.ALL],
                hide: ['password'],
            },
            bar: {
                show: ['return']
            }
        }
    }
}
