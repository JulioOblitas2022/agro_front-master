import React from 'react'
import {
    Switch,
    Route
  } from "react-router-dom"
import { LoginPage } from './login/LoginPage';
import { PasswordResetPage } from './password-reset/PasswordResetPage';

export const AuthenticationRoutes = () => {
    return (
        <Switch>
            <Route exact path="/auth/login" component={LoginPage} />
            <Route exact path="/auth/password-reset" component={PasswordResetPage} />
        </Switch>
    )
}