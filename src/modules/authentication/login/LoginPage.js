import React, { useState } from 'react';
import { useDispatch } from 'react-redux';
import { Link } from 'react-router-dom';
import { useForm } from '../../../hooks/useForm';
import { startLogin } from '../../../services/user.service';
import './loginStyle.css';

export const LoginPage = () => {
	const dispatch = useDispatch();
	const [usernameNotValue, setUsernameNotValue] = useState(false);
	const [passwordNotValue, setPasswordNotValue] = useState(false);
	const [ loginForm, handleInputChange, reset ] = useForm( {username: '', password: '', rememberMe: true} );

	const login = (e) => {
		e.preventDefault();
		if (loginForm['username'] !== '' && loginForm['password'] !== '') {
			dispatch(startLogin(loginForm));
		} else {
			if (loginForm.username === '') {
				setUsernameNotValue(true);
			}
			if (loginForm.password === '') {
				setPasswordNotValue(true);
			}
		}
	}
	
	React.useEffect(() => {
		// setRemember(true);
	}, [loginForm])
	
    return (
		<>
			<div className="body-login">
				<div className="login__wrapper">
					<div className="login__form">
					<div className="login__content">
						<h1>Login</h1>
						<form onSubmit={login}>
							<div className="group">
								<input className={usernameNotValue ? 'error-input' : ''} type="text" name="username" onChange={ handleInputChange } autoComplete="off" />
								<span className="highlight"></span>
								<span className="bar"></span>
								<label>Username</label>
							</div>
							
							<div className="group">
								<input className={passwordNotValue ? 'error-input' : ''} type="password" name="password" onChange={ handleInputChange } />
								<span className="highlight"></span>
								<span className="bar"></span>
								<label>Password</label>
							</div>
							
							<div className="login__options">
								<input type="checkbox" name="rememberMe" checked={loginForm.rememberMe}  onChange={ handleInputChange }/>
								<label htmlFor="remember_me"> Remember me </label>
								
								<Link to="/auth/password-reset"> Forgot password? </Link>
							</div>
							
							<button type="submit" className="login__button">Login</button>
							
							<p>
								Dont got an account yet?
								<br />
								<Link to="/auth/login"> Create an account </Link>
							</p>
						</form>
					</div>
					</div>
				</div>
			</div>
		</>
    )
}
