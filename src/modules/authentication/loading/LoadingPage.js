import React from 'react';
import { Spin } from 'antd';
import './loadingStyle.css';

export const LoadingPage = () => {
    return (
        <div className="loadingPage">
            <Spin tip="Loading..." size="large" />
        </div>
    )
}
