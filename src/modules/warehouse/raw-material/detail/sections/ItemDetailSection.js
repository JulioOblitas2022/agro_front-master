import React from 'react';
import { DATE_FORMAT } from '../../../../../constants/constants';
import { BUTTON_DEFAULT } from '../../../../../framework/bar-button/BarButtonFw';
import GridFw, { COLUMN_DEFAULT } from '../../../../../framework/grid/GridFw';
import { ProductModal } from '../modals/ProductModal';

export const createItemDetailSection = (pageApi) => {

    const getGrid = () => pageApi.getComponent('itemDetailSectionGrid');

    const fnDelete = (value, row, index) => {
        getGrid().deleteItem(row.key);      
    }

    const itemConfig = {
        events: {
            afterAddEvent : (data) => {
                getGrid().addItem(data);
            }
        }
    }

    const ItemModalConfig = (data) =>  {
        return {
            width: 1600,
            maskClosable: false,
            component: <ProductModal name={data}  getParentApi={pageApi.getApi} config={itemConfig}  />
        }
    };


    const barHeaderConfig = {
        fields:[
            {...BUTTON_DEFAULT.ADD, label:'Agregar Item', onClick: ()=>{  pageApi.showModal(ItemModalConfig('')); }}
        ]
    }

    const gridConfig = {
        title: 'Otros items registrados',
        rowId: 'rawMaterialDetailId',
        showIndex: true,
        pagination: true,
        autoLoad: true,
        collapsible: true,
        load: {
            validation: () => pageApi.getParamId() != null, //solo se dispara si el valor es distinto de nulo
            queryId: 164,
            orderBy: 'rawMaterialDetailId desc',
            params: { warehouseIncomeId: pageApi.getParamId() },
            hideMessage: true, 
            hideMessageError: true
        },
        save: {
            queryId: 165,
            params: { warehouseIncomeId: pageApi.getParamId() },
            hideMessage: true, 
            hideMessageError: true
        },
        fields: [
            { name: 'description', label: 'Item', editable: false },
            { type: 'select', name: 'unitMeasureId', label: 'Uni. Med.', labelName: 'unitMeasureIdDes', load: { queryId: 568, params: () => {return {masterItemId: getGrid()?.itemEditing?.masterItemId}}}, search: true, editable: true },
            { type: 'number', name: 'quantity', label: 'Cantidad', editable: true},
            { name: 'lot', label: 'Nro Lote', editable: true},
            { type: 'select', name: 'departamentoId', label: 'Departamento', labelName: 'departamentoIdDes', load: { queryId: 78 }, search: true, editable: true },
            {
                type: 'select', name: 'provinciaId', label: 'Provincia', labelName: 'provinciaIdDes', load: { queryId: 79 }, search: true, editable: true,
                parents: [
                    { name: 'departamentoId', paramName: 'departamentoId', required: true }
                ]
            },
            {
                type: 'select', name: 'distritoId', label: 'Distrito', labelName: 'distritoIdDes', load: { queryId: 80 }, search: true, editable: true,
                parents: [
                    { name: 'provinciaId', paramName: 'provinciaId', required: true }
                ]
            },
            { name: 'details', label: 'Fondo/Hacienda/Lugar Origen', editable: true},
            { type: 'date', name: 'receptionTime', label: 'Hora Recepcion', editable: true, format: DATE_FORMAT.HHmm, inputFormat: DATE_FORMAT.HHmm},
            {...COLUMN_DEFAULT.DELETE, onClick: fnDelete}
        ],
        barHeader: barHeaderConfig
    };

    return (
        <GridFw
            name="itemDetailSectionGrid"
            getParentApi={pageApi.getApi}
            config={gridConfig}
        />
    )
}