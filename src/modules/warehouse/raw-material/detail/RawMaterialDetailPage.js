import React from 'react'
import BarButtonFw from '../../../../framework/bar-button/BarButtonFw';
import FormFw from '../../../../framework/form/FormFw';
import { BasePage } from '../../../../framework/pages/BasePage';
import { getConfigRawMaterialDetail } from './getConfigRawMaterialDetail';
import { getModeRawMaterialDetail } from './getModeRawMaterialDetail';
import { getValidationRawMaterialDetail } from './getValidationRawMaterialDetail';
import { createItemDetailSection } from './sections/ItemDetailSection';

export class RawMaterialDetailPage extends BasePage {
    createPageProperties = (pageApi) => {
        return {
            title: 'MATERIA PRIMA',
            validation: getValidationRawMaterialDetail,
            mode: getModeRawMaterialDetail
        };
    }

    renderPage(pageApi) {

        const config = getConfigRawMaterialDetail(pageApi);

        return (
            <>
                <FormFw
                    name="form"
                    getParentApi={pageApi.getApi}
                    config={config.form}
                />
                
                {createItemDetailSection(pageApi)}

                <BarButtonFw
                    name="bar"
                    getParentApi={pageApi.getApi}
                    config={config.bar}
                />
            </>
        )
    }
}
