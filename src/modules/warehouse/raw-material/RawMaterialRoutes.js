import React from 'react';
import { Switch, Route, useRouteMatch } from 'react-router-dom';

import { RawMaterialAdminPage } from './admin/RawMaterialAdminPage';
import { RawMaterialDetailPage } from './detail/RawMaterialDetailPage';

export const RawMaterialRoutes = () => {
    const { path } = useRouteMatch();
    return (
        <Switch>
            <Route exact path={`${path}/raw-material-admin`} component={RawMaterialAdminPage} />
            <Route path={`${path}/raw-material`} component={RawMaterialDetailPage} />
            <Route path={`${path}/raw-material/:id`} component={RawMaterialDetailPage} />
        </Switch>
    )
}

export default RawMaterialRoutes;
