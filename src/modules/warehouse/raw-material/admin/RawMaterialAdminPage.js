import React from 'react'
import BarButtonFw, { BUTTON_DEFAULT } from '../../../../framework/bar-button/BarButtonFw';
import FormFw from '../../../../framework/form/FormFw';
import GridFw from '../../../../framework/grid/GridFw';
import { BasePage } from '../../../../framework/pages/BasePage'
import { DATE_FORMAT } from "../../../../constants/constants";
import { COLUMN_DEFAULT } from "../../../../framework/grid/GridFw";
import { getFormDefaultParams } from "../../../shared/util/getFormDefaultParams";
import { getGridServerDefaultParams } from "../../../shared/util/getGridServerDefaultParams";

export class RawMaterialAdminPage extends BasePage {
    
    createPagePropertiesBase(pageApi) {
        return {
            title: 'BANDEJA DE INGRESO - MATERIA PRIMA',
            barHeader: {
                fields:[
                    {...BUTTON_DEFAULT.NEW, label:'Nuevo', link: '/warehouse/raw-material/raw-material/new'}
                ]
            }
        };
    }

    renderPage(pageApi) {

        const getGrid = () => pageApi.getComponent('grid');
        const getForm = () => pageApi.getComponent('form');

        const form = {
            ...getFormDefaultParams(),
            fields: [
                { name: 'documentTypeNumber', label: 'Nro. Documento', column: 4 },
                { type: 'select', name: 'providerId', label: 'Proveedor', column: 4, search: true, load: { queryId: 159 } },
                { type: 'select', name: 'warehouseId', label: 'Almacén', column: 4, load: { queryId: 34 } },
                { type: 'date', name: 'dateIni', label: 'Fecha Desde', format: DATE_FORMAT.DDMMYYYY, inputFormat: DATE_FORMAT.YYYYMMDD, column: 4},
                { type: 'date', name: 'dateEnd', label: 'Fecha Hasta', format: DATE_FORMAT.DDMMYYYY, inputFormat: DATE_FORMAT.YYYYMMDD, column: 4},
                { type: 'select', name: 'companyId', label: 'Empresa', column: 4, load: { queryId: 554, } },
            ]
        }

        const bar = {
            fields: [
                { ...BUTTON_DEFAULT.SEARCH, onClick: () => getGrid().load({ params: getForm().getData() }) },
                { ...BUTTON_DEFAULT.CLEAR, onClick: () => getForm().reset() }
            ]
        }

        const grid = {
            ...getGridServerDefaultParams({ queryLoad: 160 }),
            fields: [
                { name: 'documentType', label: 'Tipo Documento' },
                { name: 'documentTypeNumber', label: 'Nro. Documento' },
                { type: 'date', name: 'docDate', label: 'Fec. Documento', format: DATE_FORMAT.DDMMYYYY },
                { name: 'provider', label: 'Proveedor' },
                { type: 'date',  name: 'entryDate', label: 'Fec. Ingreso', format: DATE_FORMAT.DDMMYYYY },
                { name: 'warehouse', label: 'Almacen' },
                { name: 'documentTypeRef', label: 'Tipo Doc. Ref.' },
                { name: 'documentRefNumber', label: 'Nro. Doc. Ref.' },
                { name: 'companyName', label: 'Empresa', },
                { ...COLUMN_DEFAULT.VIEW, link: (value, row, index) => `/warehouse/raw-material/raw-material/${row.warehouseIncomeId}`, label: 'Opciones', colSpan: 2 },
                { ...COLUMN_DEFAULT.EDIT, link: (value, row, index) => `/warehouse/raw-material/raw-material/${row.warehouseIncomeId}/update`, colSpan: 0 },
                // {...COLUMN_DEFAULT.DELETE, process: {fnOk: (result)=>getGrid().load({params:getForm().getData()}),filter: (value, row, index) => ({queryId: 0, params: {warehouseIncomeId: row.warehouseIncomeId}}) }}
            ]
        }

        return (
            <>
                <FormFw
                    name="form"
                    getParentApi={pageApi.getApi}
                    config={form}
                />
                <BarButtonFw
                    name="bar"
                    getParentApi={pageApi.getApi}
                    config={bar}
                />
                <GridFw
                    name="grid"
                    getParentApi={pageApi.getApi}
                    config={grid}
                />
            </>
        )
    }
}
