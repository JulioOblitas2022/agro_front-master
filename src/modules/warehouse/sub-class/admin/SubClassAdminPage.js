import React from 'react'
import BarButtonFw, { BUTTON_DEFAULT } from '../../../../framework/bar-button/BarButtonFw';
import FormFw from '../../../../framework/form/FormFw';
import GridFw from '../../../../framework/grid/GridFw';
import { BasePage } from '../../../../framework/pages/BasePage';
import { COLUMN_DEFAULT } from "../../../../framework/grid/GridFw";
import { B_SELECT_CONSTANTS } from "../../../shared/constantes/BSelectConstants";
import { getFormDefaultParams } from "../../../shared/util/getFormDefaultParams";
import { getGridServerDefaultParams } from "../../../shared/util/getGridServerDefaultParams";

export class SubClassAdminPage extends BasePage {
    createPagePropertiesBase(pageApi) {
        return {
            title: 'Sub Clase',
            barHeader: {
                fields:[
                    {...BUTTON_DEFAULT.NEW, label:'Nuevo', link: '/warehouse/sub-class/sub-class/new'}
                ]
            }
        };
    }
    
    renderPage(pageApi) {
        const getGrid = () => pageApi.getComponent('grid');
        const getForm = () => pageApi.getComponent('form');

        const form = {
            ...getFormDefaultParams(),
            fields: [
                { name: 'description', label: 'Descripcion', column: 4 },
                { type: 'select', name: 'typeExistence', label: 'Tipo de Existencia', column: 4, load: { queryId: 185 } },
                { type: 'select', name: 'familyId', label: 'Familia', column: 4, allowClear: true, load: { queryId: 50 } },
                { type: 'select', name: 'classId', label: 'Clase', column: 4, allowClear: true, load: { queryId: 346 } },
                { type: 'select', name: 'companyId', label: 'Empresa', column: 4, load: { queryId: 554 } },
            ]
        }

        const bar = {
            fields: [
                { ...BUTTON_DEFAULT.SEARCH, onClick: () => getGrid().load({ params: getForm().getData() }) },
                { ...BUTTON_DEFAULT.CLEAR, onClick: () => getForm().reset() }
            ]
        }

        const grid = {
            ...getGridServerDefaultParams({ queryLoad: 52 }),
            fields: [
                { name: 'existenceText', label: 'Tipo de Existencia', sorter: { multiple: 1 } },
                { name: 'family', label: 'Familia', },
                { name: 'class', label: 'Clase', },
                { name: 'description', label: 'Descripcion', sorter: { multiple: 1 } },
                { name: 'prefixText', label: 'Prefijo', },
                { name: 'companyName', label: 'EMPRESA', },
                { ...COLUMN_DEFAULT.VIEW, link: (value, row, index) => `/warehouse/sub-class/sub-class/id-${row.subClassId}`, label: 'Opciones', colSpan: 2 },
                { ...COLUMN_DEFAULT.EDIT, link: (value, row, index) => `/warehouse/sub-class/sub-class/id-${row.subClassId}/update`, colSpan: 0 },
                // {...COLUMN_DEFAULT.DELETE, process: {fnOk: (result)=>getGrid().load({params:getForm().getData()}),filter: (value, row, index) => ({queryId: 55, params: {subClassId: row.subClassId}}) }}
            ]
        }
        return (
            <>
                <FormFw
                    name="form"
                    getParentApi={pageApi.getApi}
                    config={form}
                />
                <BarButtonFw
                    name="bar"
                    getParentApi={pageApi.getApi}
                    config={bar}
                />
                <GridFw
                    name="grid"
                    getParentApi={pageApi.getApi}
                    config={grid}
                />
            </>
        )
    }
}
