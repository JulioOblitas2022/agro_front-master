import React from 'react';
import { Route, Switch, useRouteMatch } from 'react-router-dom';

import { SubClassAdminPage } from './admin/SubClassAdminPage';
import { SubClassDetailPage } from './detail/SubClassDetailPage';

const SubClassRoutes = () => {
    const { path } = useRouteMatch();
    return (
        <Switch>
            <Route exact path={`${path}/sub-class-admin`} component={SubClassAdminPage} />
            <Route path={`${path}/sub-class`} component={SubClassDetailPage} />
            <Route path={`${path}/sub-class/:id`} component={SubClassDetailPage} /> 
        </Switch>
    )
}

export default SubClassRoutes;