import { PAGE_MODE } from '../../../../constants/constants';
import { BUTTON_DEFAULT } from '../../../../framework/bar-button/BarButtonFw';
import { B_SELECT_CONSTANTS } from '../../../shared/constantes/BSelectConstants';

export const getConfigSubClassDetail = (pageApi) => {
    const getForm = () => pageApi.getComponent('form');

    return {
        form: {
            load: {
                queryId: 56,
                params: { subClassId: pageApi.getParamId() },
            },
            update: {
                queryId: 54,
                postLink: (resp, values) => '/warehouse/sub-class/sub-class-admin'
            },
            save: {
                queryId: 53,
                postLink: (resp, values) => '/warehouse/sub-class/sub-class-admin'
            },
            fields: [
                { ...B_SELECT_CONSTANTS.TYPE_EXISTENCES },
                { ...B_SELECT_CONSTANTS.FAMILY },
                { ...B_SELECT_CONSTANTS.CLASS },
                { name: 'description', label: 'Descripcion', column: 4 },
                { name: 'prefix', label: 'Prefijo', column: 4, },
                { type: 'select', name: 'companyId', label: 'Empresa', column: 4, load: { queryId: 554 } },
            ],
            onComponentLoad: (formApi, pageApi) => {
                if (pageApi.getPageMode() !== PAGE_MODE.NEW)
                    formApi.load();
            }
        },
        bar: {
            fields: [
                { ...BUTTON_DEFAULT.SAVE, onClick: () => getForm().save() },
                { ...BUTTON_DEFAULT.UPDATE, onClick: () => getForm().update() },
                { ...BUTTON_DEFAULT.RETURN, link: '/warehouse/sub-class/sub-class-admin' }
            ],
        },
        grid: {}
    };
}
