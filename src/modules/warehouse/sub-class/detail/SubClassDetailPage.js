import React from 'react'
import BarButtonFw from '../../../../framework/bar-button/BarButtonFw';
import FormFw from '../../../../framework/form/FormFw';
import { BasePage } from '../../../../framework/pages/BasePage'
import { getConfigSubClassDetail } from './getConfigSubClassDetail';
import { getModeSubClassDetail } from './getModeSubClassDetail';
import { getValidationSubClassDetail } from './getValidationSubClassDetail';

export class SubClassDetailPage extends BasePage {
    createPageProperties = (pageApi) => {
        return {
            title: 'Detalle Sub Clase',
            validation: getValidationSubClassDetail,
            mode: getModeSubClassDetail
        };
    }

    renderPage(pageApi) {

        const config = getConfigSubClassDetail(pageApi);

        return (
            <>
                <FormFw
                    name="form"
                    getParentApi={pageApi.getApi}
                    config={config.form}
                />
                <BarButtonFw
                    name="bar"
                    getParentApi={pageApi.getApi}
                    config={config.bar}
                />
            </>
        )
    }
}
