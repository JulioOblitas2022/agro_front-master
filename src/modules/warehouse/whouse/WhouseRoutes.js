import React from 'react'
import { Route, Switch, useRouteMatch } from 'react-router-dom'

import { WhouseAdminPage } from './admin/WhouseAdminPage';
import { WhouseDetailPage } from './detail/WhouseDetailPage';

const WhouseRoutes = () => {
    const { path } = useRouteMatch();
    return (
        <Switch>
            <Route exact path={`${path}/whouse-admin`} component={WhouseAdminPage} />
            <Route path={`${path}/whouse`} component={WhouseDetailPage} />
            <Route path={`${path}/whouse/:id`} component={WhouseDetailPage} />
        </Switch>
    )
}

export default WhouseRoutes;
