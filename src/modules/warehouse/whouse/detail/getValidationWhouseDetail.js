export const getValidationWhouseDetail = () => {
    return {
        form: {
            local: [{ required: true, message: 'Local es requerido' }],
            description: [{ required: true, message: 'Descripcion es requerida' }],
            itemType: [{ required: true, message: 'Tipo de Item es requerido' }],
            companyId: [{ required: true, message: 'Empresa es requerido' }],
        },
        grid: {

        },
        button: {

        },
        bar: {

        }
    }
}
