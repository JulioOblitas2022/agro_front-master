import { PAGE_MODE } from '../../../../constants/constants';
import { BUTTON_DEFAULT } from '../../../../framework/bar-button/BarButtonFw';

export const getConfigWhouseDetail = (pageApi) => {

    const getForm = () => pageApi.getComponent('form');

    return {
        form: {
            load: {
                queryId: 31,
                params: { warehouseId: pageApi.getParamId() },
            },
            update: {
                queryId: 32,
                postLink: (resp, values) => '/warehouse/whouse/whouse-admin'
            },
            save: {
                queryId: 29,
                postLink: (resp, values) => '/warehouse/whouse/whouse-admin'
            },
            fields: [
                { type: 'select', name: 'localId', label: 'Local', column: 4, load: { queryId: 198 } },
                { name: 'description', label: 'Descripcion', column: 4 },
                { type: 'select', name: 'itemType', label: 'Tipo de Item', column: 4, load: { queryId: 185 } },
                { type: 'select', name: 'companyId', label: 'Empresa', column: 4, load: { queryId: 554 } },
            ],
            onComponentLoad: (formApi, pageApi) => {
                if (pageApi.getPageMode() !== PAGE_MODE.NEW)
                    formApi.load();
            }
        },
        bar: {
            fields: [
                { ...BUTTON_DEFAULT.SAVE, onClick: () => getForm().save() },
                { ...BUTTON_DEFAULT.UPDATE, onClick: () => getForm().update() },
                { ...BUTTON_DEFAULT.RETURN, link: '/warehouse/whouse/whouse-admin' }
            ],
        },
        grid: {}
    };
}
