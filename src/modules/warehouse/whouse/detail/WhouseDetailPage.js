import React from 'react'
import BarButtonFw from '../../../../framework/bar-button/BarButtonFw';
import FormFw from '../../../../framework/form/FormFw';
import { BasePage } from '../../../../framework/pages/BasePage';
import { getConfigWhouseDetail } from './getConfigWhouseDetail';
import { getModeWhouseDetail } from './getModeWhouseDetail';
import { getValidationWhouseDetail } from './getValidationWhouseDetail';

export class WhouseDetailPage extends BasePage {

    createPageProperties = (pageApi) => {
        return {
            title: 'Detalle Almacenes',
            validation: getValidationWhouseDetail,
            mode: getModeWhouseDetail
        };
    }

    renderPage(pageApi) {

        const config = getConfigWhouseDetail(pageApi);

        return (
            <>
                <FormFw
                    name="form"
                    getParentApi={pageApi.getApi}
                    config={config.form}
                />
                <BarButtonFw
                    name="bar"
                    getParentApi={pageApi.getApi}
                    config={config.bar}
                />
            </>
        )
    }

}
