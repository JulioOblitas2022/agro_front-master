import { DATE_FORMAT } from '../../../../constants/constants';
import { BUTTON_DEFAULT } from '../../../../framework/bar-button/BarButtonFw';
import { getFormDefaultParams } from '../../../shared/util/getFormDefaultParams';
import { getGridServerDefaultParams } from '../../../shared/util/getGridServerDefaultParams';

export const getConfigInventory = (pageApi) => {
    const getGrid = () => pageApi.getComponent('grid');
    const getForm = () => pageApi.getComponent('form');
    
    return {
        form: {
            ...getFormDefaultParams(),
            fields:[
                {type: 'select', name:'movementType', label:'Tipo Movimiento', column: 4,
                    load: {
                        dataList: [
                            {label: 'INGRESO', value: '1'},
                            {label: 'SALIDA', value: '2'},
                        ]
                    }
                },
                {type: 'select', name:'warehouseId', label:'Almacén', column: 4, load: { queryId: 34 } },
                {type: 'select', name:'operationTypeId', label:'Tipo Operacion', column: 4, load: { queryId: 77 }},
                {type: 'date', name: 'docDateIn',label:'Fecha Desde', format: DATE_FORMAT.DDMMYYYY, column: 4},
                {type: 'date', name: 'docDateEnd',label:'Fecha Hasta', format: DATE_FORMAT.DDMMYYYY, column: 4},
                {type: 'select', name:'existType', label:'Tipo Existencia', column: 4, load: { queryId: 185 } },
                {type: 'select', name:'state', label:'Estado', column: 4,
                    load: {
                        dataList: [
                            {label: 'Todos', value: '1'},
                            {label: 'Facturados', value: '2'},
                            {label: 'Mo Facturados', value: '3'},
                        ]
                    }
                },
            ]
        },
        bar: { fields: [
            {...BUTTON_DEFAULT.SEARCH, onClick: ()=>getGrid().load({params:getForm().getData()})},
            {...BUTTON_DEFAULT.CLEAR, onClick: ()=>getForm().reset()}
        ]},
        grid: {
            ...getGridServerDefaultParams({queryLoad: 184}),
            fields:[
                {name: 'docType', label: 'Tipo Documento', sorter: {multiple: 1}},
                {name: 'docDate', label: 'Fecha Doc'},
                {name: 'docTypeNumber', label: 'N° Documento'},
                {name: 'provider', label: 'Proveedor'},
                {name: 'item', label: 'Item'},
                {name: 'unitPrice', label: 'P. Unitario'},
                {name: 'total', label: 'Total'},
                {name: 'lote', label: 'N° Lote'},
                {name: 'operationType', label: 'Tipo Operación'},
                {name: 'docPurchaseNumber', label: 'N° Doc. Compra'},
                {name: 'docPurchaseDate', label: 'Fecha Doc. Compra'},
                // {...COLUMN_DEFAULT.DELETE, process: {fnOk: (result)=>getGrid().load({params:getForm().getData()}),filter: (value, row, index) => ({queryId: 0, params: {outputId: row.outputId}}) }}
            ]
        }
    }
}