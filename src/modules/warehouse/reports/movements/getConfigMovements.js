import { BUTTON_DEFAULT } from "../../../../framework/bar-button/BarButtonFw";
import { B_SELECT_CONSTANTS } from "../../../shared/constantes/BSelectConstants";
import { getFormDefaultParams } from "../../../shared/util/getFormDefaultParams";
import { getGridServerDefaultParams } from "../../../shared/util/getGridServerDefaultParams";

export const getConfigMovements = (pageApi) => {
    const getGrid = () => pageApi.getComponent('grid');
    const getForm = () => pageApi.getComponent('form');
    
    return {
        form: {
            title: 'Filtros de busqueda',
            ...getFormDefaultParams(),
            fields:[
                {type: 'select', name:'warehouseId', label:'Almacén', column: 6, load: { queryId: 34 } },
                {type: 'select', name:'typeExistence', label:'Tipo de Existencia', column: 6,
                    load: {
                        dataList: [
                            {label: 'SUBPRODUCTOS', value: '1'},
                            {label: 'PRODUCTOS', value: '2'},
                        ]
                    }
                },
                {...B_SELECT_CONSTANTS.YEARS},
                {type: 'date', label:'Fecha Desde', column: 4},
                {type: 'date', label:'Fecha Hasta', column: 4},
            ]
        },
        bar: { fields: [
            {...BUTTON_DEFAULT.SEARCH, onClick: ()=>getGrid().load({params:getForm().getData()})},
            {...BUTTON_DEFAULT.CLEAR, onClick: ()=>getForm().reset()}
        ]},
        grid: {
            title: 'Resultados',
            ...getGridServerDefaultParams({queryLoad: 182}),
            fields:[
                {name: 'typeExistence', label: 'Tipo de Movimiento'},
                {name: 'code', label: 'F. Documento'},
                {name: 'description', label: 'nro Documento'},
                {name: 'unitMeasuse', label: 'Proveedor'},
                {name: 'initialStock', label: 'Item'},
                {name: 'inputs', label: 'Cantidad'},
                {name: 'outputs', label: 'P. Unitario'},
                {name: 'balance', label: 'Total'},
                {name: 'balance', label: 'Nro Lote'},
                {name: 'balance', label: 'Tipo de Operacion'},
                {name: 'balance', label: 'Tipo Doc'},
                {name: 'balance', label: 'Nro.Doc Compra'},
                {name: 'balance', label: 'Fecha Doc. Compra'},
            ]
        }
    }
}
