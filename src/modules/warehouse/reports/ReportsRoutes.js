import React from 'react';
import { Route, Switch, useRouteMatch } from 'react-router-dom';

import { InventoryPage } from './inventory/InventoryPage';
import { KardexPage } from './kardex/KardexPage';
import { MovementsPage } from './movements/MovementsPage';

const ReportsRoutes = () => {
    const { path } = useRouteMatch();
    return (
        <Switch>
            <Route exact path={`${path}/kardex`} component={KardexPage} />
            <Route exact path={`${path}/inventory`} component={InventoryPage} />
            <Route exact path={`${path}/movements`} component={MovementsPage} />
        </Switch>
    )
}
// npm install react-data-export --save
export default ReportsRoutes;
