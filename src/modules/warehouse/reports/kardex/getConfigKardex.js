import { BUTTON_DEFAULT } from "../../../../framework/bar-button/BarButtonFw";
import { B_SELECT_CONSTANTS } from "../../../shared/constantes/BSelectConstants";
import { getFormDefaultParams } from "../../../shared/util/getFormDefaultParams";
import { getGridServerDefaultParams } from "../../../shared/util/getGridServerDefaultParams";

export const getConfigKardex = (pageApi) => {
    const getGrid = () => pageApi.getComponent('grid');
    const getForm = () => pageApi.getComponent('form');
    
    return {
        form: {
            ...getFormDefaultParams(),
            fields:[
                {type: 'select', name:'warehouseId', label:'Almacén', column: 4, load: { queryId: 34 } },
                {type: 'select', name:'typeExistence', label:'Tipo de Existencia', column: 4, load: { queryId:185 } },
                {type: 'date', label:'Fecha Desde', column: 4},
                {type: 'date', label:'Fecha Hasta', column: 4},
            ]
        },
        bar: { fields: [
            {...BUTTON_DEFAULT.SEARCH, onClick: ()=>getGrid().load({params:getForm().getData()})},
            {...BUTTON_DEFAULT.CLEAR, onClick: ()=>getForm().reset()}
        ]},
        grid: {
            ...getGridServerDefaultParams({queryLoad: 182}),
            fields:[
                {name: 'existencesType', label: 'Tipo de Existencia'},
                {name: 'code', label: 'Codigo Producto'},
                {name: 'description', label: 'Descripcion'},
                {name: 'unitMeasuse', label: 'Uni. Med'},
                {name: 'initialStock', label: 'Stock Inicial'},
                {name: 'inputs', label: 'Ingresos'},
                {name: 'outputs', label: 'Salidas'},
                {name: 'balance', label: 'Saldo'},
            ]
        }
    }
}
