import React from 'react'
import BarButtonFw, { BUTTON_DEFAULT } from '../../../../framework/bar-button/BarButtonFw';
import FormFw from '../../../../framework/form/FormFw';
import GridFw from '../../../../framework/grid/GridFw';
import { BasePage } from '../../../../framework/pages/BasePage';
import { COLUMN_DEFAULT } from '../../../../framework/grid/GridFw';
import { getFormDefaultParams } from '../../../shared/util/getFormDefaultParams';
import { getGridServerDefaultParams } from '../../../shared/util/getGridServerDefaultParams';

export class MasterItemAdminPage extends BasePage {
    createPageProperties = (pageApi) => {
        return {
            title: 'Maestro Items',
            barHeader: {
                fields:[
                    {...BUTTON_DEFAULT.NEW, label:'Nuevo', link: '/warehouse/master-item/master-item/new'}
                ]
            }
        };
    }

    renderPage(pageApi) {

        const getGrid = () => pageApi.getComponent('grid');
        const getForm = () => pageApi.getComponent('form');

        const form = {
            ...getFormDefaultParams(),
            fields: [
                { name: 'code', label: 'Codigo', column: 4 },
                { name: 'description', label: 'Descripcion', column: 4 },
                { type: 'select', name: 'itemTypeId', label: 'Tipo Item', column: 4, load: { queryId: 185 } },
            ]
        }

        const bar = {
            fields: [
                { ...BUTTON_DEFAULT.SEARCH, onClick: () => getGrid().load({ params: getForm().getData() }) },
                { ...BUTTON_DEFAULT.CLEAR, onClick: () => getForm().reset() }
            ]
        }

        const grid = {
            ...getGridServerDefaultParams({ queryLoad: 155 }),
            fields: [
                { name: 'code', label: 'Codigo', sorter: { multiple: 1 } },
                { name: 'description', label: 'Descripcion', sorter: { multiple: 1 } },
                { name: 'unitMeasure', label: 'Unidad' },
                { name: 'coinText', label: 'Moneda', },
                { name: 'minStock', label: 'Stock', },
                { name: 'itemTypeText', label: 'Tipo Item', },
                { name: 'activeText', label: 'Estado', },
                { ...COLUMN_DEFAULT.VIEW, link: (value, row, index) => `/warehouse/master-item/master-item/id-${row.masterItemId}`, label: 'Opciones', colSpan: 2 },
                { ...COLUMN_DEFAULT.EDIT, link: (value, row, index) => `/warehouse/master-item/master-item/id-${row.masterItemId}/update`, colSpan: 0 },
                // {...COLUMN_DEFAULT.DELETE,
                //     process: {
                //         fnOk: (result)=>getGrid().load({params:getForm().getData()}),
                //         filter: (value, row, index) => ({queryId: 157, params: {masterItemId: row.masterItemId}})
                //     }
                // }
            ]
        }
        return (
            <>
                <FormFw
                    name="form"
                    getParentApi={pageApi.getApi}
                    config={form}
                />
                <BarButtonFw
                    name="bar"
                    getParentApi={pageApi.getApi}
                    config={bar}
                />
                <GridFw
                    name="grid"
                    getParentApi={pageApi.getApi}
                    config={grid}
                />
            </>
        )
    }
}
