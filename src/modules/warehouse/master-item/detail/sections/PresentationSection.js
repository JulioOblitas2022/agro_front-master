import React from 'react';
import FormGridFw, { FORM_GRID_MODE } from '../../../../../framework/form-grid/FormGridFw';

export const createPresentationSection = (pageApi) => {

    const gridConfig = {
        title: 'PRESENTACIONES',
        mode: FORM_GRID_MODE.FORM_ADD,
        collapsible: true,
        form: {
            title: 'Datos',
            fields: [
                { name: 'abbreviation', label: 'Pres. Abrv', column: 6 },
                { name: 'description', label: 'Pres. Descripcion', column: 6 },
                { type: 'select', name: 'unitMeasureId', label: 'Uni. Med. Basica', labelName: 'unitMeasure', column: 6, load: { queryId: 192 } },
                { type: 'number', name: 'quantityBasicUnitMeasure', label: 'Can. Uni. Med. Basica', column: 6 },
            ]
        },
        grid: {
            title: 'Unidad',
            rowId: 'masterItemPresentationId',
            showIndex: true,
            pagination: true,
            autoLoad: true,
            load: {
                validation: () => pageApi.getParamId() != null, //solo se dispara si el valor es distinto de nulo
                queryId: 153,
                orderBy: 'masterItemPresentationId desc',
                params: { masterItemId: pageApi.getParamId() }
            },
            save: {
                queryId: 151,
                params: { masterItemId: pageApi.getParamId() }
            },
            fields: [
                { name: 'abbreviation', label: 'Pres. Abrv', width: 100 },
                { name: 'description', label: 'Pres. Descripcion', width: 100 },
                { name: 'unitMeasure', label: 'Uni. Med. Basica', width: 100, },
                { name: 'quantityBasicUnitMeasure', label: 'Can. Uni. Med. Basica', width: 100 },
            ]
        }
    };

    return (
        <FormGridFw
            name="presentationGrid"
            getParentApi={pageApi.getApi}
            config={gridConfig}
        />
    )
}
