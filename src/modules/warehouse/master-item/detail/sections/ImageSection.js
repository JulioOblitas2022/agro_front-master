import React from 'react';
import FormGridFw, { FORM_GRID_MODE } from '../../../../../framework/form-grid/FormGridFw';

export const createImageSection = (pageApi) => {
    
    const gridConfig = {
        title: 'FOTOGRAFIA',
        mode: FORM_GRID_MODE.FORM_ADD,
        collapsible: true,
        form: {
            fields:[
                {name:'description', label:'Descripción', column: 6},
                {name:'path', label:'Adjunto', column: 6, multiple: false, accept:['image/png', 'image/jpeg']},
            ]
        },
        grid: {
            title: '',
            rowId: 'masterItemImageId',
            showIndex: true,
            pagination: true,
            autoLoad: true,
            load: {
                validation: () => pageApi.getParamId()!=null, //solo se dispara si el valor es distinto de nulo
                queryId: 152,
                orderBy: 'masterItemImageId desc',
                params: {masterItemId: pageApi.getParamId()}
            },
            save: {
                queryId: 150,
                params: {masterItemId: pageApi.getParamId()}
            },
            fields:[
                {name:'description', label:'Descripcion', width: 100},
                {name:'path', label:'Adjunto', width: 100},
            ]
        }
    };  

    return (
        <FormGridFw
            name="imageGrid"
            getParentApi={pageApi.getApi}
            config={gridConfig}
        />
    )
}
