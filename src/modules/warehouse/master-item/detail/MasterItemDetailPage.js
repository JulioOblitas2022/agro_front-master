import React from 'react'
import BarButtonFw from '../../../../framework/bar-button/BarButtonFw';
import FormFw from '../../../../framework/form/FormFw';
import { BasePage } from '../../../../framework/pages/BasePage';
import { getConfigMasterItemDetail } from './getConfigMasterItemDetail';
import { getModeMasterItemDetail } from './getModeMasterItemDetail';
import { getValidationMasterItemDetail } from './getValidationMasterItemDetail';
import { createImageSection } from './sections/ImageSection';
import { createPresentationSection } from './sections/PresentationSection';

export class MasterItemDetailPage extends BasePage {

    createPageProperties = (pageApi) => {
        return {
            title: 'Detalle Maestro Items',
            validation: getValidationMasterItemDetail,
            mode: getModeMasterItemDetail
        };
    }

    renderPage(pageApi) {

        const config = getConfigMasterItemDetail(pageApi);

        return (
            <>
                <FormFw
                    name="form"
                    getParentApi={pageApi.getApi}
                    config={config.form}
                />
                {createImageSection(pageApi)}
                {createPresentationSection(pageApi)}
                <br/>
                <BarButtonFw
                    name="bar"
                    getParentApi={pageApi.getApi}
                    config={config.bar}
                />
            </>
        )
    }
}
