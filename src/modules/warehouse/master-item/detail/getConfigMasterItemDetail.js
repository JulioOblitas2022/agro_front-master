import { PAGE_MODE } from "../../../../constants/constants";
import { BUTTON_DEFAULT } from "../../../../framework/bar-button/BarButtonFw";
import { UserUtil } from "../../../../util/UserUtil";
import { B_SELECT_CONSTANTS } from "../../../shared/constantes/BSelectConstants";

export const getConfigMasterItemDetail = (pageApi) => {

    const getForm = () => pageApi.getComponent('form');
    const getGridImageGrid = () => pageApi.getComponent('imageGrid.grid');
    const getGridPresentationGrid = () => pageApi.getComponent('presentationGrid.grid');

    return {
        form: {
            load: {
                queryId: 154,
                params: { masterItemId: pageApi.getParamId() },
            },
            update: {
                queryId: 156,
                postLink: (resp, values) => '/warehouse/master-item/master-item-admin',
                params: {
                    modifiedBy: UserUtil.getUserId()
                },
                fnOk: (resp) => {
                    getGridImageGrid().save();
                    getGridPresentationGrid().save();
                },
            },
            save: {
                queryId: 148,
                postLink: (resp, values) => '/warehouse/master-item/master-item-admin',
                params: {
                    createdBy: UserUtil.getUserId()
                },
                fnOk: (resp) => {
                    const masterItemId = resp.dataObject?.masterItemId;
                    if (masterItemId) {
                        getGridImageGrid().save({ params: { masterItemId } });
                        getGridPresentationGrid().save({ params: { masterItemId } });
                    }
                },
            },
            fnOk: (resp) => { },
            title: 'REGISTRO DE ITEMS',
            fields: [
                { type: 'divider', label: 'DATOS PRINCIPALES' },
                { name: 'code', label: 'Codigo', column: 4 },
                { type: 'select', name: 'itemTypeId', label: 'Tipo Item', column: 4, load: { queryId: 185 } },
                // {type:'select', name:'familyId', label:'Familia', column: 4, load: {queryId: 50},},
                { ...B_SELECT_CONSTANTS.FAMILY, column: 4 },
                { ...B_SELECT_CONSTANTS.CLASS, column: 4 },
                { ...B_SELECT_CONSTANTS.SUB_CLASS, column: 4 },
                { name: 'lotPrefix', label: 'Prefijo Lote', column: 4 },
                { name: 'description', label: 'Descripcion', column: 6 },
                { name: 'technicalDescription', label: 'Descripcion Tecnica', column: 6 },
                { type: 'textarea', name: 'characteristics', label: 'Caracteristicas', column: 12 },
                { type: 'select', name: 'coin', label: 'Moneda', column: 4, load: { queryId: 187 } },
                { type: 'number', name: 'timeUtil', label: 'Tiempo Util (dias)', column: 4 },
                { type: 'number', name: 'initialStock', label: 'Stock Inicial', column: 4 },
                { type: 'number', name: 'currentStock', label: 'Stock Actual', column: 4 },
                { type: 'number', name: 'minStock', label: 'Stock Minimo', column: 4 },
                { type: 'number', name: 'maxStock', label: 'Stock Maximo', column: 4 },
                { type: 'decimal', name: 'initialPrice', label: 'Precio Inicial', column: 4 },
                { type: 'decimal', name: 'currentPrice', label: 'Precio Actual', column: 4 },
                {
                    type: 'radio', name: 'active', label: 'Estado', column: 4, value: 1,
                    load: {
                        dataList: [
                            { label: 'ACTIVO', value: 1 },
                            { label: 'INACTIVO', value: 2 },
                        ]
                    }
                }

            ],
            // validations: getValidationMasterItemDetail()['form'],
            // mode : getDataPageMode(),
            onComponentLoad: (formApi, pageApi) => {
                if (pageApi.getPageMode() !== PAGE_MODE.NEW)
                    formApi.load();
            }
        },
        bar: {
            fields: [
                { ...BUTTON_DEFAULT.SAVE, onClick: () => getForm().save() },
                { ...BUTTON_DEFAULT.UPDATE, onClick: () => getForm().update() },
                { ...BUTTON_DEFAULT.RETURN, link: '/warehouse/master-item/master-item-admin' }
            ],
            // mode : getDataPageMode()
        },
        grid: {},
    };
}
