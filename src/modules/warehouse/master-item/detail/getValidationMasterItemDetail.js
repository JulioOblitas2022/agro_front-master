export const getValidationMasterItemDetail = () => {
    return {
        form: {
            code: [ {required: true, message: 'Codigo es requerido' } ],
            itemTypeId: [ {required: true, message: 'Tipo de item es requerido' } ],
            familyId: [ {required: true, message: 'Familia es requerida' } ],
            classId: [ {required: true, message: 'Clase es requerida' } ],
            subClassId: [ {required: true, message: 'SubClase es requerida' } ],
            lotPrefix: [ {required: true, message: 'Prefijo Lote es requerido' } ],
            description: [ {required: true, message: 'Descripcion es requerida' } ],
            technicalDescription: [ {required: true, message: 'Descripcion Tecnica es requerida' } ],
            characteristics: [ {required: true, message: 'Caracteristicas son requeridas' } ],
            initialStock: [ {required: true, message: 'Stock Inicial es requerido' } ],
            currentStock: [ {required: true, message: 'Stock Actual es requerido' } ],
            minStock: [ {required: true, message: 'Stock Minimo es requerido' } ],
            maxStock: [ {required: true, message: 'Stock Maximo es requerido' } ],
            coin: [ {required: true, message: 'Moneda es requerida' } ],
            timeUtil: [ {required: true, message: 'Tiempo Util es requerido' } ],
            initialPrice: [ {required: true, message: 'Precio Inicial es requerido' } ],
            currentPrice: [ {required: true, message: 'Precio Actual es requerido' } ],
        },
        grid: {

        },
        button: {

        },
        bar: {

        }
    }
}
