import React from 'react';
import { Route, Switch, useRouteMatch } from 'react-router-dom';

import { MasterItemAdminPage } from './admin/MasterItemAdminPage';
import { MasterItemDetailPage } from './detail/MasterItemDetailPage';

const MasterItemRoutes = () => {
    const { path } = useRouteMatch();
    return (
        <Switch>
            <Route exact path={`${path}/master-item-admin`} component={MasterItemAdminPage} />
            <Route path={`${path}/master-item`} component={MasterItemDetailPage} />
            <Route path={`${path}/master-item/:id`} component={MasterItemDetailPage} /> 
        </Switch>
    )
}

export default MasterItemRoutes;