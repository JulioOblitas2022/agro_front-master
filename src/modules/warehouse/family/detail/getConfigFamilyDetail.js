import { PAGE_MODE } from '../../../../constants/constants';
import { BUTTON_DEFAULT } from '../../../../framework/bar-button/BarButtonFw';

export const getConfigFamilyDetail = (pageApi) => {

    const getForm = () => pageApi.getComponent('form');

    return {
        form: {
            load: {
                queryId: 41,
                params: { familyId: pageApi.getParamId() },
            },
            update: {
                queryId: 43,
                postLink: (resp, values) => '/warehouse/family/family-admin'
            },
            save: {
                queryId: 42,
                postLink: (resp, values) => '/warehouse/family/family-admin'
            },
            fields: [
                { type: 'select', name: 'typeExistence', label: 'Tipo de Existencia', column: 4, load: { queryId: 185 } },
                { name: 'description', label: 'Descripcion', column: 4 },
                { name: 'prefix', label: 'Prefijo', column: 4 },
                { type: 'select', name: 'companyId', label: 'Empresa', column: 4, load: { queryId: 554 }, search: true },
            ],
            onComponentLoad: (formApi, pageApi) => {
                if (pageApi.getPageMode() !== PAGE_MODE.NEW)
                    formApi.load();
            }
        },
        bar: {
            fields: [
                { ...BUTTON_DEFAULT.SAVE, onClick: () => getForm().save() },
                { ...BUTTON_DEFAULT.UPDATE, onClick: () => getForm().update() },
                { ...BUTTON_DEFAULT.RETURN, link: '/warehouse/family/family-admin' }
            ],
        },
        grid: {}
    };
}
