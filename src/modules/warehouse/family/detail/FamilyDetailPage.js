import React from 'react'
import BarButtonFw from '../../../../framework/bar-button/BarButtonFw';
import FormFw from '../../../../framework/form/FormFw';
import { BasePage } from '../../../../framework/pages/BasePage';
import { getConfigFamilyDetail } from './getConfigFamilyDetail';
import { getModeFamilyDetail } from './getModeFamilyDetail';
import { getValidationFamilyDetail } from './getValidationFamilyDetail';

export class FamilyDetailPage extends BasePage {
    createPageProperties = (pageApi) => {
        return {
            title: 'Detalle Familia',
            validation: getValidationFamilyDetail,
            mode: getModeFamilyDetail
        };
    }

    renderPage(pageApi) {

        const config = getConfigFamilyDetail(pageApi);

        return (
            <>
                <FormFw
                    name="form"
                    getParentApi ={pageApi.getApi }
                    config={config.form}
                />
                <BarButtonFw
                    name="bar"
                    getParentApi ={pageApi.getApi }
                    config={config.bar}
                />
            </>
        )
    }
}
