import React from 'react'
import BarButtonFw, { BUTTON_DEFAULT } from '../../../../framework/bar-button/BarButtonFw';
import FormFw from '../../../../framework/form/FormFw';
import GridFw from '../../../../framework/grid/GridFw';
import { BasePage } from '../../../../framework/pages/BasePage';
import { COLUMN_DEFAULT } from '../../../../framework/grid/GridFw';
import { getFormDefaultParams } from '../../../shared/util/getFormDefaultParams';
import { getGridServerDefaultParams } from '../../../shared/util/getGridServerDefaultParams';


export class FamilyAdminPage extends BasePage {

    createPagePropertiesBase(pageApi) {
        return {
            title: 'Familia',
            barHeader : {
                fields:[
                    {...BUTTON_DEFAULT.NEW, label:'Nuevo', link: '/warehouse/family/family/new'}
                ]
            }
        };
    }

    renderPage(pageApi) {
        const getGrid = () => pageApi.getComponent('grid');
        const getForm = () => pageApi.getComponent('form');

        const form = {
            ...getFormDefaultParams(),
            fields: [
                { type: 'select', name: 'typeExistence', label: 'Tipo de Existencia', column: 4, load: { queryId: 185 }, search: true },
                { name: 'description', label: 'Descripcion', column: 4 },
                { name: 'prefix', label: 'Prefijo', column: 4 },
                { type: 'select', name: 'companyId', label: 'Empresa', column: 4, load: { queryId: 554 }, search: true },
            ]
        }

        const bar = {
            fields: [
                { ...BUTTON_DEFAULT.SEARCH, onClick: () => getGrid().load({ params: getForm().getData() }) },
                { ...BUTTON_DEFAULT.CLEAR, onClick: () => getForm().reset() }
            ]
        }

        const grid = {
            ...getGridServerDefaultParams({ queryLoad: 40 }),
            fields: [
                { name: 'existenceText', label: 'Tipo de Existencia', sorter: { multiple: 1 } },
                { name: 'description', label: 'Descripcion', sorter: { multiple: 1 } },
                { name: 'prefixText', label: 'Prefijo', },
                { name: 'companyName', label: 'EMPRESA', },
                { ...COLUMN_DEFAULT.VIEW, link: (value, row, index) => `/warehouse/family/family/id-${row.familyId}`, label: 'Opciones', colSpan: 2 },
                { ...COLUMN_DEFAULT.EDIT, link: (value, row, index) => `/warehouse/family/family/id-${row.familyId}/update`, colSpan: 0 },
                // {...COLUMN_DEFAULT.DELETE, process: {fnOk: (result)=>getGrid().load({params:getForm().getData()}),filter: (value, row, index) => ({queryId: 44, params: {familyId: row.familyId}}) }}
            ]
        }
        return (
            <>
                <FormFw
                    name="form"
                    getParentApi ={pageApi.getApi }
                    config={form}
                />
                <BarButtonFw
                    name="bar"
                    getParentApi ={pageApi.getApi }
                    config={bar}
                />
                <GridFw
                    name="grid"
                    getParentApi ={pageApi.getApi }
                    config={grid}
                />
            </>
        )
    }
}
