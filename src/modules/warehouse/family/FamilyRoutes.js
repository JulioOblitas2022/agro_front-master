import React from 'react'
import { Route, Switch, useRouteMatch } from 'react-router-dom'

import { FamilyAdminPage } from './admin/FamilyAdminPage';
import { FamilyDetailPage } from './detail/FamilyDetailPage';

const FamilyRoutes = () => {
    const { path } = useRouteMatch();
    return (
        <Switch>
            <Route exact path={`${path}/family-admin`} component={FamilyAdminPage} />
            <Route path={`${path}/family`} component={FamilyDetailPage} />
            <Route path={`${path}/family/:id`} component={FamilyDetailPage} />
        </Switch>
    )
}

export default FamilyRoutes;
