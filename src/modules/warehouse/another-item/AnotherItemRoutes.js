import React from 'react'
import { Route, Switch, useRouteMatch } from 'react-router-dom'

import { AnotherItemAdminPage } from './admin/AnotherItemAdminPage'
import { AnotherItemDetailPage } from './detail/AnotherItemDetailPage'

const AnotherItemRoutes = () => {
    const { path } = useRouteMatch();
    return (
        <Switch>
            <Route exact path={`${path}/another-item-admin`} component={AnotherItemAdminPage} />
            <Route path={`${path}/another-item`} component={AnotherItemDetailPage} />
            <Route path={`${path}/another-item/:id`} component={AnotherItemDetailPage} />
        </Switch>
    )
}

export default AnotherItemRoutes;
