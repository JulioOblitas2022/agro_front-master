import React from 'react'
import { DATE_FORMAT } from '../../../../../constants/constants';
import { BUTTON_DEFAULT } from '../../../../../framework/bar-button/BarButtonFw';
import FormGridFw, { FORM_GRID_MODE } from '../../../../../framework/form-grid/FormGridFw';
import GridFw, { COLUMN_DEFAULT } from '../../../../../framework/grid/GridFw';
import { ProductModal } from '../modals/ProductModal';

export const createProductSection = (pageApi) => {

    const getGrid = () => pageApi.getComponent('productGrid');

    const fnDelete = (value, row, index) => {
        getGrid().deleteItem(row.key);      
    }

    const itemConfig = {
        events: {
            afterAddEvent : (data) => {
                getGrid().addItem(data);
            }
        }
    }

    const ItemModalConfig = (data) =>  {
        return {
            width: 1600,
            maskClosable: false,
            component: <ProductModal name={data}  getParentApi={pageApi.getApi} config={itemConfig}  />
        }
    };


    const barHeaderConfig = {
        fields:[
            {...BUTTON_DEFAULT.ADD, label:'Agregar Item', onClick: ()=>{  pageApi.showModal(ItemModalConfig('')); }}
        ]
    }

    const gridConfig = {
        title: 'Otros items registrados',
        rowId: 'anotherItemDetailId',
        showIndex: true,
        pagination: true,
        autoLoad: true,
        load: {
            validation: () => pageApi.getParamId() != null, //solo se dispara si el valor es distinto de nulo
            queryId: 167,
            orderBy: 'anotherItemDetailId desc',
            params: { warehouseIncomeId: pageApi.getParamId() },
            hideMessage: true, 
            hideMessageError: true
        },
        save: {
            queryId: 168,
            params: { warehouseIncomeId: pageApi.getParamId() },
            hideMessage: true, 
            hideMessageError: true
        },
        fields: [
            { name: 'itemTypeText', label: 'Codigo' },
            { name: 'description', label: 'Mercaderia / Producto / Servicio' },
            { type: 'select', name: 'unitMeasureId', label: 'Uni. Med.', labelName: 'unitMeasureIdDes', load: { queryId: 568, params: () => {return {masterItemId: getGrid()?.itemEditing?.masterItemId}}}, search: true, editable: true },
            { type: 'number', name: 'quantity', label: 'Cantidad', editable: true},
            { name: 'lot', label: 'Lote', editable: true},
            { type: 'date', name: 'productionDate', label: 'Fecha Produccion', format: DATE_FORMAT.DDMMYYYY, inputFormat: DATE_FORMAT.YYYYMMDD, editable: true, column: 12 },
            { type: 'select', name: 'checkProduct', label: 'Conforme', labelName: 'checkProductDes', editable: true, load: { queryId: 250 } },
            {...COLUMN_DEFAULT.DELETE, onClick: fnDelete}
        ],
        barHeader: barHeaderConfig
    };

    return (
        <GridFw
            name="productGrid"
            getParentApi={pageApi.getApi}
            config={gridConfig}
        />
    )
}