import React from 'react'
import BarButtonFw from '../../../../framework/bar-button/BarButtonFw';
import FormFw from '../../../../framework/form/FormFw';
import { BasePage } from '../../../../framework/pages/BasePage';
import { getConfigAnotherItemDetail } from './getConfigAnotherItemDetail';
import { getModeAnotherItemDetail } from './getModeAnotherItemDetail';
import { getValidationAnotherItemDetail } from './getValidationAnotherItemDetail';
import { createProductSection } from './sections/ProductSection';

export class AnotherItemDetailPage extends BasePage {

    createPageProperties = (pageApi) => {
        return {
            title: 'Detalle Otros Items',
            validation: getValidationAnotherItemDetail,
            mode: getModeAnotherItemDetail
        };
    }

    renderPage(pageApi) {

        const config = getConfigAnotherItemDetail(pageApi);

        return (
            <>
                <FormFw
                    name="form"
                    getParentApi={pageApi.getApi}
                    config={config.form}
                />
                {createProductSection(pageApi)}
                <BarButtonFw
                    name="bar"
                    getParentApi={pageApi.getApi}
                    config={config.bar}
                />
            </>
        )
    }
}
