import React, { useState } from 'react';
import { DATE_FORMAT, PAGE_MODE } from '../../../../constants/constants';
import { BUTTON_DEFAULT } from '../../../../framework/bar-button/BarButtonFw';
import { DateUtil } from '../../../../util/DateUtil';
import { UserUtil } from '../../../../util/UserUtil';
import { B_SELECT_CONSTANTS } from '../../../shared/constantes/BSelectConstants';
import { DocReferencesModal } from './modals/DocReferencesModal';

export const getConfigAnotherItemDetail = (pageApi) => {
    const getForm = () => pageApi.getComponent('form');
    const getProductGrid = () => pageApi.getComponent('productGrid');
    // const getDataPageMode = () => pageApi.getDataPageMode(getModeAnotherItemDetail());

    const getGrid = () => pageApi.getComponent('getConfigAnotherItemDetail');

    const docRefConfig = {
        events: {
            afterAddEvent: (data) => {
                //getForm().setFieldData('docRefNumber', data[0].docRefNumber)
                let itemData = JSON.parse(data[0].items);
                if (itemData !== null) {
                    //getProductGrid().reset();
                    getProductGrid().setData(itemData);
                }
                else {
                    getProductGrid().reset();
                }
                getForm().setFieldData('documentRefId', data[0].documentRefId);
                getForm().setFieldData('documentRefNumber', data[0].documentRefNumber);

            }
        }
    }

    const DocRefModalConfig = {
        width: 1000,
        maskClosable: false,
        component: <DocReferencesModal name="docRefModal" getParentApi={pageApi.getApi} config={docRefConfig} />
    };

    return {
        form: {
            load: {
                queryId: 161,
                params: { warehouseIncomeId: pageApi.getParamId() },
            },
            save: {
                queryId: 166,
                postLink: (resp, values) => '/warehouse/another-item/another-item-admin',
                params: {createdBy: UserUtil.getUserId()},
                fnOk: (resp) => {
                    const warehouseIncomeId = resp.dataObject?.warehouseIncomeId;
                    if (warehouseIncomeId) {
                        getProductGrid().save({ params: { warehouseIncomeId } });
                    }
                }
            },
            update: {
                queryId: 163,
                params: { warehouseIncomeId: pageApi.getParamId(), modifiedBy: UserUtil.getUserId() },
                postLink: (resp, values) => '/warehouse/another-item/another-item-admin',
                fnOk: (resp) => {
                    getProductGrid().save();
                }
            },
            title: 'Datos Principales',
            fields: [
                { type: 'select', name: 'companyId', label: 'Empresa', column: 4, load: { queryId: 554 }, search: true },
                { type: 'date', name: 'entryDate', label: 'Fecha Ingreso', format: DATE_FORMAT.DDMMYYYY, inputFormat: DATE_FORMAT.YYYYMMDD, column: 4, value: DateUtil.getDate },
                { type: 'date', name: 'docDate', label: 'Fecha Documento', format: DATE_FORMAT.DDMMYYYY, inputFormat: DATE_FORMAT.YYYYMMDD, column: 4, value: DateUtil.getDate },
                { type: 'select', name: 'operationTypeId', label: 'Tipo Operacion', column: 4, load: { queryId: 77 }, value: 2 },
                { type: 'select', name: 'providerId', label: 'Proveedor', column: 4, search: true, load: { queryId: 159 } },
                { type: 'select', name: 'warehouseId', label: 'Almacén', column: 4, load: { queryId: 34 }, search: true },
                { type: 'select', name: 'documentTypeId', label: 'Tipo Documento', column: 4, load: { queryId: 524 }, search: true,
                    parents: [
                        { name: 'warehouseId', paramName: 'warehouseId', required: true }
                    ],
                    onChange: (value, values, formApi) => {
                        let correlativo = getForm().getFieldApi('documentTypeId').getSelectedItem()?.correlativo;
                        getForm().setFieldData('documentTypeNumber', correlativo);
                    }
                },
                { name: 'documentTypeNumber', label: 'Nro. Documento', column: 4, readOnly: true, maxLength: 10, },
                { type: 'select', name: 'responsableId', label: 'Encargado', column: 4, search: true, load: { queryId: 309 } },
                { type: 'select', name: 'documentTypeRefId', label: 'Doc. Referencia', column: 4, search: true, load: { queryId: 195 },
                    onChange: (value) => {
                        if (value == 84) {
                            getForm()?.getConfig()?.fields[12].setVisible(true);
                        } 
                        else if (value != 84) {
                            getForm()?.getConfig()?.fields[12].setVisible(false);
                            getForm()?.setFieldData('documentRefId', '');
                            getForm()?.setFieldData('documentRefNumber', '');
                        }
                    }, value: 84

                },
                { type: 'hidden', name: 'documentRefId' },
                { name: 'documentRefNumber', label: 'Nro. Documento Ref', column: 4, maxLength: 10, }, 
                {
                    type: 'button', column: 1, name: 'modal',
                    onClick: (value) => {
                        pageApi.showModal(DocRefModalConfig);

                    },
                },
                { type: 'textarea', name: 'observations', label: 'Observaciones', column: 12 },
            ],
            onComponentLoad: (formApi, pageApi) => {
                if (pageApi.getPageMode() !== PAGE_MODE.NEW)
                    formApi.load();
            }
        },
        bar: {
            fields: [
                { ...BUTTON_DEFAULT.SAVE, onClick: () => getForm().save() },
                { ...BUTTON_DEFAULT.UPDATE, onClick: () => getForm().update() },
                { ...BUTTON_DEFAULT.RETURN, link: '/warehouse/another-item/another-item-admin' }
            ],
        },
        grid: {}
    }
}
