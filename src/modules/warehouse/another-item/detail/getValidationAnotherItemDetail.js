export const getValidationAnotherItemDetail = () => {
    return {
        form: {
            entryDate: [{ required: true, message: 'Fecha Entrada es requerido' }],
            docDate: [{ required: true, message: 'fecha Doc. es requerido' }],
            warehouseId: [{ required: true, message: 'Almacen es requerido' }],
            operationTypeId: [{ required: true, message: 'Tipo operacion es requerido' }],
            docTypeId: [{ required: true, message: 'Tipo Doc. es requerido' }],
            docTypeNumber: [{ required: true, message: 'Doc. es requerido' }],
            providerId: [{ required: true, message: 'Proveedor es requerido' }],
            responsableId: [{ required: true, message: 'Responsable es requerido' }],
            docRefId: [{ required: true, message: 'Ref. Doc es requerido' }],
            docRefNumber: [{ required: true, message: 'Nro. Doc es requerido' }],
            companyId: [{ required: true, message: 'Empresa es requerido' }],
        },
        grid: {

        },
        button: {

        },
        bar: {

        }
    }
}
