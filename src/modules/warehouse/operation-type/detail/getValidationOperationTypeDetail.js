
export const getValidationOperationTypeDetail = () => {
    return {
        form: {
            description: [{ required: true, message: 'Descripcion es requerida' }],
            sunatCode: [{ required: true, message: 'Cod. Sunat es requerida' }],
            movementType: [{ required: true, message: 'Tipo Movimiento es requerido' }],
            companyId: [{ required: true, message: 'Empresa es requerido' }],
        },
        grid: {

        },
        button: {

        },
        bar: {

        }
    }
}
