import React from 'react';
import BarButtonFw from '../../../../framework/bar-button/BarButtonFw';
import FormFw from '../../../../framework/form/FormFw';
import { BasePage } from '../../../../framework/pages/BasePage';
import { getConfigOperationTypeDetail } from './getConfigOperationTypeDetail';
import { getModeOperationTypeDetail } from './getModeOperationTypeDetail';
import { getValidationOperationTypeDetail } from './getValidationOperationTypeDetail';


export class OperationTypeDetailPage extends BasePage {
    
    createPageProperties = (pageApi) => {
        return {
            title: 'Detalle Tipo Operacion',
            validation: getValidationOperationTypeDetail,
            mode: getModeOperationTypeDetail,
        };
    }

    renderPage(pageApi) {
        const config = getConfigOperationTypeDetail(pageApi);
          
        return (
            <>
                <FormFw
                    name="form"
                    getParentApi={pageApi.getApi}
                    config={config.form}
                />
                <BarButtonFw
                    name="bar"
                    getParentApi={pageApi.getApi}
                    config={config.bar}
                />
            </>
        )
    }
}


