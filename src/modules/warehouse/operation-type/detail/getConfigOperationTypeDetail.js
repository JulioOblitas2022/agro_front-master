import { PAGE_MODE } from "../../../../constants/constants";
import { BUTTON_DEFAULT } from "../../../../framework/bar-button/BarButtonFw";

export const getConfigOperationTypeDetail = (parent) => {
    const getForm = () => parent.getComponent('form');

    return {
        form: {
            load: {
                queryId: 28,
                params: { operationTypeId: parent.getParamId() },
            },
            update: {
                queryId: 26,
                postLink: (resp, values) => '/warehouse/operation-type/operation-type-admin'
            },
            save: {
                queryId: 25,
                postLink: (resp, values) => '/warehouse/operation-type/operation-type-admin'
            },
            fields: [
                { name: 'description', label: 'Descripcion', column: 4 },
                { name: 'sunatCode', label: 'Cod. Sunat', column: 4 },
                { type: 'radio', name: 'movementType', label: 'Tipo Movimiento', column: 4,
                    load: {
                        dataList: [
                            { label: 'INGRESO', value: 1 },
                            { label: 'SALIDA', value: 2 },
                        ]
                    }
                },
                { type: 'select', name: 'companyId', label: 'Empresa', column: 4, load: { queryId: 554, } },
            ],
            onComponentLoad: (formApi, pageApi) => {
                if (pageApi.getPageMode() !== PAGE_MODE.NEW)
                    formApi.load();
            }
        },
        bar: {
            fields: [
                { ...BUTTON_DEFAULT.SAVE, onClick: () => getForm().save() },
                { ...BUTTON_DEFAULT.UPDATE, onClick: () => getForm().update() },
                { ...BUTTON_DEFAULT.RETURN, link: '/warehouse/operation-type/operation-type-admin' }
            ],
        },
        grid: {}
    };
}