import React from 'react'
import { Switch, Route, useRouteMatch } from "react-router-dom"

import { OperationTypeAdminPage } from './admin/OperationTypeAdminPage';
import { OperationTypeDetailPage } from './detail/OperationTypeDetailPage';

const OperationTypeRoutes = () => {
    const { path } = useRouteMatch();
    return (
        <Switch>
            <Route exact path={`${path}/operation-type-admin`} component={OperationTypeAdminPage} />
            <Route path={`${path}/operation-type`} component={OperationTypeDetailPage} />
            <Route path={`${path}/operation-type/:id`} component={OperationTypeDetailPage} />
        </Switch>
    )
}
export default OperationTypeRoutes;