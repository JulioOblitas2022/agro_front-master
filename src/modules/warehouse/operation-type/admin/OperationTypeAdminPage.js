import React from 'react';
import BarButtonFw, { BUTTON_DEFAULT } from '../../../../framework/bar-button/BarButtonFw';
import FormFw from '../../../../framework/form/FormFw';
import GridFw from '../../../../framework/grid/GridFw';
import { BasePage } from '../../../../framework/pages/BasePage';
import { COLUMN_DEFAULT } from "../../../../framework/grid/GridFw";
import { getFormDefaultParams } from "../../../shared/util/getFormDefaultParams";
import { getGridServerDefaultParams } from "../../../shared/util/getGridServerDefaultParams";

export class OperationTypeAdminPage extends BasePage {

    createPageProperties = (pageApi) => {
        return {
            title: 'Tipo Operacion',
            barHeader: {
                fields:[
                    {...BUTTON_DEFAULT.NEW, label:'Nuevo', link: '/warehouse/operation-type/operation-type/new'}
                ]
            }
        };
    }

    renderPage(pageApi) {
        const getGrid = () => pageApi.getComponent('grid');
        const getForm = () => pageApi.getComponent('form');

        const form = {
            ...getFormDefaultParams(),
            fields: [
                { name: 'description', label: 'Descripcion', column: 4 },
                { name: 'sunatCode', label: 'Cod. Sunat', column: 4 },
                {
                    type: 'select', name: 'movementType', label: 'Tipo Movimiento', column: 4,
                    load: {
                        dataList: [
                            { label: 'INGRESO', value: '1' },
                            { label: 'SALIDA', value: '2' },
                        ]
                    }
                },
                {
                    type: 'select', name: 'companyId', label: 'Empresa', column: 4,
                    load: {
                        queryId: 554,
                    }
                },
            ]
        }

        const bar = {
            fields: [
                { ...BUTTON_DEFAULT.SEARCH, onClick: () => getGrid().load({ params: getForm().getData() }) },
                { ...BUTTON_DEFAULT.CLEAR, onClick: () => getForm().reset() }
            ]
        }

        const grid = {
            ...getGridServerDefaultParams({ queryLoad: 24 }),
            fields: [
                { name: 'description', label: 'Descripcion', sorter: { multiple: 1 } },
                { name: 'sunatCode', label: 'Cod. Sunat', sorter: { multiple: 2 } },
                { name: 'movementText', label: 'Tipo Movimiento', },
                { name: 'companyName', label: 'EMPRESA', },
                { ...COLUMN_DEFAULT.VIEW, link: (value, row, index) => `/warehouse/operation-type/operation-type/id-${row.operationTypeId}`, label: 'Opciones', colSpan: 2 },
                { ...COLUMN_DEFAULT.EDIT, link: (value, row, index) => `/warehouse/operation-type/operation-type/id-${row.operationTypeId}/update`, colSpan: 0 },
                // {...COLUMN_DEFAULT.DELETE, 
                //     process: {
                //         fnOk: (result)=>getGrid().load({params:getForm().getData()}),
                //         filter: (value, row, index) => ({queryId: 27, params: {operationTypeId: row.operationTypeId}}), 
                //         message: (resp, values) => {
                //             console.log(resp);
                //             console.log(values);
                //             return `No se puede Eliminar el tipo Operación se está usando en otro módulo`;
                //         },
                //     }
                // }
            ]
        }

        return (
            <>
                <FormFw
                    name="form"
                    getParentApi={pageApi.getApi}
                    config={form}
                />
                <BarButtonFw
                    name="bar"
                    getParentApi={pageApi.getApi}
                    config={bar}
                />
                <GridFw
                    name="grid"
                    getParentApi={pageApi.getApi}
                    config={grid}
                />
            </>
        )
    }
}

