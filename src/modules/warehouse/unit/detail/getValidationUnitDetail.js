
export const getValidationUnitDetail = () => {
    return {
        form: {
            description: [ {required: true, message: 'Descripción es requerido' } ],
            abbreviation: [ {required: true, message: 'Abreviatura es requerido' } ],
            sunatCode: [ {required: true, message: 'Código Sunat es requerido' } ],
            companyId: [ {required: true, message: 'Empresa es requerido' } ],
        },
        grid: {

        },
        button: {

        },
        bar: {

        }
    }
}
