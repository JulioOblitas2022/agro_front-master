import { PAGE_MODE } from '../../../../constants/constants';
import { BUTTON_DEFAULT } from '../../../../framework/bar-button/BarButtonFw';

export const getConfigUnitDetail = (parent) => {

    const getForm = () => parent.getComponent('form');
    

    return {
        form: {
            load:{
                queryId: 14,
                params: {unitId: parent.getParamId()},
            },
            update:{
                queryId: 18,
                postLink: (resp, values) => '/warehouse/unit/unit-admin'
            },
            save:{
                queryId: 12,
                postLink: (resp, values) => '/warehouse/unit/unit-admin'
            },
            fields:[
                {name:'description', label:'Descripción', column: 4},
                {name:'abbreviation', label:'Abreviatura', column: 4},
                {name:'sunatCode', label:'Código Sunat', column: 4},
                { type: 'select', name: 'companyId', label: 'Empresa', column: 4, load: { queryId: 554 }, search: true },
                
            ],
            onComponentLoad : (formApi, pageApi) =>{
                if (pageApi.getPageMode() !== PAGE_MODE.NEW)
                    formApi.load();
            }
        },
        bar: {
            fields: [
                {...BUTTON_DEFAULT.SAVE, onClick: () => getForm().save()},
                {...BUTTON_DEFAULT.UPDATE, onClick: () => getForm().update()},
                {...BUTTON_DEFAULT.RETURN, link: '/warehouse/unit/unit-admin'}
            ],
        },
        grid: {}
    };
}