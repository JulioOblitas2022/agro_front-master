import React from 'react';
import BarButtonFw from '../../../../framework/bar-button/BarButtonFw';
import FormFw from '../../../../framework/form/FormFw';
import { BasePage } from '../../../../framework/pages/BasePage';
import { getConfigUnitDetail } from './getConfigUnitDetail';
import { getModeUnitDetail } from './getModeUnitDetail';
import { getValidationUnitDetail } from './getValidationUnitDetail';


export class UnitDetailPage extends BasePage {
    
    createPageProperties = (parent) => {
        return {
            title: 'Detalle Unidades',
            validation: getValidationUnitDetail,
            mode: getModeUnitDetail
        };
    }

    renderPage(pageApi) {
        const config = getConfigUnitDetail(pageApi);
          
        return (
            <>
                <FormFw
                    name="form"
                    getParentApi={pageApi.getApi}
                    config={config.form}
                />
                <BarButtonFw
                    name="bar"
                    getParentApi={pageApi.getApi}
                    config={config.bar}
                />
            </>
        )
    }
}


