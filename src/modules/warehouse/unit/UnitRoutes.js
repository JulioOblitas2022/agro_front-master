import React from 'react'
import { Switch, Route, useRouteMatch } from "react-router-dom"

import { UnitAdminPage } from './admin/UnitAdminPage'
import { UnitDetailPage } from './detail/UnitDetailPage'

const UnitRoutes = () => {
    const { path } = useRouteMatch();
    return (
        <Switch>
            <Route exact path={`${path}/unit-admin`} component={UnitAdminPage} />
            <Route path={`${path}/unit`} component={UnitDetailPage} />
            <Route path={`${path}/unit/:id`} component={UnitDetailPage} />
        </Switch>
    )
}
export default UnitRoutes;