import React from 'react';
import { Route, Switch, useRouteMatch } from 'react-router-dom';

import { OutputsSeveralAdminPage } from './admin/OutputsSeveralAdminPage';
import { OutputsSeveralDetailPage } from './detail/OutputsSeveralDetailPage';

const OutputsSeveralRoutes = () => {
    const { path } = useRouteMatch();
    return (
        <Switch>
            <Route exact path={`${path}/outputs-several-admin`} component={OutputsSeveralAdminPage} />
            <Route path={`${path}/outputs-several`} component={OutputsSeveralDetailPage} />
            <Route path={`${path}/outputs-several/:id`} component={OutputsSeveralDetailPage} />
        </Switch>
    )
}

export default OutputsSeveralRoutes;