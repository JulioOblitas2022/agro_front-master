
export const getValidationOutputsSeveralDetail = () => {
    return {
        form: {
            entryDate: [ {required: true, message: 'Fecha Entrada es requerido' } ],
            docDate: [ {required: true, message: 'fecha Doc. es requerido' } ],
            operationTypeId: [ {required: true, message: 'Tipo operacion es requerido' } ],
            docRefId: [ {required: true, message: 'Ref. Doc es requerido' } ],
            docRefNumber: [ {required: true, message: 'Nro. Doc es requerido' } ],
            applicantId: [ {required: true, message: 'Solicitante es requerido' } ],
        },
        grid: {

        },
        button: {

        },
        bar: {

        }
    }
}
