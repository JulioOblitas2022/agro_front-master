import React from 'react'
import { DATE_FORMAT } from '../../../../../constants/constants';
import { BUTTON_DEFAULT } from '../../../../../framework/bar-button/BarButtonFw';
import GridFw, { COLUMN_DEFAULT } from '../../../../../framework/grid/GridFw';
import { ProductModal } from '../modals/ProductModal';

export const createOutputServeralSection = (pageApi) => {

    const getGrid = () => pageApi.getComponent('outputSeveralGrid');

    const fnDelete = (value, row, index) => {
        getGrid().deleteItem(row.key);      
    }

    const itemConfig = {
        events: {
            afterAddEvent : (data) => {
                getGrid().addItem(data);
            }
        }
    }

    const ItemModalConfig = (data) =>  {
        return {
            width: 1600,
            maskClosable: false,
            component: <ProductModal name={data}  getParentApi={pageApi.getApi} config={itemConfig}  />
        }
    };


    const barHeaderConfig = {
        fields:[
            {...BUTTON_DEFAULT.ADD, label:'Agregar Item', onClick: ()=>{  pageApi.showModal(ItemModalConfig('')); }}
        ]
    }

    const gridConfig = {
        title: 'Otros items registrados',
        rowId: 'outputSeveralDetailId',
        showIndex: true,
        pagination: true,
        autoLoad: true,
        collapsible: true,
        load: {
            validation: () => pageApi.getParamId()!=null, //solo se dispara si el valor es distinto de nulo
            queryId: 180,
            //orderBy: 'outputSeveralDetailId desc',
            params: {outputSeveralId: pageApi.getParamId()},
            hideMessage: true, 
            hideMessageError: true
        },
        save: {
            queryId: 181,
            params: {outputSeveralId: pageApi.getParamId()},
            hideMessage: true, 
            hideMessageError: true
        },
        fields: [
            {name:'itemTypeText', label:'Tipo Item'},
            {name:'description', label:'Item'},
            { type: 'select', name: 'unitMeasureId', label: 'Uni. Med.', labelName: 'unitMeasureIdDes', load: { queryId: 568, params: () => {return {masterItemId: getGrid()?.itemEditing?.masterItemId}}}, search: true, editable: true },
            {name:'lot', label:'Lote', editable: true},
            {type: 'number', name:'quantityRequired', label:'Cantidad Requerida', editable: true},
            {type: 'date', name:'deliveryTime', label:'Hora Entrega', format: DATE_FORMAT.HHmm, inputFormat: DATE_FORMAT.HHmm, editable: true},
            {type: 'select', name:'warehouseId', label:'Almacén', editable: true, load: { queryId: 34 }, labelName:'warehouseIdDes' },
            {...COLUMN_DEFAULT.DELETE, onClick: fnDelete}
        ],
        barHeader: barHeaderConfig
    };

    return (
        <GridFw
            name="outputSeveralGrid"
            getParentApi={pageApi.getApi}
            config={gridConfig} 
        />
    )
}