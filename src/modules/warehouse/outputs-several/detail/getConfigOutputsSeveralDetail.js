import { DATE_FORMAT, PAGE_MODE } from '../../../../constants/constants';
import { BUTTON_DEFAULT } from '../../../../framework/bar-button/BarButtonFw';
import { B_SELECT_CONSTANTS } from '../../../shared/constantes/BSelectConstants';

export const getConfigOutputsSeveralDetail = (pageApi) => {
    const getForm = () => pageApi.getComponent('form');
    const getOutputSeveralGrid = () => pageApi.getComponent('outputSeveralGrid.grid');

    return {
        form: {
            load:{
                queryId: 177,
                params: {outputSeveralId: pageApi.getParamId()},
            },
            update:{
                queryId: 179,
                postLink: (resp, values) => '/warehouse/outputs-several/outputs-several-admin',
                fnOk: (resp) => {
                    getOutputSeveralGrid().save();
                }
            },
            save:{
                queryId: 178,
                postLink: (resp, values) => '/warehouse/outputs-several/outputs-several-admin',
                fnOk: (resp) => {
                    const outputSeveralId = resp?.dataObject;
                    if (outputSeveralId){
                        getOutputSeveralGrid().save({params:{outputSeveralId}});
                    }
                        
                }
            },
            title: 'Datos Principales',
            fields:[
                {type: 'date', name: 'departureDate', label:'Fecha Salida', format: DATE_FORMAT.DDMMYYYY, inputFormat: DATE_FORMAT.YYYYMMDD, column: 4},
                {type: 'date', name: 'docDate',label:'Fecha Documento', format: DATE_FORMAT.DDMMYYYY, inputFormat: DATE_FORMAT.YYYYMMDD, column: 4},
                {type: 'select', name:'operationTypeId', label:'Tipo Operacion', column: 4, load: { queryId: 77 }},
                {type:'select', name:'docRefId', label:'Doc. Referencia', column: 4, load: {queryId: 195}},
                {name:'docRefNumber', label:'Nro. Documento', column: 4},
                {type: 'select', name:'applicantId', label:'Solicitante', column: 4, search: true, load: { queryId: 309 } },
                {type: 'textarea', name: 'observations', label:'Observaciones', column: 12},
            ],
            onComponentLoad : (formApi, pageApi) =>{
                if (pageApi.getPageMode() !== PAGE_MODE.NEW)
                    formApi.load();
            }
        },
        bar: {
            fields: [
                {...BUTTON_DEFAULT.SAVE, onClick: () => getForm().save()},
                {...BUTTON_DEFAULT.UPDATE, onClick: () => getForm().update()},
                {...BUTTON_DEFAULT.RETURN, link: '/warehouse/outputs-several/outputs-several-admin'}
            ],
        },
        grid: {}
    }
}