import React from 'react'
import BarButtonFw from '../../../../framework/bar-button/BarButtonFw';
import FormFw from '../../../../framework/form/FormFw';
import { BasePage } from '../../../../framework/pages/BasePage';
import { getConfigOutputsSeveralDetail } from './getConfigOutputsSeveralDetail';
import { getModeOutputsSeveralDetail } from './getModeOutputsSeveralDetail';
import { getValidationOutputsSeveralDetail } from './getValidationOutputsSeveralDetail';
import { createOutputServeralSection } from './sections/OutputServeralSection';

export class OutputsSeveralDetailPage extends BasePage {

    createPageProperties = (pageApi) => {
        return {
            title: 'Registro de salidas',
            validation: getValidationOutputsSeveralDetail,
            mode: getModeOutputsSeveralDetail
        };
    }

    renderPage(pageApi) {

        const config = getConfigOutputsSeveralDetail(pageApi);

        return (
            <>
                <FormFw
                    name="form"
                    getParentApi={pageApi.getApi}
                    config={config.form}
                />
                {createOutputServeralSection(pageApi)}
                <BarButtonFw
                    name="bar"
                    getParentApi={pageApi.getApi}
                    config={config.bar}
                />
            </>
        )
    }
}