import React from 'react'
import BarButtonFw, { BUTTON_DEFAULT } from '../../../../framework/bar-button/BarButtonFw';
import FormFw from '../../../../framework/form/FormFw';
import GridFw from '../../../../framework/grid/GridFw';
import { BasePage } from '../../../../framework/pages/BasePage';
import { COLUMN_DEFAULT } from "../../../../framework/grid/GridFw";
import { getFormDefaultParams } from "../../../shared/util/getFormDefaultParams";
import { getGridServerDefaultParams } from "../../../shared/util/getGridServerDefaultParams";
import { DATE_FORMAT } from '../../../../constants/constants';

export class OutputsSeveralAdminPage extends BasePage {

    createPageProperties = (pageApi) => {
        return {
            title: 'Bandeja de salida varios',
            barHeader: {
                fields:[
                    {...BUTTON_DEFAULT.NEW, label:'Nueva Salida', link: '/warehouse/outputs-several/outputs-several/new'}
                ]
            }
        };
    }

    renderPage(pageApi) {

        const getGrid = () => pageApi.getComponent('grid');
        const getForm = () => pageApi.getComponent('form');
    

        const form = {
            ...getFormDefaultParams(),
            fields:[
                {name:'code', label:'Codigo', column: 4},
                {name:'description', label:'Descripcion', column: 4},
            ]
        }

        const bar = { fields: [
            {...BUTTON_DEFAULT.SEARCH, onClick: ()=>getGrid().load({params:getForm().getData()})},
            {...BUTTON_DEFAULT.CLEAR, onClick: ()=>getForm().reset()}
        ]}

        const grid = {
            ...getGridServerDefaultParams({queryLoad: 176}),
            fields:[
                {name: 'correlative', label: 'Correlativo', sorter: {multiple: 1}},
                {type: 'date', name: 'docDate', label: 'Fecha Doc', format: DATE_FORMAT.DDMMYYYY},
                {name: 'applicantId', label: 'Solicitante'},
                {type: 'date', name: 'departureDate', label: 'Fch. Salida', format: DATE_FORMAT.DDMMYYYY},
                {name: 'docRefId', label: 'Tipo Doc. ref.'},
                {name: 'docRefNumber', label: 'Nro. Doc. Ref.'},
                {...COLUMN_DEFAULT.VIEW, link: (value, row, index) => `/warehouse/outputs-several/outputs-several/${row.outputSeveralId}`, label: 'Opciones', colSpan: 2 },
                {...COLUMN_DEFAULT.EDIT, link: (value, row, index) => `/warehouse/outputs-several/outputs-several/${row.outputSeveralId}/update`, colSpan: 0 },
            ]
        }

        return (
            <>
                <FormFw
                    name="form"
                    getParentApi={pageApi.getApi}
                    config={form}
                />
                <BarButtonFw
                    name="bar"
                    getParentApi={pageApi.getApi}
                    config={bar}
                />
                <GridFw
                    name="grid"
                    getParentApi={pageApi.getApi}
                    config={grid}
                />
            </>
        )
    }
}