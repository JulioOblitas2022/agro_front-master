import { BUTTON_DEFAULT } from '../../../../framework/bar-button/BarButtonFw';
import { COLUMN_DEFAULT } from '../../../../framework/grid/GridFw';
import { getFormDefaultParams } from '../../../shared/util/getFormDefaultParams';
import { getGridServerDefaultParams } from '../../../shared/util/getGridServerDefaultParams';

export const getConfigWhouseSerieAdmin = (pageApi) => {
    const getGrid = () => pageApi.getComponent('grid');
    const getForm = () => pageApi.getComponent('form');

    return {
        form: {
            ...getFormDefaultParams(),
            fields: [
                { type: 'select', name: 'local', label: 'Local', column: 4, load: { queryId: 198 }, search: true },
                { type: 'select', name: 'warehouseId', label: 'Almacen', column: 4, load: { queryId: 34 }, search: true },
                { type: 'select', name: 'documentType', label: 'Tipo de Documento', column: 4, load: { queryId: 195 }, search: true },
                { name: 'serialNumber', label: 'Numero de serie', column: 4 },
                { type: 'select', name: 'companyId', label: 'Empresa', column: 4, load: { queryId: 554 } },
            ]
        },
        bar: {
            fields: [
                { ...BUTTON_DEFAULT.SEARCH, onClick: () => getGrid().load({ params: getForm().getData() }) },
                { ...BUTTON_DEFAULT.CLEAR, onClick: () => getForm().reset() }
            ]
        },
        grid: {
            ...getGridServerDefaultParams({ queryLoad: 35 }),
            fields: [
                { name: 'localText', label: 'Local', sorter: { multiple: 1 } },
                { name: 'warehouse', label: 'Almacen' },
                { name: 'documentText', label: 'Tipo de Documento' },
                { name: 'serialNumber', label: 'Numero de serie', },
                { name: 'companyName', label: 'EMPRESA', },
                { ...COLUMN_DEFAULT.VIEW, link: (value, row, index) => `/warehouse/whouse-serie/whouse-serie/id-${row.warehouseSerieId}` },
                { ...COLUMN_DEFAULT.EDIT, link: (value, row, index) => `/warehouse/whouse-serie/whouse-serie/id-${row.warehouseSerieId}/update` },
                { ...COLUMN_DEFAULT.DELETE, process: { fnOk: (result) => getGrid().load({ params: getForm().getData() }), filter: (value, row, index) => ({ queryId: 39, params: { warehouseSerieId: row.warehouseSerieId } }) } }
            ]
        }
    }
}
