import React from 'react'
import { Route, Switch, useRouteMatch } from 'react-router-dom'

import { WhouseSerieAdminPage } from './admin/WhouseSerieAdminPage';
import { WhouseSerieDetailPage } from './detail/WhouseSerieDetailPage';

const WhouseSerieRoutes = () => {
    const { path } = useRouteMatch();
    return (
        <Switch>
            <Route exact path={`${path}/whouse-serie-admin`} component={WhouseSerieAdminPage} />
            <Route path={`${path}/whouse-serie`} component={WhouseSerieDetailPage} />
            <Route path={`${path}/whouse-serie/:id`} component={WhouseSerieDetailPage} />
        </Switch>
    )
}

export default WhouseSerieRoutes;
