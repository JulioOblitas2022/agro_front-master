
export const getValidationWhouseSerieDetail = () => {
    return {
        form: {
            local: [{ required: true, message: 'Local es requerido' }],
            warehouseId: [{ required: true, message: 'Almacen es requerido' }],
            documentType: [{ required: true, message: 'Tipo de Documento es requerido' }],
            serialNumber: [{ required: true, message: 'Numero de Serie es a' }],
            companyId: [{ required: true, message: 'Empresa es requerido' }],
        },
        grid: {

        },
        button: {

        },
        bar: {

        }
    }
}
