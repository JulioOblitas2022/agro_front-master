import React from 'react'
import BarButtonFw from '../../../../framework/bar-button/BarButtonFw';
import FormFw from '../../../../framework/form/FormFw';
import { BasePage } from '../../../../framework/pages/BasePage';
import { getConfigWhouseSerieDetail } from './getConfigWhouseSerieDetail';
import { getModeWhouseSerieDetail } from './getModeWhouseSerieDetail';
import { getValidationWhouseSerieDetail } from './getValidationWhouseSerieDetail';

export class WhouseSerieDetailPage extends BasePage {

    createPageProperties = (pageApi) => {
        return {
            title: 'Detalle Series por Almacen',
            validation: getValidationWhouseSerieDetail,
            mode: getModeWhouseSerieDetail
        };
    }

    renderPage(pageApi) {

        const config = getConfigWhouseSerieDetail(pageApi);

        return (
            <>
                <FormFw
                    name="form"
                    getParentApi={pageApi.getApi}
                    config={config.form}
                />
                <BarButtonFw
                    name="bar"
                    getParentApi={pageApi.getApi}
                    config={config.bar}
                />
            </>
        )
    }
}
