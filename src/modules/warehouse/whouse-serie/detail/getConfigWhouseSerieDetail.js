import { PAGE_MODE } from '../../../../constants/constants';
import { BUTTON_DEFAULT } from '../../../../framework/bar-button/BarButtonFw';

export const getConfigWhouseSerieDetail = (pageApi) => {
    const getForm = () => pageApi.getComponent('form');

    return {
        form: {
            load: {
                queryId: 36,
                params: { warehouseSerieId: pageApi.getParamId() },
            },
            update: {
                queryId: 38,
                postLink: (resp, values) => '/warehouse/whouse-serie/whouse-serie-admin'
            },
            save: {
                queryId: 37,
                postLink: (resp, values) => '/warehouse/whouse-serie/whouse-serie-admin'
            },
            fields: [
                { type: 'select', name: 'local', label: 'Local', column: 6, load: { queryId: 198 }, search: true },
                { type: 'select', name: 'warehouseId', label: 'Almacen', column: 6, load: { queryId: 34 }, search: true },
                { type: 'select', name: 'documentType', label: 'Tipo de Documento', column: 6, load: { queryId: 195 }, search: true },
                { name: 'serialNumber', label: 'Numero de serie', column: 6 },
                { type: 'select', name: 'companyId', label: 'Empresa', column: 6, load: { queryId: 554 } },
            ],
            onComponentLoad: (formApi, pageApi) => {
                if (pageApi.getPageMode() !== PAGE_MODE.NEW)
                    formApi.load();
            }
        },
        bar: {
            fields: [
                { ...BUTTON_DEFAULT.SAVE, onClick: () => getForm().save() },
                { ...BUTTON_DEFAULT.UPDATE, onClick: () => getForm().update() },
                { ...BUTTON_DEFAULT.RETURN, link: '/warehouse/whouse-serie/whouse-serie-admin' }
            ],
        },
        grid: {}
    };
}
