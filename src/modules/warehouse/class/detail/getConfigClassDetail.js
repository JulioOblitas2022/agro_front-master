import { PAGE_MODE } from "../../../../constants/constants";
import { BUTTON_DEFAULT } from "../../../../framework/bar-button/BarButtonFw";
import { B_SELECT_CONSTANTS } from '../../../shared/constantes/BSelectConstants';

export const getConfigClassDetail = (pageApi) => {

    const getForm = () => pageApi.getComponent('form');

    return {
        form: {
            load: {
                queryId: 46,
                params: { classId: pageApi.getParamId() },
            },
            update: {
                queryId: 48,
                postLink: (resp, values) => '/warehouse/class/class-admin'
            },
            save: {
                queryId: 47,
                postLink: (resp, values) => '/warehouse/class/class-admin'
            },
            fnOk: (resp) => {
                console.log(resp)
            },
            fields: [
                { type: 'select', name: 'typeExistence', label: 'Tipo de Existencia', column: 4, load: { queryId: 185 } },
                { ...B_SELECT_CONSTANTS.FAMILY, column: 4 },
                { name: 'description', label: 'Descripcion', column: 4 },
                { name: 'prefix', label: 'Prefijo', column: 4, },
                { type: 'select', name: 'companyId', label: 'Empresa', column: 4, load: { queryId: 554 }, search: true },
            ],
            onComponentLoad: (formApi, pageApi) => {
                if (pageApi.getPageMode() !== PAGE_MODE.NEW)
                    formApi.load();
            }
        },
        bar: {
            fields: [
                { ...BUTTON_DEFAULT.SAVE, onClick: () => getForm().save() },
                { ...BUTTON_DEFAULT.UPDATE, onClick: () => getForm().update() },
                { ...BUTTON_DEFAULT.RETURN, link: '/warehouse/class/class-admin' }
            ],
        },
        grid: {}
    };
}
