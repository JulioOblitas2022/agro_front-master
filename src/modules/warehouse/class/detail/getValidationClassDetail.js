export const getValidationClassDetail = () => {
    return {
        form: {
            typeExistence: [{ required: true, message: 'Tipo de existencia es requerida' }],
            familyId: [{ required: true, message: 'Familia es requerida' }],
            description: [{ required: true, message: 'Descripcion es requerida' }],
            prefix: [{ required: true, message: 'Prefijo es requerido' }],
            companyId: [{ required: true, message: 'Empresa es requerido' }],
        },
        grid: {

        },
        button: {

        },
        bar: {

        }
    }
}
