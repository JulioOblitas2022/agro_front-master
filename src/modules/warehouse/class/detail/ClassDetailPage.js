import React from 'react'
import BarButtonFw from '../../../../framework/bar-button/BarButtonFw';
import FormFw from '../../../../framework/form/FormFw';
import { BasePage } from '../../../../framework/pages/BasePage';
import { getConfigClassDetail } from './getConfigClassDetail';
import { getModeClassDetail } from './getModeClassDetail';
import { getValidationClassDetail } from './getValidationClassDetail';

export class ClassDetailPage extends BasePage {
    createPageProperties = (pageApi) => {
        return {
            title: 'Detalle Clase',
            validation: getValidationClassDetail,
            mode: getModeClassDetail
        };
    }

    renderPage(pageApi) {

        const config = getConfigClassDetail(pageApi);

        return (
            <>
                <FormFw
                    name="form"
                    getParentApi={pageApi.getApi}
                    config={config.form}
                />
                <BarButtonFw
                    name="bar"
                    getParentApi={pageApi.getApi}
                    config={config.bar}
                />
            </>
        )
    }
}
