import React from 'react';
import { Route, Switch, useRouteMatch } from 'react-router-dom';

import { ClassAdminPage } from './admin/ClassAdminPage';
import { ClassDetailPage } from './detail/ClassDetailPage';

const ClassRoutes = () => {
    const { path } = useRouteMatch();
    return (
        <Switch>
           <Route exact path={`${path}/class-admin`} component={ClassAdminPage} />
            <Route path={`${path}/class`} component={ClassDetailPage} />
            <Route path={`${path}/class/:id`} component={ClassDetailPage} /> 
        </Switch>
    )
}

export default ClassRoutes;