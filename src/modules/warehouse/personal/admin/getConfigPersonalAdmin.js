import { BUTTON_DEFAULT } from "../../../../framework/bar-button/BarButtonFw";
import { COLUMN_DEFAULT } from "../../../../framework/grid/GridFw";
import { getFormDefaultParams } from "../../../shared/util/getFormDefaultParams";
import { getGridServerDefaultParams } from "../../../shared/util/getGridServerDefaultParams";

export const getConfigPersonalAdmin = (pageApi) => {
    const getGrid = () => pageApi.getComponent('grid');
    const getForm = () => pageApi.getComponent('form');

    return {
        form: {
            ...getFormDefaultParams(),
            fields: [
                { type: 'select', name: 'employeeId', label: 'Empleado', column: 4, load: { queryId: 309 }, search: true },
                { type: 'select', name: 'position', label: 'Cargo', column: 4, load: { queryId: 374 }, search: true },
                { type: 'select', name: 'companyId', label: 'Empresa', column: 4, load: { queryId: 554 } },
            ]
        },
        bar: {
            fields: [
                { ...BUTTON_DEFAULT.SEARCH, onClick: () => getGrid().load({ params: getForm().getData() }) },
                { ...BUTTON_DEFAULT.CLEAR, onClick: () => getForm().reset() }
            ]
        },
        grid: {
            ...getGridServerDefaultParams({ queryLoad: 58 }),
            fields: [
                { name: 'employee', label: 'Empleado' },
                { name: 'positionText', label: 'Cargo' },
                { name: 'state', label: 'Estado', },
                { name: 'companyName', label: 'EMPRESA', },
                { ...COLUMN_DEFAULT.VIEW, link: (value, row, index) => `/warehouse/personal/personal/id-${row.personalId}` },
                { ...COLUMN_DEFAULT.EDIT, link: (value, row, index) => `/warehouse/personal/personal/id-${row.personalId}/update` },
                { ...COLUMN_DEFAULT.DELETE, process: { fnOk: (result) => getGrid().load({ params: getForm().getData() }), filter: (value, row, index) => ({ queryId: 61, params: { personalId: row.personalId } }) } }
            ]
        }
    }
}
