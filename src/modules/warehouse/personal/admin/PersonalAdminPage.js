import React from 'react'
import BarButtonFw, { BUTTON_DEFAULT } from '../../../../framework/bar-button/BarButtonFw';
import FormFw from '../../../../framework/form/FormFw';
import GridFw from '../../../../framework/grid/GridFw';
import { BasePage } from '../../../../framework/pages/BasePage'
import { COLUMN_DEFAULT } from "../../../../framework/grid/GridFw";
import { getFormDefaultParams } from "../../../shared/util/getFormDefaultParams";
import { getGridServerDefaultParams } from "../../../shared/util/getGridServerDefaultParams";

export class PersonalAdminPage extends BasePage {
    
    createPagePropertiesBase(pageApi) {
        return {
            title: 'Personal',
            barHeader: {
                fields:[
                    {...BUTTON_DEFAULT.NEW, label:'Nuevo', link: '/warehouse/personal/personal/new'}
                ]
            }
        };
    }

    renderPage(pageApi) {
        const getGrid = () => pageApi.getComponent('grid');
        const getForm = () => pageApi.getComponent('form');

        const form = {
            ...getFormDefaultParams(),
            fields: [
                { type: 'select', name: 'employeeId', label: 'Empleado', column: 4, load: { queryId: 309 }, search: true },
                { type: 'select', name: 'position', label: 'Cargo', column: 4, load: { queryId: 374 }, search: true },
                { type: 'select', name: 'companyId', label: 'Empresa', column: 4, load: { queryId: 554 } },
            ]
        }

        const bar = {
            fields: [
                { ...BUTTON_DEFAULT.SEARCH, onClick: () => getGrid().load({ params: getForm().getData() }) },
                { ...BUTTON_DEFAULT.CLEAR, onClick: () => getForm().reset() }
            ]
        }

        const grid = {
            ...getGridServerDefaultParams({ queryLoad: 58 }),
            fields: [
                { name: 'employee', label: 'Empleado' },
                { name: 'positionText', label: 'Cargo' },
                { name: 'state', label: 'Estado', },
                { name: 'companyName', label: 'EMPRESA', },
                { ...COLUMN_DEFAULT.VIEW, link: (value, row, index) => `/warehouse/personal/personal/id-${row.personalId}`, label: 'Opciones', colSpan: 3 },
                { ...COLUMN_DEFAULT.EDIT, link: (value, row, index) => `/warehouse/personal/personal/id-${row.personalId}/update`, colSpan: 0 },
                { ...COLUMN_DEFAULT.DELETE, process: { fnOk: (result) => getGrid().load({ params: getForm().getData() }), filter: (value, row, index) => ({ queryId: 61, params: { personalId: row.personalId } }) }, colSpan: 0 }
            ]
        }
        return (
            <>
                <FormFw
                    name="form"
                    getParentApi={pageApi.getApi}
                    config={form}
                />
                <BarButtonFw
                    name="bar"
                    getParentApi={pageApi.getApi}
                    config={bar}
                />
                <GridFw
                    name="grid"
                    getParentApi={pageApi.getApi}
                    config={grid}
                />
            </>
        )
    }
}
