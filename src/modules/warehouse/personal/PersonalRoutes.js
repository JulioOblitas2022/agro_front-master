import React from 'react';
import { Route, Switch, useRouteMatch } from 'react-router-dom';

import { PersonalAdminPage } from './admin/PersonalAdminPage';
import { PersonalDetailPage } from './detail/PersonalDetailPage';

const PersonalRoutes = () => {
    const { path } = useRouteMatch();
    return (
        <Switch>
            <Route exact path={`${path}/personal-admin`} component={PersonalAdminPage} />
            <Route path={`${path}/personal`} component={PersonalDetailPage} />
            <Route path={`${path}/personal/:id`} component={PersonalDetailPage} />
        </Switch>
    )
}

export default PersonalRoutes;
