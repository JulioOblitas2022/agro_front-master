export const getValidationPersonalDetail = () => {
    return {
        form: {
            employeeId: [{ required: true, message: 'Empleado es requerido' }],
            position: [{ required: true, message: 'Cargo es requerido' }],
            active: [{ required: true, message: 'Estado es requerido' }],
            companyId: [{ required: true, message: 'Empresa es requerido' }],
        },
        grid: {

        },
        button: {

        },
        bar: {

        }
    }
}
