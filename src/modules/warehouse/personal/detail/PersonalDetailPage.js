import React from 'react'
import BarButtonFw from '../../../../framework/bar-button/BarButtonFw';
import FormFw from '../../../../framework/form/FormFw';
import { BasePage } from '../../../../framework/pages/BasePage';
import { getConfigPersonalDetail } from './getConfigPersonalDetail';
import { getModePersonalDetail } from './getModePersonalDetail';
import { getValidationPersonalDetail } from './getValidationPersonalDetail';

export class PersonalDetailPage extends BasePage {
    createPageProperties = (pageApi) => {
        return {
            title: 'Detalle Personal',
            validation: getValidationPersonalDetail,
            mode: getModePersonalDetail
        };
    }

    renderPage(pageApi) {

        const config = getConfigPersonalDetail(pageApi);

        return (
            <>
                <FormFw
                    name="form"
                    getParentApi={pageApi.getApi}
                    config={config.form}
                />
                <BarButtonFw
                    name="bar"
                    getParentApi={pageApi.getApi}
                    config={config.bar}
                />
            </>
        )
    }
}
