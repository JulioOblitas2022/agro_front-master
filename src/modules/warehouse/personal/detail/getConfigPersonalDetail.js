import { PAGE_MODE } from "../../../../constants/constants";
import { BUTTON_DEFAULT } from "../../../../framework/bar-button/BarButtonFw";

export const getConfigPersonalDetail = (pageApi) => {

    const getForm = () => pageApi.getComponent('form');

    return {
        form: {
            load: {
                queryId: 57,
                params: { personalId: pageApi.getParamId() },
            },
            update: {
                queryId: 60,
                postLink: (resp, values) => '/warehouse/personal/personal-admin'
            },
            save: {
                queryId: 59,
                postLink: (resp, values) => '/warehouse/personal/personal-admin'
            },
            fields: [
                { type: 'select', name: 'employeeId', label: 'Empleado', column: 4, load: { queryId: 309 } },
                { type: 'select', name: 'position', label: 'Cargo', column: 4, load: { queryId: 374 } },
                {
                    type: 'radio', name: 'active', label: 'Estado', column: 4,
                    load: {
                        dataList: [
                            { label: 'ACTIVO', value: 1 },
                            { label: 'INACTIVO', value: 2 },
                        ]
                    }
                },
                { type: 'select', name: 'companyId', label: 'Empresa', column: 4, load: { queryId: 554 } },
            ],
            onComponentLoad: (formApi, pageApi) => {
                if (pageApi.getPageMode() !== PAGE_MODE.NEW)
                    formApi.load();
            }
        },
        bar: {
            fields: [
                { ...BUTTON_DEFAULT.SAVE, onClick: () => getForm().save() },
                { ...BUTTON_DEFAULT.UPDATE, onClick: () => getForm().update() },
                { ...BUTTON_DEFAULT.RETURN, link: '/warehouse/personal/personal-admin' }
            ],
        },
        grid: {}
    };
}
