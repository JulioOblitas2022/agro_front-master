import React from 'react'
import BarButtonFw, { BUTTON_DEFAULT } from '../../../../framework/bar-button/BarButtonFw';
import FormFw from '../../../../framework/form/FormFw';
import GridFw from '../../../../framework/grid/GridFw';
import { BasePage } from '../../../../framework/pages/BasePage';
import { DATE_FORMAT } from '../../../../constants/constants';
import { COLUMN_DEFAULT } from '../../../../framework/grid/GridFw';
import { getFormDefaultParams } from '../../../shared/util/getFormDefaultParams';
import { getGridServerDefaultParams } from '../../../shared/util/getGridServerDefaultParams';

export class OutputsAdminPage extends BasePage {

    createPageProperties = (pageApi) => {
        return {
            title: 'Bandeja de salida',
            barHeader: {
                fields:[
                    {...BUTTON_DEFAULT.NEW, label:'Nueva Salida', link: '/warehouse/outputs/outputs/new'}
                ]
            }
        };
    }

    renderPage(pageApi) {

        const getGrid = () => pageApi.getComponent('grid');
        const getForm = () => pageApi.getComponent('form');

        const form = {
            ...getFormDefaultParams(),
            fields: [
                { name: 'documentTypeNumber', label: 'Nro. de Documento', column: 4 },
                { type: 'select', name: 'warehouseId', label: 'Almacén', column: 3, load: { queryId: 34 } },
            ]
        }

        const bar = {
            fields: [
                { ...BUTTON_DEFAULT.SEARCH, onClick: () => getGrid().load({ params: getForm().getData() }) },
                { ...BUTTON_DEFAULT.CLEAR, onClick: () => getForm().reset() }
            ]
        }

        const grid = {
            ...getGridServerDefaultParams({ queryLoad: 170 }),
            fields: [
                { name: 'correlative', label: 'Correlativo', sorter: { multiple: 1 } },
                { type: 'date', name: 'docDate', label: 'Fecha Doc', format: DATE_FORMAT.DDMMYYYY},
                { name: 'docType', label: 'Tipo Doc.'},
                { name: 'docTypeNumber', label: 'N° Documento' },
                { name: 'customer', label: 'Proveedor' },
                { type: 'date', name: 'departureDate', label: 'Fch. Ingreso', format: DATE_FORMAT.DDMMYYYY },
                { name: 'warehouse', label: 'Almacen' },
                { name: 'docRef', label: 'Tipo Doc. ref.' },
                { name: 'docRefNumber', label: 'Nro. Doc. Ref.' },
                { ...COLUMN_DEFAULT.VIEW, link: (value, row, index) => `/warehouse/outputs/outputs/${row.outputId}`, label: 'Opciones', colSpan: 2 },
                { ...COLUMN_DEFAULT.EDIT, link: (value, row, index) => `/warehouse/outputs/outputs/${row.outputId}/update`, colSpan: 0 },
                // {...COLUMN_DEFAULT.DELETE, process: {fnOk: (result)=>getGrid().load({params:getForm().getData()}),filter: (value, row, index) => ({queryId: 0, params: {outputId: row.outputId}}) }}
            ]
        }

        return (
            <>
                <FormFw
                    name="form"
                    getParentApi={pageApi.getApi}
                    config={form}
                />
                <BarButtonFw
                    name="bar"
                    getParentApi={pageApi.getApi}
                    config={bar}
                />
                <GridFw
                    name="grid"
                    getParentApi={pageApi.getApi}
                    config={grid}
                />
            </>
        )
    }
}