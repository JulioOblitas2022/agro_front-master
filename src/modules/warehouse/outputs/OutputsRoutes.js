import React from 'react';
import { Route, Switch, useRouteMatch } from 'react-router-dom';

import { OutputsAdminPage } from './admin/OutputsAdminPage';
import { OutputsDetailPage } from './detail/OutputsDetailPage';

const OutputsRoutes = () => {
    const { path } = useRouteMatch();
    return (
        <Switch>
            <Route exact path={`${path}/outputs-admin`} component={OutputsAdminPage} />
            <Route path={`${path}/outputs`} component={OutputsDetailPage} />
            <Route path={`${path}/outputs/:id`} component={OutputsDetailPage} />
        </Switch>
    )
}

export default OutputsRoutes;
