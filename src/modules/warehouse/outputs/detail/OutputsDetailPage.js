import React from 'react'
import BarButtonFw from '../../../../framework/bar-button/BarButtonFw';
import FormFw from '../../../../framework/form/FormFw';
import { BasePage } from '../../../../framework/pages/BasePage';
import { getConfigOutputsDetail } from './getConfigOutputsDetail';
import { getModeOutputsDetail } from './getModeOutputsDetail';
import { getValidationOutputsDetail } from './getValidationOutputsDetail';
import { createOutputSection } from './sections/OutputSection';

export class OutputsDetailPage extends BasePage {

    createPageProperties = (pageApi) => {
        return {
            title: 'Registro de salidas',
            validation: getValidationOutputsDetail,
            mode: getModeOutputsDetail
        };
    }

    renderPage(pageApi) {

        const config = getConfigOutputsDetail(pageApi);

        return (
            <>
                <FormFw
                    name="form"
                    getParentApi={pageApi.getApi}
                    config={config.form}
                />
                {createOutputSection(pageApi)}
                <BarButtonFw
                    name="bar"
                    getParentApi={pageApi.getApi}
                    config={config.bar}
                />
            </>
        )
    }
}