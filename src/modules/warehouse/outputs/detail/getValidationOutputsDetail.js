export const getValidationOutputsDetail = () => {
    return {
        form: {
            entryDate: [ {required: true, message: 'Fecha Entrada es requerido' } ],
            docDate: [ {required: true, message: 'fecha Doc. es requerido' } ],
            warehouseId: [ {required: true, message: 'Almacen es requerido' } ],
            operationTypeId: [ {required: true, message: 'Tipo operacion es requerido' } ],
            docTypeId: [ {required: true, message: 'Tipo Doc. es requerido' } ],
            docTypeNumber: [ {required: true, message: 'Doc. es requerido' } ],
            customerId: [ {required: true, message: 'Cliente es requerido' } ],
            applicantId: [ {required: true, message: 'Solicitante es requerido' } ],
            docRefId: [ {required: true, message: 'Ref. Doc es requerido' } ],
            docRefNumber: [ {required: true, message: 'Nro. Doc es requerido' } ],
        },
        grid: {

        },
        button: {

        },
        bar: {

        }
    }
}
