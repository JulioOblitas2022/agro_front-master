import { DATE_FORMAT, PAGE_MODE } from '../../../../constants/constants';
import { BUTTON_DEFAULT } from '../../../../framework/bar-button/BarButtonFw';
import { DateUtil } from '../../../../util/DateUtil';
import { MessageUtil } from '../../../../util/MessageUtil';
import { DocReferencesRequerimentModal } from './modals/DocReferencesRequerimentModal';

export const getConfigOutputsDetail = (pageApi) => {
    const getForm = () => pageApi.getComponent('form');
    const getOutputGrid = () => pageApi.getComponent('outputGrid');

    const docRefConfig = {
        events: {
            afterAddEvent: (data) => {

                let itemData = JSON.parse(data[0].items);
                //console.log(itemData);
                if (itemData !== null) {
                    getOutputGrid().setData(itemData);
                }
                else {
                    getOutputGrid().reset();
                }
                getForm().setFieldData('documentRefId', data[0].requestOrderId);
                getForm().setFieldData('documentRefNumber', data[0].documentRefNumber);

                const motiveId = data[0].reasonId;
                if (motiveId !== null) {
                    getForm().setFieldData('reasonId', data[0].reasonId);
                } else {
                    getForm().setFieldData('reasonId', ''); //ARREGLAR Y CAMBIAR POR RESET 
                }

            }
        }
    }

    const DocRefModalOuputConfig = {
        width: 1000,
        maskClosable: false,
        component: <DocReferencesRequerimentModal name="docRefModal" getParentApi={pageApi.getApi} config={docRefConfig} />
    };

    return {
        form: {
            load: {
                queryId: 171,
                params: { outputId: pageApi.getParamId() },
            },
            update: {
                queryId: 173,
                postLink: (resp, values) => '/warehouse/outputs/outputs-admin',
                fnOk: (resp) => {
                    getOutputGrid().save();
                }
            },
            save: {
                queryId: 172,
                postLink: (resp, values) => '/warehouse/outputs/outputs-admin',
                fnOk: (resp) => {
                    const outputId = resp.dataObject?.outputId;
                    if (outputId) {
                        getOutputGrid().save({ params: { outputId } });
                    }
                }
            },
            title: 'Datos Principales',
            fields: [
                { type: 'select', name: 'companyId', label: 'Empresa', column: 4, load: { queryId: 554, }, search: true },
                { type: 'date', name: 'departureDate', label: 'Fecha Salida', format: DATE_FORMAT.DDMMYYYY, inputFormat: DATE_FORMAT.YYYYMMDD, column: 4, value: DateUtil.getDate },
                { type: 'date', name: 'docDate', label: 'Fecha Documento', format: DATE_FORMAT.DDMMYYYY, inputFormat: DATE_FORMAT.YYYYMMDD, column: 4, value: DateUtil.getDate },
                { type: 'select', name: 'warehouseId', label: 'Almacén', column: 4, load: { queryId: 34 }, search: true },
                { type: 'select', name: 'documentTypeId', label: 'Tipo Documento', column: 4, load: { queryId: 524 }, search: true,
                    parents: [
                        { name: 'warehouseId', paramName: 'warehouseId', required: true }
                    ],
                    onChange: (value) => {
                        let correlativo = getForm()?.getFieldApi('documentTypeId')?.getSelectedItem()?.correlativo;
                        if (correlativo !== undefined) {
                            getForm().setFieldData('documentTypeNumber', correlativo);
                        }
                    }
                },
                { name: 'documentTypeNumber', label: 'Nro. Documento', column: 4, readOnly: true, maxLength: 10, },
                { type: 'select', name: 'customerId', label: 'Cliente', column: 4, search: true, load: { queryId: 361 } },
                { type: 'select', name: 'applicantId', label: 'Solicitante', column: 4, search: true, load: { queryId: 309 } },
                { type: 'select', name: 'operationTypeId', label: 'Tipo Operación', column: 4, load: { queryId: 77 }, search: true },
                { type: 'select', name: 'documentTypeRefId', label: 'Doc. Referencia', load: { queryId: 195 }, column: 4, value: 75 , search: true,
                    onChange: (value) => {
                        if (value === 75) {
                            getForm()?.getConfig()?.fields[12].setVisible(true);
                        } 
                        else if (value !== 75) {
                            getForm()?.getConfig()?.fields[12].setVisible(false);
                            getForm()?.setFieldData('documentRefId', '');
                            getForm()?.setFieldData('documentRefNumber', '');
                        }
                    },
                },
                { type: 'hidden', name: 'documentRefId' },
                { name: 'documentRefNumber', label: 'Nro. Documento', column: 4 },
                { type: 'button', column: 1, name: 'modal', onClick: (value) => pageApi.showModal(DocRefModalOuputConfig) },
                { type: 'textarea', name: 'observations', label: 'Observaciones', column: 12 },
            ],
            onComponentLoad: (formApi, pageApi) => {
                if (pageApi.getPageMode() !== PAGE_MODE.NEW)
                    formApi.load();
            }
        },
        bar: {
            fields: [
                {
                    ...BUTTON_DEFAULT.SAVE, onClick: () => {

                        let items = getOutputGrid()?.getData();
                        if (items) {

                            let lott = 0;
                            items.forEach(item => {
                                lott = lott + parseFloat(item.lot);
                            });

                            if (lott > 0 && lott !== null && lott !== undefined && lott !== NaN) {
                                getForm().save()
                            } else {
                                return MessageUtil('warning', 'Alerta!', 'Debe Ingresar el Lote de los Items');
                            }
                        }

                    }
                },
                { ...BUTTON_DEFAULT.UPDATE, onClick: () => getForm().update() },
                { ...BUTTON_DEFAULT.RETURN, link: '/warehouse/outputs/outputs-admin' }
            ],
        },
        grid: {}
    }
}
