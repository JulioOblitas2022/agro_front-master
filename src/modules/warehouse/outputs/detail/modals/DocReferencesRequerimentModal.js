import React from 'react';
import { DATE_FORMAT } from '../../../../../constants/constants';
import FormGridFw, { FORM_GRID_MODE } from '../../../../../framework/form-grid/FormGridFw';
import { SELECTION_TYPE } from '../../../../../framework/grid/GridFw';
import { BasePage } from '../../../../../framework/pages/BasePage';

export class DocReferencesRequerimentModal extends BasePage {




    createPageProperties = (pageApi) => {
        return {
            title: 'Documentos de Referencia',
        };
    }

    renderPage(pageApi) {
        //console.log(pageApi.name);
        const formGridConfig = {
            mode: FORM_GRID_MODE.FORM_SEARCH_ADD,
            afterAddEvent: (data) => {
                pageApi.getEvent('afterAddEvent')(data);
                pageApi.hideThisModal();
            },
            form: {
                fields: [
                    { name: 'numDoc', label: 'N° Documento', column: 6, maxLength: 10 },
                ]
            },
            grid: {
                title: 'Documentos seleccionados',
                rowId: 'codigo',
                showIndex: false,
                autoLoad: true,
                pagination: false,
                selection: { type: SELECTION_TYPE.SINGLE },
                load: {
                    queryId: 562,
                    orderBy: "1"
                },
                fields: [
                    { name: 'docRefNumber', label: 'N° Documento' },
                    { type: 'date', name: 'fecha', label: 'Fecha', format: DATE_FORMAT.DDMMYYYY },
                    { name: 'obs', label: 'Obs' },
                    { name: 'mes', label: 'Mes' },
                    { name: 'anio', label: 'Año' },
                    { name: 'area', label: 'Area' }
                ]
            }
        };

        return (
            <>
                <FormGridFw
                    name="docRefGrid"
                    getParentApi={pageApi.getApi}
                    config={formGridConfig}
                />
            </>
        )

    }

}