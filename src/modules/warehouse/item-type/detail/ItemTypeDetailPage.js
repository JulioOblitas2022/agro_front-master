import React from 'react';
import BarButtonFw from '../../../../framework/bar-button/BarButtonFw';
import FormFw from '../../../../framework/form/FormFw';
import { BasePage } from '../../../../framework/pages/BasePage';
import { getConfigItemTypeDetail } from './getConfigItemTypeDetail';
import { getModeItemTypeDetail } from './getModeItemTypeDetail';
import { getValidationItemTypeDetail } from './getValidationItemTypeDetail';


export class ItemTypeDetailPage extends BasePage {
    
    createPageProperties = (pageApi) => {
        return {
            title: 'Detalle Tipo Items',
            validation: getValidationItemTypeDetail,
            mode: getModeItemTypeDetail
        };
    }

    renderPage(pageApi) {
        const config = getConfigItemTypeDetail(pageApi);
          
        return (
            <>
                <FormFw
                    name="form"
                    getParentApi={pageApi.getApi}
                    config={config.form}
                />
                <BarButtonFw
                    name="bar"
                    getParentApi={pageApi.getApi}
                    config={config.bar}
                />
            </>
        )
    }
}


