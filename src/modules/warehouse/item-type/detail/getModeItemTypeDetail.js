import { MODE_SHORTCUT } from "../../../../constants/constants"

export const getModeItemTypeDetail = () => {
    return {
        new: {
            bar: {
                show: ['save', 'return']
            }
        },
        update:{
            bar: {
                show: ['update', 'return']
            }
        },
        read:{
            form: {
                read: [MODE_SHORTCUT.ALL],
                hide: ['password'],
            },
            bar: {
                show: ['return']
            }
        }
    }
}
