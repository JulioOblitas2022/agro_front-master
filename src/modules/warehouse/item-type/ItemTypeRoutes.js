import React from 'react'
import { Switch, Route, useRouteMatch } from "react-router-dom"
import { ItemTypeAdminPage } from './admin/ItemTypeAdminPage'
import { ItemTypeDetailPage } from './detail/ItemTypeDetailPage'

const ItemTypeRoutes = () => {
    const { path } = useRouteMatch();
    return (
        <Switch>
            <Route exact path={`${path}/item-type-admin`} component={ItemTypeAdminPage} />
            <Route path={`${path}/item-type`} component={ItemTypeDetailPage} />
            <Route path={`${path}/item-type/:id`} component={ItemTypeDetailPage} />
        </Switch>
    )
}
export default ItemTypeRoutes;