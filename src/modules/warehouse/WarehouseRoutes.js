import React, { lazy } from 'react';
import { Switch, Route, useRouteMatch } from 'react-router-dom';

const UnitRoutes = lazy(() => import('./unit/UnitRoutes'));
const ItemTypeRoutes = lazy(() => import('./item-type/ItemTypeRoutes'));
const OperationTypeRoutes = lazy(() => import('./operation-type/OperationTypeRoutes'));
const WhouseRoutes = lazy(() => import('./whouse/WhouseRoutes'));
const WhouseSerieRoutes = lazy(() => import('./whouse-serie/WhouseSerieRoutes'));
const FamilyRoutes = lazy(() => import('./family/FamilyRoutes'));
const ClassRoutes = lazy(() => import('./class/ClassRoutes'));
const SubClassRoutes = lazy(() => import('./sub-class/SubClassRoutes'));
const PersonalRoutes = lazy(() => import('./personal/PersonalRoutes'));
const MasterItemRoutes = lazy(() => import('./master-item/MasterItemRoutes'));
const RawMaterialRoutes = lazy(() => import('./raw-material/RawMaterialRoutes'));
const AnotherItemRoutes = lazy(() => import('./another-item/AnotherItemRoutes'));
const OutputsRoutes = lazy(() => import('./outputs/OutputsRoutes'));
const OutputsSeveralRoutes = lazy(() => import('./outputs-several/OutputsSeveralRoutes'));
const ReportsRoutes = lazy(() => import('./reports/ReportsRoutes'));

export const WarehouseRoutes = () => {
    const { path } = useRouteMatch();
    return (
        <Switch>
            <Route path={`${path}/unit`} component={UnitRoutes} />
            <Route path={`${path}/item-type`} component={ItemTypeRoutes} />
            <Route path={`${path}/operation-type`} component={OperationTypeRoutes} />
            <Route path={`${path}/whouse`} component={WhouseRoutes} />
            <Route path={`${path}/whouse-serie`} component={WhouseSerieRoutes} />
            <Route path={`${path}/family`} component={FamilyRoutes} />
            <Route path={`${path}/class`} component={ClassRoutes} />
            <Route path={`${path}/sub-class`} component={SubClassRoutes} />
            <Route path={`${path}/personal`} component={PersonalRoutes} />
            <Route path={`${path}/master-item`} component={MasterItemRoutes} />
            <Route path={`${path}/raw-material`} component={RawMaterialRoutes} />
            <Route path={`${path}/another-item`} component={AnotherItemRoutes} />
            <Route path={`${path}/outputs`} component={OutputsRoutes} />
            <Route path={`${path}/outputs-several`} component={OutputsSeveralRoutes} />
            <Route path={`${path}/reports`} component={ReportsRoutes} />
        </Switch>
    )
}


export default WarehouseRoutes;