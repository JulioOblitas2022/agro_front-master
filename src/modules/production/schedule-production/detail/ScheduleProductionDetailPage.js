import React from 'react'
import BarButtonFw from '../../../../framework/bar-button/BarButtonFw';
import FormFw from '../../../../framework/form/FormFw';
import { BasePage } from '../../../../framework/pages/BasePage';
import { getConfigScheduleProductionDetail } from './getConfigScheduleProductionDetail';
import { getModeScheduleProductionDetail } from './getModeScheduleProductionDetail';
import { getValidationScheduleProductionDetail } from './getValidationScheduleProductionDetail';
import { createProductionOrderSection } from './sections/ProductionOrderSection';
import { createProductSection } from './sections/ProductSection';
import { Tabs } from "antd";
import { StickyContainer, Sticky } from "react-sticky";
import './tabs.css';
import { ScheduleProductionSection } from './sections/ScheduleProductionSection';
import { SuppliesRequiredSection } from './sections/SuppliesRequiredSection';
import { IntermediateRequiredSection } from './sections/IntermediateRequiredSection';
import { IntermediatePersonalSection } from './sections/IntermediatePersonalSection';

export class ScheduleProductionDetailPage extends BasePage {
    
    createPageProperties = (pageApi) => {
        return {
            title: 'DETALLE DE PROGRAMACIÓN DE PRODUCCIÓN',
            validation: getValidationScheduleProductionDetail,
            mode: getModeScheduleProductionDetail
        };
    }

    renderPage(pageApi) {

        const config = getConfigScheduleProductionDetail(pageApi);

        const { TabPane } = Tabs;

        const renderTabBar = (props, DefaultTabBar) => (
        <Sticky bottomOffset={80}>
            {({ style }) => (
            <DefaultTabBar
                {...props}
                className="site-custom-tab-bar"
                style={style}
            />
            )}
        </Sticky>
        );

        return (
            <>
                

                <StickyContainer>
                <Tabs defaultActiveKey="1" renderTabBar={renderTabBar}>
                    <TabPane tab="Programación" key="1">
                        <FormFw
                            name="form"
                            getParentApi={pageApi.getApi}
                            config={config.form}
                        />
                        
                        {createProductionOrderSection(pageApi)}

                        {createProductSection(pageApi)}

                        <BarButtonFw
                            name="bar"
                            getParentApi={pageApi.getApi}
                            config={config.bar}
                        />
                    </TabPane>
                    <TabPane tab="Cronograma Producción" key="2">
                        <ScheduleProductionSection/>
                    </TabPane>
                    <TabPane tab="Insumos Requeridos" key="3" >
                        <SuppliesRequiredSection/>
                    </TabPane>
                    <TabPane tab="Intermedios Requeridos" key="4">
                        <IntermediateRequiredSection/>
                    </TabPane>
                    <TabPane tab="Personal Intermedio" key="5">
                        <IntermediatePersonalSection/>
                    </TabPane>
                </Tabs>
                </StickyContainer>
            </>
        )
    }
}