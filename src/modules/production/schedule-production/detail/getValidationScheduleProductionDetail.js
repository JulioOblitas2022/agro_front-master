export const getValidationScheduleProductionDetail = () => {
    return {
        form: {
            provider: [ {required: true, message: 'Proveedor es requerido' } ],
            localId: [ {required: true, message: 'Local es requerido' } ],
            docNumber: [ {required: true, message: 'N° Documento es requerido' } ],
            applicantId: [ {required: true, message: 'Solicitante es requerido' } ],
            areaId: [ {required: true, message: 'Área es requerido' } ],
            coinId: [ {required: true, message: 'Moneda es requerido' } ],
            paymentConditionId: [ {required: true, message: 'Condición de Pago es requerido' } ],
            reasonId: [ {required: true, message: 'Motivo es requerido' } ],
        },
        grid: {

        },
        button: {

        },
        bar: {

        }
    }
}
