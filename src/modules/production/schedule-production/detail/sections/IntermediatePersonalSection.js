import React from 'react'
import GridFw from '../../../../../framework/grid/GridFw';
import { BasePage } from '../../../../../framework/pages/BasePage';
import { getGridServerDefaultParams } from '../../../../shared/util/getGridServerDefaultParams';

export class IntermediatePersonalSection extends BasePage {
    createPagePropertiesBase(pageApi) {
        return {
            title: 'Personal Intermedio',
        };
    }

    renderPage(pageApi) {

        const getGrid = () => pageApi.getComponent('grid');

        
        const grid = {
            ...getGridServerDefaultParams({queryLoad: 450}),
            title: 'RESULTADOS',
            scroll:{
                x:'auto'
            },
            fields:[
                {name: 'product', label: 'Producto'},
                {name: 'unitMeasure', label: 'Unidad Medida',},
                {name: 'deliverDate', label: 'Cantidad Hora',},
                {name: 'stock', label: 'Cantidad Requerida',},
                {name: 'quantity', label: 'N° Día',},
                {name: 'quantityProducer', label: 'Día Inicio',}
            ]
        };

        return (
            <>
                <GridFw
                    name="grid"
                    getParentApi={pageApi.getApi}
                    config={grid}
                />
            </>
        )
    }
}
