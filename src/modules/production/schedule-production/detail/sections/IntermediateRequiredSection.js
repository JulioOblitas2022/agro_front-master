import React from 'react'
import GridFw from '../../../../../framework/grid/GridFw';
import { BasePage } from '../../../../../framework/pages/BasePage';
import { getGridServerDefaultParams } from '../../../../shared/util/getGridServerDefaultParams';

export class IntermediateRequiredSection extends BasePage {
    createPagePropertiesBase(pageApi) {
        return {
            title: 'Personal Intermedio',
        };
    }

    renderPage(pageApi) {

        const getGrid = () => pageApi.getComponent('grid');

        
        const grid = {
            ...getGridServerDefaultParams({queryLoad: 450}),
            title: 'RESULTADOS',
            scroll:{
                x:'auto'
            },
            fields:[
                {name: 'product', label: 'Producto'},
                {name: 'unitMeasure', label: 'Unidad Medida',},
                {name: 'deliverDate', label: 'Fec. Uso',},
                {name: 'stock', label: 'Stock Actual',},
                {name: 'quantity', label: 'Cantidad Requerida',},
                {name: 'quantityProducer', label: 'Saldo',}
            ]
        };

        return (
            <>
                <GridFw
                    name="grid"
                    getParentApi={pageApi.getApi}
                    config={grid}
                />
            </>
        )
    }
}
