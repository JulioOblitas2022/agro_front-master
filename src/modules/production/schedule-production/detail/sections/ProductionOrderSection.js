
import { DATE_FORMAT } from '../../../../../constants/constants';
import { BUTTON_DEFAULT } from '../../../../../framework/bar-button/BarButtonFw';
import GridFw, { COLUMN_DEFAULT } from '../../../../../framework/grid/GridFw';
import { ProductionOrderModal } from '../modals/ProductionOrderModal';
export const createProductionOrderSection = (pageApi) => {
    
    const fnDelete = (value, row, index) => {
        getGrid().deleteItem(row.key);      
    }
    
    const getGrid = () => pageApi.getComponent('ProductionOrderSection');

    const getGridProduct = () => pageApi.getComponent('ProductSection');

    const documentConfig = {
        events: {
            afterAddEvent : (data) => {
                getGrid().addItem(data);
                let items = JSON.parse(data[0].items);
                getGridProduct().addItem(items);
            }
        }
    }

    const DocumentModalConfig = {
        width: 1600,
        maskClosable: false,
        component: <ProductionOrderModal name="documentModal"  getParentApi={pageApi.getApi} config={documentConfig} />
    };


    const barHeaderConfig = {
        fields:[
            {...BUTTON_DEFAULT.SEARCH, label:'Buscar Orden Producción', onClick: ()=>{  pageApi.showModal(DocumentModalConfig); }}
        ]
    }
    const gridConfig = {
        title: 'ORDEN DE PRODUCCIÓN',
        rowId: 'productionScheduleProductionOrderDetailId',
        showIndex: true,
        pagination: true,
        autoLoad: false,
        collapsible: true,
        load: {
            validation: () => pageApi.getParamId()!=null, //solo se dispara si el valor es distinto de nulo
            queryId: 475, 
            orderBy: 'productionScheduleProductionOrderDetailId desc',
            params: {productionScheduleId: pageApi.getParamId()}
        },
        save: {
            queryId: 474,
            params: {productionScheduleId: pageApi.getParamId()}
        },
        fields:[
            {name: 'productionOrderNumber', label: 'Nro. Ord. Prod.'},
            {type: 'date', name: 'broadcastDate', label: 'Fecha Emisión', format: DATE_FORMAT.DDMMYYYY},
            {type: 'decimal', name: 'quantity', label:'Nro. Items', editable:true},
            {name: 'responsable', label:'Responsable'},
            {name: 'priority', label:'Prioridad'},
            {...COLUMN_DEFAULT.DELETE, onClick: fnDelete, label: 'Opciones'}
        ],
        barHeader: barHeaderConfig
    };

    return (
        <GridFw
            name="ProductionOrderSection"
            getParentApi={pageApi.getApi}
            config={gridConfig} 
        />
    )
}
