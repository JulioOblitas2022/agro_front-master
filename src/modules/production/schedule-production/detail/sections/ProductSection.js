
import { DATE_FORMAT } from '../../../../../constants/constants';
import { BUTTON_DEFAULT } from '../../../../../framework/bar-button/BarButtonFw';
import GridFw, { COLUMN_DEFAULT } from '../../../../../framework/grid/GridFw';
import { ProductModal } from '../modals/ProductModal';
import { SuppliesModal } from '../modals/SuppliesModal';
export const createProductSection = (pageApi) => {
    
    const fnDelete = (value, row, index) => {
        getGrid().deleteItem(row.key);      
    }
    
    const getGrid = () => pageApi.getComponent('ProductSection');

    const itemConfig = {
        events: {
            afterAddEvent : (data) => {
                getGrid().addItem(data);
            }
        }
    }

    const ItemModalConfig = {
        width: 1600,
        maskClosable: false,
        component: <ProductModal name="documentModal"  getParentApi={pageApi.getApi} config={itemConfig} />
    };

    const SuppliesModalConfig = (data) =>  {
        return {
            width: 1600,
            maskClosable: false,
            component: <SuppliesModal name={data}  getParentApi={pageApi.getApi}  />
        }
    };


    const barHeaderConfig = {
        fields:[
            {...BUTTON_DEFAULT.SEARCH, label:'Buscar Producto', onClick: ()=>{  pageApi.showModal(ItemModalConfig); }}
        ]
    }
    const gridConfig = {
        title: 'LISTA DE PRODUCTOS',
        rowId: 'productionScheduleProductDetailId',
        showIndex: true,
        pagination: true,
        autoLoad: false,
        collapsible: true,
        load: {
            validation: () => pageApi.getParamId()!=null, //solo se dispara si el valor es distinto de nulo
            queryId: 477,
            orderBy: 'productionScheduleProductDetailId desc',
            params: {productionScheduleId: pageApi.getParamId()}
        },
        save: {
            queryId: 476,
            params: {productionScheduleId: pageApi.getParamId()}
        },
        scroll:{
          x:'auto'  
        },
        fields:[
            { name: 'item', label: 'Producto'},
            { type: 'select', name: 'recipeId', label: 'Receta', editable:true, load: {queryId:469, params: () => {return {masterItemId: getGrid()?.itemEditing?.masterItemId}}}, labelName: 'recipe'},
            { type: 'select', name: 'lineId', label: 'Línea', editable:true, load: {queryId:570, params: () => {return {recipeId: getGrid()?.itemEditing?.recipeId}}}, labelName: 'line'},
            { type: 'select', name: 'unitMeasureId', label: 'Uni. Med.', labelName: 'unitMeasure', load: { queryId: 568, params: () => {return {masterItemId: getGrid()?.itemEditing?.masterItemId}}}, search: true, editable: true },
            { name: 'quantity', label:'Cantidad', editable:true},
            { type: 'date',name: 'deliverDate', label: 'Fec. Entrega', editable:true, format: DATE_FORMAT.DDMMYYYY, inputFormat: DATE_FORMAT.YYYYMMDD},
            { type: 'date',name: 'productionDate', label: 'Fec. Producción', editable:true, format: DATE_FORMAT.DDMMYYYY, inputFormat: DATE_FORMAT.YYYYMMDD},
            { type: 'select', name: 'responsableId', label: 'Responsable de Producto', labelName: 'responsable', column: 4, load: { queryId: 566 }, search: true, editable: true },
            { type: 'date', name:'hourInitial', label: 'Hora Inicio', editable:true, format: DATE_FORMAT.HHmm, inputFormat: DATE_FORMAT.HHmmss},
            { type: 'date', name:'hourEnd', label: 'Hora Fin', editable:true, format: DATE_FORMAT.HHmm, inputFormat: DATE_FORMAT.HHmmss},
            {...COLUMN_DEFAULT.VIEW, onClick: (value, row, formApi) => pageApi.showModal(SuppliesModalConfig(row)), colSpan: 2, label: 'Opciones'},
            {...COLUMN_DEFAULT.DELETE, onClick: fnDelete, colSpan: 0},
        ],
       // barHeader: barHeaderConfig
    };

    return (
        <GridFw
            name="ProductSection"
            getParentApi={pageApi.getApi}
            config={gridConfig} 
        />
    )
}

