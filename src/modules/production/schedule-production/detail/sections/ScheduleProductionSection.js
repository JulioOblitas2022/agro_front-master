import React from 'react'
import { DATE_FORMAT } from '../../../../../constants/constants';
import GridFw from '../../../../../framework/grid/GridFw';
import { BasePage } from '../../../../../framework/pages/BasePage';

export class ScheduleProductionSection extends BasePage {
    createPagePropertiesBase(pageApi) {
        return {
            title: 'Cronograma de Producción',
        };
    }

    renderPage(pageApi) {

        const getGrid = () => pageApi.getComponent('gridCronograma');

        const gridConfig = {
            title: 'CRONOGRAMA DE PRODUCCIÓN',
            rowId: 'productionScheduleProductCronograma',
            showIndex: true,
            pagination: true,
            autoLoad: true,
            collapsible: true,
            load: {
                validation: () => pageApi.getParamId()!=null, //solo se dispara si el valor es distinto de nulo
                queryId: 605, 
                orderBy: 'productionScheduleProductCronograma desc',
                params: {productionScheduleId: pageApi.getParamId()}
            },
            fields:[
                {name: 'product', label: 'Producto'},
                {name: 'unitMeasure', label: 'Unidad Medida',},
                {type: 'date', name: 'deliverDate', label: 'Fec. Entrega', format: DATE_FORMAT.DDMMYYYY},
                {name: 'stock', label: 'Stock Actual',},
                {name: 'quantity', label: 'Cantidad',},
                {name: 'quantity', label: 'Cantidad Producir',}
            ],
        };

        return (
            <>
                <GridFw
                    name="gridCronograma"
                    getParentApi={pageApi.getApi}
                    config={gridConfig}
                />
            </>
        )
    }
}
