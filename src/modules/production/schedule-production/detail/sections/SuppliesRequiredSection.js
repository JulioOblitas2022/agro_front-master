import React from 'react'
import { DATE_FORMAT } from '../../../../../constants/constants';
import GridFw from '../../../../../framework/grid/GridFw';
import { BasePage } from '../../../../../framework/pages/BasePage';
import { getGridServerDefaultParams } from '../../../../shared/util/getGridServerDefaultParams';

export class SuppliesRequiredSection extends BasePage {
    createPagePropertiesBase(pageApi) {
        return {
            title: 'Insumos Requeridos',
        };
    }

    renderPage(pageApi) {

        const getGrid = () => pageApi.getComponent('gridInsumos');

        const gridConfig = {
            title: 'LISTA DE INSUMOS',
            rowId: 'productionScheduleProductDetailId',
            showIndex: true,
            pagination: true,
            autoLoad: true,
            collapsible: true,
            load: {
                validation: () => pageApi.getParamId()!=null, //solo se dispara si el valor es distinto de nulo
                queryId: 606, 
                orderBy: 'productionScheduleProductCronograma desc',
                params: {productionScheduleId: pageApi.getParamId()}
            },
            scroll:{
              x:'auto'  
            },
            fields:[
                {name: 'item', label: 'Producto'},
                {name: 'unitMeasure', label: 'Unidad Medida',},
                { type: 'date', name: 'productionDate', label: 'Fec. Uso', format: DATE_FORMAT.DDMMYYYY},
                {name: 'stock', label: 'Stock Actual',},
                {name: 'quantity', label: 'Cantidad Requerida',},
                {name: 'quantity', label: 'Saldo',},
            ],
        };


        return (
            <>
                <GridFw
                    name="gridInsumos"
                    getParentApi={pageApi.getApi}
                    config={gridConfig}
                />
            </>
        )
    }
}
