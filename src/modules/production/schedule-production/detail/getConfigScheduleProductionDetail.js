import { UserOutlined } from '@ant-design/icons';
import { DATE_FORMAT, PAGE_MODE } from '../../../../constants/constants';
import { BUTTON_DEFAULT } from '../../../../framework/bar-button/BarButtonFw';
import { UserUtil } from '../../../../util/UserUtil';
import { DateUtil } from "../../../../util/DateUtil";

export const getConfigScheduleProductionDetail = (pageApi) => {
    const getForm = () => pageApi.getComponent('form');
    const getGridProductDetailSection = () => pageApi.getComponent('ProductSection');
    const getGridProductionOrderDetailSection = () => pageApi.getComponent('ProductionOrderSection');
    const getGridCronograma = () => pageApi.getComponent('gridCronograma');
    const getGridInsumos = () => pageApi.getComponent('gridInsumos');
    //const Br = React.createClass({ render() { return (<Text> {"\n"} </Text>) } })

    return {
        form: {
            autoLoad: true,
            load: {
                queryId: 449,
                params: { productionScheduleId: pageApi.getParamId() || 0 },
                fnOk: (resp) => {
                    getGridProductionOrderDetailSection().load({ params: { productionScheduleId: pageApi.getParamId() } });
                    getGridProductDetailSection().load({ params: { productionScheduleId: pageApi.getParamId() } });
                    getGridCronograma().load({params: { productionScheduleId: pageApi.getParamId() }})
                    getGridInsumos().load({params: { productionScheduleId: pageApi.getParamId() }})
                }
            },
            update: {
                queryId: 448,
                postLink: (resp, values) => '/production/schedule-production/schedule-production-admin',
                params: { productionScheduleId: pageApi.getParamId() },
                fnOk: (resp) => {
                    getGridProductionOrderDetailSection().save({ params: { productionScheduleId: pageApi.getParamId() } });
                    getGridProductDetailSection().save({ params: { productionScheduleId: pageApi.getParamId() } });
                }
            },
            save: {
                queryId: 447,
                postLink: (resp, values) => '/production/schedule-production/schedule-production-admin',
                params: { createdBy: UserUtil.getUserId() },
                fnOk: (resp) => {
                    const productionScheduleId = resp.dataObject?.productionScheduleId;
                    if (productionScheduleId) {
                        getGridProductionOrderDetailSection().save({ params: { productionScheduleId } });
                        getGridProductDetailSection().save({ params: { productionScheduleId } });
                    }
                }
            },
            title: 'PROGRAMAR PRODUCCIÓN',
            fields: [
                { type: 'divider', label: 'DATOS DE LA ORDEN' },
                { type: 'select', name: 'companyId', label: 'Empresa', column: 4, load: { queryId: 554, }, search: true },
                { name: 'numberProductionSchedule', label: 'Nro. Documento', column: 4, readOnly: true},
                { type: 'date', name: 'bradcastDate', label: 'Fecha Registro', format: DATE_FORMAT.DDMMYYYY, inputFormat: DATE_FORMAT.YYYYMMDD, column: 4, value: DateUtil.getDate },
                { type: 'select', name: 'programmerId', label: 'Programador', column: 4, load: { queryId: 566 }, search: true },
                { type: 'date', name: 'startDate', label: 'Fecha Inicio', format: DATE_FORMAT.DDMMYYYY, inputFormat: DATE_FORMAT.YYYYMMDD, column: 4, value: DateUtil.startOfMonth },
                { type: 'date', name: 'endDate', label: 'Fecha Termino', format: DATE_FORMAT.DDMMYYYY, inputFormat: DATE_FORMAT.YYYYMMDD, column: 4, value: DateUtil.endOfMonth },
                { type: 'number', name: 'numberDayProduction', label: 'Días Producción', column: 4, value: 5 },
                { type: 'textarea', name: 'observations', label: 'Observaciones', column: 12 },
            ],
            onComponentLoad: (formApi, pageApi) => {
                if (pageApi.getPageMode() !== PAGE_MODE.NEW)
                    formApi.load();
            }
        },
        bar: {
            fields: [
                { ...BUTTON_DEFAULT.SAVE, onClick: () => getForm().save() },
                { ...BUTTON_DEFAULT.UPDATE, onClick: () => getForm().update() },
                { ...BUTTON_DEFAULT.RETURN, link: '/production/schedule-production/schedule-production-admin' }
            ],
        },
        grid: {}
    }
}
