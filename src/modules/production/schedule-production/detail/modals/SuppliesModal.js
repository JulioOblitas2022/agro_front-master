import React from 'react';
import GridFw, { SELECTION_TYPE } from '../../../../../framework/grid/GridFw';
import { BasePage } from '../../../../../framework/pages/BasePage';

export class SuppliesModal extends BasePage {
    
    
    createPageProperties = (pageApi) => {
        return {
            title: 'INSUMOS DE RECETA',
        };
    }

    renderPage(pageApi) {
       
        const gridConfig = {
                title: '',
                rowId: 'codigo',
                showIndex: false,
                autoLoad: true,
                pagination: false,
                load: {
                    queryId: 564,
                    orderBy: "1",
                    params: {
                        productId: pageApi.name.productId,
                        productRecipeDetailId: pageApi.name.recipeId
                    },
                },
                fields:[
                    {name:'existenceType', label:'Tipo de Existencia'},
                    {name:'code', label:'Código'},
                    {name:'description', label:'Descripción'},
                    {name:'unitMeasure', label:'Unidad de Medida'},
                    {name:'quantity', label:'Cantidad Teórica'},
                ]
        };  
    
        return (
            <>
                <GridFw 
                    name="grid" 
                    getParentApi={pageApi.getApi}
                    config={gridConfig}
                />
            </>
        )

    }
    
}