import React from 'react';
import { DATE_FORMAT } from '../../../../../constants/constants';
import BarButtonFw, { BUTTON_DEFAULT } from '../../../../../framework/bar-button/BarButtonFw';
import FormGridFw, { FORM_GRID_MODE } from '../../../../../framework/form-grid/FormGridFw';
import FormFw from '../../../../../framework/form/FormFw';
import { SELECTION_TYPE } from '../../../../../framework/grid/GridFw';
import { BasePage } from '../../../../../framework/pages/BasePage';

export class ProductModal extends BasePage {
    
    
    createPageProperties = (pageApi) => {
        return {
            title: 'Busqueda de Items',
        };
    }
    
    renderPage(pageApi) {
        const formGridConfig = {
            mode: FORM_GRID_MODE.FORM_SEARCH_ADD,
            afterAddEvent: (data) => {
                pageApi.getEvent('afterAddEvent')(data);
                pageApi.hideThisModal();
            },
            form: {
                fields:[
                    {type: 'select',name:'itemTypeId', label:'Tipo Item', column: 6, load: {queryId: 185}},
                ]
            },
            grid: {
                title: 'Documentos seleccionados',
                rowId: 'codigo',
                showIndex: false,
                autoLoad: true,
                pagination: false,
                selection: {type: SELECTION_TYPE.MULTIPLE},
                load: {
                    queryId: 473,
                    orderBy: "1"
                },
                fields:[
                    {name:'code', label:'Código'},
                    {name:'description', label:'Descripción'},
                    {name:'unitMeasure', label:'Unidad Medida'},
                    {type: 'date', name:'broadcastDate', label: 'Fecha', format: DATE_FORMAT.DDMMYYYY}
                ]
            }
        };  
    
        return (
            <>
                <FormGridFw 
                    name="itemsGrid" 
                    getParentApi={pageApi.getApi}
                    config={formGridConfig}
                />
            </>
        )

    }
    
}