import React from 'react';
import { DATE_FORMAT } from '../../../../../constants/constants';
import FormGridFw, { FORM_GRID_MODE } from '../../../../../framework/form-grid/FormGridFw';
import { SELECTION_TYPE } from '../../../../../framework/grid/GridFw';
import { BasePage } from '../../../../../framework/pages/BasePage';

export class ProductionOrderModal extends BasePage {
    
    
    createPageProperties = (pageApi) => {
        return {
            title: 'Busqueda de Documentos',
        };
    }
    
    renderPage(pageApi) {
        const formGridConfig = {
            mode: FORM_GRID_MODE.FORM_SEARCH_ADD,
            afterAddEvent: (data) => {
                pageApi.getEvent('afterAddEvent')(data);
                pageApi.hideThisModal();
            },
            form: {
                fields:[
                    {name:'productionOrderNumber', label:'Nro. Ord. Prod.', column: 6},
                ]
            },
            grid: {
                title: 'Documentos seleccionados',
                rowId: 'codigo',
                showIndex: false,
                autoLoad: true,
                pagination: false,
                selection: {type: SELECTION_TYPE.MULTIPLE},
                load: {
                    queryId: 472,
                },
                fields:[
                    {name:'productionOrderNumber', label:'Nro. Documento'},
                    {type:'date', name:'broadcastDate', label:'Fecha', format: DATE_FORMAT.DDMMYYYY},
                    {name:'responsable', label:'Responsable'},
                    {name:'priority', label:'Prioridad'}
                ]
            }
        };  
    
        return (
            <>
                <FormGridFw 
                    name="documentosGrid" 
                    getParentApi={pageApi.getApi}
                    config={formGridConfig}
                />
            </>
        )

    }
    
}