import React from 'react'
import { Route, Switch, useRouteMatch } from 'react-router';
import { ScheduleProductionAdminPage } from './admin/ScheduleProductionAdminPage';
import { ScheduleProductionDetailPage } from './detail/ScheduleProductionDetailPage';

const ScheduleProductionRoutes = () => {
    const { path } = useRouteMatch();
    return (
        <Switch>
            <Route exact path={`${path}/schedule-production-admin`} component={ScheduleProductionAdminPage} />
            <Route path={`${path}/schedule-production`} component={ScheduleProductionDetailPage} />
            <Route path={`${path}/schedule-production/:id`} component={ScheduleProductionDetailPage} />
        </Switch>
    )
}

export default ScheduleProductionRoutes;