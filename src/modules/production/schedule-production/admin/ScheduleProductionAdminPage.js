import React from 'react'
import BarButtonFw, { BUTTON_DEFAULT } from '../../../../framework/bar-button/BarButtonFw';
import FormFw from '../../../../framework/form/FormFw';
import GridFw from '../../../../framework/grid/GridFw';
import { BasePage } from '../../../../framework/pages/BasePage';
import { DATE_FORMAT } from '../../../../constants/constants';
import { COLUMN_DEFAULT } from '../../../../framework/grid/GridFw';
import { getFormDefaultParams } from '../../../shared/util/getFormDefaultParams';
import { getGridServerDefaultParams } from '../../../shared/util/getGridServerDefaultParams';

export class ScheduleProductionAdminPage extends BasePage {
    createPagePropertiesBase(pageApi) {
        return {
            title: 'PROGRAMAR PRODUCCIÓN',
            barHeader: {
                fields:[
                    {...BUTTON_DEFAULT.NEW, label:'Nuevo', link: '/production/schedule-production/schedule-production/new'}
                ]
            }
        };
    }

    renderPage(pageApi) {

        const getGrid = () => pageApi.getComponent('grid');
        const getForm = () => pageApi.getComponent('form');

        const form = {
            ...getFormDefaultParams(),
            title: 'PROGRAMAR PRODUCCIÓN',
            fields:[
                {name:'numberScheduleProduction', label:'N° Programa', column: 4},
                {type: 'select', name:'employeeId', label:'Programador', column: 4, load: { queryId: 566 }, search: true },
                // {type: 'date', name: 'broadcastDate',label:'Fecha Desde', format: DATE_FORMAT.DDMMYYYY, column: 4},
                // {type: 'date', name: 'deliverDate',label:'Fecha Hasta', format: DATE_FORMAT.DDMMYYYY, column: 4},
            ]
        };
        const bar = { fields: [
            {...BUTTON_DEFAULT.SEARCH, onClick: ()=>getGrid().load({params:getForm().getData()})},
            {...BUTTON_DEFAULT.CLEAR, onClick: ()=>getForm().reset()}
        ]};
        
        const grid = {
            ...getGridServerDefaultParams({queryLoad: 450}),
            title: 'RESULTADOS',
            scroll:{
                x:1300
            },
            fields:[
                {name: 'numberProductionSchedule', label: 'N° Programa'},
                {type:'date', name: 'bradcastDate', label: 'Fec. Emisiòn', format: DATE_FORMAT.DDMMYYYY},
                {type:'date', name: 'startDate', label: 'Fec. Inicio', format: DATE_FORMAT.DDMMYYYY},
                {type:'date', name: 'endDate', label: 'Fec. Termino', format: DATE_FORMAT.DDMMYYYY},
                {name: 'programmer', label: 'Responsable',},
                {...COLUMN_DEFAULT.VIEW, link: (value, row, index) => `/production/schedule-production/schedule-production/${row.productionScheduleId}`, label: 'Opciones', colSpan: 2 },
                {...COLUMN_DEFAULT.EDIT, link: (value, row, index) => `/production/schedule-production/schedule-production/${row.productionScheduleId}/update`, colSpan: 0 }
            ]
        };

        return (
            <>
                <FormFw
                    name="form"
                    getParentApi={pageApi.getApi}
                    config={form}
                />
                <BarButtonFw
                    name="bar"
                    getParentApi={pageApi.getApi}
                    config={bar}
                />
                <GridFw
                    name="grid"
                    getParentApi={pageApi.getApi}
                    config={grid}
                />
            </>
        )
    }
}
