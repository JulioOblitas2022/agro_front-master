import React from 'react'
import { Route, Switch, useRouteMatch } from 'react-router';
import { SeasonalityAdminPage } from './admin/SeasonalityAdminPage';
import { SeasonalityDetailPage } from './detail/SeasonalityDetailPage';

const SeasonalityRoutes = () => {
    const { path } = useRouteMatch();
    return (
        <Switch>
            <Route exact path={`${path}/seasonality-admin`} component={SeasonalityAdminPage} />
            <Route path={`${path}/seasonality`} component={SeasonalityDetailPage} />
            <Route path={`${path}/seasonality/:id`} component={SeasonalityDetailPage} /> 
        </Switch>
    )
}

export default SeasonalityRoutes;