import { PAGE_MODE } from "../../../../constants/constants";
import { BUTTON_DEFAULT } from "../../../../framework/bar-button/BarButtonFw";
import { UserUtil } from "../../../../util/UserUtil";
import { ProductModal } from "./modals/ProductModal";

export const getConfigSeasonalityDetail = (pageApi) => {
    const getForm = () => pageApi.getComponent('form');

    const itemConfig = {
        events: {
            afterAddEvent : (data) => {
                getForm().setFieldData('masterItemId', data[0].masterItemId);
                getForm().setFieldData('description', data[0].description);
            }
        }
    }

    const ItemModalConfig = (data) =>  {
        return {
            width: 1600,
            maskClosable: false,
            component: <ProductModal name={data}  getParentApi={pageApi.getApi} config={itemConfig}  />
        }
    };

    return {
        form: {
            load:{
                queryId: 378,
                params: {seasonalityId: pageApi.getParamId()},
            },
            update:{
                queryId: 379,
                postLink: (resp, values) => '/production/seasonality/seasonality-admin',
                params: {modifiedBy: UserUtil.getUserId()}
            },
            save:{
                queryId: 376,
                postLink: (resp, values) => '/production/seasonality/seasonality-admin',
                params: {createdBy: UserUtil.getUserId()}
            },
            title: 'DATOS PRINCIPALES',
            fields:[
                // {type: 'select', name:'masterItemId', label:'Mat. Prima', column: 12, load: {queryId:375}, search: true},
                { type: 'hidden', name: 'masterItemId' },
                { name:'description', label:'Materia Prima', column: 10, readOnly: true, },
                { type: 'button', column: 2, name: 'modal', onClick: (value) => { pageApi.showModal(ItemModalConfig('')); }, },
                { type: 'divider', label: 'Peridodo - Valor'},
                { type: 'select', name:'january', label:'ENERO', column: 4,
                    load: {
                        dataList: [
                            {value: 4, label: 'ESCACES'},
                            {value: 5, label: 'ABUNDANCIA'},
                            {value: 6, label: 'REGULAR'},
                        ]
                    }
                },
                {type: 'select', name:'february', label:'FEBRERO', column: 4,
                    load: {
                        dataList: [
                            {value: 4, label: 'ESCACES'},
                            {value: 5, label: 'ABUNDANCIA'},
                            {value: 6, label: 'REGULAR'},
                        ]
                    }
                },
                {type: 'select', name:'march', label:'MARZO', column: 4,
                    load: {
                        dataList: [
                            {value: 1, label: 'REGULAR'},
                            {value: 2, label: 'ESCASES'},
                        ]
                    }
                },
                {type: 'select', name:'april', label:'ABRIL', column: 4,
                    load: {
                        dataList: [
                            {value: 4, label: 'ESCACES'},
                            {value: 5, label: 'ABUNDANCIA'},
                            {value: 6, label: 'REGULAR'},
                        ]
                    }
                },
                {type: 'select', name:'may', label:'MAYO', column: 4,
                    load: {
                        dataList: [
                            {value: 4, label: 'ESCACES'},
                            {value: 5, label: 'ABUNDANCIA'},
                            {value: 6, label: 'REGULAR'},
                        ]
                    }
                },
                {type: 'select', name:'june', label:'JUNIO', column: 4,
                    load: {
                        dataList: [
                            {value: 4, label: 'ESCACES'},
                            {value: 5, label: 'ABUNDANCIA'},
                            {value: 6, label: 'REGULAR'},
                        ]
                    }
                },
                {type: 'select', name:'july', label:'JULIO', column: 4,
                    load: {
                        dataList: [
                            {value: 4, label: 'ESCACES'},
                            {value: 5, label: 'ABUNDANCIA'},
                            {value: 6, label: 'REGULAR'},
                        ]
                    }
                },
                {type: 'select', name:'august', label:'AGOSTO', column: 4,
                    load: {
                        dataList: [
                            {value: 4, label: 'ESCACES'},
                            {value: 5, label: 'ABUNDANCIA'},
                            {value: 6, label: 'REGULAR'},
                        ]
                    }
                },
                {type: 'select', name:'september', label:'SETIEMBRE', column: 4,
                    load: {
                        dataList: [
                            {value: 4, label: 'ESCACES'},
                            {value: 5, label: 'ABUNDANCIA'},
                            {value: 6, label: 'REGULAR'},
                        ]
                    }
                },
                {type: 'select', name:'october', label:'OCTUBRE', column: 4,
                    load: {
                        dataList: [
                            {value: 4, label: 'ESCACES'},
                            {value: 5, label: 'ABUNDANCIA'},
                            {value: 6, label: 'REGULAR'},
                        ]
                    }
                },
                {type: 'select', name:'november', label:'NOVIEMBRE', column: 4,
                    load: {
                        dataList: [
                            {value: 4, label: 'ESCACES'},
                            {value: 5, label: 'ABUNDANCIA'},
                            {value: 6, label: 'REGULAR'},
                        ]
                    }
                },
                {type: 'select', name:'december', label:'DICIEMBRE', column: 4,
                    load: {
                        dataList: [
                            {value: 4, label: 'ESCACES'},
                            {value: 5, label: 'ABUNDANCIA'},
                            {value: 6, label: 'REGULAR'},
                        ]
                    }
                },
                {type: 'radio', name:'diverseTask', label:'Tarea Diversa', column: 4, load: {queryId: 250}},
            ],
            onComponentLoad : (formApi, pageApi) =>{
                if (pageApi.getPageMode() !== PAGE_MODE.NEW)
                    formApi.load();
            }
        },
        bar: {
            fields: [
                {...BUTTON_DEFAULT.SAVE, onClick: () => getForm().save()},
                {...BUTTON_DEFAULT.UPDATE, onClick: () => getForm().update()},
                {...BUTTON_DEFAULT.RETURN, link: '/production/seasonality/seasonality-admin'}
            ],
        },
        grid: {}
    }
}