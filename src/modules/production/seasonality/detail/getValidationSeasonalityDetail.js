
export const getValidationSeasonalityDetail = () => {
    return {
        form: {
            masterItemId: [ {required: true, message: 'Mat. Prima es requerida' } ],
            january: [ {required: true, message: 'ENERO es requerido' } ],
            february: [ {required: true, message: 'FEBRERO es requerido' } ],
            march: [ {required: true, message: 'MARZO es requeridA' } ],
            april: [ {required: true, message: 'ABRIL es requerido' } ],
            may: [ {required: true, message: 'MAYO es requerido' } ],
            june: [ {required: true, message: 'JUNIO es requerido' } ],
            july: [ {required: true, message: 'JULIO es requerido' } ],
            august: [ {required: true, message: 'AGOSTO es requerido' } ],
            september: [ {required: true, message: 'AGOSTO es requerido' } ],
            october: [ {required: true, message: 'OCTUBRE es requerido' } ],
            november: [ {required: true, message: 'NOVIEMBRE es requerido' } ],
            december: [ {required: true, message: 'DICIEMBRE es requerido' } ],
            diverseTask: [ {required: true, message: 'Tarea Diversa es requerida' } ],
        },
        grid: {

        },
        button: {

        },
        bar: {

        }
    }
}
