import React from 'react'
import BarButtonFw from '../../../../framework/bar-button/BarButtonFw';
import FormFw from '../../../../framework/form/FormFw';
import { BasePage } from '../../../../framework/pages/BasePage';
import { getConfigSeasonalityDetail } from './getConfigSeasonalityDetail';
import { getModeSeasonalityDetail } from './getModeSeasonalityDetail';
import { getValidationSeasonalityDetail } from './getValidationSeasonalityDetail';

export class SeasonalityDetailPage extends BasePage {
    
    createPageProperties = (pageApi) => {
        return {
            title: 'REGISTRO DE ESTACIONALIDAD',
            validation: getValidationSeasonalityDetail,
            mode: getModeSeasonalityDetail
        };
    }

    renderPage(pageApi) {

        const config = getConfigSeasonalityDetail(pageApi);

        return (
            <>
                <FormFw
                    name="form"
                    getParentApi={pageApi.getApi}
                    config={config.form}
                />
                <BarButtonFw
                    name="bar"
                    getParentApi={pageApi.getApi}
                    config={config.bar}
                />
            </>
        )
    }
}