import React from 'react';
import FormGridFw, { FORM_GRID_MODE } from '../../../../../framework/form-grid/FormGridFw';
import { SELECTION_TYPE } from '../../../../../framework/grid/GridFw';
import { BasePage } from '../../../../../framework/pages/BasePage';

export class ProductModal extends BasePage {
    
    
    createPageProperties = (pageApi) => {
        return {
            title: 'Busqueda de Items',
        };
    }

    renderPage(pageApi) {
       

        const formGridConfig = {
            mode: FORM_GRID_MODE.FORM_SEARCH_ADD,
            afterAddEvent: (data) => {
                pageApi.getEvent('afterAddEvent')(data);
                pageApi.hideThisModal();
            },
            form: {
                fields:[
                    {name:'code', label:'Código', column: 4},
                    {name:'description', label:'Descripción', column: 4},
                    {type: 'select',name:'itemTypeId', label:'Tipo Item', column: 4, load: {queryId: 185}},
                ]
            },
            grid: {
                title: 'Documentos seleccionados',
                rowId: 'codigo',
                showIndex: false,
                autoLoad: true,
                pagination: false,
                selection: {type: SELECTION_TYPE.SINGLE},
                load: {
                    queryId: 158,
                },
                fields:[
                    {name:'code', label:'Código',},
                    {name:'description', label:'Descripción',},
                    {name:'itemTypeText', label:'Tipo Item'},
                ]
            }
        };  
    
        return (
            <>
                <FormGridFw 
                    name="itemsGrid" 
                    getParentApi={pageApi.getApi}
                    config={formGridConfig}
                />
            </>
        )

    }
    
}