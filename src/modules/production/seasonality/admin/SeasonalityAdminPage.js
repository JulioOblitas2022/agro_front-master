import React from 'react'
import BarButtonFw, { BUTTON_DEFAULT } from '../../../../framework/bar-button/BarButtonFw';
import FormFw from '../../../../framework/form/FormFw';
import GridFw from '../../../../framework/grid/GridFw';
import { BasePage } from '../../../../framework/pages/BasePage';
import { COLUMN_DEFAULT } from '../../../../framework/grid/GridFw';
import { getFormDefaultParams } from '../../../shared/util/getFormDefaultParams';
import { getGridServerDefaultParams } from '../../../shared/util/getGridServerDefaultParams';


export class SeasonalityAdminPage extends BasePage {

    createPagePropertiesBase(pageApi) {
        return {
            title: 'BANDEJA DE ESTACIONALIDAD',
            barHeader: {
                fields:[
                    {...BUTTON_DEFAULT.NEW, label:'Nuevo', link: '/production/seasonality/seasonality/new'}
                ]
            }
        };
    }

    renderPage(pageApi) {
        const getGrid = () => pageApi.getComponent('grid');
        const getForm = () => pageApi.getComponent('form');
        
        const form = {
            ...getFormDefaultParams(),
            title: 'Filtros de búsqueda',
            fields:[
                {name:'code', label:'Codigo', column: 6},
                {type: 'select', name:'masterItemId', label:'Mat. Prima', column: 6, load: {queryId:375}, search: true},
            ]
        }

        const bar = { fields: [
            {...BUTTON_DEFAULT.SEARCH, onClick: ()=>getGrid().load({params:getForm().getData()})},
            {...BUTTON_DEFAULT.CLEAR, onClick: ()=>getForm().reset()}
        ]}

        const grid = {
            ...getGridServerDefaultParams({queryLoad: 377}),
            title: 'Resultados',
            fields:[
                {name: 'code', label: 'Codigo',},
                {name: 'masterItem', label: 'Materia Prima',},
                {name: 'january', label: 'ENE',},
                {name: 'february', label: 'FEB',},
                {name: 'march', label: 'MAR',},
                {name: 'april', label: 'ABR',},
                {name: 'may', label: 'MAY',},
                {name: 'june', label: 'JUN',},
                {name: 'july', label: 'JUL',},
                {name: 'august', label: 'AGO',},
                {name: 'september', label: 'SET',},
                {name: 'october', label: 'OCT',},
                {name: 'november', label: 'NOV',},
                {name: 'december', label: 'DIC',},
                {...COLUMN_DEFAULT.VIEW, link: (value, row, index) => `/production/seasonality/seasonality/${row.seasonalityId}`, label: 'Opciones', colSpan: 3 },
                {...COLUMN_DEFAULT.EDIT, link: (value, row, index) => `/production/seasonality/seasonality/${row.seasonalityId}/update`, colSpan: 0 },
                {...COLUMN_DEFAULT.DELETE, process: {fnOk: (result)=>getGrid().load({params:getForm().getData()}),filter: (value, row, index) => ({queryId: 380, params: {seasonalityId: row.seasonalityId}}) }, colSpan: 0}
            ]
        }
        return (
            <>
                <FormFw
                    name="form"
                    getParentApi={pageApi.getApi}
                    config={form}
                />
                <BarButtonFw
                    name="bar"
                    getParentApi={pageApi.getApi}
                    config={bar}
                />
                <GridFw
                    name="grid"
                    getParentApi={pageApi.getApi}
                    config={grid}
                />
            </>
        )
    }
}