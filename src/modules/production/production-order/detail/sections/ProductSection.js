
import { DATE_FORMAT } from '../../../../../constants/constants';
import { BUTTON_DEFAULT } from '../../../../../framework/bar-button/BarButtonFw';
import GridFw, { COLUMN_DEFAULT } from '../../../../../framework/grid/GridFw';
import { ProductModal } from '../modals/ProductModal';

export const createProductSection = (pageApi) => {
    
    const fnDelete = (value, row, index) => {
        getGrid().deleteItem(row.key);      
    }
    
    const getGrid = () => pageApi.getComponent('ItemsSection');

    const formProduct = () => pageApi.getComponent('form');

    const itemConfig = {
        events: {
            afterAddEvent : (data) => {
                getGrid().addItem(data);
            }
        }
    }

    const ItemModalConfig = (data) =>  {
        return {
            width: 1600,
            maskClosable: false,
            component: <ProductModal name={data}  getParentApi={pageApi.getApi} config={itemConfig}  />
        }
    };


    const barHeaderConfig = {
        fields:[
            {...BUTTON_DEFAULT.SEARCH, label:'Agregar Item', onClick: ()=>{  pageApi.showModal(ItemModalConfig(formProduct()?.getData())); }}
        ]
    }


    const gridConfig = {
        title: 'Items',
        collapsible: true,
        rowId: 'productionOrderDetailId',
        showIndex: true,
        pagination: true,
        autoLoad: true,
        load: {
            validation: () => pageApi.getParamId()!=null, //solo se dispara si el valor es distinto de nulo
            queryId: 471,
            orderBy: 'productionOrderDetailId desc',
            params: {productionOrderId: pageApi.getParamId()}
        },
        save: {
            queryId: 470,
            params: {productionOrderId: pageApi.getParamId()}
        },
        scroll:{
          x: 'auto'  
        },
        fields:[
            { name: 'item', label: 'Producto'},
            { type: 'select', name: 'recipeId', label: 'Receta', editable:true, load: {queryId:469, params: () => {return {masterItemId: getGrid()?.itemEditing?.masterItemId}}}, labelName: 'recipeDes'},
            { type: 'select', name: 'unitMeasureId', label: 'Uni. Med.', labelName: 'unitMeasureDes', load: { queryId: 568, params: () => {return {masterItemId: getGrid()?.itemEditing?.masterItemId}}}, search: true, editable: true },
            { type: 'decimal', name: 'quantity', label:'Cantidad', editable:true},
            { type: 'date', name: 'deliverDate', label: 'Fecha Entrega', editable:true, format: DATE_FORMAT.DDMMYYYY, inputFormat: DATE_FORMAT.YYYYMMDD},
            { type: 'number', name: 'numberArmy', label:'N° Armada', editable:true},
            {...COLUMN_DEFAULT.DELETE, onClick: fnDelete, label:'Opciones'}
        ],
        barHeader: barHeaderConfig
    };

    return (
        <GridFw
            name="ItemsSection"
            getParentApi={pageApi.getApi}
            config={gridConfig} 
        />
    )
}

