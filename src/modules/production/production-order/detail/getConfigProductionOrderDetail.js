import { DATE_FORMAT, PAGE_MODE } from "../../../../constants/constants";
import { BUTTON_DEFAULT } from "../../../../framework/bar-button/BarButtonFw";
import { DateUtil } from "../../../../util/DateUtil";
import { MessageUtil } from "../../../../util/MessageUtil";
import { UserUtil } from "../../../../util/UserUtil";
import { DocReferencesModal } from "./modals/DocReferencesModal";

export const getConfigProductionOrderDetail = (pageApi) => {
    const getForm = () => pageApi.getComponent('form');
    const getProductGrid = () => pageApi.getComponent('ItemsSection');

    const docRefConfig = {
        events: {
            afterAddEvent: (data) => {

                let itemData = JSON.parse(data[0].items);
                if (itemData !== null) {
                    getProductGrid().setData(itemData);
                }
                else {
                    getProductGrid().reset();
                }
                getForm().setFieldData('documentRefId', data[0].documentRefId);
                getForm().setFieldData('documentRefNumber', data[0].documentRefNumber);

            }
        }
    }

    const DocRefModalDocumentConfig = (id) => {
        return {
            width: 1000,
            maskClosable: false,
            component: <DocReferencesModal name={id} getParentApi={pageApi.getApi} config={docRefConfig} />
        }
    };

    return {
        form: {
            autoLoad: true,
            load: {
                queryId: 462,
                params: { productionOrderId: pageApi.getParamId() || 0 },
                fnOk: (resp) => {
                    getProductGrid().load({ params: { productionOrderId: pageApi.getParamId() } });
                }
            },
            update: {
                queryId: 461,
                postLink: (resp, values) => '/production/production-order/production-order-admin',
                params: { modifiedBy: UserUtil.getUserId() },
                fnOk: (resp) => {
                    getProductGrid().save();
                }
            },
            save: {
                queryId: 460,
                postLink: (resp, values) => '/production/production-order/production-order-admin',
                params: { createdBy: UserUtil.getUserId() },
                fnOk: (resp) => {
                    const productionOrderId = resp.dataObject?.productionOrderId;
                    if (productionOrderId) {
                        getProductGrid().save({ params: { productionOrderId } });
                    }

                }
            },
            title: 'Datos Principales',
            fields: [
                { type: 'select', name: 'companyId', label: 'Empresa', column: 4, load: { queryId: 554, }, search: true },
                { name: 'productionOrderNumber', label: 'Nro. Documento', column: 4, readOnly: true},
                { type: 'date', name: 'bradcastDate', label: 'Fecha Emisión', format: DATE_FORMAT.DDMMYYYY, inputFormat: DATE_FORMAT.YYYYMMDD, column: 4, value: DateUtil.getDate },
                { type: 'select', name: 'documentTypeRefId', label: 'Tipo Doc. Ref.', column: 4, load: { queryId: 458 } },
                { type: 'hidden', name: 'documentRefId'},
                { name: 'documentRefNumber', label: 'Nro. Documento', column: 4, readOnly: true},
                { type: 'button', column: 1, name: 'modal',
                    onClick: (value) => {
                        let documentTypeRefId = getForm()?.getFieldApi('documentTypeRefId')?.getSelectedItem()?.value;
                        if (documentTypeRefId !== undefined) {
                            pageApi.showModal(DocRefModalDocumentConfig(documentTypeRefId));
                        } else {
                            return MessageUtil('warning', 'Alerta!', 'Debe Seleccionar el Tipo de Documento de Referencia');
                        }

                    },
                },
                { type: 'select', name: 'responsableId', label: 'Responsable', column: 4, load: { queryId: 309 } },
                { type: 'select', name: 'priorityId', label: 'Prioridad', column: 4, load: { queryId: 252 } },
                { type: 'date', name: 'deliverDate', label: 'Fecha Entrega', format: DATE_FORMAT.DDMMYYYY, inputFormat: DATE_FORMAT.YYYYMMDD, column: 4, },
                { type: 'textarea', name: 'observations', label: 'Observaciones', column: 8 },
            ],
            onComponentLoad: (formApi, pageApi) => {
                if (pageApi.getPageMode() !== PAGE_MODE.NEW)
                    formApi.load();
            }
        },
        bar: {
            fields: [
                { ...BUTTON_DEFAULT.SAVE, onClick: () => getForm().save() },
                { ...BUTTON_DEFAULT.UPDATE, onClick: () => getForm().update() },
                { ...BUTTON_DEFAULT.RETURN, link: '/production/production-order/production-order-admin' }
            ],
        },
        grid: {}
    }
}