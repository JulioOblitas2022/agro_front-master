import React from 'react'
import BarButtonFw from '../../../../framework/bar-button/BarButtonFw';
import FormFw from '../../../../framework/form/FormFw';
import { BasePage } from '../../../../framework/pages/BasePage';
import { getConfigProductionOrderDetail } from './getConfigProductionOrderDetail';
import { getModeProductionOrderDetail } from './getModeProductionOrderDetail';
import { getValidationProductionOrderDetail } from './getValidationProductionOrderDetail';
import { createProductSection } from './sections/ProductSection';

export class ProductionOrderDetailPage extends BasePage {

    createPageProperties = (pageApi) => {
        return {
            title: 'REGISTRO DE ORDEN DE PRODUCCION',
            validation: getValidationProductionOrderDetail,
            mode: getModeProductionOrderDetail
        };
    }

    renderPage(pageApi) {

        const config = getConfigProductionOrderDetail(pageApi);

        return (
            <>
                <FormFw
                    name="form"
                    getParentApi={pageApi.getApi}
                    config={config.form}
                />
                {createProductSection(pageApi)}
                
                <BarButtonFw
                    name="bar"
                    getParentApi={pageApi.getApi}
                    config={config.bar}
                />
            </>
        )
    }
}
