import React from 'react'
import BarButtonFw, { BUTTON_DEFAULT } from '../../../../framework/bar-button/BarButtonFw';
import FormFw from '../../../../framework/form/FormFw';
import GridFw from '../../../../framework/grid/GridFw';
import { BasePage } from '../../../../framework/pages/BasePage';
import { DATE_FORMAT } from "../../../../constants/constants";
import { COLUMN_DEFAULT } from "../../../../framework/grid/GridFw";
import { getFormDefaultParams } from "../../../shared/util/getFormDefaultParams";
import { getGridServerDefaultParams } from "../../../shared/util/getGridServerDefaultParams";

export class ProductionOrderAdminPage extends BasePage {

    createPageProperties = (pageApi) => {
        return {
            title: 'BANDEJA DE ORDEN DE PRODUCCION',
            barHeader: {
                fields:[
                    {...BUTTON_DEFAULT.NEW, label:'Nuevo', link: '/production/production-order/production-order/new'}
                ]
            }
        };
    }

    renderPage(pageApi) {
        const getGrid = () => pageApi.getComponent('grid');
        const getForm = () => pageApi.getComponent('form');

        const form = {
            ...getFormDefaultParams(),
            fields:[
                {name:'productionOrderNumber', label:'N° Orden', column: 4},
                {type: 'select', name:'documentTypeRefId', label:'Tipo Doc. Ref.', column: 4, load: { queryId: 458 } },
                // {type: 'date', name: 'entryDate', label: 'Fecha Emision', column: 4, format: 'DD/MM/YYYY', inputFormat: DATE_FORMAT.YYYYMMDD},
                // {type: 'date', name: 'deliveryDate', label: 'Fecha Entrega', column: 4, format: 'DD/MM/YYYY', inputFormat: DATE_FORMAT.YYYYMMDD},
            ]
        };
        
        const bar = { 
            fields: [
                {...BUTTON_DEFAULT.SEARCH, onClick: ()=>getGrid().load({params:getForm().getData()})},
                {...BUTTON_DEFAULT.CLEAR, onClick: ()=>getForm().reset()}
            ]
        };

        const grid = {
            ...getGridServerDefaultParams({queryLoad: 463}),
            fields:[
                {name: 'productionOrderNumber', label: 'N° Orden', sorter: {multiple: 1}},
                {type: 'date', name: 'bradcastDate', label: 'Fecha Emision', format: DATE_FORMAT.DDMMYYYY},
                {type: 'date', name: 'deliverDate', label: 'Fecha Entrega', format: DATE_FORMAT.DDMMYYYY},
                {name: 'documentType', label: 'Tip. Doc. Ref'},
                {name: 'docRefNumber', label: 'N° Doc. Ref'},
                {name: 'itemsNumber', label: 'N° Items'},
                { ...COLUMN_DEFAULT.VIEW, link: (value, row, index) => `/production/production-order/production-order/${row.productionOrderId}`, label: 'Opciones', colSpan: 2 },
                { ...COLUMN_DEFAULT.EDIT, link: (value, row, index) => `/production/production-order/production-order/${row.productionOrderId}/update`, colSpan: 0 },
            ]
        }

        return (
            <>
                <FormFw
                    name="form"
                    getParentApi={pageApi.getApi}
                    config={form}
                />
                <BarButtonFw
                    name="bar"
                    getParentApi={pageApi.getApi}
                    config={bar}
                />
                <GridFw
                    name="grid"
                    getParentApi={pageApi.getApi}
                    config={grid}
                />
            </>
        )
    }
}