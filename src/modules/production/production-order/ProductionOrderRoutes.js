import React from 'react'
import { Route, Switch, useRouteMatch } from 'react-router';
import { ProductionOrderAdminPage } from './admin/ProductionOrderAdminPage';
import { ProductionOrderDetailPage } from './detail/ProductionOrderDetailPage';

const ProductionOrderRoutes = () => {
    const { path } = useRouteMatch();
    return (
        <Switch>
            <Route exact path={`${path}/production-order-admin`} component={ProductionOrderAdminPage} />
            <Route path={`${path}/production-order`} component={ProductionOrderDetailPage} />
            <Route path={`${path}/production-order/:id`} component={ProductionOrderDetailPage} /> 
        </Switch>
    )
}

export default ProductionOrderRoutes;