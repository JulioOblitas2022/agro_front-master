import React from 'react'
import { Route, Switch, useRouteMatch } from 'react-router';
import { ProductionTasksAdminPage } from './admin/ProductionTasksAdminPage';
import { ProductionTasksDetailPage } from './detail/ProductionTasksDetailPage';

const ProductionTasksRoutes = () => {
    const { path } = useRouteMatch();
    return (
        <Switch>
            <Route exact path={`${path}/production-tasks-admin`} component={ProductionTasksAdminPage} />
            <Route path={`${path}/production-tasks`} component={ProductionTasksDetailPage} />
            <Route path={`${path}/production-tasks/:id`} component={ProductionTasksDetailPage} /> 
        </Switch>
    )
}

export default ProductionTasksRoutes;