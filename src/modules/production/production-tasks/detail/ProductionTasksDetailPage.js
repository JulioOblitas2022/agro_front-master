import React from 'react'
import BarButtonFw from '../../../../framework/bar-button/BarButtonFw';
import FormFw from '../../../../framework/form/FormFw';
import { BasePage } from '../../../../framework/pages/BasePage';
import { getConfigProductionTasksDetail } from './getConfigProductionTasksDetail';
import { getModeProductionTasksDetail } from './getModeProductionTasksDetail';
import { getValidationProductionTasksDetail } from './getValidationProductionTasksDetail';
import { createPersonalSection } from './sections/PersonalSection';
import { createTaskSection } from './sections/TasksSection';

export class ProductionTasksDetailPage extends BasePage {
    
    createPageProperties = (pageApi) => {
        return {
            title: 'REGISTRO DE TAREAS DE PRODUCCIÓN',
            validation: getValidationProductionTasksDetail,
            mode: getModeProductionTasksDetail
        };
    }

    renderPage(pageApi) {

        const config = getConfigProductionTasksDetail(pageApi);

        return (
            <>
                <FormFw
                    name="form"
                    getParentApi={pageApi.getApi}
                    config={config.form}
                />

                {createTaskSection(pageApi)}

                {createPersonalSection(pageApi)}

                <BarButtonFw
                    name="bar"
                    getParentApi={pageApi.getApi}
                    config={config.bar}
                />
            </>
        )
    }
}