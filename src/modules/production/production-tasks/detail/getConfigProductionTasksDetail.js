import { DATE_FORMAT, PAGE_MODE } from "../../../../constants/constants";
import { BUTTON_DEFAULT } from "../../../../framework/bar-button/BarButtonFw";
import { UserUtil } from "../../../../util/UserUtil";

export const getConfigProductionTasksDetail = (pageApi) => {
    const getForm = () => pageApi.getComponent('form');

    return {
        form: {
            load:{
                queryId: 454,
                params: {equipmentId: pageApi.getParamId()},
            },
            update:{
                queryId: 456,
                postLink: (resp, values) => '/production/equipment/equipment-admin',
                params: {modifiedBy: UserUtil.getUserId()}
            },
            save:{
                queryId: 451,
                postLink: (resp, values) => '/production/equipment/equipment-admin',
                params: {createdBy: UserUtil.getUserId()}
            },
            title: 'DATOS PRINCIPALES',
            fields:[
                {
                    label: "DATOS DE LA SOLICITUD",
                    fields: [
                        { type: 'select', name: 'documentType', label: 'Tipo Documento', value:62, column: 4, load: { queryId: 195 }, readOnly: true },
                        { type: 'date', name: 'dateRegister', label: 'Fecha Registro', column: 4 },
                        { name: 'document', label: 'Nro. Documento', column: 4 },
                        { type: 'select', name: 'applicant', label: 'Solicitante', column: 12, load: { queryId: 309 }, search: true },
                    ]
                },
                {
                    label: "DATOS DE LA PRODUCCIÓN",
                    
                    fields: [
                        
                        {type: 'select', name:'productionRecordId', label:'Nro. Producción', column: 6, load: {queryId:501}, search: true,
                            onChange: (value, values) => {
                                let productionQuantity = getForm().getFieldApi('productionRecordId').getSelectedItem()?.productionQuantity;
                                let productionDate = getForm().getFieldApi('productionRecordId').getSelectedItem()?.productionDate;
                                let product = getForm().getFieldApi('productionRecordId').getSelectedItem()?.product;
                                let unitMeasure = getForm().getFieldApi('productionRecordId').getSelectedItem()?.unitMeasure;
                                let recipe = getForm().getFieldApi('productionRecordId').getSelectedItem()?.recipe;
                                let productionOrderNumber = getForm().getFieldApi('productionRecordId').getSelectedItem()?.productionOrderNumber;
                                let productionOrderDate = getForm().getFieldApi('productionRecordId').getSelectedItem()?.productionOrderDate;

                                getForm()?.setFieldData('productionQuantity', productionQuantity); 
                                getForm()?.setFieldData('productionDate', productionDate); 
                                getForm()?.setFieldData('product', product);
                                getForm()?.setFieldData('unitMeasure', unitMeasure); 
                                getForm()?.setFieldData('recipe', recipe); 
                                getForm()?.setFieldData('productionOrderNumber', productionOrderNumber); 
                                getForm()?.setFieldData('productionOrderDate', productionOrderDate); 
                            }
                        },
                        { type: 'select', name: 'productionType', label: 'Tipo Busqueda Prod.', column: 4, load: { queryId: 452 } },
                        {name: 'product', label:'Producto', column: 4,},
                        {name: 'unitMeasure', label:'Uni. Medida', column: 4,},
                        {name:'productionQuantity', label:'Cantidad a Producir', column: 4,},
                        {type: 'date', name:'productionDate', label:'Fec. Producción', column: 4, format: DATE_FORMAT.DDMMYYYY, inputFormat: DATE_FORMAT.YYYYMMDD},
                        {name: 'recipe', label:'Receta', column: 4,},
                        {name: 'productionOrderNumber', label:'Nro. Ord. Prod.', column: 4,},
                        {type: 'date', name:'productionOrderDate', label:'Fec. Ord.Prod.', column: 4, format: DATE_FORMAT.DDMMYYYY, inputFormat: DATE_FORMAT.YYYYMMDD},
                        {type: 'textarea',name: 'observations', label:'Observaciones', column: 12,}
                    ]
                }
            ],
            onComponentLoad : (formApi, pageApi) =>{
                if (pageApi.getPageMode() !== PAGE_MODE.NEW)
                    formApi.load();
            }
        },
        bar: {
            fields: [
                {...BUTTON_DEFAULT.SAVE, onClick: () => getForm().save()},
                {...BUTTON_DEFAULT.UPDATE, onClick: () => getForm().update()},
                {...BUTTON_DEFAULT.RETURN, link: '/production/production-tasks/production-tasks-admin'}
            ],
        },
        grid: {}
    }
}