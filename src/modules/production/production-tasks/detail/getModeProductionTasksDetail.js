import { MODE_SHORTCUT } from '../../../../constants/constants'

export const getModeProductionTasksDetail = () => {
    return {
        new: {
            form: {
                hide: ['equipmentId']
            },
            bar: {
                show: ['save', 'return']
            }
        },
        update:{
            form: {
                read: ['equipmentId']
            },
            bar: {
                show: ['update', 'return']
            }
        },
        read:{
            form: {
                read: [MODE_SHORTCUT.ALL]
            },
            bar: {
                show: ['return']
            }
        }
    }
}
