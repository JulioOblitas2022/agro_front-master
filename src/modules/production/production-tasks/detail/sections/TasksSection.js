
import { DATE_FORMAT } from '../../../../../constants/constants';
import { BUTTON_DEFAULT } from '../../../../../framework/bar-button/BarButtonFw';
import FormGridFw, { FORM_GRID_MODE } from '../../../../../framework/form-grid/FormGridFw';
import GridFw, { COLUMN_DEFAULT } from '../../../../../framework/grid/GridFw';

export const createTaskSection = (pageApi) => {
    
    const fnDelete = (value, row, index) => {
        getGrid().deleteItem(row.key);      
    }
    
    const getGrid = () => pageApi.getComponent('TasksSection');

    const fnNew = () => {
        getGrid().addItem({}, true);
    }

    // const barHeaderConfig = {
    //     fields:[
    //         {...BUTTON_DEFAULT.NEW, label:'Agregar Tarea', onClick: fnNew}
    //     ]
    // }


    // const gridConfig = {
    //     title: 'Tareas Disponibles',
    //     collapsible: true,
    //     rowId: 'productionRecordDetailId',
    //     showIndex: true,
    //     pagination: true,
    //     autoLoad: true,
    //     load: {
    //         validation: () => pageApi.getParamId()!=null, //solo se dispara si el valor es distinto de nulo
    //         queryId: 464,
    //         orderBy: 'productionRecordDetailId desc',
    //         params: {taskRequestId: pageApi.getParamId()}
    //     },
    //     save: {
    //         queryId: 457,
    //         params: {taskRequestId: pageApi.getParamId()}
    //     },
    //     scroll:{
    //       x:1300  
    //     },
    //     fields: [
    //         { name: 'task', label: 'Tarea', allowClear: true, editable: true, width: 150 },
    //         { type: 'date', name: "dateProcess", label: "Fecha Proceso", editable: true, format: 'DD/MM/YYYY', inputFormat: DATE_FORMAT.YYYYMMDD, },
    //         { type: 'date', name: "startTime", label: "Hora Inicio", editable: true, format: DATE_FORMAT.HHmm, inputFormat: DATE_FORMAT.HHmm, },
    //         { type: 'date', name: "endTime", label: "Hora Termino", editable: true, format: DATE_FORMAT.HHmm, inputFormat: DATE_FORMAT.HHmm, },
    //         { type: 'number', name: "people", label: "Nro.Personas", editable: true },
    //         { type: 'select', name: "unit", label: "Unidad M.",labelName: 'unitdes', editable: true, load: { queryId: 192 } },
    //         { type: 'number', name: "quantity", label: "Cantidad", editable: true },
    //         { type: 'number', name: "costTask", label: "Costo Tarea", editable: true },
    //         {type: 'select', name: "toPrint",  label: "Imprimir", labelName: 'printdes',  editable: true, load: {
    //                 queryId: 465
    //             }
    //         },
    //         { type: 'number', name: "orderTask", label: "Orden", editable: true },
    //         { ...COLUMN_DEFAULT.DELETE, label: 'Opciones', onClick: fnDelete, confirm: false }
    //     ],
    //     barHeader: barHeaderConfig
    // };

    const gridConfig = {
        //title: 'Tareas',
        mode: FORM_GRID_MODE.FORM_ADD,
        collapsible: true,
        form: {
            fields:[
                {type:'select', name:'tasksId', label:'Seleccionar Tareas', multiple:true, search: true, column: 12, load: {queryId: 526}},
            ],
            dataOptions: {
                out: {
                    fnTransform: (values, api) => {
                        let result = [];
                        if (values['tasksId'].length > 0){
                            let items = api.getFieldApi('tasksId').getSelectedItem();
                            result = items.map(item => ({tasksId: item.value, task: item.label, unitMeasureId: item.unitMeasureId , unitMeasure: item.unitMeasure, price: item.price}));
                        }                                                
                        return result;                                                                 
                    }
                }
            },
        },
        grid: {
            title: 'Tareas',
            rowId: 'outputDetailId',
            showIndex: true,
            pagination: true,
            autoLoad: true,
            collapsible: true,
            load: {
                validation: () => pageApi.getParamId()!=null, //solo se dispara si el valor es distinto de nulo
                queryId: 464,
                orderBy: 'productionRecordDetailId desc',
                params: {taskRequestId: pageApi.getParamId()}
            },
            save: {
                queryId: 457,
                params: {taskRequestId: pageApi.getParamId()}
            },
            scroll:{
              x:1300  
            },
            fields: [
                { name: 'task', label: 'Tarea', allowClear: true, editable: true, width: 200, column: 12 },
                { type: 'date', name: "dateProcess", label: "Fecha Proceso", editable: true, width: 200, column: 12, format: 'DD/MM/YYYY', inputFormat: DATE_FORMAT.YYYYMMDD, },
                { type: 'date', name: "startTime", label: "Hora Inicio", editable: true, width: 200, column: 12, format: DATE_FORMAT.HHmm, inputFormat: DATE_FORMAT.HHmm, },
                { type: 'date', name: "endTime", label: "Hora Termino", editable: true, width: 200, column: 12, format: DATE_FORMAT.HHmm, inputFormat: DATE_FORMAT.HHmm, },
                { type: 'number', name: "people", label: "Nro.Personas", editable: true, width: 200, column: 12 },
                { name: "unitMeasure", label: "Unidad M.",width: 200, column: 12},
                { type: 'number', name: "quantity", label: "Cantidad", editable: true, width: 200, column: 12 },
                { type: 'number', name: "price", label: "Costo Tarea", editable: true, width: 200, column: 12 },
                { type: 'select', name: "toPrint",  label: "Imprimir", labelName: 'printdes',  editable: true, width: 200, column: 12, load: { queryId: 465 } },
                { type: 'number', name: "orderTask", label: "Orden", editable: true, width: 200, column: 12 },
            ],
        }
    };

    return (
        <FormGridFw
            name="TasksSection"
            getParentApi={pageApi.getApi}
            config={gridConfig} 
        />
    )
}

