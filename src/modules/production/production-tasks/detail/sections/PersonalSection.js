
import moment from 'moment';
import { DATE_FORMAT } from '../../../../../constants/constants';
import { BUTTON_DEFAULT } from '../../../../../framework/bar-button/BarButtonFw';
import GridFw, { COLUMN_DEFAULT } from '../../../../../framework/grid/GridFw';


export const createPersonalSection = (pageApi) => {
    
    const fnDelete = (value, row, index) => {
        getGrid().deleteItem(row.key);      
    }
    
    const getGrid = () => pageApi.getComponent('PersonalSection');

    const fnNew = () => {
        getGrid().addItem({}, true);
    }

    const barHeaderConfig = {
        fields: [
            { ...BUTTON_DEFAULT.NEW, label: 'Calcular Horas', onClick: ()=> {
                const nuevo = getGrid().getData().map(data => {
                    var hora1 = moment(data.endTime, 'HH:mm')
                    var hora2 = moment(data.startTime, 'HH:mm')
                    const restaTiempo = hora1.diff(hora2)
                    const difFechaMinutos = moment.duration(restaTiempo);
                    const horaCalculada = difFechaMinutos.format("HH:mm");
                    data.decimalHour = horaCalculada    
                    data.totalHour = horaCalculada                       
                    return data
                })
                getGrid().setData(nuevo)
            } },
            { ...BUTTON_DEFAULT.NEW, label: 'Ranking de Tarea', onClick: fnNew },
            { ...BUTTON_DEFAULT.NEW, label: 'Agregar Personal', onClick: fnNew },
        ]
    }


    const gridConfig = {
        title: 'Personal Disponible',
        collapsible: true,
        rowId: 'productionRecordDetailId',
        showIndex: true,
        pagination: true,
        autoLoad: true,
        load: {
            validation: () => pageApi.getParamId()!=null, //solo se dispara si el valor es distinto de nulo
            queryId: 466,
            orderBy: 'productionRecordDetailId desc',
            params: {taskRequestId: pageApi.getParamId()}
        },
        save: {
            queryId: 467,
            params: {taskRequestId: pageApi.getParamId()}
        },
        scroll:{
          x:1300  
        },
        fields: [
            { name: 'namePeople', label: 'Persona', allowClear: true, editable: true, width: 150 },
            { type: 'date', name: "startTime", label: "Hora Inicio", editable: true, format: DATE_FORMAT.HHmm, inputFormat: DATE_FORMAT.HHmm,  },
            { type: 'date', name: "endTime", label: "Hora Termino", editable: true, format: DATE_FORMAT.HHmm, inputFormat: DATE_FORMAT.HHmm, },
            { type: 'number', name: "package", label: "Nro.Envases", editable: true },
            { type: 'number', name: "grossWeight", label: "Peso Bruto", editable: true },
            { type: 'number', name: "quantity", label: "Cantidad", editable: true },
            { name: "decimalHour", label: "Hora Decimal", readOnly: true, onChange: () => {
                console.log(getGrid()?.getFormEditable()?.getData(), 'editable')
            } },
            { name: "totalHour", label: "Horas total", readOnly: true },
            { name: "unit", label: "Kg/Hr/P",  readOnly: true },
            { ...COLUMN_DEFAULT.DELETE, label: 'Opciones', onClick: fnDelete, confirm: false }
        ],
        barHeader: barHeaderConfig
    };

    return (
        <GridFw
            name="PersonalSection"
            getParentApi={pageApi.getApi}
            config={gridConfig} 
        />
    )
}

