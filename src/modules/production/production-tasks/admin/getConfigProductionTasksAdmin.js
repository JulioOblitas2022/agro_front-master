import { DATE_FORMAT } from "../../../../constants/constants";
import { BUTTON_DEFAULT } from "../../../../framework/bar-button/BarButtonFw";
import { COLUMN_DEFAULT } from "../../../../framework/grid/GridFw";
import { B_SELECT_CONSTANTS } from "../../../shared/constantes/BSelectConstants";
import { getFormDefaultParams } from "../../../shared/util/getFormDefaultParams";
import { getGridServerDefaultParams } from "../../../shared/util/getGridServerDefaultParams";

export const getConfigProductionTasksAdmin = (pageApi) => {
    const getGrid = () => pageApi.getComponent('grid');
    const getForm = () => pageApi.getComponent('form');
    
    return {
        form : {
            ...getFormDefaultParams(),
            title: 'Filtros de Búsqueda',
            fields: [
                { name: 'documentPerson', label: 'Nro. Producción', column: 4 },
                { type: 'select', name: 'TIPO_SERVICIO', label: 'Producto', search: true, column: 4, load: { queryId: 70 } },
                { type: 'select', name: 'TIPO_SERVICIO', label: 'Cod. Receta', search: true, column: 4, load: { queryId: 70 } },
                { type: 'date', name: 'fch1', label: 'Fecha Emisión', column: 4 },
                { type: 'date', name:'fch2', label: 'Fecha Entrega', column: 4 },
                {...B_SELECT_CONSTANTS.ACTIVE, column: 4},
            ]
        },
        bar: { fields: [
            {...BUTTON_DEFAULT.SEARCH, onClick: ()=>getGrid().load({params:getForm().getData()})},
            {...BUTTON_DEFAULT.CLEAR, onClick: ()=>getForm().reset()}
        ]},
        grid: {
            ...getGridServerDefaultParams({queryLoad: 453}),
            title: 'Resultados',
            fields:[
                { name: 'dateRegister', label: 'Fecha Registro' },
                { name: 'document', label: 'Nro.Documento'},
                { name: 'applicant', label: 'Responsable'},
                { name: '', label: 'Local'},
                { name: 'production', label: 'Nro.Producción'},
                { name: 'product', label: 'Producto'},
                { name: 'unit', label: 'Unidad Medida'},
                { name: 'birthDate', label: 'Estado'},
                { ...COLUMN_DEFAULT.EDIT, label: 'Opciones', colSpan:2,
                link: (value, row, index) => `/production/production-tasks/taskRequest/${row.taskRequestId}/update`
                },
                {
                    ...COLUMN_DEFAULT.DELETE, colSpan: 0,
                    process: {
                        fnOk: (result) => getGrid().load({ params: getForm().getData() }),
                        filter: (value, row, index) => ({ queryId: 455, params: { taskRequestId: row.taskRequestId } })
                    }
                },    
            ]
        }
    }
}