import React from 'react'
import { Route, Switch, useRouteMatch } from 'react-router';
import { ScheduleCalendarProductionAdminPage } from './admin/ScheduleCalendarProductionAdminPage';

const ScheduleCalendarProductionRoutes = () => {
    const { path } = useRouteMatch();
    return (
        <Switch>
            <Route exact path={`${path}/schedule-calendar-production-admin`} component={ScheduleCalendarProductionAdminPage} />
        </Switch>
    )
}

export default ScheduleCalendarProductionRoutes;