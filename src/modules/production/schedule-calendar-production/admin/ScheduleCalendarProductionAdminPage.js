import React from 'react'
import FormFw from '../../../../framework/form/FormFw';
import GridFw from '../../../../framework/grid/GridFw';
import { BasePage } from '../../../../framework/pages/BasePage';
import { getFormDefaultParams } from '../../../shared/util/getFormDefaultParams';

export class ScheduleCalendarProductionAdminPage extends BasePage {
    createPagePropertiesBase(pageApi) {
        return {
            title: 'CRONOGRAMA DE PRODUCCIÓN',
        };
    }

    renderPage(pageApi) {

        const getForm = () => pageApi.getComponent('form');

        const form = {
            ...getFormDefaultParams(),
            fields:[
                {type: 'calendar', name:'calendar'},
            ]
        };

        return (
            <>
                <FormFw
                    name="form"
                    getParentApi={pageApi.getApi}
                    config={form}
                />
            </>
        )
    }
}
