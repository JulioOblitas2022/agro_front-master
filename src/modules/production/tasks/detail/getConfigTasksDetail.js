import { PAGE_MODE } from "../../../../constants/constants";
import { BUTTON_DEFAULT } from "../../../../framework/bar-button/BarButtonFw";
import { UserUtil } from "../../../../util/UserUtil";

export const getConfigTasksDetail = (pageApi) => {
    const getForm = () => pageApi.getComponent('form');

    return {
        form: {
            load:{
                queryId: 302,
                params: {taskId: pageApi.getParamId()},
            },
            update:{
                queryId: 304,
                postLink: (resp, values) => '/production/tasks/tasks-admin',
                params: {modifiedBy: UserUtil.getUserId()}
            },
            save:{
                queryId: 301,
                postLink: (resp, values) => '/production/tasks/tasks-admin',
                params: {createdBy: UserUtil.getUserId()}
            },
            title: 'DATOS PRINCIPALES',
            fields:[
                {name:'code', label:'Codigo', column: 4},
                {name:'description', label:'Descripcion', column: 4},
                {name:'abbreviation', label:'Abrev.', column: 4},
                {type: 'select', name:'unitMeasureId', label:'Unidad Medida', column: 4, load: {queryId: 192}},
                {type: 'decimal', name:'price', label:'Precio', column: 4},
                {type: 'textarea', name:'observations', label:'Observaciones', column: 12, rows: 5},
                {type: 'radio', name:'diverseTask', label:'Tarea Diversa', column: 4, load: {queryId: 250}},
            ],
            onComponentLoad : (formApi, pageApi) =>{
                if (pageApi.getPageMode() !== PAGE_MODE.NEW)
                    formApi.load();
            }
        },
        bar: {
            fields: [
                {...BUTTON_DEFAULT.SAVE, onClick: () => getForm().save()},
                {...BUTTON_DEFAULT.UPDATE, onClick: () => getForm().update()},
                {...BUTTON_DEFAULT.RETURN, link: '/production/tasks/tasks-admin'}
            ],
        },
        grid: {}
    }
}