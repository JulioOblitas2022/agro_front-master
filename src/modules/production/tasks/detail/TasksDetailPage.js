import React from 'react'
import BarButtonFw from '../../../../framework/bar-button/BarButtonFw';
import FormFw from '../../../../framework/form/FormFw';
import { BasePage } from '../../../../framework/pages/BasePage';
import { getConfigTasksDetail } from './getConfigTasksDetail';
import { getModeTasksDetail } from './getModeTasksDetail';
import { getValidationTasksDetail } from './getValidationTasksDetail';

export class TasksDetailPage extends BasePage {
    
    createPageProperties = (pageApi) => {
        return {
            title: 'REGISTRO DE TAREA',
            validation: getValidationTasksDetail,
            mode: getModeTasksDetail
        };
    }

    renderPage(pageApi) {

        const config = getConfigTasksDetail(pageApi);

        return (
            <>
                <FormFw
                    name="form"
                    getParentApi={pageApi.getApi}
                    config={config.form}
                />
                <BarButtonFw
                    name="bar"
                    getParentApi={pageApi.getApi}
                    config={config.bar}
                />
            </>
        )
    }
}