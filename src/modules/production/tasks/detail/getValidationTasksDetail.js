
export const getValidationTasksDetail = () => {
    return {
        form: {
            code: [ {required: true, message: 'Codigo es requerido' } ],
            description: [ {required: true, message: 'Descripcion es requerida' } ],
            abbreviation: [ {required: true, message: 'Abrev. es requerida' } ],
            unitMeasureId: [ {required: true, message: 'Unidad de Medida es requerida' } ],
            price: [ {required: true, message: 'Precio es requerido' } ],
            diverseTask: [ {required: true, message: 'Tarea Diversa es requerida' } ],
        },
        grid: {

        },
        button: {

        },
        bar: {

        }
    }
}
