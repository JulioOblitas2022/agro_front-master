import React from 'react'
import BarButtonFw, { BUTTON_DEFAULT } from '../../../../framework/bar-button/BarButtonFw';
import FormFw from '../../../../framework/form/FormFw';
import GridFw from '../../../../framework/grid/GridFw';
import { BasePage } from '../../../../framework/pages/BasePage';
import { COLUMN_DEFAULT } from "../../../../framework/grid/GridFw";
import { getFormDefaultParams } from "../../../shared/util/getFormDefaultParams";
import { getGridServerDefaultParams } from "../../../shared/util/getGridServerDefaultParams";

export class TasksAdminPage extends BasePage {

    createPagePropertiesBase(pageApi) {
        return {
            title: 'TAREAS',
            barHeader: {
                fields:[
                    {...BUTTON_DEFAULT.NEW, label:'Nuevo', link: '/production/tasks/tasks/new'}
                ]
            }
        };
    }

    renderPage(pageApi) {
        const getGrid = () => pageApi.getComponent('grid');
        const getForm = () => pageApi.getComponent('form');

        const form = {
            ...getFormDefaultParams(),
            title: 'Filtros de búsqueda',
            fields:[
                {name:'code', label:'Codigo', column: 3},
                {name:'description', label:'Descripcion', column: 3},
            ]
        };
        
        const bar = { fields: [
            {...BUTTON_DEFAULT.SEARCH, onClick: ()=>getGrid().load({params:getForm().getData()})},
            {...BUTTON_DEFAULT.CLEAR, onClick: ()=>getForm().reset()}
        ]};

        const grid = {
            ...getGridServerDefaultParams({queryLoad: 303}),
            title: 'Listado de Tareas',
            fields:[
                {name: 'code', label: 'Codigo',},
                {name: 'description', label: 'Descripcion',},
                {name: 'abbreviation', label: 'Abrev',},
                {name: 'unitMeasure', label: 'Uni. Med.',},
                {type: 'decimal', name: 'price', label: 'Precio'},
                {name: 'diverseTask', label: 'Es Diverso',},
                {...COLUMN_DEFAULT.VIEW, link: (value, row, index) => `/production/tasks/tasks/${row.taskId}`, label: 'Opciones', colSpan: 3 },
                {...COLUMN_DEFAULT.EDIT, link: (value, row, index) => `/production/tasks/tasks/${row.taskId}/update`, colSpan: 0 },
                {...COLUMN_DEFAULT.DELETE, process: {fnOk: (result)=>getGrid().load({params:getForm().getData()}),filter: (value, row, index) => ({queryId: 305, params: {taskId: row.taskId}}) }, colSpan: 0}
            ]
        };

        return (
            <>
                <FormFw
                    name="form"
                    getParentApi={pageApi.getApi}
                    config={form}
                />
                <BarButtonFw
                    name="bar"
                    getParentApi={pageApi.getApi}
                    config={bar}
                />
                <GridFw
                    name="grid"
                    getParentApi={pageApi.getApi}
                    config={grid}
                />
            </>
        )
    }
}