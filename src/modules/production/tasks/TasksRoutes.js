import React from 'react'
import { Route, Switch, useRouteMatch } from 'react-router';
import { TasksAdminPage } from './admin/TasksAdminPage';
import { TasksDetailPage } from './detail/TasksDetailPage';

const TasksRoutes = () => {
    const { path } = useRouteMatch();
    return (
        <Switch>
            <Route exact path={`${path}/tasks-admin`} component={TasksAdminPage} />
            <Route path={`${path}/tasks`} component={TasksDetailPage} />
            <Route path={`${path}/tasks/:id`} component={TasksDetailPage} /> 
        </Switch>
    )
}

export default TasksRoutes;