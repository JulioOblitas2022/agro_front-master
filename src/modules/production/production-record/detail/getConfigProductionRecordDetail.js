import { DATE_FORMAT, PAGE_MODE } from "../../../../constants/constants";
import { BUTTON_DEFAULT } from "../../../../framework/bar-button/BarButtonFw";
import { UserUtil } from "../../../../util/UserUtil";
import { DateUtil } from "../../../../util/DateUtil";

export const getConfigProductionRecordDetail = (pageApi) => {
    const getForm = () => pageApi.getComponent('form');
    const getGridItem = () => pageApi.getComponent('ItemsSection');

    return {
        form: {
            load: {
                queryId: 487,
                params: { productionRecordId: pageApi.getParamId() },
                fnOk: (resp) => {
                    getGridItem().load({ params: { productionRecordId: pageApi.getParamId() } });
                }
            },
            update: {
                queryId: 486,
                postLink: (resp, values) => '/production/production-record/production-record-admin',
                params: { modifiedBy: UserUtil.getUserId() },
                fnOk: (resp) => {
                    getGridItem().save({ params: { productionRecordId: pageApi.getParamId() } });
                }
            },
            save: {
                queryId: 485,
                postLink: (resp, values) => '/production/production-record/production-record-admin',
                params: { createdBy: UserUtil.getUserId() },
                fnOk: (resp) => {
                    let productionRecordId = resp.dataObject?.productionRecordId;
                    if (productionRecordId) {
                        getGridItem().save({ params: { productionRecordId } });
                    }
                }
            },
            title: 'DATOS PRINCIPALES',
            fields: [
                { type: 'select', name: 'productionTypeId', label: 'Tipo Producción', column: 4, load: { queryId: 478 } },
                { type: 'select', name: 'documentTypeId', label: 'Tipo Documento', column: 4, load: { queryId: 195 }, value: "PARTE DE PRODUCCION" },
                { type: 'date', name: 'recordDate', label: 'Fec. Registro', column: 4, format: DATE_FORMAT.DDMMYYYY, inputFormat: DATE_FORMAT.YYYYMMDD, value: DateUtil.getDate, readOnly: true },
                //{ type: 'select', name: 'documentRefId', label: 'Doc. Referencia', column: 6, load: { queryId: 195 }, value: 56 },
                { type: 'select', name: 'documentRefId', label: 'Doc. Referencia', column: 6, load: { queryId: 195 }, value: "ORDEN DE PRODUCCION" },
                { type: 'select', name: 'productionOrderId', label: 'Doc. Nro', column: 6, load: { queryId: 479 }, search: true },
                {
                    type: 'select', name: 'productId', label: 'Producto', column: 4, load: { queryId: 480 }, search: true, onChange: (value, values) => {
                        let unitMeasure = getForm().getFieldApi('productId').getSelectedItem().unitMeasure;
                        getForm().setFieldData('unitMeasure', unitMeasure);
                    }
                },
                { name: 'unitMeasure', label: 'Uni. Medida', column: 4, },
                { type: 'select', name: 'supervisorId', label: 'Supervisor', column: 4, load: { queryId: 309 }, search: true },
                { type: 'number', name: 'productionQuantity', label: 'Cantidad a Producir', column: 4, },
                { type: 'date', name: 'productionDate', label: 'Fec. Producción', column: 4, format: DATE_FORMAT.DDMMYYYY, inputFormat: DATE_FORMAT.YYYYMMDD, value: DateUtil.getDate },
                { type: 'date', name: 'startTime', label: 'Hora Inicio', column: 4, format: DATE_FORMAT.HHmm, inputFormat: DATE_FORMAT.HHmmss },
                { type: 'date', name: 'endTime', label: 'Hora Termino', column: 4, format: DATE_FORMAT.HHmm, inputFormat: DATE_FORMAT.HHmmss },
                { name: 'lot', label: 'Lote', column: 4, },
                {
                    type: 'select', name: 'recipeId', label: 'Receta', column: 4, load: { queryId: 481 }, search: true,
                    parents: [
                        { name: 'productId', paramName: 'productId', required: true }
                    ]
                },
                {
                    type: 'select', name: 'lineId', label: 'Linea', column: 4, load: { queryId: 482 }, search: true,
                    parents: [
                        { name: 'productId', paramName: 'productId', required: true }
                    ]
                },
                { type: 'select', name: 'customerId', label: 'Cliente', column: 4, load: { queryId: 361 }, search: true },
                {
                    type: 'select', name: 'customerOrderId', label: 'Pedido', column: 4, load: { queryId: 483 }, search: true,
                    parents: [
                        { name: 'customerId', paramName: 'customerId', required: true }
                    ]
                },
            ],
            onComponentLoad: (formApi, pageApi) => {
                if (pageApi.getPageMode() !== PAGE_MODE.NEW)
                    formApi.load();
            }
        },
        bar: {
            fields: [
                { ...BUTTON_DEFAULT.SAVE, onClick: () => getForm().save() },
                { ...BUTTON_DEFAULT.UPDATE, onClick: () => getForm().update() },
                { ...BUTTON_DEFAULT.RETURN, link: '/production/production-record/production-record-admin' }
            ],
        },
        grid: {}
    }
}