import React from 'react'
import BarButtonFw from '../../../../framework/bar-button/BarButtonFw';
import FormFw from '../../../../framework/form/FormFw';
import { BasePage } from '../../../../framework/pages/BasePage';
import { getConfigProductionRecordDetail } from './getConfigProductionRecordDetail';
import { getModeProductionRecordDetail } from './getModeProductionRecordDetail';
import { getValidationProductionRecordDetail } from './getValidationProductionRecordDetail';
import { createRawMaterialSection } from './sections/RawMaterialSection';

export class ProductionRecordDetailPage extends BasePage {
    
    createPageProperties = (pageApi) => {
        return {
            title: 'REGISTRO DE PRODUCCIÓN',
            validation: getValidationProductionRecordDetail,
            mode: getModeProductionRecordDetail
        };
    }

    renderPage(pageApi) {

        const config = getConfigProductionRecordDetail(pageApi);

        return (
            <>
                <FormFw
                    name="form"
                    getParentApi={pageApi.getApi}
                    config={config.form}
                />

                {createRawMaterialSection(pageApi)}

                <BarButtonFw
                    name="bar"
                    getParentApi={pageApi.getApi}
                    config={config.bar}
                />
            </>
        )
    }
}