import { MODE_SHORTCUT } from '../../../../constants/constants'

export const getModeProductionRecordDetail = () => {
    return {
        new: {
            form: {
                read:['documentTypeId','unitMeasure']
            },
            bar: {
                show: ['save', 'return']
            }
        },
        update:{
            form: {
                read:['documentTypeId','unitMeasure']
            },
            bar: {
                show: ['update', 'return']
            }
        },
        read:{
            form: {
                read: [MODE_SHORTCUT.ALL]
            },
            bar: {
                show: ['return']
            }
        }
    }
}
