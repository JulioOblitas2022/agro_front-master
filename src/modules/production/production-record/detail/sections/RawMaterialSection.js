
import { DATE_FORMAT } from '../../../../../constants/constants';
import { BUTTON_DEFAULT } from '../../../../../framework/bar-button/BarButtonFw';
import GridFw, { COLUMN_DEFAULT } from '../../../../../framework/grid/GridFw';
import { RawMaterialModal } from '../modals/RawMaterialModal';

export const createRawMaterialSection = (pageApi) => {
    
    const fnDelete = (value, row, index) => {
        getGrid().deleteItem(row.key);      
    }
    
    const getGrid = () => pageApi.getComponent('ItemsSection');

    const formProduct = () => pageApi.getComponent('form');

    const itemConfig = {
        events: {
            afterAddEvent : (data) => {
                getGrid().addItem(data);
            }
        }
    }

    const ItemModalConfig = (data) =>  {
        return {
            width: 1600,
            maskClosable: false,
            component: <RawMaterialModal name={data}  getParentApi={pageApi.getApi} config={itemConfig}  />
        }
    };


    const barHeaderConfig = {
        fields:[
            {...BUTTON_DEFAULT.SEARCH, label:'Agregar Item', onClick: ()=>{  pageApi.showModal(ItemModalConfig(formProduct()?.getData())); }}
        ]
    }


    const gridConfig = {
        title: 'Items',
        collapsible: true,
        rowId: 'productionRecordDetailId',
        showIndex: true,
        pagination: true,
        autoLoad: true,
        load: {
            validation: () => pageApi.getParamId()!=null, //solo se dispara si el valor es distinto de nulo
            queryId: 500,
            orderBy: 'productionRecordDetailId desc',
            params: {productionRecordId: pageApi.getParamId()}
        },
        save: {
            queryId: 494,
            params: {productionRecordId: pageApi.getParamId()}
        },
        scroll:{
          x:1300  
        },
        fields:[
            {name: 'correlative', label: 'Nro. Doc. MP.',width: 200},
            {name: 'masterItem', label: 'Materia Prima', width: 200},
            {name: 'price', label: 'Precio',width: 200},
            {...COLUMN_DEFAULT.DELETE, onClick: fnDelete}
        ],
        barHeader: barHeaderConfig
    };

    return (
        <GridFw
            name="ItemsSection"
            getParentApi={pageApi.getApi}
            config={gridConfig} 
        />
    )
}

