import React from 'react';
import FormGridFw, { FORM_GRID_MODE } from '../../../../../framework/form-grid/FormGridFw';
import { SELECTION_TYPE } from '../../../../../framework/grid/GridFw';
import { BasePage } from '../../../../../framework/pages/BasePage';

export class RawMaterialModal extends BasePage {
    
    
    createPageProperties = (pageApi) => {
        return {
            title: 'Busqueda de Materia Prima',
        };
    }

    renderPage(pageApi) {
       

        const formGridConfig = {
            mode: FORM_GRID_MODE.FORM_SEARCH_ADD,
            afterAddEvent: (data) => {
                pageApi.getEvent('afterAddEvent')(data);
                pageApi.hideThisModal();
            },
            form: {
                fields:[
                    {name:'correlative', label:'Código', column: 6},
                ]
            },
            grid: {
                title: 'Documentos seleccionados',
                rowId: 'codigo',
                showIndex: false,
                autoLoad: true,
                pagination: false,
                selection: {type: SELECTION_TYPE.MULTIPLE},
                load: {
                    queryId: 484,
                    orderBy: "1"
                },
                fields:[
                    {name:'correlative', label:'Código', width: 100},
                    {name:'masterItem', label:'Descripción', width: 100},
                    {name:'price', label:'Precio',  width: 100},
                ]
            }
        };  
    
        return (
            <>
                <FormGridFw 
                    name="itemsGrid" 
                    getParentApi={pageApi.getApi}
                    config={formGridConfig}
                />
            </>
        )

    }
    
}