import { DATE_FORMAT } from "../../../../constants/constants";
import { BUTTON_DEFAULT } from "../../../../framework/bar-button/BarButtonFw";
import { COLUMN_DEFAULT } from "../../../../framework/grid/GridFw";
import { getFormDefaultParams } from "../../../shared/util/getFormDefaultParams";
import { getGridServerDefaultParams } from "../../../shared/util/getGridServerDefaultParams";

export const getConfigProductionRecordAdmin = (pageApi) => {
    const getGrid = () => pageApi.getComponent('grid');
    const getForm = () => pageApi.getComponent('form');

    return {
        form: {
            ...getFormDefaultParams(),
            title: 'Filtros de búsqueda',
            fields: [
                { name: 'productionRecordNumber', label: 'Nro. Prod.', column: 4 },
                { type: 'date', name: 'dateFrom', label: 'Desde', column: 4, format: 'DD/MM/YYYY', inputFormat: DATE_FORMAT.YYYYMMDD },
                { type: 'date', name: 'dateUntil', label: 'Hasta', column: 4, format: 'DD/MM/YYYY', inputFormat: DATE_FORMAT.YYYYMMDD },
            ]
        },
        bar: {
            fields: [
                { ...BUTTON_DEFAULT.SEARCH, onClick: () => getGrid().load({ params: getForm().getData() }) },
                { ...BUTTON_DEFAULT.CLEAR, onClick: () => getForm().reset() }
            ]
        },
        grid: {
            ...getGridServerDefaultParams({ queryLoad: 490 }),
            title: 'Resultados',
            fields: [
                { name: 'productionRecordNumber', label: 'Nro. Prod.', width: 100 },
                { name: 'product', label: 'Producto', width: 100 },
                { name: 'productionDate', label: 'Fec. Prod.', width: 100 },
                { name: 'productionQuantity', label: 'Cant. Producida', width: 100 },
                { name: 'lot', label: 'Lote', width: 100 },
                { name: 'recipe', label: 'Receta', width: 100 },
                { name: 'line', label: 'Linea', width: 100 },
                { ...COLUMN_DEFAULT.VIEW, link: (value, row, index) => `/production/production-record/production-record/${row.productionRecordId}` },
                { ...COLUMN_DEFAULT.EDIT, link: (value, row, index) => `/production/production-record/production-record/${row.productionRecordId}/update` },
            ]
        }
    }
}