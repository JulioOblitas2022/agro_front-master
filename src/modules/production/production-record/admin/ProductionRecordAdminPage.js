import React from 'react'
import BarButtonFw, { BUTTON_DEFAULT } from '../../../../framework/bar-button/BarButtonFw';
import FormFw from '../../../../framework/form/FormFw';
import GridFw from '../../../../framework/grid/GridFw';
import { BasePage } from '../../../../framework/pages/BasePage';
import { getConfigProductionRecordAdmin } from './getConfigProductionRecordAdmin';

export class ProductionRecordAdminPage extends BasePage {

    createPagePropertiesBase(pageApi) {
        return {
            title: 'BANDEJA DE REGISTRO DE PRODUCCIÓN',
            barHeader: {
                fields:[
                    {...BUTTON_DEFAULT.NEW, label:'Nuevo', link: '/production/production-record/production-record/new'}
                ]
            }
        };
    }

    renderPage(pageApi) {

        const config = getConfigProductionRecordAdmin(pageApi);

        return (
            <>
                <FormFw
                    name="form"
                    getParentApi={pageApi.getApi}
                    config={config.form}
                />
                <BarButtonFw
                    name="bar"
                    getParentApi={pageApi.getApi}
                    config={config.bar}
                />
                <GridFw
                    name="grid"
                    getParentApi={pageApi.getApi}
                    config={config.grid}
                />
            </>
        )
    }
}