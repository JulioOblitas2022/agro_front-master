import React from 'react'
import { Route, Switch, useRouteMatch } from 'react-router';
import { ProductionRecordAdminPage } from './admin/ProductionRecordAdminPage';
import { ProductionRecordDetailPage } from './detail/ProductionRecordDetailPage';

const ProductionRecordRoutes = () => {
    const { path } = useRouteMatch();
    return (
        <Switch>
            <Route exact path={`${path}/production-record-admin`} component={ProductionRecordAdminPage} />
            <Route path={`${path}/production-record`} component={ProductionRecordDetailPage} />
            <Route path={`${path}/production-record/:id`} component={ProductionRecordDetailPage} /> 
        </Switch>
    )
}

export default ProductionRecordRoutes;