import React from 'react'
import BarButtonFw from '../../../../framework/bar-button/BarButtonFw';
import FormFw from '../../../../framework/form/FormFw';
import { BasePage } from '../../../../framework/pages/BasePage';
import { getConfigRequestOrderDetail } from './getConfigRequestOrderDetail';
import { getModeRequestOrderDetail } from './getModeRequestOrderDetail';
import { getValidationRequestOrderDetail } from './getValidationRequestOrderDetail';
import { createRequestOrderSection } from './sections/RequestOrderSection';

export class RequestOrderDetailPage extends BasePage {

    createPageProperties = (pageApi) => {
        return {
            title: 'REGISTRO DE ORDEN DE REQUERIMIENTO',
            validation: getValidationRequestOrderDetail,
            mode: getModeRequestOrderDetail
        };
    }

    renderPage(pageApi) {

        const config = getConfigRequestOrderDetail(pageApi);

        return (
            <>
                <FormFw
                    name="form"
                    getParentApi={pageApi.getApi}
                    config={config.form}
                />
                {createRequestOrderSection(pageApi)}
                <BarButtonFw
                    name="bar"
                    getParentApi={pageApi.getApi}
                    config={config.bar}
                />
            </>
        )
    }
}
