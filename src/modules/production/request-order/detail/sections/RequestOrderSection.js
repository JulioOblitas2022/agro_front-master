import React from 'react';
import { BUTTON_DEFAULT } from '../../../../../framework/bar-button/BarButtonFw';
import FormGridFw, { FORM_GRID_MODE } from '../../../../../framework/form-grid/FormGridFw';
import GridFw, { COLUMN_DEFAULT } from '../../../../../framework/grid/GridFw';
import { ProductModal } from '../modals/ProductModal';

export const createRequestOrderSection = (pageApi) => {
     
    const getGrid = () => pageApi.getComponent('requestOrderGrid');

    const fnDelete = (value, row, index) => {
        getGrid().deleteItem(row.key);      
    }

    const itemConfig = {
        events: {
            afterAddEvent : (data) => {
                getGrid().addItem(data);
            }
        }
    }

    const ItemModalConfig = (data) =>  {
        return {
            width: 1600,
            maskClosable: false,
            component: <ProductModal name={data}  getParentApi={pageApi.getApi} config={itemConfig}  />
        }
    };


    const barHeaderConfig = {
        fields:[
            {...BUTTON_DEFAULT.ADD, label:'Agregar Item', onClick: ()=>{  pageApi.showModal(ItemModalConfig('')); }}
        ]
    }

    const gridConfig = {
        title: 'Otros items registrados',
        rowId: 'requestOrderDetailId',
        showIndex: true,
        pagination: false,
        autoLoad: true,
        load: {
            validation: () => pageApi.getParamId() != null, //solo se dispara si el valor es distinto de nulo
            queryId: 226,
            orderBy: 'requestOrderDetailId desc',
            params: { requestOrderId: pageApi.getParamId() },
            hideMessage: true, 
            hideMessageError: true
        },
        save: {
            queryId: 225,
            params: { requestOrderId: pageApi.getParamId() },
            hideMessage: true, 
            hideMessageError: true
        },
        fields: [
            { name: 'itemTypeText', label: 'Tipo Item' },
            { name: 'description', label: 'Item' },
            { type: 'select', name: 'unitMeasureId', label: 'Uni. Med.', labelName: 'unitMeasureDes', load: { queryId: 568, params: () => {return {masterItemId: getGrid()?.itemEditing?.masterItemId}}}, search: true, editable: true },
            { type: 'number', name: 'quantity', label: 'Cantidad', editable: true },
            {...COLUMN_DEFAULT.DELETE, onClick: fnDelete, label: 'Opciones'}
        ],
        barHeader: barHeaderConfig
    };

    return (
        <GridFw
            name="requestOrderGrid"
            getParentApi={pageApi.getApi}
            config={gridConfig}
        />
    )
}