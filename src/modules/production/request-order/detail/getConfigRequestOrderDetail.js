import { DATE_FORMAT, PAGE_MODE } from '../../../../constants/constants';
import { BUTTON_DEFAULT } from '../../../../framework/bar-button/BarButtonFw';
import { DateUtil } from '../../../../util/DateUtil';
import { UserUtil } from '../../../../util/UserUtil';

export const getConfigRequestOrderDetail = (pageApi) => {
    const getForm = () => pageApi.getComponent('form');
    const getRequestOrderGrid = () => pageApi.getComponent('requestOrderGrid');

    return {
        form: {
            autoLoad: true,
            load: {
                queryId: 227,
                params: { requestOrderId: pageApi.getParamId() || 0 },
                fnOk: (resp) => {
                    getRequestOrderGrid().load({ params: {requestOrderId: pageApi.getParamId()} });
                }
            },
            save: {
                queryId: 224,
                postLink: (resp, values) => '/production/request-order/request-order-admin',
                params: { createdBy: UserUtil.getUserId() },
                fnOk: (resp) => {
                    const requestOrderId = resp.dataObject?.requestOrderId;
                    if (requestOrderId) {
                        getRequestOrderGrid().save({ params: { requestOrderId } });
                    }

                }
            },
            update: {
                queryId: 522,
                postLink: (resp, values) => '/production/request-order/request-order-admin',
                params: { modifiedBy: UserUtil.getUserId() },
                fnOk: (resp) => {
                    getRequestOrderGrid().save({ params: { requestOrderId: resp.dataObject?.requestOrderId } });
                }
            },
            title: 'Datos Principales',
            fields: [
                { type: 'select', name: 'companyId', label: 'Empresa', column: 4, load: { queryId: 554, } },
                { name: 'requestOrderNumber', label: 'Nro. Documento', column: 4, readOnly: true},
                { type: 'date', name: 'broadcastDate', label: 'Fecha Emisión', format: DATE_FORMAT.DDMMYYYY, inputFormat: DATE_FORMAT.YYYYMMDD, column: 4, value: DateUtil.getDate },
                { type: 'select', name: 'localId', label: 'Local', column: 4, load: { queryId: 198 },
                    parents: [{paramName: 'companyId', name: 'companyId', required: true}]
                },
                { type: 'select', name: 'applicantId', label: 'Solicitante', column: 4, load: { queryId: 309 } },
                { type: 'select', name: 'areaId', label: 'Area', column: 4, load: { queryId: 186 }, search: true },
                { type: 'select', name: 'priorityId', label: 'Prioridad', column: 4, load: { queryId: 252 } },
                { type: 'select', name: 'reasonId', label: 'Motivo', column: 4, load: { queryId: 189 }, search: true },
                { type: 'date', name: 'deliverDate', label: 'Fecha Entrega', format: DATE_FORMAT.DDMMYYYY, inputFormat: DATE_FORMAT.YYYYMMDD, column: 4, value: DateUtil.getDate },
                {
                    type: 'radio', name: 'active', label: 'Estado', column: 4, value: 1, disabled: true,
                    load: {
                        dataList: [
                            { label: 'PENDIENTE', value: 1 },
                            { label: 'TERMINADO', value: 2 },
                        ]
                    }
                },
                { type: 'textarea', name: 'observations', label: 'Observaciones', column: 9 },

            ],
            onComponentLoad: (formApi, pageApi) => {
                if (pageApi.getPageMode() !== PAGE_MODE.NEW)
                    formApi.load();
            }
        },
        bar: {
            fields: [
                { ...BUTTON_DEFAULT.SAVE, onClick: () => getForm().save() },
                { ...BUTTON_DEFAULT.UPDATE, onClick: () => getForm().update() },
                { ...BUTTON_DEFAULT.RETURN, link: '/production/request-order/request-order-admin' }
            ],
        },
        grid: {}
    }
}