import React from 'react'
import { Route, Switch, useRouteMatch } from 'react-router';
import { YieldsAdminPage } from './admin/YieldsAdminPage';
import { YieldsDetailPage } from './detail/YieldsDetailPage';

const YieldsRoutes = () => {
    const { path } = useRouteMatch();
    return (
        <Switch>
            <Route exact path={`${path}/yields-admin`} component={YieldsAdminPage} />
            <Route path={`${path}/yields`} component={YieldsDetailPage} />
            <Route path={`${path}/yields/:id`} component={YieldsDetailPage} /> 
        </Switch>
    )
}

export default YieldsRoutes;