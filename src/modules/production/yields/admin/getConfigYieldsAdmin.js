import { BUTTON_DEFAULT } from "../../../../framework/bar-button/BarButtonFw";
import { COLUMN_DEFAULT } from "../../../../framework/grid/GridFw";
import { getFormDefaultParams } from "../../../shared/util/getFormDefaultParams";
import { getGridServerDefaultParams } from "../../../shared/util/getGridServerDefaultParams";

export const getConfigYieldsAdmin = (pageApi) => {
    const getGrid = () => pageApi.getComponent('grid');
    const getForm = () => pageApi.getComponent('form');
    
    return {
        form: {
            ...getFormDefaultParams(),
            title: 'Filtros de búsqueda',
            fields:[
                {name:'code', label:'Cod. Mat. Prima', column: 6},
                {type: 'select', name:'masterItemId', label:'Mat. Prima', column: 6, load: {queryId:375}, search: true},
            ]
        },
        bar: { fields: [
            {...BUTTON_DEFAULT.SEARCH, onClick: ()=>getGrid().load({params:getForm().getData()})},
            {...BUTTON_DEFAULT.CLEAR, onClick: ()=>getForm().reset()}
        ]},
        grid: {
            ...getGridServerDefaultParams({queryLoad: 385}),
            title: 'Resultados',
            fields:[
                {name: 'code', label: 'Cod. Mat. Prima',},
                {name: 'masterItem', label: 'Mat. Prima',},
                {name: 'codeProduct', label: 'Cod. Producto',},
                {name: 'product', label: 'Producto',},
                {name: 'percentage', label: 'Rendimiento',},
                {...COLUMN_DEFAULT.VIEW, link: (value, row, index) => `/production/yields/yields/${row.yieldId}` },
                {...COLUMN_DEFAULT.EDIT, link: (value, row, index) => `/production/yields/yields/${row.yieldId}/update` },
                {...COLUMN_DEFAULT.DELETE, process: {fnOk: (result)=>getGrid().load({params:getForm().getData()}),filter: (value, row, index) => ({queryId: 382, params: {yieldId: row.yieldId}}) }}
            ]
        }
    }
}