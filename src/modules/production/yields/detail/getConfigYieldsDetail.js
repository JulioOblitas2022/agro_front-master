import { PAGE_MODE } from '../../../../constants/constants';
import { BUTTON_DEFAULT } from '../../../../framework/bar-button/BarButtonFw';
import { UserUtil } from '../../../../util/UserUtil';

export const getConfigYieldsDetail = (pageApi) => {
    const getForm = () => pageApi.getComponent('form');

    return {
        form: {
            load:{
                queryId: 384,
                params: {yieldId: pageApi.getParamId()},
            },
            update:{
                queryId: 383,
                postLink: (resp, values) => '/production/yields/yields-admin',
                params: {modifiedBy: UserUtil.getUserId()}
            },
            save:{
                queryId: 386,
                postLink: (resp, values) => '/production/yields/yields-admin',
                params: {createdBy: UserUtil.getUserId()}
            },
            title: 'DATOS PRINCIPALES',
            fields:[
                {type: 'select', name:'masterItemId', label:'Mat. Prima', column: 4, load: {queryId: 375}, search: true},
                {type: 'select', name:'productId', label:'Producto', column: 4, allowClear: true, load: {queryId: 381}, search: true,
                    parents: [
                        {name:'masterItemId', paramName: 'masterItemId', required: true}
                    ]
                },
                {type: 'number', name:'percentage', label:'Rendimiento %', column: 4,},
            ],
            onComponentLoad : (formApi, pageApi) =>{
                if (pageApi.getPageMode() !== PAGE_MODE.NEW)
                    formApi.load();
            }
        },
        bar: {
            fields: [
                {...BUTTON_DEFAULT.SAVE, onClick: () => getForm().save()},
                {...BUTTON_DEFAULT.UPDATE, onClick: () => getForm().update()},
                {...BUTTON_DEFAULT.RETURN, link: '/production/yields/yields-admin'}
            ],
        },
        grid: {}
    }
}