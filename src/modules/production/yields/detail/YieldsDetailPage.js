import React from 'react'
import BarButtonFw from '../../../../framework/bar-button/BarButtonFw';
import FormFw from '../../../../framework/form/FormFw';
import { BasePage } from '../../../../framework/pages/BasePage';
import { getConfigYieldsDetail } from './getConfigYieldsDetail';
import { getModeYieldsDetail } from './getModeYieldsDetail';
import { getValidationYieldsDetail } from './getValidationYieldsDetail';

export class YieldsDetailPage extends BasePage {
    
    createPageProperties = (pageApi) => {
        return {
            title: 'REGISTRO DE RENDIMIENTO',
            validation: getValidationYieldsDetail,
            mode: getModeYieldsDetail
        };
    }

    renderPage(pageApi) {

        const config = getConfigYieldsDetail(pageApi);

        return (
            <>
                <FormFw
                    name="form"
                    getParentApi={pageApi.getApi}
                    config={config.form}
                />
                <BarButtonFw
                    name="bar"
                    getParentApi={pageApi.getApi}
                    config={config.bar}
                />
            </>
        )
    }
}