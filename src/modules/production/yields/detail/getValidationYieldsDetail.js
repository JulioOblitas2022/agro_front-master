
export const getValidationYieldsDetail = () => {
    return {
        form: {
            masterItemId: [ {required: true, message: 'Mat. Prima es requerida' } ],
            productId: [ {required: true, message: 'Producto es requerido' } ],
            percentage: [ {required: true, message: 'Procentaje % es requerido' } ],
        },
        grid: {

        },
        button: {

        },
        bar: {

        }
    }
}
