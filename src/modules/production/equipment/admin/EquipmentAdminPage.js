import React from 'react'
import BarButtonFw, { BUTTON_DEFAULT } from '../../../../framework/bar-button/BarButtonFw';
import FormFw from '../../../../framework/form/FormFw';
import GridFw from '../../../../framework/grid/GridFw';
import { BasePage } from '../../../../framework/pages/BasePage';
import { DATE_FORMAT } from "../../../../constants/constants";
import { COLUMN_DEFAULT } from "../../../../framework/grid/GridFw";
import { getFormDefaultParams } from "../../../shared/util/getFormDefaultParams";
import { getGridServerDefaultParams } from "../../../shared/util/getGridServerDefaultParams";

export class EquipmentAdminPage extends BasePage {

    createPagePropertiesBase(pageApi) {
        return {
            title: 'BANDEJA DE EQUIPOS',
            barHeader: {
                fields:[
                    {...BUTTON_DEFAULT.NEW, label:'Nuevo', link: '/production/equipment/equipment/new'}
                ]
            }
        };
    }

    renderPage(pageApi) {

        const getGrid = () => pageApi.getComponent('grid');
        const getForm = () => pageApi.getComponent('form');

        const form = {
            ...getFormDefaultParams(),
            title: 'Filtros de búsqueda',
            fields:[
                {name:'equipmentId', label:'Id. Registro', column: 6},
                {name:'description', label:'Descripcion', column: 6},
                {type: 'date', name:'dateFrom', label:'Desde', column: 4, format: 'DD/MM/YYYY', inputFormat: DATE_FORMAT.YYYYMMDD},
                {type: 'date', name:'dateUntil', label:'Hasta', column: 4, format: 'DD/MM/YYYY', inputFormat: DATE_FORMAT.YYYYMMDD},
                {type: 'switch', name:'active', label:'Estado', column: 4, value: true},
            ]
        };

        const bar = { fields: [
            {...BUTTON_DEFAULT.SEARCH, onClick: ()=>getGrid().load({params:getForm().getData()})},
            {...BUTTON_DEFAULT.CLEAR, onClick: ()=>getForm().reset()}
        ]};

        const grid = {
            ...getGridServerDefaultParams({queryLoad: 389}),
            title: 'Resultados',
            fields:[
                {name: 'equipmentId', label: 'Id. Registro',},
                {name: 'description', label: 'Descripcion',},
                {...COLUMN_DEFAULT.VIEW, link: (value, row, index) => `/production/equipment/equipment/${row.equipmentId}` },
                {...COLUMN_DEFAULT.EDIT, link: (value, row, index) => `/production/equipment/equipment/${row.equipmentId}/update` },
                {...COLUMN_DEFAULT.DELETE, process: {fnOk: (result)=>getGrid().load({params:getForm().getData()}),filter: (value, row, index) => ({queryId: 391, params: {equipmentId: row.equipmentId}}) }}
            ]
        }

        return (
            <>
                <FormFw
                    name="form"
                    getParentApi={pageApi.getApi}
                    config={form}
                />
                <BarButtonFw
                    name="bar"
                    getParentApi={pageApi.getApi}
                    config={bar}
                />
                <GridFw
                    name="grid"
                    getParentApi={pageApi.getApi}
                    config={grid}
                />
            </>
        )
    }
}