import { PAGE_MODE } from "../../../../constants/constants";
import { BUTTON_DEFAULT } from "../../../../framework/bar-button/BarButtonFw";
import { UserUtil } from "../../../../util/UserUtil";

export const getConfigEquipmentDetail = (pageApi) => {
    const getForm = () => pageApi.getComponent('form');

    return {
        form: {
            load:{
                queryId: 388,
                params: {equipmentId: pageApi.getParamId()},
            },
            update:{
                queryId: 390,
                postLink: (resp, values) => '/production/equipment/equipment-admin',
                params: {modifiedBy: UserUtil.getUserId()}
            },
            save:{
                queryId: 387,
                postLink: (resp, values) => '/production/equipment/equipment-admin',
                params: {createdBy: UserUtil.getUserId()}
            },
            title: 'DATOS PRINCIPALES',
            fields:[
                {name:'equipmentId', label:'Id. Registro', column: 6,},
                {name:'description', label:'Descripcion', column: 6,},
            ],
            onComponentLoad : (formApi, pageApi) =>{
                if (pageApi.getPageMode() !== PAGE_MODE.NEW)
                    formApi.load();
            }
        },
        bar: {
            fields: [
                {...BUTTON_DEFAULT.SAVE, onClick: () => getForm().save()},
                {...BUTTON_DEFAULT.UPDATE, onClick: () => getForm().update()},
                {...BUTTON_DEFAULT.RETURN, link: '/production/equipment/equipment-admin'}
            ],
        },
        grid: {}
    }
}