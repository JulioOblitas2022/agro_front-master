import React from 'react'
import BarButtonFw from '../../../../framework/bar-button/BarButtonFw';
import FormFw from '../../../../framework/form/FormFw';
import { BasePage } from '../../../../framework/pages/BasePage';
import { getConfigEquipmentDetail } from './getConfigEquipmentDetail';
import { getModeEquipmentDetail } from './getModeEquipmentDetail';
import { getValidationEquipmentDetail } from './getValidationEquipmentDetail';

export class EquipmentDetailPage extends BasePage {
    
    createPageProperties = (pageApi) => {
        return {
            title: 'REGISTRO DE EQUIPOS',
            validation: getValidationEquipmentDetail,
            mode: getModeEquipmentDetail
        };
    }

    renderPage(pageApi) {

        const config = getConfigEquipmentDetail(pageApi);

        return (
            <>
                <FormFw
                    name="form"
                    getParentApi={pageApi.getApi}
                    config={config.form}
                />
                <BarButtonFw
                    name="bar"
                    getParentApi={pageApi.getApi}
                    config={config.bar}
                />
            </>
        )
    }
}