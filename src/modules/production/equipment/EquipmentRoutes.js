import React from 'react'
import { Route, Switch, useRouteMatch } from 'react-router';
import { EquipmentAdminPage } from './admin/EquipmentAdminPage';
import { EquipmentDetailPage } from './detail/EquipmentDetailPage';

const EquipmentRoutes = () => {
    const { path } = useRouteMatch();
    return (
        <Switch>
            <Route exact path={`${path}/equipment-admin`} component={EquipmentAdminPage} />
            <Route path={`${path}/equipment`} component={EquipmentDetailPage} />
            <Route path={`${path}/equipment/:id`} component={EquipmentDetailPage} /> 
        </Switch>
    )
}

export default EquipmentRoutes;