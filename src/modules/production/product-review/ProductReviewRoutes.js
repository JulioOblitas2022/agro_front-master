import React from 'react'
import { Route, Switch, useRouteMatch } from 'react-router';
import { ProductReviewAdminPage } from './admin/ProductReviewAdminPage';
import { ProductReviewDetailPage } from './detail/ProductReviewDetailPage';

const ProductReviewRoutes = () => {
    const { path } = useRouteMatch();
    return (
        <Switch>
            <Route exact path={`${path}/product-review-admin`} component={ProductReviewAdminPage} />
            <Route path={`${path}/product-review`} component={ProductReviewDetailPage} />
            <Route path={`${path}/product-review/:id`} component={ProductReviewDetailPage} /> 
        </Switch>
    )
}

export default ProductReviewRoutes;