import { MODE_SHORTCUT } from '../../../../constants/constants'

export const getModeProductReviewDetail = () => {
    return {
        new: {
            form: {
                read: ['documentTypeId','productionQuantity','productionDate','product','unitMeasure','recipe','productionOrderNumber','productionOrderDate','startDate','endDate','startTime','endTime','lot']
            },
            bar: {
                show: ['save', 'return']
            }
        },
        update:{
            form: {
                read: ['documentTypeId','productionQuantity','productionDate','product','unitMeasure','recipe','productionOrderNumber','productionOrderDate','startDate','endDate','startTime','endTime','lot']
            },
            bar: {
                show: ['update', 'return']
            }
        },
        read:{
            form: {
                read: [MODE_SHORTCUT.ALL]
            },
            bar: {
                show: ['return']
            }
        }
    }
}
