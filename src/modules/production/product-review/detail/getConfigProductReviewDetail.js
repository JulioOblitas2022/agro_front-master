import { DATE_FORMAT, PAGE_MODE } from "../../../../constants/constants";
import { BUTTON_DEFAULT } from "../../../../framework/bar-button/BarButtonFw";
import { UserUtil } from "../../../../util/UserUtil";

export const getConfigProductReviewDetail = (pageApi) => {
    const getForm = () => pageApi.getComponent('form');

    return {
        form: {
            load:{
                queryId: 512,
                params: {productReviewId: pageApi.getParamId()},
            },
            update:{
                queryId: 511,
                postLink: (resp, values) => '/production/product-review/product-review-admin',
                params: {modifiedBy: UserUtil.getUserId()},
            },
            save:{
                queryId: 510,
                postLink: (resp, values) => '/production/product-review/product-review-admin',
                params: {createdBy: UserUtil.getUserId()},
            },
            //title: 'DATOS DE LA SOLICITUD',
            fields:[
                {type: 'divider', label: 'DATOS DE LA PRODUCCION'},
                {type: 'select', name:'documentTypeId', label:'Tipo Documento', column: 4, load: {queryId:195}, value: 53},
                {type: 'select', name:'productionRecordId', label:'Nro. Producción', column: 6, load: {queryId:501}, search: true,
                    onChange: (value, values) => {
                        let productionQuantity = getForm().getFieldApi('productionRecordId').getSelectedItem()?.productionQuantity;
                        let productionDate = getForm().getFieldApi('productionRecordId').getSelectedItem()?.productionDate;
                        let product = getForm().getFieldApi('productionRecordId').getSelectedItem()?.product;
                        let unitMeasure = getForm().getFieldApi('productionRecordId').getSelectedItem()?.unitMeasure;
                        let recipe = getForm().getFieldApi('productionRecordId').getSelectedItem()?.recipe;
                        let startDate = getForm().getFieldApi('productionRecordId').getSelectedItem()?.startDate;
                        let endDate = getForm().getFieldApi('productionRecordId').getSelectedItem()?.endDate;
                        let startTime = getForm().getFieldApi('productionRecordId').getSelectedItem()?.startTime;
                        let endTime = getForm().getFieldApi('productionRecordId').getSelectedItem()?.endTime;
                        let lot = getForm().getFieldApi('productionRecordId').getSelectedItem()?.lot;

                        getForm()?.setFieldData('productionQuantity', productionQuantity); 
                        getForm()?.setFieldData('productionDate', productionDate); 
                        getForm()?.setFieldData('product', product);
                        getForm()?.setFieldData('unitMeasure', unitMeasure); 
                        getForm()?.setFieldData('recipe', recipe); 
                        getForm()?.setFieldData('startDate', startDate); 
                        getForm()?.setFieldData('endDate', endDate); 
                        getForm()?.setFieldData('startTime', startTime); 
                        getForm()?.setFieldData('endTime', endTime); 
                        getForm()?.setFieldData('lot', lot); 
                    }
                },
                {name: 'product', label:'Producto', column: 4,},
                {name: 'recipe', label:'Receta', column: 4,},
                {name: 'lot', label:'Lote', column: 4,},
                {name: 'unitMeasure', label:'Uni. Medida', column: 4,},
                {name:'productionQuantity', label:'Cantidad a Producir', column: 4,},
                {type: 'date', name:'startDate', label:'Fec. Inicio', column: 4, format: DATE_FORMAT.DDMMYYYY, inputFormat: DATE_FORMAT.YYYYMMDD},
                {type: 'date', name:'endDate', label:'Fec. Termino', column: 4, format: DATE_FORMAT.DDMMYYYY, inputFormat: DATE_FORMAT.YYYYMMDD},
                {type: 'date',name:'startTime', label:'Hora Inicio', column: 4, format: DATE_FORMAT.HHmm, inputFormat: DATE_FORMAT.HHmmss},
                {type: 'date',name:'endTime', label:'Hora Termino', column: 4, format: DATE_FORMAT.HHmm, inputFormat: DATE_FORMAT.HHmmss},
                {type: 'divider', label: 'DATOS DE LA REVISIÓN'},
                {name: 'conformingQuantity', label:'Cant. Prod. Conforme', column: 4,},
                {name: 'observationConforming', column: 8,},
                {name: 'nonConformingQuantity', label:'Cant. Prod. No Conforme', column: 4,},
                {name: 'observationNonConforming',  column: 8,},
                {type: 'select', name: 'responsableId', label:'Responsable', column: 4, load: {queryId: 309}, search: true},
                {type: 'date', name: 'reviewDate', label:'Fec. Revisión', column: 4, format: DATE_FORMAT.DDMMYYYY, inputFormat: DATE_FORMAT.YYYYMMDD},
                {type: 'date', name: 'reviewTime', label:'Hora Revisión', column: 4, format: DATE_FORMAT.HHmm, inputFormat: DATE_FORMAT.HHmmss},
            ],
            onComponentLoad : (formApi, pageApi) =>{
                if (pageApi.getPageMode() !== PAGE_MODE.NEW)
                    formApi.load();
            }
        },
        bar: {
            fields: [
                {...BUTTON_DEFAULT.SAVE, onClick: () => getForm().save()},
                {...BUTTON_DEFAULT.UPDATE, onClick: () => getForm().update()},
                {...BUTTON_DEFAULT.RETURN, link: '/production/product-review/product-review-admin'}
            ],
        },
        grid: {}
    }
}