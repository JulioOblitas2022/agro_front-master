import React from 'react'
import BarButtonFw from '../../../../framework/bar-button/BarButtonFw';
import FormFw from '../../../../framework/form/FormFw';
import { BasePage } from '../../../../framework/pages/BasePage';
import { getConfigProductReviewDetail } from './getConfigProductReviewDetail';
import { getModeProductReviewDetail } from './getModeProductReviewDetail';
import { getValidationProductReviewDetail } from './getValidationProductReviewDetail';

export class ProductReviewDetailPage extends BasePage {
    
    createPageProperties = (pageApi) => {
        return {
            title: 'REGISTRO DE REVISIÓN DE PRODUCTOS',
            validation: getValidationProductReviewDetail,
            mode: getModeProductReviewDetail
        };
    }

    renderPage(pageApi) {

        const config = getConfigProductReviewDetail(pageApi);

        return (
            <>
                <FormFw
                    name="form"
                    getParentApi={pageApi.getApi}
                    config={config.form}
                />

                <BarButtonFw
                    name="bar"
                    getParentApi={pageApi.getApi}
                    config={config.bar}
                />
            </>
        )
    }
}