import React from 'react'
import BarButtonFw, { BUTTON_DEFAULT } from '../../../../framework/bar-button/BarButtonFw';
import FormFw from '../../../../framework/form/FormFw';
import GridFw from '../../../../framework/grid/GridFw';
import { BasePage } from '../../../../framework/pages/BasePage';
import { getConfigProductReviewAdmin } from './getConfigProductReviewAdmin';

export class ProductReviewAdminPage extends BasePage {

    createPagePropertiesBase(pageApi) {
        return {
            title: 'BANDEJA DE REVISIÓN DE PRODUCTOS',
            barHeader: {
                fields:[
                    {...BUTTON_DEFAULT.NEW, label:'Nuevo', link: '/production/product-review/product-review/new'}
                ]
            }
        };
    }

    renderPage(pageApi) {

        const config = getConfigProductReviewAdmin(pageApi);

        return (
            <>
                <FormFw
                    name="form"
                    getParentApi={pageApi.getApi}
                    config={config.form}
                />
                <BarButtonFw
                    name="bar"
                    getParentApi={pageApi.getApi}
                    config={config.bar}
                />
                <GridFw
                    name="grid"
                    getParentApi={pageApi.getApi}
                    config={config.grid}
                />
            </>
        )
    }
}