import React from 'react';
import BarButtonFw, { BUTTON_DEFAULT } from '../../../../../framework/bar-button/BarButtonFw';
import FormGridFw, { FORM_GRID_MODE } from '../../../../../framework/form-grid/FormGridFw';
import FormFw from '../../../../../framework/form/FormFw';
import GridFw, { SELECTION_TYPE } from '../../../../../framework/grid/GridFw';
import { BasePage } from '../../../../../framework/pages/BasePage';

export class SuppliesModal extends BasePage {
    
    
    createPageProperties = (pageApi) => {
        return {
            title: 'INSUMOS DE RECETA',
        };
    }

    renderPage(pageApi) {
       
        const SuppliesModalConfig = (data) =>  {
            return {
                width: 1600,
                maskClosable: false,
                component: <SuppliesModal name={data}  getParentApi={pageApi.getApi}  />
            }
        };

        const gridConfig = {
                title: '',
                rowId: 'codigo',
                showIndex: false,
                autoLoad: true,
                pagination: false,
                load: {
                    queryId: 564,
                    orderBy: "1",
                    params: {
                        productId: pageApi.name.productId,
                        productRecipeDetailId: pageApi.name.productRecipeDetailId
                    },
                },
                fields:[
                    {name:'existenceType', label:'Tipo de Existencia'},
                    {name:'code', label:'Código'},
                    {name:'description', label:'Descripción'},
                    {name:'unitMeasure', label:'Unidad de Medida'},
                    {name:'quantity', label:'Cantidad Teórica'},
                ]
        };  
    
        return (
            <>
                <GridFw 
                    name="grid" 
                    getParentApi={pageApi.getApi}
                    config={gridConfig}
                />
            </>
        )

    }
    
}