import React from 'react';
import BarButtonFw, { BUTTON_DEFAULT } from '../../../../../framework/bar-button/BarButtonFw';
import FormGridFw, { FORM_GRID_MODE } from '../../../../../framework/form-grid/FormGridFw';
import FormFw from '../../../../../framework/form/FormFw';
import GridFw, { SELECTION_TYPE } from '../../../../../framework/grid/GridFw';
import { BasePage } from '../../../../../framework/pages/BasePage';

export class LineModal extends BasePage {
    
    
    createPageProperties = (pageApi) => {
        return {
            title: 'INSUMOS DE RECETA',
        };
    }

    renderPage(pageApi) {
       
        

        const gridConfig = {
                title: '',
                rowId: 'codigo',
                showIndex: false,
                autoLoad: true,
                pagination: false,
                load: {
                    queryId: 565,
                    orderBy: "1",
                    params: {
                        productId: pageApi.name.productId,
                        productRecipeLineDetailId: pageApi.name.productRecipeLineDetailId
                    },
                },
                fields:[
                    {name:'task', label:'Tarea'},
                    {name:'taskPercentage', label:'Porcentaje'},
                    {name:'quantityProcess', label:'Cantidad KLG'},
                    {name:'quantityPerson', label:'Cantidad Personas'},
                    {name:'equipment', label:'Equipo'},
                    {name:'quantityEquipment', label:'Cantidad Equipo'},
                    {name:'numberPersonEquipment', label:'Cantidad Equipo Persona'},
                    {name:'quantityKiloPerson', label:'Kilo x Persona x Hora'},
                    {name:'quantityKiloTime', label:'Linea x Hora'},
                    {name:'quantityKiloLine', label:'Linea x Tiempo'},
                ]
        };  
    
        return (
            <>
                <GridFw 
                    name="grid" 
                    getParentApi={pageApi.getApi}
                    config={gridConfig}
                />
            </>
        )

    }
    
}