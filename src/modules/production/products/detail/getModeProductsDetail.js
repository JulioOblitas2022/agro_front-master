import { MODE_SHORTCUT } from '../../../../constants/constants'

export const getModeProductsDetail = () => {
    return {
        new: {
            // form: {
            //     hide: ['code']
            // },
            bar: {
                show: ['save', 'return']
            }
        },
        update:{
            // form: {
            //     read: ['code']
            // },
            bar: {
                show: ['update', 'return']
            }
        },
        read:{
            form: {
                read: [MODE_SHORTCUT.ALL]
            },
            bar: {
                show: ['return']
            }
        }
    }
}
