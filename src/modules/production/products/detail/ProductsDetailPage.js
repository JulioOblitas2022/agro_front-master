import { Col, Row } from 'antd';
import React from 'react'
import BarButtonFw from '../../../../framework/bar-button/BarButtonFw';
import FormFw from '../../../../framework/form/FormFw';
import { BasePage } from '../../../../framework/pages/BasePage';
import { getConfigProductsDetail } from './getConfigProductsDetail';
import { getModeProductsDetail } from './getModeProductsDetail';
import { getValidationProductsDetail } from './getValidationProductsDetail';
import { createLocalSection } from './sections/LocalSection';
import { createRecipeLinesSection } from './sections/RecipeLinesSection';
import { createRecipesSection } from './sections/RecipesSection';
import { createWarehouseSection } from './sections/WarehouseSection';

export class ProductsDetailPage extends BasePage {

    createPageProperties = (pageApi) => {
        return {
            title: 'REGISTRO PRODUCTOS',
            validation: getValidationProductsDetail,
            mode: getModeProductsDetail
        };
    }

    renderPage(pageApi) {

        const config = getConfigProductsDetail(pageApi);

        return (
            <>
                <FormFw
                    name="form"
                    getParentApi={pageApi.getApi}
                    config={config.form}
                />
                {createRecipesSection(pageApi)}
                {createRecipeLinesSection(pageApi)}
                <Row>
                    <Col span={12} style={{ right: '5px'}}>
                        {createLocalSection(pageApi)}
                    </Col>
                    <Col span={12}  style={{ left: '10px'}}>
                        {createWarehouseSection(pageApi)}
                    </Col>
                </Row>
                <BarButtonFw
                    name="bar"
                    getParentApi={pageApi.getApi}
                    config={config.bar}
                />
            </>
        )
    }
}