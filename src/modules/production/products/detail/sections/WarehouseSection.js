import React from 'react'
import FormGridFw, { FORM_GRID_MODE } from '../../../../../framework/form-grid/FormGridFw';

export const createWarehouseSection = (pageApi) => {

    const gridConfig = {
        title: 'ALMACENES DEL LOCAL',
        mode: FORM_GRID_MODE.FORM_ADD,
        collapsible: true,
        form: {
            fields:[
                {type: 'select', name:'warehouseId', label:'Almacen', labelName:'warehouseIdDes', column: 12, load: {queryId: 34}, search: true},
            ],
        },
        grid: {
            // title: '',
            rowId: 'productWarehouseDetailId',
            showIndex: true,
            pagination: true,
            autoLoad: true,
            collapsible: false,
            load: {
                validation: () => pageApi.getParamId()!=null, //solo se dispara si el valor es distinto de nulo
                queryId: 446,
                orderBy: 'productWarehouseDetailId desc',
                params: {productId: pageApi.getParamId()}
            },
            save: {
                queryId: 445,
                params: {productId: pageApi.getParamId()}
            },
            fields:[
                {type: 'select', name: 'warehouseId', label: 'Almacen', labelName:'warehouseIdDes', width: 30, load: {queryId: 34}, editable:true},
                {name: 'min', label: 'Cap. Min', width: 30, editable: true},
                {name: 'max', label:'Cap. Max', width: 30, editable: true},
            ],
        }
    };

    return (
        <FormGridFw
            name="warehouseGrid"
            getParentApi={pageApi.getApi}
            config={gridConfig} 
        />
    )
}