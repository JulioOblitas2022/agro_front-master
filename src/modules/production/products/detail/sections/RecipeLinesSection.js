import React from 'react'
import { DATE_FORMAT } from '../../../../../constants/constants';
import { BUTTON_DEFAULT } from '../../../../../framework/bar-button/BarButtonFw';
import FormGridFw, { FORM_GRID_MODE } from '../../../../../framework/form-grid/FormGridFw';
import GridFw, { COLUMN_DEFAULT } from '../../../../../framework/grid/GridFw';
import { LineModal } from '../modals/LineModal';

export const createRecipeLinesSection = (pageApi) => {

    const getGrid = () => pageApi.getComponent('recipeLinesGrid');

    const fnNew = () => {
        getGrid().addItem({}, true);
    }

    const LineModalConfig = (data) =>  {
        return {
            width: 1600,
            maskClosable: false,
            component: <LineModal name={data}  getParentApi={pageApi.getApi}  />
        }
    };

    const barConfigLineaReceta = {
        fields: [
            { ...BUTTON_DEFAULT.ADD, label: 'Agregar Linea', onClick: fnNew }
        ]
    }

    const gridConfig = {
        title: 'LINEAS DE LA RECETA',
        rowId: 'productRecipeLineDetailId',
        showIndex: true,
        pagination: true,
        autoLoad: true,
        collapsible: false,
        load: {
            validation: () => pageApi.getParamId()!=null, //solo se dispara si el valor es distinto de nulo
            queryId: 442,
            //orderBy: 'productRecipeLineDetailId desc',
            params: {productId: pageApi.getParamId()},
            hideMessage: true, 
            hideMessageError: true
        },
        save: {
            queryId: 441,
            params: {productId: pageApi.getParamId()},
            hideMessage: true, 
            hideMessageError: true
        },
        fields:[
            {name: 'description', label: 'Descripcion', editable:true},
            {type:'select', name:'unitMeasureId', label:'U. M.', labelName:'unitMeasureIdDes', editable:true, load: {queryId: 192}, search: true},
            {type:'select', name:'masterItemId', label: 'Producto Principal', labelName:'masterItemIdDes', editable:true, load: {queryId: 158}, search: true},
            {type: 'decimal', name:'realTime', label:'Tiempo real', editable:true},
            {type: 'number', name: 'quantityProduct', label: 'Cantidad x Hora', editable:true},
            {type: 'number', name: 'operators', label: 'Operarios', editable:true},
            {name:'efficiency', label:'Eficiencia', editable:true},
            {type:'select', name: 'active', label: 'Estado', labelName:'activeDes', load: {dataList:[{value:1, label:'Activo'},{value:2, label:'Inactivo'}]}, editable:true},
            {type: 'decimal', name: 'priceHourTask', label: 'Precio Hora Tarea', editable:true},
            {...COLUMN_DEFAULT.VIEW, onClick: (value, row, formApi) => pageApi.showModal(LineModalConfig(row))}
        ],
        barHeader: barConfigLineaReceta
    };

    return (
        <GridFw
            name="recipeLinesGrid"
            getParentApi={pageApi.getApi}
            config={gridConfig} 
        />
    )
}