import React from 'react'
import FormGridFw, { FORM_GRID_MODE } from '../../../../../framework/form-grid/FormGridFw';

export const createLocalSection = (pageApi) => {

    const gridConfig = {
        title: 'LOCAL DE LA EMPRESA',
        mode: FORM_GRID_MODE.FORM_ADD,
        collapsible: true,
        form: {
            fields:[
                {type: 'select', name:'localId', label:'Local', labelName:'localIdDes', column: 12, load: { queryId: 198 }, search:true },
            ],
        },
        grid: {
            // title: '',
            rowId: 'productLocalDetailId',
            showIndex: true,
            pagination: true,
            autoLoad: true,
            collapsible: false,
            load: {
                validation: () => pageApi.getParamId()!=null, //solo se dispara si el valor es distinto de nulo
                queryId: 444,
                orderBy: 'productLocalDetailId desc',
                params: {productId: pageApi.getParamId()}
            },
            save: {
                queryId: 443,
                params: {productId: pageApi.getParamId()}
            },
            fields:[
                {type: 'select', name: 'localId', label: 'Local', labelName:'localIdDes', load: { queryId: 198 }, search:true, width: 100, editable:true},
            ],
        }
    };

    return (
        <FormGridFw
            name="localGrid"
            getParentApi={pageApi.getApi}
            config={gridConfig}
        />
    )
}