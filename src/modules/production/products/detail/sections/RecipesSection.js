import React from 'react'
import { BUTTON_DEFAULT } from '../../../../../framework/bar-button/BarButtonFw';
import FormGridFw, { FORM_GRID_MODE } from '../../../../../framework/form-grid/FormGridFw';
import GridFw, { COLUMN_DEFAULT } from '../../../../../framework/grid/GridFw';
import { SuppliesModal } from '../modals/SuppliesModal';

export const createRecipesSection = (pageApi) => {

    const getGrid = () => pageApi.getComponent('recipesGrid');

    const fnNew = () => {
        getGrid().addItem({}, true);
    }

    const SuppliesModalConfig = (data) =>  {
        return {
            width: 1600,
            maskClosable: false,
            component: <SuppliesModal name={data}  getParentApi={pageApi.getApi}  />
        }
    };

    const barConfigReceta = {
        fields: [
            { ...BUTTON_DEFAULT.ADD, label: 'Agregar Receta', onClick: fnNew }
        ]
    }

    const gridConfig = {
        title: 'RECETAS',
        rowId: 'productRecipeDetailId',
        showIndex: true,
        pagination: true,
        autoLoad: true,
        collapsible: false,
        load: {
            validation: () => pageApi.getParamId()!=null, //solo se dispara si el valor es distinto de nulo
            queryId: 440,
            //orderBy: 'productRecipeDetailId desc',
            params: {productId: pageApi.getParamId()},
            hideMessage: true, 
            hideMessageError: true
        },
        save: {
            queryId: 439,
            params: {productId: pageApi.getParamId()},
            hideMessage: true, 
            hideMessageError: true
        },
        fields:[
            {name: 'code', label: 'Codigo', editable: true},
            {name: 'description', label: 'Descripcion', editable: true},
            {type: 'select', name:'unitMeasureId', label:'Unidad Medida', labelName:'unitMeasureIdDes', load: {queryId: 192}, search: true, editable: true},
            {type: 'number', name: 'quantity', label: 'Cantidad', editable: true},
            {type: 'number', name:'priorityId', label:'Prioridad', editable: true},
            {name: 'observations', label: 'Observaciones', editable: true},
            {type: 'select', name: 'active', label: 'Estado', labelName:'activeDes', editable: true, load: {dataList:[{value:1, label:'Activo'},{value:2, label:'Inactivo'}]}},
            {...COLUMN_DEFAULT.VIEW, onClick: (value, row, formApi) => pageApi.showModal(SuppliesModalConfig(row))}
        ],
        barHeader: barConfigReceta
    };

    return (
        <GridFw
            name="recipesGrid"
            getParentApi={pageApi.getApi}
            config={gridConfig} 
        />
    )
}