import { PAGE_MODE } from "../../../../constants/constants";
import { BUTTON_DEFAULT } from "../../../../framework/bar-button/BarButtonFw";
import { UserUtil } from "../../../../util/UserUtil";
import { B_SELECT_CONSTANTS } from "../../../shared/constantes/BSelectConstants";

export const getConfigProductsDetail = (pageApi) => {
    const getForm = () => pageApi.getComponent('form');
    const getRecipesGrid = () => pageApi.getComponent('recipesGrid');
    const getRecipeLinesGrid = () => pageApi.getComponent('recipeLinesGrid');
    const getLocalGrid = () => pageApi.getComponent('localGrid.grid');
    const getWarehouseGrid = () => pageApi.getComponent('warehouseGrid.grid');

    return {
        form: {
            load:{
                queryId: 437,
                params: {productId: pageApi.getParamId()},
                fnOk: (resp) => {
                    getRecipesGrid().load({params: {productId: pageApi.getParamId()} });
                    getRecipeLinesGrid().load({params: {productId: pageApi.getParamId()} });
                    getLocalGrid().load({params: {productId: pageApi.getParamId()} });
                    getWarehouseGrid().load({params: {productId: pageApi.getParamId()} });
                }
            },
            update:{
                queryId: 436,
                postLink: (resp, values) => '/production/products/products-admin',
                params: {modifiedBy: UserUtil.getUserId()},
                fnOk: (resp) => {
                    getRecipesGrid().save();
                    getRecipeLinesGrid().save();
                    getLocalGrid().save();
                    getWarehouseGrid().save();
                }
            },
            save:{
                queryId: 435,
                postLink: (resp, values) => '/production/products/products-admin',
                params: {createdBy: UserUtil.getUserId()},
                fnOk: (resp) => {
                    const productId = resp.dataObject?.productId;
                    if (productId){
                        getRecipesGrid().save({params:{productId}});
                        getRecipeLinesGrid().save({params:{productId}});
                        getLocalGrid().save({params:{productId}});
                        getWarehouseGrid().save({params:{productId}});
                    }
                        
                }
            },
            title: 'Datos Principales',
            fields:[
                {name: 'code', label: 'Codigo', column: 4},
                {name: 'description', label: 'Descripcion', column: 8},
                //{type: 'select', name: 'masterItemId', label: 'Producto', column: 4, load: {queryId: 158}, search: true},
                {type:'select', name:'familyId', label:'Familia', column: 4, load: {queryId: 50}, search: true},
                {...B_SELECT_CONSTANTS.CLASS, column: 4, search: true},
                {...B_SELECT_CONSTANTS.SUB_CLASS, column: 4, search: true},
                {type:'select', name:'unitMeasureId', label:'Unidad Medida', column: 4, load: {queryId: 192}, search: true},
                {type: 'switch', name: 'active', label: 'Estado', value: true},
                {type: 'textarea', name: 'observations', label:'Observaciones', column: 12},
            ],
            onComponentLoad : (formApi, pageApi) =>{
                if (pageApi.getPageMode() !== PAGE_MODE.NEW)
                    formApi.load();
            }
        },
        bar: {
            fields: [
                {...BUTTON_DEFAULT.SAVE, onClick: () => getForm().save()},
                {...BUTTON_DEFAULT.UPDATE, onClick: () => getForm().update()},
                {...BUTTON_DEFAULT.RETURN, link: '/production/products/products-admin'}
            ],
        },
        grid: {}
    }
}