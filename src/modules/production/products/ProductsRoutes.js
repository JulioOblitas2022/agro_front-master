import React from 'react'
import { Route, Switch, useRouteMatch } from 'react-router';
import { ProductsAdminPage } from './admin/ProductsAdminPage';
import { ProductsDetailPage } from './detail/ProductsDetailPage';

const ProductsRoutes = () => {
    const { path } = useRouteMatch();
    return (
        <Switch>
            <Route exact path={`${path}/products-admin`} component={ProductsAdminPage} />
            <Route path={`${path}/products`} component={ProductsDetailPage} />
            <Route path={`${path}/products/:id`} component={ProductsDetailPage} /> 
        </Switch>
    )
}

export default ProductsRoutes;
