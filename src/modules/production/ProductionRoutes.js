import React, { lazy } from 'react';
import { Route, Switch, useRouteMatch } from 'react-router-dom';

const TasksRoutes = lazy(() => import('./tasks/TasksRoutes'));
const SeasonalityRoutes = lazy(() => import('./seasonality/SeasonalityRoutes'));
const YieldsRoutes = lazy(() => import('./yields/YieldsRoutes'));
const ProductsRoutes = lazy(() => import('./products/ProductsRoutes'));
const EquipmentRoutes = lazy(() => import('./equipment/EquipmentRoutes'));
const RequestOrderRoutes = lazy(() => import('./request-order/RequestOrderRoutes'));
const ProductionOrderRoutes = lazy(() => import('./production-order/ProductionOrderRoutes'));
const ScheduleProductionRoutes = lazy(() => import('./schedule-production/ScheduleProductionRoutes'));
const ProductionRecordRoutes = lazy(() => import('./production-record/ProductionRecordRoutes'));
const MaterialRequestRoutes = lazy(() => import('./material-request/MaterialRequestRoutes'));
const ProductReviewRoutes = lazy(() => import('./product-review/ProductReviewRoutes'));
const ProductionTasksRoutes = lazy(() => import('./production-tasks/ProductionTasksRoutes'));
const ScheduleCalendarProductionRoutes = lazy(() => import('./schedule-calendar-production/ScheduleCalendarProductionRoutes'));

const ProductionRoutes = () => {
    const { path } = useRouteMatch();
    return (
        <Switch>
            <Route path={`${path}/tasks`} component={TasksRoutes} />
            <Route path={`${path}/seasonality`} component={SeasonalityRoutes} />
            <Route path={`${path}/yields`} component={YieldsRoutes} />
            <Route path={`${path}/products`} component={ProductsRoutes} />
            <Route path={`${path}/equipment`} component={EquipmentRoutes} />
            <Route path={`${path}/request-order`} component={RequestOrderRoutes} />
            <Route path={`${path}/production-order`} component={ProductionOrderRoutes} />
            <Route path={`${path}/schedule-production`} component={ScheduleProductionRoutes} />
            <Route path={`${path}/production-record`} component={ProductionRecordRoutes} />
            <Route path={`${path}/material-request`} component={MaterialRequestRoutes} />
            <Route path={`${path}/product-review`} component={ProductReviewRoutes} />
            <Route path={`${path}/production-tasks`} component={ProductionTasksRoutes} />
            <Route path={`${path}/schedule-calendar-production`} component={ScheduleCalendarProductionRoutes} />
        </Switch>
    )
}

export default ProductionRoutes;
