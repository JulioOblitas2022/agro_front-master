import { DATE_FORMAT } from "../../../../constants/constants";
import { BUTTON_DEFAULT } from "../../../../framework/bar-button/BarButtonFw";
import { COLUMN_DEFAULT } from "../../../../framework/grid/GridFw";
import { getFormDefaultParams } from "../../../shared/util/getFormDefaultParams";
import { getGridServerDefaultParams } from "../../../shared/util/getGridServerDefaultParams";

export const getConfigMaterialRequestAdmin = (pageApi) => {
    const getGrid = () => pageApi.getComponent('grid');
    const getForm = () => pageApi.getComponent('form');
    
    return {
        form: {
            ...getFormDefaultParams(),
            title: 'Filtros de búsqueda',
            fields:[
                {name:'materialRequestNumber', label:'Nro. Solicitud.', column: 4},
                {type: 'date', name:'dateFrom', label:'Desde', column: 4, format: 'DD/MM/YYYY', inputFormat: DATE_FORMAT.YYYYMMDD},
                {type: 'date', name:'dateUntil', label:'Hasta', column: 4, format: 'DD/MM/YYYY', inputFormat: DATE_FORMAT.YYYYMMDD},
            ]
        },
        bar: { fields: [
            {...BUTTON_DEFAULT.SEARCH, onClick: ()=>getGrid().load({params:getForm().getData()})},
            {...BUTTON_DEFAULT.CLEAR, onClick: ()=>getForm().reset()}
        ]},
        grid: {
            ...getGridServerDefaultParams({queryLoad: 506}),
            title: 'LISTA DE SOLICITUD DE MATERIALES',
            scroll:{
                x: 1300
            },
            fields:[
                {name: 'materialRequestNumber', label:'Nro. Solicitud.', width: 100},
                {name: 'broadcastDate', label:'Fec. Emisión', width: 100},
                {name: 'product', label: 'Producto', width: 100},
                {name: 'recipe', label: 'Receta', width: 100},
                {name: 'productionQuantity', label: 'Cant. Producir', width: 100},
                {name: 'productionDate', label: 'Fec. Prod.', width: 100},
                {name: 'productionRecordNumber', label: 'Nro. Prod.', width: 100},
                {...COLUMN_DEFAULT.VIEW, link: (value, row, index) => `/production/material-request/material-request/${row.materialRequestId}` },
                {...COLUMN_DEFAULT.EDIT, link: (value, row, index) => `/production/material-request/material-request/${row.materialRequestId}/update` },
            ]
        }
    }
}