
import { DATE_FORMAT } from '../../../../../constants/constants';
import { BUTTON_DEFAULT } from '../../../../../framework/bar-button/BarButtonFw';
import GridFw, { COLUMN_DEFAULT } from '../../../../../framework/grid/GridFw';
import { ProductSuppliesModal } from '../modals/ProductSuppliesModal';

export const createProductSuppliesSection = (pageApi) => {
    
    const fnDelete = (value, row, index) => {
        getGrid().deleteItem(row.key);      
    }
    
    const getGrid = () => pageApi.getComponent('ProductSuppliesSection');

    const formProduct = () => pageApi.getComponent('form');

    const itemConfig = {
        events: {
            afterAddEvent : (data) => {
                getGrid().addItem(data);
            }
        }
    }

    const ItemModalConfig =  {
        
            width: 1600,
            maskClosable: false,
            component: <ProductSuppliesModal name='productSuppliesModal'  getParentApi={pageApi.getApi} config={itemConfig}  />
    };


    const barHeaderConfig = {
        fields:[
            {...BUTTON_DEFAULT.SEARCH, label:'Agregar Producto', onClick: ()=>{  pageApi.showModal(ItemModalConfig); }}
        ]
    }


    const gridConfig = {
        title: 'Productos',
        collapsible: true,
        rowId: 'materialRequestDetailId',
        showIndex: true,
        pagination: true,
        autoLoad: true,
        load: {
            validation: () => pageApi.getParamId()!=null, //solo se dispara si el valor es distinto de nulo
            queryId: 509,
            orderBy: 'materialRequestDetailId desc',
            params: {materialRequestId: pageApi.getParamId()}
        },
        save: {
            queryId: 508,
            params: {materialRequestId: pageApi.getParamId()}
        },
        scroll:{
          x:1300  
        },
        fields:[
            {name: 'code', label: 'Código', width: 200},
            {name: 'masterItem', label: 'Producto', width: 200},
            {name: 'unitMeasure', label: 'Unidad Medida',width: 200},
            {type: 'number',name: 'quantityTeoric', label: 'Cantidad Teórica',width: 200, editable:true},
            {type: 'number',name: 'quantityRequired', label: 'Cantidad Requerida',width: 200, editable:true},
            {name: 'lot', label: 'Lote',width: 200},
            {...COLUMN_DEFAULT.DELETE, onClick: fnDelete}
        ],
        barHeader: barHeaderConfig
    };

    return (
        <GridFw
            name="ProductSuppliesSection"
            getParentApi={pageApi.getApi}
            config={gridConfig} 
        />
    )
}

