import { DATE_FORMAT, PAGE_MODE } from "../../../../constants/constants";
import { BUTTON_DEFAULT } from "../../../../framework/bar-button/BarButtonFw";
import { UserUtil } from "../../../../util/UserUtil";
import { DateUtil } from "../../../../util/DateUtil";

export const getConfigMaterialRequestDetail = (pageApi) => {
    const getForm = () => pageApi.getComponent('form');
    const getGridProductos = () => pageApi.getComponent('ProductSuppliesSection');

    return {
        form: {
            load: {
                queryId: 505,
                params: { materialRequestId: pageApi.getParamId() },
                fnOk: (resp) => {
                    getGridProductos().load({ params: { materialRequestId: pageApi.getParamId() } })
                }
            },
            update: {
                queryId: 504,
                postLink: (resp, values) => '/production/material-request/material-request-admin',
                params: { modifiedBy: UserUtil.getUserId() },
                fnOk: (resp) => {
                    getGridProductos().save({ params: { materialRequestId: pageApi.getParamId() } })
                }
            },
            save: {
                queryId: 503,
                postLink: (resp, values) => '/production/material-request/material-request-admin',
                params: { createdBy: UserUtil.getUserId() },
                fnOk: (resp) => {
                    let materialRequestId = resp.dataObject?.materialRequestId;
                    if (materialRequestId) {
                        getGridProductos().save({ params: { materialRequestId } })
                    }
                }
            },
            //title: 'DATOS DE LA SOLICITUD',
            fields: [
                { type: 'divider', label: 'DATOS DE LA SOLICITUD' },
                { type: 'select', name: 'documentTypeId', label: 'Tipo Documento', column: 4, load: { queryId: 195 }, value: 52 },
                { type: 'date', name: 'recordDate', label: 'Fec. Registro', column: 4, format: DATE_FORMAT.DDMMYYYY, inputFormat: DATE_FORMAT.YYYYMMDD, value: DateUtil.getDate },
                { type: 'select', name: 'applicantId', label: 'Solicitante', column: 4, load: { queryId: 309 } },
                { type: 'divider', label: 'DATOS DE LA PRODUCCIÓN' },
                {
                    type: 'select', name: 'productionRecordId', label: 'Nro. Producción', column: 6, load: { queryId: 501 }, search: true,
                    onChange: (value, values) => {
                        let productionQuantity = getForm().getFieldApi('productionRecordId').getSelectedItem()?.productionQuantity;
                        let productionDate = getForm().getFieldApi('productionRecordId').getSelectedItem()?.productionDate;
                        let product = getForm().getFieldApi('productionRecordId').getSelectedItem()?.product;
                        let unitMeasure = getForm().getFieldApi('productionRecordId').getSelectedItem()?.unitMeasure;
                        let recipe = getForm().getFieldApi('productionRecordId').getSelectedItem()?.recipe;
                        let productionOrderNumber = getForm().getFieldApi('productionRecordId').getSelectedItem()?.productionOrderNumber;
                        let productionOrderDate = getForm().getFieldApi('productionRecordId').getSelectedItem()?.productionOrderDate;

                        getForm()?.setFieldData('productionQuantity', productionQuantity);
                        getForm()?.setFieldData('productionDate', productionDate);
                        getForm()?.setFieldData('product', product);
                        getForm()?.setFieldData('unitMeasure', unitMeasure);
                        getForm()?.setFieldData('recipe', recipe);
                        getForm()?.setFieldData('productionOrderNumber', productionOrderNumber);
                        getForm()?.setFieldData('productionOrderDate', productionOrderDate);
                    }
                },
                { name: 'product', label: 'Producto', column: 4, },
                { name: 'unitMeasure', label: 'Uni. Medida', column: 4, },
                { name: 'productionQuantity', label: 'Cantidad a Producir', column: 4, },
                { type: 'date', name: 'productionDate', label: 'Fec. Producción', column: 4, format: DATE_FORMAT.DDMMYYYY, inputFormat: DATE_FORMAT.YYYYMMDD },
                { name: 'recipe', label: 'Receta', column: 4, },
                { name: 'productionOrderNumber', label: 'Nro. Ord. Prod.', column: 4, },
                { type: 'date', name: 'productionOrderDate', label: 'Fec. Ord.Prod.', column: 4, format: DATE_FORMAT.DDMMYYYY, inputFormat: DATE_FORMAT.YYYYMMDD },
                { type: 'textarea', name: 'observations', label: 'Observaciones', column: 12, },
            ],
            onComponentLoad: (formApi, pageApi) => {
                if (pageApi.getPageMode() !== PAGE_MODE.NEW)
                    formApi.load();
            }
        },
        bar: {
            fields: [
                { ...BUTTON_DEFAULT.SAVE, onClick: () => getForm().save() },
                { ...BUTTON_DEFAULT.UPDATE, onClick: () => getForm().update() },
                { ...BUTTON_DEFAULT.RETURN, link: '/production/material-request/material-request-admin' }
            ],
        },
        grid: {}
    }
}