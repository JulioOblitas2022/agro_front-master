import React from 'react'
import BarButtonFw from '../../../../framework/bar-button/BarButtonFw';
import FormFw from '../../../../framework/form/FormFw';
import { BasePage } from '../../../../framework/pages/BasePage';
import { getConfigMaterialRequestDetail } from './getConfigMaterialRequestDetail';
import { getModeMaterialRequestDetail } from './getModeMaterialRequestDetail';
import { getValidationMaterialRequestDetail } from './getValidationMaterialRequestDetail';
import { createProductSuppliesSection } from './sections/ProductSuppliesSection';

export class MaterialRequestDetailPage extends BasePage {
    
    createPageProperties = (pageApi) => {
        return {
            title: 'REGISTRO DE SOLICITUD DE MATERIALES',
            validation: getValidationMaterialRequestDetail,
            mode: getModeMaterialRequestDetail
        };
    }

    renderPage(pageApi) {

        const config = getConfigMaterialRequestDetail(pageApi);

        return (
            <>
                <FormFw
                    name="form"
                    getParentApi={pageApi.getApi}
                    config={config.form}
                />

                {createProductSuppliesSection(pageApi)}

                <BarButtonFw
                    name="bar"
                    getParentApi={pageApi.getApi}
                    config={config.bar}
                />
            </>
        )
    }
}