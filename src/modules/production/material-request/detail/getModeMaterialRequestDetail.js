import { MODE_SHORTCUT } from '../../../../constants/constants'

export const getModeMaterialRequestDetail = () => {
    return {
        new: {
            form: {
                read: ['documentTypeId','productionQuantity','productionDate','product','unitMeasure','recipe','productionOrderNumber','productionOrderDate']
            },
            bar: {
                show: ['save', 'return']
            }
        },
        update:{
            form: {
                read: ['documentTypeId','productionQuantity','productionDate','product','unitMeasure','recipe','productionOrderNumber','productionOrderDate']
            },
            bar: {
                show: ['update', 'return']
            }
        },
        read:{
            form: {
                read: [MODE_SHORTCUT.ALL]
            },
            bar: {
                show: ['return']
            }
        }
    }
}
