import React from 'react'
import { Route, Switch, useRouteMatch } from 'react-router';
import { MaterialRequestAdminPage } from './admin/MaterialRequestAdminPage';
import { MaterialRequestDetailPage } from './detail/MaterialRequestDetailPage';

const MaterialRequestRoutes = () => {
    const { path } = useRouteMatch();
    return (
        <Switch>
            <Route exact path={`${path}/material-request-admin`} component={MaterialRequestAdminPage} />
            <Route path={`${path}/material-request`} component={MaterialRequestDetailPage} />
            <Route path={`${path}/material-request/:id`} component={MaterialRequestDetailPage} /> 
        </Switch>
    )
}

export default MaterialRequestRoutes;