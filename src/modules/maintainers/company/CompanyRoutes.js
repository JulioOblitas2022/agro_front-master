import React from 'react'
import { Route, Switch, useRouteMatch } from 'react-router';
import { CompanyAdminPage } from './admin/CompanyAdminPage';
import { CompanyDetailPage } from './detail/CompanyDetailPage';

const CompanyRoutes = () => {
    const { path } = useRouteMatch();
    return (
        <Switch>
            <Route exact path={`${path}/company-admin`} component={CompanyAdminPage} />
            <Route path={`${path}/company`} component={CompanyDetailPage} />
            <Route path={`${path}/company/:id`} component={CompanyDetailPage} />
        </Switch>
    )
}

export default CompanyRoutes;