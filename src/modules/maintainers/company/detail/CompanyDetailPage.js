import React from 'react'
import BarButtonFw from '../../../../framework/bar-button/BarButtonFw';
import FormFw from '../../../../framework/form/FormFw';
import { BasePage } from '../../../../framework/pages/BasePage';
import { getConfigCompanyDetail } from './getConfigCompanyDetail';
import { getModeCompanyDetail } from './getModeCompanyDetail';
import { getValidationCompanyDetail } from './getValidationCompanyDetail';

export class CompanyDetailPage extends BasePage {
    
    createPageProperties = (pageApi) => {
        return {
            title: 'DETALLE EMPRESA',
            validation: getValidationCompanyDetail,
            mode: getModeCompanyDetail
        };
    }

    renderPage(pageApi) {

        const config = getConfigCompanyDetail(pageApi);

        return (
            <>
                <FormFw
                    name="form"
                    getParentApi={pageApi.getApi}
                    config={config.form}
                />
                <BarButtonFw
                    name="bar"
                    getParentApi={pageApi.getApi}
                    config={config.bar}
                />
            </>
        )
    }
}