
export const getValidationCompanyDetail = () => {
    return {
        form: {
            companyName: [{ required: true, message: 'Empresa es requerida' }],
            docTypeId: [{ required: true, message: 'Doc. Personal es requerido' }],
            cardId: [{ required: true, message: 'Doc. Identidad es requerido' }],
            shortName: [{ required: true, message: 'Nombre Corto es requerido' }],
            address: [{ required: true, message: 'Direccion es requerida' }],
            email: [{ required: true, message: 'Correo es requerido' }],
            passwordEmail: [{ required: true, message: 'Contraseña es requerida' }],
        },
        grid: {

        },
        button: {

        },
        bar: {

        }
    }
}
