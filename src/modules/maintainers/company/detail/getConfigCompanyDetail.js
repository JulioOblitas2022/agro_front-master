import { DATE_FORMAT, PAGE_MODE } from "../../../../constants/constants";
import { BUTTON_DEFAULT } from "../../../../framework/bar-button/BarButtonFw";
import { B_SELECT_CONSTANTS } from "../../../shared/constantes/BSelectConstants";

export const getConfigCompanyDetail = (pageApi) => {

    const getForm = () => pageApi.getComponent('form');

    return {
        form: {
            load: {
                queryId: 556,
                params: { companyId: pageApi.getParamId() },
            },
            update: {
                queryId: 557,
                postLink: (resp, values) => '/maintainers/company/company-admin'
            },
            save: {
                queryId: 555,
                postLink: (resp, values) => '/maintainers/company/company-admin'
            },
            fields: [
                { name: 'companyName', label: 'Empresa', column: 3, },
                { name: 'docTypeId', label: 'Doc. Personal', column: 3 },
                { name: 'cardId', label: 'Doc. Identidad', column: 3 },
                { name: 'shortName', label: 'Nombre Corto', column: 3 },
                { name: 'address', label: 'Direccion', column: 4 },
                { name: 'email', label: 'Correo', column: 4 },
                { name: 'passwordEmail', label: 'Contraseña', column: 4 },
                { name: 'salesManager', label: 'Nombre Manager de Ventas', column: 4 },
                { name: 'salesContact', label: 'Correo de Ventas', column: 4 },
                { name: 'alternativeName', label: 'Nombre Alterno Empresa', column: 4 },
                { type: 'switch', name: 'active', label: 'Activo', column: 3, value: true },
            ],
            onComponentLoad: (formApi, pageApi) => {
                if (pageApi.getPageMode() !== PAGE_MODE.NEW)
                    formApi.load();
            }
        },
        bar: {
            fields: [
                { ...BUTTON_DEFAULT.SAVE, onClick: () => getForm().save() },
                { ...BUTTON_DEFAULT.UPDATE, onClick: () => getForm().update() },
                { ...BUTTON_DEFAULT.RETURN, link: '/maintainers/company/company-admin' }
            ],
        },
        grid: {}
    }
}
