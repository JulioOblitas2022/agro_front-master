import { BUTTON_DEFAULT } from "../../../../framework/bar-button/BarButtonFw";
import { COLUMN_DEFAULT } from "../../../../framework/grid/GridFw";
import { getFormDefaultParams } from "../../../shared/util/getFormDefaultParams";
import { getGridServerDefaultParams } from "../../../shared/util/getGridServerDefaultParams";

export const getConfigCompanyAdmin = (pageApi) => {
    const getGrid = () => pageApi.getComponent('grid');
    const getForm = () => pageApi.getComponent('form');

    return {
        form: {
            ...getFormDefaultParams(),
            title: 'FILTROS DE BUSQUEDA',
            fields: [
                { name: 'companyName', label: 'Empresa', column: 4, },
                { name: 'cardId', label: 'Doc. Identidad', column: 4 },
                { name: 'shortName', label: 'Nombre Corto', column: 4 },
            ]
        },
        bar: {
            fields: [
                { ...BUTTON_DEFAULT.SEARCH, onClick: () => getGrid().load({ params: getForm().getData() }) },
                { ...BUTTON_DEFAULT.CLEAR, onClick: () => getForm().reset() }
            ]
        },
        grid: {
            ...getGridServerDefaultParams({ queryLoad: 243 }),
            title: 'RESULTADOS',
            fields: [
                { name: 'name', label: 'Razon Social', },
                { name: 'cardId', label: 'Doc. Identidad', },
                { name: 'shortName', label: 'Nom. Corto', },
                { name: 'address', label: 'Direccion', },
                { ...COLUMN_DEFAULT.VIEW, link: (value, row, index) => `/maintainers/company/company/id-${row.companyId}` },
                { ...COLUMN_DEFAULT.EDIT, link: (value, row, index) => `/maintainers/company/company/id-${row.companyId}/update` },
                { ...COLUMN_DEFAULT.DELETE, process: { fnOk: (result) => getGrid().load({ params: getForm().getData() }), filter: (value, row, index) => ({ queryId: 558, params: { companyId: row.companyId } }) } }
            ]
        }
    }
}