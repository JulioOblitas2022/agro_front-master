
export const getValidationEmployeeDetail = () => {
    return {
        form: {
            name: [ {required: true, message: 'Nombres son requeridos' } ],
            lastName: [ {required: true, message: 'Apellidos son requeridos' } ],
            cardId: [ {required: true, message: 'Doc. Identidad es requerido' } ],
            bornInDepartamentoId: [ {required: true, message: 'Doc. Identidad es requerido' } ],
            bornInProvinciaId: [ {required: true, message: 'Doc. Identidad es requerido' } ],
            bornInDistritoId: [ {required: true, message: 'Doc. Identidad es requerido' } ],
        },
        grid: {

        },
        button: {

        },
        bar: {

        }
    }
}
