import React from 'react'
import BarButtonFw from '../../../../framework/bar-button/BarButtonFw';
import FormFw from '../../../../framework/form/FormFw';
import { BasePage } from '../../../../framework/pages/BasePage';
import { getConfigEmployeeDetail } from './getConfigEmployeeDetail';
import { getModeEmployeeDetail } from './getModeEmployeeDetail';
import { getValidationEmployeeDetail } from './getValidationEmployeeDetail';

export class EmployeeDetailPage extends BasePage {
    
    createPageProperties = (pageApi) => {
        return {
            title: 'DETALLE EMPLEADO',
            validation: getValidationEmployeeDetail,
            mode: getModeEmployeeDetail
        };
    }

    renderPage(pageApi) {

        const config = getConfigEmployeeDetail(pageApi);

        return (
            <>
                <FormFw
                    name="form"
                    getParentApi={pageApi.getApi}
                    config={config.form}
                />
                <BarButtonFw
                    name="bar"
                    getParentApi={pageApi.getApi}
                    config={config.bar}
                />
            </>
        )
    }
}