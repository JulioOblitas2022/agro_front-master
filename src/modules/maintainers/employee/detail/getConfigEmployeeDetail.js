import { DATE_FORMAT, PAGE_MODE } from "../../../../constants/constants";
import { BUTTON_DEFAULT } from "../../../../framework/bar-button/BarButtonFw";
import { B_SELECT_CONSTANTS } from "../../../shared/constantes/BSelectConstants";

export const getConfigEmployeeDetail = (pageApi) => {
    
    const getForm = () => pageApi.getComponent('form');

    return {
        form: {
            load:{
                queryId: 144,
                params: {employeeId: pageApi.getParamId()},
            },
            update:{
                queryId: 146,
                postLink: (resp, values) => '/maintainers/employee/employee-admin'
            },
            save:{
                queryId: 145,
                postLink: (resp, values) => '/maintainers/employee/employee-admin'
            },
            fields:[
                {type: 'select', name:'companyId', label:'Empresa', column: 3, load: { queryId: 244 }, search: true},
                {type: 'select', name:'docTypeId', label:'Doc. Personal', column: 3, load:{ queryId: 195 }, search: true},
                {name:'cardId', label:'Doc. Identidad', column: 3},
                {name:'name', label:'Nombres', column: 3},
                {name:'lastName', label:'Apellidos', column: 3},
                {name:'dateBirth', label:'Fecha Nac.', format: 'DD/MM/YYYY', inputFormat: DATE_FORMAT.YYYYMMDD, column: 3},
                {...B_SELECT_CONSTANTS.SEX},
                {name:'essaludCard', label:'Carnet EsSalud', column: 3},
                {type: 'date', name:'entrydate', label:'Fecha Ingreso', format: 'DD/MM/YYYY', inputFormat: DATE_FORMAT.YYYYMMDD, column: 3},
                {type: 'switch', name:'householdAllowance', label:'Asignacion Familiar', column: 3, value: true},
                {type: 'number', name:'basicSalary', label:'Sueldo Basico', column: 3},
                {type: 'switch', name:'hasBonus', label:'Tiene Bono', column: 3, value: true},
                {type: 'number', name:'paymentAmount', label:'Imp. Pago', column: 3},
                {type: 'number', name:'extraPayment', label:'Imp. Extra', column: 3},
                {type: 'switch', name:'piecework', label:'Destajo', column: 3, value: true},
                {name:'address', label:'Direccion', column: 3},
                {name:'email', label:'Correo', column: 3},
                
                {type:'select', name:'bornInDepartamentoId', label:'Dep. Nac.', column: 3, allowClear: true, load: {queryId: 78}, search:true},
                {type: 'select', name:'bornInProvinciaId', label:'Prov. Nac.', column: 3, allowClear: true, load: {queryId: 79}, search:true,
                    parents: [
                        {name:'bornInDepartamentoId', paramName: 'bornInDepartamentoId', required: true}
                    ]
                },
                {type: 'select', name:'bornInDistritoId', label:'Dis. Nac.', column: 3, allowClear: true, load: {queryId: 80}, search:true,
                    parents: [
                        {name:'bornInProvinciaId', paramName: 'bornInProvinciaId', required: true}
                    ]
                },

                {type:'select', name:'currentDepartamentoId', label:'Dep. Actual', column: 3, allowClear: true, load: {queryId: 78}, search:true},
                {type: 'select', name:'currentProvinciaId', label:'Prov. Actual', column: 3, allowClear: true, load: {queryId: 79}, search:true,
                    parents: [
                        {name:'currentDepartamentoId', paramName: 'currentDepartamentoId', required: true}
                    ]
                },
                {type: 'select', name:'currentDistritoId', label:'Dis. Actual', column: 3, allowClear: true, load: {queryId: 80}, search:true,
                    parents: [
                        {name:'currentProvinciaId', paramName: 'currentProvinciaId', required: true}
                    ]
                },

                {type: 'switch', name:'outstanding', label:'Destacado', column: 3, value: true}, // checked:"Si", unChecked:"No"
                {type: 'date', name:'departureDate', label:'Fecha Baja', format: 'DD/MM/YYYY', inputFormat: DATE_FORMAT.YYYYMMDD, column: 3},
                {type: 'switch', name:'active', label:'Activo', column: 3, value: true},
            ],
            onComponentLoad : (formApi, pageApi) =>{
                if (pageApi.getPageMode() !== PAGE_MODE.NEW)
                    formApi.load();
            }
        },
        bar: {
            fields: [
                {...BUTTON_DEFAULT.SAVE, onClick: () => getForm().save()},
                {...BUTTON_DEFAULT.UPDATE, onClick: () => getForm().update()},
                {...BUTTON_DEFAULT.RETURN, link: '/maintainers/employee/employee-admin'}
            ],
        },
        grid: {}
    }
}
