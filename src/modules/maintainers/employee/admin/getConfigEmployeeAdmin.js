import { BUTTON_DEFAULT } from '../../../../framework/bar-button/BarButtonFw';
import { COLUMN_DEFAULT } from '../../../../framework/grid/GridFw';
import { getFormDefaultParams } from '../../../shared/util/getFormDefaultParams';
import { getGridServerDefaultParams } from '../../../shared/util/getGridServerDefaultParams';

export const getConfigEmployeeAdmin = (pageApi) => {
    const getGrid = () => pageApi.getComponent('grid');
    const getForm = () => pageApi.getComponent('form');
    
    return {
        form: {
            ...getFormDefaultParams(),
            title: 'FILTROS DE BUSQUEDA',
            fields:[
                {name:'name', label:'Nombres', column: 6},
                {name:'lastName', label:'Apellidos', column: 6},
            ]
        },
        bar: { fields: [
            {...BUTTON_DEFAULT.SEARCH, onClick: ()=>getGrid().load({params:getForm().getData()})},
            {...BUTTON_DEFAULT.CLEAR, onClick: ()=>getForm().reset()}
        ]},
        grid: {
            ...getGridServerDefaultParams({queryLoad: 242}),
            title: 'RESULTADOS',
            fields:[
                {name: 'name', label: 'Nombres',},
                {name: 'lastName', label: 'Apellidos',},
                {name: 'cardId', label: 'Doc. Identidad',},
                {name: 'dateBirth', label: 'Fecha Nac.',},
                {...COLUMN_DEFAULT.VIEW, link: (value, row, index) => `/maintainers/employee/employee/${row.employeeId}` },
                {...COLUMN_DEFAULT.EDIT, link: (value, row, index) => `/maintainers/employee/employee/${row.employeeId}/update` },
                {...COLUMN_DEFAULT.DELETE, process: {fnOk: (result)=>getGrid().load({params:getForm().getData()}),filter: (value, row, index) => ({queryId: 147, params: {employeeId: row.employeeId}}) }}
            ]
        }
    }
}