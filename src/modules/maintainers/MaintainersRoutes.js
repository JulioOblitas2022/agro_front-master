import React, { lazy } from 'react'
import { Route, Switch, useRouteMatch } from 'react-router-dom'

const CategoryRoutes = lazy(() => import('./category/CategoryRoutes'));
const EmployeeRoutes = lazy(() => import('./employee/EmployeeRoutes'));
const CompanyRoutes = lazy(() => import('./company/CompanyRoutes'));

const MaintainersRoutes = () => {
    const { path } = useRouteMatch();
    return (
        <Switch>
            <Route path={`${path}/category`} component={CategoryRoutes} />
            <Route path={`${path}/employee`} component={EmployeeRoutes} />
            <Route path={`${path}/company`} component={CompanyRoutes} />
        </Switch>
    )
}

export default MaintainersRoutes;