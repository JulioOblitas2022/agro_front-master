import React from 'react'
import { Route, Switch, useRouteMatch } from 'react-router-dom'

import { CategoryAdminPage } from './admin/CategoryAdminPage';
import { CategoryDetailPage } from './detail/CategoryDetailPage';

const CategoryRoutes = () => {
    const { path } = useRouteMatch();
    return (
        <Switch>
            <Route exact path={`${path}/category-admin`} component={CategoryAdminPage} />
            <Route path={`${path}/category`} component={CategoryDetailPage} />
            <Route path={`${path}/category/:id`} component={CategoryDetailPage} />
        </Switch>
    )
}

export default CategoryRoutes;