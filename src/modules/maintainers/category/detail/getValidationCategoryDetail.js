export const getValidationCategoryDetail = () => {
    return {
        form: {
            description: [ {required: true, message: 'Descripcion es requerida' } ],
            type: [ {required: true, message: 'Tipo es requerido' } ],
        },
        grid: {

        },
        button: {

        },
        bar: {

        }
    }
}
