import React from 'react'
import BarButtonFw from '../../../../framework/bar-button/BarButtonFw';
import FormFw from '../../../../framework/form/FormFw';
import { BasePage } from '../../../../framework/pages/BasePage';
import { getConfigCategoryDetail } from './getConfigCategoryDetail';
import { getModeCategoryDetail } from './getModeCategoryDetail';
import { getValidationCategoryDetail } from './getValidationCategoryDetail';

export class CategoryDetailPage extends BasePage {
    
    createPageProperties = (pageApi) => {
        return {
            title: 'DETALLE CATEGORIA',
            validation: getValidationCategoryDetail,
            mode: getModeCategoryDetail
        };
    }

    renderPage(pageApi) {

        const config = getConfigCategoryDetail(pageApi);

        return (
            <>
                <FormFw
                    name="form"
                    getParentApi={pageApi.getApi}
                    config={config.form}
                />
                <BarButtonFw
                    name="bar"
                    getParentApi={pageApi.getApi}
                    config={config.bar}
                />
            </>
        )
    }
}