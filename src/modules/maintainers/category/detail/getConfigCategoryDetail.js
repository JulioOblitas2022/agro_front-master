import { PAGE_MODE } from '../../../../constants/constants';
import { BUTTON_DEFAULT } from '../../../../framework/bar-button/BarButtonFw';

export const getConfigCategoryDetail = (pageApi) => {
    
    const getForm = () => pageApi.getComponent('form');
    // const getDataPageMode = () => pageApi.getDataPageMode(getModeCategoryDetail());

    return {
        form: {
            load:{
                queryId: 144,
                params: {categoryId: pageApi.getParamId()},
            },
            update:{
                queryId: 146,
                postLink: (resp, values) => '/maintainers/category/category-admin'
            },
            save:{
                queryId: 145,
                postLink: (resp, values) => '/maintainers/category/category-admin'
            },
            fields:[
                {name:'description', label:'Descripcion', column: 4},
                {name:'code', label:'Codigo', column: 4},
                {type: 'select', name:'type', label:'Tipo', column: 4,
                    load: {
                        dataList: [
                            {value: '1', label: 'BREVETE'},
                            {value: '2', label: 'EMPLEADO'},
                            {value: '3', label: 'EMPRESA'},
                        ]
                    }
                },
            ],
            // validations: getValidationCategoryDetail()['form'],
            // mode : getDataPageMode(),
            onComponentLoad : (formApi, pageApi) =>{
                if (pageApi.getPageMode() !== PAGE_MODE.NEW)
                    formApi.load();
            }
        },
        bar: {
            fields: [
                {...BUTTON_DEFAULT.SAVE, onClick: () => getForm().save()},
                {...BUTTON_DEFAULT.UPDATE, onClick: () => getForm().update()},
                {...BUTTON_DEFAULT.RETURN, link: '/maintainers/category/category-admin'}
            ],
            // mode : getDataPageMode()
        },
        grid: {}
    }
}