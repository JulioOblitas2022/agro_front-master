import React from 'react'
import BarButtonFw, { BUTTON_DEFAULT } from '../../../../framework/bar-button/BarButtonFw';
import FormFw from '../../../../framework/form/FormFw';
import GridFw from '../../../../framework/grid/GridFw';
import { BasePage } from '../../../../framework/pages/BasePage';
import { getConfigCategoryAdmin } from './getConfigCategoryAdmin';

export class CategoryAdminPage extends BasePage {
    createPagePropertiesBase(pageApi) {
        return {
            title: 'CATEGORIAS',
            barHeader: {
                fields:[
                    {...BUTTON_DEFAULT.NEW, label:'Nueva', link: '/maintainers/category/category/new'}
                ]
            }
        };
    }
    
    renderPage(pageApi) {

        const config = getConfigCategoryAdmin(pageApi);

        return (
            <>
                <FormFw name="form" getParentApi={pageApi.getApi} config={config.form} />
                <BarButtonFw name="bar" getParentApi={pageApi.getApi} config={config.bar} />
                <GridFw name="grid" getParentApi={pageApi.getApi} config={config.grid} />
            </>
        )
    }
}