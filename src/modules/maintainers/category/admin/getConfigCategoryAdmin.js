import { BUTTON_DEFAULT } from '../../../../framework/bar-button/BarButtonFw';
import { COLUMN_DEFAULT } from '../../../../framework/grid/GridFw';
import { getFormDefaultParams } from '../../../shared/util/getFormDefaultParams';
import { getGridServerDefaultParams } from '../../../shared/util/getGridServerDefaultParams';

export const getConfigCategoryAdmin = (pageApi) => {
    const getGrid = () => pageApi.getComponent('grid');
    const getForm = () => pageApi.getComponent('form');
    
    return {
        form: {
            ...getFormDefaultParams(),
            title: 'FILTROS DE BUSQUEDA',
            fields:[
                {name:'description', label:'Categoria', column: 6},
            ]
        },
        bar: { fields: [
            {...BUTTON_DEFAULT.SEARCH, onClick: ()=>getGrid().load({params:getForm().getData()})},
            {...BUTTON_DEFAULT.CLEAR, onClick: ()=>getForm().reset()}
        ]},
        grid: {
            ...getGridServerDefaultParams({queryLoad: 143}),
            title: 'RESULTADOS',
            fields:[
                {name: 'description', label: 'Categoria',},
                {name: 'code', label: 'Codigo',},
                {...COLUMN_DEFAULT.VIEW, link: (value, row, index) => `/maintainers/category/category/id-${row.categoryId}` },
                {...COLUMN_DEFAULT.EDIT, link: (value, row, index) => `/maintainers/category/category/id-${row.categoryId}/update` },
                {...COLUMN_DEFAULT.DELETE, process: {fnOk: (result)=>getGrid().load({params:getForm().getData()}),filter: (value, row, index) => ({queryId: 147, params: {categoryId: row.categoryId}}) }}
            ]
        }
    }
}
