import React from 'react'
import { Route, Switch, useRouteMatch } from 'react-router';
import { EmployeeAdminPage } from './admin/EmployeeAdminPage';
import { EmployeeDetailPage } from './detail/EmployeeDetailPage';

const EmployeeRoutes = () => {
    const { path } = useRouteMatch();
    return (
        <Switch>
            <Route exact path={`${path}/employee-admin`} component={EmployeeAdminPage} />
            <Route path={`${path}/employee`} component={EmployeeDetailPage} />
            <Route path={`${path}/employee/:id`} component={EmployeeDetailPage} /> 
        </Switch>
    )
}

export default EmployeeRoutes;