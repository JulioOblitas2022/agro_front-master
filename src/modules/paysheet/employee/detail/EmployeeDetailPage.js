import React from 'react'
import BarButtonFw, { BUTTON_DEFAULT } from '../../../../framework/bar-button/BarButtonFw';
import FormFw from '../../../../framework/form/FormFw';
import { PAGE_MODE } from '../../../../constants/constants'; 
import FormGridFw, { FORM_GRID_MODE } from "../../../../framework/form-grid/FormGridFw";
import { B_SELECT_CONSTANTS } from "../../../shared/constantes/BSelectConstants";
import { BasePage } from '../../../../framework/pages/BasePage';
import { getEmployeeDetailMode } from './getEmployeeDetailMode';
/* import { getConfigEquipmentAdmin } from './getConfigEquipmentAdmin'; */

export class EmployeeDetailPage extends BasePage {

    createPagePropertiesBase(pageApi) {
        return {
            title: 'BANDEJA DE EMPLEADOS',
            mode: getEmployeeDetailMode
        };
    }

    renderPage(pageApi) {
        /*  const getGrid = () => pageApi.getComponent('grid'); */
        const getForm = () => pageApi.getComponent('form');
        const getFormGridConfig = () => pageApi.getComponent("formGrid.grid");

        const formConfig = {
            load: {
                queryId:426,
                params:{ employeeId: pageApi.getParamId()}
            },
            save: {
                queryId: 422,
                fnOk:(resp) => {
                    const obj = resp.dataObject
                    console.log(resp)
                    getFormGridConfig().save({ params: { employeeId: obj.employeeId } })
                },
                postLink: (resp, values) => '/paysheet/employee/employee-admin'
            },
            update: {
                queryId: 427,
                postLink: (resp, values) => '/paysheet/employee/employee-admin'
            },
            fields: [
                {
                    label: "REGISTRO DE EMPLEADO",
                    fields: [
                        { type: 'select', name: 'documentTypePersonId', label: 'Tipo Documento', column: 6, load: { queryId: 220 } },
                        { name: 'documentPerson', label: 'Nro. Documento', column: 6 },
                        { name: 'lastName1', label: 'Apellido Paterno', column: 6 },
                        { name: 'lastName2', label: 'Apellido Materno', column: 6 },
                        { name: 'name', label: 'Nombre', column: 6 },
                        { ...B_SELECT_CONSTANTS.SEX },
                        { name: 'direction', label: 'Direccion', column: 12 },
                        { type: 'select', name: 'departamentoId', label: 'Dep. Referencia', column: 6, load: { queryId: 78 } },
                        {
                            type: 'select', name: 'provinciaId', label: 'Prov. Residencia', column: 6, load: { queryId: 79 },
                            parents: [
                                { name: 'departamentoId', paramName: 'departamentoId', required: true }
                            ]
                        },
                        {
                            type: 'select', name: 'distritoId', label: 'Dis. Referencia', column: 12, load: { queryId: 80 },
                            parents: [
                                { name: 'provinciaId', paramName: 'provinciaId', required: true }
                            ]
                        },
                        { name: 'phone', label: 'Nro. Teléfono', column: 6 },
                        { name: 'email', label: 'Correo Electrónico', column: 6 },
                        { type: 'select', name: 'departamentoIdBirth', label: 'Dep. Nacimiento',column: 6,load: { queryId: 78 } },
                        { type: 'select', name: 'provinciaIdBirth', label: 'Prov. Nacimiento', column: 6, load: { queryId: 79 },
                            parents: [
                                { name: 'departamentoIdBirth', paramName: 'departamentoId', required: true }
                            ] },
                        { type: 'select', name: 'distritoIdBirth', label: 'Dis. Nacimiento', column: 6, load: { queryId: 80 },
                            parents: [
                                { name: 'provinciaIdBirth', paramName: 'provinciaId', required: true }
                            ] },
                        { type: 'date', name: 'birthDate', label: 'Fecha Nacimiento', column: 6 },
                        { name: 'text', label: 'Fotografia', column: 6 },
                    ]
                },
                {
                    label: "DATOS LABORALES",
                    fields: [
                        { name: 'workAddress', label: 'Dirección', column: 6 },
                        { type: "number", name: 'paymentAmount', label: 'Imp. Hrs. Normales', column: 6 },
                        { type: "number", name: 'extraPayment', label: 'Imp. Hrs. Extras', column: 6 },
                        {  name: 'phoneEsalud', label: 'Nro. Esalud', column: 6 },
                        { type: "date", name: 'admissionDate', label: 'Fch. Ingreso', column: 6 },
                        { type: "date", name: 'departureDate', label: 'Fch. Baja', column: 6 },
                        { type: 'switch', name: 'checkAsig', label: 'Asignación Familiar', column: 2, value: true },
                        { type: 'switch', name: 'checkDes', label: 'Destajo', column: 2, value: true },
                        { type: 'switch', name: 'checkPer', label: 'Personal destacado', column: 2, value: true },
                    ]
                }

            ],
            onComponentLoad: (formApi, pageApi) => {
                if (pageApi.getPageMode() !== PAGE_MODE.NEW)
                    formApi.load();
            }
        };

        const formGridConfig = {
            title: 'PERIODO LABORAL',
            mode: FORM_GRID_MODE.FORM_ADD,
            form: {
                fields: [
                    { name: 'typeContract', label: 'Tipo Extensión Contrato', column: 4, },
                    { type: 'date', name: 'admissionDate', label: 'Fch. Inicio/Reinicia', column: 4, },
                    { type: 'date', name: 'departureDate', label: 'Tipo Fin/Suspensión', column: 4, },
                ]
            },
            grid: {
                showIndex: true,
                autoLoad: true,
                pagination: {
                    server: true
                },
                rowId: 'employeeWorkId',
                load: {
                    validation: () => pageApi.getParamId() != null,
                    queryId: 428,
                    params: { employeeId: pageApi.getParamId() },
                    hideMessage: true,
                    hideMessageError: true,
                },
                save: {
                    queryId: 425 ,
                    hideMessage: true,
                    hideMessageError: true,
                },
                fields: [
                    { name: '', label: 'Nro.Categoría' },
                    { name: 'typeContract', label: 'Tipo Extensión Contrato'},
                    { name: 'admissionDate', label: 'Fch.Inicio/Reinicia', },
                    { name: 'departureDate', label: 'Fch.Fin/Suspensión', },
                ],
            }
        };



        const barButtonconfig = {
            fields: [
                { ...BUTTON_DEFAULT.RETURN, link: "/paysheet/employee/employee-admin" },
                {
                    ...BUTTON_DEFAULT.SAVE, onClick: () => {
                        getForm().save()
                    }
                },
                {...BUTTON_DEFAULT.UPDATE, onClick: () => {
                    getForm().update()
                    getFormGridConfig().save({ params: { employeeId: pageApi.getParamId() } })
                } },
            ]
        };


        return (
            <>
                <FormFw
                    name="form"
                    getParentApi={pageApi.getApi}
                    config={formConfig} />
                <FormGridFw
                    name="formGrid"
                    getParentApi={pageApi.getApi}
                    config={formGridConfig}
                />
                <BarButtonFw
                    name="bar"
                    getParentApi={pageApi.getApi}
                    config={barButtonconfig} />
            </>
        )
    }
}