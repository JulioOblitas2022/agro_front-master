import React from 'react'
import BarButtonFw, { BUTTON_DEFAULT } from '../../../../framework/bar-button/BarButtonFw';
import FormFw from '../../../../framework/form/FormFw';
import GridFw, {COLUMN_DEFAULT} from '../../../../framework/grid/GridFw';
import { B_SELECT_CONSTANTS } from "../../../shared/constantes/BSelectConstants";
import { BasePage } from '../../../../framework/pages/BasePage';
/* import { getConfigEquipmentAdmin } from './getConfigEquipmentAdmin'; */

export class EmployeeAdminPage extends BasePage {

    createPagePropertiesBase(pageApi) {
        return {
            title: 'BANDEJA DE EMPLEADOS',
            barHeader: {
                fields:[
                    {...BUTTON_DEFAULT.NEW, label:'Nuevo', link: '/paysheet/employee/employee/new'}
                ]
            }
        };
    }

    renderPage(pageApi) {
        const getGrid = () => pageApi.getComponent('grid');
        const getForm = () => pageApi.getComponent('form');

        const formConfig = {
            title: 'Filtros de Búsqueda',
            fields: [
                { name: 'documentPerson', label: 'Nro. DNI', column: 4 },
                { type: 'select', name: 'TIPO_SERVICIO', label: 'Empleado', search: true, column: 4, load: { queryId: 70 } },
                { type: 'date', name: 'ESTADO', label: 'Fecha Emisión', column: 4, load: { queryId: 27 } },
                { name: 'date', label: 'Fecha Entrega', column: 4 },
                {...B_SELECT_CONSTANTS.ACTIVE, column: 4},
            ]
        };

        const gridConfig = {
            showIndex: true,
            autoLoad: true,
            pagination: { server: true },
            load: {
                queryId: 423,
                orderBy: "1",
                hideMessage: true,
                hideMessageError: true,
            },
            fields: [
                { name: 'nombre', label: 'Apellido y Nombres' },
                { name: 'documentPerson', label: 'Nro. DNI' },
                { name: 'documentTypePersonId', label: 'Tipo Documento' , },
                { name: 'admissionDate', label: 'Fch.Ingreso', },
                { name: 'departureDate', label: 'Fch.Cese' ,},
                { name: 'birthDate', label: 'Fch.Nac.' , },
                { ...COLUMN_DEFAULT.EDIT, label: 'Opciones',  colSpan: 2,
                link: (value, row, index) => `/paysheet/employee/employee/${row.employeeId}/update`
                },
                {
                    ...COLUMN_DEFAULT.DELETE,
                    process: {
                        fnOk: (result) => getGrid().load({ params: getForm().getData() }),
                        filter: (value, row, index) => ({ queryId: 424, params: { employeeId: row.employeeId } })
                    }
                },

            ]
        };


        const barButtonconfig = {
            fields: [
                { ...BUTTON_DEFAULT.SEARCH, onClick: () => getGrid().load({ params: getForm().getData() }) },
                { ...BUTTON_DEFAULT.CLEAR, onClick: () => getForm().reset() },
            ]
        };
      

        return (
            <>
               <FormFw
                    name="form"
                    getParentApi={pageApi.getApi}
                    config={formConfig} />

                <BarButtonFw
                    name="bar"
                    getParentApi={pageApi.getApi}
                    config={barButtonconfig} />
                <GridFw
                    name="grid"
                    getParentApi={pageApi.getApi}
                    config={gridConfig}
                />
            </>
        )
    }
}