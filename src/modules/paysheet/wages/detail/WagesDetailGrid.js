import React, { Component } from 'react'
import { Alert, Col, DatePicker, Divider, Form, Input, message, Modal, Row, Select, Table, Tag } from 'antd';
import { EnvConstants } from '../../../../EnvConstants';
import { LoadUtil } from '../../../../framework/util/LoadUtil';
import * as moment from 'moment';
import 'moment-duration-format';
import WagesDetailPaysheet from './WagesDetailPaysheet'

export default class WagesDetailGrid extends Component {
  state = {
    columns: [],
    data: [],
    dataSueldos: [],
    dataHoras: [],
    dataFechas: [],
    modalVisible: false,
    dataRow: {}
  }

  showModal = () => {
    this.setState({
      modalVisible: true
    });
  };

  cancelModal = () => {
    this.setState({
      modalVisible: false
    });
  };

  diasEntreFechas = (desde, hasta) => {
    const dia_actual = desde;
    const fechas = [];
    while (dia_actual.isSameOrBefore(hasta)) {
      fechas.push(dia_actual.format('DD-MM-YYYY'));
      dia_actual.add(1, 'days');
    }
    return fechas;
  };


  capturarData = (responsable, fechaInicio, fechaFin, radio) => {
    //mandar bd
    const fechaInicioFormateada = moment(fechaInicio, 'DD-MM-YYYY').format('YYYY-MM-DD');
    const fechaFinFormateada = moment(fechaFin, 'DD-MM-YYYY').format('YYYY-MM-DD');
    //obtener dias entre el rango
    const desde = moment(fechaInicioFormateada);
    const hasta = moment(fechaFinFormateada);
    const results = this.diasEntreFechas(desde, hasta);
    //column inicial
    let columnaInicial = [
      {
        title: 'Nro. DNI',
        dataIndex: 'dni',
        key: 'dni',
      },
      {
        title: 'Colaborador',
        dataIndex: 'colaborador',
        key: 'colaborador',
      },
    ]
    results.map((data, index) => {
      columnaInicial = [
        ...columnaInicial,
        {
          title: `Día ${index + 1} ${data}`,
          dataIndex: `${data}`,
          key: `${data}`,
        }
      ]
    })
    columnaInicial = [
      ...columnaInicial,
      {
        title: 'Total',
        dataIndex: 'total',
        key: 'total'
      }
    ]
    //query
    LoadUtil.loadByQueryId({
      url: EnvConstants.GET_URL_READ(),
      queryId: 525,
      reqParams: {
        params: { applicant: responsable, dateStart: fechaInicioFormateada, dateEnd: fechaFinFormateada }
      },
      fnOk: (resp) => {
        const obj = resp.dataList
        //horas trabajadas
        const arregloEmpleados = []
        obj.map(data => {
          if (arregloEmpleados.length > 0) {
            if (arregloEmpleados.some(item => item.idEmpleado === data.namePeople)) {
              arregloEmpleados.map((item, index) => {
                if (item.idEmpleado === data.namePeople) {
                  if (item.data.some(fecha => Object.keys(fecha) == data.dateRegisterFormat)) {
                    item.data.map((fecha, fechaIndex) => {
                      if (Object.keys(fecha) == data.dateRegisterFormat) {
                        let valor = arregloEmpleados[index].data[fechaIndex][Object.keys(fecha)]
                        let valorFormat = moment.duration(valor)
                        let hora = moment.duration(data.totalHour)
                        let horaCalculada = moment.duration(valorFormat).add(hora)
                        let horaCalculadaFormat = horaCalculada.format("HH:mm")//libreria
                        arregloEmpleados[index].data[fechaIndex][Object.keys(fecha)] = horaCalculadaFormat
                      }
                    })
                  } else {
                    arregloEmpleados[index].data.push(
                      {
                        [data.dateRegisterFormat]: data.totalHour
                      }
                    )
                  }

                }
              })
            } else {
              arregloEmpleados.push({
                dni: data.documentPerson,
                empleado: data.name,
                idEmpleado: data.namePeople,
                sueldoHora: data.payment,
                data: [
                  {
                    [data.dateRegisterFormat]: data.totalHour
                  }
                ]
              })
            }
          } else {
            arregloEmpleados.push(
              {
                dni: data.documentPerson,
                empleado: data.name,
                idEmpleado: data.namePeople,
                sueldoHora: data.payment,
                data: [
                  {
                    [data.dateRegisterFormat]: data.totalHour
                  }
                ]
              }
            )
          }
        })

        //cargar data que irá al state
        let dataInicial = []

        arregloEmpleados.map((data, index) => {
          let valor = "00:00"
          let obj = {
            key: index + 1,
            dni: data.dni,
            colaborador: data.empleado,
            idColaborador: data.idEmpleado,
            sueldoHora: data.sueldoHora
          }
          data.data.map((item, index) => {
            let valorFormat = moment.duration(valor)
            let horaFormat = moment.duration(item[Object.keys(item)])
            let horaCalculada = moment.duration(valorFormat).add(horaFormat)
            valor = horaCalculada.format("HH:mm")//libreria
            obj = {
              ...obj,
              [Object.keys(item)]: item[Object.keys(item)]
            }
          })
          obj = {
            ...obj,
            total: valor
          }
          dataInicial = [
            ...dataInicial,
            obj
          ]
        })

        //agregar tiempo 0:00 si está vacio
        results.map(data => {
          dataInicial.map(item => {
            if (!item[data]) {
              item[data] = '00:00'
            }
          })
        })

        //sueldo
        let dataSueldos = []
        dataInicial.map((data, index) => {
          let valor = 0
          let obj = {
            key: data.key,
            dni: data.dni,
            colaborador: data.colaborador,
            sueldoHora: data.sueldoHora,
            idColaborador: data.idColaborador
          }
          results.map(item => {
            let decimal = parseFloat(data[item].substr(-2)) / 60 //obtener los dos ultimos valores
            let hora = parseFloat(data[item].substr(0, 2))
            let total = decimal + hora
            valor = valor + total
            obj = {
              ...obj,
              [item]: total.toFixed(2)
            }
          })
          obj = {
            ...obj,
            total: valor.toFixed(2)
          }
          dataSueldos = [
            ...dataSueldos,
            obj
          ]
        })

        //setear al state
        this.setState({
          columns: columnaInicial,
          data: radio === 1 ? dataSueldos : dataInicial, //mandar al grid dependiendo del radio
          dataHoras: dataInicial,
          dataSueldos: dataSueldos,
          dataFechas: results
        })
      }
    });
  }

  //funcion cambiar de data dependiendo del radio
  cambiarData = (radio) => {
    this.setState({
      data: radio === 1 ? this.state.dataSueldos : this.state.dataHoras
    })
  }

  mandarDataGuardar = () => {
    let dataGuardar = []
    this.state.dataHoras.map(data => {
      this.state.dataFechas.map(fecha => {
        let obj = {
          idEmployee: data.idColaborador,
          dateWorking: fecha,
          hourWorking: data[fecha],
          totalHour: data.total
        }
        this.state.dataSueldos.map(sueldo => {
          if (sueldo.idColaborador === data.idColaborador) {
            obj = {
              ...obj,
              paymentWorking: sueldo[fecha],
              totalPayment: sueldo.total
            }
          }
        })
        dataGuardar = [
          ...dataGuardar,
          obj
        ]
      })
    })

    return dataGuardar
  }

  componentDidMount() {
    //console.log(this.props.params) 
    //cargar data
    LoadUtil.loadByQueryId({
      url: EnvConstants.GET_URL_READ(),     
      queryId: 528,
      reqParams: {
        params: { wageId: this.props.params }
      },
      fnOk: (resp) => {
        const obj = resp.dataList
        //horas trabajadas
        const arregloEmpleados = []
        obj.map(data => {
          if (arregloEmpleados.length > 0) {
            if (arregloEmpleados.some(item => item.idEmpleado === data.idEmployee)) {
              arregloEmpleados.map((item, index) => {
                if (item.idEmpleado === data.idEmployee) {
                  arregloEmpleados[index].dataHoras.push(
                    {
                      [data.dateWorking]: data.hourWorking,
                    }
                  )
                  arregloEmpleados[index].dataSueldos.push(
                    {
                      [data.dateWorking]: data.paymentWorking,
                    }
                  )
                }
              })
            } else {
              arregloEmpleados.push(
                {
                  dni: data.documentPerson,
                  empleado: data.name,
                  idEmpleado: data.idEmployee,
                  totalHoras: data.totalHour,
                  totalSueldo: data.totalPayment,
                  dataHoras: [
                    {
                      [data.dateWorking]: data.hourWorking
                    }
                  ],
                  dataSueldos: [
                    {
                      [data.dateWorking]: data.paymentWorking
                    }
                  ]

                }
              )
            }
          } else {
            arregloEmpleados.push(
              {
                dni: data.documentPerson,
                empleado: data.name,
                idEmpleado: data.idEmployee,
                totalHoras: data.totalHour,
                totalSueldo: data.totalPayment,
                dataHoras: [
                  {
                    [data.dateWorking]: data.hourWorking
                  }
                ],
                dataSueldos: [
                  {
                    [data.dateWorking]: data.paymentWorking
                  }
                ]
              }
            )
          }
        })

        //cargar data que irá al state horas y cargar data de fechas para la columna
        let dataInicial = []
        let fecha = []
        arregloEmpleados.map((data, index) => {
          let obj = {
            key: index + 1,
            dni: data.dni,
            colaborador: data.empleado,
            idColaborador: data.idEmpleado,
            total: data.totalHoras
          }
          data.dataHoras.map(item => {
            obj = {
              ...obj,
              [Object.keys(item)]: item[Object.keys(item)]
            }
            if (index === 0) {
              fecha.push(Object.keys(item).toString())
            }
          })
          dataInicial = [
            ...dataInicial,
            obj
          ]
        })
        console.log(dataInicial)

        //cargar data que irá al state horas y cargar data de fechas para la columna
        let dataSueldos = []
        arregloEmpleados.map((data, index) => {
          let obj = {
            key: index + 1,
            dni: data.dni,
            colaborador: data.empleado,
            idColaborador: data.idEmpleado,
            total: data.totalSueldo.toFixed(2)
          }
          data.dataSueldos.map(item => {
            obj = {
              ...obj,
              [Object.keys(item)]: item[Object.keys(item)].toFixed(2)
            }
          })
          dataSueldos = [
            ...dataSueldos,
            obj
          ]
        })
        console.log(dataSueldos)

        //column inicial
        let columnaInicial = [
          {
            title: 'Nro. DNI',
            dataIndex: 'dni',
            key: 'dni',
          },
          {
            title: 'Colaborador',
            dataIndex: 'colaborador',
            key: 'colaborador',
            render: (value) => <a onClick={() => this.showModal()}>{value}</a>
          },
        ]
        fecha.map((data, index) => {
          columnaInicial = [
            ...columnaInicial,
            {
              title: `Día ${index + 1} ${data}`,
              dataIndex: `${data}`,
              key: `${data}`,
            }
          ]
        })
        columnaInicial = [
          ...columnaInicial,
          {
            title: 'Total',
            dataIndex: 'total',
            key: 'total'
          }
        ]

        this.setState({
          columns: columnaInicial,
          dataFechas: fecha,
          dataHoras: dataInicial,
          dataSueldos,
          data: dataSueldos
        })
        /* console.log(dataInicial)
        console.log(columnaInicial) */
      }
    })
  }
  render() {

    return (
      <>
        <div className="container-fw">
          <Row className="header-bar-fw">
            <Col span={12} className="header-bar-fw-title"  >
              PAGOS DE JORNADA
            </Col>
          </Row>
          <div className="container-fw-body">
            <Table columns={this.state.columns} dataSource={this.state.data} scroll={{ x: 'auto' }}
              onRow={(record, rowIndex) => {
                return {
                  onClick: event => {
                    this.setState({
                      dataRow: record
                    })
                  }, // click row
                  //onDoubleClick: event => { }, // double click row
                  //onContextMenu: event => { }, // right button click row
                  //onMouseEnter: event => { }, // mouse enter row
                  //onMouseLeave: event => { }, // mouse leave row
                };
              }} />
          </div>

        </div>
        <Modal title="Detalle Colaborador" visible={this.state.modalVisible} onCancel={this.cancelModal} width={1000} footer={null}>
          <WagesDetailPaysheet dataRow={this.state.dataRow} dataForm={this.props.paramsForm} />
        </Modal>
      </>
    )
  }
}
