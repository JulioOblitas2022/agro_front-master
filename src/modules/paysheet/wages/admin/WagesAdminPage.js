import React from 'react'
import BarButtonFw, { BUTTON_DEFAULT } from '../../../../framework/bar-button/BarButtonFw';
import FormFw from '../../../../framework/form/FormFw';
import GridFw, {COLUMN_DEFAULT} from '../../../../framework/grid/GridFw';
import { B_SELECT_CONSTANTS } from "../../../shared/constantes/BSelectConstants";
import { BasePage } from '../../../../framework/pages/BasePage';
/* import { getConfigEquipmentAdmin } from './getConfigEquipmentAdmin'; */

export class WagesAdminPage extends BasePage {

    createPagePropertiesBase(pageApi) {
        return {
            title: 'BANDEJA DE PLANILLA JORNALES',
            barHeader: {
                fields:[
                    {...BUTTON_DEFAULT.NEW, label:'Nuevo', link: '/paysheet/wages/wages/new'}
                ]
            }
        };
    }

    renderPage(pageApi) {
        const getGrid = () => pageApi.getComponent('grid');
        const getForm = () => pageApi.getComponent('form');

        const formConfig = {
            title: 'Filtros de Búsqueda',
            fields: [
                { name: 'documentPerson', label: 'Nro. Documento', column: 4 },
                { type: 'select', name: 'TIPO_SERVICIO', label: 'Nro. Semana', search: true, column: 4, load: { queryId: 70 } },
                { type: 'select', name: 'applicant', label: 'Responsable', search: true, column: 4, load: { queryId: 309} },
                { type: 'date', name: 'ESTADO', label: 'Fecha Emisión', column: 4},
                { name: 'date', label: 'Fecha Entrega', column: 4 },
                { type: 'select', name: 'local', label: 'Local', search: true, column: 4, load: { queryId: 198 } },
            ]
        };

        const gridConfig = {
            showIndex: true,
            autoLoad: true,
            pagination: { server: true },
            load: {
                queryId: 493,
                orderBy: "1",
                hideMessage: true,
                hideMessageError: true,
            },
            fields: [
                { name: 'dateRegister', label: 'Fech. Registro' },
                { name: 'documentPerson', label: 'Nro. Semana' },
                { name: 'documentTypePersonId', label: 'T.D' , },
                { name: 'numProcess', label: 'Nro.Documento', },
                { name: 'dateStart', label: 'Fch.Inicio' ,},
                { name: 'dateEnd', label: 'Fch.Termino' , },
                { name: 'applicant', label: 'Responsable' , },
                { name: 'nameLocal', label: 'Local' , },
                { name: 'birthDate', label: 'Importe' , },
                { ...COLUMN_DEFAULT.VIEW, label: 'Opciones', colSpan:2,
                link: (value, row, index) => `/paysheet/wages/wages/${row.wageId}`
                },
                {
                    ...COLUMN_DEFAULT.DELETE, colSpan: 0,
                    process: {
                        fnOk: (result) => getGrid().load({ params: getForm().getData() }),
                        filter: (value, row, index) => ({ queryId: 498, params: { wageId: row.wageId } })
                    }
                },

            ]
        };

        const barButtonconfig = {
            fields: [
                { ...BUTTON_DEFAULT.SEARCH, onClick: () => getGrid().load({ params: getForm().getData() }) },
                { ...BUTTON_DEFAULT.CLEAR, onClick: () => getForm().reset() },
            ]
        };
      
        return (
            <>
               <FormFw
                    name="form"
                    getParentApi={pageApi.getApi}
                    config={formConfig} />

                <BarButtonFw
                    name="bar"
                    getParentApi={pageApi.getApi}
                    config={barButtonconfig} />
                <GridFw
                    name="grid"
                    getParentApi={pageApi.getApi}
                    config={gridConfig}
                />
            </>
        )
    }
}