import React from 'react'
import { Route, Switch, useRouteMatch } from 'react-router';
import { WagesAdminPage } from './admin/WagesAdminPage';
import { WagesDetailPage } from './detail/WagesDetailPage';

const UnifiedWagesRoutes = () => {
    const { path } = useRouteMatch();
    return (
        <Switch>
            <Route exact path={`${path}/wages-admin`} component={WagesAdminPage} />
            <Route path={`${path}/wages`} component={WagesDetailPage} />
            <Route path={`${path}/wages/:id`} component={WagesDetailPage} /> 
        </Switch>
    )
}

export default UnifiedWagesRoutes;