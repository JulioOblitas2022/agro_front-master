import React from 'react'
import { PAGE_MODE, DATE_FORMAT } from '../../../../constants/constants';
import GridFw, { COLUMN_DEFAULT } from "../../../../framework/grid/GridFw";
import { BasePage } from '../../../../framework/pages/BasePage';
import { EnvConstants } from '../../../../EnvConstants';
import { LoadUtil } from '../../../../framework/util/LoadUtil';

export default class WagesDetailPaysheet extends BasePage {

    createPagePropertiesBase(pageApi) {
        return {
           /*  title: 'BANDEJA DE EMPLEADOS',
            mode: getWagesDetailMode */
        };
    }

    renderPage(pageApi) {
        const getGrid = () => pageApi.getComponent('grid');
        console.log(this.props, 'props')
        //inicio
        if(getGrid()){
            getGrid().load({ params: {namePeople: this.props.dataRow.idColaborador , taskPeopleId: pageApi.getParamId()} })
        }
        /* getGrid().load({ params: {namePeople: this.props.dataRow.idColaborador} }) */

        const gridConfig = {
            /* title: 'PLANILLAS RELACIONADAS', */
            scroll:{ x: 'auto' },
            collapsible: true,
            showIndex: true,
            autoLoad: true,
            pagination: { server: true },
            load: {
                queryId: 545,
                params: { namePeople: this.props.dataRow.idColaborador, taskPeopleId: pageApi.getParamId() },
                hideMessage: true,
                hideMessageError: true,
            },
            save: {
                queryId: 457,
                hideMessage: true,
                hideMessageError: true,
            },
            fields: [
                { name: 'name', label: 'Apellidos y Nombres'},
                { name: 'task', label: 'Producto'},
                { name: 'task', label: 'Tarea'},
                { name: 'dateRegisterFormat', label: 'Fecha Trabajo'},
                { name: 'task', label: 'N° Sol. Tarea'},
                { name: 'task', label: 'Tipo'},
                { name: 'startTime', label: 'Hora Inicio'},
                { name: 'endTime', label: 'Hora Fin'},
                { name: 'quantity', label: 'Can.Prod.'},
                { name: 'totalHour', label: 'Tiempo'},
                { name: 'totalHour', label: 'N° Horas'},
                { name: 'task', label: 'Importe'},
            ]
        };


        return (
            <>

                <GridFw
                    name="grid"
                    getParentApi={pageApi.getApi}
                    config={gridConfig}
                />

            </>
        )
    }
}