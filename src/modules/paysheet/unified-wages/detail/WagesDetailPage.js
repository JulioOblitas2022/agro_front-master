import React from 'react'
import BarButtonFw, { BUTTON_DEFAULT } from '../../../../framework/bar-button/BarButtonFw';
import FormFw from '../../../../framework/form/FormFw';
import { PAGE_MODE, DATE_FORMAT } from '../../../../constants/constants';
import GridFw, { COLUMN_DEFAULT } from "../../../../framework/grid/GridFw";
import { BasePage } from '../../../../framework/pages/BasePage';
import { getWagesDetailMode } from './getWagesDetailMode';
import WagesDetailGrid from './WagesDetailGrid'
import { EnvConstants } from '../../../../EnvConstants';
import { LoadUtil } from '../../../../framework/util/LoadUtil';

export class WagesDetailPage extends BasePage {

    //ref para el grid
    grid = React.createRef()

    capturarData = (responsable, fechaInicio, fechaFin, radio) => {
        this.grid.current.capturarData(responsable, fechaInicio, fechaFin, radio)
    }

    createPagePropertiesBase(pageApi) {
        return {
            title: 'BANDEJA DE EMPLEADOS',
            mode: getWagesDetailMode
        };
    }

    renderPage(pageApi) {
        const getGrid = () => pageApi.getComponent('grid');
        const getForm = () => pageApi.getComponent('form');
        const getFormGridConfig = () => pageApi.getComponent("formGrid.grid");

        const fnNew = () => {
            getGrid().addItem({}, true);
        }

        const fnDelete = (value, row, index) => {
            getGrid().deleteItem(row.key);
        }

        const formConfig = {
            load: {
                queryId: 492,
                params: { unifiedWageId: pageApi.getParamId() }
            },
            save: {
                queryId: 489,
                fnOk: (resp) => {
                    const obj = resp.dataObject

                    getGrid().save({ params: { unifiedWageId: obj.unifiedWageId } })
                    const dataGrid = this.grid.current.mandarDataGuardar()
                    dataGrid.map(data => {
                        data.unifiedWageId = obj.unifiedWageId
                        LoadUtil.loadByQueryId({
                            url: EnvConstants.GET_FW_FORM_URL_SAVE_BASE(),
                            queryId: 529,
                            params: data
                        })
                    })
                },
                postLink: (resp, values) => '/paysheet/unified-wages/wages-admin'
            },
            update: {
                queryId: 497,
                postLink: (resp, values) => '/paysheet/unified-wages/wages-admin'
            },
            fields: [
                {
                    label: "DATOS DE LA JORNADA",
                    fields: [
                        { name: 'numProcess', label: 'Nro. Proceso', column: 6, load: { queryId: 220 } },
                        { type: 'date', name: 'dateRegister', label: 'Fch. Registro', column: 6 },
                        { type: 'date', name: 'dateStart', label: 'Fch. Inicio', column: 6 },
                        { type: 'date', name: 'dateEnd', label: 'Fch. Termino', column: 6 },
                        { type: 'select', name: 'applicant', label: 'Responsable', column: 6, load: { queryId: 309 }, search: true },
                        /* { type: 'select', name: 'local', label: 'Local', column: 6, load: { queryId: 198}}, */
                        { name: 'observations', label: 'Observaciones', column: 12, row: 2 },
                        {
                            type: 'radio', name: 'radio', value: 1, label: 'Opciones', load: {
                                dataList: [
                                    { label: 'Cálculo de Pagos', value: 1 },
                                    { label: 'Horas Trabajadas', value: 2 },
                                ]
                            }, column: 3, onChange: (value) => {
                                if (this.grid.current.state.data.length > 0) {
                                    this.grid.current.cambiarData(value)
                                }
                            }
                        }
                    ]
                },
            ],
            onComponentLoad: (formApi, pageApi) => {
                if (pageApi.getPageMode() !== PAGE_MODE.NEW)
                    formApi.load();
            }
        };

        const gridBarHeaderButtonconfig = {
            fields: [
                { ...BUTTON_DEFAULT.NEW, label: 'Agregar Planilla', onClick: fnNew }
            ]
        }

        const gridConfig = {
            title: 'PLANILLAS RELACIONADAS',
            showIndex: true,
            autoLoad: true,
            pagination: { server: true },
            rowId: 'unifiedWagesDetailRelatedId',
            barHeader: gridBarHeaderButtonconfig,
            load: {
                validation: () => pageApi.getParamId() != null,
                queryId: 548,
                params: { unifiedWageId: pageApi.getParamId() },
                hideMessage: true,
                hideMessageError: true,
            },
            save: {
                queryId: 546 ,
                hideMessage: true,
                hideMessageError: true,
            },
            fields: [
                { type:'select', name: 'wageId', label: 'Planilla', labelName:'namePlanilla', allowClear: true, editable: true, load: { queryId: 547 }, search: true },
                { ...COLUMN_DEFAULT.DELETE, label: 'Opciones', onClick: fnDelete, confirm: false }
            ]
        };

        const barButtonconfig = {
            fields: [
                { ...BUTTON_DEFAULT.RETURN, link: "/paysheet/unified-wages/wages-admin" },
                {
                    ...BUTTON_DEFAULT.SAVE, onClick: () => {
                        if (this.grid.current.state.data.length > 0) {
                            getForm().save()
                        } else {
                            alert("no hay data en el grid")
                        }
                    }
                },
                /* {
                    ...BUTTON_DEFAULT.UPDATE, onClick: () => {
                        getForm().update() 
                    }
                }, */
            ]
        };

        const barButtonconfig2 = {
            fields: [
                {
                    ...BUTTON_DEFAULT.SAVE, label: 'Ver Tareas Realizadas', onClick: () => {
                        if (getForm().getData()['dateStart'] && getForm().getData()['dateEnd'] && getForm().getData()['applicant']) {
                            this.capturarData(
                                getForm().getData()['applicant'],
                                getForm().getData()['dateStart'],
                                getForm().getData()['dateEnd'],
                                getForm().getData()['radio'])
                        } else {
                            alert("llenar campos")
                        }
                    }
                },
            ]
        };


        return (
            <>
                <FormFw
                    name="form"
                    getParentApi={pageApi.getApi}
                    config={formConfig} 
                />
                <BarButtonFw
                    name="bar"
                    getParentApi={pageApi.getApi}
                    config={barButtonconfig2} 
                />
                <GridFw 
                    name="grid" 
                    getParentApi={pageApi.getApi} 
                    config={gridConfig} 
                />
                <WagesDetailGrid
                    ref={this.grid}
                    params={pageApi.getParamId()}
                />
                <BarButtonFw
                    name="bar"
                    getParentApi={pageApi.getApi}
                    config={barButtonconfig} 
                />
            </>
        )
    }
}