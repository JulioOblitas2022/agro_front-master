import React, { lazy } from 'react'
import { Route, Switch, useRouteMatch } from 'react-router-dom';

const EmployeeRoutes = lazy(() => import('./employee/EmployeeRoutes'));
const TaskRequestRoutes = lazy(() => import('./task-request/TaskRequestRoutes'));
const WagesRoutes = lazy(() => import('./wages/WagesRoutes'));
const UnifiedWagesRoutes = lazy(() => import('./unified-wages/UnifiedWagesRoutes'));
const BirthdayRoutes = lazy(() => import('./birthday-report/BirthdayRoutes'));

const PaysheetRoutes = () => {
    const { path } = useRouteMatch();
    return (
        <Switch>
            <Route path={`${path}/employee`} component={EmployeeRoutes} />
            <Route path={`${path}/taskRequest`} component={TaskRequestRoutes} />
            <Route path={`${path}/wages`} component={WagesRoutes} />
            <Route path={`${path}/unified-wages`} component={UnifiedWagesRoutes} />
            <Route path={`${path}/birthday-report`} component={BirthdayRoutes} />
        </Switch>
    )
}

export default PaysheetRoutes;
