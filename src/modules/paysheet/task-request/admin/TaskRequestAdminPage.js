import React from 'react'
import BarButtonFw, { BUTTON_DEFAULT } from '../../../../framework/bar-button/BarButtonFw';
import FormFw from '../../../../framework/form/FormFw';
import GridFw, {COLUMN_DEFAULT} from '../../../../framework/grid/GridFw';
import { B_SELECT_CONSTANTS } from "../../../shared/constantes/BSelectConstants";
import { BasePage } from '../../../../framework/pages/BasePage';

export class TaskRequestAdminPage extends BasePage {

    createPagePropertiesBase(pageApi) {
        return {
            title: 'BANDEJA DE SOLICITUD DE TAREAS',
            barHeader: {
                fields:[
                    {...BUTTON_DEFAULT.NEW, label:'Nuevo', link: '/paysheet/taskRequest/taskRequest/new'}
                ]
            }
        };
    }

    renderPage(pageApi) {
        const getGrid = () => pageApi.getComponent('grid');
        const getForm = () => pageApi.getComponent('form');

//        type: 'select', name: 'recipeId', label: 'Receta', column: 4, load: { queryId: 481 }, search: true,
        const formConfig = {
            title: 'Filtros de Búsqueda',
            fields: [
                { name: 'productionRecordId', label: 'Nro. Producción', column: 4 },                               
                { type: 'select', name: 'productId', label: 'Producto', search: true, column: 4, load: { queryId: 480 } },                                               
                { type: 'select', name: 'recipeId', label: 'Cod. Receta', search: true, column: 4, load: { queryId: 481 }, parents: [
                    { name: 'productId', paramName: 'productId', required: true }
                ] },
                { type: 'date', name: 'fch1', label: 'Fecha Emisión', column: 4 },
                { type: 'date', name:'fch2', label: 'Fecha Entrega', column: 4 },
                {...B_SELECT_CONSTANTS.ACTIVE, column: 4},
            ]
        };

        const gridConfig = {
            showIndex: true,
            autoLoad: true,
            pagination: { server: true },
            load: {
                queryId: 453,
                orderBy: "1",
                hideMessage: true,
                hideMessageError: true,
            },
            fields: [
                { name: 'dateRegister', label: 'Fecha Registro' },
                { name: 'document', label: 'Nro.Documento'},
                { name: 'applicant', label: 'Responsable'},
                { name: '', label: 'Local'},
                { name: 'productionRecordId', label: 'Nro.Producción'},
                { name: 'product', label: 'Producto'},
                { name: 'unit', label: 'Unidad Medida'},
                { name: 'birthDate', label: 'Estado'},
                { ...COLUMN_DEFAULT.EDIT, label: 'Opciones', colSpan:2,
                link: (value, row, index) => `/paysheet/taskRequest/taskRequest/${row.taskRequestId}/update`
                },
                {
                    ...COLUMN_DEFAULT.DELETE, colSpan: 0,
                    process: {
                        fnOk: (result) => getGrid().load({ params: getForm().getData() }),
                        filter: (value, row, index) => ({ queryId: 455, params: { taskRequestId: row.taskRequestId } })
                    }
                },

            ]
        };


        const barButtonconfig = {
            fields: [
                { ...BUTTON_DEFAULT.SEARCH, onClick: () => getGrid().load({ params: getForm().getData() }) },
                { ...BUTTON_DEFAULT.CLEAR, onClick: () => getForm().reset() },
            ]
        };
      

        return (
            <>
               <FormFw
                    name="form"
                    getParentApi={pageApi.getApi}
                    config={formConfig} />

                <BarButtonFw
                    name="bar"
                    getParentApi={pageApi.getApi}
                    config={barButtonconfig} />
                <GridFw
                    name="grid"
                    getParentApi={pageApi.getApi}
                    config={gridConfig}
                />
            </>
        )
    }
}