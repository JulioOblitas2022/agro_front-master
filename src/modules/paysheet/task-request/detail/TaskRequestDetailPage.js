import React from 'react'
import BarButtonFw, { BUTTON_DEFAULT } from '../../../../framework/bar-button/BarButtonFw';
import FormFw from '../../../../framework/form/FormFw';
import { PAGE_MODE, DATE_FORMAT } from '../../../../constants/constants';
import GridFw, { COLUMN_DEFAULT } from "../../../../framework/grid/GridFw";
import { BasePage } from '../../../../framework/pages/BasePage';
import { getTaskRequestDetailMode } from './getTaskRequestDetailMode';
import './TaskRequestDetailPage.css'
import * as moment from 'moment';
import 'moment-duration-format';

export class TaskRequestDetailPage extends BasePage {

    createPagePropertiesBase(pageApi) {
        return {
            title: 'REGISTRO DE SOLICITUD DE TAREAS',
            mode: getTaskRequestDetailMode
        };
    }

    renderPage(pageApi) {
        const getGrid = () => pageApi.getComponent('grid');
        const getGrid2 = () => pageApi.getComponent('grid2');
        const getForm = () => pageApi.getComponent('form');

        const fnNew = () => {
            getGrid().addItem({}, true);
        }

        const fnDelete = (value, row, index) => {
            getGrid().deleteItem(row.key);
        }
        const fnNew2 = () => {
            getGrid2().addItem({}, true);
        }

        const fnDelete2 = (value, row, index) => {
            getGrid2().deleteItem(row.key);
        }

        const formConfig = {
            load: {
                queryId: 454,
                params: { taskRequestId: pageApi.getParamId() },
                hideMessage: true,
                hideMessageError: true,
            },
            save: {
                queryId: 451,
                fnOk: (resp) => {
                    const obj = resp.dataObject
                    console.log(resp)
                    getGrid().save({ params: { taskRequestId: obj.taskRequestId } })
                    getGrid2().save({ params: { taskRequestId: obj.taskRequestId } })
                },
                postLink: (resp, values) => '/paysheet/taskRequest/taskRequest-admin'
            },
            update: {
                queryId: 456,
                postLink: (resp, values) => '/paysheet/taskRequest/taskRequest-admin'
            },

            fields: [
                {
                    label: "DATOS DE LA SOLICITUD",
                    fields: [
                        //{ type: 'select', name: 'documentType', label: 'Tipo Documento', value: 62, column: 4, load: { queryId: 195 }, readOnly: true },
                        { type: 'select', name: 'documentType', label: 'Tipo Documento', value: "SOLICITUD DE TAREAS", column: 4, load: { queryId: 195 } },
                        { type: 'date', name: 'dateRegister', label: 'Fecha Registro', column: 4 },
                        { name: 'document', label: 'Nro. Documento', column: 4 },
                        { type: 'select', name: 'applicant', label: 'Solicitante', column: 12, load: { queryId: 309 }, search: true },
                    ]
                },
                {
                    label: "DATOS DE LA PRODUCCIÓN",

                    fields: [
                        {
                            type: 'select', name: 'productionRecordId', label: 'Nº Parte Producción', column: 6, load: { queryId: 501 }, search: true,
                            onChange: (value, values) => {
                                let productionQuantity = getForm().getFieldApi('productionRecordId').getSelectedItem()?.productionQuantity;
                                let productionDate = getForm().getFieldApi('productionRecordId').getSelectedItem()?.productionDate;
                                let product = getForm().getFieldApi('productionRecordId').getSelectedItem()?.product;
                                let unitMeasure = getForm().getFieldApi('productionRecordId').getSelectedItem()?.unitMeasure;
                                let recipe = getForm().getFieldApi('productionRecordId').getSelectedItem()?.recipe;
                                let productionOrderNumber = getForm().getFieldApi('productionRecordId').getSelectedItem()?.productionOrderNumber;
                                let productionOrderDate = getForm().getFieldApi('productionRecordId').getSelectedItem()?.productionOrderDate;

                                getForm()?.setFieldData('quantityProduct', productionQuantity);
                                getForm()?.setFieldData('dateProduction', productionDate);
                                getForm()?.setFieldData('product', product);
                                getForm()?.setFieldData('unit', unitMeasure);
                                getForm()?.setFieldData('recipe', recipe);
                                getForm()?.setFieldData('productOrder', productionOrderNumber);
                                getForm()?.setFieldData('dateProductOrder', productionOrderDate);
                            }
                        },
                        { type: 'select', name: 'productionType', label: 'Tipo Busqueda Prod.', column: 4, load: { queryId: 452 } },
                        { name: 'product', label: 'Producto', column: 12, readOnly: true },
                        { type: 'select', name: 'unit', label: 'Unidad Medidad', column: 4, load: { queryId: 192 }, readOnly: true },
                        { name: 'quantityProduct', label: 'Cantidad a Producir', column: 4, readOnly: true },
                        { type: 'date', name: 'dateProduction', label: 'Fecha Producción', column: 4, readOnly: true, format: 'DD/MM/YYYY', inputFormat: DATE_FORMAT.YYYYMMDD, },
                        { name: 'recipe', label: 'Receta', column: 4, readOnly: true },
                        { name: 'productOrder', label: 'Orden Producción', column: 4, readOnly: true },
                        { type: 'date', name: 'dateProductOrder', label: 'Fecha Orden Prod.', column: 4, readOnly: true, format: 'DD/MM/YYYY', inputFormat: DATE_FORMAT.YYYYMMDD, },
                        { type: 'textarea', name: 'observations', label: 'Observaciones', column: 12, row: 4 },
                    ]
                }
            ],
            onComponentLoad: (formApi, pageApi) => {
                if (pageApi.getPageMode() !== PAGE_MODE.NEW)
                    formApi.load();
            }
        };

        const gridBarHeaderButtonconfig = {
            fields: [
                { ...BUTTON_DEFAULT.NEW, label: 'Agregar Tarea', onClick: fnNew }
            ]
        }

        const gridConfig = {
            title: 'TAREAS DISPONIBLES',
            showIndex: true,
            autoLoad: true,
            pagination: { server: true },
            rowId: 'taskId',
            barHeader: gridBarHeaderButtonconfig,
            load: {
                validation: () => pageApi.getParamId() != null,
                queryId: 464,
                params: { taskRequestId: pageApi.getParamId() },
                hideMessage: true,
                hideMessageError: true,
            },
            save: {
                queryId: 457,
                hideMessage: true,
                hideMessageError: true,
            },
            fields: [
                { name: 'task', label: 'Tarea', allowClear: true, editable: true, width: 150 },
                { type: 'date', name: "dateProcess", label: "Fecha Proceso", editable: true, format: 'DD/MM/YYYY', inputFormat: DATE_FORMAT.YYYYMMDD, },
                { type: 'date', name: "startTime", label: "Hora Inicio", editable: true, format: DATE_FORMAT.HHmm, inputFormat: DATE_FORMAT.HHmm, },
                { type: 'date', name: "endTime", label: "Hora Termino", editable: true, format: DATE_FORMAT.HHmm, inputFormat: DATE_FORMAT.HHmm, },
                { type: 'number', name: "people", label: "Nro.Personas", editable: true },
                { type: 'select', name: "unit", label: "Unidad M.", labelName: 'unitdes', editable: true, load: { queryId: 192 } },
                { type: 'number', name: "quantity", label: "Cantidad", editable: true },
                { type: 'number', name: "costTask", label: "Costo Tarea", editable: true },
                {
                    type: 'select', name: "toPrint", label: "Imprimir", labelName: 'printdes', editable: true, load: {
                        queryId: 465
                    }
                },
                { type: 'number', name: "orderTask", label: "Orden", editable: true },
                { ...COLUMN_DEFAULT.DELETE, label: 'Opciones', onClick: fnDelete, confirm: false }
            ]
        };

        const gridBarHeaderButtonconfig2 = {
            fields: [
                {
                    ...BUTTON_DEFAULT.NEW, label: 'Calcular Horas', onClick: () => {
                        const nuevo = getGrid2().getData().map(data => {
                            var hora1 = moment(data.endTime, 'HH:mm')
                            var hora2 = moment(data.startTime, 'HH:mm')
                            const restaTiempo = hora1.diff(hora2)
                            const difFechaMinutos = moment.duration(restaTiempo);
                            const horaCalculada = difFechaMinutos.format("HH:mm");//libreria
                            data.decimalHour = horaCalculada
                            data.totalHour = horaCalculada
                            return data
                        })
                        getGrid2().setData(nuevo)
                    }
                },
                { ...BUTTON_DEFAULT.NEW, label: 'Ranking de Tarea', onClick: fnNew2 },
                { ...BUTTON_DEFAULT.NEW, label: 'Agregar Personal', onClick: fnNew2 },
            ]
        }

        const gridConfig2 = {
            title: 'TAREAS DISPONIBLES',
            collapsible: true,
            showIndex: true,
            autoLoad: true,
            pagination: { server: true },
            rowId: 'taskPeopleId',
            barHeader: gridBarHeaderButtonconfig2,
            updateItem: {
                fnAfter: (row) => {

                }
            },
            load: {
                validation: () => pageApi.getParamId() != null,
                queryId: 466,
                params: { taskRequestId: pageApi.getParamId() },
                hideMessage: true,
                hideMessageError: true,
            },
            save: {
                queryId: 467,
                hideMessage: true,
                hideMessageError: true,
            },
            fields: [
                { type: 'select', name: 'namePeople', label: 'Persona', labelName: 'name', allowClear: true, editable: true, width: 150, load: { queryId: 523, search: true } },
                { type: 'date', name: "startTime", label: "Hora Inicio", editable: true, format: DATE_FORMAT.HHmm, inputFormat: DATE_FORMAT.HHmm, },
                { type: 'date', name: "endTime", label: "Hora Termino", editable: true, format: DATE_FORMAT.HHmm, inputFormat: DATE_FORMAT.HHmm, },
                { type: 'number', name: "package", label: "Nro.Envases", editable: true },
                { type: 'number', name: "grossWeight", label: "Peso Bruto", editable: true },
                { type: 'number', name: "quantity", label: "Cantidad", editable: true },
                {
                    name: "decimalHour", label: "Hora Decimal", readOnly: true, onChange: () => {
                        console.log(getGrid2().getFormEditable().getData(), 'editable')
                    }
                },
                { name: "totalHour", label: "Horas total", readOnly: true },
                { name: "unit", label: "Kg/Hr/P", readOnly: true },
                { ...COLUMN_DEFAULT.DELETE, label: 'Opciones', onClick: fnDelete2, confirm: false }
            ]
        };



        const barButtonconfig = {
            fields: [
                { ...BUTTON_DEFAULT.RETURN, link: "/paysheet/taskRequest/taskRequest-admin" },
                {
                    ...BUTTON_DEFAULT.SAVE, onClick: () => {
                        getForm().save()
                    }
                },
                {
                    ...BUTTON_DEFAULT.UPDATE, onClick: () => {
                        getGrid().save({ params: { taskRequestId: pageApi.getParamId() } })
                        getGrid2().save({ params: { taskRequestId: pageApi.getParamId() } })
                        getForm().update()


                    }
                },
            ]
        };


        return (
            <>
                <FormFw
                    name="form"
                    getParentApi={pageApi.getApi}
                    config={formConfig} />
                <div className="gridCol">
                    <GridFw name="grid" getParentApi={pageApi.getApi} config={gridConfig} />
                </div>
                <div className="gridCol">
                    <GridFw name="grid2" getParentApi={pageApi.getApi} config={gridConfig2} />
                </div>

                <BarButtonFw
                    name="bar"
                    getParentApi={pageApi.getApi}
                    config={barButtonconfig} />
            </>
        )
    }
}