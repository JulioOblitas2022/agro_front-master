export const getTaskRequestDetailMode = () => {
    return {
        new: {
            bar: {
                show: ['save', 'return']
            }
        },
        update:{
            bar: {
                show: ['update', 'return']
            }
        }
    }
};