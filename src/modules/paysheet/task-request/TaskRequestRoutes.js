import React from 'react'
import { Route, Switch, useRouteMatch } from 'react-router';
import { TaskRequestAdminPage } from './admin/TaskRequestAdminPage';
import { TaskRequestDetailPage } from './detail/TaskRequestDetailPage';

const TaskRequestRoutes = () => {
    const { path } = useRouteMatch();
    return (
        <Switch>
            <Route exact path={`${path}/taskRequest-admin`} component={TaskRequestAdminPage} />
            <Route path={`${path}/taskRequest`} component={TaskRequestDetailPage} />
            <Route path={`${path}/taskRequest/:id`} component={TaskRequestDetailPage} /> 
        </Switch>
    )
}

export default TaskRequestRoutes;