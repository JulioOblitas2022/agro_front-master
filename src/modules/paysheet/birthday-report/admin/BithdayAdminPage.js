import React from 'react'
import BarButtonFw, { BUTTON_DEFAULT } from '../../../../framework/bar-button/BarButtonFw';
import FormFw from '../../../../framework/form/FormFw';
import GridFw, {COLUMN_DEFAULT} from '../../../../framework/grid/GridFw';
import { B_SELECT_CONSTANTS } from "../../../shared/constantes/BSelectConstants";
import { BasePage } from '../../../../framework/pages/BasePage';
import ReportPDF from "./pdf/ReportPDF"
/* import { getConfigEquipmentAdmin } from './getConfigEquipmentAdmin'; */

export class BithdayAdminPage extends BasePage {

    createPagePropertiesBase(pageApi) {
        return {
            title: 'REPORTE DE CUMPLEAÑOS',
        };
    }

    renderPage(pageApi) {
        const getGrid = () => pageApi.getComponent('grid');
        const getForm = () => pageApi.getComponent('form');

        const formConfig = {
            title: 'Filtros de Búsqueda',
            fields: [
                { type: 'select', name: 'TIPO_SERVICIO', label: 'Empresa', search: true, column: 4, load: { queryId: 70 } },
                { type: 'select', name: 'monthBirthDate', label: 'Mes', search: true, column: 4, load: { queryId: 515 } },
                { type: 'select', name: 'checkPer', label: 'Personal Destacado', column: 4, load: { dataList: [
                    {label: 'No', value: 0},
                    {label: 'Si', value: 1}
                ]} },
            ]
        };

        const gridConfig = {
            showIndex: true,
            autoLoad: false,
            pagination: { server: true },
            load: {
                queryId: 514,
                orderBy: "1",
                hideMessage: true,
                hideMessageError: true,
            },
            fields: [                
                { name: 'documentPerson', label: 'Nro. DNI' },
                { name: 'nombre', label: 'Apellido y Nombres' },
                { name: 'dayBirthDate', label: 'Día' , },
                { name: 'des_checkPer', label: 'Destacado', },
            ]
        };

        const barButtonconfig = {
            fields: [
                { ...BUTTON_DEFAULT.SEARCH, onClick: () => getGrid().load({ params: getForm().getData() }) },
                { ...BUTTON_DEFAULT.CLEAR, onClick: () => getForm().reset() },
            ]
        };

        const barButtonconfig2 = {
            fields: [
                { label:'Exportar Reporte PDF', onClick: () =>
                    {   
                        if(getGrid().getData().length>0 && getForm().getData()['monthBirthDate']) {
                            ReportPDF(getGrid().getData())
                        }else {
                            alert("Falta ingresar el mes o no hay data")
                        }
                    }
                 
                },
            ]
        };
      

        return (
            <>
               <FormFw
                    name="form"
                    getParentApi={pageApi.getApi}
                    config={formConfig} />

                <BarButtonFw
                    name="bar"
                    getParentApi={pageApi.getApi}
                    config={barButtonconfig} />
                <GridFw
                    name="grid"
                    getParentApi={pageApi.getApi}
                    config={gridConfig}
                    />
                <BarButtonFw
                    name="bar"
                    getParentApi={pageApi.getApi}
                    config={barButtonconfig2} />
            </>
        )
    }
}