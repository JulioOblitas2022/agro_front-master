import jsPDF from 'jspdf'
import 'jspdf-autotable'
import moment from 'moment';

const ReportPDF = (data) => {

    console.log(data)

    const dataUsuarios = []
    data.map((data, index) => {
        dataUsuarios.push([
            data.documentPerson,
            data.nombre,
            data.dayBirthDate,
            data.checkPer === 1 ? 'DESTACADO' : ''
        ])
    })


    const diaActual = moment().format('DD/MM/YYYY');
    const horaActual = moment().format('hh:mm:ss');

    //datapdf
    const doc = new jsPDF('p', 'pt');
    doc.setFontSize(11)
    doc.setFont("helvetica", "bold");
    doc.text(450, 40, `Fch. Emisión: ${diaActual}`)
    doc.text(450, 55, `Hora Emisión: ${horaActual}`)
    doc.setFontSize(13)
    doc.text('REPORTE DE CUMPLEAÑOS ', 300, 95, 'center')
    doc.text(`Empresa :  NOMBRE EMPRESA`, 40, 125)
    doc.text(`Mes     :  ${data[0].nameMonth}`, 40, 140)
    doc.setFont("helvetica", "normal");
    doc.setFontSize(12)
    doc.autoTable({
        head: [['N° D.N.I', 'Apellidos y Nombres', 'Día', 'Destacado']],
        body: dataUsuarios,
        startY: 160,//138
        theme: 'plain',
        headStyles: {halign: 'center', lineWidth: {top:2, bottom: 2}, cellPadding: 3, fontSize: 11},
        bodyStyles: {cellPadding: {top : 10, right: 5, left:5},},
        styles: {lineWidth: 0, cellPadding: 5, fontSize: 10},
        })
    doc.save(`REPORTE_DE_CUMPLEAÑOS.pdf`)
}

export default ReportPDF
