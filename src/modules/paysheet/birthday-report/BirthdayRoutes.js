import React from 'react'
import { Route, Switch, useRouteMatch } from 'react-router';
import { BithdayAdminPage } from './admin/BithdayAdminPage';

const BirthdayRoutes = () => {
    const { path } = useRouteMatch();
    return (
        <Switch>
            <Route exact path={`${path}/birthday-admin`} component={BithdayAdminPage} />

        </Switch>
    )
}

export default BirthdayRoutes;