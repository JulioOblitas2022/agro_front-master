import React, { lazy } from 'react'
import { Route, Switch, useRouteMatch } from 'react-router-dom';

const SalesPlanRoutes = lazy(() => import('./sales-plan/SalesPlanRoutes'));

const ManagementRoutes = () => {
    const { path } = useRouteMatch();
    return (
        <Switch>
            <Route path={`${path}/salesPlan`} component={SalesPlanRoutes} />
        </Switch>
    )
}

export default ManagementRoutes;
