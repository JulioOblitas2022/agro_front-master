import React from 'react'
import BarButtonFw, { BUTTON_DEFAULT } from '../../../../framework/bar-button/BarButtonFw';
import FormFw from '../../../../framework/form/FormFw';
import GridFw, {COLUMN_DEFAULT} from '../../../../framework/grid/GridFw';
import { B_SELECT_CONSTANTS } from "../../../shared/constantes/BSelectConstants";
import { BasePage } from '../../../../framework/pages/BasePage';
/* import { getConfigEquipmentAdmin } from './getConfigEquipmentAdmin'; */

export class SalesPlanAdminPage extends BasePage {

    createPagePropertiesBase(pageApi) {
        return {
            title: 'PLAN DE VENTAS',
            barHeader: {
                fields:[
                    {...BUTTON_DEFAULT.NEW, label:'Nuevo', link: '/management/salesPlan/salesPlan/new'}
                ]
            }
        };
    }

    renderPage(pageApi) {
        const getGrid = () => pageApi.getComponent('grid');
        const getForm = () => pageApi.getComponent('form');

        const formConfig = {
            title: 'Filtros de Búsqueda',
            fields: [
                { name: 'documentPerson', label: 'Nro. Plan', column: 4 },
                { name: 'DESCRIPCION', label: 'Descripción', column:4},
                { type: 'date', name: 'FECH1', label: 'Fecha reg. Inicio', column: 4},
                { type: 'date', name: 'FECH2', label: 'Fecha reg. Hasta', column: 4},
            ]
        };

        const gridConfig = {
            showIndex: true,
            autoLoad: true,
            pagination: { server: true },
            load: {
                queryId: 519,
                orderBy: "1",
                hideMessage: true,
                hideMessageError: true,
            },
            fields: [
                { name: 'salesPlanId', label: 'Nro Plan' },
                { name: 'description', label: 'Descripción' },
                { name: 'dateRegister', label: 'Fch.Creación' , },
                { name: 'nameMonth', label: 'Mes de Inicio', },
                { name: 'des_active', label: 'Estado' ,},
                { ...COLUMN_DEFAULT.EDIT, label: 'Opciones', colSpan: 2 ,
                link: (value, row, index) => `/management/salesPlan/salesPlan/${row.salesPlanId}/update`
                },
                {
                    ...COLUMN_DEFAULT.DELETE, colSpan: 0,
                    process: {
                        fnOk: (result) => getGrid().load({ params: getForm().getData() }),
                        filter: (value, row, index) => ({ queryId: 521, params: { salesPlanId: row.salesPlanId } })
                    }
                },

            ]
        };


        const barButtonconfig = {
            fields: [
                { ...BUTTON_DEFAULT.SEARCH, onClick: () => getGrid().load({ params: getForm().getData() }) },
                { ...BUTTON_DEFAULT.CLEAR, onClick: () => getForm().reset() },
            ]
        };
      

        return (
            <>
               <FormFw
                    name="form"
                    getParentApi={pageApi.getApi}
                    config={formConfig} />

                <BarButtonFw
                    name="bar"
                    getParentApi={pageApi.getApi}
                    config={barButtonconfig} />
                <GridFw
                    name="grid"
                    getParentApi={pageApi.getApi}
                    config={gridConfig}
                />
            </>
        )
    }
}