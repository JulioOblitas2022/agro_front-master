import React from 'react'
import { Route, Switch, useRouteMatch } from 'react-router';
import { SalesPlanAdminPage } from './admin/SalesPlanAdminPage';
import { SalesPlanDetailPage } from './detail/SalesPlanDetailPage';

const SalesPlanRoutes = () => {
    const { path } = useRouteMatch();
    return (
        <Switch>
            <Route exact path={`${path}/salesPlan-admin`} component={SalesPlanAdminPage} />
            <Route path={`${path}/salesPlan`} component={SalesPlanDetailPage} />
            <Route path={`${path}/salesPlan/:id`} component={SalesPlanDetailPage} /> 
        </Switch>
    )
}

export default SalesPlanRoutes;