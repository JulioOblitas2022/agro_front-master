export const getEmployeeDetailMode = () => {
    return {
        new: {
            bar: {
                show: ['save', 'return']
            }
        },
        update:{
            bar: {
                show: ['update', 'return']
            }
        }
    }
};