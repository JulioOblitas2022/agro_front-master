import React from 'react'
import BarButtonFw, { BUTTON_DEFAULT } from '../../../../framework/bar-button/BarButtonFw';
import FormFw from '../../../../framework/form/FormFw';
import { PAGE_MODE, DATE_FORMAT } from '../../../../constants/constants';
import GridFw, { COLUMN_DEFAULT } from "../../../../framework/grid/GridFw";
import { BasePage } from '../../../../framework/pages/BasePage';
import { getEmployeeDetailMode } from './getSalesPlanDetailMode';
/* import { getConfigEquipmentAdmin } from './getConfigEquipmentAdmin'; */

export class SalesPlanDetailPage extends BasePage {

    createPagePropertiesBase(pageApi) {
        return {
            title: 'REGISTRO DE PLAN DE VENTAS',
            mode: getEmployeeDetailMode
        };
    }

    renderPage(pageApi) {
        const getGrid = () => pageApi.getComponent('grid');
        const getForm = () => pageApi.getComponent('form');

        const formConfig = {
            load: {
                queryId: 518,
                params: { salesPlanId: pageApi.getParamId() }
            },
            save: {
                queryId: 517,
                fnOk: (resp) => {
                    const obj = resp.dataObject
                    console.log(resp)
                    /*  getFormGridConfig().save({ params: { employeeId: obj.employeeId } }) */
                },
                postLink: (resp, values) => '/management/salesPlan/salesPlan-admin'
            },
            update: {
                queryId: 520,
                postLink: (resp, values) => '/management/salesPlan/salesPlan-admin'
            },
            fields: [
                {
                    label: "DATOS PRINCIPALES",
                    fields: [
                        { type: 'date', name: 'dateRegister', label: 'Fecha Creación', column: 4 },
                        { name: 'description', label: 'Descripción', column: 8 },
                        { type: 'select', name: 'monthId', label: 'Mes de Inicio', column: 4, load: { queryId: 515 } },
                        { type: 'select', name: 'historyId', label: 'Histórico', column: 4, load: { queryId: 516 } },
                    ]
                },
            ],
            onComponentLoad: (formApi, pageApi) => {
                if (pageApi.getPageMode() !== PAGE_MODE.NEW)
                    formApi.load();
            }
        };

        const gridConfig = {
            title: 'DETALLES DEL PLAN DE VENTAS',
            collapsible: true,
            showIndex: true,
            autoLoad: true,
            pagination: { server: true },
            rowId: 'taskId',
            load: {
                validation: () => pageApi.getParamId() != null,
                queryId: 464,
                params: { taskRequestId: pageApi.getParamId() },
                hideMessage: true,
                hideMessageError: true,
            },
            save: {
                queryId: 457,
                hideMessage: true,
                hideMessageError: true,
            },
            fields: [
                { name: 'task', label: 'Codigo'},
                { name: 'task', label: 'Descripción'},
                { type: 'select', name: "unit", label: "Unidad M.",load: { queryId: 192 }},
                { name: 'task', label: 'Mayo'},
                { name: 'task', label: 'Junio'},
                { name: 'task', label: 'Julio'},
                { name: 'task', label: 'Agosto'},
                { name: 'task', label: 'Septiembre'},
                { name: 'task', label: 'Octubre'},
                { name: 'task', label: 'Noviembre'},
                { name: 'task', label: 'Diciembre'},
                { name: 'task', label: 'Enero'},
                { name: 'task', label: 'Febrero'},
                { name: 'task', label: 'Marzo'},
                { name: 'task', label: 'Abril'},
            ]
        };



        const barButtonconfig = {
            fields: [
                { ...BUTTON_DEFAULT.RETURN, link: "/management/salesPlan/salesPlan-admin" },
                {
                    ...BUTTON_DEFAULT.SAVE, onClick: () => {
                        getForm().save()
                    }
                },
                {
                    ...BUTTON_DEFAULT.UPDATE, onClick: () => {
                        getForm().update()
                        /* getFormGridConfig().save({ params: { employeeId: pageApi.getParamId() } }) */
                    }
                },
            ]
        };


        return (
            <>
                <FormFw
                    name="form"
                    getParentApi={pageApi.getApi}
                    config={formConfig} />
                <GridFw name="grid" getParentApi={pageApi.getApi} config={gridConfig} />
                <BarButtonFw
                    name="bar"
                    getParentApi={pageApi.getApi}
                    config={barButtonconfig} />
            </>
        )
    }
}