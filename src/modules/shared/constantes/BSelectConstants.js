import { QueryCache } from "./QueryCache";

export const B_SELECT_CONSTANTS = {
    SEX: { type: 'select', name: 'sex', label: 'Sexo', column: 3, load: { queryId: QueryCache.SEX0.queryId } },
    SEDE: { type: 'select', name: 'Sede', label: 'Sede', column: 3, load: { queryId: 0, cache: true } },
    //
    DEPARTAMENTO: { type: 'select', name: 'departamentoId', label: 'Departamento', column: 4, allowClear: true, load: { queryId: 78 }, search: true },
    PROVINCIA: {
        type: 'select', name: 'provinciaId', label: 'Provincia', column: 4, allowClear: true, load: { queryId: 79 }, search: true,
        parents: [
            { name: 'departamentoId', paramName: 'departamentoId', required: true }
        ]
    },
    DISTRITO: {
        type: 'select', name: 'distritoId', label: 'Distrito', column: 4, allowClear: true, load: { queryId: 80 }, search: true,
        parents: [
            { name: 'provinciaId', paramName: 'provinciaId', required: true }
        ]
    },
    //
    DOC_REF: { type: 'select', name: 'docRefId', label: 'Doc. Referencia', column: 4, load: { queryId: QueryCache.DOC_REF_LIST.queryId, cache: true } },
    YEARS: { type: 'select', name: 'year', label: 'Año', column: 3, load: { queryId: QueryCache.LIST_YEARS.queryId, cache: true } },
    //
    TYPE_EXISTENCES: { type: 'select', name: 'typeExistence', label: 'Tipo de Existencia', column: 4, load: { queryId: 185 } },

    FAMILY: {
        type: 'select', name: 'familyId', label: 'Familia', column: 4, allowClear: true, load: { queryId: 190 },
        parents: [
            { name: 'itemTypeId', paramName: 'typeExistence', required: true }
        ]
    },
    CLASS: {
        type: 'select', name: 'classId', label: 'Clase', column: 4, allowClear: true, load: { queryId: 51 },
        parents: [
            { name: 'familyId', paramName: 'familyId', required: true }
        ]
    },
    SUB_CLASS: {
        type: 'select', name: 'subClassId', label: 'Sub-Clase', column: 4, allowClear: true, load: { queryId: 76 },
        parents: [
            { name: 'classId', paramName: 'classId', required: true }
        ]
    },
    //
    //
    ACTIVE: {
        type: 'select', name: 'active', label: 'Estado', column: 4,
        load: { queryId: QueryCache.ACTIVE.queryId, cache: true }
    },
};