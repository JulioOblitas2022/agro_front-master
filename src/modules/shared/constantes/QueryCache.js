import { getDataUtil } from "../util/getDataUtil";

export const QueryCache = {
    SEX0: {
        queryId: -1,
        dataList: [
            { label: 'Masculino', value: '1' },
            { label: 'Femenino', value: '2' },
            { label: 'Indefinido', value: '0' }
        ]
    },
    TIPO_DOCUMENTO: {//combo se sexo
        queryId: -2,
        dataList: [
            { label: 'DNI', value: '1' },
            { label: 'Pasaporte', value: '2' }
        ]
    },
    DOC_REF_LIST: {
        queryId: -7,
        dataList: [
            { label: 'BOLETA DE VENTA', value: '1' },
            { label: 'CANJE DE DOCUMENTOS', value: '2' },
            { label: 'CARGO ASOCIADO', value: '3' },
            { label: 'COMPROBANTE DE RETRACCION', value: '4' },
            { label: 'COMPROBANTE DE PERCEPCION', value: '5' },
            { label: 'COMPROBANTE DE PERCEPCION - VENTA INTERNA', value: '6' },
            { label: 'COMPROBANTE DE RETENCION', value: '7' },
        ]
    },
    LIST_YEARS: {
        queryId: -10,
        dataList: getDataUtil().years
    },
    ACTIVE: {
        queryId: -3,
        dataList: [
            { label: 'ACTIVO', value: '1' },
            { label: 'INACTIVO', value: '2' },
        ]
    }
};
