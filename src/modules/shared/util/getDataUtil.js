
export const getDataUtil = () => {
    const uploadYears = () => {
        let dataList = [];
        let date = new Date();
        let year = parseInt(date.getFullYear());
        let yearResult = year - 10;
        for (let i = year; i >= yearResult; i--) {
            dataList = [...dataList, {label: i, value: i}]
        }
        return dataList;
    }

    return {
        years: uploadYears(),
    }
}
