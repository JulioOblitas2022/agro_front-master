import { EnvConstants } from "../../../EnvConstants";
const getFormUrlFromQueryId = (url, queryId) => {
    return {
        url : url,
        request : {
            queryId: queryId
        }
    };
}
export const getFormDefaultParams = (config) => {
    config = config || {};
    return {
        load: config['queryLoad'] ? getFormUrlFromQueryId(EnvConstants.GET_FW_FORM_URL_LOAD_BASE(),config['queryLoad']) : null,
        save: config['querySave'] ? getFormUrlFromQueryId(EnvConstants.GET_FW_FORM_URL_SAVE_BASE(),config['querySave']) : null,
        update: config['queryUpdate'] ? getFormUrlFromQueryId(EnvConstants.GET_FW_FORM_URL_UPDATE_BASE(),config['queryUpdate']) : null,
        selectLoadUrl: EnvConstants.GET_FW_FORM_URL_SELECT_BASE()
    };
}

