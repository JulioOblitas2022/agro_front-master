import { EnvConstants } from "../../../EnvConstants";
export const getGridServerDefaultParams = (config) => {
    return {
        autoLoad: true,
        pagination: {//true or object definition
            server: true
        },
        load: {
            url: EnvConstants.GET_FW_GRID_URL_LOAD_BASE(),
            queryId: config['queryLoad'],
            orderBy: "1",
            params: config['params']
        }
    };
}
