import { createStore, combineReducers, applyMiddleware, compose } from 'redux';
import thunk from 'redux-thunk';
// import { createLogger } from 'redux-logger';
import { authentication } from '../reducers/authentication.reducer';

const composeEnhancers = (typeof window !== 'undefined' && window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__) || compose;

const reducers = combineReducers({
    auth: authentication,
});

// const loggerMiddleware = createLogger();

export const store = createStore(
    reducers,
    // applyMiddleware( thunk ),
    composeEnhancers(applyMiddleware( thunk ) )
);
