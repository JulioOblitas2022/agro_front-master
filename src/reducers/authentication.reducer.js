import { userConstants } from "../constants/userConstants";
import { StorageUtil } from "../util/StorageUtil";

const token = StorageUtil.getItem('ACCESS_TOKEN');
const initialState = token ? { isLoggedIn: true, access_token: token, loading: false } : {isLoggedIn: false, access_token: '', loading: false};

export const authentication = ( state = initialState, action ) => {

    switch ( action.type ) {
        case userConstants.LOGIN:
            return {
                ...state,
                access_token: action.payload,
                isLoggedIn: true
            }

        case userConstants.LOGOUT:
                return {
                    ...state,
                    access_token: '',
                    isLoggedIn: false
                }

        case userConstants.START_LOADING:
            return {
                ...state,
                loading: true
            }
    
        case userConstants.FINISH_LOADING:
            return {
                ...state,
                loading: false
            }
    
        default:
            return state;
    }

}